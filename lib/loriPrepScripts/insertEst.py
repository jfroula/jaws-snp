#!/usr/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from os.path import realpath, abspath, isfile, join, split, splitext

from os import curdir, mkdir, system, chdir

from sys import stderr, stdout

from optparse import OptionParser

from math import sqrt

import subprocess

#==============================================================
def printAndExecute( cmd, execute=True ):
    stdout.write( "%s\n"%cmd )
    if ( execute ): system( cmd )
    return 
#==============================================================
class helpfulFileClass(object):
    def __init__(self,fileName):
        self.fileName = fileName
        self.determineType()
        self.openFileHandle()
    
    def determineType(self):
        if ( isGzipFile(self.fileName) ):
            self.fileType = 'gzip'
        elif ( isBzipFile(self.fileName) ):
            self.fileType = 'bzip'
        else:
            self.fileType = 'uncompressed'
        #####
    
    def openFileHandle(self):
        if ( self.fileType == 'gzip' ):
            self.p = subprocess.Popen( 'cat %s | gunzip -c'%self.fileName, shell=True, stdout=subprocess.PIPE )
            self.oh = self.p.stdout
        elif ( self.fileType == 'bzip' ):
            self.p = subprocess.Popen( 'cat %s | bunzip2 -c'%self.fileName, shell=True, stdout=subprocess.PIPE )
            self.oh = self.p.stdout
        elif ( self.fileType == 'uncompressed' ):
            self.oh = open(self.fileName)
        #####
    
    def readline(self):
        return self.oh.readline()
    
    def close(self):
        if ( self.fileType == 'gzip' ):
            self.p.poll()
        elif ( self.fileType == 'bzip' ):
            self.p.poll()
        else:
            self.oh.close()
        #####
    

#==============================================================
def fasta_or_fastq( fileName ):

    # Testing the first four lines
    oh = helpfulFileClass(fileName)
    line1 = oh.readline()
    line2 = oh.readline()
    line3 = oh.readline()
    line4 = oh.readline()
    oh.close()
    
    if ( line1[0] == ">" ): return 'FASTA'

    if ( (line3[0] == '+') and \
         (line1[0] == '@') ): return 'FASTQ'

    return 'UNKNOWN'
#==============================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        stderr.write( '%s is a bzip2 file\n'%fileName )
        return True
    except IOError:
        pass
    #####
    return False
#==============================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        stderr.write( '%s is a gzip file\n'%fileName )
        return True
    except IOError:
        return False
    #####

#==============================================================
def generateTmpDirName( dirNum=None, tmpDirName=None ):
    if ( (dirNum==None) and (tmpDirName==None) ):
        return r'tmp.%s.%s'%( uname()[1], getpid() )
    elif ( (dirNum==None) and (tmpDirName!=None) ):
        return tmpDirName
    else:
        return r'tmp.%s.%d'%( uname()[1], dirNum )
    #####
    
#==============================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True
#==============================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ):
        raise IOError( 'Directory not found: %s'%dirName )
    return True
#==============================================================
class histogramClass(object):
    
    """
    Used to compute histogram.
    """
    
    def __init__(self, minVal, maxVal, nBins):
    
        if ( minVal == maxVal ):
            raise ValueError( 'Minimum and Maximum Values are equal in histogram')
        #####
    
        self.minVal = minVal
        self.maxVal = maxVal
        self.nBins  = int(nBins)
        self.dx     = (self.maxVal - self.minVal) / float( self.nBins )
        
        self.initialize()
    
    def initialize(self):
    
        # Forming the header string
        self.strLen     = 6
        columnLabel     = self.strLen * ['']
        columnLabel[0]  = 'Midpoint'
        columnLabel[1]  = 'Counts'
        columnLabel[2]  = 'cumCounts'
        columnLabel[3]  = 'NormHist'
        columnLabel[4]  = 'cumHist'
        columnLabel[5]  = ''
        
        self.offset          = [15 for n in xrange(self.strLen)]
        
        self.headerString = fileHeader_Class( columnLabel, \
                                        self.offset).generateHeaderString()
        
        # Initializing the arrays    
        self.histogram = self.nBins * [0]
        self.cumCounts = self.nBins * [0]
        self.normHist  = self.nBins * [0.0]
        self.cumHist   = self.nBins * [0.0]
        self.height    = 0
        return
    
    def __repr__(self):
        return self.generateOutputString()
        
    def __str__(self):
        return self.generateOutputString()
    
    def selectBin(self, x_value):
        if x_value == self.maxVal: return self.nBins - 1
        else: return int(  ( x_value - self.minVal ) / self.dx )
    
    def addData(self, x_value):
        index = self.selectBin(x_value)
        try:
            self.histogram[index] += 1
        except IndexError:
            if ( index > self.nBins ):
                raise ValueError( '%f produced an index out of range: %d > %d\n'% (x_value, index, self.nBins) )
    
    def addExtent(self, extent):
        indices = range(self.selectBin(extent[0]), self.selectBin(extent[1]))
        if not indices: indices = [self.selectBin(extent[0])]
        for index in indices:
            try:
                self.histogram[index] += 1
            except IndexError:
                if ( index > self.nBins ):
                    raise ValueError( '%f produced an index out of range: %d > %d\n'% (extent[1], index, self.nBins) )


    def normalizeHist(self):
        if not self.histogram: return
        
        # Normalize Histogram
        totalArea     = sum([(self.dx*float(item)) for item in self.histogram])
        self.normHist = [float(item)/totalArea for item in self.histogram]
    
        ## Cumulative distribution ##
        self.cumCounts    = self.nBins * [0]
        self.cumCounts[0] = self.histogram[0]
        
        self.cumHist      = self.nBins * [0.0]
        self.cumHist[0]   = self.normHist[0] * self.dx
        
        self.height = float(self.histogram[0])
        for n in xrange( 1, self.nBins ):
            self.cumCounts[n] = self.cumCounts[n-1] + self.histogram[n]
            self.cumHist[n]   = self.cumHist[n-1] + self.normHist[n] * self.dx
            if self.histogram[n] > self.height: self.height = float(self.histogram[n])
        #####
        return
    
    def findingBounds(self, alpha):
        # Looping through to find a more natural upper and lower bound
        self.normalizeHist()
        lowerBound = alpha
        upperBound = 1.0 - alpha
        minVal = None
        maxVal = None
        noMinVal = True
        noMaxVal = True
        for n in xrange( self.nBins ):
            # Looking at the lower bound
            if ( noMinVal and (self.cumHist[n] > lowerBound)  ):
                if ( (n-1) > 0 ):
                    minVal = self.minVal + float(n-1) * self.dx
                else:
                    minVal = self.minVal
                #####
                noMinVal = False
            #####
            
            # Looking at the upper bound
            if ( noMaxVal and (self.cumHist[n] > upperBound) ):
                maxVal = self.minVal + float(n) * self.dx
                noMaxVal = False
                break
            #####
        #####
        return minVal, maxVal
    
    def generateGraphString(self, index, char, width=20):
        if ( self.histogram[index] == 0 ):
            tmpStr = '|'
        else:
            numVals = self.height > 0 and max(1, int(width*(self.histogram[index]/self.height))) or 0
            tmpStr = '|%s'%( char * numVals )
        #####
        return tmpStr.ljust(width+1)
    
    def generateOutputString(self):
        
        # Check to see if there is anything in the histogram
        if ( sum(self.histogram) == 0 ):
            return ''.join( [self.headerString, 'EMPTY HISTOGRAM\n\n'] )
        #####
        
        # Normalize data
        self.normalizeHist()

        # Generating the formatting statements
        formatting      = ['%12.4e' for n in xrange(self.strLen)]
        formatting[0]   = '%12.2f'
        formatting[1]   = '%d'
        formatting[2]   = '%d'
        formatting[5]   = '|%s'
        
        # Generating the output string
        outputString = self.nBins * ['']
        for n in xrange( self.nBins ):
            # Generating the string (midpoint value)
            x_val = self.minVal + float(n) * self.dx + 0.50*self.dx
            tempString    = (self.strLen+1) * ['']
            tempString[0] = (formatting[0] % x_val            ).strip().ljust(self.offset[0])
            tempString[1] = (formatting[1] % self.histogram[n]).strip().ljust(self.offset[1])
            tempString[2] = (formatting[2] % self.cumCounts[n]).strip().ljust(self.offset[2])
            tempString[3] = (formatting[3] % self.normHist[n] ).strip().ljust(self.offset[3])
            tempString[4] = (formatting[4] % self.cumHist[n]  ).strip().ljust(self.offset[4])
            tempString[5] =  self.generateGraphString(n, "#", width=50)
            tempString[6] = '\n'
            outputString[n] = ''.join( tempString )
        #####
    
        return ''.join( [self.headerString, ''.join( outputString )] )

    def generateOutputDict(self):
        
        # Check to see if there is anything in the histogram
        outputDict = { 'midpoint':[], 'counts':[], 'hist':[] }
        if ( sum(self.histogram) == 0 ):
            return outputDict
        else:
            self.normalizeHist()
        #####
        
        for n in xrange( self.nBins ):
            # Generating the string (midpoint value)
            x_val = self.minVal + float(n) * self.dx + 0.50*self.dx
            outputDict['midpoint'].append( x_val )
            outputDict['counts'].append( self.histogram[n] )
            outputDict['hist'].append( self.generateGraphString(n, "#", width=50) )
        #####
    
        return outputDict

#==============================================================
def iterCounter( maxValue ):
    return newIterCounter(maxValue).next
#==============================================================
def newIterCounter( maxValue ):
    # Commification
    import re
    commify_re = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
    commify = lambda tmpInt: commify_re( ',', '%d'%(tmpInt) )
    from math import fmod
    outputText = '%s reads processed\n'
    tmpCounter = 1
    readCutoff = 10
    while (True ):
        tmpCounter += 1
        if ( not int(fmod( tmpCounter, readCutoff )) ):
            stderr.write( outputText%commify( tmpCounter ) )
            readCutoff = min(10 * readCutoff, maxValue )
        #####
        yield
    #####
    return
#==============================================================    
class SeqRecord(object):

    """
    A SeqRecord object holds a sequence and information about it.

    Main attributes:
      - id          - Identifier such as a locus tag (string)
      - seq         - The sequence itself (Seq object or similar)

    Additional attributes:
      - description - Additional text (string)
    """

    def __init__( self, seq, id = "<unk_id>", description = "<unk_descr>"):
        """
        Create a SeqRecord.
        """
        if (id is not None) and (not isinstance(id, basestring)):
            # Lots of existing code uses id=None... this may be a bad idea.
            raise TypeError("id argument should be a string")
        #####
        if not isinstance(description, basestring):
            raise TypeError("description argument should be a string")
        #####

        # Storing the values
        self._seq        = seq
        self.id          = id
        self.description = description

        self.baseAttrSet = set(["A_bases", "C_bases", "G_bases", \
                                "T_bases", "N_bases", "X_bases", \
                                "GC_bases", "AT_bases", "basePairs"])
    
    def __getattr__(self, name):
        if (name in self.baseAttrSet) and (name not in self.__dict__):
            self.countBases()
            return self.__dict__[name]
        else:
            raise AttributeError("No attribute '%s' in SeqRecord" % name)
        
    def countBases(self):
        # Computing useful quantities
        self.A_bases   = self.seq.count('A') + self.seq.count('a')
        self.C_bases   = self.seq.count('C') + self.seq.count('c')
        self.G_bases   = self.seq.count('G') + self.seq.count('g')
        self.T_bases   = self.seq.count('T') + self.seq.count('t')
        self.N_bases   = self.seq.count('N') + self.seq.count('n')
        self.X_bases   = self.seq.count('X') + self.seq.count('x')
        self.AT_bases  = self.A_bases + self.T_bases
        self.GC_bases  = self.G_bases + self.C_bases
        self.basePairs = self.AT_bases + self.GC_bases

    def _set_seq(self, value):
        self._seq = value

    seq = property( fget = lambda self : self._seq, \
                    fset = _set_seq, \
                    doc  = "The sequence, as a Seq or MutableSeq object." )

    def __getitem__(self, index):
        """
        Returns a sub-sequence or an individual letter.
        """
        if isinstance(index, int):
            return self.seq[index]
        #####
        raise ValueError, "Invalid index"

    def __iter__(self):
        """
        Iterate over the letters in the sequence.
        """
        return iter( self.seq )

    def __str__(self):
        """
        A human readable summary of the record and its annotation (string).
        """
        lines = []
        if ( self.id ): lines.append("ID: %s" % self.id)
        if ( self.description ):
            lines.append("Description: %s" % self.description)
        #####
        lines.append( repr(self.seq) )
        return '\n'.join( lines )

    def __repr__(self):
        """
        A concise summary of the record for debugging (string).
        Note that long sequences are shown truncated. Also note that any
        annotations, letter_annotations and features are not shown (as they
        would lead to a very long string).
        """
        return self.__class__.__name__ + "(seq=%s, id=%s, description=%s)"% \
               tuple(map(repr,(self._seq, self.id, self.description)))

    def format(self):
        """
        Returns the record as a string in the specified file format.
        The format should be a lower case string supported as an output
        format by Bio.SeqIO, which is used to turn the SeqRecord into a
        string.  e.g.
        """
        return self.__format__()

    def __format__(self):
        """
        Returns the record as a string in the specified file format.
        This method supports the python format() function added in
        Python 2.6/3.0.  The format_spec should be a lower case string
        supported by Bio.SeqIO as an output file format. See also the
        SeqRecord's format() method.
        """
        handle = StringIO()
        writeFASTA( [self], handle )
        return handle.getvalue()

    def __len__(self):
        """
        Returns the length of the sequence.
        For example, using Bio.SeqIO to read in a FASTA nucleotide file:
        """
        return len(self.seq)

    def __add__(self, other):
        """
        Add another sequence or string to this sequence.

        The other sequence can be a SeqRecord object, a Seq object, or a plain
        Python string. If you add a plain string or a Seq (like) object,
        the new SeqRecord will simply have this appended to the existing
        data. However, any per letter annotation will be lost:

        """
        if not isinstance(other, SeqRecord):
            # Assume it is a string or a Seq.
            return SeqRecord( (self.seq + other), id = self.id, \
                             description = self.description )
        #####

        # Adding two SeqRecord objects... must merge annotation.
        answer = SeqRecord( self.seq + other.seq )

        # Take common id/name/description/annotation
        if self.id == other.id:
            answer.id = self.id
        #####

        if self.description == other.description:
            answer.description = self.description
        #####

        return answer

    def __radd__(self, other):
        """Add another sequence or string to this sequence (from the left).

        This method handles adding a Seq object (or similar, e.g. MutableSeq)
        or a plain Python string (on the left) to a SeqRecord (on the right).
        """
        if isinstance(other, SeqRecord):
            raise RuntimeError("This should have happened via the __add__ of "
                               "the other SeqRecord being added!")
        #####
        return SeqRecord( ''.join([other,self.seq]), \
                          id = self.id, \
                          description = self.description )

#==============================================================
def iterFASTA( fastaFileHandle, arachneReads=False ):
    """Generator function to iterate over Fasta records (as SeqRecord objects).

    Call:  FastaIterator( fastaFileHandle, arachneReads )

    fastaFileHandle - input file handle
    """

    # Splitting pattern
    subPattern = re.compile( r'[\n\r ]' )
    subLine    = subPattern.sub

    if isinstance( fastaFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = fastaFileHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record ' + \
                              'encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ):
            break
        #####
    #####
    
    if ( arachneReads ):
        buildRecord = lambda lines, scaffID, descr: SeqRecord( Seq( ''.join(lines) ),
                                                               id          = descr.strip(),
                                                               description = scaffold_id )
    else:
        buildRecord = lambda lines, scaffID, descr: SeqRecord( Seq( ''.join(lines) ),
                                                               id          = scaffold_id,
                                                               description = descr.strip() )
    #####

    # Reading the contents of the FASTA file
    while True:

        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should " +
                              "start with '>' character" )
        #####

        # Pulling the id
        splitText = line[1:].split(None,1)

        try:
            scaffold_id = splitText[0]
        except IndexError:
            print line
            raise ValueError( "iterFASTA has detected an empty scaffold ID." +
                              " Make sure all scaffolds have IDs." )
        #####

        # Everything else is the description
        try:
            descr = splitText[1]
        except IndexError:
            descr = ''
        #####

        # Reading the sequence
        lines = []
        line  = fastaFileHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or \
                 (line[0] == '>') ): break

            # Removes internal and trailing whitespace and floating '\r'
            lines.append( subLine( '', line) )

            # Reading the next line
            line = fastaFileHandle.readline()
        #####

        # Yield up the record and wait for the next iteration
        yield buildRecord( lines, scaffold_id, descr )

        # Stopping the iteration
        if ( not line ): return

    #####

    assert False, 'Should never reach this line!!'
#==============================================================
class FastaWriter_class( object ):

    """
    Class to write Fasta format files.
    """

    def __init__(self, handle, wrap):

        # Defining the regular expression for cleaning
        self.subPattern = re.compile( r'[\n\r]' )
        self.clean      = self.subPattern.sub

        # Getting the handle
        self.handle = handle

        self.wrap = None
        if wrap:
            if wrap < 1:
                raise ValueError
            #####
        #####
        self.wrap = wrap

    def write_record(self, record):
        id          = self.clean( '', record.id )
        description = self.clean( '', record.description )
        if ( description and (description.split(None,1)[0] == id) ):
            title = description
        elif description:
            title = "%s %s" % (id, description)
        else:
            title = id
        #####
        self.handle.write(">%s\n" % title)
        try:
            data = str( record.seq )
        except AttributeError:
            if record.seq is None:
                raise TypeError("SeqRecord (id=%s) has None for its sequence." \
                                % record.id)
            else:
                raise TypeError("SeqRecord (id=%s) has an invalid sequence." \
                                % record.id)
            #####
        #####
        if self.wrap:
            for i in range( 0, len(data), self.wrap ):
                self.handle.write( data[i:i+self.wrap] + '\n' )
            #####
        else:
            self.handle.write( data + '\n' )
        #####
        return

    def write_records(self, records):
        count = 0
        for record in records:
            self.write_record(record)
            count += 1
        #####
        return count

    def write_file(self, records):
        count = self.write_records(records)
        return count

#==============================================================
def writeFASTA( sequences, handle, wrap=80 ):
    """
    Write complete set of sequences to a file.

     - sequences - A list (or iterator) of SeqRecord objects.
     - handle    - File handle object to write to.
     - format    - lower case string describing the file format to write.

    You should close the handle after calling this function.

    Returns the number of records written (as an integer).
    """

    if isinstance( handle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    if isinstance( sequences, SeqRecord ):
        raise ValueError( "Use a SeqRecord list/iterator, not just " + \
                          "a single SeqRecord" )
    #####

    # Writing the file
    count = FastaWriter_class( handle, wrap ).write_file( sequences )

    assert isinstance(count, int), "Internal error - the FASTA " \
           "writer should have returned the record count, not %s" \
           % ( repr(count) )

    return count
#==============================================================
def convertAndClip( ucQual,seq,name,charToAscii ):
    # Convert the qual to ints
    windowSize = 20
    qualCutOff = 25
    minVal     = windowSize * qualCutOff
    qual       = [charToAscii[item] for item in ucQual]
    # find HQ window
    qualLen     = len(qual) - windowSize + 1
    clippedList = ''
    for i in range( qualLen ):clippedList = clippedList+doesPass_dict[sum(qual[i:i+windowSize]) >= minVal]

    HQRanges = []
    for item in findStuff( clippedList ):HQRanges.append(item.span()) 
    
    try:
        clipStart = HQRanges[0][0] 
        clipEnd   = HQRanges[-1][1] + 19
        if (clipEnd +1 == qualLen +windowSize): clipEnd += 1
    except IndexError:
        return
    #####
    
    # Return the clipped read    
    return name,seq[clipStart:clipEnd],qual[clipStart:clipEnd]

#==============================================================
def estimateInsertSize(FASTA, ASSEMBLY, INDEX, upperBound = 2000, numberOfBins = 100):

    # Is needed to translate from fastq format
    charToAscii   = dict( [ (chr(val),(int(val)-offset)) for val in range(0,120)] )
    numberOfReads = 100000
    numberToSkip  = 100000
    # Pulling the fileNames
    FASTA         = realpath(FASTA)
    if ( not isfile(FASTA) ): parser.error( '%s can not be found'%FASTA )
    
    ASSEMBLY      = realpath(ASSEMBLY)
    if ( not isfile(ASSEMBLY) ): parser.error( '%s can not be found'%ASSEMBLY )

    # Indexing the genome
    indexName     = realpath(INDEX)
    
    # Create temporary directory
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpPath  = join( basePath, generateTmpDirName() )
    deleteDir(tmpPath)
    mkdir(tmpPath, 0777)
    testDirectory(tmpPath)
    
    # Changing the directory
    chdir( tmpPath )
    
    # Performing the alignments
    clipCmd = '/mnt/local/EXBIN/clip -B 10000 -f 25 -L 0 -c 75 %s'%FASTA
    
    # Make this into a subprocess and then feed them into the subprocess
    readProc = subprocess.Popen("cat %s | bunzip2 -c | awk '{if(NR%4==1){name=$1}if(NR%4==2){seq=$1}if(NR%4==0){print name,seq,$1}}'"%(FASTA), shell=True, stdout=subprocess.PIPE)
    
    contamProcess = subprocess.Popen( contamCMD , bufsize = 10000000, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE )
    
    for line in readProc.stdout: 
        record = line.split(None)
        try:
            name, seq, qual = convertAndClip( record[2], record[1], record[0], charToAscii )
        except TypeError:
            continue
        #####
    #####    

    cmd     = '%s | tail -n+%d | head -%d | awk -f /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScrits/estimateInsertSize.awk INDEX=%s'%(numberToSkip,numberOfReads,indexName)
    system( cmd )
    
    # Creating the insert size distribution
    alignCmd    = 'bwa sampe %s R1.sai R2.sai R1.fasta R2.fasta paired.sam'%indexName
    cmd         = '%s | grep -v ^@SQ | cut -f9 | grep -v ^0 | grep -v ^-' % alignCmd
    p           = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    insertSizes = [int(line.strip()) for line in p.stdout if unicode(line.strip()).isnumeric()]
    p.poll()

    # Changing directory
    chdir( basePath )
    
    # Performing the statistical analysis of the output
    tmpHist = histogramClass( 0, upperBound, numberOfBins )
    map( tmpHist.addData, filter(lambda x:(x>0 and x<=upperBound), insertSizes) )
    minVal, maxVal = tmpHist.findingBounds( 0.001 )
    minVal         = 0 if ( minVal < 100 ) else minVal
    maxVal         = int( 100 * round( float(maxVal) / 100.0 ) ) # Rounds up to the nearest 100
    
    # Creating the final histogram
    finalHist = histogramClass( minVal, maxVal, numberOfBins )
    map( finalHist.addData, filter(lambda x:(x>=minVal and x<=maxVal), insertSizes) )
    print finalHist.generateOutputString()
    outputDict = finalHist.generateOutputDict()
    
    # Computing the final statistics
    x = []
    map( x.append, filter(lambda x:(x>=minVal and x<=maxVal), insertSizes) )
    sum_x  = sum( map(float,x) )
    sum_x2 = sum( map( lambda x:float(x*x), x) )
    N      = float(len(x))
    mean   = sum_x / N
    std    = sqrt( sum_x2 / N - mean * mean )
    
    # Creating the plots
    outputFileBase = splitext( split(FASTA)[1] )[0]
    insertSizeFile = '%s.insertSizes'%outputFileBase
    oh = open( insertSizeFile, 'w' )
    nVals = len(outputDict['midpoint'])
    for n in xrange( nVals ):
        oh.write( '%.3f\t%d\n'%( outputDict['midpoint'][n], outputDict['counts'][n] ) )
    #####
    oh.close()
    
    # Writing the GNUplot file
    plotFile = '%s.plotFile'%outputFileBase
    oh = open( plotFile, 'w' )
    oh.write( 'reset\n' )
    oh.write( "set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\n" )
    oh.write( 'set ylabel \"Counts\"\n' )
    oh.write( 'set xlabel \"Insert Size (bp)\"\n' )
    oh.write( 'set nokey\n' )
    oh.write( 'set title \"%s, Insert=%.1f+/-%.1f\"\n'%(outputFileBase,mean,std) )
    oh.write( 'set output \"%s.insertSize.png\"\n'%outputFileBase )
    oh.write( 'set size ratio 0.50\n' )
    oh.write( 'set grid x y\n' )
    oh.write( 'set ytics\n' )
    oh.write( 'set autoscale y\n' )
    oh.write( 'plot \"%s\" using 1:2 title \"Map\" axis x1y1 with boxes\n'%insertSizeFile )
    oh.close()
    
    # Making the plot
    system( 'gnuplot %s'%plotFile )

    # Removing the temporary directory
    system( 'rm -rf %s'%tmpPath )
    
    return
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()