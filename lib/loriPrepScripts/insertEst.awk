BEGIN {RS=">"}

{

    # Splitting the record
    split($0,x,"\n")
    readID = x[1]
    seq    = x[2]

    # Changing the readID from "-R' to "/""
    gsub(/(-R)/, "/", readID)
    
    # Storing the R1
    if( substr(readID,length(readID),1) == 1 ) {
        R1_base  = substr(readID,1,length(readID)-1)
        R1_entry = ">"readID"\n"seq
        mainDict[R1_base] = R1_entry
    }

    # Do we have a pair?
    if( substr(readID,length(readID),1) == 2 ) {
            
        # Checking whether the R1 and R2 reads have a partner
        R2_base  = substr(readID,1,length(readID)-1)
        if ( R2_base in mainDict ){

            # Write out the reads
            print R1_entry         > "R1.fasta"
            print ">"readID"\n"seq > "R2.fasta"

            # Performing the alignments
            print R1_entry         | "/mnt/local/EXBIN/bwa aln -t 3 "INDEX" /dev/stdin > R1.sai"
            print ">"readID"\n"seq | "/mnt/local/EXBIN/bwa aln -t 3 "INDEX" /dev/stdin > R2.sai"

        }

    }

}