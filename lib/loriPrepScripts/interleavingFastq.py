#!/usr/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2013$"
#=========================================================================
# EDIT LOG
#
#
#
#
#=========================================================================
from time import time
from sys import  stderr, stdout
from os import system
import subprocess
from optparse import OptionParser
#=========================================================================
def iterCounter( maxValue ):
    return newIterCounter(maxValue).next
#=========================================================================
def newIterCounter( maxValue ):
    # Commification
    import re
    commify_re = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
    commify = lambda tmpInt: commify_re( ',', '%d'%(tmpInt) )
    from math import fmod
    outputText = '%s reads processed\n'
    tmpCounter = 1
    readCutoff = 10
    while (True ):
        tmpCounter += 1
        if ( not int(fmod( tmpCounter, readCutoff )) ):
            stderr.write( outputText%commify( tmpCounter ) )
            readCutoff = min(10 * readCutoff, maxValue )
        #####
        yield
    #####
    return
#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False
#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        return True
    except IOError:
        return False
    #####

#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
#=========================================================================        

def real_main():
    # Defining the program options
    usage = "usage: %prog [R1] [R2] [options]"

    parser = OptionParser(usage)

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
    
        headCmd = r"awk '{ if(NR%4==1){name = $0} if(NR%4==2){seq = $1} if(NR%4 == 0){print name,seq,$1} }'"
    
        fastqR1 = args[0]
        if ( isBzipFile(fastqR1) ):
            read1Cmd    = 'cat %s | bunzip2 -c'%fastqR1
            sepProcess1 = subprocess.Popen( '%s | %s'%(read1Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
        elif ( isGzipFile(fastqR1) ):
            read1Cmd    = 'cat %s | gunzip -c'%fastqR1
            sepProcess1 = subprocess.Popen( '%s | %s'%(read1Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
        else:
            read1Cmd    = 'cat %s'%fastqR1
            sepProcess1 = subprocess.Popen( '%s | %s'%(read1Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
        #####
        fastqR2 = args[1]
        if ( isBzipFile(fastqR2) ):
            read2Cmd    = 'cat %s | bunzip2 -c'%fastqR2
            sepProcess2 = subprocess.Popen( '%s | %s'%(read2Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
        elif ( isGzipFile(fastqR2) ):
            read2Cmd    = 'cat %s | gunzip -c'%fastqR2
            sepProcess2 = subprocess.Popen( '%s | %s'%(read2Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
        else:
            read2Cmd    = 'cat %s'%fastqR2
            sepProcess2 = subprocess.Popen( '%s | %s'%(read2Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
        #####
#        libBase = args[2]
        
        x = iterCounter(1000000)
        
    #####
    sepString = None
    while True:
        line = sepProcess1.readline()
        if ( sepString == None ):
            if ( line[0] == "@" ):
                readEnd = line.strip().split(" ")[0][-2:]
                sepString = " "
                if ( (readEnd == "/1") or (readEnd == "/2") ): sepString = "/"
                break
            else:
                continue
            #####
        #####
    #####
        
    lookingDict = {}
    sepProcess1 = subprocess.Popen( '%s | %s'%(read1Cmd, headCmd), shell=True, stdout=subprocess.PIPE).stdout
    while (True):
        try:
            r1Record = sepProcess1.next().split(None)
            r2Record = sepProcess2.next().split(None)
        except StopIteration:
            break
        #####
        name1 = r1Record[0].split("/")[0]
        name2 = r2Record[0].split("/")[0]
        
        if ((name1==name2)and(sepString != "/")):
            stdout.write("%s %s\n%s\n+\n%s\n%s %s\n%s\n+\n%s\n"%(r1Record[0], r1Record[1], r1Record[2], r1Record[3], r2Record[0], r2Record[1], r2Record[2], r2Record[3]))
            continue
        #####
        if ((name1==name2)and(sepString == "/")):
            if(len(r1Record)>3):
                r1Record[1] = r1Record[2]
                r1Record[2] = r1Record[3]
            #####
            
            stdout.write("%s\n%s\n+\n%s\n%s\n%s\n+\n%s\n"%("%s/1"%name1, r1Record[1], r1Record[2], "%s/2"%name2, r2Record[1], r2Record[2]))
            continue
        #####
         
        try:
            # the R2 in Dict print
            lookingDict[r1Record[0]]
            stdout.write("%s %s\n%s\n+\n%s\n%s %s\n%s\n+\n%s\n"%(r1Record[0], r1Record[1], r1Record[2], r1Record[3], lookingDict[r1Record[0]][0], lookingDict[r1Record[0]][1], lookingDict[r1Record[0]][2], lookingDict[r1Record[0]][3]))
            lookingDict.pop(r1Record[0])
        except KeyError:
            lookingDict[r1Record[0]] = r1Record 
        #####
        
        try:
            # the R1 in Dict print
            lookingDict[r2Record[0]]
            stdout.write("%s %s\n%s\n+\n%s\n%s %s\n%s\n+\n%s\n"%(lookingDict[r2Record[0]][0], lookingDict[r2Record[0]][1], lookingDict[r2Record[0]][2], lookingDict[r2Record[0]][3], r2Record[0], r2Record[1], r2Record[2], r2Record[3]))
            lookingDict.pop(r2Record[0])
        except KeyError:
            lookingDict[r2Record[0]] = r2Record
        #####       
    #####
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()
