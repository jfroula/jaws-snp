#!/usr/bin/python3
__author__="plott"
__date__ ="$July 12, 2013$"

from optparse import OptionParser

from os import system, curdir

from os.path import abspath, realpath, isfile

import subprocess

import os

from itertools import cycle, chain

from collections import deque

from sys import argv, stdin, stdout, stderr

from time import time, sleep

import re

# Regular Expression
commify_re  = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
#=========================================================================
def updatef(base, update={}):
        copy = dict()
        for k,v in base.items():
                copy[k] = v
        for k,v in update.items():
                copy[k] = v
        return copy
        
#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False
#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        return True
    except IOError:
        return False
    #####

#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    if ( isGzipFile(fileName) ): return 'cat %s | gunzip -c '%( fileName)
    if ( isBzipFile(fileName) ): return 'cat %s | bunzip2 -c '%( fileName)
    return 'cat %s '%( fileName)
#=========================================================================
def printAndExec(cmd, execute=True):
    """This function will take a command and print it to the screen and execute the command. If fasle is given for the second argument then the command will only be printed to the screen."""
    stderr.write("%s\n"%cmd)
    if (execute): system( cmd )
    return
    
#=========================================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )
#=========================================================================
def writeAndEcho( outputString, oh ):
    oh.write( outputString )
    stdout.write( outputString )
    return 
#==========================================================================
def logAndReport( cmdString ):
    log_out = open('predup.cmgLog','a')
    stderr.write(cmdString)
    log_out.write(cmdString)
    log_out.close()
#=========================================================================
def launchJob(merList, baseName, nFasta):
    tmp_oh   = open( '%s-%d.fasta'%(baseName, nFasta), 'w' )
    for name, seq in merList:  
        tmp_oh.write('>%s\n%s\n'%(name,seq))
    tmp_oh.close()
    
    shellHeader = "#$ -e /dev/null\n#$ -o /dev/null\n#$ -cwd\nmodule load blat/3.4\n"
    filterCmd   = r"awk '{ if ( ($10!=$14) && ($12<=1) && ($16<=1) && ((($1+$4)/$11) >= 0.98) ) print $10,$11,$14,$15 }'"
    blatCmd     = 'blat -noHead -t=dna -q=dna -tileSize=18 -extendThroughN %s-%s.fasta %s-%s.fasta stdout'%(baseName, nFasta, baseName, nFasta)
    # Make shell command and scripts
    screeningCmd = "%s%s | %s | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/predupGuts.py %s-%s\nrm %s-%s.fasta %s-%s.sh"%(shellHeader, blatCmd, filterCmd, baseName, nFasta, baseName, nFasta, baseName, nFasta)
    script       = open("%s-%d.sh"%(baseName, nFasta), "w")
    script.write(screeningCmd)
    script.close()
    
    while True:
        curRunning = [int(item.strip()) for item in subprocess.Popen('qstat | grep -c "%s-"'%(baseName), shell=True, stdout=subprocess.PIPE).stdout][0]
        if ( curRunning < 2000 ):break
        sleep(30)
    #####
    
    system("qsub %s-%d.sh"%(baseName, nFasta))
    return
    
#=========================================================================
def real_main():     
    # Global variables for stats
    global rejected
    # Defining the program options
    usage = "usage: %prog [FASTA][BASE NAME][options]"
    
    parser = OptionParser(usage)
    
    nReads = 2000
    parser.add_option( '-n', \
                       "--nReads", \
                       type    = 'int', \
                       help    = 'Number of reads per blat fasta: Default:  %d.'%(nReads), \
                       default = nReads )
    
    # Parsing the arguements
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling FASTA File
        fastaFile = realpath( args[0] )
        baseName  = args[1]
        frontCMD  = testFile( fastaFile ) 
    #####
    
    # is the file a fastq or a fasta
    formatTest = [item.strip() for item in subprocess.Popen("%s | head -4"%frontCMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout]
    if (formatTest[2] == "+"):
        insertedCMD = "awk '{if(NR%4==1){print \">\" substr($1,2) }if(NR%4==2){print}}' |"
    else:
        insertedCMD = " "
    #####
    
    
    
    # Building the awk command It is broken into part for easy understanding
    awkBeg  = 'BEGIN {RS = "-R1";n = 0;j = n = split("A C G T", t);for (i = 0; ++i <= n;){map[t[i]] = t[j--]}};'
    awkFun  = 'function reverseComp(s){p = "";for(i=length(s); i > 0; i--) { p = p map[substr(s, i, 1)] };return p};'
    awkChk  = '{split($2 , wholeName, "-");name = substr(wholeName[1], 2);if (substr(name, 1, 1) == "" ){prevName = substr($0, 2);next}{if (name != prevName){print \"The FASTA file is not interleaved. Use the interleaver to fix this and then try again\";print prevName"\\n"name;exit}'
    awkBld  = 'seq1=reverseComp($1);seq2=$3;mer1=substr($1, 1, 48);mer2=substr(seq2, 1, 48);mer=mer1 mer2;insert="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN";'
    awkOut  = 'print mer"\\t"name"\\t"seq1 insert seq2}prevName = substr($4, 2)}'
    awkCMD  = "awk '%s%s%s%s%s'"%(awkBeg, awkFun, awkChk, awkBld, awkOut)
    
    # AWK to split up the output
    
    # Change this into a subprocess
    print '%s | %s %s | sort -k1 '%(frontCMD, insertedCMD, awkCMD)
    
    
    handle      = subprocess.Popen('%s | %s %s | sort -k1'%(frontCMD, insertedCMD, awkCMD), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout
    
    # Process Parameters
    batchSize = options.nReads  
    
    # Stats information
    screened      = 0
    retained      = 0
    rejected      = 0
    tot96mer      = 0
    totReads      = 0
    excludeDict   = {}
    exclusionFile = 'excludedDuplicateReads.dat.bz2'
    oh_final      = Bzip_file(exclusionFile , 'w')
    
    # Starting our timer    
    start = time()    
    print("\tBlat and Screen")
    count       = 0
    nFasta      = 0
    allFinished = False
    for line in handle:
        mer, name, seq = line.split(None)
        
        totReads += 1
        # Still the same mer 
        try:
            if mer == currentMer:
                try:
                    # Is the read an exact match to another read in the group
                    if excludeDict[seq]:
                        # Yes. Count it as a duplicate and move to the next read
                        oh_final.write('%s-R1\n%s-R2\n'%(name, name))
                        rejected += 1
                        continue
                    #####
                except KeyError:
                    # No. All it to the list and dict to be processed
                    excludeDict[seq] = name
                    merList.append( ( name, seq ) )
                    continue
                #####    
            else:
                tot96mer += 1
                listLen   = len(merList) 
                # Builds the bin 
                if (listLen < batchSize): 
                    currentMer = mer
                    merList.append( ( name, seq ) )
                    continue
                #####
                if (listLen > 10000):
                    for chunk in range(listLen/10000 + 1): 
                        launchJob( merList[(chunk -1)*10000:chunk*10000], baseName, nFasta )
                        nFasta   += 1
                    #####
                    merList  = []
                    merList.append( ( name, seq ) )
                    excludeDict = {}
                    continue
                #####
                # Return the List and restarts the excludeDict
                launchJob( merList, baseName, nFasta )
                nFasta   += 1
                merList  = []
                merList.append( ( name, seq ) )
                excludeDict = {}
            #####   
        except NameError:
            # Getting the fist mer
            currentMer = mer
            tot96mer += 1
            merList    = []
            merList.append( ( name, seq ) ) 
            continue       
        #####
    #####
    launchJob( merList, baseName, nFasta )
    oh_final.close()
    
    # Clean up and sumation
    while True:
        curRunning = [int(item.strip()) for item in subprocess.Popen('qstat | grep -c "%s-"'%(baseName), shell=True, stdout=subprocess.PIPE).stdout][0]
        if (curRunning == 0):break
        sleep(30)
    #####
    
    duplicatePairs = [int(item.strip()) for item in subprocess.Popen("cat excludedDuplicateReads_%s-*.dat | awk '{print | \" bzip2 >> excludedDuplicateReads.dat.bz2\"}END{print NR/2}'"%(baseName), shell=True, stdout=subprocess.PIPE).stdout][0]
    
    system('rm -f excludedDuplicateReads_%s*.dat'%(baseName))
    oh_sum   = open('predupSummary.dat', 'w')
    binAvg   = (float(totReads) - float(rejected))/float(nFasta)
    totRejec = duplicatePairs + rejected
    retained = totReads - totRejec
    unique   = float(retained)/float(totReads)*100
    runTime  = time() - start
    writeAndEcho( "===============================\n", oh_sum )
    writeAndEcho( "SUMMARY:\n", oh_sum )
    writeAndEcho( "Total Pairs   \t%s\n"%commify(int(totReads)), oh_sum )
    writeAndEcho( "Total 96mers  \t%s\n"%commify(int(tot96mer)), oh_sum )
    writeAndEcho( "Pairs Rejected\t%s\n"%commify(int(totRejec)), oh_sum )
    writeAndEcho( "Pairs Retained\t%s\n"%commify(int(retained)), oh_sum )
    writeAndEcho( "Percent Unique\t%.2f\n"%unique, oh_sum )
    writeAndEcho( "Avg Batch Size\t%.2f\n"%binAvg, oh_sum )
    writeAndEcho( "Run Time\t%.2fs\n"%runTime, oh_sum )
    writeAndEcho( "===============================\n", oh_sum )
    oh_sum.close()

#==============================================================    
if ( __name__ == '__main__' ):
    real_main()