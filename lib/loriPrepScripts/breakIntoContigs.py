#!/usr/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import iterFASTA, writeFASTA, SeqRecord, iterCounter

import re

from os.path import realpath, isfile

from optparse import OptionParser

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [outputFileName] [options]"

    parser = OptionParser(usage)

    defLength = 0
    parser.add_option( "-m", \
                       "--minContigLength", \
                       type    = 'int', \
                       help    = "Minimum contig length.  Default=%d"%defLength, \
                       default = defLength )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTA = realpath(args[0])
        if ( not isfile(FASTA) ): parser.error( '%s can not be found'%FASTA )

        outputFileName = realpath(args[1])
    #####
    
    # Reading in the minimum contig length
    minContigLength = options.minContigLength
    
    # Contig breaker
    contigBreakFinder = re.compile( r'N{1,}' ).finditer
    oh = open( outputFileName, 'w' )
    x = iterCounter(10000)
    for record in iterFASTA( open(FASTA) ):
        # Pulling the sequence
        tmpSeq = str(record.seq)
        nBases = len(tmpSeq)
        # Creating the break indicees
        d = [item.span() for item in contigBreakFinder(tmpSeq)]
        d.insert(0,(0,0))
        d.append((nBases,nBases))
        nContig = 1
        for n in xrange(len(d)-1):
            start = d[n][1]
            stop  = d[n+1][0]
            contigSeq = tmpSeq[ start:stop ]
            if ( len(contigSeq) < minContigLength ): continue
            contigRecord = SeqRecord( id="%s|contig=%d"%(record.id,nContig), seq = contigSeq, description='' )
            writeFASTA( [contigRecord], oh )
            nContig += 1
        #####
        x()
    #####
    oh.close()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()


  
