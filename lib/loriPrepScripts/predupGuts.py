#!/usr/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2013$"
#=========================================================================
# EDIT LOG
#
#
#
#
#=========================================================================
from time import time, sleep
from sys import  stderr, stdin, argv
from os import system
import subprocess
from optparse import OptionParser
#=========================================================================
def screenSingle(nameSet, readList, ID1, ID2, size2):
    # Add the name to the global set
    nameSet. add(ID2)
    for group in readList:
        # Find the local set and add the new name
        if ID1 not in group[0] : continue
        group[0].add(ID2)
        # Who is bigger?
        winTest = sorted([(size2, ID2), group[1]])
        group[1] = winTest[-1]
        break
    #####
    return
#=========================================================================
def screenIndex(readList, ID1):
    for index in range(len(readList)):
        if ID1 not in readList[index][0]: continue
        IDIndex = index
        break
    #####
    return IDIndex
#=========================================================================
def real_main():
    
    # Screen the output
    exclusionFile = 'excludedDuplicateReads_%s.dat'%(argv[1])
    oh_final = open(exclusionFile , 'w')

    nameSet = set()
    readList = []
    for line in stdin:
        line = line.decode('utf-8')
        try:
            queryID, querySize, targetID, targetSize = line.split(None)
        except ValueError:
            continue
        #####
        qInSet = queryID in nameSet
        tInSet = targetID in nameSet
        nameSet. add(queryID)
        # Both are in a set already          
        if (qInSet and tInSet):
            # Get the tuple index
            targetIndex= screenIndex(readList, targetID)
            queryIndex = screenIndex(readList, queryID) 
            # Are they the same?
            if (queryIndex == targetIndex): continue
            # Collapse the lists
            newSet   = readList[targetIndex][0].union(readList[queryIndex][0])
            winTest  = sorted([readList[targetIndex][1],readList[queryIndex][1]])
            newWin   = winTest[-1]
            for popIndex in sorted([targetIndex, queryIndex], reverse=True): readList.pop(popIndex)
            readList.append([newSet, newWin])
        # Only the query is in a set
        elif(qInSet):
            screenSingle(nameSet, readList, queryID, targetID, targetSize)
        # Only the target is in a set
        elif(tInSet):
            screenSingle(nameSet, readList, targetID, queryID, querySize)               
        # Need a new tuple
        else:# Check sizes and make a new tuple and append it to the list
            # Add names to the sets
            newSet = set([queryID, targetID])
            nameSet.add(queryID)
            nameSet.add(targetID)
            # Which is bigger
            winTest =  sorted([(querySize, queryID), (targetSize, targetID)])
            winner  = winTest[-1]
            # Add it to the list
            readList.append([newSet, winner])
        #####
    #####
    # Remove the uniqe names from the set 
    for group in readList: nameSet.remove(group[1][1])
    #screened += len(nameSet)
    # Print the duplicate read names
    for name in nameSet: oh_final.write('%s-R1\n%s-R2\n'%(name, name))
    
    
    oh_final.close()           
    sleep(30)
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()