#!/usr/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2013$"
#=========================================================================
# EDIT LOG
#
#
#
#
#=========================================================================
from time import time
from sys import  stderr, stdout
from os import system
import subprocess
import gzip
from optparse import OptionParser
import re

# Useful Dictionaries
doesPass_dict = {True:'1',False:'0'}
charToAscii   = dict( [ (chr(val),(int(val)-33)) for val in range(33,120)] )
intToChar     = dict( [ ((int(val)-33),chr(val)) for val in range(33,120)] )
commify_re    = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
findStuff     = re.compile( r'(1+)' ).finditer
#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
        
#=========================================================================        
def isBzipFile( fileName ):
    try:
        import bz2
        x      = bz2.BZ2File(fileName)
        y1     = x.readline().strip()
        y2     = x.readline().strip()
        y3     = x.readline().strip()
        y4     = x.readline().strip()
        format = "FASTA"
        if (y3 == "+"): 
            format = "FASTQ"
        return format
    except IOError:
        pass
    #####
    print "I FAILED"
    return False, ""
#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x      = gzip.GzipFile(fileName)
        y1     = x.readline().strip()
        y2     = x.readline().strip()
        y3     = x.readline().strip()
        y4     = x.readline().strip()
        format = "FASTA"
        if (y3 == "+"): format = "FASTQ"
        return format
    except IOError:
        return False , ""
    #####

#=========================================================================
def isUncompressedFile(fileName):
    try:
        x      = open(fileName)
        y1     = x.readline().strip()
        y2     = x.readline().strip()
        y3     = x.readline().strip()
        y4     = x.readline().strip()
        format = "FASTA"
        if (y3 == "+"): format = "FASTQ"
        return format
    except IOError:
        return False , ""
    #####

#=========================================================================
def determineOutput(outFileName):
    splitName = outFileName.split(".")
    if (splitName[-1] == "gz"):
        return gzip.open(outFileName,"w")
    elif (splitName[-1] == "bz2"):
        return Bzip_file(outFileName,"w")
    else:
        return open(outFileName,"w")
    #####
    
#=========================================================================
def iterFASTA( fastaFileHandle ):
    """Generator function to iterate over Fasta records (as SeqRecord objects).

    Call:  FastaIterator( fastaFileHandle, arachneReads )

    fastaFileHandle - input file handle
    """

    # Splitting pattern
    subPattern = re.compile( r'[\n\r ]' )
    subLine    = subPattern.sub

    if isinstance( fastaFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = fastaFileHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record ' + \
                              'encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ):
            break
        #####
    #####
    
    
    buildRecord = lambda lines, scaffID, descr: {"seq":''.join(lines),
                                                "id":scaffold_id,
                                                "description":descr.strip()}
    #####

    # Reading the contents of the FASTA file
    while True:

        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should " +
                              "start with '>' character" )
        #####

        # Pulling the id
        splitText = line[1:].split(None,1)

        try:
            scaffold_id = splitText[0]
        except IndexError:
            print line
            raise ValueError( "iterFASTA has detected an empty scaffold ID." +
                              " Make sure all scaffolds have IDs." )
        #####

        # Everything else is the description
        try:
            descr = splitText[1]
        except IndexError:
            descr = ''
        #####

        # Reading the sequence
        lines = []
        line  = fastaFileHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or \
                 (line[0] == '>') ): break

            # Removes internal and trailing whitespace and floating '\r'
            lines.append( subLine( '', line) )

            # Reading the next line
            line = fastaFileHandle.readline()
        #####

        # Yield up the record and wait for the next iteration
        yield buildRecord( lines, scaffold_id, descr )

        # Stopping the iteration
        if ( not line ): return

    #####

    assert False, 'Should never reach this line!!'
#=========================================================================
def iterQUAL( qualFileHandle ):
    """Generator function to iterate over QUAL file handles (as QUALRecord 
       objects).

    Call:  iterQUAL( qualFileHandle, arachneReads )

    qualFileHandle:  MUST BE A FILE HANDLE!!
    """

    # Splitting pattern
    subLine = re.compile( r'[\n\r]' ).sub

    if isinstance( qualFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = qualFileHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record ' + \
                              'encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ):
            break
        #####
    #####
    
    

    buildRecord = lambda scores, scaffID, descr: { "qual":[int(item) for item in qual],
                                                   "id": scaffold_id,
                                                   "description" : descr.strip() }
    #####

    # Reading the contents of the FASTA file
    while True:

        if ( line[0] != ">" ):
            raise ValueError( "Records in QUAL files should " +
                              "start with '>' character" )
        #####

        # Pulling the id
        splitText = line[1:].split(None,1)

        try:
            scaffold_id = splitText[0]
        except IndexError:
            raise ValueError( "iterQUAL has detected an empty scaffold ID." +
                              " Make sure all scaffolds have IDs." )
        #####

        # Everything else is the description
        try:
            descr = splitText[1]
        except IndexError:
            descr = ''
        #####

        # Reading the scores
        lines = []
        line  = qualFileHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or \
                 (line[0] == '>') ) : break

            # Removes internal and trailing whitespace and floating '\r'
            lines.extend( [int(item) for item in subLine( '', line).split(None)] )

            # Reading the next line
            line = qualFileHandle.readline()
        #####

        # Yield up the record and wait for the next iteration
        yield buildRecord( lines, scaffold_id, descr )

        # Stopping the iteration
        if ( not line ):
            return
        ####

    #####

    assert False, 'Should never reach this line!!'
#=========================================================================
def getData(FASTA_oh, QUAL_oh):
    fasta_iter = iterFASTA(FASTA_oh)
    qual_iter  = iterQUAL(QUAL_oh)
    while True:
        try:
            fastaRecord = fasta_iter.next()
            qualRecord  = qual_iter.next()
        except StopIteration:
            break
        #####
        if (fastaRecord.id == qualRecord.id):
            yield [fastaRecord["id"], fastaRecord["seq"], qualRecord["qual"]]
        else:
            stderr.write("!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\nFILES ARE NOT IN THE SAME ORDER!!!\n\tORDER THE FILES ARE RESUBMIT\n!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\n")
            assert False
        #####
    #####
    return
    
#=========================================================================
def getDataFastq(FASTQ_oh):
    while True:
        try:
            name, seq, qual = FASTQ_oh.readline().split(None)
        except StopIteration:
            break
        #####
        qual = [charToAscii[item] for item in qual]
        
        yield [name, seq, qual]
    #####
    return
#========================================================================= 
def writeFASTA(oh_out, oh_qual, record):
    oh_out.write(">%s\n%s\n"%(record[0], record[1]))
    return
#========================================================================= 
def writeFASTA_QUAL(oh_out, oh_qual, record):
    oh_out.write(">%s\n%s\n"%(record[0], record[1]))
    qual = ' '.join([str(item) for item in record[2]])
    oh_qual.write(">%s\n%s\n"%(record[0], qual))
    return
#========================================================================= 
def writeFASTQ(oh_out, oh_qual, record):
    convertedQual = ''.join([intToChar[item] for item in record[2]])
    oh_out.write("@%s\n%s\n+\n%s\n"%(record[0], record[1]))
    return
#=========================================================================    
def real_main():
    # Defining the program options
    usage = "usage: %prog [FASTQ FILE] [options]"

    parser = OptionParser(usage)

    QUALCUTOFF = 25
    parser.add_option( '-f', \
                       "--QUALCUTOFF", \
                       type    = 'int', \
                       help    = 'Tartget Quality Score: %d.'%QUALCUTOFF, \
                       default = QUALCUTOFF )
                       
    MINLENGTH = 50
    parser.add_option( "-c", \
                       "--MINLENGTH", \
                       type    = 'int', \
                       help    = 'Minimum clipped read length.  Default:  %d'%MINLENGTH, \
                       default = MINLENGTH )
    
    qualFile = None
    parser.add_option( "-q", \
                       "--qualFile", \
                       type    = 'str', \
                       help    = 'Quality file to use for clipping a FASTA file. Default FASTQ input', \
                       default = qualFile )

    parser.add_option( '-o', \
                       "--output", \
                       type    = 'str', \
                       dest    = 'output', \
                       help    = "Output file name.  Default:  stdout." )
    parser.set_defaults( output = None )
    
    outputFormat = 'fasta'
    parser.add_option( "-r", \
                       "--format", \
                       type    = "str", \
                       help    = "Output format: fasta, fasta_qual, fastq. Default: fasta", \
                       default = outputFormat )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Is the input FASTQ or FASTA
        FASTQ = args[0] 
        if (isBzipFile( FASTQ)[0]):
            format = isBzipFile( FASTQ)
            readMe = "cat %s | bunzip2"%(FASTQ)
        elif (isGzipFile( FASTQ)[0]):
            format = isGzipFile( FASTQ)
            readMe = "cat %s | gunzip"%(FASTQ)
        elif (isUncompressedFile( FASTQ)[0]):
            format = isUncompressedFile( FASTQ)
            readMe = "cat %s"%(FASTQ)
        else:
            stderr.write("!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\nUNKNOWN FILE COMPRESSION!!!\n\tCHECK THE COMPRESSION AND RESUBMIT\n!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\n")
            assert False
        #####  
        
        print format
        assert False
        # Is the input FASTA
        if (format == "FASTA"):
            # Is there a QUAL for the FASTA
            if (options.qualFile == None):
                stderr.write("!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\nFASTA FORMAT WITH NO QUAL FILE!!!\n\tMUST HAVE QUAL FILE WHEN CLIPPING A FASTA FILE\n!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\n")
                assert False
            #####
            if (isBzipFile( options.qualFile)):
                qualReadMe = "cat %s | bunzip2"%(options.qualFile)
            elif (isGzipFile( options.qualFile)):
                qualReadMe = "cat %s | gunzip"%(options.qualFile)
            elif (isUncompressedFile( options.qualFile)):
                qualReadMe = "cat %s"%(options.qualFile)
            else:
                stderr.write("!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\nUNKNOWN FILE COMPRESSION!!!\n\tCHECK THE COMPRESSION OF QUAL FILE AND RESUBMIT\n!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\n")
                assert False
            ##### 
            dataIter   = getData(iterFASTA( subprocess.Popen(readMe, shell=True, stdout=subprocess.PIPE).stdout,  subprocess.Popen(qualReadMe, shell=True, stdout=subprocess.PIPE).stdout)())
        
        # Is the input FASTQ
        else:
            awkCMD   = "awk '{if(NR%4==1){name = $0}if(NR%4==2){seq = $1}if(NR%4==0){print name,seq,$1}}' "
            readMe   = "%s | %s"%(readMe, awkCMD)
            dataIter = getDataFastq( subprocess.Popen(readMe, shell=True, stdout=subprocess.PIPE).stdout )
        #####
        
        
        # What is the Out format and where is it writting to
        outFormat     = ["fasta","fasta_qual","fastq"]
        outFormatDict = {"fasta":writeFASTA,"fasta_qual":writeFASTA_QUAL,"fastq":writeFASTQ}
        outputFormat  = options.format.lower()
        oh_qual       = ''
        
        # Is it FASTA
        if (outputFormat == "fasta"):
            writeFunc = outFormatDict[outputFormat]
            if (options.output != None):
                oh_out = determineOutput(options.output)
            else:
                oh_out = stdout
            #####
        # Is it FASTA ans QUAL
        elif (outputFormat == "fasta_qual"):
            writeFunc = outFormatDict[outputFormat]
            if (options.output != None):
                oh_out = determineOutput(options.output)
                oh_qual = determineOutput(options.output.replace("fasta","qual"))
            else:
                stderr.write("!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\nFASTA_QUAL NEEDS AN OUTPUT FILE\n!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\n")
                assert False
            #####
        # Is it FASTQ
        elif (outputFormat == "fastq"):
            writeFunc = outFormatDict[outputFormat]
            if (options.output != None):
                oh_out = determineOutput(options.output)
            else:
                oh_out = stdout
            #####
        else:
            stderr.write("!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\nOUTOUT FORMAT NOT RECOGNIZED!!!\n\tFASTA,FASTA_QUAL,FASTQ ACCEPTED\n!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!\n")
            assert False
        ##### 
    #####
    
    # Here begins the clip
    #======================================================================================
    windowSize = 20
    qualCutOff = options.QUALCUTOFF
    minVal     = windowSize * qualCutOff
    
    while True:
        
        try:
            name, seq, qual = dataIter.next().split(None)
        except StopIteration:
            break
        #####     
        # find HQ window
        qualLen     = len(qual) - windowSize + 1
        clippedList = ''
        for i in range( qualLen ):clippedList = clippedList+doesPass_dict[sum(qual[i:i+windowSize]) >= minVal]
    
        HQRanges = []
        for item in findStuff( clippedList ):HQRanges.append(item.span()) 
        
        try:
            clipStart = HQRanges[0][0] 
            clipEnd   = HQRanges[-1][1] + 19
            if (clipEnd +1 == qualLen +windowSize): clipEnd += 1
        except IndexError:
            return
        #####
        
        # Return the clipped read    
        writeFunc(oh_out, oh_qual, [name,seq[clipStart:clipEnd],qual[clipStart:clipEnd]])
    #####    
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()