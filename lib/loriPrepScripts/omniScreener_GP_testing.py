#!/usr/bin/python3
__author__="plott"
__date__ ="$June 18, 2015$"

from optparse import OptionParser

from os import curdir, getpid, uname, mkdir, chdir, system

from os.path import abspath, realpath, isfile, curdir, join, isdir

import subprocess

import os

from itertools import cycle, chain

from collections import deque

from sys import argv, stdin, stdout, stderr

from time import time, sleep

import re

from shutil import rmtree

import time

# Regular Expression
commify_re  = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub

# lambda functions
terminate = lambda x: (not x) or (x[0] == '>')

#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
#=========================================================================
def updatef(base, update={}):
        copy = dict()
        for k,v in base.items():
                copy[k] = v
        for k,v in update.items():
                copy[k] = v
        return copy
        
#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False
#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        return True
    except IOError:
        return False
    #####

#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    if ( isGzipFile(fileName) ): return 'cat %s | gunzip -c '%( fileName)
    if ( isBzipFile(fileName) ): return 'cat %s | bunzip2 -c '%( fileName)
    return 'cat %s '%( fileName)
#=========================================================================
def printAndExec(cmd, execute=True):
    """This function will take a command and print it to the screen and execute the command. If fasle is given for the second argument then the command will only be printed to the screen."""
    stderr.write("%s\n"%cmd)
    if (execute): system( cmd )
    return
    
#=========================================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )
#=========================================================================
def writeAndEcho( outputString, oh ):
    oh.write( outputString )
    stdout.write( outputString )
    return 
#==========================================================================
def logAndReport( cmdString ):
    log_out = open('predup.cmgLog','a')
    stderr.write(cmdString)
    log_out.write(cmdString)
    log_out.close()
#=========================================================================
def iterItems( tmpHandle ):
    # Stripped down iterator for reads files
    while True:
        line = tmpHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        splitLine = line[1:].split(None)
        id = splitLine[0]
        try:
            desc = ' '.join( splitLine[1:] )
        except IndexError:
            desc = ''
        #####
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( terminate(line) ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield {'id':id.replace("|","_"), 'data':''.join(tmpData), 'desc':desc}
        # Stopping the iteration
        if ( not line ):
            return
        ####
    #####
    assert False, 'Should never reach this line!!'
#=========================================================================
def launchJob(baseName, record, alignCmdDict, dbList, totalBases):
    shellHeader  = "#!/bin/bash\n#$ -l ram.c=%dG\n#$ -e /dev/null\n#$ -o /dev/null\n#$ -cwd\nmodule load blast+/2.2.29\n"
    contigOutFile = "%s_%s.OmniScreen.tsv"%(baseName,record['id'].replace("|","_"))
    tmp_local = open(contigOutFile,"w")
    tmp_local.close()
    mem,cores = [1,1]
    fastaName = "%s_%s.fasta"%(baseName,record['id'].replace("|","_"))
    oh_fasta  = open(fastaName,"w")
    writeRecord( record, oh_fasta)
    for job in dbList:
        outStr = "%s\n%s\n"%(shellHeader%(mem), alignCmdDict[job]%(cores,fastaName, totalBases, job, record['id'].replace("|","_"), contigOutFile) )
        oh = open("%s_%s_%s.sh"%(baseName,record['id'].replace("|","_"),job),"w")
        oh.write(outStr)
        oh.close()
        system("qsub %s_%s_%s.sh"%(baseName,record['id'].replace("|","_"),job))
    #####
    return
    
#=========================================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True
#=========================================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True
#=========================================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )
#=========================================================================
def createTmpDirectory():
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpDir   = generateTmpDirName()
    tmpPath  = join( basePath, tmpDir )
    deleteDir(tmpPath)
    mkdir(tmpDir, 0o777)
    testDirectory(tmpDir)
    return tmpDir, tmpPath, basePath
#=========================================================================
def writeRecord( tmpRec, oh ):
    wrap = 100
    nBases = len( tmpRec['data'] )
    oh.write('>%s %s\n'%(tmpRec['id'],tmpRec['desc']))
    for i in range( 0, nBases, wrap ):
        oh.write( tmpRec['data'][i:i+wrap] + '\n' )
    #####
    return True
#=========================================================================
def printAndExec( cmd, execute=True ):
    stderr.write( '%s\n'%cmd )
    try:
        if ( execute ): system( cmd )
    except KeyboardInterrupt:
        print( "KeyboardInterrupt detected, stopping the execution" )
        exit()
    #####
    return
    
#=========================================================================    
class intervalClass(object):
    def __init__(self,start,end,fragID):
        start, end                = sorted([start,end])
        self.intervalDict         = {}
        self.intervalDict[fragID] = [start,end]

    def addInterval(self,start,end,fragID):
        start, end = sorted([start,end])
        try:
            self.intervalDict[fragID].append( (start,end) )
            self.collapseIntervals(fragID)
        except KeyError:
            self.intervalDict[fragID] = [(start,end)]
        #####
        return
        
    def isOverlapped(self,s1,e1,s2,e2):
        return not ( (s2>e1) or (s1>e2) )

    def collapseIntervals(self,fragID):
        notDone = True
        while (notDone):
            notDone    = False
            nIntervals = len( self.intervalDict[fragID] )
            for i in range(nIntervals-1):
                s1, e1 = intervals[i]
                for j in range( (i+1), nIntervals ):
                    s2, e2 = interval[j]
                    if ( self.isOverlapped(s1,e1,s2,e2) ):
                        # Compute the new bound
                        sortedBounds = sorted([s1,e1,s2,e2])
                        ns = sortedBounds[0]
                        ne = sortedBounds[-1]
                        # Collapsing the intervals
                        self.intervalDict[fragID][i] = (ns,ne)
                        self.intervalDict[fragID].pop(j)
                        notDone = True
                        break
                    #####
                #####
                if ( notDone ): break
            #####
        #####
        return

    def computeComposition(self,totalBases):
        alignBases = sum( [ (end-start+1) for start,end in self.intervalDict.values() ] )
        return 100.0 * float(alignBases) / float( totalBases )
        

#=========================================================================
def governor(strBase, nJobs):
    totalTime = 0
    timeInc   = 5
    checker   = 0
    p = subprocess.Popen( 'qstat | grep "%s"'%(strBase), shell=True, stdout=subprocess.PIPE )
    for line in p.stdout:
        checker += 1
    #####
    p.poll()
    if checker < nJobs: return
    
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        checker    = 0
        p = subprocess.Popen( 'qstat | grep "%s"'%(strBase), shell=True, stdout=subprocess.PIPE )
        for line in p.stdout:
            checker += 1
        #####
        p.poll()
        if checker < nJobs: break
        stdout.write( 'Jobs running........%d seconds\n'%totalTime )
    #####
    p.poll()
    stdout.write( 'Below %d, Launching More Jobs!!\n'%nJobs )
    return
#=========================================================================
def jobsFinished_cbp(strBase):
    totalTime = 0
    timeInc   = 60
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        checker    = 0
        p = subprocess.Popen( 'qstat | grep "%s"'%(strBase), shell=True, stdout=subprocess.PIPE )
        for line in p.stdout:
            checker += 1
        #####
        if checker == 0: break
        p.poll()
        stdout.write( '.' )
        if (totalTime%600 == 0 ): stdout.write( ' ' )
        if (totalTime%3600 == 0 ): stdout.write( '\n' )
        
    #####
    p.poll()
    stdout.write( 'Jobs Finished!!\n' )
    return
#=========================================================================

def real_main():     
    # Global variables for stats
    global rejected
    # Defining the program options
    usage = "usage: %prog [FASTA][BASE NAME][options]"
    
    parser = OptionParser(usage)
    
    nJobs = 9000
    parser.add_option( '-n', \
                       "--nJobs", \
                       type    = 'int', \
                       help    = 'Maximum number of jobs in queue at any one time: Default:  %d.'%(nJobs), \
                       default = nJobs )
    
    parser.add_option( '-f', \
                       "--prokScreen", \
                       action  = "store_true", \
                       dest    = 'prokScreen', \
                       help    = "Perform the prokaryote protein screen.  Default:  No prokaryotic screen." )
    parser.set_defaults( prokScreen = False )

    parser.add_option( '-s', \
                       "--smallProkScreen", \
                       action  = "store_true", \
                       dest    = 'smallProkScreen', \
                       help    = "Perform the prokaryote protein screen with 20% of the full prokaryote protein screen.  Default:  No prokaryotic screen." )
    parser.set_defaults( smallProkScreen = False )


    parser.add_option( '-L', \
                       "--noLengthScreen", \
                       action  = "store_true", \
                       dest    = 'noLengthScreen', \
                       help    = "Do not screen contigs for length.  Default:  1,000bp <= contig <= 100,000bp" )
    parser.set_defaults( noLengthScreen = False )

    parser.add_option( '-R', \
                       "--rDNA_only", \
                       action  = "store_true", \
                       dest    = 'rDNA_only', \
                       help    = "Only screen rDNA.  Default: Screen rdna, chloro, and mito" )
    parser.set_defaults( rDNA_only = False )

    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )

    else:
        # Pulling FASTA File
        fastaFile = realpath( args[0] )
        baseName  = args[1]
        readCmd  = testFile( fastaFile ) 
    #####
    #=================================================================
    # Creating a temporary directory
    tmpDir, tmpPath, basePath = createTmpDirectory()
    chdir( tmpPath )
    
    # Break into contigs
    tmpContigsFile = join( tmpPath, 'tmp.contigs.fasta' )
    printAndExec( 'python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/breakIntoContigs.py %s %s'%(fastaFile,tmpContigsFile) )
    
    
    if ( options.rDNA_only ):
        dbList = ['rdna']
    else:
        dbList = ['rdna', 'mito', 'chloro', 'fungal']
        if (options.smallProkScreen): dbList = dbList + ['prok_1']
        if ( options.prokScreen )   : dbList = dbList + ['prok_1','prok_2','prok_3','prok_4','prok_5']
    #####

    # Computing the blasted bases
    totalBases = {}
    gcDict     = {}    
    # Commands and Arguments
    # Alignment filtering command
    # Filters for a bitscore of >=300, and then appends the dbtype to the end of the alignment
    # BLASTX Parameters
    # a = 2  enables up to 2 processors to be used for the blast
    # Q = 11 is the bacterial/archeal translation table
    # f = 14 is the neighborhood score to seed an alignment
    # W = 3  is the seed size for seeding alignments
    # -F 'm S' -U:  "m" means masking is on, "S" means we are using SEG to mask low complexity protein sequences, and -U means we are masking lower case letters.
    # e = 1 sets the upper bound on the E-value
    # m = 8 is the single line summary output format
    # b = 10000 truncates the report to this many alignments
    # v = 10000 number of database sequences to show
    # MEGABLAST Parameters
    # a = 2 enables up to 2 processors to be used for blast
    # b = 0 means show all alignments
    # f = T show full ID in the output
    # D = 3 essentially equivalent ot "-m 8" in blast

    shellHeader  = "#!/bin/bash\n#$ -l ram.c=%dG\n#$ -e /dev/null\n#$ -o /dev/null\n#$ -cwd\nmodule load blast+/2.2.29\n"
    
    coreNumDict  = {False:[5,2],True:[40,8]}
    
    alignCmdDict = {'rdna'  : "megablast -d /home/l3d43/screeningData/rdna.fasta -a %d -i %s -b 0 -f T -D 3 -o stdout | awk '{if ($12>=300) print}' | sed 's/$/ rdna/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'mito'  : "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/mitochondrionDb | awk '{if ($12>=300) print}' | sed 's/$/ mito/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s ",\
                    'chloro': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/plastidDb | awk '{if ($12>=300) print}' | sed 's/$/ chloro/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'fungal': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/fungalProt | awk '{if ($12>=300) print}' | sed 's/$/ fungal/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'prok_1': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/microbialDB_1 | awk '{if ($12>=300) print}' | sed 's/$/ prok_1/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'prok_2': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/microbialDB_2 | awk '{if ($12>=300) print}' | sed 's/$/ prok_2/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'prok_3': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/microbialDB_3 | awk '{if ($12>=300) print}' | sed 's/$/ prok_3/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'prok_4': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/microbialDB_4 | awk '{if ($12>=300) print}' | sed 's/$/ prok_4/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
                    'prok_5': "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/microbialDB_5 | awk '{if ($12>=300) print}' | sed 's/$/ prok_5/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s" }
    
    maxLength = 100000
    minLength = 1000
    for record in iterItems(subprocess.Popen("cat %s"%(tmpContigsFile),shell=True,stdout=subprocess.PIPE).stdout):
        # Pull the sequence data
        recordName  = record['id']
        recordSeq   = record['data']
        recordBases = len(recordSeq)
        if (recordBases > 10000):
            # Break the contig into 10KB chunks and run them
            nChunks  = int(recordBases/10000)
            leftOver = recordBases%10000
            for i in range(nChunks):
                name = "%s_%d"%(recordName, i)
                seq  = recordSeq[i*10000:(i+1)*10000]
                totalBases[name] = 10000
                gcDict[name]     = (seq.upper().count('G') + seq.upper().count('C')) * 100.0 / 10000
                launchJob(baseName, {"id":name,"data":seq,'desc':""}, alignCmdDict, dbList, 10000)
                counter = i
            #####
            if (leftOver < 1000): continue
            name = "%s_%d"%(recordName, counter+1)
            seq  = recordSeq[-leftOver:]
            totalBases[name] = leftOver
            gcDict[name]     = (seq.upper().count('G') + seq.upper().count('C')) * 100.0 / leftOver
            launchJob(baseName, {"id":name,"data":seq,'desc':""}, alignCmdDict, dbList, leftOver)
        elif ( (not options.noLengthScreen) and ((recordBases<minLength) or (recordBases>maxLength)) ): 
            continue
        else:
            totalBases[recordName] = recordBases
            gcDict[recordName]     = (recordSeq.upper().count('G') + recordSeq.upper().count('C')) * 100.0 / recordBases
            launchJob(baseName, record, alignCmdDict, dbList, recordBases)
        #####
        governor(baseName[:10], options.nJobs)
    #####
    
    jobsFinished_cbp(baseName[:10])
    
    outFileList = [item.strip() for item in subprocess.Popen("ls %s*.OmniScreen.tsv"%(baseName), shell=True, stdout=subprocess.PIPE).stdout]
    
    oh_final = open("%s.omniScreen.tsv"%(baseName),"w")
    
    mainString = ['scaffID']
    mainString.append( 'screenedBases' )
    mainString.append( 'GC_per' )
    perStr = '%'
    for item in dbList: mainString.append( r'%s%s'%(item,perStr) )
    for item in dbList: mainString.append( 'top_%s'%item )
    underline = [len(item)*'-' for item in mainString]
    oh_final.write( '%s\n'%('\t'.join(mainString)) )
    oh_final.write( '%s\n'%('\t'.join(underline)) )

    for output in outFileList:
        # Pull the scaffold result file
        resultDict   = dict([(line.split(None)[1],[line.split(None)[0],line.split(None)[2],line.split(None)[3]])for line in open(output)])
        currentScaff = resultDict[dbList[0]][0]
        # ScaffID
        outputString = [currentScaff]
        # Screened Bases
        outputString.append(str(totalBases[currentScaff]))
        # GC content
        outputString.append('%.2f'%(gcDict[currentScaff]))
        for dbType in dbList: outputString.append(resultDict[dbList[0]][1])
        for dbType in dbList: outputString.append(resultDict[dbList[0]][2])
        oh_final.write('%s\n'%( '\t'.join(outputString) ))
    #####
    
    # Move the results
    system("mv %s.omniScreen.tsv %s "%(baseName, basePath))
        
    # Back into the working directory
    chdir( basePath )
    
    # Taking out the trash
    #deleteDir( tmpPath )    
    
#==============================================================    
if ( __name__ == '__main__' ):
    real_main()