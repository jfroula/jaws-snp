#!/apps/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

# Local Library Imports
from hagsc_lib import generateTmpDirName, deleteDir, testDirectory, remoteServer4, deleteFile, iterFASTA, commify

# Python Library Imports
from optparse import OptionParser

from sys import stdout, stderr

from os.path import isfile, abspath, join, realpath

from os import curdir, mkdir

from math import log10

import gzip

#==============================================================
def iterBLASTN( tmpHandle ):
    for line in tmpHandle:
        splitLine = line.split(None)
        tmpDict                = {}
        tmpDict['queryID']     = splitLine[0]
        tmpDict['subjectID']   = splitLine[1]
        tmpDict['perID']       = float(splitLine[2])
        tmpDict['alignLength'] = int(splitLine[3])
        tmpDict['mismatch']    = int(splitLine[4])
        tmpDict['gapOpening']  = int(splitLine[5])
        tmpDict['qStart']      = int(splitLine[6])
        tmpDict['qEnd']        = int(splitLine[7])
        tmpDict['sStart']      = int(splitLine[8])
        tmpDict['sEnd']        = int(splitLine[9])
        tmpDict['eValue']      = float(splitLine[10])
        tmpDict['bitScore']    = float(splitLine[11])
        yield tmpDict
    #####
    return

#==============================================================
def blastHeader():
    headerItem     = 12 * ['']
    headerItem[0]  = 'queryID'
    headerItem[1]  = 'subjectID'
    headerItem[2]  = 'perID'
    headerItem[3]  = 'alignLength'
#    headerItem[4]  = 'mismatch'
#    headerItem[5]  = 'gapOpening'
    headerItem[4]  = 'qSize'
    headerItem[5]  = 'qStart'
    headerItem[6]  = 'qEnd'
    headerItem[7]  = 'sSize'
    headerItem[8]  = 'sStart'
    headerItem[9]  = 'sEnd'
    headerItem[10] = 'eValue'
    headerItem[11] = 'bitScore'
    dashItem = 12 * ['']
    for n in xrange(10): dashItem[n] = len(headerItem[n]) * '-'
    outputString = [ '\t'.join( headerItem ), '\n', '\t'.join( dashItem ) ]
    return ''.join(outputString)

#==============================================================
def strITEM( record, contigSizes, cloneSizes ):
    strItem     = 13*['']
    strItem[0]  = record['queryID']
    strItem[1]  = record['subjectID']
    strItem[2]  = '%.2f'%record['perID']
    strItem[3]  = '%d'%record['alignLength']
#    strItem[4] = '%d'%record['mismatch']
#    strItem[5] = '%d'%record['gapOpening']
    strItem[4]  = '%d'%contigSizes[record['queryID']]
    strItem[5]  = '%d'%record['qStart']
    strItem[6]  = '%d'%record['qEnd']
    strItem[7]  = '%d'%cloneSizes[record['subjectID']]
    strItem[8]  = '%d'%record['sStart']
    strItem[9]  = '%d'%record['sEnd']
    strItem[10] = '%.2G'%record['eValue']
    strItem[11] = '%.2f'%record['bitScore']
    strItem[12] = '\n'
    return '\t'.join(strItem)

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [CLONE FASTA] [options]"

    parser = OptionParser(usage)

    targetRegions = 'ALL'
    parser.add_option( "-r", \
                       "--targetRegion", \
                       type    = 'string', \
                       help    = "Specific target region (M=MORPHOLOGICAL, DS=DISEASE_SCN, DR=DISEASE_RPP, J=JACKSON_LAB, SI=SEED_COMPOSITION_ISO, SP=SEED_COMPOSITION_PRO.  Default=All target regions.", \
                       default = 'ALL' )

    parser.add_option( "-M", \
                       "--useMaskedTargetRegions", \
                       action  = 'store_true', \
                       dest    = 'useMaskedTargetRegions', \
                       help    = "Use Masked Target Regions:  Default=False" )
    parser.set_defaults( useMaskedTargetRegions = False )

    # Parsing the arguements
    (options, args)      = parser.parse_args()
    specificTargetRegion = options.targetRegion
    targetRegionSet      = set(['ALL', 'M', 'DS', 'DR', 'J', 'SI', 'SP'])
    if ( specificTargetRegion not in targetRegionSet ):
        parser.error( 'The region specified: %s, is not valid\n'%specificTargetRegions )
    #####
    
    # Pulling the clone file
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        cloneFASTA = realpath( args[0] )
        if ( not isfile(cloneFASTA) ): parser.error( '%s can not be found'%cloneFASTA )
        contigSizes = dict( [( record.id,len(record.seq) ) for record in iterFASTA(open(cloneFASTA))] )
    #####
    
    # Specifying the target regions
    targetRegionDir          = '/home/t3c2/AIP4/JWJ_ANALYSIS/Soybean/soymap/targetRegions'
    targetRegion_dict        = {}
    if ( options.useMaskedTargetRegions ):
        targetRegion_dict['M']   = ['Gm03_start=42374079_end=42694079_MORPHOLOGICAL_DT_2.fasta.masked', \
                                    'Gm19_start=44819743_end=45139743_MORPHOLOGICAL_DT_1.fasta.masked']
        targetRegion_dict['DS']  = ['Gm18_start=1656025_end=2010234_DISEASE_RESISTANCE_SCN_1.fasta.masked', \
                                    'Gm11_start=37045905_end=37398911_DISEASE_RESISTANCE_SCN_2.fasta.masked']
        targetRegion_dict['DR']  = ['Gm09_start=44890000_end=45210000_DISEASE_RESISTANCE_RPP_2.fasta.masked', \
                                    'Gm18_start=55645000_end=55965000_DISEASE_RESISTANCE_RPP_1.fasta.masked']
        targetRegion_dict['SI']  = ['Gm07_start=37104977_end=37424977_SEED_COMPOSITION_ISO_1.fasta.masked',\
                                    'Gm13_start=27391000_end=27711000_SEED_COMPOSITION_ISO_2.fasta.masked']
        targetRegion_dict['SP']  = ['Gm20_start=32190000_end=32510000_SEED_COMPOSITION_PRO_1.fasta.masked',\
                                    'Gm10_start=37085000_end=37405000_SEED_COMPOSITION_PRO_2.fasta.masked']
        targetRegion_dict['J']   = ['Gm08_start=12200797_end=12643245_JACKSON_LAB_1.fasta.masked',\
                                    'Gm15_start=49538981_end=49864656_JACKSON_LAB_2.fasta.masked']
        targetRegion_dict['ALL'] = ['allTargetRegions.fasta.masked']
    else:
        targetRegion_dict['M']   = ['Gm03_start=42374079_end=42694079_MORPHOLOGICAL_DT_2.fasta', \
                                    'Gm19_start=44819743_end=45139743_MORPHOLOGICAL_DT_1.fasta']
        targetRegion_dict['DS']  = ['Gm18_start=1656025_end=2010234_DISEASE_RESISTANCE_SCN_1.fasta', \
                                    'Gm11_start=37045905_end=37398911_DISEASE_RESISTANCE_SCN_2.fasta']
        targetRegion_dict['DR']  = ['Gm09_start=44890000_end=45210000_DISEASE_RESISTANCE_RPP_2.fasta', \
                                    'Gm18_start=55645000_end=55965000_DISEASE_RESISTANCE_RPP_1.fasta']
        targetRegion_dict['SI']  = ['Gm07_start=37104977_end=37424977_SEED_COMPOSITION_ISO_1.fasta',\
                                    'Gm13_start=27391000_end=27711000_SEED_COMPOSITION_ISO_2.fasta']
        targetRegion_dict['SP']  = ['Gm20_start=32190000_end=32510000_SEED_COMPOSITION_PRO_1.fasta',\
                                    'Gm10_start=37085000_end=37405000_SEED_COMPOSITION_PRO_2.fasta']
        targetRegion_dict['J']   = ['Gm08_start=12200797_end=12643245_JACKSON_LAB_1.fasta',\
                                    'Gm15_start=49538981_end=49864656_JACKSON_LAB_2.fasta']
        targetRegion_dict['ALL'] = ['allTargetRegions.fasta']
    #####
    
    # Setting up the temp directory
    basePath = abspath(curdir)
    tmpDir   = generateTmpDirName()
    tmpPath  = join( basePath, tmpDir )
    stderr.write( '\n-Creating tmp directory: %s\n'%tmpDir )
    deleteDir(tmpPath)
    mkdir(tmpDir, 0777)
    testDirectory(tmpDir)
    
    # Concatenating the proper files together
    oh = open( '%s/tmpTargetRegions.fasta'%tmpPath, 'w' )
    for fileName in targetRegion_dict[specificTargetRegion]:
        print fileName
        for line in open( '%s/%s'%(targetRegionDir,fileName) ): oh.write(line)
    #####
    oh.close()
    cloneSizes = dict( [( record.id,len(record.seq) ) for record in iterFASTA(open('%s/tmpTargetRegions.fasta'%tmpPath))] )

    # Formatting the database    
    formatCommand = '/mnt/local/EXBIN/formatdb -l %s/formatdb.log -pF -i %s/tmpTargetRegions.fasta'%(tmpPath, tmpPath)
    
    # BLASTing
    blastOutputFile = '%s/cloneBlast.blastn.gz'%(tmpPath)
    cmdList = []
    cmdList.append( '/mnt/local/EXBIN/blastall -p blastn' )
    cmdList.append( '-d %s/tmpTargetRegions.fasta'%tmpPath )
    cmdList.append( '-i %s'%cloneFASTA )
    cmdList.append( '-r 1 -q -1 -G 1 -E 2 -e 0.01 -W 9 -F "m D" -U -m 8 | /usr/bin/gzip -c > %s'%(blastOutputFile) )
    cmd = ' '.join( cmdList )
    
    # Run the cases
    stderr.write( '\n-Performing the Cross Species BLAST\n')
    perlFile = 'cloneBLAST.pl'
    remoteServer4( [ formatCommand, cmd ], 1, '101', perlFile )
        
    # Parsing the information
    stderr.write( '-Parsing output...\n')
    cloneDict = {}
    for record in iterBLASTN( gzip.open( blastOutputFile ) ):
        if ( cloneDict.has_key(record['queryID']) ):
            try:
                cloneDict[record['queryID']][record['subjectID']].append( ( record['sStart'], record ) )
            except KeyError:
                cloneDict[record['queryID']][record['subjectID']] = [ ( record['sStart'], record ) ]
            #####
        else:
            cloneDict[record['queryID']] = {}
            cloneDict[record['queryID']][record['subjectID']] = [ ( record['sStart'], record ) ]
        #####
    #####
    
    # Function for testing overlap
    isOverlapped = lambda x, y: not ( (y[1]['qStart'] > x[1]['qEnd']) or (x[1]['qStart'] > y[1]['qEnd']) )
    alignLen     = lambda x: x[1]['qEnd'] - x[1]['qStart']
    
    # Computing all non-overlapping hits
    stderr.write( '\n-Scoring maximal non-overlapping alignments\n')
    queryKeys = cloneDict.keys()
    totalScores = {}
    for queryID in queryKeys:
        subjectKeys = cloneDict[queryID].keys()
        for subjectID in subjectKeys:
            # Pulling the alignment list
            hitList = [item for item in cloneDict[queryID][subjectID]]
            hitList.sort()
            nHitsOrig   = len(hitList)
            if ( nHitsOrig == 1 ): continue
            # Reducing the alignments to the longest set of non-overlapping alignments
            hitList.sort()
            while (True):
                popAlign = False
                # Look at all possible pairs of hits
                nHits = len(hitList)
                for n in xrange( nHits - 1 ):
                    for m in xrange( n+1, nHits ):
                        if ( isOverlapped( hitList[n], hitList[m]) ):
                            Ln = alignLen(hitList[n])
                            Sn = hitList[n][1]['bitScore']
                            Lm = alignLen(hitList[m])
                            Sm = hitList[m][1]['bitScore']
                            if ( Ln > Lm ):
                                popMe    = m
                                popAlign = True
                            elif ( Ln < Lm ):
                                popMe    = n
                                popAlign = True
                            elif ( Ln == Lm ):
                                # Check to see who scores higher
                                if ( Sn > Sm ):
                                    popMe    = m
                                    popAlign = True
                                elif (Sn < Sm ):
                                    popMe    = n
                                    popAlign = True
                                elif ( Sn == Sm ):
                                    # Just pick the nth one
                                    popMe    = n
                                    popAlign = True
                                #####
                            #####
                        #####
                        if ( popAlign ): break
                    #####
                    if ( popAlign ): break
                #####
                if ( not popAlign ): break
                # Something to do?
                if ( popAlign ):
                    hitList.pop(popMe)
                #####
            #####
            # Computing the scores for the alignment
            try:
                for tmpStart, record in hitList: totalScores[subjectID] += record['bitScore']
                #####
            except KeyError:
                totalScores[subjectID] = 0
                for tmpStart, record in hitList: totalScores[subjectID] += record['bitScore']
                #####
            #####
        #####
    #####
    
    # Printing out the cumulative scores for each region
    DSU = [ (value,key) for key, value in totalScores.iteritems()]
    DSU.sort()
    stdout.write( 'Target Region                                              Cumulative Score\n')
    stdout.write( '-------------                                              ----------------\n')
    for totalScore, subjectID in DSU:
        cts      = commify(totalScore)    
        nSpacing = ( 75 - len(subjectID) - len(cts) ) * ' '
        stdout.write( '%s%s%s\n'%(subjectID, nSpacing, cts) )
    #####
    
    # Taking out the trash
    deleteFile(perlFile)
    stderr.write( '\n-Removing Temporary directory\n\n')
    deleteDir(tmpPath)
    return

#     # Sorting all alignments
#     stdout.write( '%s\n'%blastHeader() )
#     allTopHits = []
#     for queryID, tmpDict in cloneDict.iteritems():
#         # Top hits
#         topHits = []        
#         for subjectID, hitList in tmpDict.iteritems():
#              hitList.sort(reverse=True)
#              topHits.append( hitList[0] )
#         #####
#         topHits.sort(reverse=True)
#         for item in topHits: allTopHits.append(item)
#     #####
#     
#     # Writing the sorted hits to the user
#     allTopHits.sort()
#     for tmpScore, record in allTopHits: stdout.write( strITEM( record, contigSizes, cloneSizes ) )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
