#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2013$"
#=========================================================================
# EDIT LOG
#    This is the working copy of merplot_gc.py in sharedPythonLibrary
#
#
#
#=========================================================================
import subprocess
from os import system
from optparse import OptionParser
from os.path import isfile
from hagsc_lib import isGzipFile, isBzipFile
#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    if ( isGzipFile(fileName) ): return "cat %s | gunzip -c  "%( fileName)
    if ( isBzipFile(fileName) ): return "cat %s | bunzip2 -c  "%( fileName)
    return "cat %s  "%( fileName)
#=========================================================================
def real_main():
    # Defining the program options
    usage = "usage: %prog [histHash] [output file] [options]"

    parser = OptionParser(usage)
    
    parser.add_option( "-u", \
                       "--upper", \
                       type    = 'float', \
                       help    = "Upper saturation bound. Default 1.5 standard deviations above the mean " ,\
                       default = False)

    
    parser.add_option( "-l", \
                       "--lower", \
                       type    = 'float', \
                       help    = "Lower saturation bound. Default 1.5 standard deviations below the mean" ,\
                       default = False)
                       
    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Not enough arguments given.  " + \
                      "View usage using --help option." )
    else:
        histFile = args[0]
        frontCMD = testFile( histFile )        
        totalCMD = "%s |awk '{if($5 != \"\")merTot += $1*$2}END{print merTot}'"%(frontCMD)
        base  = args[1]
        stdDev = False
        if(options.lower):
            if(options.lower < options.upper):
                lower = options.lower
                upper = options.upper
                stdDev = True
            else:
                parse.error("the lower bound must be smaller the the upper bound")
            #####
        #####
    #####
    totProcess = subprocess.Popen( totalCMD, shell=True, stdout=subprocess.PIPE )
    totalMer   = [int(line.split()[0]) for line in totProcess.stdout]
    totalMer   = totalMer[0]
    smCMD      = "%s |awk '{if($4 != \"\"){totMer = (($1*$2)/%.1f); gbar += $5*totMer; g2bar += ($5*$5)*totMer}}END{sigma = (gbar*gbar)-g2bar; if( sigma < 0 ){sigma = sigma * -1} sigma = 3*(sqrt(sigma)); print  sigma,gbar}'"%(frontCMD ,totalMer)
    smProc     =  subprocess.Popen( smCMD, shell=True, stdout=subprocess.PIPE )
    for line in smProc.stdout: 
        sigma , mean = line.split(None)
        sigma = float(sigma)
        mean  = float(mean)
    #####
    
    oh_plot = open('plot.gcHist.dat','w')
    oh_plot.write("reset\nunset key\nset terminal png enhanced\nset output \"%s\"\nset terminal png nocrop size 2000,1111 font 'Helvetica Bold,35'\n"%(base))
    oh_plot.write("set border linewidth 4\nset format y \"10^{%01T}\"\nset format x \"10^{%01T}\"\nset ytic\nset logscale y\nset logscale x\n")
    oh_plot.write("set palette defined ( 0 '#000090',1 '#000fff',2 '#0090ff',3 '#0fffee',4 '#90ff70',5 '#ffee00',6 '#ff7000',7 '#ee0000',8 '#7f0000')\n")
    oh_plot.write("set ylabel \"Fraction of Total Sequence\"\nset xlabel \"kmer Frequency\"\nset y2label \"Cumulative\" offset -2,0\n")
    oh_plot.write("set style fill solid 1.0\nset style line 5 lt rgb \"black\" lw 3 pt 0\nset yrange [.0000001:1]\nset y2range [0:1]\nset xrange [1:]\nset y2tics\n")
    oh_plot.write("set y2tics\nset ytics nomirror\nset grid x y2\n")
    if(stdDev):
        oh_plot.write("set cbrange[%f:%f]\n"%(lower, upper))
    else:
        lower = mean - sigma
        upper = mean + sigma
        oh_plot.write("set cbrange[%f:%f]\n"%(lower, upper))
    #####
    oh_plot.write("plot \"< awk 'BEGIN{last = 0; cume = 0}{current = $1;if(current != (last + 1)){for (i = last; i< current;i ++){print i,0,0,cume,0}}print $0; cume = $4;last = current}' %s\" using 1:($2*$1/%.1f):5 axis x1y1 with boxes palette ,  '%s' using 1:($4/100) axis x1y2 with linespoints ls  5"%(histFile, totalMer, histFile))
    oh_plot.close()
    system('gnuplot plot.gcHist.dat')
    system('rm -r plot.gcHist.dat')
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()
