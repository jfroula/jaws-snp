#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Nov 12, 2010 9:50:17 AM$"

# Local Library Imports
from hagsc_lib import blastn_bestHit

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

#===============================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [query fasta] [target fasta] [options]"

    parser = OptionParser(usage)

    parser.add_option( "-c", \
                       "--clusterToUse", \
                       type    = 'string', \
                       help    = "Cluster to use.  Default=101.", \
                       default = '101' )

    parser.add_option( "-o", \
                       "--outputFile", \
                       type    = 'string', \
                       help    = "Output file Name, Defalut=None", \
                       default = None )

    parser.add_option( "-p", \
                       "--numberOfCPUs", \
                       type    = 'int', \
                       help    = "Number of CPUs.  Default=10", \
                       default = 10 )

    parser.add_option( "-i", \
                       "--IDPercent", \
                       type    = "float", \
                       help    = "ID% cutoff.  Default: 85.0", \
                       default = 85.0 )
                       
    parser.add_option( "-v", \
                       "--covPercent", \
                       type    = 'float', \
                       help    = "Coverage% cutoff.  Default:  90.0", \
                       default = 90.0 )
    
    parser.add_option( "-e", \
                       "--explorationParams", \
                       action  = 'store_true', \
                       dest    = 'explorationParams', \
                       help    = "Use exploratory parameter set [1,-1] (Default is a mapping parameter set [1,-3])" )
    parser.set_defaults( explorationParams = False )

    parser.add_option( "-n", \
                       "--intermediateParams", \
                       action  = 'store_true', \
                       dest    = 'intermediateParams', \
                       help    = "Use intermediate parameter set [1,-2] (Default is a mapping parameter set [1,-3])" )
    parser.set_defaults( intermediateParams = False )

    parser.add_option( "-T", \
                       "--runTblastN", \
                       action  = 'store_true', \
                       dest    = 'runTblastN', \
                       help    = "Perform tblastn alignment (Default is blastn)" )
    parser.set_defaults( intermediateParams = False )
    
    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        queryFASTA = args[0]
        if ( not isfile(queryFASTA) ): parser.error( '%s can not be found'%queryFASTA )
        targetFASTA = args[1]
        if ( not isfile(targetFASTA) ): parser.error( '%s can not be found'%targetFASTA )
    #####

    # Extracting the param set type
    paramSetChoice = 'Mapping'
    if ( options.explorationParams ):
        paramSetChoice = 'Exploration'
    #####
    
    if ( options.intermediateParams ):
        paramSetChoice = 'Intermediate'
    #####
    
    # /mnt/local/EXBIN/blastall -p tblastn 
    #                           -d JOIN_3.fasta 
    #                           -i cacao_keithanne_genes.fasta 
    #                           -m 8 
    #                           -f 999 
    #                           -e 1e-2 > cacao_keithanne_genes.fasta.out &
    if ( options.runTblastN ):
        tmpKeyWords = {'program':'tblastn', \
                       'align_view':'8', \
                       'hit_extend':'500', \
                       'expectation':'1.0e-2'}
        paramSetChoice = 'tblastn'
    else:
        tmpKeyWords = None
    #####
    
    # Blastn call
    blastn_bestHit( queryFASTA, \
                    targetFASTA, \
                    nFastaFiles=options.numberOfCPUs, \
                    clusterNum=options.clusterToUse, \
                    cleanUp = True, \
                    outputFileName=options.outputFile, \
                    keyWords=tmpKeyWords, \
                    debugMode=False, \
                    tmpDirNum=None, \
                    cov_cutoff=options.covPercent, \
                    ID_cutoff=options.IDPercent, \
                    cutoffLength=0, \
                    paramSet=paramSetChoice )
    return
    
#####

def profile_main():
    from cProfile import Profile
    from pstats import Stats
    prof  = Profile().runctx("real_main()", globals(), locals())
    stats = Stats( prof ).sort_stats("time").print_stats(20)
    return
#####

if ( __name__ == '__main__' ):
    real_main()


