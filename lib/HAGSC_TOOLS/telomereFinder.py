#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott"
__date__ ="$June 26, 2013$"

from hagsc_lib import isGzipFile, isBzipFile, revComp
import subprocess
from os.path import isfile
from optparse import OptionParser
#==============================================================
def writeOut(x):
    oh.write('%s-R1\n%s-R2\n'%(x, x)) 

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE] [Telomer Seq] [% of Read to exclude]"

    parser = OptionParser(usage)

    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )

    else:
        # Setting up proper path name
        # Is the FASTA real
        if isfile(args[0]): 
            fastaFile = args[0]
        else:
            parser.error( 'Fasta File:  %s is not found.'% args[0])
        #####
        # Did the use give a file of mers
        telSeq    = args[1]
        telRev    = revComp(telSeq)
        telLen    = len(telSeq)
        subString = '%s|%s'%(telSeq, telRev)
        frac   = float(args[2])/100.00
    #####


    # Setting up the greatest awk command ever
    global oh
    awkCMD  = "awk 'BEGIN { RS = \">\" } ; {x = (gsub(/%s/, \"%s\", $2 ) )} {if ($2 == \"\") next} { if ((x*%d)/length($2) > %.2f ) print $1 } '"%(subString, telSeq, telLen, frac)
    readSet = set()
    oh      = open('telomericReadList.dat', 'w')
    
    if ( isGzipFile(fastaFile) ):
        telCMD = 'cat %s | gunzip -c | %s '%( fastaFile, awkCMD )
    elif ( isBzipFile(fastaFile) ):
        telCMD = 'cat %s | bunzip2 -c| %s '%( fastaFile, awkCMD )
    else:
        telCMD = 'cat %s| %s '%( fastaFile, awkCMD )
    #####
    n = 0
    tmpProcess = subprocess.Popen( telCMD, shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess.stdout: readSet.add(line.split('-R')[0])
    tmpProcess.kill()
    map(writeOut, readSet)

#==============================================================    
if ( __name__ == '__main__' ):
    real_main()

