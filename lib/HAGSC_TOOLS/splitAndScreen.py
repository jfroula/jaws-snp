#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Feb 25, 2010 11:05:17 AM$"

# Local Library Imports
from hagsc_lib import split_and_screen
from hagsc_lib import testFile

# Python Library Imports
from optparse import OptionParser

def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE] [OUTFILE BASENAME] [options]"

    parser = OptionParser(usage)

    defaultValue = -1
    parser.add_option( "-n", \
                       "--nRecords_per_File", \
                       type    = 'int', \
                       help    = "Number of records written per split " + \
                                 "file.\nDefault: Pull all reads.",
                       default = defaultValue )

    defaultValue = -1
    parser.add_option( "-L", \
                       "--minLength", \
                       type    = 'int', \
                       help    = "Minimum record length. " + \
                                 "\nDefault: No lower bound",
                       default = defaultValue )

    defaultValue = -1
    parser.add_option( "-U", \
                       "--maxLength", \
                       type    = 'int', \
                       help    = "Maximum record length. " + \
                                 "\nDefault: No upper bound",
                       default = defaultValue )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if ( not testFile(args[0]) ):
            parser.error( '%s can not be found'%args[0] )
        #####
        fasta_file_in    = args[0]
        base_outFileName = args[1]
        testFile(fasta_file_in)
    #####

    # Parsing the options
    nRecords_per_File = options.nRecords_per_File

    # Pulling the maximum and minimum lengths
    minLength = options.minLength
    maxLength = options.maxLength

    if ( (minLength>0) and (maxLength>0) and (minLength >= maxLength) ):
        parser.error( "Mininum length must be larger than the maximum." )
    #####

    # Pushing the information to an underlying function
    split_and_screen( fasta_file_in, \
                      base_outFileName, \
                      nRecords_per_File, \
                      minLength, \
                      maxLength )
    return

if ( __name__ == '__main__' ):
    real_main()


