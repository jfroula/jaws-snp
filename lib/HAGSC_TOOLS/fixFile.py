#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Sep 9, 2010 9:50:17 AM$"

# Local Library Imports
from hagsc_lib import fixFile
from hagsc_lib import testFile

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

def real_main():

    # Defining the program options
    usage = "usage: %prog [ANY FILE TYPE]"

    parser = OptionParser(usage)

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if ( not isfile(args[0]) ):
            parser.error( '%s can not be found'%args[0] )
        #####
        file_in    = args[0]
        testFile( file_in )
    #####

    # Pushing the information to an underlying function
    fixFile( file_in )

    return

if ( __name__ == '__main__' ):
    real_main()


