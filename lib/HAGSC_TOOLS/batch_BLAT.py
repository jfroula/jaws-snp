#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  6/4/15"

from optparse import OptionParser

from os.path import realpath, isfile

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import testFile, testDirectory
from hagsc_lib import iterFASTA, writeFASTA
from hagsc_lib import deleteDir, deleteFile
from hagsc_lib import generateTmpDirName
from hagsc_lib import equal_size_split
from hagsc_lib import remoteServer4
from hagsc_lib import baseFileName
from hagsc_lib import gzip_path
from hagsc_lib import getFiles

from os.path import join, abspath, curdir

from os import mkdir

from sys import stderr, stdout

import gzip

#==============================================================
class setup_base_class(object):

    def __init__(self, fastaFile, \
                       nFastaFiles, \
                       outputFileName=None ):

        # Make sure the files exist
        testFile(fastaFile)

        # Reading in the control variables
        self.fastaFile         = fastaFile
        self.nFastaFiles       = nFastaFiles

        # Generating the directory information
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                     baseFileName(self.fastaFile) )

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        equal_size_split( self.fastaFile, \
                          self.baseOutFileName, \
                          self.nFastaFiles, \
                          arachneReadFormat=False, \
                          noSort=True )
        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

#==============================================================
class main_run_class( setup_base_class ):
    def __init__(self, queryFile, \
                       targetFile, \
                       nFastaFiles, \
                       clusterNum, \
                       outputFileName, \
                       blatParameters, \
                       awkCommand ):

        ## Initializing the best hit base class
        setup_base_class.__init__( self, \
                                   queryFile, \
                                   nFastaFiles )
        
        # Reading in the information
        self.queryFile      = queryFile
        self.targetFile     = targetFile
        self.clusterNum     = clusterNum
        self.outputFileName = outputFileName
        self.blatParameters = blatParameters
        self.awkCommand     = awkCommand

    def executeCases(self):
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Break the fasta file into little pieces
        self.splitFASTA()
        # Step 4:  BLAT Fastas
        self.sendToCluster()
        # Step 5: Parse Output
        self.parseOutput()
        # Step 7: Cleanup
        self.clean_tmp()
        return

    def sendToCluster(self):
    
        stderr.write( '\t-BLATing Reads\n')

        # (1)  Set up the command list
        commandList = []
        frontCommand = '/mnt/local/EXBIN/blat %s'%self.blatParameters
        for fastaFile in getFiles( 'fasta', self.tmpPath ):
            testFile( fastaFile )
            localOutputFile = '%s.blat'%fastaFile
            if ( self.awkCommand == '' ):
                commandList.append( '%s %s %s stdout | gzip -c > %s.gz'%( frontCommand, self.targetFile, fastaFile, localOutputFile ) )
            else:
                commandList.append( r"%s %s %s stdout | awk \'{%s}\' | gzip -c > %s.gz"%( frontCommand, self.targetFile, fastaFile, self.awkCommand, localOutputFile ) )
            #####
        #####
        
        # (2)  Run the cases
        perlFile = 'blat_run.pl'
        remoteServer4( commandList, min(len(commandList),self.nFastaFiles), self.clusterNum, perlFile )
        deleteFile(perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        deleteFile(self.outputFileName)
        oh = open( self.outputFileName, 'w' )
        for gzipFile in getFiles( 'gz', self.tmpPath ):
            gzipHandle = gzip.open(gzipFile)
            map( oh.write, gzipHandle )
            gzipHandle.close()
        #####
        oh.close()
        return

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [query fasta] [target fasta] [output file] [options]"

    parser = OptionParser(usage)

    blatParameters = "-noHead -extendThroughN"
    parser.add_option( '-p', \
                       "--blatParameters", \
                       type    = "str", \
                       help    = "BLAT parameters.  Default: %s"%blatParameters, \
                       default = blatParameters )

    awkCommand = r"x=(\$11-\$1)+\$2+\$6+\$8;if(x==0)print"
    parser.add_option( '-a', \
                       "--awkCommand", \
                       type    = "str", \
                       help    = "Trailing awk command parameters.  Default: %s"%awkCommand, \
                       default = awkCommand )

    clusterNum = "ssd"
    parser.add_option( '-c', \
                       "--clusterNum", \
                       type    = "str", \
                       help    = "Cluster number.  Default: %s"%clusterNum, \
                       default = clusterNum )

    nJobs = 20
    parser.add_option( '-n', \
                       "--nJobs", \
                       type    = "int", \
                       help    = "Number of jobs on the cluster.  Default: %d"%nJobs, \
                       default = nJobs )
                       
    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        queryFASTA = realpath(args[0])
        if ( not isfile(queryFASTA) ): parser.error( '%s can not be found'%queryFASTA )

        targetFASTA = realpath(args[1])
        if ( not isfile(targetFASTA) ): parser.error( '%s can not be found'%targetFASTA )

        outputFileName = realpath(args[2])
    #####
    
    # Controlling parameters
    main_run_class( queryFASTA, \
                    targetFASTA, \
                    options.nJobs, \
                    options.clusterNum, \
                    outputFileName, \
                    options.blatParameters, \
                    options.awkCommand ).executeCases()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
