#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Apr 22, 2014$"
#=========================================================================
# EDIT LOG
#
#
#
#
#=========================================================================
from time import time
from sys import  stderr
from os import getpid, uname, system
import subprocess
from optparse import OptionParser
from os.path import isfile
import re
#=========================================================================
commify_re  = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True
#=========================================================================
def StoI(line, rawInfoDict):
    try:
        newLine = line.split(":")
        val = float(''.join(newLine[1].split(",")))
    except ValueError:
        return 
    #####
    try:
        rawInfoDict[newLine[0]] += val
    except KeyError:
        pass
    #####
    return
#=========================================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )
#========================================================================= 
def real_main():
    # Defining the program options
    usage = "usage: %prog [Stats File(s)][PhredHist File(s)][Out Base][options]"

    parser = OptionParser(usage)

    parser.add_option( '-p', \
                       "--phixGraph", \
                       action  = "store_true", \
                       dest    = 'phixGraph', \
                       help    = "Produce a plot set for just phix data.  Default:  Produce plots for genome data." )
    parser.set_defaults( qualOnly = False )
    
    parser.add_option( '-f', \
                       "--listFile", \
                       action  = "store_true", \
                       dest    = 'listFile', \
                       help    = "Stats File and PhredHist arguments are files containing a list of files to use.  Default:  Arguments are lists and not files containing lists." )
    parser.set_defaults( seqOnly = False )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        if(options.listFile):
            # Read in list files
            testFile(args[0])
            statsFileList = []
            for line in open(args[0]):
                fileName = line.split(None)[0]
                testFile(fileName)
                statsFileList.append(fileName)
            #####
            
            testFile(args[1])
            phredFileList = []
            for line in open(args[1]):
                fileName = line.split(None)[0]
                testFile(fileName)
                phredFileList.append(fileName)
            #####
        else:
            # Making the file list from arguments
            statsFileList = args[0].split(',')
            for item in statsFileList: testFile(item)
        
            phredFileList = args[1].split(',')
            for item in phredFileList: testFile(item)
        #####
        
        baseName  = args[2]
        tmpPID    = [None, getpid()][bool(True)]
        identfier = r'%s.%s'%( uname()[1], tmpPID )
    #####
    # Read in the Data
    # Will need to keep track of all data entries
    statsDict   = {1:{},2:{}}
    readLength  = 0
    read2Exists = False
    for item in statsFileList:
    
        rVal    = 1
        tmpList = item.split("_R2")
        if (len(tmpList)>1):
            read2Exists = True
            rVal        = 2
        #####
        
        for line in open(item):
            num, qual, A_s, T_s, G_s, C_s, N_s, totBP, totQual, qual_phix, A_s_phix, T_s_phix, G_s_phix, C_s_phix ,N_s_phix ,totBP_phix, totQual_phix = line.split(None)
            if(num == "#"):continue
            num        = int(num)
            readLength = max(readLength,num)
            try:
                statsDict[rVal][num][0]  += int(qual)
                statsDict[rVal][num][1]  += int(A_s)
                statsDict[rVal][num][2]  += int(T_s)
                statsDict[rVal][num][3]  += int(G_s)
                statsDict[rVal][num][4]  += int(C_s)
                statsDict[rVal][num][5]  += int(N_s)
                statsDict[rVal][num][6]  += int(totBP)
                statsDict[rVal][num][7]  += float(totQual)
                statsDict[rVal][num][8]  += int(qual_phix)
                statsDict[rVal][num][9]  += int(A_s_phix)
                statsDict[rVal][num][10]  += int(T_s_phix)
                statsDict[rVal][num][11] += int(G_s_phix)
                statsDict[rVal][num][12] += int(C_s_phix)
                statsDict[rVal][num][13] += int(N_s_phix)
                statsDict[rVal][num][14] += int(totBP_phix)
                statsDict[rVal][num][15] += float(totQual_phix)
            except:
                statsDict[rVal][num] = [int(qual), int(A_s), int(T_s), int(G_s), int(C_s), int(N_s), int(totBP), float(totQual), int(qual_phix), int(A_s_phix), int(T_s_phix), int(G_s_phix), int(C_s_phix), int(N_s_phix), int(totBP_phix),float(totQual_phix)]
            #####
        #####
    #####
    phredOutString = []
    badReads       = 0
    goodReads      = 0
    totReads       = 0
    totBases       = 0
    totHQBases     = 0
    avgWOF         = 0.0
    rawInfoDict    = {'Number of reads':totReads,'Total bases':totBases,'Total Phred20 bases':totHQBases,'Phred Average without failures':avgWOF}
    binDict        = {}
    for item in phredFileList:
        phredOutString.append('\n\n%s\n'%item)
        for line in open(item):
            splitLine = line.split(None)
            phredOutString.append(line)
            if(len(splitLine) == 0): continue
            if((splitLine[0] == "Phred20") or (splitLine[0] == "-------")):continue
            try:
                barLocation = splitLine[3][0]
                if(len(splitLine[0])<4): barLocation = splitLine[4][0]

                if(barLocation == "|"):
                    
                    nReads = int(splitLine[1])
                    
                    binNum = int(splitLine[0].split("-")[0])
                    
                    if(binNum<100): nReads = int(splitLine[2])
                    if(binNum < 4):
                        badReads += nReads
                    else:
                        goodReads += nReads
                    #####
                    try:
                        binDict[binNum] += nReads
                    except KeyError:
                        binDict[binNum] = nReads
                    #####
                else:
                    StoI(line,rawInfoDict)
                #####
            except IndexError:
                StoI(line,rawInfoDict)
            #####
        #####
    #####
    
    rawInfoDict['Phred Average without failures'] = rawInfoDict['Phred Average without failures']/len(phredFileList)
    keyList     = list(binDict.keys())
    phredOutString.append('\n\nCombined\nPhred20       Reads %ofReads\n-------   --------- --------\n')
    keyList.sort()
       
    sf  = "{:3}-{:3} {:12}  {:>5}%  |{:50}\n"
    for key in keyList:        
        binGNum = binDict[key]
        perGNum = float(100.0*binGNum)/float(rawInfoDict['Number of reads'])
        gXNum   = int(perGNum/2)
        
        if(int(float('%.1f'%perGNum))%2 == 1): gXNum += 1
        phredOutString.append(sf.format(key,(key+9),binGNum,'%.1f'%perGNum,'X'*gXNum))
        #phredOutString.append("%d-%d\t%d\t    %.1f%%\t|%s\n"%(key,(key+9),binGNum,perGNum,'X'*gXNum))
    #####
    genomeTotal = int(rawInfoDict['Number of reads'])
    phredOutString.append("\nNumber of reads: %s\nTotal bases: %s\nTotal Phred20 bases: %s\n"%(commify(int(genomeTotal)),commify(int(genomeTotal)),commify(int(rawInfoDict['Total Phred20 bases']))))
    phredOutString.append("Average length: %d\nPhred Average: %.1f\n"%(readLength,(float(rawInfoDict['Total Phred20 bases'])/float(genomeTotal))))
    phredOutString.append("Phred Average without failures: %.1f\nPercent failed: %.1f%%\n"%(float(rawInfoDict['Phred Average without failures']),float(badReads*100.0)/float(genomeTotal)))

    combined = open('%s.combined.stats'%(baseName),'w')
    combined.write(''.join(phredOutString))
    combined.close()
        
    qualPlot = ''
    seqPlot  = ''
    
    keyList = list(statsDict[1].keys())
    keyList.sort()
    
    plot1    = "reset\nset key outside\nset terminal pngcairo enhanced\nset output '%s.stats.png'\n"%(baseName)
    
    if (options.phixGraph):
        seq_oh_1  = open('seq_1.%s.dat'%(identfier),'w')
        qual_oh_1 = open('qual_1.%s.dat'%(identfier),'w')
        for key in keyList:
            value = statsDict[1][key]
            aScore = float(value[9])/float(value[14])
            tScore = float(value[10])/float(value[14])
            gScore = float(value[11])/float(value[14])
            cScore = float(value[12])/float(value[14])
            nScore = float(value[13])/float(value[14])
            seq_oh_1.write('%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n'%(key,aScore,tScore,gScore,cScore,nScore))
            qual_oh_1.write('%s\t%.2f\n'%(key,(float(value[7])/float(value[13])) ))  
        #####
        
        qual_oh_1.close()
        seq_oh_1.close()
        
        plot2    = "set terminal png nocrop size 2000,1111 font 'Helvetica,18'\nset multiplot layout 2,1 title '%s'\n"%(baseName.split("/")[-1])
        plot6    = "set ylabel 'Average Quality_R1'\nset xlabel 'Base Position' offset 0,.5\n"
        qualPlot = "plot 'qual_1.%s.dat' using 1:3 title 'Avg_phix' with lines ls 1, 30 with lines ls 3"%(identfier)
        plot7    = "\nset yrange[0:.6]\nset ylabel 'Sequence Complexity'\nset xlabel 'Base Position' offset 0,.5\n"
        seqPlot  = "plot 'seq_1.%s.dat' using 1:2 title 'A' with lines ls 5,'seq_1.%s.dat' using 1:3 title 'T' with lines ls 2,'seq_1.%s.dat' using 1:4 title 'G' with lines ls 3,'seq_1.%s.dat' using 1:5 title 'C' with lines ls 4,'seq_1.%s.dat' using 1:6 title 'N' with lines ls 1"%(identfier,identfier,\
                                                                                                                                                                                                                                                                                          identfier,identfier.\
                                                                                                                                                                                                                                                                                          identfier)
        
        if(read2Exists):
            seq_oh_2  = open('seq_2.%s.dat'%(identfier),'w')
            qual_oh_2 = open('qual_2.%s.dat'%(identfier),'w')
            for key in keyList:
                value = statsDict[2][key]
                aScore = float(value[9])/float(value[14])
                tScore = float(value[10])/float(value[14])
                gScore = float(value[11])/float(value[14])
                cScore = float(value[12])/float(value[14])
                nScore = float(value[13])/float(value[14])
                seq_oh_2.write('%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n'%(key,aScore,tScore,gScore,cScore,nScore))
                qual_oh_2.write('%s\t%.2f\n'%(key,(float(value[7])/float(value[13])) ))  
            #####
            qual_oh_2.close()
            seq_oh_2.close()
        
            plot2    = "set terminal png nocrop size 2000,1111 font 'Helvetica,18'\nset multiplot layout 2,2 title '%s'\n"%(baseName.split("/")[-1])
            plot6    = "set ylabel 'Average Quality_R1'\nset xlabel 'Base Position' offset 0,.5\nplot 'qual_1.%s.dat' using 1:3 title 'Avg_phix' with lines ls 1\n"%(identfier)
            qualPlot = "set ylabel 'Average Quality_R2'\nset xlabel 'Base Position' offset 0,.5\nplot 'qual_2.%s.dat' using 1:3 title 'Avg_phix' with lines ls 1\n"%(identfier)
            plot7    = "set yrange[0:.6]\nset ylabel 'Sequence Complexity_R1'\nset xlabel 'Base Position' offset 0,.5\nplot 'seq_1.%s.dat' using 1:2 title 'A' with lines ls 5,'seq_1.%s.dat' using 1:3 title 'T' with lines ls 2,'seq_1.%s.dat' using 1:4 title 'G' with lines ls 3,'seq_1.%s.dat' using 1:5 title 'C' with lines ls 4,'seq_1.%s.dat' using 1:6 title 'N' with lines ls 1\n"%(identfier,identfier,identfier,identfier,identfier)
            seqPlot  = "set yrange[0:.6]\nset ylabel 'Sequence Complexity_R2'\nset xlabel 'Base Position' offset 0,.5\nplot 'seq_2.%s.dat' using 1:2 title 'A' with lines ls 5,'seq_2.%s.dat' using 1:3 title 'T' with lines ls 2,'seq_2.%s.dat' using 1:4 title 'G' with lines ls 3,'seq_2.%s.dat' using 1:5 title 'C' with lines ls 4,'seq_2.%s.dat' using 1:6 title 'N' with lines ls 1"%(identfier,identfier,identfier,identfier,identfier) 
        
    else:
        seq_oh_1  = open('seq_1.%s.dat'%(identfier),'w')
        qual_oh_1 = open('qual_1.%s.dat'%(identfier),'w')
        for key in keyList:
            value = statsDict[1][key]
            if (float(value[6]) == 0.0):
                aScore = 0.0
                tScore = 0.0
                gScore = 0.0
                cScore = 0.0
                nScore = 0.0

            else:
                aScore = float(value[1])/float(value[6])
                tScore = float(value[2])/float(value[6])
                gScore = float(value[3])/float(value[6])
                cScore = float(value[4])/float(value[6])
                nScore = float(value[5])/float(value[6])
            #####
            seq_oh_1.write('%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n'%(key,aScore,tScore,gScore,cScore,nScore))
            if(float(value[15]) >= 1):    
                qual_oh_1.write('%s\t%.2f\t%.2f\n'%(key,(float(value[0])/float(value[7])),(float(value[8])/float(value[15])) ))  
            else:
                qual_oh_1.write('%s\t%.2f\t%.2f\n'%(key,(float(value[0])/float(value[7])),0.0 ))
            #####
        #####
        
        qual_oh_1.close()
        seq_oh_1.close()
        
        plot2    = "set terminal png nocrop size 2000,1111 font 'Helvetica,18'\nset multiplot layout 2,1 title '%s'\n"%(baseName.split("/")[-1])
        plot6    = "set ylabel 'Average Quality_R1'\nset xlabel 'Base Position' offset 0,.5\n"
        qualPlot = "plot 'qual_1.%s.dat' using 1:2 title 'Avg' with lines ls 2,'qual_1.%s.dat' using 1:3 title 'Avg_phix' with lines ls 1, 30 with lines ls 3"%(identfier,identfier)
        plot7    = "\nset yrange[0:.6]\nset ylabel 'Sequence Complexity'\nset xlabel 'Base Position' offset 0,.5\n"
        seqPlot  = "plot 'seq_1.%s.dat' using 1:2 title 'A' with lines ls 5,'seq_1.%s.dat' using 1:3 title 'T' with lines ls 2,'seq_1.%s.dat' using 1:4 title 'G' with lines ls 3,'seq_1.%s.dat' using 1:5 title 'C' with lines ls 4,'seq_1.%s.dat' using 1:6 title 'N' with lines ls 1"%(identfier,identfier,\
                                                                                                                                                                                                                                                                                          identfier,identfier,\
                                                                                                                                                                                                                                                                                          identfier)
        
        if(read2Exists):
            seq_oh_2  = open('seq_2.%s.dat'%(identfier),'w')
            qual_oh_2 = open('qual_2.%s.dat'%(identfier),'w')
            for key in keyList:
                value = statsDict[2][key]
                if (float(value[6]) == 0.0):
                    aScore = 0.0
                    tScore = 0.0
                    gScore = 0.0
                    cScore = 0.0
                    nScore = 0.0
    
                else:
                    aScore = float(value[1])/float(value[6])
                    tScore = float(value[2])/float(value[6])
                    gScore = float(value[3])/float(value[6])
                    cScore = float(value[4])/float(value[6])
                    nScore = float(value[5])/float(value[6])
                #####

                seq_oh_2.write('%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n'%(key,aScore,tScore,gScore,cScore,nScore))
                if(float(value[15]) >= 1):
                    qual_oh_2.write('%s\t%.2f\t%.2f\n'%(key,(float(value[0])/float(value[7])),(float(value[8])/float(value[15])) )) 
                else:
                    qual_oh_2.write('%s\t%.2f\t%.2f\n'%(key,(float(value[0])/float(value[7])),0.0 ))
                #####
            #####
            qual_oh_2.close()
            seq_oh_2.close()
            
            plot2    = "set terminal png nocrop size 2000,1111 font 'Helvetica,18'\nset multiplot layout 2,2 title '%s'\n"%(baseName.split("/")[-1])
            plot6    = "set ylabel 'Average Quality_R1'\nset xlabel 'Base Position' offset 0,.5\nplot 'qual_1.%s.dat' using 1:2 title 'Avg' with lines ls 2,'qual_1.%s.dat' using 1:3 title 'Avg_phix' with lines ls 1, 30 with lines ls 3\n"%(identfier,identfier)
            qualPlot = "set ylabel 'Average Quality_R2'\nset xlabel 'Base Position' offset 0,.5\nplot 'qual_2.%s.dat' using 1:2 title 'Avg' with lines ls 2,'qual_2.%s.dat' using 1:3 title 'Avg_phix' with lines ls 1, 30 with lines ls 3\n"%(identfier,identfier)
            plot7    = "set yrange[0:.6]\nset ylabel 'Sequence Complexity_R1'\nset xlabel 'Base Position' offset 0,.5\nplot 'seq_1.%s.dat' using 1:2 title 'A' with lines ls 5,'seq_1.%s.dat' using 1:3 title 'T' with lines ls 2,'seq_1.%s.dat' using 1:4 title 'G' with lines ls 3,'seq_1.%s.dat' using 1:5 title 'C' with lines ls 4,'seq_1.%s.dat' using 1:6 title 'N' with lines ls 1\n"%(identfier,identfier,identfier,identfier,identfier)
            seqPlot  = "set yrange[0:.6]\nset ylabel 'Sequence Complexity_R2'\nset xlabel 'Base Position' offset 0,.5\nplot 'seq_2.%s.dat' using 1:2 title 'A' with lines ls 5,'seq_2.%s.dat' using 1:3 title 'T' with lines ls 2,'seq_2.%s.dat' using 1:4 title 'G' with lines ls 3,'seq_2.%s.dat' using 1:5 title 'C' with lines ls 4,'seq_2.%s.dat' using 1:6 title 'N' with lines ls 1"%(identfier,identfier,identfier,identfier,identfier) 
        #####
    #####

    
    plot3    = "set style line 1 lw 2\nset style line 2 lw 2\nset style line 3 lw 2\nset style line 4 lw 2\nset style line 5 lw 2\n"
    plot4    = "set border linewidth 2\nset ytic\nset yrange[0:45]\nset xtics 0,%d,%d\nset xrange[0:%d]\n"%(int(readLength/10),readLength,readLength)
    plot5    = "set grid x\nset tmargin 1\nset bmargin 3\nset lmargin 8\nset rmargin 16\n"
    
    plotLine = "%s%s%s%s%s%s%s%s%s"%(plot1,plot2,plot3,plot4,plot5,plot6,qualPlot,plot7,seqPlot)  
    
    plot_oh = open('plot.%s.dat'%(identfier),'w')
    plot_oh.write(plotLine)
    plot_oh.close()
    system('gnuplot plot.%s.dat'%(identfier))
    system('rm -f plot.%s.dat seq_*.%s.dat qual_*.%s.dat'%(identfier,identfier,identfier))
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()