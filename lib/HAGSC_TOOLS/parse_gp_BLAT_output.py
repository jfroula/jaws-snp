__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  6/29/16"

from hagsc_lib import deleteFile, getFiles, deleteDir

from sys import stderr, argv

import subprocess

#==============================================================
def real_main():

    tmpPath        = argv[1]
    outputFileName = argv[2]

    stderr.write( '\t-Parsing output...\n')
    # Looping over gzip files
    deleteFile(outputFileName)
    oh = open( outputFileName, 'w' )
    for gzipFile in getFiles( 'gz', tmpPath ):
        p = subprocess.Popen( 'cat %s | gunzip -c'%gzipFile, shell=True, stdout=subprocess.PIPE )
        map( oh.write, p.stdout )
        p.poll()
    #####
    oh.close()

#     deleteDir( tmpPath )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
