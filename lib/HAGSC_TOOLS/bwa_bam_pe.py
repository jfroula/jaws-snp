#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import testDirectory, isGzipFile
from hagsc_lib import iterFASTA, writeFASTA
from hagsc_lib import deleteDir, deleteFile
from hagsc_lib import generateTmpDirName
from hagsc_lib import equal_read_split
from hagsc_lib import remoteServer4
from hagsc_lib import baseFileName
from hagsc_lib import iterCounter
from hagsc_lib import gzip_path
from hagsc_lib import getFiles

from os.path import join, abspath, curdir, realpath, isfile

from os import mkdir, system, chdir

from subprocess import Popen
from subprocess import PIPE

from sys import stderr, stdout, argv

from optparse import OptionParser

import gzip

#==============================================================
def testFile(fileName):
    if ( not isfile( fileName ) ):
        raise IOError( 'File not found: %s'%fileName )
    #####
    return True

#==============================================================
class setup_base_class(object):

    def __init__(self, fileDict, optionsDict):

        # Make sure the files exist
        for key, value in fileDict.iteritems():
            if ( key != 'outputBase' ): testFile(value)
        #####

        # Reading in the control variables
        self.fileDict    = fileDict
        self.optionsDict = optionsDict
        self.targetIndex = 'target_fasta_index'

        # Generating the directory information
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, self.fileDict['outputBase'] )

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA_and_indexTarget(self):

        stderr.write( '\t-Splitting FASTA and QUAL files\n')
        
        pairID = 'R1'

        #==================================================
        # Computing the target number of records for a file
        readsFASTA   = self.fileDict['%s_FASTA'%pairID]
        gzippedFASTA = False
        if ( isGzipFile(readsFASTA) ):
            cmd = r"cat %s | gunzip -c | grep -c '>'"%readsFASTA
            gzippedFASTA = True
        else:
            cmd = r"grep -c '>' %s"%readsFASTA
        #####
        stderr.write( 'Executing: %s\n'%cmd )
        tmpProcess = Popen( cmd, shell=True, stdout=PIPE )
        nReads     = int( [item.strip() for item in tmpProcess.stdout][0] )
        tmpProcess.kill()
        targetRecords = int( float(nReads) / float(self.optionsDict['nFastaFiles']) ) + 1
        stderr.write( 'Total Reads: %d\n'%targetRecords )

        #=================================================
        # Partitioning the read names using the FASTA file
        fileCounter   = 1
        recordCounter = 0
        tmpReadsFile  = open( join( self.tmpPath, 'tmpReads_%s_%d.dat'%(pairID,fileCounter) ), 'w' )
        if ( gzippedFASTA ):
            in_FASTA = gzip.open( readsFASTA, 'r' )
        else:
            in_FASTA = open( readsFASTA, 'r' )
        #####
        while ( True ):
            line = in_FASTA.readline()
            if ( not line ): break
            if ( line[0] != '>' ): continue
            # Writing the read name
            tmpReadsFile.write( '%s\n'%( line[1:].split(None)[0] ) )
            recordCounter += 1
            # Checking the number of reads
            if ( recordCounter >= targetRecords ):
                tmpReadsFile.close()
                stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                              fileCounter, recordCounter, targetRecords) )
                fileCounter  += 1
                tmpReadsFile  = open( join( self.tmpPath, 'tmpReads_%s_%d.dat'%(pairID,fileCounter) ), 'w' )
                recordCounter = 0
            #####
        #####
        in_FASTA.close()
        tmpReadsFile.close()
        stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                                    fileCounter, recordCounter, targetRecords) )

        #==========================================
        # Extract seq and qual from the reads files
        cmd = [ '/mnt/local/EXBIN/extract_seq_and_qual.test -S .%s.fasta'%pairID ]
        for n in xrange(1, fileCounter + 1):
            tmpReadsFile  = join( self.tmpPath, 'tmpReads_%s_%d.dat'%(pairID,n) )
            cmd.append( '-i %s'%tmpReadsFile )
        #####
        cmd.append( '%s'%self.fileDict['%s_FASTA'%pairID] )
        cmdStr = ' '.join( cmd )
        print cmdStr
        system( cmdStr )

        #==========================================
        # Extracting the R2 reads, NEED TO KEEP THE READ NAME ORDER THE EXACT SAME BETWEEN R1 AND R2
        pairID = 'R2'
        cmd    = [ '/mnt/local/EXBIN/extract_seq_and_qual.test -S .%s.fasta'%pairID ]
        for n in xrange(1, fileCounter + 1):
            tmpR1File = join( self.tmpPath, 'tmpReads_R1_%d.dat'%n )
            tmpR2File = join( self.tmpPath, 'tmpReads_R2_%d.dat'%n )
            system( 'cp -f %s %s'%(tmpR1File,tmpR2File) )
            system( 'sed -i "s/\/1/\/2/g" %s'%tmpR2File )
            cmd.append( '-i %s'%tmpR2File )
        #####
        cmd.append( '%s'%self.fileDict['%s_FASTA'%pairID] )
        cmdStr = ' '.join( cmd )
        print cmdStr
        system( cmdStr )
        
        #=================================================================
        # Index Target File
        chdir(self.tmpPath)
        system( '/mnt/local/EXBIN/bwa index -p %s -a bwtsw %s'%(self.targetIndex,self.fileDict['targetFASTA']) )
        chdir(self.basePath)
        
        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

#==============================================================
class main_run_class( setup_base_class ):
    def __init__(self, fileDict, \
                       optionsDict ):

        ## Initializing the best hit base class
        setup_base_class.__init__( self, fileDict, optionsDict )

        # Reading in the information
        self.fileDict    = fileDict
        self.optionsDict = optionsDict

    def executeCases(self):
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Break the fasta/qual file into little pieces, and index the target FASTA
        self.splitFASTA_and_indexTarget()
        # Step 3:  Create the sorted bam files
        self.sendToCluster()
        # Step 4: Parse Output
        self.parseOutput()
        # Step 5: Cleanup
        self.clean_tmp()
        return

    def sendToCluster(self):
        stderr.write( '\t-Aligning Reads\n')
        # (1)  Set up the command list
        commandList = []
        for n in xrange( 1, self.optionsDict['nFastaFiles'] + 1 ):
            tmpR1_seq  = join( self.tmpPath, 'tmpReads_R1_%d.dat.R1.fasta'%n )
            tmpR2_seq  = join( self.tmpPath, 'tmpReads_R2_%d.dat.R2.fasta'%n )
            tarIdx = join( self.tmpPath, self.targetIndex )
            bamOut = join( self.tmpPath, 'sorted_%d'%n )
            
            tmpR1_sai  = join( self.tmpPath, 'tmpReads_R1_%d.sai'%n )
            tmpR2_sai  = join( self.tmpPath, 'tmpReads_R2_%d.sai'%n )
            tmp_sam    = join( self.tmpPath, 'tmpReads_%d.sam'%n )
            c1 = '/mnt/local/EXBIN/bwa aln -t 3 %s %s > %s'%(tarIdx,tmpR1_seq,tmpR1_sai)
            c2 = '/mnt/local/EXBIN/bwa aln -t 3 %s %s > %s'%(tarIdx,tmpR2_seq,tmpR2_sai)
            # SAMPE OPTIONS
#            Options: -a INT   maximum insert size [500]
#                     -o INT   maximum occurrences for one end [100000]
#                     -n INT   maximum hits to output for paired reads [3]
#                     -N INT   maximum hits to output for discordant pairs [10]
#                     -c FLOAT prior of chimeric rate (lower bound) [1.0e-05]
#                     -f FILE  sam file to output results to [stdout]
#                     -r STR   read group header line such as `@RG\tID:foo\tSM:bar' [null]
#                     -P       preload index into memory (for base-space reads only)
#                     -s       disable Smith-Waterman for the unmapped mate
#                     -A       disable insert size estimate (force -s)
            c3 = '/mnt/local/EXBIN/bwa sampe -f %s -o 1000 -a 2000 -P -A -s %s %s %s %s %s'%(tmp_sam,tarIdx,tmpR1_sai,tmpR2_sai,tmpR1_seq,tmpR2_seq)
            c4 = 'rm -f %s %s'%(tmpR1_sai,tmpR2_sai)
            
            tmp_bam    = join( self.tmpPath, 'tmpReads_%d.bam'%n )
            c5 = '/opt/samtools-0.1.18/samtools view -Shb -o %s %s'%(tmp_bam,tmp_sam)
            c6 = 'rm -f %s'%(tmp_sam)
            sortedBase = join( self.tmpPath, 'sorted_%d'%n )
            c7 = '/opt/samtools-0.1.18/samtools sort %s %s'%(tmp_bam,sortedBase)
            c8 = 'rm -f %s'%(tmp_bam)
            sorted_bam  = join( self.tmpPath, 'sorted_%d.bam'%n )
            c9 = '/opt/samtools-0.1.18/samtools index %s'%sorted_bam
            commandList.append( ' ; '.join([c1,c2,c3,c4,c5,c6,c7,c8,c9]) )
        #####
        
        # (2)  Run Commands
        perlFile = 'BAM_BAM.pl'
        remoteServer4( commandList, self.optionsDict['nFastaFiles'], self.optionsDict['clusterNum'], perlFile )
        deleteFile(perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Merging Sorted BAM Files\n')
        bamOutputFile = '%s.sorted.bam'%(self.optionsDict['outputBase']) 
        cmd = [ '/opt/samtools-0.1.18/samtools merge -f %s'%bamOutputFile  ]
        for n in xrange(1, self.optionsDict['nFastaFiles'] + 1):
            tmpBamFile  = join( self.tmpPath, 'sorted_%d.bam'%(n) )
            cmd.append( '%s'%tmpBamFile )
        #####
        cmdStr = ' '.join( cmd )
        system( cmdStr )
        system( '/opt/samtools-0.1.18/samtools index %s'%bamOutputFile )
        return

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [R1 fasta] [R1 qual] [R2 fasta] [R2 qual] [target fasta] [output Base] [options]"

    parser = OptionParser(usage)

    defVal = 'ssdtest'
    parser.add_option( "-c", \
                       "--clusterToUse", \
                       type    = 'string', \
                       help    = "Cluster to use.  Default=%s."%defVal, \
                       default = '%s'%defVal )

    defVal = 5
    parser.add_option( "-p", \
                       "--numberOfCPUs", \
                       type    = 'int', \
                       help    = "Number of CPUs.  Default=%d"%defVal, \
                       default = defVal )

    numMismatchesAllowed = -1
    parser.add_option( '-n', \
                       "--numMismatchesAllowed", \
                       type    = "int", \
                       help    = "Number of query mismatches allowed (does not include gaps!).  Default: bwa default", \
                       default = numMismatchesAllowed )
                       
    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 6 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        R1_FASTA = realpath(args[0])
        testFile( R1_FASTA )

        R1_QUAL = realpath(args[1])
        testFile( R1_QUAL )

        R2_FASTA = realpath(args[2])
        testFile( R2_FASTA )

        R2_QUAL = realpath(args[3])
        testFile( R2_QUAL )

        targetFASTA = realpath(args[4])
        testFile(targetFASTA)

        outputBase = realpath(args[5])
    #####
    
    # Pulling the options
    nFastaFiles = options.numberOfCPUs
    clusterNum  = options.clusterToUse

    # Pulling additional options    
    optionsDict = {'numMismatchesAllowed':options.numMismatchesAllowed, \
                   'nFastaFiles':nFastaFiles, \
                   'clusterNum':clusterNum, \
                   'outputBase':outputBase}
    
    fileDict = {'R1_FASTA':R1_FASTA, \
                'R1_QUAL':R1_QUAL, \
                'R2_FASTA':R2_FASTA, \
                'R2_QUAL':R2_QUAL, \
                'targetFASTA':targetFASTA, \
                'outputBase':outputBase }

    # Running the cluster code
    main_run_class( fileDict, optionsDict ).executeCases()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
