#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="7-March-2011"

from hagsc_lib import countBasePairs
from hagsc_lib import iterFASTA
from hagsc_lib import writeFASTA
from hagsc_lib import SeqRecord
from hagsc_lib import baseFileName
from hagsc_lib import FASTAFile_dict

from os.path import isfile
from os.path import abspath

from optparse import OptionParser

import re

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [reads file] [FASTA file] [options]"

    parser = OptionParser(usage)

    baseIncrement = 7
    parser.add_option( "-i", \
                       "--baseSizeIncrement", \
                       type    = 'int', \
                       help    = "Discretization size(KB).  Default=%d KB"%baseIncrement, \
                       default = baseIncrement )

    outputFileName = ''
    parser.add_option( "-o", \
                       "--outputFileName", \
                       type    = 'string', \
                       help    = "OutputFileName.  Default= readsFile_prepedForScreening.fasta", \
                       default = outputFileName )

    nIncrements = 5
    parser.add_option( "-n", \
                       "--numIncrements", \
                       type    = 'int', \
                       help    = "Number of increments to use.  Default=%d"%nIncrements, \
                       default = nIncrements )
                       
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
    
        # Pulling the reads file
        tmpFile = abspath(args[0])
        if ( isfile(tmpFile) ):
            readsFile = abspath(args[0])
        else:
            parser.error( 'Reads File:  %s is not found.'%abspath(args[0]) )
        #####

        # Pulling the fasta file
        tmpFile = abspath(args[1])
        if ( isfile(tmpFile) ):
            fastaFile = abspath(args[1])
        else:
            parser.error( 'FASTA File:  %s is not found.'%abspath(args[1]) )
        #####
        
    #####
    
    # Reading in the base size increment
    if ( options.baseSizeIncrement > 0 ):
        baseSizeIncrement = options.baseSizeIncrement * 1000
    else:
        parser.error( "Base size increment must be > 0." )
    #####

    # Reading in the number of increments
    if ( options.numIncrements > 0 ):
        numIncrements = options.numIncrements
    else:
        parser.error( "Number of increments must be > 0." )
    #####
    
    # Pulling the output file name
    if ( options.outputFileName == '' ):
        pre = baseFileName(readsFile)
        outputFileName = '%s_prepedForScreening.fasta'%pre
    else:
        outputFileName = options.outputFileName
    #####
    
    # Pulling the readSet
    scaffSet = set( [item.strip() for item in open(readsFile,'r')] )
    
    # Assembly Index    
    assemblyIndex = FASTAFile_dict( fastaFile )
    
    # Contig breaker
    contigBreakFinder = re.compile( r'N{5,}' ).finditer

    # Pulling parts of the genome
    outputHandle = open( outputFileName, 'w' )
    for scaffID in scaffSet:

        # Pulling the record
        record = assemblyIndex[scaffID]
        recSeq = str(record.seq)
        nBases = len(recSeq)
        
        if ( nBases < baseSizeIncrement ):
            writeFASTA( [record], outputHandle )
            continue
        #####
        
        # Generating the contig bounds
        d = [item.span() for item in contigBreakFinder(recSeq)]
        d.insert( 0, (0,0) )
        d.append( (nBases,nBases) )
        scaffCounter     = 1
        notWritten       = True
        for n in xrange( len(d) - 1 ):
            # Getting the location of the start and end
            start      = d[n][1]
            stop       = d[n+1][0]
            contigSeq  = recSeq[ start:stop ]
            contigSize = len(contigSeq)
            nContig    = n + 1
            
            # Screening small contigs
            if ( (contigSize < baseSizeIncrement) and \
                 (contigSize < 1000 ) ): continue
            
            # Breaking contigs into pieces
            partitionCounter = 0
            while ( True ):
                # Generating the start and end
                tmpStart = partitionCounter * baseSizeIncrement
                tmpEnd   = (partitionCounter + 1) * baseSizeIncrement
                if ( tmpEnd >= contigSize ):  tmpEnd = contigSize
                # Checking the number of base pairs
                tmpSeq = contigSeq[tmpStart:tmpEnd]
                if ( countBasePairs(tmpSeq) > 500 ):
                    # Forming the phoney record
                    tmpRecord = SeqRecord( id  = '%s_contig=%d_partition=%d'%(record.id,nContig,scaffCounter), \
                                           seq = tmpSeq, \
                                           description = '' )
                    writeFASTA( [tmpRecord], outputHandle )
                    notWritten = False
                    # Incrementing the scaffold counter
                    scaffCounter += 1
                    if ( scaffCounter > numIncrements ): break
                #####
                # Checking to see if we have extended past the length
                if ( tmpEnd >= contigSize ): break
                # Incrementing the walking counter
                partitionCounter += 1
            #####
            if ( scaffCounter > numIncrements ): break
            # If all contigs were too small then write the record
            if ( notWritten ): writeFASTA( [record], outputHandle )
        #####

    #####
    outputHandle.close()
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
