#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__='Brent Wilson, bwilson@hudsonalpha.org'
__date__ ='$${date} ${time}$'

from os import system, getpid, uname, mkdir, chdir

from os.path import realpath, isfile, join, abspath, curdir, splitext, split, isdir

from optparse import OptionParser

from sys import stdout, stderr, exit

from shutil import rmtree

import gzip

import re

#==============================================================
	
def real_main():
    
    # Defining the program options
    usage = 'usage: %prog [Reads Positions File] [24mask Positions File] [100mask Positions File] [Reads Base Name] [24mask Regions Base Name] [100mask Regions Base Name]'

    parser = OptionParser(usage)
                       
    defVal = True
    parser.add_option( "-P", \
                       "--PacBio", \
                       action = 'store_false', \
                       help    = "Indicate that the reads are from Pac Bio.  Default=%r."%defVal, \
                       default = '%r'%defVal )
    
    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 8 ):
        parser.error( 'Incorrect number of arguments.  ' + \
                      'View usage using --help option.' )
    else:
        # Pulling the fileNames
        
        readsFile = realpath(args[0])
        if ( not isfile(readsFile) ): parser.error( '%s can not be found'%readsFile )
        
        regionsFile = realpath(args[1])
        if ( not isfile(regionsFile) ): parser.error( '%s can not be found'%regionsFile )
        
        bigRegionsFile = realpath(args[2])
        if ( not isfile(regionsFile) ): parser.error( '%s can not be found'%regionsFile )
        
        lastRegionsFile = realpath(args[3])
        if ( not isfile(lastRegionsFile) ): parser.error( '%s can not be found'%lastRegionsFile )

        outName = realpath(args[4])
        
        anName = realpath(args[5])
        
        bigAnName = realpath(args[6])
        
        lastAnName = realpath(args[7])

        #####

    #####
    
    pacBio = options.PacBio
    
    maskListNames = [[anName, regionsFile], [bigAnName, bigRegionsFile], [lastAnName, lastRegionsFile]]
    
    # Header: @SQ	SN:Gm01	LN:55915595
    # QNAME FLAG RNAME POS MAPQ CIGAR RNEXT PNEXT TLEN SEQ QUAL
    
    fakeSam = open('%s.sam'%outName, 'w')
    counter = 0
    endcheck = 0
    
    fakeSam.write('@SQ\tSN:Gm01\tLN:55915595\n')
    fakeSam.write('@PG\tID:bwa\tPN:bwa\tVN:0.6.0-r85\n')
    
    for line in open('%s'%readsFile, 'r'):
	    lsplit = line.split(None)
	    if pacBio == 'True':
		    start = int(lsplit [7])
		    end = int(lsplit [8])
		    length = end - start
		    if end > endcheck:
			    endcheck = end
		    fakeSam.write('FLHBFN1:216:D0RHAACXX:1:2111:5028:50%d\t163\tGm01\t%s\t2\t%dM\t=\t%s\t0\t*\t*\n'%(counter, start, length, end))
		    counter += 1
	    else:
		    if line [0] == 'm':
			    start = int(lsplit [7])
			    end = int(lsplit [8])
			    length = end - start
			    if end > endcheck:
				    endcheck = end
			    fakeSam.write('FLHBFN1:216:D0RHAACXX:1:2111:5028:50%d\t163\tGm01\t%s\t2\t%dM\t=\t%s\t0\t*\t*\n'%(counter, start, length, end))
			    counter += 1
  
    fakeSam.close()
    
    for names in maskListNames:
		    
	    otherSam = open('%s.sam'%names [0], 'w')
	    counter = 0
	    
	    otherSam.write('@SQ\tSN:Gm01\tLN:55915595\n')
	    otherSam.write('@PG\tID:bwa\tPN:bwa\tVN:0.6.0-r85\n')
	    
	    position = 0
	    openMask = 0
	    binStart = 0
	    binEnd = 0
	    binPositions = []
	    
	    for line in open('%s'%names [1], 'r'):
		    if line [0] != '>':
			    line = line [:-2]
			    for character in line:
				    position += 1
				    if character == 'X' and openMask == 0:
					    openMask = 1
					    binStart = position
				    elif openMask == 1 and character != 'X':
					    binEnd = position - 1
					    openMask = 0
					    binPositions.append((binStart,binEnd))
					    
	    for maskBin in binPositions:
		    start = int(maskBin [0])
		    end = int(maskBin [1])
		    length = end - start
		    otherSam.write('FLHBFN1:216:D0RHAACXX:1:2111:5028:50%d\t179\tGm01\t%s\t64\t%dM\t=\t%s\t0\t*\t*\n'%(counter, start, length, end))
		    counter += 1
		    otherSam.write('FLHBFN1:216:D0RHAACXX:1:2111:5028:50%d\t179\tGm01\t%s\t64\t%dM\t=\t%s\t0\t*\t*\n'%(counter, start, length, end))
		    counter += 1
		    otherSam.write('FLHBFN1:216:D0RHAACXX:1:2111:5028:50%d\t179\tGm01\t%s\t64\t%dM\t=\t%s\t0\t*\t*\n'%(counter, start, length, end))
		    counter += 1
	
	    otherSam.close()
    
    plotRange = int(endcheck * 1.03)
    
    system('/opt/samtools-0.1.18/samtools view -Sb -o %s.bam %s.sam'%(outName, outName))
    system('/opt/samtools-0.1.18/samtools view -Sb -o %s.bam %s.sam'%(anName, anName))
    system('/opt/samtools-0.1.18/samtools view -Sb -o %s.bam %s.sam'%(bigAnName, bigAnName))
    system('/opt/samtools-0.1.18/samtools view -Sb -o %s.bam %s.sam'%(lastAnName, lastAnName))
    system('/opt/samtools-0.1.18/samtools sort %s.bam %s.sorted'%(outName, outName))
    system('/opt/samtools-0.1.18/samtools sort %s.bam %s.sorted'%(anName, anName))
    system('/opt/samtools-0.1.18/samtools sort %s.bam %s.sorted'%(bigAnName, bigAnName))
    system('/opt/samtools-0.1.18/samtools sort %s.bam %s.sorted'%(lastAnName, lastAnName))
    system('/opt/samtools-0.1.18/samtools index %s.sorted.bam'%outName)
    system('/opt/samtools-0.1.18/samtools index %s.sorted.bam'%anName)
    system('/opt/samtools-0.1.18/samtools index %s.sorted.bam'%bigAnName)
    system('/opt/samtools-0.1.18/samtools index %s.sorted.bam'%lastAnName)
    
    system('java -mx2048m -jar /mnt/raid2/SEQ/BamView_v1.2.7.jar -a %s.sorted.bam %s.sorted.bam %s.sorted.bam %s.sorted.bam -n %d -pc -v ST'%(outName, anName, bigAnName, lastAnName, plotRange))

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
