#!/usr/common/usg/languages/python/2.7-anaconda/bin/python3
__author__="Chris Plott, cplott@hudsonalpha.org"
__date__ ="Created:  1/21/14"

from os.path import realpath, isdir, isfile, join, abspath, curdir

from os import system

from sys import stderr

from optparse import OptionParser

import subprocess

from time import time

from array import array

from hapipe.debug import profile
#=========================================================================
# Regular expressions
import re
commify_re          = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
findStuff           = re.compile( r'(1+)' ).finditer
findNBlocks         = re.compile( r'([N]+)' ).finditer
checkNSeq           = re.compile( r'((?=[ACTGN])[N]+)' ).split
findLowerCaseBlocks = re.compile( r'[1]+' ).finditer

#=========================================================================
# Useful Dictionaries
doesPass_dict = {True:'1',False:'0'}
indexChange   = {'1':'2','2':'1'}

#=========================================================================
# Translation tables
LC_table  = ''.maketrans( 'ACGTNXacgt', '0000001111' )
readTrans = ''.maketrans( ':-', '__' )
trantab   = ''.maketrans("ATCG", "TAGC")
#=========================================================================
class clusterClass(object):
	def __init__(self,initialPoint):
		self.points	 = [initialPoint]
		self.sum	 = initialPoint
		self.nPoints = 1.0
		self.mean	 = initialPoint

	def addPoint(self,point):
		self.points.append( point )
		self.sum += point
		self.nPoints += 1.0
		self.mean = self.sum / self.nPoints

	def __repr__(self):
		return self.points.__repr__()

	def __str__(self):
		return self.__repr__()
#=========================================================================
def simpleCluster(data):

	# Copying and sorting the data
	tmpData = [item for item in data]
	tmpData.sort()
	nStop = len(data) - 1
	# Selecting the first two points in the cluster
	clusters = 2 * [None]
	clusters[0] = clusterClass( tmpData[-1] )
	clusters[1] = clusterClass( tmpData[0]	)
	# Setting up the index list of remaining points
	nCounter = 1
	while ( True ):
		# Pick next point
		tmpPoint = tmpData[nCounter]
		# Who is it closest too?
		d_0 = abs( tmpPoint - clusters[0].mean )
		d_1 = abs( tmpPoint - clusters[1].mean )
		if ( d_0 <= d_1 ):
			clusters[0].addPoint(tmpPoint)
		else:
			clusters[1].addPoint(tmpPoint)
		#####
		# Continue?
		nCounter += 1
		if ( nCounter == nStop ): break
	#####
	
	return [clusters[0], clusters[1]]
	
#=========================================================================
class simpleSequenceScore(object):
	def __init__(self):

		# Initializing the triple counter
		bases = ['A','C','G','T']
		self.tripletCounts = {}
		for L_1 in bases:
			for L_2 in bases:
				for L_3 in bases:
					trpl = ''.join([L_1,L_2,L_3])
					self.tripletCounts[trpl] = 0
				#####
			#####
		#####

	def computeSimpleSequenceScore(self, seq):
	
		# Initializing the scores
		for trpl in self.tripletCounts.keys(): self.tripletCounts[trpl] = 0

		# Computing the size
		nSize = len(seq)

		# Accurately tabulating the triples
        # Counting the triples
		for n in range( nSize - 1 ):
			try:
				self.tripletCounts[seq[n:n+3]] += 1
			except KeyError:
				continue
			#####
		#####
	
		L	   = nSize - 2.0

		# Computing the score
		sigma = 0
		k	  = 0.0
		for t_count in self.tripletCounts.values():
			sigma += t_count * ( t_count - 1 )
			if ( t_count > 1 ): k += 1.0
		#####

		# Correction for sequencing errors
		if ( k > 2.0 ):
			# Cluster the 3mer counts to find errant 3mers arising due to
			# sequencing error
			clusters = simpleCluster( \
					[item for item in self.tripletCounts.values() if item > 1] )
			diff = clusters[0].mean - clusters[1].mean
			# If the cluster means are more than 10 counts from one another,
			# then recount
			if ( diff > 10 ):
				k = float( len(clusters[0].points) )
				# Recompute sigma
				sigma = 0
				for t_count in clusters[0].points:
					sigma += t_count * ( t_count - 1 )
				#####
			else:
				# For regular sequence reset the k-value if it is larger than 4
				if ( k > 4.0 ): k = 4.0
			#####
		#####

		# Compute score
		score = k * float( sigma ) / L / ( L - k )
	
		# Normalized for repetitive simple sequence
		return score
		
#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
        
#=========================================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )
#=========================================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True
#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True
#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False
#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        return True
    except IOError:
        return False
    #####

#=========================================================================
def printAndExec( cmd, oh_cmdLog, execute=True ):
    stderr.write( '%s\n\n'%cmd )
    oh_cmdLog.write( '%s\n\n'%cmd )
    try:
        if ( execute ): system( cmd )
    except KeyboardInterrupt:
        print( "KeyboardInterrupt detected, stopping the execution" )
        exit()
    #####
    return
    
#=========================================================================
def quickConvert(ucQual, charToAscii):
    qual = [charToAscii[item] for item in ucQual]
    return qual
#=========================================================================
def justConvert(ucQual, seq, name, vecScreen, charToAscii, libType):
    qual       = [charToAscii[item] for item in ucQual]
    return '%s 1 %d'%(name, len(qual)), seq, qual
#=========================================================================
def convertAndClip( ucQual,seq,name,vecScreen,charToAscii, libType ):
    # Convert the qual to ints
    windowSize = 20
    qualCutOff = 25
    minVal     = windowSize * qualCutOff
    qual       = [charToAscii[item] for item in ucQual]
    
    # find HQ window
    qualLen     = len(qual) - windowSize + 1
    clippedList = ''
    for i in range( qualLen ):clippedList = clippedList+doesPass_dict[sum(qual[i:i+windowSize]) >= minVal]

    HQRanges = []
    for item in findStuff( clippedList ):HQRanges.append(item.span()) 
    
    try:
        clipStart = HQRanges[0][0] 
        clipEnd   = HQRanges[-1][1] + 19
        if (clipEnd +1 == qualLen +windowSize): clipEnd += 1
    except IndexError:
        return
    #####
    
    # Return the clipped read    
    if ( vecScreen ):
        return '%s %d %d'%(name,clipStart,clipEnd),seq,qual
    else:
        return name,seq[clipStart:clipEnd],qual[clipStart:clipEnd]
    #####

#=========================================================================
def new_custom_LFPE_clip( clippedName, clippedSeq, clippedQual,libType, pairedTypeSet, merSize, clipLength,vecMerSet, records):
    # Pulling the baseName
    unclippedRecords      = records
    name ,HQ_start,HQ_end = clippedName.split(None)
    baseName, readIndex   = name.split('-R')
    
    # Reading in the high quality range
    HQ_start = int(HQ_start)
    HQ_end   = int(HQ_end)

    # Mask the read
    maskingDict   = {0:{'A':'A','T':'T','C':'C','G':'G','N':'N'},1:{'A':'a','T':'t','C':'c','G':'g','N':'n'}}
    lenSeq     = len(clippedSeq)
    maskedList  = [0] * lenSeq
    for val in range( lenSeq - (merSize-1) ):
        if ( clippedSeq[val:(val+merSize)] in vecMerSet ):
            for i in range(merSize): maskedList[val+i] = 1
        #####
    #####
    # Was the read masked
    if ( sum( maskedList ) == 0 ): return name, clippedSeq[ HQ_start:HQ_end ], clippedQual[ HQ_start:HQ_end ], unclippedRecords

    tmpSeq = ''.join( [maskingDict[maskedList[item]][clippedSeq[item]] for item in range(lenSeq)] )      
    # Hard coded values
    HQ_region_spacing = 36 #bp
    collapseSpacing   = 5  #bp
    minRegionLength   = int( HQ_region_spacing / 2 ) #bp
    minReadLength     = clipLength

    # Pulling the sequence
    readLength = len( tmpSeq )
    
    # Computing the amount of lower case sequence
    lowerCaseRepresentation = tmpSeq.translate(LC_table)
    
    # Finding the blocks of masked sequence
    LC_regions = []
    for item in findLowerCaseBlocks(lowerCaseRepresentation):
        start, end = item.span()
        tmpLen     = end - start + 1
        LC_regions.append( (start, end, tmpLen) )
    #####
    # Sort the regions by start value and
    # collapse if the regions are within 5 bases of
    # one another.
    LC_regions.sort()
    addedN_range = False
    N_baseSet    = set()
    nIter = 0
    while ( True ):
        noCollapsing = True
        for n in range( len(LC_regions) - 1 ):
            s1, e1, L1 = LC_regions[n]
            s2, e2, L2 = LC_regions[n+1]
            if ( (s2-e1) <= collapseSpacing ):
                # Collapse the regions
                LC_regions.pop(n+1)
                LC_regions[n] = ( s1, e2, (e2 - s1 + 1) )
                noCollapsing  = False
                break
            elif ( collapseSpacing < (s2-e1) < minRegionLength ):
                # Look for N's in the beween sections
                # and adding them as an individual section
                addedN_range = False
                for m in range( e1, s2 ):
                    if ( m in N_baseSet ): continue
                    if ( tmpSeq[m] == 'N' ):
                        N_baseSet.add(m)
                        LC_regions.append( (m, m, 1) )
                        LC_regions.sort()
                        noCollapsing = False
                        addedN_range = True
                    #####
                #####
                if ( addedN_range ): 
                    addedN_range = False
                    break
                #####
            #####
        #####
        if ( noCollapsing ): break
        if ( nIter > 100 ):
            print( record['id'] )
            print( HQ_start, HQ_end )
            print( tmpSeq )
            print( lowerCaseRepresentation )
            print( LC_regions )
            print( "------------------" )
            assert False
        #####
        nIter += 1
    #####
    # Filter regions based on size and location
    LC_regions.sort()
    popList = []
    for n in reversed(range(len(LC_regions))):
        # Is the section short enough to toss?
        if ( LC_regions[n][2] < minRegionLength ):
            # Is the region further away from the HQ range than the linker size?
            # Must be further than the linker size from both HQ ends!!
            if ( (( HQ_end - LC_regions[n][0] ) > HQ_region_spacing) and \
                 (( LC_regions[n][1] - HQ_start -1) > HQ_region_spacing) ):
                    popList.append(n)
            #####
        #####
    #####
    if ( len(popList) > 0 ):
        # Filtering on size
        popList.sort(reverse=True)
        for item in popList: LC_regions.pop(item)
    #####

    # Cut the read at the lowest start value
    # as long as it is long enough
    LC_regions.sort()
    if ( len(LC_regions) > 0 ):
        if ( LC_regions[0][0] >= minReadLength ):
            newEnd              = records[1].find(clippedSeq[:LC_regions[0][0]])+LC_regions[0][0]
            unclippedRecords[1] = unclippedRecords[1][:newEnd]
            unclippedRecords[2] = unclippedRecords[2][:newEnd]
            return name, clippedSeq[:LC_regions[0][0]], clippedQual[:LC_regions[0][0]],unclippedRecords
        #####
    else:
        if ( libType in pairedTypeSet ):
            return name, clippedSeq[HQ_start:HQ_end], clippedQual[HQ_start:HQ_end], unclippedRecords
        else:
            return name, clippedSeq, clippedQual, unclippedRecords
#=========================================================================
def iterItems( tmpHandle ):
    # Stripped down iterator for reads files
    while True:
        line = tmpHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        splitLine = line[1:].split(None)
        id = splitLine[0]
        try:
            desc = ' '.join( splitLine[1:] )
        except IndexError:
            desc = ''
        #####
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or (line[0] == '>')): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield {'id':id, 'data':''.join(tmpData), 'desc':desc}
        # Stopping the iteration
        if ( not line ): return
    #####
    assert False, 'Should never reach this line!!'
#=========================================================================
class counterClass(object):
    def __init__(self, allowUnpairedReads, contamFASTA, libBase, fastqFile, libType, numLibs):
        self.libBase           = libBase
        self.fastqFile         = fastqFile
        self.contamFASTA       = contamFASTA
        self.libType           = libType
        self.numLibs           = numLibs
        
        self.readsExtracted    = 0
        self.seqExtracted      = 0
        
        self.readsPrepped      = 0
        self.seqPrepped        = 0
        
        self.readsLostToSimple = 0
        self.seqLostToSimple   = 0
        
        self.readsLostToContam = 0
        self.seqLostToContam   = 0
        self.contamSummery     = {}
        
        self.readsLostToVector = 0
        self.readsLostToLQ     = 0
        self.readsLostToNs     = 0
        self.readNotPaired     = 0
        
        self.statsPrint        = {0:self.printReport}
        
        self.inc               = 2
        if ( allowUnpairedReads ): self.inc = 1
        
        
    #####
    def printReport(self):
        # Writing the extraction summary
        outputString = []
        outputString.append( "\n=============================\n" )
        outputString.append( "CUMULATIVE EXTRACTION SUMMARY\n" )
        outputString.append( "=============================\n" )
        outputString.append( "Reads File:        %s\n"%self.fastqFile )
        outputString.append( "Reads Extracted:   %s\n"%commify(self.readsExtracted) )
        outputString.append( "Seq   Extracted:   %s bp\n---\n"%commify(self.seqExtracted) )
        outputString.append( "N Lost Reads:      %s\n---\n"%(commify(self.readsLostToNs)))
        outputString.append( "Low Quality Reads: %s\n---\n"%(commify(self.readsLostToLQ)))
        outputString.append( "Vector trimmed:    %s\n---\n"%(commify(self.readsLostToVector)) )
        outputString.append( "Simple Reads:      %s\n"%(commify(self.readsLostToSimple)) )
        outputString.append( "Simple Seq:        %s bp\n---\n"%commify(self.seqLostToSimple) )
        outputString.append( "Prepped Reads:     %s\n"%(commify(self.readsPrepped)) )
        outputString.append( "Prepped Seq:       %s bp\n"%commify(self.seqPrepped) )
        if ( self.contamFASTA != None ):
            outputString.append( "---\nContaminant Reads: %s\n"%(commify(self.readsLostToContam)) )
            outputString.append( "Contaminant Seq:   %s bp\nContaminant Summary:\n"%commify(self.seqLostToContam) )
            DSU = [(value,key) for key,value in self.contamSummery.items()]
            DSU.sort(reverse=True)
            for conCount, conName in DSU:
                outputString.append( "\t\t%s:\t%s\n"%( conName, commify(conCount) ) )
            #####
        #####
        outputString.append( "=============================\n" )
        
        # Writing the extraction summary for the user
        stats_h = open( '%s.extractionStats'%self.libBase, 'w' )
        printAndExec( ''.join(outputString), stats_h, execute=False )
        #stats_h = open( '%s.extractionStats'%self.libBase, 'w' )
        #stats_h.write( ''.join(outputString) )
        stats_h.close()
    #####
    def increment_readsExtracted(self, seq):
        self.readsExtracted += 1
        self.seqExtracted   += len(seq)
        
        if ( self.readsExtracted%100000 == 0 ): print('%s reads extracted'%( commify(self.readsExtracted) ) )
        if ( self.readsExtracted%10000000 == 0 ):self.printReport()
    #####
    def increment_readsPrepped(self, seq):
        self.readsPrepped += 1
        self.seqPrepped   += len(seq)
    #####
    def increment_readsLostToContam(self, seq):
        self.readsLostToContam += 1
        self.seqLostToContam   += len(seq)
    ##### 
    def update_contamSummery(self, newContam):
        for key,value in newContam.items(): 
            try:
                self.contamSummery[key] += value
            except KeyError:
                self.contamSummery[key] = value
            #####
        #####
    #####
    
    #####
    def increment_readsLostToSimple(self, seq, seq2 = ''):
        self.readsLostToSimple += 1
        self.seqLostToSimple   += len(seq)
    #####
    def increment_readsLostToVector(self):
        self.readsLostToVector += self.inc
    #####
    def increment_readsLostToLQ(self):
        self.readsLostToLQ += self.inc
    ##### 
    def increment_readsLostToNs(self):
        self.readsLostToNs += self.inc
    #####    
#=========================================================================
class FASTQ_Iter(object):
    def __init__(self, FASTQlist,awkFastqCmd,allowUnpairedReads, seqString, counterClass ):
        self.sepString          = seqString
        self.allowUnpairedReads = allowUnpairedReads
        self.awkCMD             = awkFastqCmd
        self.counterClass       = counterClass
        self.lostToNSet         = set()
        self.inBufferSet        = set()
        self.iterList           = [self.iterFASTQ(subprocess.Popen( '%s | %s'%(item, self.awkCMD),shell=True, stdout=subprocess.PIPE).stdout, self.allowUnpairedReads, self.sepString) for item in FASTQlist]
        if(len(self.iterList)>1):
            self.nextCMD    = self.next_2
            self.buffDict   = {}
            self.buffList   = []
            self.buffSize   = 0
        else:
            self.nextCMD = self.next_1
        #####

    #####
    def iterFASTQ(self, fastqHandle, allowUnpairedReads, seqString ):
        while(True):
            try:
                line            = fastqHandle.readline().decode('utf-8')
                name, seq, qual = line.split(None)
            except ValueError:
                return
            #####
            self.counterClass.increment_readsExtracted(seq)
                         
            yield [name, seq, qual]
        ##### 
   
    #####
    def next_1(self):
        # There is only one FASTQ
        while(True):
            records = [] 
            recSize = 2
            if (self.allowUnpairedReads): recSize = 1
            returnMe = True
            for i in range(recSize):
                try:
                    name, seq, qual = self.iterList[0].__next__()
                except ValueError:
                    return
                #####
                seqList = [item for item in checkNSeq(seq) if item not in '']
                if ( seqList[0][0] == 'N' ):
                    qual = qual[len(seqList[0]):]
                    seqList.pop(0)
                #####
                try:
                    if ( seqList[-1][0] == 'N' ):
                        qual = qual[:-len(seqList[-1])]
                        seqList.pop(-1)
                    #####
                except IndexError:
                    returnMe = False
                    continue
                #####
                while(True):
                    if ( len(seqList) < 2 ):
                        break
                    #####
                    testList = seqList[0:3]
                    if (len(testList[0])==1):
                        qual    = qual[len(seqList[0])+len(seqList[1]):]
                        seqList = seqList[2:]
                        returnMe = False
                        continue
                    #####
                    if (len(testList[1])>1):
                        seqList = testList[0]
                        break
                    else:
                        break
                    #####
                #####
                cleanedSeq  = ''.join(seqList)
                cleanedQual = qual[:len(cleanedSeq)] 
                records.append([name, cleanedSeq, cleanedQual])          
            #####
            if ( returnMe ):yield records  
        #####
        

    def next_2(self):
        # There are two FASTQ's
        while(True):
            missingDict ={}
            records     = []
            matches     = True
            
            for record in self.iterList: 
                name, seq, qual = record.__next__()
                
                try:
                    baseName, index = name.split('-R')
                except ValueError:
                    print ("THERE IS A NAME PROBLEM ON THIS RECORD!!!!")
                    print ("%s\n%s\n%s"%(name, seq, qual))
                    assert False
                #####    
                
                if(baseName in self.lostToNSet):
                    self.lostToNSet.remove(baseName)
                    continue
                #####
                
                seqList = []
                for item in checkNSeq(seq):
                    if item in '': continue
                    seqList.append(item)
                #####
                
                if ( seqList[0][0] == 'N' ):
                    qual = qual[len(seqList[0]):]
                    seqList.pop(0)
                #####
                try:
                    if ( seqList[-1][0] == 'N' ):
                        qual = qual[:-len(seqList[-1])]
                        seqList.pop(-1)
                    #####
                except IndexError:
                    self.counterClass.increment_readsLostToNs()
                    if (baseName in self.inBufferSet):
                        self.lostToNSet.remove(baseName)
                        removeFromBuffer(baseName)
                        continue
                    else:
                        self.lostToNSet.add(baseName)
                        continue
                    #####
                #####
                while(True):
                    if ( len(seqList) < 2 ):
                        break
                    #####
                    testList = seqList[0:3]
                    if (len(testList[0])==1):
                        qual    = qual[len(seqList[0])+len(seqList[1]):]
                        seqList = seqList[2:]
                        continue
                    #####
                    if (len(testList[1])>1):
                        seqList = testList[0]
                        break
                    else:
                        break
                    #####
                #####
                cleanedSeq  = ''.join(seqList)
                if (len(cleanedSeq)<50):
                    self.counterClass.increment_readsLostToNs()
                    if (baseName in self.inBufferSet):
                        self.lostToNSet.remove(baseName)
                        removeFromBuffer(baseName)
                        continue
                    else:
                        self.lostToNSet.add(baseName)
                        continue
                    #####
                #####
                cleanedQual = qual[:len(cleanedSeq)]
                
                records = [name, cleanedSeq, cleanedQual]
                                
                try:
                    self.buffDict[baseName].append((int(index),records))
                    yield self.removeFromBuffer(baseName)
                except KeyError:
                    self.buffDict[baseName]=[(int(index),records)]
                    self.addToBuffer(baseName)
                #####             
                if(baseName in self.lostToNSet):
                    self.lostToNSet.remove(baseName)
                    removeFromBuffer(baseName)
                    continue
                #####
            #####
        #####    
    #####
        
    #####
    def next(self):
        return self.nextCMD().__next__()  
    #####  
    def removeFromBuffer(self, baseName):
        self.buffList.remove(baseName)
        self.buffSize -= 1
        sortRec = sorted(self.buffDict.pop(baseName))
        return [sortRec[0][1],sortRec[1][1]]
    #####
    def addToBuffer(self, baseName):
        if(self.buffSize == 10000):
            buffItem = self.buffList.pop(0)
            self.buffList.append(baseName)
            self.buffDict.pop(buffItem)
        else:
            self.buffList.append(baseName)
            self.buffSize += 1
        #####
    
    #####
#=========================================================================      
def contamScreen(contamBuffer, contamCMD, mainCounterClass, allowUnpairedReads,contamFasta_oh, contamQual_oh, wholeBuff, charToAscii, intToChar, whole=False):
    inputStr      = ''
    newContamStat = {}
    removeSet     = set()
    increment     = 2
    if(allowUnpairedReads): increment = 1
    contamProcess = subprocess.Popen( contamCMD , bufsize = 10000000, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE )
    
    inputStr = ''.join( ">%s\n%s\n"%(record[0],record[1]) for record in contamBuffer )
    
    # Process the BLAT output
    contamOutput = contamProcess.communicate(input = inputStr.encode())[0].decode('UTF-8').split("\n")
    for hit in contamOutput:
        try:
            read, contam = hit.split(None)
        except ValueError:
            break
        #####
        if(read in removeSet): continue
        
        removeSet.add(read)
        if (not allowUnpairedReads):
            # If paired pull the other read
            base, index  = read.split('-R')
            removeSet.add( '%s-R%s'%( base, indexChange[index] ) )
        #####
        try:
            newContamStat[contam] += increment
        except KeyError:
            newContamStat[contam] = increment
        ####
    #####
    contamProcess.poll()

    # Rebuild the records list removing the contaminated reads
    screenedList = []
    if (whole):
        for record in wholeBuff:
            if( record[0] in removeSet ): 
                writeFastaQual( [record], contamFasta_oh, contamQual_oh, mainCounterClass.increment_readsLostToContam, charToAscii, intToChar )
                continue
            #####
            screenedList.append(record)
        #####
    else:
        for record in contamBuffer:
            if( record[0] in removeSet ):
                writeFastaQual( [record], contamFasta_oh, contamQual_oh, mainCounterClass.increment_readsLostToContam, charToAscii, intToChar )
                continue
            #####
            screenedList.append(record)
        #####
    #####
    
    mainCounterClass.update_contamSummery( newContamStat )
    
    # Return the screened Reads for writing
    return screenedList
    
#=========================================================================    
def writeFastaQual_old( clippedRecord, Fasta_oh, Qual_oh, counter, charToAscii ):
    for record in clippedRecord:
        Fasta_oh.write( bytes( '>%s\n%s\n'%( record[0], record[1] ),'UTF-8' ) )
        if(type(record[2][0]) != type(3)):
            qual = quickConvert(record[2], charToAscii)
        else:
            qual = record[2]
        Qual_oh.write( bytes( '>%s\n%s\n'%( record[0], (str(qual).replace(',','')[1:-1])),'UTF-8' ) )
        counter( record[1] )
    #####
    return
#=========================================================================    
def writeFastaQual( clippedRecord, Fasta_oh, Qual_oh, counter, charToAscii, intToChar ):
    for record in clippedRecord:
        try:
            name       = record[0]
            seq        = record[1]
            
            if(type(record[2][0]) != type(3)):
                qualList = quickConvert(record[2], charToAscii)
            else:
                qualList = record[2]
            #####
            qual = ''.join([intToChar[item] for item in qualList])
            Fasta_oh.write( bytes( '@%s\n%s\n+\n%s\n'%( name, seq, qual ),'UTF-8' ) )
            counter( record[1] )
        except IndexError:
            print(record)
            continue
        #####
    #####
    return
#=========================================================================
def vectorCheck(readCmd,awkFastqCmd,linkerDict,libType,libTypeSet,merDict, sepString, clipLength,charToAscii, pairedTypeSet,maxReadLen, simpleScreenClass, linkerFASTA, contamFASTA):
    # Loop over the linkers with proper mer sizes
    startTime       = time()
    telCutOff       = .7
    if (libType in pairedTypeSet): telCutOff   = .3
    
    vec_oh          = open('vectorCheckReport.dat','w')
    telSeq          = 'TTAGGG'
    telSeqRev       = telSeq.translate(trantab)[::-1]
    telLength       = len(telSeq)
    telMax          = maxReadLen * telCutOff / telLength
    teloReads       = 0
    totalReads      = 0
    kmerSize        = 32
    PkDict          = {}
    PkTDict         = {}
    nKmers          = (maxReadLen)-kmerSize+1
    PTList          = [0]*(nKmers)
    totalKmers      = 0
    halfMaxRead     = int(maxReadLen/2.0)
    vecTestProc     = subprocess.Popen( "%s | head -400000 | awk '{if (NR%%4 ==2 ) print}'"%(readCmd),shell=True, stdout=subprocess.PIPE)
    
    
    # Make the kmer
    for rawLine in vecTestProc.stdout:
        line = rawLine.decode('UTF-8')
        # Make kmers for vector check 
        for i in range(nKmers):
            kmer = line[i:(i+kmerSize)]
            revK = kmer.translate(trantab)[::-1]
            kKey = '_'.join(sorted([kmer,revK]))
            PTList[i]  += 1.0
            totalKmers += 1.0
            try:
                PkDict[kKey] += 1.0
            except KeyError:
                PkDict[kKey]  = 1.0
            #####
            
            try:
                PkTDict[kKey][i] += 1.0
            except KeyError:
                PkTDict[kKey]     = array('d', [0]*maxReadLen)
                PkTDict[kKey][i] += 1.0
            #####
            totalKmers += 1.0
        #####
        totalReads += 1.0
        
        # How much Telomere is there?
        telCount = line.count(telSeq) + line.count(telSeqRev)
        if (telCount >= telMax): teloReads += 1.0
        #####
    #####
    
    # Are there any over represented kmers?
    resultStr  = ''
    resultDict = {}
    n = 1
    for key in PkTDict:
        kTotal  = PkDict[key]/totalKmers
        ratio   = []
        keyList = key.split('_')
        front   = simpleScreenClass.computeSimpleSequenceScore(keyList[0])
        if ((kTotal < 1.e-4)): continue
        if (front > 0.1): continue
        firstHalf = float(sum(PkTDict[kKey][:halfMaxRead]))
        whole     = float(sum(PkTDict[kKey]))
        if ((firstHalf/whole)<.6): continue
        resultStr       = '%s>%d\n%s\n>%d\n%s\n'%(resultStr,n,keyList[0],(n+1),keyList[1])
        resultDict[n]   = keyList[0]
        resultDict[n+1] = keyList[1]
        n += 2           
    #####
    
    # No, continue
    if(len(resultDict)==0):
        vec_oh.write('Percentage Telomeric %.2f\n\n'%(teloReads*100/totalReads))
        vec_oh.write('NO UNUSUAL VECTOR FOUND:\tMOVING ON\n')
        return
    #####
    
    # Yes, it is known vector
    blatSet      = libTypeSet
    blatSet.add('CONTAM')
    linkerDict['CONTAM'] = contamFASTA
    failedString = ''
    kmerSet      = set(resultDict.keys())
    allVec       = False
    for vector in blatSet:
        print('\tSTART PROCESS %s'%vector)
        vecTestProc   = subprocess.Popen("/mnt/local/EXBIN/blat -noHead %s stdin stdout | awk '{print $10}'"%(linkerDict[vector]),shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        vecTestOutput = vecTestProc.communicate(input = str(resultStr).encode())[0].decode('UTF-8').strip().split("\n")
        if(vecTestOutput[0] == ''): 
            print('\tNO HITS')
            continue
        failedString = '%s\n\t\t%s-%d hits'%(failedString,vector,len(vecTestOutput))
        
        if (len(vecTestOutput) == n): 
            
            allVec = True
            continue
        #####
        for line in vecTestOutput:
            if(int(line) not in kmerSet): continue
            kmerSet.remove(int(line))
        #####
    #####    
    vec_oh.write('Percentage Telomeric %.2f\n\n'%(teloReads*100/totalReads))
    if ((allVec) or (len(kmerSet)==0)): 
        vec_oh.write('ALL KMERS FOUND IN: %s\n'%(failedString))
        return
    else:
        kmer_oh = open('OverAbundantKmers.fasta','w')
        stderr.write('\tOVER ABUNDANT KMERS DETECTED\n')
        n = 1
        for kmer in kmerSet:
            vec_oh.write('%s\n'%(resultDict[kmer]))
            kmer_oh.write('>%d\n%s\n'%(n,resultDict[kmer]))
            n += 1
        #####
        assert False
    #####
    
#=========================================================================
def real_main():
    # Defining the program options
    usage = "usage: %prog [Project path] [Library base name] [options]"

    parser = OptionParser(usage)

    nReads = -1
    parser.add_option( '-n', \
                       "--nReads", \
                       type    = 'int', \
                       help    = 'Number of reads to extract: Default:  Extract all.', \
                       default = nReads )
                       
    nSkip = 0
    parser.add_option( "-k", \
                       "--nSkip", \
                       type    = "int", \
                       help    = "Number of reads to skip at the beginning of the file: Default:  Skip first %s"%commify(nSkip), \
                       default = nSkip )

    contamFASTA = None
    parser.add_option( "-c", \
                       "--contamFASTA", \
                       type    = 'str', \
                       help    = 'Contaminant FASTA File.  Default:  no contaminant screen', \
                       default = contamFASTA )
    
    linkerFASTA = None
    parser.add_option( "-v", \
                       "--linkerFASTA", \
                       type    = 'str', \
                       help    = 'Force different vector or linker sequence.', \
                       default = linkerFASTA )

    parser.add_option( '-e', \
                       "--noVecCheck", \
                       action  = "store_false", \
                       dest    = 'noVecCheck', \
                       help    = "Skips the search for different vector then the one for the library type.  Default:  Does not search for different vector." )
    parser.set_defaults( noVecCheck = True )
    
    merSize    = None
    parser.add_option( "-m", \
                       "--merSize", \
                       type    = "int", \
                       help    = "Change the Linker Masking kmer size. Default:         "
                                 "LFPE,CLRS,NEXP-10mer                                  "
                                 "NGEN,NEXT,JGIFRAG,METHYL-10mer                        "
                                 "RNASEQ-14mer", \
                       default = merSize )

    parser.add_option( '-u', \
                       "--allowUnpairedReads", \
                       action  = "store_true", \
                       dest    = 'allowUnpairedReads', \
                       help    = "Allow Unpaired Reads.  Default:  Enforces Pairing." )
    parser.set_defaults( allowUnpairedReads = False )
    
    parser.add_option( '-s', \
                       "--noSimpleFilter", \
                       action  = "store_true", \
                       dest    = 'noSimpleFilter', \
                       help    = "Do Not Filter Simple Sequence.  Default:  Filter Simple Sequence." )
    parser.set_defaults( noSimpleFilter = False )
    
    defaultContamID = 95
    parser.add_option( '-i', \
                       "--ContamID", \
                       type    = 'int', \
                       help    = 'Minimum contam read identity: Default:  %d'%(defaultContamID), \
                       default = defaultContamID )
    
    defaultContamCoverage = 50
    parser.add_option( '-o', \
                       "--ContamCoverage", \
                       type    = 'int', \
                       help    = 'Minimum contam read coverage: Default:  %d'%(defaultContamCoverage), \
                       default = defaultContamCoverage )


    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling project path
        projectPath = realpath( args[0] )
        testDirectory( projectPath )
        # Pulling the library base
        libBase = args[1]
    #####

    #=================================================================
    # Checking for the contaminant file
    screenContam = False
    if ( options.contamFASTA != None ): 
        screenContam = True
        contamFASTA = realpath(options.contamFASTA)
        testFile( contamFASTA )
        
        # Blat command for contam screening
        blatCMD        = '/mnt/local/EXBIN/blat -noHead -extendThroughN %s stdin stdout'%(contamFASTA)
        contamAWKCMD   = "awk '{matches = $1+$3+$4; split($19,SIZE,\",\");split($20,STARTS,\",\"); qlen=(STARTS[length(STARTS)-1]+SIZE[length(SIZE)-1])-STARTS[1]; COV=100*(qlen/$11); ID=matches*100/qlen; if((COV>=%d)&&(ID>=%d)){ print $10, $14; next}}'"%(options.ContamCoverage,options.ContamID)
        contamCMD      = '%s | %s'%(blatCMD, contamAWKCMD)
        
        # For batch contam screening
        contamBuffer   = []
        wholeBuff      = []
    #####
    
    # Setting Simple Filter
    maxCon            = 98.0 / 100.0
    simpleScoreCutoff = maxCon * maxCon
    # Are Unpaired Reads allowed
    
    allowUnpairedReads = options.allowUnpairedReads
    recordNumber       = 2 
    if (allowUnpairedReads): recordNumber = 1
    
    screenSimple = True
    if(options.noSimpleFilter):screenSimple = False
    
    #=================================================================
    # Reading lib.config
    libDict    = {}
    testDirectory( projectPath )
    unProcDir  =  join(projectPath,'unProcessed')
    testDirectory( unProcDir )
    configFile = join(unProcDir,'lib.config')
    testFile(configFile)
    for line in open( configFile ):
        splitLine = line.split(None)
        if ( splitLine == [] ): continue
        libDict[splitLine[0]] = splitLine[1:]
    #####

    #=================================================================
    # Checking the status of the library
    pairedTypeSet = set(['LFPE','CLRS','RNASEQ', 'NEXP'])
    libTypeSet    = set(['JGIFRAG','NGEN','NEXT','METHYL']).union(pairedTypeSet)
    linkerDict    = { 'JGIFRAG': join(projectPath,'linkerSeq/illuminaLinker.fasta'),\
                      'LFPE':    join(projectPath,'linkerSeq/LFPE_linker.fasta'),   \
                      'CLRS':    join(projectPath,'linkerSeq/CRELOX_linker.fasta'), \
                      'NGEN':    join(projectPath,'linkerSeq/nugenAdapter.fasta'),  \
                      'NEXT':    join(projectPath,'linkerSeq/nexteraAdapter.fasta'),\
                      'NEXP':    join(projectPath,'linkerSeq/nexteraAdapter.fasta'),\
                      'RNASEQ':  join(projectPath,'linkerSeq/rnaSeqLinker.fasta'),\
                      'METHYL':  join(projectPath,'linkerSeq/otherIlluminaLinker.fasta') }
    merDict       = { 'JGIFRAG': 10, 'LFPE': 10, 'CLRS': 10, \
                      'NGEN': 10,'NEXT': 10,'RNASEQ': 14,\
                      'METHYL':10, 'NEXP': 10 }
    
    
    if ( libBase in libDict ):
        libStatus = libDict[libBase][3]
        libType   = libDict[libBase][0]
        readLen   = int(libDict[libBase][2].split('x')[1])
        # Check to see if it has been processed
        if ( libStatus != "UNPROCESSED" ): raise IOError( 'LIBRARY STATUS IS %s, HAULTING THE PREP PROCESS\n'%libStatus )
        # Check to see if it has an appropriate type
        if ( libType not in libTypeSet ):  raise IOError( 'LIBRARY TYPE is %s, HAULTING THE PREP PROCESS\n'%libType )
        
        # If it is paired, then it must have a linkerSeq file!
        if ( not options.merSize ):
            merSize = merDict[libType]
        else:
            merSize = options.merSize
        #####
        
        # Is it a linear lib
        isLinear = (libType not in pairedTypeSet)
        
        # Checking for the vector file
        linkerFASTA = realpath(linkerDict[libType])
        testFile( linkerFASTA )
        
        if ( options.linkerFASTA != None ): 
            linkerFASTA = realpath(options.linkerFASTA)
            libTypeSet.add('CUSTOM')
            linkerDict['CUSTOM'] = linkerFASTA
            merDict['CUSTOM']    = merSize
            testFile( linkerFASTA )
            
        # Make the vector mer set
        vecScreen = True
        vecMerSet = set()
            
        for record in iterItems( open(linkerFASTA) ):
            vectorSeq       = record['data']
            for val in range(len(vectorSeq)-(merSize-1)): 
                kmer = vectorSeq[val:(val+merSize)].upper()
                vecMerSet.add(kmer)
                vecMerSet.add( kmer.translate(trantab)[::-1] )
            #####
        ##### 
        
        if (libType == 'RNASEQ'): 
            screenSimple           = False
            allowUnpairedReads     = True
        #####
        
        clippingFunc = convertAndClip
        
        if (libType == 'METHYL'):
            clippingFunc           = justConvert
            screenSimple           = False
        #####
        
        whole = False
        if (libType not in pairedTypeSet ): whole=True
        
        stderr.write( '\n\nLIBRARY NAME:       %s\n'%libBase )
        stderr.write( 'LIBRARY STATUS IS:  %s\n'%libStatus )
        stderr.write( '\tLibrary Type: %s\n'%libDict[libBase][0] )
        stderr.write( '\tInsert Size:  %s\n'%libDict[libBase][1] )
        stderr.write( '\tRead Length:  %s\n'%libDict[libBase][2] )
        fastqFILES = []
        for item in libDict[libBase][4].split(','):
            tmpFASTQ = join(unProcDir,item)
            if ( testFile( tmpFASTQ ) ):
                fastqFILES.append(tmpFASTQ)
            #####
            stderr.write( '\tFASTQ File:   %s\n'%tmpFASTQ )
        #####
    else:
        raise IOError( 'Library name not found in %s'%configFile )
    #####
    
    #What is the min length of a clipped read
    if ( readLen >= 250 ):
        clipLength = 75
    else:
        clipLength = 50
    #####
    
    if(libType in pairedTypeSet): clipLength = 50
    #=================================================================
    # Set up the temporary directory
    #tmpDir, tmpPath, basePath = createTmpDirectory()
    
    #=================================================================    
    # Initialize the counting class for Statistics
    
    mainCounterClass  = counterClass(options.allowUnpairedReads, options.contamFASTA, libBase, fastqFILES, libType, len(fastqFILES))
    simpleScreenClass = simpleSequenceScore()
    #=================================================================
    
    # Handles needed to prep reads
    simpleFasta_oh = Bzip_file('%s.simpleReads.fastq.bz2'%(libBase), 'w')
    simpleQual_oh  = " " #Bzip_file('%s.simpleReads.qual.bz2'%(libBase), 'w')
    if ( screenContam ):
        contamFasta_oh = Bzip_file('%s.contam.fastq.bz2'%(libBase), 'w')
        contamQual_oh  = " " #Bzip_file('%s.contam.qual.bz2'%(libBase), 'w')
    #####
    if ( libType in pairedTypeSet ):
        preppedFasta_oh    = Bzip_file('%s.prepped.uncomp.fastq.bz2'%(libBase), 'w')
        preppedQual_oh     = " " #Bzip_file('%s.prepped.uncomp.qual.bz2'%(libBase), 'w')
    else:
        preppedFasta_oh    = Bzip_file('%s.prepped.fastq.bz2'%(libBase), 'w')
        preppedQual_oh     = " " #Bzip_file('%s.prepped.qual.bz2'%(libBase), 'w')
    #####

    #=========================================================================
    # Read 1000 reads into the fastq and then pull 10
    
    readCmdList = []
    for fastq in fastqFILES:
        if ( isBzipFile(fastq) ):
            readCmdList.append('cat %s | bunzip2 -c'%fastq)
        elif ( isGzipFile(fastq) ):
            readCmdList.append('cat %s | gunzip -c'%fastq)
        else:
            readCmdList.append('cat %s'%fastq)
        #####
    #####
         
    headCmd     = 'head -4000 | tail -40'
    sepProcess = subprocess.Popen( '%s | %s'%(readCmdList[0], headCmd), shell=True, stdout=subprocess.PIPE)
    
    # Use the 10 reads pulled to determine phred encoding and sepString
    testQual  = ''
    sepString = None
    while(True):
        try:    
            lines = [     sepProcess.stdout.readline().decode('utf-8') ]
            lines.append( sepProcess.stdout.readline().decode('utf-8') )
            lines.append( sepProcess.stdout.readline().decode('utf-8') )
            lines.append( sepProcess.stdout.readline().decode('utf-8') )
            testQual = ''.join([testQual,lines[3].split(None)[0]])
        except IndexError:
            valueList = []
            for value in testQual: valueList.append(ord(value)) 
            valueList.sort()
            maxVal = valueList[-1]
            minVal = valueList[0]
            
            if( (minVal >= 33) and (maxVal <= 76) ):
                offset = 33
            elif( (minVal >= 54) and (maxVal <= 114) ):
                offset = 64
            else:
                raise IOError('UNKNOWN QUAL SCORE ENCODING')
            ##### 
            break 
        #####
        
        if ( sepString == None ):
            for line in lines:
                if ( line[0] == "@" ):
                    readEnd = line.strip().split(" ")[0][-2:]
                    sepString = " "
                    if ( (readEnd == "/1") or (readEnd == "/2") ): sepString = "/"
                    break
                else:
                    continue
                #####
            #####
        #####
    #####

    # USE THE CODE ABOVE TO figure out what type of PHRED score conversion to use
    charToAscii = dict( [ (chr(val),(int(val)-offset)) for val in range(0,120)] )
    intToChar   = dict( [ ((int(val)-offset),chr(val)) for val in range(33,120)] )
    #=========================================================================
    # Build the awk command needed to pull the reads
    if (sepString == " "):
        awkLine1 = 'if(NR%%4==1){gsub(/:|#|-/, \"_\", $1);Rval = \"-R\" substr($2, 1, 1); name = substr($1,2,length($1)) Rval}'%()
        
    else:
        awkLine1 = 'if(NR%%4==1){gsub(/:|#|-/, \"_\", $1);{gsub(/\//,"-R",$1)};name=substr($1,2,length($1))}'%()
    #####
    
    awkFastqCmd = "awk '{%sif(NR%%4==2){seq=$1}if(NR%%4==3){next}if(NR%%4==0){print name\" \"seq\" \"$1}}'"%(awkLine1)
    
    # Check to make sure the right vector is being used
    if ( not options.noVecCheck ): vectorCheck(readCmdList[0],awkFastqCmd,linkerDict,libType, libTypeSet, merDict, sepString, clipLength, charToAscii, pairedTypeSet,readLen, simpleScreenClass, linkerFASTA, contamFASTA) 
    
    # Opening the file to read the data
    readBroker = FASTQ_Iter(readCmdList,awkFastqCmd,allowUnpairedReads, sepString, mainCounterClass)
    
    #=========================================================================
    # Recording the commands used in the command log
    oh_cmdLog = open( '%s.cmdLOG'%libBase, 'w' )
    for i in range(len(readCmdList)):
        oh_cmdLog.write('Read in Fastq:\n%s | %s\n'%(readCmdList[i], awkFastqCmd))
    #####
    oh_cmdLog.write('Converted Fastq to Fasta Qual using PHRED %d\n'%(offset))
    oh_cmdLog.write('Clipped Reads with -f 20 and -c %d\n'%(clipLength))
    oh_cmdLog.write('Custom Vector Clipped with %s\n'%(linkerFASTA))
    if(screenSimple):oh_cmdLog.write('Lightly Screened Simple Sequence\n')
    if(screenContam):oh_cmdLog.write('Screened Contaminate with %s\n'%(contamFASTA))
    oh_cmdLog.close()

    #=========================================================================
    # Process the reads
    while(True):
        
        #=====================================================================
        # PART 1 PULLING THE READS
        if ( (options.nReads > 0) and (mainCounterClass.readsExtracted >= options.nReads) ): 
            if ( screenContam ): 
                clippedRecord = contamScreen( contamBuffer, contamCMD, mainCounterClass, allowUnpairedReads, contamFasta_oh, contamQual_oh, wholeBuff,charToAscii,intToChar, whole )
                writeFastaQual( clippedRecord, preppedFasta_oh, preppedQual_oh, mainCounterClass.increment_readsPrepped, charToAscii, intToChar )
            #####
            break
        #####
        
        try:
            records = readBroker.next()
        except StopIteration:
            if(screenContam ):
                clippedRecord = contamScreen( contamBuffer, contamCMD, mainCounterClass, allowUnpairedReads, contamFasta_oh, contamQual_oh, wholeBuff,charToAscii, intToChar,whole )
                writeFastaQual( clippedRecord, preppedFasta_oh, preppedQual_oh, mainCounterClass.increment_readsPrepped, charToAscii, intToChar )
            #####
            break
        #####
        
        #=====================================================================
        # PART 2 HIGH QUALITY SCREEN

        # Clip the reads
        clippedRecord = []
        Linearclip    = 0
        for i in range(recordNumber):
            try:
                name, seq, qual   = clippingFunc( records[i][2],records[i][1],records[i][0],vecScreen, charToAscii, libType )
            except TypeError:
                continue
            #####
            
            # Are the reads long enough
            if ( vecScreen ):
                tmpName, start, end = name.split(None)
                clippedReadLength = int( end )-int( start )
            else:
                clippedReadLength = len( seq )
            #####
            if ( clippedReadLength<clipLength ) : 
                # If it is linear keep it. The other read my by high quality
                if (isLinear):
                    Linearclip += 1
                    clippedRecord.append( [name, seq, qual] )
                continue
            
            clippedRecord.append( [name, seq, qual] )
        #####
        # Did the reads pass
        # If it is linear and both reads are LQ toss the reads
        if ((len(clippedRecord)<recordNumber)or(Linearclip == recordNumber)):
            mainCounterClass.increment_readsLostToLQ()
            continue
        #####
        #=====================================================================
        # PART 3 TRIMMING THE VECTOR
        if ( vecScreen ):
            vectorTrimmedRecord = []
            for i in range( recordNumber ): 
                try:
                    name, seq, qual, newRecords = new_custom_LFPE_clip( clippedRecord[i][0],clippedRecord[i][1],clippedRecord[i][2],libType, pairedTypeSet, merSize, clipLength, vecMerSet, records[i] )
                except TypeError:
                    continue
                #####
                if ( len( seq )<clipLength ): continue
                vectorTrimmedRecord.append( [name, seq, qual] )    
                records[i] =  newRecords
            #####
            if ( len( vectorTrimmedRecord ) < recordNumber ): 
                mainCounterClass.increment_readsLostToVector()
                continue
            #####
            clippedRecord = vectorTrimmedRecord
        #####
        
        #=====================================================================
        # PART 4 SIMPLE SEQUENCE SCREEN
        if ( screenSimple ):
            simpleRecord = []
            for i in range(recordNumber):
                name = clippedRecord[i][0]
                seq  = clippedRecord[i][1]
                qual = clippedRecord[i][2]
            
                dataScore=simpleScreenClass.computeSimpleSequenceScore( seq )
                
                if ( dataScore < simpleScoreCutoff ): simpleRecord.append( [name, seq, qual] )
            #####
            
            # If Read fails incremtn the counter and write it to the simple file
            if ( len( simpleRecord ) < 1 ):
                writeFastaQual( clippedRecord, simpleFasta_oh, simpleQual_oh, mainCounterClass.increment_readsLostToSimple, charToAscii, intToChar )                
                continue
            #####
        #####

        #=====================================================================
        # PART 5 CONTAMINATE SCREEN
        if ( screenContam ): 
            
            for i in range( recordNumber ): contamBuffer.append(clippedRecord[i])
            if (libType not in pairedTypeSet ): 
                for i in range( recordNumber ): wholeBuff.append(records[i])
            #####
            
            if ( len( contamBuffer ) >= 2500 ):
                if (isLinear ): 
                    records = contamScreen( contamBuffer, contamCMD, mainCounterClass, allowUnpairedReads, contamFasta_oh, contamQual_oh, wholeBuff,charToAscii, intToChar, whole=True )
                else:
                    clippedRecord = contamScreen( contamBuffer, contamCMD, mainCounterClass, allowUnpairedReads, contamFasta_oh, contamQual_oh, wholeBuff,charToAscii,intToChar,whole=False )
                #####
                contamBuffer  = []
                wholeBuff     = []
            else: 
                continue     
        
        #=====================================================================
        # PART 6 WRITE OUT THE READS
        
        if ( isLinear ):
            writeFastaQual( records, preppedFasta_oh, preppedQual_oh, mainCounterClass.increment_readsPrepped, charToAscii, intToChar )
        else:
            writeFastaQual( clippedRecord, preppedFasta_oh, preppedQual_oh, mainCounterClass.increment_readsPrepped, charToAscii, intToChar)
    #=========================================================================
    
    # Closing the output
    simpleFasta_oh.close()
    #simpleQual_oh.close()
    preppedFasta_oh.close()
    #preppedQual_oh.close()
    if ( screenContam ):
        contamFasta_oh.close()
    #    contamQual_oh.close()
    #####
    mainCounterClass.printReport()
    
    tmpName = 'prepped'
    if ( libType in pairedTypeSet ): tmpName = 'prepped.uncomp'
    
    printAndExec( '/mnt/local/EXBIN/clip -B 1000000 -b -c %d -f 25 -L 500 %s.%s.fastq.bz2 | /mnt/local/EXBIN/histogram_hash -g -B 500000 -m 24 -z 5g -o %s.%s.gc.histHash -'%(clipLength,libBase,tmpName,libBase,tmpName), stderr, execute=True )

    # Updating the lib.config file with the library status
    stderr.write( 'Updating the library status to PROCESSED in the lib.config file\n' )
    libFileContents = [line.split(None) for line in open(configFile)]
    oh = open( configFile, 'w' )
    for splitLine in libFileContents:
        if ( libBase == splitLine[0] ):
            stderr.write( "CONVERTED %s TO PROCESSED\n"%libBase )
            splitLine[4] = "PROCESSED"
        oh.write( '%s\n'%( '\t'.join( splitLine ) ) )
    #####
    oh.close()
    # QC the library
    if ( (options.allowUnpairedReads) ):
        # qclib3.py --file %s --output %s --report --skip 1000000 —limit 500000
        cmd1 = "/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s --output %s.R1 --report --skip 1000000 --limit 500000 --forward; /mnt/raid2/SEQ/py3/tools/qclib3.py --file %s --output %s.R2 --report --skip 1000000 --limit 500000 --reverse"%(fastqFILES[0],libBase,fastqFILES[0],libBase)
        cmd2 = "/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s.%s.fastq.bz2 --output %s.%s.R1 --report --skip 1000000 --limit 500000 --forward"%(libBase,tmpName,libBase)
    elif (len(fastqFILES)>1):
        cmd1 = "/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s --output %s_R1 --report --skip 1000000 --limit 500000;/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s --output %s_R2 --report --skip 1000000 --limit 500000"%(fastqFILES[0],libBase,fastqFILES[1],libBase)
        cmd2 = "/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s.%s.fastq.bz2 --output %s.%s_R1 --report --skip 1000000 --limit 500000 --forward; /mnt/raid2/SEQ/py3/tools/qclib3.py --file %s.%s.fastq.bz2 --output %s.%s_R2 --report --skip 1000000 --limit 500000 --reverse"%(libBase,tmpName,libBase,tmpName,libBase,tmpName,libBase,tmpName)
    else:
        cmd1 = "/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s --output %s_R1 --report --skip 1000000 --limit 500000 --forward; /mnt/raid2/SEQ/py3/tools/qclib3.py --file %s --output %s_R2 --report --skip 1000000 --limit 500000 --reverse"%(fastqFILES[0], libBase, fastqFILES[0], libBase)
        cmd2 = "/mnt/raid2/SEQ/py3/tools/qclib3.py --file %s.%s.fastq.bz2 --output %s.%s_R1 --report --skip 1000000 --limit 500000 --forward; /mnt/raid2/SEQ/py3/tools/qclib3.py --file %s.%s.fastq.bz2 --output %s.%s_R2 --report --skip 1000000 --limit 500000 --reverse"%(libBase,tmpName,libBase,tmpName,libBase,tmpName,libBase,tmpName)
    #####
    printAndExec( cmd1, stderr, execute=True )
    printAndExec( cmd2, stderr, execute=True )
    

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
    #profile(real_main)
