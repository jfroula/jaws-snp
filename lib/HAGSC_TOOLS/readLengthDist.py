#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Feb 25, 2010 11:51:26 AM$"

# Local Library Imports
from hagsc_lib import histogramClass
from hagsc_lib import iterCounter
from hagsc_lib import numRecords
from hagsc_lib import iterFASTA
from hagsc_lib import testFile
from hagsc_lib import commify

# Python Library Imports
from optparse import OptionParser

from os.path import isfile, realpath, split, splitext

from os import system

from sys import stdout

from math import ceil

from array import array

import gzip

from math import sqrt

#=========================================================================
def isGzipFile( fileName ):
    try:
        import gzip
        x = gzip.open(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False

#=========================================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE] [options]"

    parser = OptionParser(usage)

    default_maxReadLength = -1
    parser.add_option( "-L", \
                       "--maxReadLength", \
                       type    = 'int', \
                       help    = "Maximum allowed read length (bp).  Default: use all reads",
                       default = default_maxReadLength )

    parser.add_option( "-b", \
                       "--binWidth", \
                       type    = 'int', \
                       help    = "Width of the histogram bins (bp). Default: 10 bp.",
                       default = 10 )

    nReadsToSample = -1
    parser.add_option( "-n", \
                       "--nReadsToSample", \
                       type    = 'int', \
                       help    = "Number of reads to sample. Default: All Reads.",
                       default = nReadsToSample )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if ( not isfile(args[0]) ):
            parser.error( '%s can not be found'%args[0] )
        #####
        fnaFile = realpath(args[0])
        testFile(fnaFile)
    #####

    # Parsing the options
    if options.maxReadLength:
        maxReadLength = options.maxReadLength
        if ( maxReadLength > 0 ):
            readLengthScreen = lambda x: (x<=maxReadLength)
        else:
            readLengthScreen = lambda x: True
        #####
    #####

    if options.binWidth:
        if ( options.binWidth <= 0 ):
            parser.error( 'Maximum read length must be >= zero.' )
        #####
        binWidth = options.binWidth
    #####
    
    #  Is it a zipped file?
    gzipFile = isGzipFile(fnaFile)
    
    # Determining the number of records to store
    nRecords = numRecords(fnaFile, isGzipped=gzipFile )
    stdout.write( '\n=======================\n')
    stdout.write( '%s RECORDS IN %s\n'%(commify(nRecords),fnaFile))
    stdout.write( '=======================\n\n')
    
    # Initializing an array for storing the lengths
    if ( options.nReadsToSample > 0 ):
        if ( options.nReadsToSample < nRecords ):
            length_array = array( 'L', options.nReadsToSample * [0] )
        else:
            length_array = array( 'L', nRecords * [0] )
        #####
    else:
        length_array = array( 'L', nRecords * [0] )
    #####

    # Pushing the information to an underlying function
    counter    = iterCounter(100000)
    n          = 0
    if ( gzipFile ):
        fastaHandle = gzip.open(fnaFile,'r')
    else:
        fastaHandle = open(fnaFile,'r')
    #####

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = fastaHandle.readline()
        try:
            if ( line[0] == ">" ): break
        except IndexError:
            if ( line == "" ):
                raise ValueError( 'Empty line prior to first record ' + \
                                  'encountered.  Exiting.\n')
                exit()
            #####
        #####
    #####
    # Pulling the data
    x = iterCounter(1000000)
    if ( options.nReadsToSample > 0 ):
        counterTest = lambda nCounter: nCounter > options.nReadsToSample
    else:
        counterTest = lambda nCounter: False
    #####
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should " +
                              "start with '>' character" )
        #####
        # Pulling the id
        id = line[1:].split(None,1)[0]
        # Reading the sequence
        readLength = 0
        line       = fastaHandle.readline()
        while True:
            # Termination conditions
            try:
                if ( line[0] == '>' ): break
            except IndexError:
                if ( not line ): break
            #####
            # Adding a line
            readLength += len(line) - 1
            line = fastaHandle.readline()
        #####
        # Computing the length
        length_array[n] = readLength
        x()
        n += 1
        # Stopping the iteration
        if ( not line ): break
        # Stopping the sampling
        if ( counterTest(n) ): break
    #####

    #--------------------------------------------
    # Generating the histogram 
    if ( maxReadLength < 0 ): maxReadLength = max(length_array)
    nBins         = int( ceil(maxReadLength / binWidth) )
    hist          = histogramClass( 0, maxReadLength, nBins )
    map( hist.addData, filter(lambda x:(x>0 and x<=maxReadLength), length_array) )

#     #--------------------------------------------
#     # Performing the statistical analysis of the output
#     minVal, maxVal = hist.findingBounds( 0.001 )
#     minVal         = 0 if ( minVal < 100 ) else minVal
#     maxVal         = int( 100 * round( float(maxVal) / 100.0 ) ) # Rounds up to the nearest 100
    
    minVal = 0
    maxVal = maxReadLength
    
    # Creating the final histogram
    finalHist = histogramClass( minVal, maxVal, nBins )
    map( finalHist.addData, filter(lambda x:(x>=minVal and x<=maxVal), length_array) )
    stdout.write( finalHist.generateOutputString() )
    outputDict = finalHist.generateOutputDict()
    
    # Computing the final statistics
    x = []
    map( x.append, filter(lambda x:(x>=minVal and x<=maxVal), length_array) )
    sum_x  = sum( map(float,x) )
    sum_x2 = sum( map( lambda x:float(x*x), x) )
    N      = float(len(x))
    mean   = sum_x / N
    std    = sqrt( sum_x2 / N - mean * mean )
    
    # Creating the plots
    outputFileBase = splitext( split(fnaFile)[1] )[0]
    readLengthFile = '%s.readLengths'%outputFileBase
    oh = open( readLengthFile, 'w' )
    nVals = len(outputDict['midpoint'])
    for n in xrange( nVals ):
        oh.write( '%.3f\t%d\n'%( outputDict['midpoint'][n], outputDict['counts'][n] ) )
    #####
    oh.close()
    
    # Writing the GNUplot file
    plotFile = '%s.plotFile'%outputFileBase
    oh = open( plotFile, 'w' )
    oh.write( 'reset\n' )
    oh.write( "set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\n" )
    oh.write( 'set ylabel \"Counts\"\n' )
    oh.write( 'set xlabel \"Read Length (bp)\"\n' )
    oh.write( 'set nokey\n' )
    oh.write( 'set title \"%s, Length=%.1f+/-%.1f\"\n'%(outputFileBase,mean,std) )
    oh.write( 'set output \"%s.readLength.png\"\n'%outputFileBase )
    oh.write( 'set size ratio 0.50\n' )
    oh.write( 'set grid x y\n' )
    oh.write( 'set ytics\n' )
    oh.write( 'set autoscale y\n' )
    oh.write( 'plot \"%s\" using 1:2 title \"Map\" axis x1y1 with boxes\n'%readLengthFile )
    oh.close()
    
    # Making the plot
    system( 'gnuplot %s'%plotFile )

    return

if ( __name__ == '__main__' ):
    real_main()
