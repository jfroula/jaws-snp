#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Nov 3, 2010 9:50:17 AM$"

# Local Library Imports
from hagsc_lib import histogramClass, isGzipFile, isBzipFile
from hagsc_lib import fileHeader_Class

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout
from sys import stderr

from math import fmod

from time import time

import subprocess

import re

import gzip

#=========================================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA or FNA file list] [options]"

    parser = OptionParser(usage)

    parser.add_option( "-e", \
                       "--excludeReadsLessThanThis", \
                       type    = 'int', \
                       help    = "Exclude reads less than this length.  Default=Include entire read.", \
                       default = -1 )

    defaultBases = 10000000
    parser.add_option( "-b", \
                       "--initialBases", \
                       type    = 'int', \
                       help    = "Number of initial bases to examine for GC content.  Not used as a screening criteria!!  Reads can have fewer bases than this and still be used in the computation.  Default=Include entire read.", \
                       default = defaultBases )

    defaultBins = 50
    parser.add_option( "-n", \
                       "--numBins", \
                       type    = 'int', \
                       help    = "Number of bins for histogram. Default=%d"%defaultBins, \
                       default = defaultBins )
                       
    parser.add_option( "-c", \
                       "--combineCounts", \
                       action  = 'store_true', \
                       dest    = 'combineCounts', \
                       help    = "Combine all counts in the individual files:  Default=False" )
    parser.set_defaults( combineCounts = False )

    parser.add_option( "-g", \
                       "--ignoreTheseReads", \
                       type    = 'string', \
                       help    = "Ignore the reads contained in this file.  Default=Ignore no reads.", \
                       default = '' )
    
    parser.add_option( "-i", \
                       "--includeOnlyTheseReads", \
                       type    = 'string', \
                       help    = "Only include the reads contained in this file.  Default=Include all reads.", \
                       default = '' )
    parser.epilog = \
"""
Examples of use:                                                        

Compute GC on all reads:  Use Defaults                                    
Exclude <=1KB and use all of the read:  -e 1000                           
Exclude <=1KB and use first 200 BP:  -e 1000 -b 200
"""

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        fileList = []
        for i in range( len(args) ):
            # Checking for problems
            if ( not isfile(args[i]) ):
                parser.error( '%s can not be found'%args[i] )
            #####
            fileList.append( args[i] )
        #####
    #####

    # Parsing the options
    if ( options.initialBases > 0 ):
        initialBases = options.initialBases
    else:
        parser.error( 'Initial bases must be > 0' )
    #####

    # Looking for the exclusion    
    excludeReads = False
    minReadSize  = -1
    if ( options.excludeReadsLessThanThis > 0 ):
        excludeReads = True
        minReadSize  = options.excludeReadsLessThanThis
    #####
    
    # Looking at ignore reads file
    ignoreReadSet = set()
    if ( options.ignoreTheseReads != '' ):
        if ( isfile( options.ignoreTheseReads ) ):
            stderr.write( 'Reading ignore File:  %s\n'%options.ignoreTheseReads )
            for line in open( options.ignoreTheseReads, 'r' ):
                ignoreReadSet.add( line.split(None)[0] )
            #####
        else:
            parser.error( 'Ignore file not found:  %s\n'%options.ignoreTheseReads )
        #####
    #####

    # Looking at include reads
    includeSet    = set()
    useIncludeSet = False
    if ( options.includeOnlyTheseReads != '' ):
        if ( isfile( options.includeOnlyTheseReads ) ):
            stderr.write( 'Reading include File:  %s\n'%options.includeOnlyTheseReads )
            for line in open( options.includeOnlyTheseReads, 'r' ):
                includeSet.add( line.split(None)[0] )
            #####
        else:
            parser.error( 'Include file not found:  %s\n'%options.ignoreTheseReads )
        #####
        useIncludeSet = True
    #####
    
    # Regular expression for finding GC
    commify = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))')

    # Initializing the histogram    
    startTime    = time()
    gc_histClass = histogramClass( 0, 100, options.numBins )
    seq          = []
    tmpCounter   = 0
    readCutoff   = 1000
    sum_GC       = 0.0
    
    # Dictionary to store file output
    if ( not options.combineCounts ):
        fileOutputDict = {}
    #####
    
    # Compute the GC content on the underlying data
    for file_in in fileList:
    
        if ( isGzipFile(file_in) ):
            fastaFileHandle = subprocess.Popen( 'cat %s | gunzip -c'%file_in, shell=True, stdout=subprocess.PIPE ).stdout
        elif ( isBzipFile(file_in) ):
            fastaFileHandle = subprocess.Popen( 'cat %s | bunzip2 -c'%file_in, shell=True, stdout=subprocess.PIPE ).stdout
        else:
            fastaFileHandle = open( file_in, 'r' )
        #####

        # Opening a handle to the file
        stderr.write( 'Computing GC%% for scaffolds in %s\n'%file_in )
        if ( not options.combineCounts ): 
            startTime    = time()
            gc_histClass = histogramClass( 0, 100, options.numBins )
            seq          = []
            tmpCounter   = 0
            sum_GC       = 0.0
            readCutoff  = 1000
        #####
        
        # Skipping any blank lines or comments at the top of the file
        while True:
            line = fastaFileHandle.readline()
            if ( line[0] == ">" ):
                break
            #####
        #####

        # Reading the contents of the FASTA file
        while True:

            # Pulling the id
            splitText = line[1:].split(None,1)
            try:
                scaffold_id = splitText[0]
            except IndexError:
                print 'Error'
                print line
                print splitText
                raise ValueError( "gc_histogram has detected an empty scaffold ID." +
                                  " Make sure all scaffolds have IDs." )
            #####

            # Reading the sequence
            seq = []
            line  = fastaFileHandle.readline()
            while True:
                # Termination conditions
                if not line : break
                if (line[0] == '>'): break
                # Removes interal whitespace and converts to upper case
                seq.append( line.strip().upper() )
                # Reading the next line
                line = fastaFileHandle.readline()
            #####
            
            # Doing something with it
            if ( (scaffold_id in ignoreReadSet) or \
                 (useIncludeSet and (scaffold_id not in includeSet) ) ):
                if ( not line ): break
                continue
            #####

            # Checking the read size
            tmpSeq = ''.join(seq)
            nSize  = len( tmpSeq )
            if ( excludeReads and ( nSize < minReadSize ) ):
                if ( not line ): break
                continue
            #####

            # Computing the GC content
            nGC    = float( tmpSeq[:initialBases].count('G') + tmpSeq[:initialBases].count('C') )
            nAT    = float( tmpSeq[:initialBases].count('A') + tmpSeq[:initialBases].count('T') )
            nBases = nGC + nAT
            if ( nBases == 0.0 ):
                gcPer = 0.0
            else:
                gcPer = 100 * nGC /  nBases
            #####
            gc_histClass.addData(gcPer)
            
            # Providing an update to the user
            tmpCounter += 1
            if ( fmod( tmpCounter, readCutoff ) == 0.0 ): 
                tmpStr = commify.sub( ',', '%d'%(tmpCounter) )
                stderr.write( '%s reads processed\n'%tmpStr )
                if ( readCutoff < 1000000 ): readCutoff *= 10
            #####
            
            # Summing the average
            sum_GC += gcPer
            
            # Stopping the iteration
            if ( not line ): break

        #####
        
        # Output results for individual files
        if ( not options.combineCounts ):
            fileOutputDict[file_in] = { 'hist':None, \
                                        'mean_GC':None, \
                                        'totTime':None }
            fileOutputDict[file_in]['hist']      = gc_histClass.generateOutputDict()
            fileOutputDict[file_in]['mean_GC']   = sum_GC / float(tmpCounter)
            fileOutputDict[file_in]['totalTime'] = time() - startTime
        #####
        
        fastaFileHandle.close()
        
    #####

    # Output results for combined files run    
    if ( options.combineCounts ):
        stdout.write( gc_histClass.generateOutputString() )
        stdout.write( '\nAverage GC%% = %.2f\n'%( sum_GC / float(tmpCounter) ) )
        stdout.write( '\nTotal Time %.2f\n\n\n'%(time() - startTime) )
    else:
        # Forming the header string
        nFiles      = len(fileList)
        strLen      = 3 * nFiles
        columnLabel = []
        formatting  = []
        offset      = []
        for n in xrange(nFiles):
            columnLabel.append( 'Midpoint' )
            columnLabel.append( 'Counts' )
            columnLabel.append( '' )
            formatting.append( '%.2f' )
            formatting.append( '%d' )
            formatting.append( '%d' )
            offset.append( 15 )
            offset.append( 15 )
            offset.append( 25 )
        #####
        headerString = fileHeader_Class( columnLabel, \
                                                  offset).generateHeaderString()
        nBins = len( fileOutputDict[fileList[0]]['hist']['midpoint'] )
        outputString = (nBins + 4) * ['']
        for file_in in fileList:
            tmpDict = fileOutputDict[file_in]
            for n in xrange( nBins ):
                outputString[n] += ('%.2f'%tmpDict['hist']['midpoint'][n]).strip().ljust(15)
                outputString[n] += ('%d'%tmpDict['hist']['counts'][n]).strip().ljust(15)
                outputString[n] += (tmpDict['hist']['hist'][n]).strip().ljust(25)
            #####
            outputString[nBins]   += ''.join( [ ('Total:').ljust(15), \
                                                ('%d'%sum(tmpDict['hist']['counts'])).ljust(40)] )
            outputString[nBins+1] += ( file_in ).ljust(55)
            outputString[nBins+2] += ( 'Average GC%% = %.2f'%tmpDict['mean_GC'] ).ljust(55)
            outputString[nBins+3] += ( 'Total Time %.2f'%tmpDict['totalTime'] ).ljust(55)
        #####
        
        # Output to the user
        stdout.write( headerString )
        for n in xrange(nBins):
            stdout.write( outputString[n] + '\n' )
        #####
        stdout.write( '\n' + outputString[nBins] + '\n\n' )
        stdout.write( outputString[nBins+1] + '\n' )
        stdout.write( outputString[nBins+2] + '\n' )
        stdout.write( outputString[nBins+3] + '\n\n\n' )
        
    #####
    return
    
#####

def profile_main():
    from cProfile import Profile
    from pstats import Stats
    prof  = Profile().runctx("real_main()", globals(), locals())
    stats = Stats( prof ).sort_stats("time").print_stats(20)
    return
#####

if ( __name__ == '__main__' ):
    real_main()


