#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Nov 8, 2010 9:50:17 AM$"

# Python Library Imports
from optparse import OptionParser

from hagsc_lib import generateTmpDirName
from hagsc_lib import testDirectory
from hagsc_lib import remoteServer4
from hagsc_lib import queueingJobs
from hagsc_lib import iterCounter
from hagsc_lib import deleteFile
from hagsc_lib import deleteDir

from math import fmod
from math import pow

from sys import stderr
from sys import stdout

from os.path import isfile
from os.path import abspath
from os.path import join
from os.path import realpath

from os import curdir
from os import mkdir
from os import chdir

from os import uname

import subprocess

import re

def real_main():

    # Defining the program options
    usage = "usage: %prog [readsFile] [lowerKmer] [upperKmer] [options]"

    parser = OptionParser(usage)

    defVal = '2g'
    parser.add_option( "-z", \
                       "--hashSize", \
                       type    = "string", \
                       help    = "number of possible n-mers to allocate memory for (k, m, or g may be suffixed):  Default:  %s"%defVal, \
                       default = '%s'%defVal )

    parser.add_option( "-m", \
                       "--merLength", \
                       type    = 'int', \
                       help    = "Mer length.  Default: 24", \
                       default = 24 )

    defVal = 95.0
    parser.add_option( "-p", \
                       "--maskPercent", \
                       type    = 'float', \
                       help    = "Masked percentage for read.  Default: %.1f"%defVal, \
                       default = defVal )

    parser.add_option( "-T", \
                       "--stripFirstPartOfTraceID", \
                       action  = 'store_true', \
                       dest    = 'stripFirstPartOfTraceID', \
                       help    = "Strip first part of trace ID.  Default: False" )
    parser.set_defaults( stripFirstPartOfTraceID = False )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    basePath = abspath(curdir)
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        readsFile = realpath(args[0])
        if ( not isfile(readsFile ) ): parser.error( 'File:  %s is not found.'%args[0] )
        # Pulling the lowerKmer
        lowerKmer = int( args[1] )
        if ( lowerKmer < 1 ): parser.error( 'lowerKmer must be >= 1:  %s'%args[1] )
        # Pulling the upperKmer
        upperKmer = int( args[2] )
        if ( upperKmer < 1 ): parser.error( 'upperKmer must be > 1:  %s'%args[2] )
        # Sanity check
        if ( upperKmer < lowerKmer ): parser.error( 'upperKmer must be >= lowerKmer' )
    #####
    
    # Check the machine name to make sure we are running in the proper location
    if ( uname()[1] in set(['hero','sidekick']) ):
        parser.error( 'You have requested a large job be ran on %s.\n\nTHIS IS NOT ADVISABLE!\n\nRun it on the cluster using the -L option.'%uname()[1] )
    #####

    # Pulling the hash size    
    hashSize = options.hashSize
    
    if ( options.merLength > 0 ):
        merLength = options.merLength
    else:
        parser.error( 'merLength must be > 0' )
    #####

    if ( options.maskPercent > 0 ):
        maskCutoff = options.maskPercent / 100.0
    else:
        parser.error( 'maskPercent must be > 0' )
    #####
        
    # Performing mask_repeats_hash to mask reads based on kmer slice
    maskHashCommand = '/mnt/local/EXBIN/mask_repeats_hash'
    maskOptions     = []
    if ( options.stripFirstPartOfTraceID ):
        maskOptions.append( '-T' )
    #####
    maskOptions.append( '-m %d'%merLength )
    maskOptions.append( '-z %s'%hashSize )
    maskOptions.append( '-Z' )
    maskOptions.append( '-B 1000000' )
    maskOptions.append( '-t %d'%lowerKmer )
    maskOptions.append( '-u %d'%upperKmer )
    cmdString = '%s %s %s'%( maskHashCommand, ' '.join(maskOptions), readsFile )
    stderr.write( "\t%s\n"%cmdString )

    # Running the command and piping the results back into the script
    stderr.write( '\n\t-Pulling reads.\n')
    tmpProcess = subprocess.Popen( cmdString, shell=True, stdout=subprocess.PIPE)
    inputPipe = tmpProcess.stdout
    
    # Skipping any blank lines or comments at the top of the file
    while True:
        line = inputPipe.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
    #####

    # Pulling the data
    while True:
        if ( line[0] != ">" ): raise ValueError( "Records in Fasta files should start with '>' character" )
        # Pulling the id
        id = line[1:].split(None,1)[0]
        # Reading the sequence
        tmpData = []
        line    = inputPipe.readline()
        while True:
            # Termination conditions
            if ( (not line) or (line[0] == '>') ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = inputPipe.readline()
        #####
        # Analyze the sequence    
        seq            = ''.join(tmpData)
        fractionMasked = float( seq.count('X') ) / float( len(seq) - seq.count('N') )    
        if ( fractionMasked >= maskCutoff ): stdout.write( '%s\n'%(id) )
        # Stopping the iteration
        if ( not line ): break
    #####
    
    tmpProcess.kill()
    
    return

#####
    
if ( __name__ == '__main__' ):
    real_main()
