__author__="jjenkins"
__date__ ="$Nov 18, 2010 10:37:47 PM$"

from hagsc_lib import generateTmpDirName
#from hagsc_lib import write_qual_record
from hagsc_lib import FASTAFile_dict
from hagsc_lib import histogramClass
from hagsc_lib import remoteServer4
from hagsc_lib import testDirectory
from hagsc_lib import deleteFile
from hagsc_lib import writeFASTA
from hagsc_lib import blatObject
from hagsc_lib import writeFASTA
from hagsc_lib import iterFASTA
from hagsc_lib import objectify
from hagsc_lib import deleteDir
from hagsc_lib import iterBLAT

from random import choice

from os.path import abspath
from os.path import join

from os import curdir
from os import mkdir
from os import system

from sys import stdout

import gzip

from math import fmod

########################################################################
class scaffoldClass( object ):

    def __init__(self, scaffID, scaffSize):

        # Initializing the sequence and clone coverage dictionary
        self.scaffID   = scaffID
        self.scaffSize = scaffSize

        # Histogram
        kmer = self.scaffSize / 50
        if kmer > 1000: kmer = 1000
        bins = int( float(self.scaffSize) / float(kmer) )
        if bins < 20: bins = 20
        if bins > 100: bins = 100
        self.hPairs = histogramClass(1,self.scaffSize,bins)

    def build_record(self, id, data="", info=""):
        return objectify({"id":id, "info":info, "data":(lambda : data)})

    def _cloneExtent(self, record1, record2):
        if record1.T_start < record2.T_start:
            return record1.T_start, record2.T_end
        else:
            return record2.T_start, record1.T_end
        #####

    def addPair(self, record_1, record_2 ):
        # Adding the reads to the histogram
        self.hPairs.addData( record_1.T_start )
#        self.hPairs.addData( record_1.T_end )
        self.hPairs.addData( record_2.T_start )
#        self.hPairs.addData( record_2.T_end )
        return

    def getPairHIST(self):
        return self.hPairs.generateOutputString()
#####

########################################################################
class blat_parseClass( object ):

    def __init__(self, blatScore, blatClass):
        self.numHits      = 1
        self.hits         = [ (blatScore, blatClass) ]
        self.maxBLATScore = blatScore

    def addHit(self, blatScore, blatClass):
        if ( blatScore < self.maxBLATScore ): return

        if ( blatScore > self.maxBLATScore ):
            self.hits         = [ (blatScore, blatClass) ]
            self.maxBLATScore = blatScore
        else:
            self.hits.append( (blatScore,blatClass) )
        #####
        return

########################################################################
class illumina_clone_analysis(object):

    def __init__(self, readFile_1, \
                       readFile_2, \
                       clonesFile, \
                       clusterNum=101,\
                       jobQueueSize = 10, \
                       pairsPerFile = 500000, \
                       outputFileName = 'readPairHit_POOL1.out',\
                       tmpDirNum = None ):

        # Getting the information from the user
        self.readFile_1     = readFile_1
        self.readFile_2     = readFile_2
        self.clonesFile     = clonesFile
        self.clusterNum     = clusterNum
        self.jobQueueSize   = jobQueueSize
        self.pairsPerFile   = pairsPerFile
        self.outputFileName = outputFileName
        self.tmpDirNum      = tmpDirNum

        # Setting base path
        self.basePath = abspath( curdir )

        # setting tmpDirNum
        if ( tmpDirNum == None ):
            self.tmpDir = generateTmpDirName()
        else:
            self.tmpDir = generateTmpDirName( tmpDirNum )
        #####
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, 'readPairHit' )

        # Generating the main readHit perl file
        self.perlFile = join( self.tmpPath, 'readPairHitJob.pl' )

        # Individual clone output files dictionary
        self.cloneOutputFiles   = {}
        self.kmerHistFASTAFiles = {}

        # Echoing the options to the user
        optionsList     = 12 * ['']
        optionsList[0]  = '---------------------\n'
        optionsList[1]  = 'readPairHit Settings:\n'
        optionsList[2]  = '---------------------\n'
        optionsList[3]  = '\t-CLONE FILE           = %s\n'%self.clonesFile
        optionsList[4]  = '\t-READ FILE #1         = %s\n'%self.readFile_1
        optionsList[5]  = '\t-READ FILE #2         = %s\n'%self.readFile_2
        optionsList[7]  = '\t-JOB QUEUE SIZE       = %d\n'%self.jobQueueSize
        optionsList[8]  = '\t-PAIRS PER FILE       = %s\n'%self.pairsPerFile
        optionsList[9]  = '\t-OUTPUT FILE NAME     = %s\n'%self.outputFileName
        optionsList[10] = '\t-TEMP DIR NUMBER      = %s\n'%str(tmpDirNum)
        optionsList[11] = '--------------------\n'
        stdout.write( ''.join(optionsList) )

        # Setting the controlling parameters for blatting
        self.oocFile     = join( self.tmpPath, '11.ooc' )
        self.oocPerlFile = join( self.tmpPath, 'oocBlat.pl' )
        self.oocKeyWords  = { 'makeOveruseTileFile':self.oocFile }
        self.defaultBlatKeyWords = { 'overuseTileFile' : self.oocFile, \
                                     'extendThrough_N' : None}

        # Sorted placements dictionary
        self.sortedPlacements = {}

        # Main output file
        self.mainOutputHandle = open( self.outputFileName, 'w')

        # Running the code
        self.pairReadHit()
        
    def pairReadHit(self):
        # Running the cases
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Create the ooc file
        self.create_ooc()
        # Step 3:  BLAT Read Pairs
        self.blatReadPairs()
        # Step 4:  Cleanup
        self.clean_tmp()
        # Step 5:  Close output file handle
        self.mainOutputHandle.close()
        return

    def create_tmp_dir(self):
        stdout.write( '\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def create_ooc(self):
        stdout.write( '\t-Creating occ file\n')
        deleteFile(self.oocFile)
        commandList = [ blatObject( self.clonesFile, \
                           self.clonesFile ).oocCommandString(self.oocKeyWords)]
        remoteServer4( commandList, 1, clusterNum=self.clusterNum, \
                                             perlFile = self.oocPerlFile, \
                                          perlCommand = '#!/apps/bin/perl -w\n')
        deleteFile(self.oocPerlFile)
        return

    def pairTest(self, plac_1, plac_2):
        # Checking for same scaffold and different direction
        if ( (plac_1.T_name == plac_2.T_name) and \
             (plac_1.strand != plac_2.strand) ):
            # Checking to see who is on the left
            delta = plac_1.T_start - plac_2.T_start
            # Do not allow more than a 4KB insert size
            if ( abs(delta) >= 4000 ): return False
            if ( delta > 0 ):
                if ( plac_2.strand == '+' ):
                    return True
                else:
                    return False
                #####
            elif ( delta < 0 ):
                if ( plac_1.strand == '+' ):
                    return True
                else:
                    return False
                #####
            else:
                # delta==0
                return False
            #####
        else:
            return False
        #####

    def analyzeBLATJobs(self, blatOutputFiles):

        # Performing the analysis
        for blatOutputFile, tmpFASTAFile in blatOutputFiles:

            stdout.write( '\t\t-Analyzing %s\n'%blatOutputFile )

            # (1)  Finding all highest scoring placements
            maxFields = None
            maxFields = {}
            for blatClass in iterBLAT( gzip.open(blatOutputFile, 'r') ):
                # Reading query and target names
                queryName = blatClass.Q_name
                # Computing the alignment score
                blatScore = blatClass.match + blatClass.repMatch

                # Skip if query and target sizes are the same,
                # and match is the size of the read, indicating
                # different names, but same read
                try:
                    maxFields[queryName].addHit(blatScore,blatClass)
                except KeyError:
                    maxFields[queryName] = blat_parseClass(blatScore,blatClass)
                #####
            #####

            # (2)  Parse the reads to find pairs
            parsedReads = None
            parsedReads = {}
            for readName in maxFields.iterkeys():
                readBaseName = readName.split('/')[0]
                try:
                    parsedReads[readBaseName].append( readName )
                except KeyError:
                    parsedReads[readBaseName] = [readName]
                #####
            #####

            # Indexing the associated FASTA file
            tmpIndex = FASTAFile_dict( tmpFASTAFile )

            # (3) Find all feasible pairs and output to a file
            for readBaseName, readList in parsedReads.iteritems():

                # Do we have a pair?
                if ( len(readList) == 2 ):

                    # Loop over all possible pairs for each readBaseName
                    allPossiblePairs = []
                    for tmp_1_score, tmp_1_plac in maxFields[readList[0]].hits:
                        for tmp_2_score, tmp_2_plac in maxFields[readList[1]].hits:
                            if ( self.pairTest(tmp_1_plac, tmp_2_plac) ):
                                allPossiblePairs.append( (tmp_1_plac, tmp_2_plac) )
                            #####
                        #####
                    #####

                    if ( allPossiblePairs == [] ): continue

                    # Randomly select a pair from the set to include
                    chosenPair = choice( allPossiblePairs )

                    # Write to main output file
                    self.mainOutputHandle.write( self.pairString( chosenPair[0], \
                                                                  chosenPair[1] ) )

                    # Update pair coverage on clones
                    scaffID   = chosenPair[0].T_name
                    scaffSize = chosenPair[0].T_size
                    try:
                        self.sortedPlacements[scaffID].addPair( chosenPair[0], \
                                                                chosenPair[1] )
                    except KeyError:
                        self.sortedPlacements[scaffID] = scaffoldClass( scaffID, \
                                                                        scaffSize )
                        self.sortedPlacements[scaffID].addPair( chosenPair[0], \
                                                                chosenPair[1] )
                    #####

                    # Writing to individual clone output file of read names
                    try:
                        self.cloneOutputFiles[scaffID].write( '%s\n'%chosenPair[0].Q_name )
                        self.cloneOutputFiles[scaffID].write( '%s\n'%chosenPair[1].Q_name )
                    except KeyError:
                        self.cloneOutputFiles[scaffID] = open( join( self.basePath,\
                                               'cloneOutputFile.%s.dat'%scaffID ), \
                                                                               'w' )
                    #####

                    # Writing the reads to the appropriate temporary fasta file
                    try:
                        writeFASTA( [tmpIndex[chosenPair[0].Q_name]], \
                                              self.kmerHistFASTAFiles[scaffID] )
                        writeFASTA( [tmpIndex[chosenPair[1].Q_name]], \
                                              self.kmerHistFASTAFiles[scaffID] )
                    except KeyError:
                        self.kmerHistFASTAFiles[scaffID] = open( \
                       join( self.tmpPath,'cloneFASTA.%s.fasta'%scaffID ), 'w' )
                    #####
                #####
            #####

        #####

        return


    def pairString(self, record_1, record_2):
        return '%s\t%d\t%s\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%s\n'%( \
                                                      record_1.T_name, \
                                                      record_1.T_size, \
                                                      record_1.Q_name, \
                                                      record_1.Q_size, \
                                                      record_1.T_start, \
                                                      record_1.T_end, \
                                                      record_1.strand, \
                                                      record_2.Q_name, \
                                                      record_2.Q_size, \
                                                      record_2.T_start, \
                                                      record_2.T_end, \
                                                      record_2.strand )

    def blatReadPairs(self):

        # Getting the file iterators
        stdout.write( '\t-Iterating over %s\n'%self.readFile_1)
        readIter_A = iterFASTA( open( self.readFile_1, 'r' ) )
        stdout.write( '\t-Iterating over %s\n'%self.readFile_2)
        readIter_B = iterFASTA( open( self.readFile_2, 'r' ) )

        # Loading default BLAT keywords
        finalKeyWords = {}
        for key,value in  self.defaultBlatKeyWords.items():
            finalKeyWords[key] = value
        #####

        # Setting up the pairs
        unpairedReads = {}

        # Queueing up the jobs
        nPairs = 0
        nFiles = 1
        basePairName   = join( self.tmpPath, 'readPairFile' )
        tmpFASTAFile   = '%s_%d.fasta'%(basePairName,nFiles)
        stdout.write( '\t\t-Writing %s\n'%tmpFASTAFile )
        blatOutputFile  = '%s.blat'%tmpFASTAFile
        blatOutputFiles = [ ('%s.gz'%blatOutputFile, tmpFASTAFile)]
        commandList     = []
        commandList.append( blatObject(tmpFASTAFile, self.clonesFile, \
                                  blatOutputFile).commandString(finalKeyWords) )
        tmpOutputHandle = open( tmpFASTAFile, 'w' )

        # Looping over the iterators
        while ( True ):

            # Reading from the first file and looking for a match
            try:
                tmpRecord_A_1   = readIter_A.next()
            except StopIteration:
                break
            #####
            canonicalName_A = tmpRecord_A_1.id.split('/')[0]
            havePair_A = False
            try:
                tmpRecord_A_2 = unpairedReads[canonicalName_A]
                havePair_A    = True
            except KeyError:
                # Add the name to the unpaired list and continue
                unpairedReads[canonicalName_A] = tmpRecord_A_1
            #####

            # Reading from the second file and looking for a match
            try:
                tmpRecord_B_1   = readIter_B.next()
            except StopIteration:
                break
            #####
            canonicalName_B = tmpRecord_B_1.id.split('/')[0]
            havePair_B = False
            try:
                tmpRecord_B_2 = unpairedReads[canonicalName_B]
                havePair_B    = True
            except KeyError:
                # Add the name to the unpaired list and continue
                unpairedReads[canonicalName_B] = tmpRecord_B_1
            #####

            # Add the next read pair to a file
            if ( havePair_A ):
                writeFASTA( [tmpRecord_A_1], tmpOutputHandle  )
                writeFASTA( [tmpRecord_A_2], tmpOutputHandle  )
                nPairs += 1
                unpairedReads.pop(canonicalName_A)
            #####

            if ( havePair_B ):
                writeFASTA( [tmpRecord_B_1], tmpOutputHandle  )
                writeFASTA( [tmpRecord_B_2], tmpOutputHandle  )
                nPairs += 1
                unpairedReads.pop(canonicalName_B)
            #####

            # If we hit our limit, then start a new file
            if ( fmod( nPairs, self.pairsPerFile ) == 0.0 ):

                # Close the old file
                tmpOutputHandle.close()

                # Check to see if we need to run the blat jobs
                if ( fmod( nFiles, self.jobQueueSize ) == 0.0 ):

                    # Run the blat jobs
                    remoteServer4( commandList, self.jobQueueSize, \
                                                 clusterNum = self.clusterNum, \
                                                   perlFile = self.perlFile, \
                                         perlCommand = '#!/apps/bin/perl -w\n' )

                    # Analyze blat jobs
                    self.analyzeBLATJobs( blatOutputFiles )

                    # Reset the counters
                    commandList     = []
                    blatOutputFiles = []
                    nFiles          = 0

                #####

                # Increment the counter
                nFiles += 1
                tmpFASTAFile   = '%s_%d.fasta'%(basePairName,nFiles)
                stdout.write( '\t\t-Writing %s\n'%tmpFASTAFile )
                blatOutputFile = '%s.blat'%tmpFASTAFile
                blatOutputFiles.append( ('%s.gz'%blatOutputFile, tmpFASTAFile) )
                commandList.append( blatObject(tmpFASTAFile, self.clonesFile, \
                                  blatOutputFile).commandString(finalKeyWords) )
                tmpOutputHandle = open( tmpFASTAFile, 'w' )

                # Restart the pair counter
                nPairs = 0

            #####

        #####

        # Running any leftover cases
        if ( commandList != [] ):
            # Run the blat jobs
            remoteServer4( commandList, self.jobQueueSize, \
                                                clusterNum = self.clusterNum, \
                                                  perlFile = self.perlFile, \
                                    perlCommand = '#!/apps/bin/perl -w\n' )
            # Analyze the blat jobs
            self.analyzeBLATJobs(blatOutputFiles)
        #####

        # Deleting the perl file
        deleteFile(self.perlFile)

        # Output the pair coverage as a QUAL and HiST files
        stdout.write( '\n\t-Outputting pair coverage in QUAL and HIST format...\n')
        hist_outputHandle = open( 'clonePairCoverage.hist', 'w' )
        for scaffID in self.sortedPlacements.iterkeys():
            stdout.write( '\t-%s\n'%scaffID )
            # Writing the histogram
            try:
                hist_outputHandle.write( '-------------------\n')
                hist_outputHandle.write( '%s\n'%scaffID)
                hist_outputHandle.write( '-------------------\n')
                hist_outputHandle.write( self.sortedPlacements[scaffID].getPairHIST() )
            except KeyError:
                continue
            #####
        #####
        hist_outputHandle.close()

        # Closing the individual clone files
        for scaffID in self.cloneOutputFiles.iterkeys():
            self.cloneOutputFiles[scaffID].close()
        #####

        # Generating the kmer histograms
        stdout.write( '\t- Generating the kmer histograms\n')
        for scaffID in self.kmerHistFASTAFiles.iterkeys():
            self.kmerHistFASTAFiles[scaffID].close()
            # Generate kmer histogram
            tmpFile_1 = join( self.basePath, 'kmerDist.%s.out'%scaffID )
            tmpFile_2 = self.kmerHistFASTAFiles[scaffID].name
            cmd = []
            cmd.append( '/mnt/local/EXBIN/histogram_hash' )
            cmd.append( '-m 24 -z 500m -o %s %s'%(tmpFile_1, tmpFile_2) )
            system( ' '.join(cmd) )
        #####
        return

    def clean_tmp(self):
        stdout.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

#####

########################################################################
if ( __name__ == '__main__' ):

    readFile_1 = '/mnt/raid3/disk23/Illuminia_PROC_DATA/100513_HWUSI-EAS1668_0006_VER238-242-247-250_UHL-pools/s_6_1_sequence.seq'
    readFile_2 = '/mnt/raid3/disk23/Illuminia_PROC_DATA/100513_HWUSI-EAS1668_0006_VER238-242-247-250_UHL-pools/s_6_2_sequence.seq'
    clonesFile = '/home/mqualls/aip/pool1/POOL1_contigs.fasta'

    # Testing DNA_read_hit
    illumina_clone_analysis( readFile_1, \
                             readFile_2, \
                             clonesFile, \
                             clusterNum=101,\
                             jobQueueSize = 20, \
                             pairsPerFile = 500000, \
                             outputFileName='readPairHit_POOL1.out',\
                             tmpDirNum =101010 )

#####
