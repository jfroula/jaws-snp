#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$May 16, 2010 10:37:17 PM$"

# Local Library Imports
from hagsc_lib import alternativeHaplotypeFinder
from hagsc_lib import testFile

# Python Library Imports
from optparse import OptionParser

# Test comment

from os.path import isfile

def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE] [cutoffSize] [options]"

    parser = OptionParser(usage)

    defaultQueueSize = 16
    parser.add_option( "-p", \
                       "--queueSize", \
                       type    = 'int', \
                       help    = "bestHit number of CPUs" + \
                                 "Default: %d"%defaultQueueSize, \
                       default = defaultQueueSize )

    defaultCluster = 101
    parser.add_option( "-c", \
                       "--clusterNum", \
                       type    = 'string', \
                       help    = "Cluster number" + \
                                 "Default: %d"%defaultCluster, \
                       default = 101 )

    defaultIDCutoff = 95.0
    parser.add_option( "-i", \
                       "--ID_cutoff", \
                       type    = 'float', \
                       help    = "Cutoff (%) for placement identity." + \
                                 "Default: %.1f"%defaultIDCutoff, \
                       default = defaultIDCutoff )

    defaultCovCutoff = 95.0
    parser.add_option( "-v", \
                       "--coverageCutoff", \
                       type    = 'float', \
                       help    = "Cutoff (%) for read coverage." + \
                                 "Default: %.1f"%defaultCovCutoff, \
                       default = defaultCovCutoff )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        if ( not isfile(args[0]) ):
            parser.error( 'File not found:  %s'%args[0] )
        #####
        fasta_file_in = args[0]

        if ( int(args[1]) < 0 ):
            parser.error( 'Scaffold cutoff must be greater than zero:  %d'%int(args[1]))
        #####
        cutoffScaffoldSize = int(args[1])
    #####

    # Parsing the options
    if ( options.queueSize >= 0):
        queueSize_in = options.queueSize
    else:
        parser.error( 'Queue size must be >= 0' )
    #####

    clusterNum_in = options.clusterNum

    if ( options.coverageCutoff >= 0):
        coverageCutoff_in = options.coverageCutoff
    else:
        parser.error( 'Coverage (%) cutoff must be >= zero.' )
    #####

    if ( options.ID_cutoff >= 0):
        ID_cutoff_in = options.ID_cutoff
    else:
        parser.error( 'Identity (%) cutoff must be >= zero.' )
    #####

    # Pushing the information to an underlying function
    alternativeHaplotypeFinder( fasta_file_in, \
                                cutoffScaffoldSize, \
                                queueSize  = queueSize_in, \
                                clusterNum = clusterNum_in, \
                                covCutoff  = coverageCutoff_in, \
                                ID_cutoff  = ID_cutoff_in  \
                               ).findAltHapScaffolds()
    return

if ( __name__ == '__main__' ):
    real_main()
