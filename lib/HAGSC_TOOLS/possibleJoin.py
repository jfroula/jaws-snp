#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Feb 23, 2010 9:46:44 AM$"

# Local Library Imports
from hagsc_lib import possibleJoin_class
from hagsc_lib import testFile
from hagsc_lib import defaultCluster

# Python Library Imports
from optparse import OptionParser

from os.path import realpath
from os.path import isfile

def real_main():

    # Defining the program options
    usage = "usage: %prog [ASSEMBLY] [BES/FES FILE] [options]"

    parser = OptionParser(usage)

    defaultOutputFile = 'possibleJoinOutput.out'
    parser.add_option( "-o", \
                       "--outputFile", \
                       type    = 'string', \
                       help    = "Output file name:  Default possibleJoinOutput.out", \
                       default = defaultOutputFile )

    parser.add_option( "-c", \
                       "--cluster_Num", \
                       type    = 'int', \
                       help    = "Cluster job will be submitted to: " + \
                                 "Default %d."%defaultCluster,
                       default = defaultCluster )

    parser.add_option( "-t", \
                       "--trim_Length", \
                       type    = 'int', \
                       help    = "Length to trim from each end of the " + \
                                 "scaffolds (kbp):  Default 140, Maximum 1000.",
                       default = 140 )

    parser.add_option( "-v", \
                       "--coverage_Cutoff", \
                       type    = 'float', \
                       help    = "Minimum Coverage % from DNA_best_hit: " + \
                                 "Default 0.0",
                       default = 0.0 )

    parser.add_option( "-i", \
                       "--identity_Cutoff", \
                       type    = 'float', \
                       help    = "Minimum ID % from DNA_best_hit: " + \
                                 "Default 90.0",
                       default = 90.0 )

    parser.add_option( "-p", \
                       "--queueSize", \
                       type    = 'int', \
                       help    = "Number of jobs:  Default 16.",
                       default = 16 )

    parser.add_option( "-d", \
                       "--debugMode", \
                       action  = 'store_true', \
                       dest    = 'debugMode', \
                       help    = "Does not run best_hit.pl:  Default=False" )
    parser.set_defaults( debugMode = False )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        for n in xrange(2):
            if (not isfile(args[n])):
                parser.error( '%s can not be found'%args[n] )
            #####
        #####
        assemblyFile = realpath( args[0] )
        BES_FES_File = realpath( args[1] )

        testFile( assemblyFile )
        testFile( BES_FES_File )
    #####

    # Parsing the options
    if ( options.trim_Length > 1000 ):
        parser.error( "Trim length in units of kbp, and the " + \
                      "maximum trim length is 1000 kbp." )
    #####
    trimLength = 1000 * options.trim_Length

    # Pushing the information to an underlying function
    possibleJoin_class( assemblyFile, \
                        BES_FES_File, \
                        options.outputFile, \
                        trimLength, \
                        options.coverage_Cutoff, \
                        options.identity_Cutoff, \
                        options.debugMode, \
                        options.cluster_Num, \
                        options.queueSize ).performJoin( keyWords={'maskType':'lower','minScore':100} )
    return

if ( __name__ == '__main__' ):
    real_main()


