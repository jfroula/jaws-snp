#!/usr/common/usg/languages/python/2.7-anaconda/bin/python3
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hapipe.format import sam
from collections import defaultdict

from os.path import join, abspath, curdir, split, splitext, isfile, isdir
from os.path import realpath

from os import getpid, uname, chdir, mkdir, chmod, system

from sys import stderr, stdout

from shutil import rmtree

from math import fmod

from optparse import OptionParser

import re
commify_re = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub

import gzip

#=========================================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True

#=========================================================================
def isBzipFile(fileName):
    try:
        import bz2
        x = bz2.BZ2FILE(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False

#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.open(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False

#==============================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )

#==============================================================
def baseFileName(fileName):
    return splitext(split(fileName)[1])[0]

#==============================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True

#==============================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )

#==============================================================
def iterCounter( maxValue ):
    x = newIterCounter(maxValue)
    return lambda : next(x)

#==============================================================
def newIterCounter( maxValue ):
    outputText = '%s reads processed\n'
    tmpCounter = 1
    readCutoff = 10
    while (True ):
        tmpCounter += 1
        if ( not int(fmod( tmpCounter, readCutoff )) ):
            stderr.write( outputText%commify( tmpCounter ) )
            readCutoff = min(10 * readCutoff, maxValue )
        #####
        yield
    #####
    return

#==============================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True

#==============================================================
def create_tmp_dir( tmpPath, deleteOld=True ):
    stderr.write( '\n\t-Creating tmp directory\n\n')
    # If the directory exists, delete it and make another one
    try:
        testDirectory(tmpPath)
        if deleteOld:
            deleteDir(tmpPath)
            mkdir( tmpPath, 0o777 )
            chmod(tmpPath, 0o777 )
            testDirectory(tmpPath)
            return
        else:
            return
        #####
    except IOError:
        mkdir( tmpPath, 0o777 )
        chmod(tmpPath, 0o777 )
        testDirectory(tmpPath)
        return
    #####

#==============================================================
def clean_tmp(tmpPath):
    stderr.write( '\t-Removing Temporary directory\n\n')
    deleteDir(tmpPath)
    return

#==============================================================
def iterItems( tmpHandle ):
    # Skipping any blank lines or comments at the top of the file
    while True:
        line = tmpHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ):
            break
        #####
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        id = line[1:].split(None,1)[0]
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or (line[0] == '>') ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield {'id':id, 'data':tmpData}
        # Stopping the iteration
        if ( not line ):
            return
        ####
    #####
    assert False, 'Should never reach this line!!'

#==============================================================
def writeITEM( record, sepString, outputHandle_1, outputHandle_2):
    # Fixing the name
    baseName, readExt = record['id'].split(sepString)
    # Writing the record
    if ( readExt == '1' ):
        outputHandle_1.write( '>%s%s1\n%s\n'%(baseName, sepString, ''.join(record['data']) ) )
    elif ( readExt == '2' ):
        outputHandle_2.write( '>%s%s2\n%s\n'%(baseName, sepString, ''.join(record['data']) ) )
    else:
        print("NOT ABLE TO FIND SEPARATOR STRING FOR READS\nreadID = %s\n=============\n"%record['id'])
        assert False
    #####
    return

#==============================================================
def split_SEQ_and_QUAL( queryFASTA, queryQUAL, reads_1_base, reads_2_base ):

    # Input iterators
    fastaITER = iterItems( open(queryFASTA) )
    useQUAL = False
    if ( queryQUAL != None ):
        qualITER  = iterItems( open(queryQUAL) )
        useQUAL = True
    #####

    # Output files
    outputFASTA_1 = open( '%s.seq'%reads_1_base, 'w' )
    outputFASTA_2 = open( '%s.seq'%reads_2_base, 'w' )

    if useQUAL: 
        outputQUAL_1  = open( '%s.seq.qual'%reads_1_base, 'w' )
        outputQUAL_2  = open( '%s.seq.qual'%reads_2_base, 'w' )
    #####
    
    # Determine the separator string
    fastaRecord = next(fastaITER)
    if ( fastaRecord['id'].count('/') > 0 ):
        sepString = '/'
    elif ( fastaRecord['id'].count('-R') > 0 ):
        sepString = '-R'
    else:
        print("NOT ABLE TO DETERMINE SEPARATOR STRING FOR READS\nreadID = %s\n=============\n"%fastaRecord['id'])
        assert False
    #####

    # Write the items to a file    
    writeITEM( fastaRecord, sepString, outputFASTA_1, outputFASTA_2 )
    if ( useQUAL ):
        qualRecord  = next(qualITER)
        writeITEM( qualRecord,  sepString, outputQUAL_1,  outputQUAL_2 )
    #####
    
    # Iterating over the reads    
    print("SPLITTING FASTA AND QUAL FILE")
    x = iterCounter(1000000)
    while ( True ):
        x()
        # Reading in a FASTA record
        try:
            writeITEM( next(fastaITER), sepString, outputFASTA_1, outputFASTA_2 )
        except StopIteration:
            pass
        #####
        if ( useQUAL ):
            # Reading in a QUAL record
            try:
                writeITEM( next(qualITER), sepString, outputQUAL_1, outputQUAL_2 )
            except StopIteration:
                break
            #####
        #####
    #####
    outputFASTA_1.close()
    outputFASTA_2.close()
    if useQUAL:
        outputQUAL_1.close()
        outputQUAL_2.close()
    #####

#==============================================================
def SAM_identity( record ):
    ID              = 100.0*sam.identity(record)
    match, mismatch = sam.mismatch_count( record )
    qlen            = int( 100.0 * match / ID )
    mismatch        = int(record[4].split(":")[2]) # Mismatches come off of the "XM" Tag
    return 100.0 * float( qlen - mismatch ) / float(qlen)
#    return 100.0*sam.identity(record)

#==============================================================
def SAM_coverage( record ):
    return 100.0*sam.coverage(record)

#==============================================================
def SAM_to_bestHit( samRecord, targetSizes ):

    # Computing identity, query length, and number of matches
    match, mismatch = sam.mismatch_count( samRecord )
    qlen            = int( match / sam.identity(samRecord) )
    mismatch        = int(samRecord[4].split(":")[2]) # Mismatches come off of the "XM" Tag
    match           = qlen - mismatch
    ID              = SAM_identity( samRecord )

    # Building the record
    bestHitRecord             = {}
    bestHitRecord['qname']    = samRecord['qname']
    bestHitRecord['qsize']    = qlen
    bestHitRecord['qstart']   = 1
    bestHitRecord['qend']     = qlen
    if ( samRecord['flags'] ==  16 ):
        bestHitRecord['placDir']  = '-'
    else:
        bestHitRecord['placDir']  = '+'
    #####
    bestHitRecord['tname']    = samRecord['tname']
    bestHitRecord['tsize']    = targetSizes[samRecord['tname']]
    bestHitRecord['tstart']   = samRecord['pos']
    bestHitRecord['tend']     = samRecord['pos'] + sam.cigar_count(samRecord, sam.cigar_tlen) - 1
    bestHitRecord['numBases'] = match
    bestHitRecord['per_ID']   = ID
    bestHitRecord['per_COV']  = SAM_coverage(samRecord)
    # Building the output string
    outputString     = 12 * ['']
    outputString[0]  = bestHitRecord['qname']
    outputString[1]  = '%d'%bestHitRecord['qsize']
    outputString[2]  = '%d'%bestHitRecord['qstart']
    outputString[3]  = '%d'%bestHitRecord['qend']
    outputString[4]  = bestHitRecord['placDir']
    outputString[5]  = bestHitRecord['tname']
    outputString[6]  = '%d'%bestHitRecord['tsize']
    outputString[7]  = '%d'%bestHitRecord['tstart']
    outputString[8]  = '%d'%bestHitRecord['tend']
    outputString[9]  = '%d'%bestHitRecord['numBases']
    outputString[10] = '%.2f'%bestHitRecord['per_ID']
    outputString[11] = '%.2f\n'%bestHitRecord['per_COV']
    return ( '\t'.join(outputString), match )

#==============================================================
def bestHitWrite( samRecordList, tmpHandle, targetSizes ):
    for samRecord in samRecordList:
        tmpHandle.write( SAM_to_bestHit(samRecord,targetSizes)[0] )
    #####
    return

#==============================================================
class parseClass( object ):

    def __init__(self, score, blatClass, nRetained=1):
        self.nRetained   = nRetained
        self.hits        = {}
        self.hits[score] = [blatClass]
        self.state       = 'low'
        self.funMap      = {'low':self.addHit_low, \
                            'high':self.addHit_high }

    def addHit(self, score, blatClass):
        self.funMap[self.state]( score, blatClass )
        return

    def addHit_low(self, score, blatClass):
        try:
            self.hits[score].append( blatClass)
        except KeyError:
            self.hits[score] = [ blatClass ]
        #####
        if ( len( self.hits.keys() ) >= self.nRetained ): self.state = 'high'
        return

    def addHit_high(self, score, blatClass):
        # Is the current hit larger than the lowest hit?
        sortedScores = sorted( self.hits.keys(), reverse=True )
        topScore     = sortedScores[0]
        lowScore     = sortedScores[-1]
        if ( score >= lowScore ):
            try:
                self.hits[score].append( blatClass)
            except KeyError:
                self.hits[score] = [ blatClass ]
            #####
            if ( len( self.hits.keys() ) > self.nRetained ):
                # Removing the lowest scoring hit
                sortedScores = sorted( self.hits.keys(), reverse=True )
                self.hits.pop( sortedScores[-1] )
            #####
        #####
        return
    
    def getTopHits(self):
        topScore = sorted( self.hits.keys(), reverse=True )[0]
        return self.hits[topScore]

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [query fasta] [target fasta] [output file] [options]"

    parser = OptionParser(usage)

    numMismatchesAllowed = -1
    parser.add_option( '-n', \
                       "--numMismatchesAllowed", \
                       type    = "int", \
                       help    = "Number of query mismatches allowed (does not include gaps!).  Default: bwa default", \
                       default = numMismatchesAllowed )
                       
    parser.add_option( '-s', \
                       "--suppressRepetitiveReads", \
                       action  = "store_true", \
                       dest    = 'suppressRepetitiveReads', \
                       help    = "Do not output repetitive reads (nAlignments>255):  Default=Indicate repetitive reads with [REPETITIVE] in output file." )
    parser.set_defaults( pairedAlignment = False )

    parser.add_option( "-b", \
                       "--bestHitFormat", \
                       action  = 'store_true', \
                       dest    = 'bestHitFormat', \
                       help    = "Output in best hit format (Linear Alignments Only):  Default=SAM formatted output." )
    parser.set_defaults( bestHitFormat = False )

    queryQUAL = None
    parser.add_option( "-q", \
                       "--queryQUAL", \
                       type    = 'str', \
                       help    = "QUAL file associated with query.  Default: None", \
                       default = queryQUAL )

    parser.add_option( "-t", \
                       "--arachneFormatReads", \
                       action  = 'store_true', \
                       dest    = 'arachneFormatReads', \
                       help    = "ARACHNE formatted reads:  Default=False" )
    parser.set_defaults( arachneFormatReads = False )

    parser.add_option( "-p", \
                       "--pairedAlignment", \
                       action  = 'store_true', \
                       dest    = 'pairedAlignment', \
                       help    = "Perform paired alignment:  Default=Single ended alignment" )
    parser.set_defaults( pairedAlignment = False )

    numReadsToAlign = -1
    parser.add_option( '-r', \
                       "--numReadsToAlign", \
                       type    = "int", \
                       help    = "Number of reads to align.  Default: Use All Reads.", \
                       default = numReadsToAlign )
    
    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        queryFASTA = realpath(args[0])
        if ( not isfile(queryFASTA) ): parser.error( '%s can not be found'%queryFASTA )

        targetFASTA = realpath(args[1])
        if ( not isfile(targetFASTA) ): parser.error( '%s can not be found'%targetFASTA )

        mainOutFile = realpath(args[2])
    #####
    
    isQueryQUAL = False
    if ( options.queryQUAL != None ):
        queryQUAL = realpath(options.queryQUAL)
        if ( not isfile(queryQUAL) ): parser.error( '%s can not be found'%queryQUAL )
        isQueryQUAL = True
    else:
        queryQUAL = None
    #####

    # Internal Parameters    
    runProcesses = True # For debugging
    deleteOldDir = False # Also for debugging
    
    # Make sure both files exist
    for tmpName in [queryFASTA, targetFASTA]: testFile(tmpName)
    if ( isQueryQUAL ): testFile(queryQUAL)
        
    # Generating the directory information
    basePath   = abspath(curdir)
    tmpDir     = generateTmpDirName()
    tmpPath    = join( basePath, tmpDir )
    outputBase = baseFileName(queryFASTA)
    bwaCall    = '/mnt/local/EXBIN/bwa'
    
    #=================================================================
    # Step 1:  Create temporary directory
    create_tmp_dir( tmpPath, deleteOld=deleteOldDir )
    chdir( tmpPath )
    
    #=================================================================
    # Step 2:  Generate the filter code
    screeningCall = '/home/mqualls/python32/bin/python3 screenSAM.py'
    oh = open( 'screenSAM.py', 'w' )
    oh.write( 'from sys import stdin, stdout\n' )
    oh.write( 'lineSplitter = lambda x: x.split(None)\n' )
    oh.write( 'for line in stdin:\n' )
    oh.write( '    try:\n' )
    oh.write( '        splitLine = lineSplitter(line)\n' )
    oh.write( '        # Screening unmapped reads\n' )
    oh.write( '        binString = bin(int(splitLine[1]))\n' )
    oh.write( '        if ( binString[-3] == "1" ): continue\n' ) # Checks for mapped read
    if ( options.pairedAlignment ):
        oh.write( '        if ( binString[-2] == "0" ): continue\n' ) # Check for proper pairing
    #####
    oh.write( '        stdout.write(line)\n' )
    oh.write( '    except ValueError:\n' )
    oh.write( '        continue\n' )
    oh.write( '    except IndexError:\n' )
    oh.write( '        continue\n' )
    oh.close()

    #=================================================================
    # Step 3: Pulling a subset of reads, if necessary
    if ( options.numReadsToAlign > 0  ):
        stderr.write( '\n-------------------\nREDUCING READ POOL TO %d READS\n'%options.numReadsToAlign )
        if runProcesses: 
            if ( isGzipFile(queryFASTA) ):
                system( "cat %s | gunzip -c | grep '>' | cut -b2- | cut -f1 -d' ' | head -%d > reducedReads.dat"%( queryFASTA, 2*options.numReadsToAlign ) )
            elif ( isBzipFile(queryFASTA) ):
                system( "cat %s | bunzip2 -c | grep '>' | cut -b2- | cut -f1 -d' ' | head -%d > reducedReads.dat"%( queryFASTA, 2*options.numReadsToAlign ) )
            else:
                system( "cat %s | grep '>' | cut -b2- | cut -f1 -d' ' | head -%d > reducedReads.dat"%( queryFASTA, 2*options.numReadsToAlign ) )
            #####
            system( '/mnt/local/EXBIN/extract_seq_and_qual -i reducedReads.dat -o reducedReads.fasta %s'%queryFASTA )
            queryFASTA = "reducedReads.fasta"
        #####
    #####

    #=================================================================
    # Step 4:  Index Target File
    indexName = 'target_fasta_index'
    if runProcesses: system( '%s index -p %s -a bwtsw %s'%(bwaCall,indexName,targetFASTA) )
    
    #=================================================================
    # Check for paired alignments
    if ( options.pairedAlignment ):
    
        # Split the reads fasta and qual into two files:  reads_1 and reads_2
        if runProcesses: split_SEQ_and_QUAL( queryFASTA, queryQUAL, 'reads_1', 'reads_2' )
        
        # Perform both alignments
        if ( options.numMismatchesAllowed < 0 ):
            if runProcesses: system( '%s aln -t 6 %s reads_1.seq > reads_1.sai'%(bwaCall,indexName) )
            if runProcesses: system( '%s aln -t 6 %s reads_2.seq > reads_2.sai'%(bwaCall,indexName) )
        else:
            if runProcesses: system( '%s aln -n %d -t 6 %s reads_1.seq > reads_1.sai'%(bwaCall,options.numMismatchesAllowed,indexName) )
            if runProcesses: system( '%s aln -n %d -t 6 %s reads_2.seq > reads_2.sai'%(bwaCall,options.numMismatchesAllowed,indexName) )
        #####

        # Screen SAM file for reads that are unpaired, and that don't align
        if runProcesses: 
            system( '%s sampe -n 255 %s reads_1.sai reads_2.sai reads_1.seq reads_2.seq | %s > %s.sam'%( bwaCall, \
                                                                                                         indexName, \
                                                                                                         screeningCall, \
                                                                                                         outputBase ) )
    else:
        
        #=================================================================
        # Step 5:  Align the query to the target
        if ( options.numMismatchesAllowed < 0 ):
            if runProcesses: system( '%s aln -t 6 %s %s > %s.sai'%(bwaCall, indexName, queryFASTA, outputBase) )
        else:
            if runProcesses: system( '%s aln -n %d -t 6 %s %s > %s.sai'%(bwaCall, options.numMismatchesAllowed, indexName, queryFASTA, outputBase) )
        #####
        
        #=================================================================
        # Step 6:  Dump out the SAM file, and screen reads that don't align
        outputSAMFile = '%s.sam'%outputBase
        if runProcesses: system( '%s samse -n 255 %s %s.sai %s | %s > %s.sam'%( bwaCall, \
                                                                                indexName, \
                                                                                outputBase, \
                                                                                queryFASTA, \
                                                                                screeningCall, \
                                                                                outputBase ) )
        #=================================================================
        # Step 7:  Screen the SAM file for decent hits
        
        # Reading in the query and target sizes
        if ( isGzipFile(targetFASTA) ):
            targetSizes = dict([(record['id'], len(''.join(record['data']))) for record in iterItems(gzip.open(targetFASTA))])
        else:
            targetSizes = dict([(record['id'], len(''.join(record['data']))) for record in iterItems(open(targetFASTA))])
        #####
        
        # Screen alignments for best hits
        oh           = open( mainOutFile, 'w' )
        if ( options.suppressRepetitiveReads ):
            isRepeat = lambda x: False
        else:
            isRepeat = lambda x: x == "XT:A:R"
        #####
        XA_parser    = re.compile( r"([A-Za-z0-9-_#:|.]+),([+-]{1})(\d+),([0-9A-Z]+),(\d+);" ).findall
        strandDict   = { "+":0, "-":16 }
        x            = iterCounter(1000000)
        for tmpRecord in sam.iter( open( '%s.sam'%outputBase ) ):

            # Parse the XA tag to expand all of the alignments in an intelligent manner
            # XA tag format: (chr,pos,CIGAR,NM;)
            try:
                parsed_XA = XA_parser(tmpRecord[8].split(":")[2])
                if ( len(parsed_XA) == 1 ):
                    samRecordList = [ dict( (key,value) for key, value in tmpRecord.items() ) ]
                else:
                    samRecordList = []
                    for tname, strand, pos, cigar, nm in parsed_XA:
                        samRecordList.append( dict( (key,value) for key, value in tmpRecord.items() ) )
                        samRecordList[-1]['tname'] = tname
                        samRecordList[-1]['pos']   = int(pos)
                        samRecordList[-1]['cigar'] = cigar
                        samRecordList[-1][1]       = "NM:i:%s"%nm
                        samRecordList[-1][4]       = "XM:i:%s"%nm
                        samRecordList[-1]['flags'] = strandDict[strand]
                        samRecordList[-1][8]       = "XA:Z:"
                    #####
                #####
            except KeyError:
                # This is a repetitive read
                oh.write( '%s\tREPETITIVE\n'%tmpRecord['qname'] )
                x()
                continue
            #####
    
            # Sorting the alignments
            tmpClass = None
            for samRecord in samRecordList:
                bestHitRecord, score = SAM_to_bestHit(samRecord,targetSizes)
                try:
                    tmpClass.addHit( score, samRecord )
                except AttributeError:
                    tmpClass = parseClass( score, samRecord )
                #####
            #####
            
            #=================================================================
            # write the "best_hit" formatted file (if desired).
            try:
                if ( options.bestHitFormat ):
                    for record in tmpClass.getTopHits(): 
                        bestHitWrite( [record], oh, targetSizes ) ; x()
                    #####
                else:
                    for record in tmpClass.getTopHits(): 
                        sam.write( [record], oh ) ; x()
                    #####
                #####
            except AttributeError:
                continue
            #####
            
        #####
        oh.close()
        
    #####

    # Step 7: Cleanup
    chdir( '../' )
    clean_tmp(tmpPath)

#==============================================================
if ( __name__ == '__main__' ):
    real_main()


