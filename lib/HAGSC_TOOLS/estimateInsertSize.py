#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import generateTmpDirName, deleteDir, testDirectory
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, writeFASTA
from hagsc_lib import iterCounter, histogramClass

from os.path import realpath, abspath, isfile, join, split, splitext
from os import curdir, mkdir, system, chdir

from sys import stderr, stdout

from optparse import OptionParser

from math import sqrt

import subprocess

#==============================================================
def printAndExecute( cmd, execute=True ):
    stdout.write( "%s\n"%cmd )
    if ( execute ): system( cmd )
    return 

#==============================================================
class helpfulFileClass(object):
    def __init__(self,fileName):
        self.fileName = fileName
        self.determineType()
        self.openFileHandle()
    
    def determineType(self):
        if ( isGzipFile(self.fileName) ):
            self.fileType = 'gzip'
        elif ( isBzipFile(self.fileName) ):
            self.fileType = 'bzip'
        else:
            self.fileType = 'uncompressed'
        #####
    
    def openFileHandle(self):
        if ( self.fileType == 'gzip' ):
            self.p = subprocess.Popen( 'cat %s | gunzip -c'%self.fileName, shell=True, stdout=subprocess.PIPE )
            self.oh = self.p.stdout
        elif ( self.fileType == 'bzip' ):
            self.p = subprocess.Popen( 'cat %s | bunzip2 -c'%self.fileName, shell=True, stdout=subprocess.PIPE )
            self.oh = self.p.stdout
        elif ( self.fileType == 'uncompressed' ):
            self.oh = open(self.fileName)
        #####
    
    def readline(self):
        return self.oh.readline()
    
    def close(self):
        if ( self.fileType == 'gzip' ):
            self.p.poll()
        elif ( self.fileType == 'bzip' ):
            self.p.poll()
        else:
            self.oh.close()
        #####

#==============================================================
def fasta_or_fastq( fileName ):

    # Testing the first four lines
    oh = helpfulFileClass(fileName)
    line1 = oh.readline()
    line2 = oh.readline()
    line3 = oh.readline()
    line4 = oh.readline()
    oh.close()
    
    if ( line1[0] == ">" ): return 'FASTA'
    if ( (line3[0] == '+') and \
         (line1[0] == '@') ): return 'FASTQ'

    return 'UNKNOWN'

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA/FASTQ] [ASSEMBLY] [options]"

    parser = OptionParser(usage)

    numberOfReads = 100000
    parser.add_option( "-n", \
                       "--numberOfReads", \
                       type    = 'float', \
                       help    = "Number of reads to consider:  Default: %d"%numberOfReads, \
                       default = numberOfReads )

    numberToSkip = 1000000
    parser.add_option( "-s", \
                       "--numberToSkip", \
                       type    = 'float', \
                       help    = "Number of reads to skip:  Default: %d"%numberToSkip, \
                       default = numberToSkip )

    parser.add_option( "-c", \
                       "--revCompReads", \
                       action  = 'store_true', \
                       dest    = 'revCompReads', \
                       help    = "Reverse complement the pairs:  Default=False" )
    parser.set_defaults( revCompReads = False )

    userIndex = None
    parser.add_option( "-i", \
                       "--userIndex", \
                       type    = 'string', \
                       help    = "User provided INDEX.  Default: Generate Index", \
                       default = userIndex )

    upperBound = 2000
    parser.add_option( "-u", \
                       "--upperBound", \
                       type    = 'int', \
                       help    = "Upper bound on insert size:  Default: %d"%upperBound, \
                       default = upperBound )

    numberOfBins = 100
    parser.add_option( "-b", \
                       "--numberOfBins", \
                       type    = 'int', \
                       help    = "Number of bins for hisogram:  Default: %d"%numberOfBins, \
                       default = numberOfBins )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTA = realpath(args[0])
        if ( not isfile(FASTA) ): parser.error( '%s can not be found'%FASTA )
        
        ASSEMBLY = realpath(args[1])
        if ( not isfile(ASSEMBLY) ): parser.error( '%s can not be found'%ASSEMBLY )

        # Indexing the genome
        if ( options.userIndex == None ):
            indexName = 'INDEX'
        else:
            indexName = realpath(options.userIndex)
        #####

    #####
    
    # Create temporary directory
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpPath  = join( basePath, generateTmpDirName() )
    deleteDir(tmpPath)
    mkdir(tmpPath, 0777)
    testDirectory(tmpPath)
    
    # Changing the directory
    chdir( tmpPath )
    
    # Indexing the genome
    if ( options.userIndex == None ):
        cmd = 'module load bwa ; bwa index -p %s -a bwtsw %s'%(indexName,ASSEMBLY)
        printAndExecute( cmd, execute=True )
    #####
    
    # Performing the alignments
    cmdList = []
    if ( isGzipFile(FASTA) ):
        cmdList.append( 'cat %s | gunzip -c'%FASTA )
    elif ( isBzipFile(FASTA) ):
        cmdList.append( 'cat %s | bunzip2 -c'%FASTA )
    else:
        cmdList.append( 'cat %s'%FASTA )
    #####
    cmdList.append( 'head -%d'%(options.numberToSkip+options.numberOfReads) )
    cmdList.append( 'tail -%d'%options.numberOfReads )
    cmdList.append( 'clip -B 10000 -f 25 -L 0 -c 75 /dev/stdin' )
    if ( options.revCompReads ):
        cmdList.append( 'extract_seq_and_qual -c /dev/stdin' )
    #####
    cmdList.append( 'awk -f /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/estimateInsertSize.awk INDEX=%s'%(indexName) )
    cmd = ' | '.join( cmdList )
    system( cmd )
    
    # Creating the insert size distribution
    alignCmd    = 'module load bwa ; bwa sampe %s R1.sai R2.sai R1.fasta R2.fasta paired.sam'%indexName
    cmd         = '%s | grep -v ^@SQ | cut -f9 | grep -v ^0 | grep -v ^-' % alignCmd
    p           = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    insertSizes = [int(line.strip()) for line in p.stdout if unicode(line.strip()).isnumeric()]
    p.poll()

    # Changing directory
    chdir( basePath )
    
    # Performing the statistical analysis of the output
    tmpHist = histogramClass( 0, options.upperBound, options.numberOfBins )
    map( tmpHist.addData, filter(lambda x:(x>0 and x<=options.upperBound), insertSizes) )
    minVal, maxVal = tmpHist.findingBounds( 0.001 )
    minVal         = 0 if ( minVal < 100 ) else minVal
    maxVal         = int( 100 * round( float(maxVal) / 100.0 ) ) # Rounds up to the nearest 100
    
    # Creating the final histogram
    finalHist = histogramClass( minVal, maxVal, options.numberOfBins )
    map( finalHist.addData, filter(lambda x:(x>=minVal and x<=maxVal), insertSizes) )
    print finalHist.generateOutputString()
    outputDict = finalHist.generateOutputDict()
    
    # Computing the final statistics
    x = []
    map( x.append, filter(lambda x:(x>=minVal and x<=maxVal), insertSizes) )
    sum_x  = sum( map(float,x) )
    sum_x2 = sum( map( lambda x:float(x*x), x) )
    N      = float(len(x))
    mean   = sum_x / N
    std    = sqrt( sum_x2 / N - mean * mean )
    
    # Creating the plots
    outputFileBase = splitext( split(FASTA)[1] )[0]
    insertSizeFile = '%s.insertSizes'%outputFileBase
    oh = open( insertSizeFile, 'w' )
    nVals = len(outputDict['midpoint'])
    for n in xrange( nVals ):
        oh.write( '%.3f\t%d\n'%( outputDict['midpoint'][n], outputDict['counts'][n] ) )
    #####
    oh.close()
    
    # Writing the GNUplot file
    plotFile = '%s.plotFile'%outputFileBase
    oh = open( plotFile, 'w' )
    oh.write( 'reset\n' )
    oh.write( "set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\n" )
    oh.write( 'set ylabel \"Counts\"\n' )
    oh.write( 'set xlabel \"Insert Size (bp)\"\n' )
    oh.write( 'set nokey\n' )
    oh.write( 'set title \"%s, Insert=%.1f+/-%.1f\"\n'%(outputFileBase,mean,std) )
    oh.write( 'set output \"%s.insertSize.png\"\n'%outputFileBase )
    oh.write( 'set size ratio 0.50\n' )
    oh.write( 'set grid x y\n' )
    oh.write( 'set ytics\n' )
    oh.write( 'set autoscale y\n' )
    oh.write( 'plot \"%s\" using 1:2 title \"Map\" axis x1y1 with boxes\n'%insertSizeFile )
    oh.close()
    
    # Making the plot
    system( 'gnuplot %s'%plotFile )

    # Removing the temporary directory
    system( 'rm -rf %s'%tmpPath )
    
    return
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
