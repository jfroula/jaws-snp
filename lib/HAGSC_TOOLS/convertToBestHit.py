#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="12-April-2011"

# Local Library Imports
from hagsc_lib import parseBLATOutput, best_extent, iterBLAT

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout, stdin
from sys import stderr

import gzip

#==============================
def checkFile( fileName, parser ):
    if ( not isfile(fileName) ): 
        parser.error( '%s can not be found'%fileName )
    else:
        return fileName
    #####

#===============
def real_main():

    # Defining the program options
    usage = "usage: %prog [BLAT file] [options]"

    parser = OptionParser(usage)

    defaultOutputFile = 'stdout'
    parser.add_option( "-o", \
                       "--outputFile", \
                       type    = 'string', \
                       help    = "Output file name:  Default %s"%defaultOutputFile, \
                       default = defaultOutputFile )

    defaultMultiple = 2
    parser.add_option( "-m", \
                       "--querySizeMultiple", \
                       type    = 'float', \
                       help    = "Alignment size limit (multiple of query size):  Default %s"%defaultMultiple, \
                       default = defaultMultiple )

    parser.add_option( "-z", \
                       "--isGzipFile", \
                       action  = 'store_true', \
                       dest    = 'isGzipFile', \
                       help    = "Input file is gzipped:  Default=False" )
    parser.set_defaults( isGzipFile = False )

    parser.add_option( "-c", \
                       "--convertOnly", \
                       action  = 'store_true', \
                       dest    = 'convertOnly', \
                       help    = "Convert each BLAT entry to bestHit format without determining the best hit:  Default=False" )
    parser.set_defaults( convertOnly = False )

    parser.add_option( "-e", \
                       "--excludeOverhanging", \
                       action  = 'store_true', \
                       dest    = 'excludeOverhanging', \
                       help    = "Exclude overhanging sequence from coverage calculation:  Default=False" )
    parser.set_defaults( excludeOverhanging = False )
                       
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        if ( args[0] == 'stdin' ):
            BLAT_inputFile = 'stdin'
        else:
            BLAT_inputFile = checkFile( args[0], parser )
        #####
    #####
    
    # Setting up the output file name
    bestHit_outputFile = options.outputFile
    
    # Parsing the output
    stderr.write( '\t-Parsing output...\n')
    if ( bestHit_outputFile == 'stdout' ):
        bestHitFileHandle = stdout
    else:
        bestHitFileHandle = open( bestHit_outputFile, 'w' )
    #####
    if ( options.isGzipFile ):
        BLAT_inputFile = gzip.open( BLAT_inputFile, 'r' )
    elif ( BLAT_inputFile == 'stdin' ):
        BLAT_inputFile = stdin
    else:
        BLAT_inputFile = open( BLAT_inputFile )
    #####
    if ( not options.convertOnly ):
#         parseBLATOutput( bestHitFileHandle, BLAT_inputFile, False, options.querySizeMultiple, False )
        parseBLATOutput( bestHitFileHandle, BLAT_inputFile, False, options.querySizeMultiple, False, options.excludeOverhanging )
    else:
        for blatClass in iterBLAT( BLAT_inputFile ):
            # Initializing the output string
            outputString    = 12 * ['']
            outputString[0] = blatClass.Q_name
            outputString[1] = '%d'%blatClass.Q_size
            outputString[2] = '%d'%blatClass.Q_start
            outputString[3] = '%d'%blatClass.Q_end
    
            # Size of match divided by shorter of read sizes
            Q_length = float( blatClass.Q_end - blatClass.Q_start )
            T_length = float( blatClass.T_end - blatClass.T_start )
            Q_size   = float( blatClass.Q_size )
            T_size   = float( blatClass.T_size )

            T_start  = float( blatClass.T_start )
            T_end    = float( blatClass.T_end )
            T_size   = float( blatClass.T_size )
            Q_start  = float( blatClass.Q_start )
            Q_end    = float( blatClass.Q_end )
            Q_size   = float( blatClass.Q_size )
            strand   = blatClass.strand
            # This option excludes the overhanging sequence 
            # During the coverage calculation
            if ( options.excludeOverhanging ):
                if ( strand == '+' ):
                    if ( (Q_size-Q_end) > (T_size-T_end) ):
                        Q_size = Q_end + (T_size-T_end)
                    #####
                    if ( Q_start > T_start ):
                        Q_size = (Q_size - Q_start) + T_start
                    #####
                elif ( strand == '-' ):
                    if ( Q_start > (T_size-T_end) ):
                        Q_size = (Q_size - Q_start) + ( T_size - T_end )
                    #####
                    if ( (Q_size-Q_end) > T_start ):
                        Q_size = Q_end + T_start
                    #####
                #####
            #####
    
            # ID = max % of identical bases in the placement length
            # of the query/target
            totalMatchSize = blatClass.match + blatClass.repMatch
            id = 100.0 * float(totalMatchSize) / min( Q_length, T_length )
            id = min( 100.0, id )
    
            # Coverage = max % of the total query/target length that places
            cov = 100.0 * max( (Q_length/Q_size), (T_length/T_size) )
            start, stop = best_extent( Q_length, \
                                  blatClass.blockSizes, \
                                  blatClass.T_starts, \
                                  options.querySizeMultiple )
            outputString[4]  = '%s'%blatClass.strand
            outputString[5]  = '%s'%blatClass.T_name
            outputString[6]  = '%d'%blatClass.T_size
            outputString[7]  = '%d'%start
            outputString[8]  = '%d'%stop
            outputString[9]  = '%d'%totalMatchSize
            outputString[10] = '%5.2f'%id
            outputString[11] = '%5.2f\n'%cov
            # Writing to output file
            bestHitFileHandle.write( '\t'.join(outputString) )
        #####
        if ( not options.isGzipFile ): BLAT_inputFile.close()
    #####
    if ( options.isGzipFile ):  BLAT_inputFile.close()
    if ( bestHit_outputFile != 'stdout'): bestHitFileHandle.close()
    return
    
#####

#=============================
if ( __name__ == '__main__' ):
    real_main()
