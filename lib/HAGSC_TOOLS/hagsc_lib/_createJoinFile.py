
__author__="jjenkins"
__date__ ="$Apr 26, 2010 12:51:12 PM$"

########################################################################
def generateJoinFile( joinList, outputFileName ):

    # Building the directed join connectivity
    joinGraph  = {}
    entrySet   = set()
    exitSet    = set()
    emptySet   = set([])
    problemSet = set()
    rc_Nodes     = set()
    non_rc_Nodes = set()
    for join in joinList:

        # Branching?
        if ( entrySet.intersection([join[0]]) != emptySet ):
            print 'Repeated Entry Node %s'%join[0]
            problemSet.add(join[0])
        #####

        if ( exitSet.intersection([join[1]]) != emptySet ):
            print 'Repeated Exit Node %s'%join[1]
            problemSet.add(join[1])
        #####

        # Adding the join
        try:
            joinGraph[join[0]].append( join[1] )
        except KeyError:
            joinGraph[join[0]] = [ join[1] ]
        #####

        # Generate the entry and exit sets
        entrySet.add( join[0] )
        entrySet.add( join[0].replace('rc','') )

        exitSet.add( join[1] )
        exitSet.add( join[1].replace('rc','') )

        # Looking for nodes that need to be flipped
        if ( join[0][:2] == 'rc' ):
            rc_Nodes.add( join[0].replace('rc','') )
        else:
            non_rc_Nodes.add( join[0] )
        #####
        if ( join[1][:2] == 'rc' ):
            rc_Nodes.add( join[1].replace('rc','') )
        else:
            non_rc_Nodes.add( join[1] )
        #####

    #####

    # Flipped Nodes
    if ( rc_Nodes.intersection(non_rc_Nodes) != emptySet ):
        print '\n\n'
        print 'The following nodes need to be flipped: '
        print rc_Nodes.intersection(non_rc_Nodes)
        print '\n\n'
        assert False
    #####

    # Finding all entry nodes
    entryNodes = []
    for node in entrySet:
        if ( exitSet.intersection([node]) == emptySet ): entryNodes.append(node)
    #####

    # Finding all exit nodes
    exitNodes = []
    for node in exitSet:
        if ( entrySet.intersection([node]) == emptySet ): exitNodes.append(node)
    #####

    outputHandle = open( outputFileName, 'w' )
    for entryNode in entryNodes:
        if ( entryNode in problemSet ): continue
        for exitNode in exitNodes:
            if ( exitNode in problemSet ): continue
            allPaths = find_all_paths( joinGraph, entryNode, exitNode )
            if ( len(allPaths) == 1  ):
                for n in xrange(len(allPaths)):
                    outputString = ' 10000 '.join( allPaths[n] )
                    outputHandle.write( outputString + '\n' )
                #####
            elif ( len(allPaths) > 1):
                print 'Multiple paths from entry to exit detected'
                print allPaths
            #####
        #####
    #####

    print '\nPROBLEM SET'
    print problemSet

########################################################################
def find_all_paths(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return [path]
    #####
    try:
        paths = []
        for node in graph[start]:
            if node not in path:
                newpaths = find_all_paths(graph, node, end, path)
                for newpath in newpaths:
                    paths.append(newpath)
                #####
            #####
        #####
    except KeyError:
        return []
    #####
    return paths

