__author__="pberry"
__date__ ="$Jun 24, 2010 1:41:26 PM$"

# Python Library Imports
from _helperClasses import histogramClass

from _jobQueueing import remoteServer4

from _helperFunctions import generateTmpDirName
from _helperFunctions import testDirectory
from _helperFunctions import deleteFile
from _helperFunctions import deleteDir
from _helperFunctions import testFile

from _globalSettings import mask_repeats_hash_path

from sys import stdout

from os.path import splitext
from os.path import abspath
from os.path import split
from os.path import join

from os import curdir
from os import mkdir

################################################################################
class scaffold_repMasker( object ):
    
    def __init__(self, filePos):

        self.filePos = filePos

        self.length    = 0
        self.num_bases = 0

        self.t1 = 0.0
        self.t2 = 0.0
        self.t3 = 0.0
        self.t4 = 0.0

        self.GC = 0
        self.AT = 0
#####

################################################################################
class repMasker_class(object):
    """

    repMasker_call( FASTA_File, mer = 24 )

    """

    def __init__( self, FASTA_File, \
                        mer=24, \
                        clump=3, \
                        minScaffSize=50000, \
                        clusterNum=101 ):


        # Do they exist?
        testFile( FASTA_File )

        # Incoming variables
        self.fastaFile     = FASTA_File
        self.mer           = mer
        self.scaffoldClump = clump
        self.minScaffSize  = minScaffSize
        self.clusterNum    = clusterNum

        # Opening the FASTA file
        self.fastaHandle = open( self.fastaFile, 'r' )

        # Setting up the temporary directory
        self.tmpDir = generateTmpDirName()
        self.basePath = abspath(curdir)
        self.tmpPath  = join( self.basePath, self.tmpDir )


        # Creating the output Files
        self.baseFileName = splitext(split(self.fastaFile)[1])[0]
        self.maskRepeatsHashPath = mask_repeats_hash_path
        self.baseOutFileName = join( self.tmpPath, self.baseFileName )
        self.perlFile = join( self.basePath, 'repMasker.pl' )

        # Main output file names
        self.rawOutputFile = join( self.basePath, \
                                   '%s_rawData.out'%self.baseFileName )
        self.histOutputFile = join( self.basePath, \
                                    '%s_hist.out'%self.baseFileName )
        self.rawFileHandle = open( self.rawOutputFile, 'w' )

        # Initializing other variables
        self.z_option = 0
        self.AT_bases = 0
        self.GC_bases = 0
        self.N_bases  = 0

        # Initializing histograms
        self.t_hist = 5 * [None]
        for n in xrange( 1,5 ):
            self.t_hist[n] = histogramClass( 0, 100, 50 )
        #####

        # Initializing other variables
        self.indexDict = {}

    def create_tmp_dir(self):
        stdout.write( '\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir( self.tmpDir, 0777 )
        testDirectory( self.tmpDir )
        return


    def perform_repMasker(self):

        self.create_tmp_dir()

        # (1) Index the file to a dictionary
        self.indexFile()

        # (2) Gather sequence statistics
        self.countFASTABasePairs()

        # (3) Mask sequence
        self.maskingSequence()

        # (4) Print out the results
        self.printData()

        # (5)  Taking out the trash
        self.clean_tmp()

    def maskingSequence(self):
        scaffoldList = self.indexDict.keys()
        scaffoldList.sort()
        counter = 0
        notDone = True
        while ( notDone ):
            # Pulling scaffolds
            startIndex = counter * self.scaffoldClump
            endIndex   = startIndex + self.scaffoldClump
            tmpList    = scaffoldList[startIndex:endIndex]
            if ( len(tmpList) < self.scaffoldClump ): notDone = False
            # Performing analysis
            self.analyzeScaffolds( tmpList )
            self.countMaskedBases( tmpList )
            counter += 1
        #####

        # Closing the FASTA File
        self.fastaHandle.close()
        self.rawFileHandle.close()

    def clean_tmp(self):
        stdout.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

    def indexFile(self):
        stdout.write( 'Indexing File....\n' )
        self.fastaHandle.seek(0)
        line = self.fastaHandle.readline()
        while line:
            if (line[0] == '>' ):
                parsedLine = line.split(None)
                scaffID = parsedLine[0][1:]
                self.indexDict[scaffID] = scaffold_repMasker( self.fastaHandle.tell() )
            #####
            line = self.fastaHandle.readline()
        #####
        return

    def countFASTABasePairs(self):
        stdout.write( 'Counting FASTA Base Pairs...\n' )
        n_mers = 0
        for scaffID, scaffClass in self.indexDict.items():
            # Go to the head location
            self.fastaHandle.seek( scaffClass.filePos )
            # Read sequence
            tmpSeq_list = []
            while True:
                line = self.fastaHandle.readline()
                if ( not line ): break
                try:
                    if ( line[0] == '>' ): break
                except IndexError:
                    continue
                #####
                tmpSeq_list.append( line[:-1] )
            #####
            tmpSeq = "".join(tmpSeq_list)

            # Store base counting information
            self.countBases(tmpSeq)
            self.indexDict[scaffID].AT = self.AT_bases
            self.indexDict[scaffID].GC = self.GC_bases
            self.indexDict[scaffID].num_bases = self.AT_bases + self.GC_bases
            self.indexDict[scaffID].length = self.AT_bases + self.GC_bases + \
                                             self.N_bases
            n_mers += self.indexDict[scaffID].num_bases
        #####

        # Determining the amount of memory to allocate (-z option)
        self.z_option = min( 1800, max( int(n_mers*2/1000000), 2 ) )
        return

    def countBases(self, seq):
        self.AT_bases = seq.count('A') + seq.count('a') + \
                        seq.count('T') + seq.count('t')
        self.GC_bases = seq.count('G') + seq.count('g') + \
                        seq.count('C') + seq.count('c')
        self.N_bases  = seq.count('N') + seq.count('n')
        return

    def analyzeScaffolds(self, scaffList):
        stdout.write( 'Analyzing Scaffolds\n' )
        commandList = []
        outputString = 7 * ['']
        for scaffID in scaffList:
            for threshold in range(1,5):
                # Generating the output file name
                outputFileName = '%s_%s_%d.out'%( self.baseOutFileName, \
                                                      scaffID, \
                                                      threshold )
                # Generating the output string
                outputString[0] = self.maskRepeatsHashPath
                outputString[1] = '-t %d'%threshold
                outputString[2] = '-m %d'%self.mer
                outputString[3] = '-z %dm'%self.z_option
                outputString[4] = '-k %d'%self.minScaffSize
                outputString[5] = '-l %s, -X %s'%( scaffID, self.fastaFile )
                outputString[6] = '> %s'%( outputFileName )
                commandList.append( ' '.join(outputString) )
            #####
        #####
        queueSize = 4 * self.scaffoldClump
        remoteServer4( commandList, queueSize, clusterNum=self.clusterNum, \
                       perlFile=self.perlFile )
        deleteFile( self.perlFile )
    #####

    def countMaskedBases(self, scaffList):
        stdout.write( 'Counting masked bases...\n' )
        for scaffID in scaffList:
            for threshold in range(1,5):
                # Generating the output file name
                outputFileName = '%s_%s_%d.out'%( self.baseOutFileName, \
                                                      scaffID, \
                                                      threshold )
                # Reading in the sequence
                tmpHandle   = open( outputFileName, 'r' )
                tmpSeq_list = []
                while True:
                    line = tmpHandle.readline()
                    if ( not line ): break
                    try:
                        if ( line[0] == '>' ): continue
                    except IndexError:
                        continue
                    #####
                    tmpSeq_list.append( line[:-1] )
                #####
                tmpSeq = "".join(tmpSeq_list)

                # Counting bases
                self.countBases( tmpSeq )
                maskedBases    = float( self.AT_bases + self.GC_bases )
                unmaskedBases  = float( self.indexDict[scaffID].num_bases )
                unique_percent = 100.0 * maskedBases / unmaskedBases

                # Recording the total
                setattr( self.indexDict[scaffID], "t%d"%threshold, unique_percent )
                tmpHandle.close()
                
                # Storing values in histogram
                self.t_hist[threshold].addData( unique_percent )
                deleteFile(outputFileName)

            #####

            scaffClass   = self.indexDict[scaffID]
            GC_percent   = 100.0 * float(scaffClass.GC) / float(scaffClass.num_bases)
            outputString = '%s\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n'%( \
                                        scaffID, \
                                        scaffClass.length, \
                                        scaffClass.num_bases,\
                                        GC_percent, \
                                        scaffClass.t1, \
                                        scaffClass.t2, \
                                        scaffClass.t3, \
                                        scaffClass.t4 )

            print outputString

            # Writing output to the main output file
            self.rawFileHandle.write( outputString)

        #####
        return

    def printData(self):
        stdout.write( 'Writing histogram output...\n' )
        tempHandle   = open( self.histOutputFile, 'w' )
        outputString = "".join( ["\n--t1--\n", \
                               self.t_hist[1].generateOutputString(), \
                               "\n--t2--\n", \
                               self.t_hist[2].generateOutputString(), \
                               "\n--t3--\n", \
                               self.t_hist[3].generateOutputString(), \
                               "\n--t4--\n", \
                               self.t_hist[4].generateOutputString() ] )
        tempHandle.write( outputString )
        tempHandle.close()
        return

    #####
