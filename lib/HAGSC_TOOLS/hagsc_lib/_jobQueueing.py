__author__="jjenkins"
__date__ ="$Mar 11, 2010 4:04:58 PM$"

from gp_lib import create_sh_file, genePool_queue_limiter

from _globalSettings import defaultCluster
from _globalSettings import zcat_path

from _helperFunctions import deleteFile
from _helperFunctions import testFile

from _helperClasses import histogramClass

from subprocess import Popen
from subprocess import PIPE

from os.path import basename
from os.path import join

from os import chmod

from sys import platform

from math import fmod

from time import sleep

from sys import stdout
from sys import stderr

#--------------
# Job Queueing
#--------------

########################################################################
def numRecords(fastaFile, arachneReadFormat=False, isGzipped=False):
    """
    numRecords(fastaFile)

    Rapidly provides the number of records in a FASTA file.
    """
    if ( isGzipped ):
        cmd = r"cat %s | gunzip -c | grep -c '>'"%fastaFile
    else:
        cmd = r"grep -c '>' %s"%fastaFile
    #####
    return int( [item.strip() for item in Popen( cmd, shell=True, stdout=PIPE ).stdout][0] )

#    # Generating the temporary file
#    testFile(fastaFile)
#    tmpOutputFile = 'scaffolds.tmp'
#    deleteFile(tmpOutputFile)
#    commandList = [r"cat %s | grep '>' > %s"%(fastaFile,tmpOutputFile)]
#    queueingJobs( commandList, 1, sleepTime=1.0 )
#    # Reading in the contents
#    scaffoldSet = set()
#    for parsedLine in iterTextFile( open(tmpOutputFile,'r'), ' ' ):
#        scaffoldSet.add( parsedLine[0][1:] )
#    #####
#    # Dumping the temporary file
#    deleteFile(tmpOutputFile)

    return scaffoldSet

########################################################################
def extractScaffoldNames(fastaFile, arachneReadFormat=False):
    """
    extractScaffoldNames(fastaFile)

    Rapidly provides a list of scaffold names from a fasta file.
    """

    if ( arachneReadFormat ):
        cmd = r"grep '>' %s | cut -f2 -d'>' | cut -f2 -d' '"%fastaFile
    else:
        cmd = r"grep '>' %s | cut -f2 -d'>' | cut -f1 -d' '"%fastaFile
    #####
    return set( [item.strip() for item in Popen( cmd, shell=True, stdout=PIPE ).stdout] )

#    # Generating the temporary file
#    testFile(fastaFile)
#    tmpOutputFile = 'scaffolds.tmp'
#    deleteFile(tmpOutputFile)
#    commandList = [r"cat %s | grep '>' > %s"%(fastaFile,tmpOutputFile)]
#    queueingJobs( commandList, 1, sleepTime=1.0 )
#    # Reading in the contents
#    scaffoldSet = set()
#    for parsedLine in iterTextFile( open(tmpOutputFile,'r'), ' ' ):
#        scaffoldSet.add( parsedLine[0][1:] )
#    #####
#    # Dumping the temporary file
#    deleteFile(tmpOutputFile)

    return scaffoldSet

########################################################################
def extract_gzip_file(gzip_fileName, output_fileName, \
                                clusterNumber=defaultCluster, runOnPC=True):
    """
    extract_gzip_file( gzip_fileName, output_fileName )

    Extracts a gzip file using zcat.  Can be run from hero, but default is
    on a PC!!
    """
    testFile(gzip_fileName)
    perlFile = 'unzipping.pl'
    deleteFile( perlFile )
    commandList = [r'%s %s > %s' % (zcat_path, gzip_fileName, output_fileName)]
    if ( runOnPC ):
        queueingJobs( commandList, 1, sleepTime=1.0 )
    else:
        remoteServer4( commandList, 1, clusterNum=clusterNumber, \
                                                      perlFile='./%s'%perlFile )
        deleteFile( perlFile )
    #####
    # Forcing the code to pause if the network is slow
    return testFile( output_fileName )

########################################################################
def executeCommandFile(commandLineFileName, numQueuedJobs=1):
    """
    executeCommandFile( commandLineFileName, numQueuedJobs = 1 )

    Execute a list of unix commands in "commandLineFileName"
    """
    commandList = [line[:-1] for line in open(commandLineFileName, 'r')]
    remoteServer4( commandList, numQueuedJobs )
    return

########################################################################
def runCommand(commandLineString, showCommand=False):
    """
    runCommand( commandLineString )

    Issues a shell command to the operating system.
    """
    if ( showCommand ):
        stderr.write('Issuing Command:  %s\n' % commandLineString)
    #####
    return Popen(commandLineString, shell=(platform != "win32"))

########################################################################
def queueingJobs(commandList, batchSize, sleepTime=5.0, showOutput = False):
    """
    queueingJobs( commandList, batchSize, sleepTime = 5.0 )

    Generates a queue of executing jobs and keeps them running
    one after another.
    """

    # Queueing up the initial batch
    nCommands = len(commandList)
    if (nCommands > batchSize):
        runningJobs   = batchSize * [None]
        jobsRemaining = nCommands - batchSize
    else:
        runningJobs   = nCommands * [None]
        jobsRemaining = 0
    #####
    for n in xrange(len(runningJobs)): runningJobs[n]=runCommand(commandList[n])

    # Main loop
    jobsRunning = True
    while (jobsRunning):
        # Full queue?
        fullQueue = True
        while (fullQueue):
            m = 0
            for polling in runningJobs:
                if ( (polling.poll() != None) ):
                    fullQueue = False
                    break
                #####
                m += 1
            #####
            if (not fullQueue):
                if (showOutput):
                    stderr.write('\tProcess %d completed\n' % m)
                #####
            else:
                sleep(sleepTime)
            #####
        #####
        # Pop completed job from queue
        runningJobs.pop(m)
        # More jobs?
        if (jobsRemaining != 0):
            nextCommand = nCommands - jobsRemaining
            runningJobs.append(runCommand(commandList[nextCommand]))
            jobsRemaining -= 1
        #####
        # All done
        if (runningJobs == []): jobsRunning = False
    #####
    if (showOutput):
        stderr.write('Queue Completed\n')
    #####
    return



########################################################################
class remoteServer4(object):
    """
    Job submission to the cluster.
    """
    def __init__(self, commandList, queueSize, \
                   clusterNum = defaultCluster, \
                   perlFile   = 'runJobs.pl', \
                   showCommands = False, \
                   perlCommand = '#!/apps/bin/perl5.8.0 -w\n' ):

        stderr.write( '\t-Starting the uge_job launcher object\n' )
        self.commandList  = commandList
        self.queueSize    = queueSize
        self.clusterNum   = clusterNum
        self.perlFile     = perlFile
        self.showCommands = showCommands
        self.perlCommand  = perlCommand
        
        # Launching the jobs
        jobCounter = 1
        shList = []
        for cmd in self.commandList:
            print cmd
            shFile = '%s.%d.sh' % ( self.perlFile, jobCounter )
            create_sh_file( self.commandList, '1:00:00', '5G', shFile, True )
            shList.append( shFile )
            jobCounter += 1
        #####
        genePool_queue_limiter( shList, self.queueSize )
        
        return


########################################################################
class remoteServer4_old(object):
    """
    Job submission to the cluster.
    """
    def __init__(self, commandList, queueSize, \
                   clusterNum = defaultCluster, \
                   perlFile   = 'runJobs.pl', \
                   showCommands = False, \
                   perlCommand = '#!/apps/bin/perl5.8.0 -w\n' ):
        stderr.write( '\t-Starting the remoteServer4 object\n' )
        self.commandList  = commandList
        self.queueSize    = queueSize
        self.clusterNum   = clusterNum
        self.perlFile     = perlFile
        self.showCommands = showCommands
        self.perlCommand  = perlCommand

        self.header    = 3 * ['']
        self.header[0] = self.perlCommand
        self.header[1] = "require '/mnt/raid2/SEQ/lib/remote_server4.pl';\n"
        if ( type(clusterNum) == type(0) ):
            self.header[2] = '&REMOTE_SERVER::init(%d);\n'%clusterNum
        elif ( type(clusterNum) == type('') ):
            self.header[2] = '&REMOTE_SERVER::init(\"%s\");\n'%clusterNum
        #####

        if ( self.perlFile[:2] != './' ):
            self.perlFile    = join('.', self.perlFile )
        #####
        self.perlCommand = [self.perlFile]
        
        self.runCases()

    def writePerlFile(self, commands):
        deleteFile(self.perlFile)
        fileHandle = open( self.perlFile, 'w' )
        fileHandle.write( ''.join(self.header) )
        for item in commands:
            if ( self.showCommands ):
                stderr.write( '%s\n'%item )
            #####
            if ( item.count("'") > 0 ):
                fileHandle.write( '&REMOTE_SERVER::add_command(\"%s\");\n'%item )
            else:
                fileHandle.write( "&REMOTE_SERVER::add_command(\'%s\');\n"%item )
            #####
        #####
        fileHandle.write( "&REMOTE_SERVER::finish_command_queue();\n" )
        fileHandle.close()
        chmod( self.perlFile, 0777 )
        return

    def queueJobs(self, commands):
        self.writePerlFile( commands )
        queueingJobs( self.perlCommand, 1, sleepTime = 1.0 )
        return

    def runCases(self):
        if ( type(self.clusterNum) == type(0) ):
            stderr.write( 'Executing remoteServer4 on cluster %d\n'% \
                                                     self.clusterNum )
        elif ( type(self.clusterNum) == type('') ):
            stderr.write( 'Executing remoteServer4 on cluster %s\n'% \
                                                     self.clusterNum )
        #####
        # Executing the command queue
        commands       = []
        commandCounter = 1
        stderr.write( '\t\t-Building the command list.\n' )
        for command in self.commandList:
            commands.append( command )
            if ( fmod(commandCounter, self.queueSize) == 0.0):
                self.queueJobs( commands )
                commands = []
            #####
            commandCounter += 1
        #####
        # Executing the remainder
        if ( commands != [] ):
            self.queueJobs( commands )
        #####
        return

#####
