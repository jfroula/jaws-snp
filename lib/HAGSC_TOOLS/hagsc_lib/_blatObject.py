
__author__="jjenkins"
__date__ ="$Jan 4, 2010 9:38:10 AM$"

# Local Library Imports
from _globalSettings import gzip_path
from _globalSettings import blatEXE

from _helperFunctions import commaSplit

# Python Library Imports
from subprocess import Popen

from sys import platform
from sys import stdout

from os.path import split

########################################################################
class blat_Entry_class(object):
    def __init__( self, match, misMatch, repMatch, Ns, Q_gap_count, \
                        Q_gap_bases, T_gap_count, T_gap_bases, strand, \
                        Q_name, Q_size, Q_start, Q_end, T_name, T_size, \
                        T_start, T_end, blockCount, blockSizes, Q_starts, \
                        T_starts ):

        self.match    = match
        self.misMatch = misMatch
        self.repMatch = repMatch

        self.Ns = Ns

        self.Q_gap_count = Q_gap_count
        self.Q_gap_bases = Q_gap_bases

        self.T_gap_count = T_gap_count
        self.T_gap_bases = T_gap_bases

        self.strand = strand

        self.Q_name  = Q_name
        self.Q_size  = Q_size
        self.Q_start = Q_start
        self.Q_end   = Q_end

        self.T_name  = T_name
        self.T_size  = T_size
        self.T_start = T_start
        self.T_end   = T_end

        self.blockCount = blockCount
        self.blockSizes = blockSizes

        self.Q_starts = Q_starts
        self.T_starts = T_starts

    def __str__(self):
        outputString = 22 * ['']
        outputString[0] = '%d\t'%self.match
        outputString[1] = '%d\t'%self.misMatch
        outputString[2] = '%d\t'%self.repMatch
        outputString[3] = '%d\t'%self.Ns
        outputString[4] = '%d\t'%self.Q_gap_count
        outputString[5] = '%d\t'%self.Q_gap_bases
        outputString[6] = '%d\t'%self.T_gap_count
        outputString[7] = '%d\t'%self.T_gap_bases
        outputString[8] = '%s\t'%self.strand

        outputString[9]  = '%s\t'%self.Q_name
        outputString[10] = '%d\t'%self.Q_size
        outputString[11] = '%d\t'%self.Q_start
        outputString[12] = '%d\t'%self.Q_end

        outputString[13] = '%s\t'%self.T_name
        outputString[14] = '%d\t'%self.T_size
        outputString[15] = '%d\t'%self.T_start
        outputString[16] = '%d\t'%self.T_end

        outputString[17] = '%d\t'%self.blockCount

        outputString[18] = '%s,\t'%(','.join( map(str, self.blockSizes) ) )
        outputString[19] = '%s,\t'%(','.join( map(str, self.Q_starts) ) )
        outputString[20] = '%s,\t'%(','.join( map(str, self.T_starts) ) )
        
        outputString[21] = '\n'

        return ''.join(outputString)

    def __repr__(self):
        return str(self)

#####

########################################################################
def iterBLAT( blatFileHandle ):
    """
    iterBLAT( blatFile )

    Generator function to iterate over entries in a blat file, returning
    blat_Entry_class objects.
    """

#     # Skipping the first 5 lines if necessary
#     firstLine = blatFileHandle.readline()
#     splitLine = firstLine.split(None)
#     try:
#         if ( splitLine[0] == 'psLayout' ):
#             for n in xrange(4): blatFileHandle.readline()
#         else:
#             # Rewind the file handle unless it is stdout
#             try:
#                 blatFileHandle.seek(0)
#             except IOError:
#                 pass
#             #####
#         #####
#     except IndexError:
#         pass
#     #####

    # Reading in the rest of the file
    notDone = True
    while ( notDone ):

        line = blatFileHandle.readline()
        if ( not line ):
            notDone = False
            break
        #####
        
        parsedLine = line.split(None)
        
        if ( line[0] == 'pslLayout' ):
            for n in xrange(4):
                blatFileHandle.readline()
            #####
            line = blatFileHandle.readline()
            if ( not line ):
                notDone = True
                break
            #####
        #####
        
        try:
            # Pulling the information
            match    = int( parsedLine[0] )
            misMatch = int( parsedLine[1] )
            repMatch = int( parsedLine[2] )
            Ns       = int( parsedLine[3] )
    
            Q_gap_count = int( parsedLine[4] )
            Q_gap_bases = int( parsedLine[5] )
            T_gap_count = int( parsedLine[6] )
            T_gap_bases = int( parsedLine[7] )
            strand      = parsedLine[8]
            
            Q_name  = parsedLine[9]
            Q_size  = int( parsedLine[10] )
            Q_start = int( parsedLine[11] )
            Q_end   = int( parsedLine[12] )
            
            T_name  = parsedLine[13]
            T_size  = int( parsedLine[14] )
            T_start = int( parsedLine[15] )
            T_end   = int( parsedLine[16] )
            
            blockCount = int( parsedLine[17] )
            blockSizes = [int(item) for item in commaSplit(parsedLine[18]) ]
            Q_starts   = [int(item) for item in commaSplit(parsedLine[19]) ]
            T_starts   = [int(item) for item in commaSplit(parsedLine[20]) ]

            # Adjusting the coordinates if necessary
            if ( strand == '+-' ):
                T_starts = [(T_size - T_starts[n]) for n in xrange(len(T_starts))]
                strand = '-'
            elif ( strand == '-+' ):
                Q_starts = [(Q_size - Q_starts[n]) for n in xrange(len(Q_starts))]
                strand = '-'
            #####
        except ValueError:
            continue
        except IndexError:
            continue
        #####
        
        # Yield up the record and wait for next iteration
        yield blat_Entry_class( match, misMatch, repMatch, Ns, Q_gap_count, \
                             Q_gap_bases, T_gap_count, T_gap_bases, strand, \
                             Q_name, Q_size, Q_start, Q_end, T_name, T_size, \
                             T_start, T_end, blockCount, blockSizes, Q_starts, \
                             T_starts )
    #####

    # Completed reading the file
    return

#####

########################################################################
class blatObject(object):

    """
    Aids in the handling of blat job submission.
    """

    def __init__( self, fastaFile, assemblyFile, outputFile=None ):
 
        self.initializeObject()

        self.fastaFile    = fastaFile
        self.assemblyFile = assemblyFile

        if ( outputFile == None ):
            fastaPath, fastaBaseName = split( fastaFile )
            self.outputFile = '%s.blat' % ( fastaBaseName )
        else:
            self.outputFile = outputFile
        #####

#-------------------------------------------------------------------------
#
#    blat - Standalone BLAT v. 32x1 fast sequence search command line tool
#    usage:
#    blat database query [-ooc=11.ooc] output.psl
#    where:
#    database and query are each either a .fa , .nib or .2bit file,
#    or a list these files one file name per line.
#    -ooc=11.ooc tells the program to load over-occurring 11-mers from
#               and external file.  This will increase the speed
#               by a factor of 40 in many cases, but is not required
#    output.psl is where to put the output.
#    Subranges of nib and .2bit files may specified using the syntax:
#      /path/file.nib:seqid:start-end
#    or
#      /path/file.2bit:seqid:start-end
#    or
#      /path/file.nib:start-end
#    With the second form, a sequence id of file:start-end will be used.
#    options:
#    -t=type     Database type.  Type is one of:
#                 dna - DNA sequence
#                 prot - protein sequence
#                 dnax - DNA sequence translated in six frames to protein
#               The default is dna
#    -q=type     Query type.  Type is one of:
#                 dna - DNA sequence
#                 rna - RNA sequence
#                 prot - protein sequence
#                 dnax - DNA sequence translated in six frames to protein
#                 rnax - DNA sequence translated in three frames to protein
#               The default is dna
#    -prot       Synonymous with -t=prot -q=prot
#    -ooc=N.ooc  Use overused tile file N.ooc.  N should correspond to
#               the tileSize
#    -tileSize=N sets the size of match that triggers an alignment.
#               Usually between 8 and 12
#               Default is 11 for DNA and 5 for protein.
#    -stepSize=N spacing between tiles. Default is tileSize.
#    -oneOff=N   If set to 1 this allows one mismatch in tile and still
#               triggers an alignments.  Default is 0.
#    -minMatch=N sets the number of tile matches.  Usually set from 2 to 4
#               Default is 2 for nucleotide, 1 for protein.
#    -minScore=N sets minimum score.  This is the matches minus the
#               mismatches minus some sort of gap penalty.  Default is 30
#    -minIdentity=N Sets minimum sequence identity (in percent).  Default is
#               90 for nucleotide searches, 25 for protein or translated
#               protein searches.
#    -maxGap=N   sets the size of maximum gap between tiles in a clump.  Usually
#               set from 0 to 3.  Default is 2. Only relevent for minMatch > 1.
#    -noHead     suppress .psl header (so it's just a tab-separated file)
#    -makeOoc=N.ooc Make overused tile file. Target needs to be complete genome.
#    -repMatch=N sets the number of repetitions of a tile allowed before
#               it is marked as overused.  Typically this is 256 for tileSize
#               12, 1024 for tile size 11, 4096 for tile size 10.
#               Default is 1024.  Typically only comes into play with makeOoc
#    -mask=type  Mask out repeats.  Alignments won't be started in masked region
#               but may extend through it in nucleotide searches.  Masked areas
#               are ignored entirely in protein or translated searches. Types are
#                 lower - mask out lower cased sequence
#                 upper - mask out upper cased sequence
#                 out   - mask according to database.out RepeatMasker .out file
#                 file.out - mask database according to RepeatMasker file.out
#    -qMask=type Mask out repeats in query sequence.  Similar to -mask above but
#               for query rather than target sequence.
#    -repeats=type Type is same as mask types above.  Repeat bases will not be
#               masked in any way, but matches in repeat areas will be reported
#               separately from matches in other areas in the psl output.
#    -minRepDivergence=NN - minimum percent divergence of repeats to allow
#               them to be unmasked.  Default is 15.  Only relevant for
#               masking using RepeatMasker .out files.
#    -dots=N     Output dot every N sequences to show program's progress
#    -trimT      Trim leading poly-T
#    -noTrimA    Don't trim trailing poly-A
#    -trimHardA  Remove poly-A tail from qSize as well as alignments in
#               psl output
#    -fastMap    Run for fast DNA/DNA remapping - not allowing introns,
#               requiring high %ID
#    -out=type   Controls output file format.  Type is one of:
#                   psl - Default.  Tab separated format, no sequence
#                   pslx - Tab separated format with sequence
#                   axt - blastz-associated axt format
#                   maf - multiz-associated maf format
#                   sim4 - similar to sim4 format
#                   wublast - similar to wublast format
#                   blast - similar to NCBI blast format
#                   blast8- NCBI blast tabular format
#                   blast9 - NCBI blast tabular format with comments
#    -fine       For high quality mRNAs look harder for small initial and
#               terminal exons.  Not recommended for ESTs
#    -maxIntron=N  Sets maximum intron size. Default is 750000
#    -extendThroughN - Allows extension of alignment through large blocks of N's
#-------------------------------------------------------------------------

        # Attribute mapping
        self.att2param = { \
                          'dna_blat'            : '-t=dna -q=dna', \
                          'protein_blat'        : '-prot', \
                          'database_type'       : '-t=', \
                          'query_type'          : '-q=', \
                          'overuseTileFile'     : '-ooc=', \
                          'tileSize'            : '-tileSize=', \
                          'stepSize'            : '-stepSize=', \
                          'oneOff'              : '-oneOff=', \
                          'minMatch'            : '-minMatch=', \
                          'minScore'            : '-minScore=', \
                          'minIdentity'         : '-minIdentity=', \
                          'maxGap'              : '-maxGap=', \
                          'noHeader'            : '-noHead', \
                          'makeOveruseTileFile' : '-makeOoc=', \
                          'repMatch'            : '-repMatch=', \
                          'maskType'            : '-mask=', \
                          'maskQueryRepeats'    : '-qMask=', \
                          'repeats'             : '-repeats=', \
                          'minimumDivergence'   : '-minRepDivergence=', \
                          'programProgress'     : '-dots=', \
                          'trimLeadingPoly_T'   : '-trimT', \
                          'noTrimTrailPoly_A'   : '-noTrimA', \
                          'removePolyATail'     : '-trimHardA', \
                          'fastDNA_remap'       : '-fastMap', \
                          'smallTerminalExon'   : '-fine', \
                          'maxIntronSize'       : '-maxxIntronSize', \
                          'extendThrough_N'     : '-extendThroughN', \
                          'outputFileType'      : '-out=' \
                          }
        return

    #####

    def initializeObject(self):
        self.assemblyFile = None
        self.outputFile   = None
        self.fastaFile    = None
        self.blatEXE      = blatEXE
        return

    def oocCommandString(self, keyWords=None):
        # Applying the keywords
        defaults = { 'makeOveruseTileFile':'11.ooc' }
        finalKeyWords = {}
        for key, value in defaults.items(): finalKeyWords[key] = value
        if ( keyWords == None ): keyWords = {}
        for key, value in keyWords.items(): finalKeyWords[key] = value
        # Initial command line string
        cline = ['%s '%self.blatEXE]
        for key, value in finalKeyWords.items():
            if ( value == None ):
                cline.append( '%s '%self.att2param[key] )
            else:
                cline.append( '%s%s '%(self.att2param[key], str(value)) )
            #####
        #####
        cline.append('%s '%self.assemblyFile)
        cline.append('%s '%self.assemblyFile)
        if ( keyWords.has_key('makeOveruseTileFile') ):
            cline.append( '%s.dat'%keyWords['makeOveruseTileFile'] )
        else:
            cline.append( '%s.dat'%defaults['makeOveruseTileFile'] )
        #####
        return ''.join( cline )

    def commandString(self, keyWords=None, gzipOutput=True):

        # Applying the keywords
        if ( keyWords == None ): keyWords = {}
        finalKeyWords = {}
        for key, value in keyWords.items(): finalKeyWords[key] = value

        # Initial command line string
        cline = ['%s '%self.blatEXE]
        for key, value in finalKeyWords.items():
            if ( value == None ):
                cline.append( '%s '%self.att2param[key] )
            else:
                cline.append( '%s%s '%(self.att2param[key], str(value)) )
            #####
        #####
        cline.append('%s '%self.assemblyFile)
        cline.append('%s '%self.fastaFile)

        # Adding gzip portion
        if ( gzipOutput ):
            cline.append( 'stdout | %s -c >  %s.gz'%( gzip_path, \
                                                           self.outputFile ) )
        else:
            cline.append( '%s'%self.outputFile )
        #####

        return ''.join( cline )

    def runBLAT( self, keyWords=None ):
        stdout.write( 'BLATTING %s\n' % self.fastaFile )
        return Popen( self.commandString(keyWords), shell=(platform!="win32") )

#####

