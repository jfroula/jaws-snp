
__author__="mqualls"
__date__ ="$Oct 21, 2010$"

from string import lower
import re
from sys import argv
from sys import stderr
from itertools import izip
from itertools import count

from _globalSettings import nucMap
from _QUAL_IO import fasta_file
from _QUAL_IO import objectify
from _QUAL_IO import write_fasta_record

#map k-mers to scaffold/location

def generate_kmers(seq, k):
    for i in xrange(0, len(seq)-k):
        yield seq[i:i+k], i
    
def catalog_kmers(fasta, k, compressor):
    catalog = {}
    reverse = {}
    for record in fasta():
        id, info = record.id, record.info
        print >>stderr, id, info
        seq = record.data()
        if id not in reverse: reverse[id] = []
        for kmer1, offset in generate_kmers(seq, k):
            kmer = compressor(kmer1.upper())
            if kmer not in catalog: catalog[kmer] = 0 #[]
            catalog[kmer] += 1 #.append((record, offset))
            reverse[id].append((kmer, offset))
    return catalog, reverse
    
#lowercase mask k-mers that occur more than repeat threshold

repeat_threshold = lambda threshold, catalog : (lambda kmer: len(catalog[kmer[0]]) > threshold)

complement = (lambda seq : ''.join( [nucMap[char] for char in seq] ))
reverse_complement = (lambda seq : ''.join( [nucMap[char] for char in reversed(seq)] ))

"""def repeat_threshold_with_rcomplements(threshold, catalog, compressor, decompressor):
    return (lambda kmer : len(catalog[kmer[0]]) >= threshold or len(catalog[kmer[0]]) + len(catalog.get(compressor(reverse_complement(decompressor(kmer[0]))), [])) >= threshold)"""
    
def repeat_threshold_with_rcomplements(threshold, catalog, compressor, decompressor):
    return (lambda kmer : catalog[kmer[0]] >= threshold or catalog[kmer[0]] + catalog.get(compressor(reverse_complement(decompressor(kmer[0]))), 0) >= threshold)
    
def filter_kmers(scaffolds, test):
    for id, matches in scaffolds.iteritems():
        scaffolds[id] = filter(test, matches)
        
def mask_kmers_by_scaff(fasta, scaffolds, masker, decompressor):
    for record in fasta():
        id = record.id
        seq = [char for char in record.data()]
        kmers = scaffolds[id]
        for match in kmers:
            kmer1, offset = match
            kmer = decompressor(kmer1)
            length = len(kmer)
            seq[offset:offset+length] = masker(seq[offset:offset+length])
        yield id, ''.join(seq)

def mask_pattern(seq, pattern_rx, masker):
    modified = [char for char in seq]
    change = False
    for match in pattern_rx.finditer(seq):
        change = True
        start, end = match.span()
        modified[start:end] = masker(modified[start:end])
    if change:
        return ''.join(modified)
    else:
        return None
    
def mask_fasta(fasta, pattern_rx, masker):
    for record in fasta():
        id, seq, info = record.id, record.data(), record.info
        #yield build_record_f(id, (lambda: mask_pattern(seq, pattern_rx, masker)), info)
        modified = mask_pattern(seq, pattern_rx, masker)
        if modified:
            yield build_record(id, modified, info)
        else:
            yield record
            
#locate lowercase sequences longer than length threshold, map to scaffold/location

def build_record(id, data="", info=""):
    return objectify({"id":id, "info":info, "data":(lambda : data)})
    
def build_record_f(id, data, info=""):
    return objectify({"id":id, "info":info, "data":data})

def rx_lowercase_length(length):
    return re.compile("[atcg](?=[atcg]{%d,})" % (length - 1))

def catalog_longmers_lcase(masked, length, compressor):
    rx = rx_lowercase_length(length)
    catalog = {}
    reverse = {}
    for record in masked():
        id, seq = record.id, record.data()
        print >>stderr, id
        reverse[id] = []
        for match in rx.finditer(seq):
            pos = match.start()
            longmer1 = seq[pos:pos+length]
            longmer = compressor(longmer1.upper())
            if longmer not in catalog: catalog[longmer] = 0#[]
            catalog[longmer] += 1 #.append( (id, pos) )
            reverse[id].append((longmer, pos))
    return catalog, reverse
    
#for each longmer, if above repeat threshold add to mask set, else if combined with reverse is above threshold add to mask set
#mask set with X

char_masker = (lambda char : (lambda seq : len(seq) * char) )

def mask_shortmers_lcase(fasta, length, repeats, compressor, decompressor):
    print >> stderr, "Cataloging %d-mers..."%length
    kmers, scaffolds = catalog_kmers(fasta, length, compressor)
    print >> stderr, "Masking %d-mers..."%length
    filter_kmers(scaffolds, repeat_threshold_with_rcomplements(repeats, kmers, compressor, decompressor))
    masked_scaffolds = mask_kmers_by_scaff(fasta, scaffolds, (lambda collection: [lower(char) for char in collection]), decompressor)
    for k,v in masked_scaffolds:
        print >> stderr, k
        yield build_record(k,v)
    
def mask_longmers_lcase(fasta, length, repeats, compressor, decompressor):
    print >> stderr, "Cataloging %d-mers..."%length
    longmers, scaffolds = catalog_longmers_lcase(fasta, length, compressor)
    print >> stderr, "Masking %d-mers..."%length
    filter_kmers(scaffolds, repeat_threshold_with_rcomplements(repeats, longmers, compressor, decompressor))
    masked_longmers = mask_kmers_by_scaff(fasta, scaffolds, char_masker("X"), decompressor)
    longmers, scaffolds = None, None
    print >> stderr, "Generating output..."
    for k,v in masked_longmers:
        print >> stderr, k
        yield build_record(k,v)

def mask_to_dict(fasta, length, repeats, masker, compressor=(lambda x: x), decompressor=(lambda x: x)):
    d = {}
    for record in masker(fasta, length, repeats, compressor, decompressor):
        d[record.id] = record
    return d
    
def mask_to_file(fasta, length, repeats, handle, masker, compressor=(lambda x: x), decompressor=(lambda x: x)):
    for record in masker(fasta, length, repeats, compressor, decompressor):
        write_fasta_record(record, handle)
        

############################################

def rx_repeat(sequence, length):
    return re.compile("(%s){%d,}"%(sequence,length))

def analyze_n_ends(seq):
    if len(seq) < 101: return
    start = seq.count('N',0,100) + seq.count('n',0,100)
    end = seq.count('N',-100) + seq.count('n',-100)
    if (start > 0) or (end > 0): return " - N ends: (%f, %f)"%(float(start)/100.0, float(end)/100.0)
    return ""
  
x_to_n_rx = re.compile("[Xx]+")
n_masker = char_masker('N')
def break_post(segment):
    return ((mask_pattern(segment, x_to_n_rx, n_masker) or segment).strip('Nn')), analyze_n_ends(segment)
    
def break_scaffold(id, seq, break_seq, break_lengths, min_seq=0, post=None, id_formatter = (lambda id, num : "%s(%d)"%(id, num))):
    cursors = [0] * len(break_lengths)
    break_on_rx = rx_repeat(break_seq, min(break_lengths))
    
    for match in break_on_rx.finditer(seq):
        start, end = match.span()
        match_length = end - start
        for break_length, cursor in izip(break_lengths, count(0)):
            if match_length >= break_length:
                segment = seq[cursors[cursor]:start]
                info = ""
                if post: 
                    segment2, info = post(segment)
                    segment, diff = segment2, len(segment) - len(segment2)
                else:
                    info, diff = "", 0
                if len(segment) >= min_seq:
                    yield break_length, id_formatter(id,cursors[cursor]+diff), segment, info
                cursors[cursor] = end+1
    
    for break_length, cursor in izip(break_lengths, cursors):
        if cursor >= len(seq): continue
        segment = seq[cursor:]
        info = ""
        if post: 
            segment2, info = post(segment)
            segment, diff = segment2, len(segment) - len(segment2)
        else:
            info, diff = "", 0
        last_id = (cursor>0) and id_formatter(id,cursor+diff) or id
        if len(segment) >= min_seq:
            yield break_length, last_id, segment, info
            
def break_fasta(fasta, break_seq, break_length, min_seq=0, post=None):
    for record in fasta():
        for _break_length, _id, _seq, _info in break_scaffold(record.id, record.data(), break_seq, [break_length], min_seq, post):
            yield build_record(_id, _seq, record.info+_info)
            
def break_fasta_to_files(fasta, break_seq, min_seq, break_lengths, output_handles, post=None):
    filetable = {}
    for length,file in izip(break_lengths, output_handles): filetable[length] = file
    print filetable
    
    for record in fasta():
        print >>stderr, record.id
        data = record.data()
        for break_length, id, seq, info in break_scaffold(record.id, data, break_seq, break_lengths, min_seq, post):
            write_fasta_record(build_record(id, seq, record.info+info), handle=filetable[break_length])
    
def filter_fasta(fasta, test):
    for record in fasta():
        if test(record): yield record
        
rx_split_scaff_name = re.compile(r"(?P<numbers>(\)\d+\()*)(?P<name>.*)")  #backwards to be greedy from the end
rx_paren_numbers = re.compile(r"\((?P<number>\d+)\)")
def base_scaff_name(split_scaff_name):
    match = rx_split_scaff_name.match(split_scaff_name[::-1])
    name = match.group("name")[::-1]
    number_string = match.group("numbers")[::-1]
    numbers = []
    for match in rx_paren_numbers.finditer(number_string):
        numbers.append(int(match.group("number")))
    return name, numbers

############################################


def real_main():
    in_name = arg[1]
    
    infile = open(in_name, "r")
    fasta = fasta_file(infile)
    
    break_sequence = 'X'
    break_lengths = [35000, 8000, 4000, 500]
    outfiles = [open("%s.break-%d.fasta"%(in_name,b), "w") for b in break_lengths]
    
    break_scaffolds(fasta, break_sequence, break_lengths, outfiles)
    
              
############################################
             
               
def profile_main():
    from cProfile import Profile
    from pstats import Stats
    prof  = Profile().runctx("real_main()", globals(), locals())
    stats = Stats( prof ).sort_stats("time").print_stats(60)
    return

arg = argv
if __name__ == "__main__":
    profile_main()
#real_main()