
def bp_composition(sequence, bases="ATCGXN"):
    counter = {}.fromkeys(bases, 0)
    for char in sequence:
        try:
            counter[char.upper()] += 1
        except:
            pass
    return counter
    
