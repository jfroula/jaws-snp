__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from _helperFunction import testFile, testDirectory, deleteDir, deleteFile

from _helperFunction import generateTmpDirName, baseFileName, getFiles

from _helperFunction import isGzipFile, isBzipFile

from _FASTA_IO import iterFASTA, writeFASTA, equal_read_split

from _jobQueueing import remoteServer4

from _globalSettings import gzip_path

from os.path import join, abspath, curdir

from os import mkdir

from sys import stderr, stdout

import gzip

########################################################################
class batch_base_class(object):

    def __init__(self, fastaFile, \
                       nFastaFiles, \
                       outputFileName=None ):

        # Make sure the files exist
        testFile(fastaFile)

        # Reading in the control variables
        self.fastaFile         = fastaFile
        self.nFastaFiles       = nFastaFiles

        # Generating the directory information
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                     baseFileName(self.fastaFile) )

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        equal_read_split( self.fastaFile, \
                          self.baseOutFileName, \
                          self.nFastaFiles )
        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

########################################################################
class batch_run_class( setup_base_class ):
    def __init__(self, queryFile, \
                       targetFile, \
                       nFastaFiles, \
                       clusterNum, \
                       outputFileName, \
                       formattedCommand ):

        ## Initializing the best hit base class
        setup_base_class.__init__( self, \
                                   queryFile, \
                                   nFastaFiles )
        
        # Reading in the information
        self.queryFile        = queryFile
        self.targetFile       = targetFile
        self.clusterNum       = clusterNum
        self.outputFileName   = outputFileName
        self.formattedCommand = formattedCommand

    def executeCases(self):
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Break the fasta file into little pieces
        self.splitFASTA()
        # Step 4:  BLAT Fastas
        self.sendToCluster()
        # Step 5: Parse Output
        self.parseOutput()
        # Step 7: Cleanup
        self.clean_tmp()
        return

    def sendToCluster(self):
    
        stderr.write( '\t-Aligning Reads\n')

        # (1)  Set up the command list
        commandList = []
        stringFormtDict = { 'target':self.targetFile, 'query':None, 'tmpOutput':None }
        for queryFile in getFiles( 'fasta', self.tmpPath ):
            testFile( queryFile )
            localOutputFile = '%s.align'%queryFile
            stringFormtDict['query'] = queryFile
            stringFormatDict['tmpOutput'] = localOutputFile
            commandList.append( self.formattedCommand % stringFormatDict )
        #####
        
        # (2)  Run the cases
        perlFile = 'batch_run.pl'
        remoteServer4( commandList, self.nFastaFiles, self.clusterNum, perlFile )
        deleteFile(perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        deleteFile(self.outputFileName)
        oh = open( self.outputFileName, 'w' )
        for alignFile in getFiles( 'align', self.tmpPath ):
            if ( isGzipFile(alignFile) ):
                tmpProcess = 
            elif ( isBzipFile(alignFile) ):
                tmpProcess = 
            else:
                tmpProcess = 
            #####
            alignHandle = tmpProcess.stdout
            map( oh.write, alignHandle )
            tmpProcess.poll()
        #####
        oh.close()
        return

#==============================================================
def real_main():

    # Controlling parameters
    queryFASTA     = ''
    targetFASTA    = ''
    nFastaFiles    = 50
    clusterNum     = 101
    outputFileName = ''
    
    main_run_class( queryFASTA, \
                    targetFASTA, \
                    nFastaFiles, \
                    clusterNum, \
                    outputFileName, \
                    formattedCommand ).executeCases()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
