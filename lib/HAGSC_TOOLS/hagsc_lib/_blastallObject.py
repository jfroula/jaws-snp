
__author__  = "Jerry Jenkins"
__date__    = "$Nov 30, 2009 10:59:03 AM$"

## Local Library Imports ##
from _globalSettings import gzip_path
from _globalSettings import blastall_path

from _helperFunctions import testFile

## Python Library Imports ##
from subprocess import Popen

from sys import platform
from sys import stdout

from os.path import exists
from os.path import split

########################################################################
class blastallObject():
    """
    Aids in the handling a blast submission.
    """
    def __init__(self, fastaFile, assemblyFile, outputFile=None):

        self.initializeObject()

        self.fastaFile    = fastaFile
        self.assemblyFile = assemblyFile

        if ( outputFile == None ):
            fastaPath, fastaBaseName = split( fastaFile )
            self.XML_fileName = '%s.xml' % ( fastaBaseName )
        else:
            self.XML_fileName = outputFile
        #####

#----------------------------------------------------------------------------
#        Scoring
#    matrix              Matrix to use.
#    gap_open            Gap open penalty.
#    gap_extend          Gap extension penalty.
#    nuc_match           Nucleotide match reward.  (BLASTN)
#    nuc_mismatch        Nucleotide mismatch penalty.  (BLASTN)
#    query_genetic_code  Genetic code for Query.
#    db_genetic_code     Genetic code for database.  (TBLAST[NX])
#
#        Algorithm
#    gapped              Whether to do a gapped alignment. T/F (not for TBLASTX)
#    expectation         Expectation value cutoff.
#    wordsize            Word size.
#    strands             Query strands to search against database.([T]BLAST[NX])
#    keep_hits           Number of best hits from a region to keep.
#    xdrop               Dropoff value (bits) for gapped alignments.
#    hit_extend          Threshold for extending hits.
#    region_length       Length of region used to judge hits.
#    db_length           Effective database length.
#    search_length       Effective length of search space.
#
#        Processing
#    filter            Filter query sequence for low complexity (with SEG)?  T/F
#    believe_query     Believe the query defline.  T/F
#    restrict_gi       Restrict search to these GI's.
#    nprocessors       Number of processors to use.
#    oldengine         Force use of old engine T/F
#
#        Formatting
#    html                Produce HTML output?  T/F
#    descriptions        Number of one-line descriptions.
#    alignments          Number of alignments.
#    align_view          Alignment view.  Integer 0-11,
#                        passed as a string or integer.
#    show_gi             Show GI's in deflines?  T/F
#    seqalign_file       seqalign file to output.
#    outfile             Output file for report.  Filename to write to, if
#                        ommitted standard output is used (which you can access
#                        from the returned handles).
#----------------------------------------------------------------------------

        # Attribute mapping
        self.att2param = {
                          'align_view'         : '-m',
                          'alignments'         : '-b',
                          'believe_query'      : '-J',
                          'database'           : '-d',
                          'db_genetic_code'    : '-D',
                          'db_length'          : '-z',
                          'descriptions'       : '-v',
                          'expectation'        : '-e',
                          'filter'             : '-F',
                          'gap_extend'         : '-E',
                          'gap_open'           : '-G',
                          'gapped'             : '-g',
                          'hit_extend'         : '-f',
                          'html'               : '-T',
                          'infile'             : '-i',
                          'keep_hits'          : '-K',
                          'matrix'             : '-M',
                          'nprocessors'        : '-a',
                          'nuc_match'          : '-r',
                          'nuc_mismatch'       : '-q',
                          'oldengine'          : '-V',
                          'outfile'            : '-o',
                          'program'            : '-p',
                          'query_genetic_code' : '-Q',
                          'region_length'      : '-L',
                          'restrict_gi'        : '-l',
                          'search_length'      : '-Y',
                          'seqalign_file'      : '-O',
                          'show_gi'            : '-I',
                          'strands'            : '-S',
                          'wordsize'           : '-W',
                          'xdrop'              : '-X'
                          }
        return

    #####

    def initializeObject(self):
        ## Initializing the object ##
        self.keyWords     = None
        self.XML_fileName = None
        self.fastaFile    = None
        self.blastallEXE  = blastall_path
        return

    def set_blastall_path(self, incomingPath):
#        testFile(incomingPath)
        self.blastallEXE = incomingPath
        return

    def commandString( self, keyWords=None, gzipOutput=True ):

        # Setting up default values
        defaults = {'program'    : 'blastn',
                    'database'   : self.assemblyFile,
                    'infile'     : self.fastaFile,
                    'align_view' : str(7),  # Specifies XML Output
                    'expectation': str(1.0e-2)
                    }
        if ( not gzipOutput ):
            defaults['outfile'] = self.XML_fileName
        #####
        
        # Setting up the final keywords
        finalKeyWords = {}
        for key, value in defaults.items(): finalKeyWords[key] = value

        # Incorporating user keywords
        if ( keyWords == None ): keyWords = {}
        for key, value in keyWords.items():
            # Does not produce an output file through "-o" if gzipping
            if ( (key == 'outfile') and gzipOutput ):
                pass
            else:
                finalKeyWords[key] = value
            #####
        #####
        
#         if not exists( self.blastallEXE ):
#             raise ValueError( "BLAST executable does not " + \
#                               "exist at %s" % self.blastallEXE )
#         #####

        # Building the command line list
        cline = ['%s '%self.blastallEXE]
        for key, value in finalKeyWords.items():
            if ( value == None ):
                cline.append( '%s '%self.att2param[key] )
            else:
                cline.append( '%s %s '%(self.att2param[key], str(value)) )
            #####
        #####

        # Adding gzip portion
        if ( gzipOutput ):
            cline.append( ' | %s -c >  %s.gz'%( gzip_path, \
                                             self.XML_fileName ) )
        #####

        # Echoing the command to the user
        stdout.write( "%s\n"%( ''.join(cline) ) )

        return ''.join( cline )

    #####

    def runBLASTALL(self, keyWords=None):
        stdout.write('BLASTING %s\n'%self.fastaFile )
        self.keyWords = keyWords
        return Popen( self.commandString(keyWords), \
                      shell=(platform!="win32") )

#####

########################################################################
def createBLASTCommandLineFile(fastaFiles, commandLineFileName, \
                               assemblyFile, blast_keyWords=[], \
                               blastEXE=blastall_path ):
    """
    createBLASTCommandLineFile( fastaFiles, commandLineFileName, \
                                assemblyFile, blast_keyWords = [], \
                                blastEXE = blastall_path )

    Creates a file containing a series of blast commands.
    """

    # Generating the list of commands
    commandList = []
    for fastaFile in fastaFiles:

        testFile(fastaFile)

        commandList.append( blastallObject(fastaFile, assemblyFile \
                              ).commandString(blast_keyWords, gzipOutput=True) )

    # Writing the commandLineFile file
    if ( commandList != [] ):
        fileHandle = open(commandLineFileName, 'w')
        fileHandle.write(''.join(['\n'.join(commandList), '\n']))
        fileHandle.close()

    return

