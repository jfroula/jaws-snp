__author__="jjenkins"
__date__ ="$Apr 16, 2010 10:37:47 PM$"

from _FASTA_IO import equal_size_split
from _FASTA_IO import writeFASTA
from _FASTA_IO import SeqRecord

from _jobQueueing import extractScaffoldNames
from _jobQueueing import remoteServer4

from _helperFunctions import createBLATHitFileName
from _helperFunctions import createBestHitFileName
from _helperFunctions import testDirectory
from _helperFunctions import deleteFile
from _helperFunctions import deleteDir
from _helperFunctions import testFile
from _helperFunctions import getFiles

from _blatObject import blatObject
from _blatObject import iterBLAT

from _globalSettings import defaultCluster

from _blastallObject import blastallObject

from _helperFunctions import generateTmpDirName
from _helperFunctions import baseFileName

from _helperClasses import fileHeader_Class

import HAGSC_XML

from os.path import splitext
from os.path import abspath
from os.path import isfile
from os.path import isdir
from os.path import join

from os import listdir
from os import curdir
from os import mkdir

from sys import stderr

from sys import exc_info

import gzip

########################################################################
def findTelomereCentromere( assemblyFile, \
                            patterns, \
                            repLow=6, \
                            repHigh=10, \
                            userKeyWords=None ):
    
    # Generating the fasta file for analysis
    telomereFile = 'telomere.fasta'
    telomereHandle = open( telomereFile, 'w')
    for pattern in patterns:
        for n in xrange(repLow, repHigh):
            record = SeqRecord( id='%dx%s'%(n,pattern), seq=n*pattern, description='' )
            writeFASTA( [record], telomereHandle )
        #####
    #####
    telomereHandle.close()
    
    # Blatting the file against the assembly
    commandList            = []
    filePre                = baseFileName(assemblyFile)
    blat_outputFileName    = '%s_telomereCentromere.blat'%filePre
    
    # Setting up the keywords
    finalKeyWords = { 'minScore':30, \
                      'extendThrough_N':None, \
                      'repMatch':-1}
    if ( userKeyWords != None ):
        for key, value in userKeyWords.items():
            finalKeyWords[key] = value
        #####
    #####
    commandList.append( \
                        blatObject( telomereFile, \
                                    assemblyFile, \
                                    blat_outputFileName \
                                   ).commandString( finalKeyWords, \
                                                    gzipOutput=False \
                                                   ) \
                       )
    remoteServer4( commandList, 1, defaultCluster )

    # Parse remaining file
    stderr.write( '\t-Parsing output...\n')
    bestHit_outputFileName = '%s_telomereCentromere.out'%filePre
    bestHitFileHandle = open( bestHit_outputFileName, 'w' )
    bestHitFileHandle.write( bestHit_headerString() )
    testFile( blat_outputFileName )
    parseBLATOutput( bestHitFileHandle, \
                     blat_outputFileName, \
                     parseAllOutput=True )
    bestHitFileHandle.close()

    return

########################################################################
class best_Hit_Entry_class(object):
    def __init__(self, BAC_recordName, BAC_size, BAC_start, BAC_end, placDir, \
                       scaffold, scaffSize, scaffStart, scaffEnd, numBases, \
                       per_coverage, per_ID ):
#-----------------------------
# Example best_Hit file output
#-----------------------------
#    Name                    size    start   stop    direction scaffold      size    start   end     #bases  coverage% ID%
#    CH216-044F13.b1.scf     1112    24      310     -       scaffold_28     4420564 4039320 4039605 274     96.14     25.72
#    CH216-036O07.b1.scf     1047    95      623     -       scaffold_38     3962478 1076306 1076830 470     89.69     50.43
#-----------------------------
        self.BAC_recordName = BAC_recordName
        self.BAC_size       = BAC_size
        self.BAC_start      = BAC_start
        self.BAC_end        = BAC_end
        self.placDir        = placDir
        self.scaffold       = scaffold
        self.scaffSize      = scaffSize
        self.scaffStart     = scaffStart
        self.scaffEnd       = scaffEnd
        self.numBases       = numBases
        self.per_ID         = per_ID
        self.per_coverage   = per_coverage

    def __str__(self):
        outputString = 13 * ['']
        outputString[0] = '%s\t'%self.BAC_recordName
        outputString[1] = '%d\t'%self.BAC_size
        outputString[2] = '%d\t'%self.BAC_start
        outputString[3] = '%d\t'%self.BAC_end
        outputString[4] = '%s\t'%self.placDir
        outputString[5] = '%s\t'%self.scaffold
        outputString[6] = '%d\t'%self.scaffSize
        outputString[7] = '%d\t'%self.scaffStart
        outputString[8] = '%d\t'%self.scaffEnd
        outputString[9] = '%d\t'%self.numBases
        outputString[10] = '%5.2f\t'%self.per_ID
        outputString[11] = '%5.2f\t'%self.per_coverage
        outputString[12] = '\n'
        return ''.join(outputString)

    def __repr__(self):
        return str(self)

    def generateString(self):
        return str(self)

#####

########################################################################
def bestHit_headerString():
        # Header Information
        strLen          = 12
        columnLabel     = strLen * ['']
        columnLabel[0]  = 'query_id'
        columnLabel[1]  = 'query_len'
        columnLabel[2]  = 'q_start'
        columnLabel[3]  = 'q_end'
        columnLabel[4]  = 'placDir'
        columnLabel[5]  = 'sbjct_id'
        columnLabel[6]  = 'sbjct_len'
        columnLabel[7]  = 's_start'
        columnLabel[8]  = 's_end'
        columnLabel[9]  = 'id_len'
        columnLabel[10] = r'%_id'
        columnLabel[11] = r'%_cov'
        return fileHeader_Class( columnLabel, \
                                 [], \
                                 tabDelimited=True ).generateHeaderString()

########################################################################
def iterBestHit( bestHitFileHandle ):
    """
    iterBestHit( bestHitFile )

    Generator function to iterate over entries in a bestHit file, returning
    best_Hit_Entry_class objects.
    """

    if isinstance( bestHitFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    noMatchSet = set(['No Match', 'No_Match', 'No_match'])

    for line in bestHitFileHandle:

        parsedLine = line.split(None)
        
        if ( len(parsedLine) < 5 ): continue
        if ( parsedLine[1] in noMatchSet ): continue
        
        # Added for pairAssessment
        if ( parsedLine[1] == 'AMBIGUOUS' ): yield parsedLine

        # Pulling the information
        try:
            BAC_recordName = parsedLine[0]
            BAC_size       = int( parsedLine[1] )
            BAC_start      = int( parsedLine[2] )
            BAC_end        = int( parsedLine[3] )

            placDir        = parsedLine[4]

            scaffold       = parsedLine[5]
            scaffSize      = int( parsedLine[6] )
            scaffStart     = int( parsedLine[7] )
            scaffEnd       = int( parsedLine[8] )

            numBases       = int( parsedLine[9] )

            per_ID         = float( parsedLine[10] )
            per_coverage   = float( parsedLine[11] )
        except ValueError:
            continue
        #####

        # Yield up the record and wait for next iteration
        yield best_Hit_Entry_class( BAC_recordName, BAC_size, BAC_start, \
                                    BAC_end, placDir, scaffold, scaffSize, \
                                    scaffStart, scaffEnd, numBases, \
                                    per_coverage, per_ID )
    #####

    # Completed reading the file
    return

#####

########################################################################
class best_hit_base_class(object):
    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       allowMultipleTopHits=False, \
                       debugMode=False, \
                       tmpDirNum=None, \
                       arachneReadFormat = False ):

        # Make sure the files exist
        testFile(fastaFile)
        testFile(assemblyFile)

        # Reading in the control variables
        self.fastaFile         = fastaFile
        self.assemblyFile      = assemblyFile
        self.nFastaFiles       = nFastaFiles
        self.clusterNum        = clusterNum
        self.cleanUp           = cleanUp
        self.keyWords          = keyWords
        self.debugMode         = debugMode
        self.arachneReadFormat = arachneReadFormat

        # If there are multiple identical top hits, then output them
        self.allowMultipleTopHits = allowMultipleTopHits
        if (  self.allowMultipleTopHits ):
            stderr.write( '\t-ALLOWING MULTIPLE TOP HITS\n')
        #####

        # Generating the directory information
        self.basePath = abspath(curdir)
        if ( self.debugMode ):
            dirListing  = listdir(self.basePath)
            tmpDirs     = filter(lambda x:(x.split('.')[0]=='tmp'), dirListing)
            if ( len(tmpDirs) == 1 ):
                self.tmpDir = tmpDirs[0]
            else:
                raise ValueError( 'Too many tmp directories for debug Mode\n' )
            #####
        else:
            # If we are running a bunch of cases, it is a good idea to renumber
            # the temporary directories instead of reusing the process id
            # Otherwise, you will get a "Stale NFS path" error
            if ( tmpDirNum == None ):
                self.tmpDir = generateTmpDirName()
            else:
                self.tmpDir = generateTmpDirName( tmpDirNum )
            #####
        #####
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                                  baseFileName(self.fastaFile) )

        # Loading in the scaffold Names
        self.scaffoldNameSet = set()

        # Generating the output file name
        if ( outputFileName == None ):
            self.mainOutFile = createBestHitFileName(self.fastaFile)
        else:
            self.mainOutFile = outputFileName
        #####

        # Generating the main bestHit perl file
        self.perlFile = join( self.tmpPath, 'bestHitJob.pl' )

        # Counting the scaffold names
        self.scaffoldNameSet = extractScaffoldNames(self.fastaFile, self.arachneReadFormat)

        # Echo the options to the user
        self.makeOptionsString(tmpDirNum)
        stderr.write( ''.join(self.optionsList) )

    def makeOptionsString(self, tmpDirNum):
        self.optionsList     = 14 * ['']
        self.optionsList[0]  = '------------------\n'
        self.optionsList[1]  = 'Best_Hit Settings:\n'
        self.optionsList[2]  = '------------------\n'
        self.optionsList[3]  = '\t-FASTA                = %s\n'%self.fastaFile
        self.optionsList[4]  = '\t-ASSEMBLY             = %s\n'%self.assemblyFile
        self.optionsList[5]  = '\t-QUEUE SIZE           = %d\n'%self.nFastaFiles
        if ( type(self.clusterNum) == type('') ):
            self.optionsList[6]  = '\t-CLUSTER NUMBER       = %s\n'%self.clusterNum
        elif ( type(self.clusterNum) == type(0) ):
            self.optionsList[6]  = '\t-CLUSTER NUMBER       = %d\n'%self.clusterNum
        #####
            
        self.optionsList[7]  = '\t-CLEANUP?             = %s\n'%self.cleanUp
        self.optionsList[8]  = '\t-OUTPUT FILE NAME     = %s\n'%self.mainOutFile
        # Generate keywords string:
        if ( self.keyWords != None ):
            keyWordString = '\n'
            for key, value in self.keyWords.items():
                keyWordString += '\t\t%s:%s\n'%(str(key),str(value))
            #####
        else:
            keyWordString = 'None\n'
        #####
        self.optionsList[9]  = '\t-KEYWORDS             = %s'%keyWordString
        self.optionsList[10] = '\t-allowMultipleTopHits = %s\n'%self.allowMultipleTopHits

        self.optionsList[11] = '\t-DEBUG MODE           = %s\n'%self.debugMode
        self.optionsList[12] = '\t-TEMP DIR NUMBER      = %s\n'%str(tmpDirNum)
        self.optionsList[13] = '--------------------\n'
        return

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        equal_size_split( self.fastaFile, \
                          self.baseOutFileName, \
                          self.nFastaFiles, \
                          self.arachneReadFormat )
        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

########################################################################
class BLAT_best_hit_base( best_hit_base_class ):
    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       allowMultipleTopHits=False, \
                       debugMode=False, \
                       tmpDirNum=None, \
                       arachneReadFormat = False ):

        ## Initializing the best hit base class
        best_hit_base_class.__init__( self, \
                                      fastaFile, \
                                      assemblyFile, \
                                      nFastaFiles, \
                                      clusterNum, \
                                      cleanUp, \
                                      outputFileName, \
                                      keyWords, \
                                      allowMultipleTopHits, \
                                      debugMode, \
                                      tmpDirNum, \
                                      arachneReadFormat )

        # Generating the ooc files
        self.oocFile     = join( self.tmpPath, '11.ooc' )
        self.oocPerlFile = join( self.tmpPath, 'oocBlat.pl' )

        # User needs to define the following
        self.oocKeyWords         = None
        self.defaultBlatKeyWords = None
        self.querySizeMultiple   = None

    def best_hit(self):
        # Error Trapping
        if ( self.oocKeyWords == None ):
            assert ValueError('self.oocKeyWords not Initialized!\n')
        #####
        if ( self.defaultBlatKeyWords == None ):
            assert ValueError('self.defaultBlatKeyWords not Initialized!\n')
        #####
        if ( self.querySizeMultiple == None ):
            assert ValueError('self.querySizeMultiple not Initialized!\n')
        #####
        # Running the cases
        if ( not self.debugMode ):
            # Step 1:  Create temporary directory
            self.create_tmp_dir()
            # Step 2:  Break the fasta file into little pieces
            self.splitFASTA()
            # Step 3:  Create the ooc file
            self.create_ooc()
            # Step 4:  BLAT Fastas
            self.blatFastas()
        #####
        # Step 5: Parse Output
        self.parseOutput()
        # Step 6: Look for stragglers
        self.findStragglers()
        # Step 7: Cleanup
        if ( self.cleanUp ): self.clean_tmp()
        return

    def create_ooc(self):
        stderr.write( '\t-Creating occ file\n')
        deleteFile(self.oocFile)
        commandList = [ blatObject( self.assemblyFile, \
                         self.assemblyFile ).oocCommandString(self.oocKeyWords)]
        remoteServer4( commandList, 1, clusterNum=self.clusterNum, \
                                                     perlFile=self.oocPerlFile )
        deleteFile(self.oocPerlFile)
        return

    def blatFastas(self):
        stderr.write( '\t-BLATing FASTA files\n')
        # Loading default keywords
        finalKeyWords = {}
        for key,value in  self.defaultBlatKeyWords.items():
            finalKeyWords[key] = value
        #####
        # Loading user keywords
        if ( self.keyWords != None ):
            for key,value in self.keyWords.items():
                finalKeyWords[key] = value
            #####
        #####
        
        # Run the cases
        commandList = []
        for fastaFile in getFiles( 'fasta', self.tmpPath ):
            testFile(fastaFile)
            localOutputFile = '%s.blat'%fastaFile
            commandList.append( blatObject(fastaFile, self.assemblyFile, \
                                 localOutputFile).commandString(finalKeyWords) )
            print commandList[-1]
            print "---"
        #####
        remoteServer4( commandList, self.nFastaFiles, self.clusterNum, \
                                                                 self.perlFile )
        deleteFile(self.perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        deleteFile(self.mainOutFile)
        bestHitFileHandle = open( self.mainOutFile, 'w' )
        bestHitFileHandle.write( ''.join(self.optionsList) )
        bestHitFileHandle.write( bestHit_headerString() )
        for gzipFile in getFiles( 'gz', self.tmpPath ):
            # Extracting the gzip file
            parseBLATOutput( bestHitFileHandle, gzipFile, \
                             self.allowMultipleTopHits, self.querySizeMultiple )
        #####  End gzip file loop
        bestHitFileHandle.close()
        return

    def findStragglers(self):
        # Loading all scaffold names
        for record in iterBestHit(open(self.mainOutFile,'r')):
            try:
                self.scaffoldNameSet.remove(record.BAC_recordName)
            except KeyError:
                continue
            #####
        #####
        # Writing the footer to the file
        if ( len(self.scaffoldNameSet) > 0 ):
            tmpHandle = open(self.mainOutFile,'a')
            for scaffID in self.scaffoldNameSet:
                tmpHandle.write( '%s\tNo_match\n'%scaffID )
            #####
            tmpHandle.close()
        #####
        return

########################################################################
class blat_parseClass( object ):

    def __init__(self, matchSize, blatClass, nRetained):
        self.nRetained    = nRetained
        self.numHits      = 1
        self.hits         = [ (matchSize, blatClass) ]
        self.minMatchSize = matchSize
        self.state        = 'low'
        self.funMap = {'low':self.addHit_low, \
                       'high':self.addHit_high }

    def addHit(self, matchSize, blatClass):
        self.funMap[self.state]( matchSize, blatClass )
        return

    def addHit_low(self, matchSize, blatClass):
        self.hits.append( (matchSize, blatClass) )
        self.numHits += 1
        if ( self.numHits >= self.nRetained ): self.state = 'high'
        return

    def addHit_high(self, matchSize, blatClass):
        # Is the current hit larger than the lowest hit?
        if ( matchSize >= self.hits[-1][0] ):
            self.hits.append( (matchSize, blatClass) )
            self.hits.sort(reverse=True)
            self.hits.pop()
        #####
        return


########################################################################
def parseBLATOutput( bestHitFileHandle, \
                     blatFileName, \
                     allowMultipleTopHits = False, \
                     querySizeMultiple    = 2, \
                     parseAllOutput       = False, \
                     excludeOverhanging   = False ):
                     
    # Reading the file contents
    maxFields = {}
    if ( isinstance( blatFileName, basestring ) ):
        # Checking the incoming file type
        if ( (splitext(blatFileName)[1] == '.gz') or \
             (splitext(blatFileName)[1] == '.gzip') ):
            outputHandle = gzip.open(blatFileName, 'r')
        else:
            outputHandle = open(blatFileName, 'r')
        #####
        stderr.write( '\t\t-%s\n'%blatFileName)
    else:
        # In case I pass in a file handle instead of just a name
        outputHandle = blatFileName
        stderr.write( '\t\t-%s\n'%blatFileName.name)
    #####
    if ( allowMultipleTopHits ):
        nRetained = 3  # Retain the top 3 hits
    else:
        nRetained = 1 # Retain only the top hit
    #####
    for blatClass in iterBLAT( outputHandle ):
        # Reading query and target names
        queryName  = blatClass.Q_name
        targetName = blatClass.T_name
        # Skip if query name == target name
        # Useful when finding paralogous placements
        if ( queryName == targetName ): continue
        # Skip if match plus repetitive match is less than
        # half length of query read.
        # Indicates that we have a very small alignment
        totalMatchSize = blatClass.match + blatClass.repMatch
        querySize      = blatClass.Q_end - blatClass.Q_start
        if ( (2*totalMatchSize) < querySize ): continue
        # Skip if query and target sizes are the same,
        # and match is the size of the read, indicating
        # different names, but same read
#         if ( (blatClass.Q_size == blatClass.T_size) and \
#              (totalMatchSize == blatClass.Q_size) ):
#             continue
#         else:
        try:
            maxFields[queryName].addHit(totalMatchSize,blatClass)
        except KeyError:
            maxFields[queryName] = blat_parseClass(totalMatchSize,blatClass,nRetained)
        #####
    #####
    
    # Close the file if I opened it, otherwise it's up to the user
    if ( not isinstance( blatFileName, basestring ) ): outputHandle.close()

    # Examining the findings
    for queryName, parseClass in maxFields.items():

        # Extracting the hit listing
        hitList = parseClass.hits

        # Sorting blat classes by total match size
        if ( parseAllOutput ):
            blatClasses = [tmpClass for tmpSize, tmpClass in hitList]
            matchSizes  = [tmpSize for tmpSize, tmpClass in hitList]
        else:
            if ( len(hitList) > 1 ):
                # Multiples of same size?
                maxSize = max( [item[0] for item in hitList] )
                blatClasses = []
                for tmpSize, tmpClass in hitList:
                    if ( tmpSize == maxSize ):
                        # Grabbing the last one
                        if ( allowMultipleTopHits ):
                            blatClasses.append( tmpClass )
                        else:
                            blatClasses = [tmpClass]
                        #####
                    #####
                #####
                # Picking the last one in order to agree
                # with the current best_hit.pl
                totalMatchSize = maxSize
            else:
                totalMatchSize = hitList[0][0]
                blatClasses    = [hitList[0][1]]
            #####
        #####

        # If there are multiples, then loop over all of the
        # identical top hits for evaluation
        for blatClass in blatClasses:

            if (parseAllOutput):
                totalMatchSize = matchSizes[0]
                matchSizes.pop(0)
            #####

            # Initializing the output string
            outputString    = 12 * ['']
            outputString[0] = blatClass.Q_name
            outputString[1] = '%d'%blatClass.Q_size
            outputString[2] = '%d'%blatClass.Q_start
            outputString[3] = '%d'%blatClass.Q_end

            # Size of match divided by shorter of read sizes
            Q_length = float( blatClass.Q_end - blatClass.Q_start )
            T_length = float( blatClass.T_end - blatClass.T_start )
            Q_size   = float( blatClass.Q_size )
            T_size   = float( blatClass.T_size )

            T_start  = float( blatClass.T_start )
            T_end    = float( blatClass.T_end )
            T_size   = float( blatClass.T_size )
            Q_start  = float( blatClass.Q_start )
            Q_end    = float( blatClass.Q_end )
            Q_size   = float( blatClass.Q_size )
            strand   = blatClass.strand
            if ( excludeOverhanging ):
                if ( strand == '+' ):
                    if ( (Q_size-Q_end) > (T_size-T_end) ):
                        Q_size = Q_end + (T_size-T_end)
                    #####
                    if ( Q_start > T_start ):
                        Q_size = (Q_size - Q_start) + T_start
                    #####
                elif ( strand == '-' ):
                    if ( Q_start > (T_size-T_end) ):
                        Q_size = (Q_size - Q_start) + ( T_size - T_end )
                    #####
                    if ( (Q_size-Q_end) > T_start ):
                        Q_size = Q_end + T_start
                    #####
                #####
            #####

            # ID = max % of identical bases in the placement length
            # of the query/target
            id = min( 100.0, 100.0 * float(totalMatchSize) / min( Q_length, T_length ) )

            # Coverage = max % of the total query/target length that places
            cov = 100.0 * max( (Q_length/Q_size), (T_length/T_size) )
            start, stop = best_extent( Q_length, \
                                  blatClass.blockSizes, \
                                  blatClass.T_starts, \
                                  querySizeMultiple )
            outputString[4]  = '%s'%blatClass.strand
            outputString[5]  = '%s'%blatClass.T_name
            outputString[6]  = '%d'%blatClass.T_size
            outputString[7]  = '%d'%start
            outputString[8]  = '%d'%stop
            outputString[9]  = '%d'%totalMatchSize
            outputString[10] = '%5.2f'%id
            outputString[11] = '%5.2f\n'%cov

            bestHitFileHandle.write( '\t'.join(outputString) )

        ##### End loop over ties for top hit

    ##### End loop over query findings
    return

########################################################################
def best_extent( qsize, blksizes, blkstarts, querySizeMultiple ):
    # Starting information
    startpt = blkstarts[0]
    endpt   = blkstarts[-1] + blksizes[-1]  # blksizes is already sorted!

    # Use entire match if not more than twice query size
    if ( (endpt - startpt) <= (2 * qsize)  ):
        return startpt, endpt
    #####

    # Otherwise, pick "best" blocks totaling less than a "querySizeMultiple"
    # of query size.  Start with the biggest block!
    nBlks     = len(blksizes)
    DSU       = [ (blksizes[n],n) for n in xrange( nBlks ) ]
    DSU.sort( reverse=True )
    max_b     = DSU[0][1]
    startpt   = blkstarts[max_b]
    endpt     = startpt + blksizes[max_b]

    # Extend to smaller block of left or right;
    # repeat until 2X of query size would be exceeded
    leftb  = max_b - 1
    rightb = max_b + 1
    while ( (leftb>=0) or (rightb<nBlks) ):
        # Left extension
        newstart = ( (leftb<0) and [startpt] or [blkstarts[leftb]] )[0]
        leftAddLength = endpt - newstart
        # Right extension
        newend = ( (rightb==nBlks) and \
                   [endpt] or \
                   [blkstarts[rightb] + blksizes[rightb]] )[0]
        rightAddLength = newend - startpt
        # Decision time
        if ( (rightb<nBlks) and \
             ( (leftb<0) or (rightAddLength < leftAddLength) ) ):
            if ( rightAddLength <= (querySizeMultiple*qsize) ):
                endpt = newend
                rightb += 1
            else:
                rightb = nBlks # Terminal condition
            #####
        elif ( leftAddLength <= (querySizeMultiple*qsize) ):
            startpt = newstart
            leftb -= 1
        else:
            leftb = -1  # Terminal condition
        #####
    ##### end while loop
    return startpt, endpt

########################################################################
class DNA_best_hit( BLAT_best_hit_base ):

    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=16, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       allowMultipleTopHits=False, \
                       debugMode=False, \
                       tmpDirNum=None, \
                       arachneReadFormat=False ):
        
        ## Initializing the BLAT-based best hit base class
        BLAT_best_hit_base.__init__(self, \
                                    fastaFile, \
                                    assemblyFile, \
                                    nFastaFiles, \
                                    clusterNum, \
                                    cleanUp, \
                                    outputFileName, \
                                    keyWords, \
                                    allowMultipleTopHits, \
                                    debugMode, \
                                    tmpDirNum, \
                                    arachneReadFormat )

        # Setting the controlling parameters
        self.oocKeyWords  = { 'makeOveruseTileFile':self.oocFile }
        self.defaultBlatKeyWords = { 'overuseTileFile' : self.oocFile, \
                                     'extendThrough_N' : None, \
                                     'minScore'        : 200 }
        self.querySizeMultiple = 2

        # Running the cases
        self.best_hit()

#####

########################################################################
class CDNA_best_hit(BLAT_best_hit_base):

    def __init__(self, fastaFile, \
                       assemblyFile, \
                       crossSpecies=False, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       allowMultipleTopHits=False, \
                       debugMode=False, \
                       tmpDirNum=None ):

        ## Initializing the BLAT-based best hit base class
        BLAT_best_hit_base.__init__(self, \
                                    fastaFile, \
                                    assemblyFile, \
                                    nFastaFiles, \
                                    clusterNum, \
                                    cleanUp, \
                                    outputFileName, \
                                    keyWords, \
                                    allowMultipleTopHits, \
                                    debugMode, \
                                    tmpDirNum )
        # Adding specific parametesr
        self.crossSpecies = crossSpecies

        # Setting the controlling parameters
        self.oocKeyWords = { 'makeOveruseTileFile' : self.oocFile, \
                             'database_type'       : 'dna', \
                             'query_type'          : 'dna' }
        if ( self.crossSpecies ):
            self.oocKeyWords['database_type'] = 'dnax'
            self.oocKeyWords['query_type']    = 'dnax'
        #####

        self.defaultBlatKeyWords = { 'overuseTileFile' : self.oocFile, \
                                     'extendThrough_N' : None, \
                                     'minScore'        : 200, \
                                     'database_type'   : 'dna', \
                                     'query_type'      : 'rna' }
        if ( self.crossSpecies ):
            self.defaultBlatKeyWords['minIdentity']   = 85
            self.defaultBlatKeyWords['minScore']      = 100
            self.defaultBlatKeyWords['database_type'] = 'dnax'
            self.defaultBlatKeyWords['query_type']    = 'rnax'
        #####

        self.querySizeMultiple = 5

        # Running the cases
        self.best_hit()

#####

########################################################################
class blastn_best_hit_base( best_hit_base_class ):
    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       allowMultipleTopHits=False, \
                       debugMode=False, \
                       tmpDirNum=None ):

        ## Initializing the best hit base class
        best_hit_base_class.__init__( self, \
                                      fastaFile, \
                                      assemblyFile, \
                                      nFastaFiles, \
                                      clusterNum, \
                                      cleanUp, \
                                      outputFileName, \
                                      keyWords, \
                                      allowMultipleTopHits, \
                                      debugMode, \
                                      tmpDirNum )

        # User needs to define the following
        self.defaultBLASTKeyWords = None
        self.ID_cutoff            = None
        self.cov_cutoff           = None
        self.cutoffLength         = None

    def augmentHeader(self):
        optEnd = self.optionsList[13]
        self.optionsList[13] = '\t-COVERAGE CUTOFF      = %.2f\n'%self.cov_cutoff
        self.optionsList.append( '\t-IDENTITY CUTOFF      = %.2f\n'%self.ID_cutoff )
        self.optionsList.append( '\t-CUTOFF LENGTH        = %.2f\n'%self.cutoffLength )
        self.optionsList.append( '\t-Keywords:\n' )
        for key,value in self.defaultBLASTKeyWords.iteritems():
            if ( type(value) == type('') ):
                self.optionsList.append( '\t\t-%s:\t%s\n'%(key,value) )
            elif ( type(value) == type(0) ):
                self.optionsList.append( '\t\t-%s:\t%d\n'%(key,value) )
            elif ( type(value) == type(0.0) ):
                self.optionsList.append( '\t\t-%s:\t%.3e\n'%(key,value) )
            #####
        #####
        self.optionsList.append( optEnd )
        return

    def best_hit(self):
        # Error Trapping
        if ( self.defaultBLASTKeyWords == None ):
            assert ValueError('self.defaultBLASTKeyWords not Initialized!\n')
        if ( self.ID_cutoff == None ):
            assert ValueError('self.ID_cutoff not Initialized!\n')
        if ( self.cov_cutoff == None ):
            assert ValueError('self.cov_cutoff not Initialized!\n')
        if ( self.cutoffLength == None ):
            assert ValueError('self.cutoffLength not Initialized!\n')
        #####
        # Add information to the output file header
        self.augmentHeader()
        # Running the cases
        if ( not self.debugMode ):
            # Step 1:  Create temporary directory
            self.create_tmp_dir()
            # Step 2:  Break the fasta file into little pieces
            self.splitFASTA()
            # Step 3:  BLAST Fastas
            self.blastFastas()
        #####
        # Step 4: Create Output Files
        self.parseOutput()
        # Step 5: Cleanup
        if ( self.cleanUp ): self.clean_tmp()
        return

    def blastFastas(self):
        stderr.write( '\t-BLASTing FASTA files\n')
        # Loading default keywords
        finalKeyWords = {}
        for key,value in self.defaultBLASTKeyWords.items():
            finalKeyWords[key] = value
        #####
        # Loading user keywords
        if ( self.keyWords != None ):
            for key,value in self.keyWords.items():
                finalKeyWords[key] = value
            #####
        #####
        # Run the cases
        commandList = []
        for fastaFile in getFiles( 'fasta', self.tmpPath ):
            testFile(fastaFile)
            localOutputFile = '%s.blastn'%fastaFile
            commandList.append( blastallObject(fastaFile, self.assemblyFile, \
                                 localOutputFile).commandString(finalKeyWords, \
                                                             gzipOutput=True ) )
        #####
        remoteServer4( commandList, self.nFastaFiles, self.clusterNum, \
                                                                 self.perlFile )
        deleteFile(self.perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        deleteFile(self.mainOutFile)
        bestHitFileHandle = open( self.mainOutFile, 'w' )
        bestHitFileHandle.write( ''.join(self.optionsList) )
        bestHitFileHandle.write( bestHit_headerString() )
        for gzipFile in getFiles( 'gz', self.tmpPath ):
            # Extracting the gzip file
            parseBLASTOutput( bestHitFileHandle, gzipFile, \
                            self.cov_cutoff, self.ID_cutoff, self.cutoffLength )
        #####  End gzip file loop
        bestHitFileHandle.close()
        return

########################################################################
def parseBLASTOutput( bestHitFileHandle, blastFileName, cov_cutoff, \
                                                  ID_cutoff, cutoffLength = 0 ):
    """
    parseBLASTOutput( blastallFileName, cov_cutoff, ID_cutoff, \
                                                             cutoffLength = 0 ):

    The goal of this code is to condense down the BLAST output into
    a compact format that I can use for further analysis without
    having to reparse the raw BLAST output file.  The *.out file
    is what is generated with this routine.
    """

    # Defining the screening functions
    lengthScreen = lambda x: ( x[1].align_length >= cutoffLength )
    cov_screen   = lambda cov: ( cov >= cov_cutoff )
    ID_screen    = lambda ID: ( ID >= ID_cutoff )

    # Counters
    nSequencesConsidered = 0
    nScreened_per        = 0

    # Main entry class
    tmpClass = best_Hit_Entry_class('',0,0,0,'','',0,0,0,0,0.0,0.0)

    rawDataString     = 13 * ['']
    rawDataString[12] = '\n'

    # Looping over the query strands and remapping

    if ( (splitext(blastFileName)[1] == '.gz') or \
         (splitext(blastFileName)[1] == '.gzip') ):
        import gzip
        outputHandle = gzip.open(blastFileName, 'r')
    else:
        outputHandle = open(blastFileName, 'r')
    #####


    stderr.write( '\t\tReading %s.....\n'%blastFileName )
    blast_iterator = HAGSC_XML.parse_XML().parse( outputHandle )
    for blast_record in blast_iterator:
        # Checking for alignments
        if ( blast_record.alignments == [] ): continue
        if ( blast_record.query_length == None ): continue
        # Computing the percentage constant
        Q_size = float( blast_record.query_length )
        # Extracting the read identifier
        tmpClass = best_Hit_Entry_class('',0,0,0,'','',0,0,0,0,0.0,0.0)
        tmpClass.BAC_recordName = blast_record.query.split(' ')[0]
        tmpClass.BAC_size       = blast_record.query_length
        # Looping over the subject strands
        didNotPlace = True
        for alignment in blast_record.alignments:
            # If it has the same name, then ignore
            # handy for hunting paralogs
            if ( alignment.hit_def == blast_record.query ): continue
            # Are there any hsp's?
            if ( alignment.hsps == [] ): continue
            S_size = float(alignment.length)
            # Screening the placements
            placs = []
            for hsp in alignment.hsps:
                S_length       = float(abs(hsp.sbjct_end - \
                                                           hsp.sbjct_start) + 1)
                Q_length       = float(abs(hsp.query_end - \
                                                           hsp.query_start) + 1)
                totalMatchSize = float( hsp.identities )
                # Below is the max % of identical bases the ratio of the
                # number of matching bases to the smallest total length
                # of the query/target
                id = 100.0 * totalMatchSize / min( Q_length, S_length )
                # Below is the max % coverage since it is the length of the
                # match divided by the length of the query
                cov = 100.0 * max( (S_length/S_size), \
                                                     (Q_length/Q_size) )
                # Screening placements for identity and coverage
                if ( cov_screen(cov) and ID_screen(id) ):
                    placs.append( (cov, hsp, id) )
                #####
            #####
            # Filtering length
            placs = filter( lengthScreen, placs )
            # Anything to write?
            if ( placs != [] ):
                # Writing the screened raw data
                didNotPlace = False
                tmpClass.scaffold = alignment.hit_def.split(' ')[0]
                tmpClass.scaffSize = alignment.length
                placs.sort(reverse=True)
                for placTemp in placs:
                    # Switching the start and end around if necessary
                    if ( placTemp[1].sbjct_start < placTemp[1].sbjct_end ):
                        tmpClass.scaffStart   = placTemp[1].sbjct_start
                        tmpClass.scaffEnd     = placTemp[1].sbjct_end
                    else:
                        tmpClass.scaffStart   = placTemp[1].sbjct_end
                        tmpClass.scaffEnd     = placTemp[1].sbjct_start
                    #####
                    # Switching the start and end around if necessary
                    if ( placTemp[1].query_start < placTemp[1].query_end ):
                        tmpClass.BAC_start    = placTemp[1].query_start
                        tmpClass.BAC_end      = placTemp[1].query_end
                    else:
                        tmpClass.BAC_start    = placTemp[1].query_end
                        tmpClass.BAC_end      = placTemp[1].query_start
                    #####
                    tmpClass.numBases     = placTemp[1].identities
                    tmpClass.per_ID       = placTemp[2]
                    tmpClass.per_coverage = placTemp[0]
                    if ( placTemp[1].sbjct_frame == -1 ):
                        tmpClass.placDir = '-'
                    else:
                        tmpClass.placDir = '+'
                    #####
                    bestHitFileHandle.write(tmpClass.generateString())
                #####
            #####
        #####
        # Screened read counter
        if ( didNotPlace ):
            nScreened_per += 1
        #####
        # Sequence counter
        nSequencesConsidered += 1
    #####

    # Closing the file
    outputHandle.close()

    # Clearing memory
    blast_iterator = None

    # Generating the summary string
    summaryString = ['FileName:  %s\n'%blastFileName]
    summaryString.append( '%d sequences did not place '%nScreened_per )
    summaryString.append( 'at greater than %f (cov) and %f (ID)'%(cov_cutoff,\
                                                                    ID_cutoff) )
    summaryString.append(' identity out of a possible %d\n'%nSequencesConsidered)

    stderr.write( ''.join(summaryString) )

########################################################################
class blastn_bestHit( blastn_best_hit_base ):
    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       debugMode=False, \
                       tmpDirNum=None, \
                       cov_cutoff=75.0, \
                       ID_cutoff=75.0, \
                       cutoffLength=0, \
                       paramSet='Exploration' ):

        ## Initializing the best hit base class
        allowMultipleTopHits=False # Not used in blastn_bestHit
        blastn_best_hit_base.__init__( self, \
                                       fastaFile, \
                                       assemblyFile, \
                                       nFastaFiles, \
                                       clusterNum, \
                                       cleanUp, \
                                       outputFileName, \
                                       keyWords, \
                                       allowMultipleTopHits, \
                                       debugMode, \
                                       tmpDirNum )

        self.defaultBLASTKeyWords = {}
        if ( paramSet == 'Exploration' ):
            # Exploration keyword set
            self.defaultBLASTKeyWords['nuc_match']    =  1
            self.defaultBLASTKeyWords['nuc_mismatch'] = -1  # 1/-1 assumes target frequency of 75%
            self.defaultBLASTKeyWords['gap_extend']   =  1
            self.defaultBLASTKeyWords['gap_open']     =  2
            self.defaultBLASTKeyWords['wordsize']     =  9
            self.defaultBLASTKeyWords['expectation']  =  1.0
            self.defaultBLASTKeyWords['alignments']   =  20000
            self.defaultBLASTKeyWords['filter']       =  r'"m D" -U'  # Soft masking of DUST and lowercase letters
        elif ( paramSet == 'Mapping' ):
            # Default keyword set is for mapping
            self.defaultBLASTKeyWords = {}
            self.defaultBLASTKeyWords['nuc_match']    =  1
            self.defaultBLASTKeyWords['nuc_mismatch'] = -3  # 1/-1 assumes target frequency of 99%
            self.defaultBLASTKeyWords['gap_extend']   =  1
            self.defaultBLASTKeyWords['gap_open']     =  2
            self.defaultBLASTKeyWords['wordsize']     =  9
            self.defaultBLASTKeyWords['expectation']  =  1.0e-20
            self.defaultBLASTKeyWords['alignments']   =  20000
            self.defaultBLASTKeyWords['filter']       =  'F'  # No complexity filtering
        elif ( paramSet == 'Intermediate' ):
            # Default keyword set is for mapping
            self.defaultBLASTKeyWords = {}
            self.defaultBLASTKeyWords['nuc_match']    =  1
            self.defaultBLASTKeyWords['nuc_mismatch'] = -2  # 1/-1 assumes target frequency of 99%
            self.defaultBLASTKeyWords['gap_extend']   =  1
            self.defaultBLASTKeyWords['gap_open']     =  2
            self.defaultBLASTKeyWords['wordsize']     =  9
            self.defaultBLASTKeyWords['expectation']  =  1.0e-20
            self.defaultBLASTKeyWords['alignments']   =  20000
            self.defaultBLASTKeyWords['filter']       =  'F'  # No complexity filtering
        elif ( paramSet == 'tblastn'):
            self.defaultBLASTKeyWords = {}
        #####

        # Settings for blastn bestHit
        self.cov_cutoff   = cov_cutoff
        self.ID_cutoff    = ID_cutoff
        self.cutoffLength = cutoffLength

        # Running the case
        self.best_hit()

########################################################################
class BLAT_hit_base_class(object):
    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       tmpDirNum=None, \
                       arachneReadNames=False ):

        # Make sure the files exist
        testFile(fastaFile)
        testFile(assemblyFile)

        # Reading in the control variables
        self.fastaFile        = fastaFile
        self.assemblyFile     = assemblyFile
        self.nFastaFiles      = nFastaFiles
        self.clusterNum       = clusterNum
        self.cleanUp          = cleanUp
        self.keyWords         = keyWords
        self.mainOutFile      = outputFileName
        self.arachneReadNames = arachneReadNames

        # Generating the directory information
        self.basePath = abspath(curdir)
        # If we are running a bunch of cases, it is a good idea to renumber
        # the temporary directories instead of reusing the process id
        # Otherwise, you will get a "Stale NFS path" error
        if ( tmpDirNum == None ):
            self.tmpDir = generateTmpDirName()
        else:
            self.tmpDir = generateTmpDirName( tmpDirNum )
        #####
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                                  baseFileName(self.fastaFile) )

        # Generating the output file name
        if ( self.mainOutFile == None ):
            self.mainOutHandle = stderr
        else:
            deleteFile(self.mainOutFile)
            self.mainOutHandle = open( self.mainOutFile, 'w' )
        #####

        # Generating the main bestHit perl file
        self.perlFile = join( self.tmpPath, 'BLAThitJob.pl' )

        # Echo the options to the user
        self.makeOptionsString(tmpDirNum)
        stderr.write( ''.join(self.optionsList) )

    def makeOptionsString(self, tmpDirNum):
        self.optionsList     = 14 * ['']
        self.optionsList[0]  = '------------------\n'
        self.optionsList[1]  = 'BLAT_Hit Settings:\n'
        self.optionsList[2]  = '------------------\n'
        self.optionsList[3]  = '\t-FASTA                = %s\n'%self.fastaFile
        self.optionsList[4]  = '\t-ASSEMBLY             = %s\n'%self.assemblyFile
        self.optionsList[5]  = '\t-QUEUE SIZE           = %d\n'%self.nFastaFiles
        if ( type(self.clusterNum) == type('') ):
            self.optionsList[6]  = '\t-CLUSTER NUMBER       = %s\n'%self.clusterNum
        elif ( type(self.clusterNum) == type(0) ):
            self.optionsList[6]  = '\t-CLUSTER NUMBER       = %d\n'%self.clusterNum
        #####
            
        self.optionsList[7]  = '\t-CLEANUP?             = %s\n'%self.cleanUp
        if ( self.mainOutFile == None ):
            self.optionsList[8]  = '\t-OUTPUT FILE NAME     = stderr\n'
        else:
            self.optionsList[8]  = '\t-OUTPUT FILE NAME     = %s\n'%self.mainOutFile
        #####
        # Generate keywords string:
        if ( self.keyWords != None ):
            keyWordString = '\n'
            for key, value in self.keyWords.items():
                keyWordString += '\t\t%s:%s\n'%(str(key),str(value))
            #####
        else:
            keyWordString = 'None\n'
        #####
        self.optionsList[9]  = '\t-KEYWORDS             = %s'%keyWordString
        self.optionsList[12] = '\t-TEMP DIR NUMBER      = %s\n'%str(tmpDirNum)
        self.optionsList[13] = '--------------------\n'
        return

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
#        deleteDir(self.tmpPath)
        if ( not isdir(self.tmpDir) ): mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        equal_size_split( self.fastaFile, \
                          self.baseOutFileName, \
                          self.nFastaFiles, \
                          self.arachneReadNames )
        return

    def clean_tmp(self):
        deleteDir(self.tmpPath)
        return

########################################################################
class BLAT_hit_base( BLAT_hit_base_class ):
    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=8, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       tmpDirNum=None, \
                       nTopHits=3, \
                       noOOC=False, \
                       arachneReadNames=False ):

        ## Initializing the best hit base class
        BLAT_hit_base_class.__init__( self, \
                                      fastaFile, \
                                      assemblyFile, \
                                      nFastaFiles, \
                                      clusterNum, \
                                      cleanUp, \
                                      outputFileName, \
                                      keyWords, \
                                      tmpDirNum, \
                                      arachneReadNames )

        # Generating the ooc files
        self.oocFile     = join( self.tmpPath, '11.ooc' )
        self.oocPerlFile = join( self.tmpPath, 'oocBlat.pl' )

        # User needs to define the following
        self.oocKeyWords         = None
        self.defaultBlatKeyWords = None
        self.querySizeMultiple   = None
        
        # Setting the screening criteria
        self.nTopHits = nTopHits
        self.noOOC    = noOOC

    def BLAT_hit(self):
        # Error Trapping
        if ( self.oocKeyWords == None ):
            assert ValueError('self.oocKeyWords not Initialized!\n')
        #####
        if ( self.defaultBlatKeyWords == None ):
            assert ValueError('self.defaultBlatKeyWords not Initialized!\n')
        #####
        if ( self.querySizeMultiple == None ):
            assert ValueError('self.querySizeMultiple not Initialized!\n')
        #####
        # Running the cases
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Break the fasta file into little pieces
        self.splitFASTA()
        # Step 3:  Create the ooc file
        if ( self.noOOC ): 
            stderr.write( '\t-Skipping the occ.11 file creation\n' )
        else:
            self.create_ooc()
        #####
        # Step 4:  BLAT Fastas
        self.blatFASTAs()
        # Step 5: Parse Output
        self.parseOutput()
        # Step 7: Cleanup
        if ( self.cleanUp ): self.clean_tmp()
        return

    def create_ooc(self):
        stderr.write( '\t-Creating occ file\n')
#        if ( isfile(self.oocFile) ): deleteFile(self.oocFile)
        commandList = [ blatObject( self.assemblyFile, \
                         self.assemblyFile ).oocCommandString(self.oocKeyWords)]
        remoteServer4( commandList, 1, clusterNum=self.clusterNum, \
                                                     perlFile=self.oocPerlFile )
        deleteFile(self.oocPerlFile)
        return

    def blatFASTAs(self):
        stderr.write( '\t-BLATing FASTA files\n')
        # Loading default keywords
        finalKeyWords = {}
        for key,value in  self.defaultBlatKeyWords.items():
            finalKeyWords[key] = value
        #####
        # Loading user keywords
        if ( self.keyWords != None ):
            for key,value in self.keyWords.items():
                finalKeyWords[key] = value
            #####
        #####
        # Run the cases
        commandList = []
        for fastaFile in getFiles( 'fasta', self.tmpPath ):
            testFile(fastaFile)
            localOutputFile = '%s.blat'%fastaFile
            commandList.append( blatObject(fastaFile, self.assemblyFile, \
                                 localOutputFile).commandString(finalKeyWords) )
        #####
        remoteServer4( commandList, self.nFastaFiles, self.clusterNum, \
                                                                 self.perlFile )
        deleteFile(self.perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        topHit_dict = {}
        if self.nTopHits > 0:
            for gzipFile in getFiles( 'gz', self.tmpPath ):
                for blatClass in iterBLAT( gzip.open(gzipFile,'r') ):
                    try:
                        topHit_dict[blatClass.Q_name].append( (blatClass.match,blatClass) )
                    except KeyError:
                        topHit_dict[blatClass.Q_name] = [ (blatClass.match,blatClass) ]
                    #####
                #####
            #####  End gzip file loop
            for scaffID, blatList in topHit_dict.iteritems():
                blatList.sort(reverse=True)
                nCounter = 0
                for blatScore, blatClass in blatList:
                    self.mainOutHandle.write( str(blatClass) )
                    nCounter += 1
                    if ( nCounter == self.nTopHits ): break
                #####
            #####
        else:
            for gzipFile in getFiles( 'gz', self.tmpPath ):
                tmpHandle = gzip.open(gzipFile,'r')
                try:
                    for blatClass in iterBLAT( tmpHandle ):
                        self.mainOutHandle.write( str(blatClass) )
                    #####
                except:
                    stderr.write( '**********************************************************\n' )
                    stderr.write( 'Unexpected error %s occurred while parsing %s\n'%( exc_info()[0], \
                                                                                      tmpHandle.name ) )
                    stderr.write( '**********************************************************\n\n' )
                #####
                tmpHandle.close()
                del tmpHandle
            #####
        #####
        if ( self.mainOutFile != None ): self.mainOutHandle.close()
        return

########################################################################
class BLAT_hit( BLAT_hit_base ):

    def __init__(self, fastaFile, \
                       assemblyFile, \
                       nFastaFiles=16, \
                       clusterNum='101', \
                       cleanUp = True, \
                       outputFileName=None, \
                       keyWords=None, \
                       tmpDirNum=None, \
                       nTopHits=0, \
                       noOOC=False, \
                       arachneReadNames=False):

        ## Initializing the BLAT-based best hit base class
        BLAT_hit_base.__init__(self, \
                                    fastaFile, \
                                    assemblyFile, \
                                    nFastaFiles, \
                                    clusterNum, \
                                    cleanUp, \
                                    outputFileName, \
                                    keyWords, \
                                    tmpDirNum, \
                                    nTopHits, \
                                    noOOC, \
                                    arachneReadNames )

        # Setting the controlling parameters
        self.oocKeyWords  = { 'makeOveruseTileFile':self.oocFile }

        # Making the decision of whether to use the ooc.11 file        
        if ( noOOC ): 
            stderr.write( '\t-NOT USING THE ooc.11 FILE\n')
            self.defaultBlatKeyWords = { 'extendThrough_N' : None }
        else:
            self.defaultBlatKeyWords = { 'overuseTileFile' : self.oocFile, \
                                         'extendThrough_N' : None }
        #####

        # Running the cases
        self.BLAT_hit()

#####

# Unit Testing
if ( __name__ == '__main__' ):
    pass
#####
