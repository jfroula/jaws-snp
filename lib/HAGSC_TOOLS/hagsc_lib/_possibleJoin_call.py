
__author__ = "jjenkins"
__date__   = "$Feb 24, 2010 5:08:30 PM$"

# Local library imports
from _FASTA_IO import generateTrimmedFileName
from _FASTA_IO import trimAssemblyFile
from _FASTA_IO import iterFASTA

from _helperFunctions import createBestHitFileName
from _helperFunctions import generateTmpDirName
from _helperFunctions import testDirectory
from _helperFunctions import parseString
from _helperFunctions import deleteFile
from _helperFunctions import deleteDir
from _helperFunctions import testFile

from _bestHit import DNA_best_hit
from _bestHit import iterBestHit

from _globalSettings import defaultCluster

from _helperClasses import fileHeader_Class

from os.path import abspath
from os.path import join
from os.path import split

from os import system
from os import curdir
from os import mkdir
from os import chdir

from sys import stdout

########################################################################
class possibleJoin_placement_class(object):

    def __init__(self, scaff, scaffSize, scaffDir, scaffStart, scaffEnd, ID, COV ):
        self.scaff        = scaff
        self.scaffSize    = scaffSize
        self.scaffDir     = scaffDir
        self.scaffStart   = scaffStart
        self.scaffEnd     = scaffEnd
        self.per_ID       = ID
        self.per_coverage = COV
#####

########################################################################
class possibleJoinEntry(object):

    def __init__(self, BAC_id, scaff_1, size_1, dir_1, start_1, end_1, ID_1, COV_1,\
                               scaff_2, size_2, dir_2, start_2, end_2, ID_2, COV_2 ):
        self.BAC_id = BAC_id
        self.placs = 2 * [None]
        self.placs[0] = possibleJoin_placement_class( scaff_1, size_1, \
                                                      dir_1, start_1, end_1, ID_1, COV_1 )
        self.placs[1] = possibleJoin_placement_class( scaff_2, size_2, \
                                                      dir_2, start_2, end_2, ID_2, COV_2 )

    def generateString( self ):
        columnLabel, offset, formatting = getPossibleJoinFormat()

        strLen      = len(offset)
        tmpData     = strLen * ['']
        tmpData[0]  = self.BAC_id
        tmpData[1]  = self.placs[0].scaff
        tmpData[2]  = self.placs[0].scaffSize
        tmpData[3]  = self.placs[0].scaffDir
        tmpData[4]  = self.placs[0].scaffStart
        tmpData[5]  = self.placs[0].scaffEnd

        tmpData[6]  = self.placs[1].scaff
        tmpData[7]  = self.placs[1].scaffSize
        tmpData[8]  = self.placs[1].scaffDir
        tmpData[9]  = self.placs[1].scaffStart
        tmpData[10] = self.placs[1].scaffEnd
        tmpData[11] = self.placs[1].per_ID
        tmpData[12] = self.placs[1].per_coverage

        # Building the string
        outputString = (strLen + 1) * ['']
        for n in xrange( strLen ):
            outputString[n] = (formatting[n]%tmpData[n]).ljust(offset[n])
        #####
        outputString[strLen] = '\n'
        return ''.join( outputString )
    
    def __repr__(self):
        return self.generateString()

#####

########################################################################
def getPossibleJoinFormat():
    strLen          = 13
    columnLabel     = strLen * ['']
    columnLabel[0]  = 'BAC_id'
    columnLabel[1]  = 'scaffold'
    columnLabel[2]  = 'scaffSize'
    columnLabel[3]  = 'dir'
    columnLabel[4]  = 'Start'
    columnLabel[5]  = 'End'
    columnLabel[6]  = 'scaffold'
    columnLabel[7]  = 'scaffSize'
    columnLabel[8]  = 'dir'
    columnLabel[9]  = 'Start'
    columnLabel[10] = 'End'
    columnLabel[11] = 'ID'
    columnLabel[12] = 'COV'

    offset          = [10 for n in xrange(strLen)]
    offset[0]       = 45  # BAC_ID
    offset[1]       = 25  # scaffoldName
    offset[2]       = 15  # scaffSize
    offset[3]       = 7   # direction
    offset[6]       = 25  # scaffoldName
    offset[7]       = 15  # scaffSize
    offset[8]       = 7   # direction

    formatting      = ['%s' for n in xrange(strLen)]
    formatting[2]   = '%d'   # size
    formatting[4]   = '%d'   # start
    formatting[5]   = '%d'   # end
    formatting[7]   = '%d'   # size
    formatting[9]   = '%d'   # start
    formatting[10]  = '%d'   # end
    formatting[11]  = '%.2f' # end
    formatting[12]  = '%.2f' # end

    return columnLabel, offset, formatting

########################################################################
def iterPossibleJoin( fileHandle ):
    """
    iterPossibleJoin( fileHandle )

    Generator function to iterate over entries in a possibleJoin file, returning
    possibleJoinEntry objects.
    """

    for line in fileHandle:

        parsedLine = line.split(None)

        # Skipping lines
        if ( len(parsedLine) > 8 ):
            if ( parsedLine[0] == 'BAC_id' ): continue
            if ( parsedLine[0][0] == '-' ): continue
        else:
            continue
        #####

        BAC_id  = parsedLine[0]
        scaff_1 = parsedLine[1]
        size_1  = int(parsedLine[2])
        dir_1   = parsedLine[3]
        start_1 = int( parsedLine[4] )
        end_1   = int( parsedLine[5] )

        scaff_2 = parsedLine[6]
        size_2  = int(parsedLine[7])
        dir_2   = parsedLine[8]
        start_2 = int( parsedLine[9] )
        end_2   = int( parsedLine[10] )

        # Yield up the record and wait for next iteration
        yield possibleJoinEntry( BAC_id, \
                                 scaff_1, size_1, dir_1, start_1, end_1,\
                                 scaff_2, size_2, dir_2, start_2, end_2 )
    #####

    # Completed reading the file
    return

#####

########################################################################
class possibleJoin_class(object):

    """
    possibleJoin_call( assemblyFile,
                       BES_FES_File,
                       outputFile,
                       trimLength = 140000,
                       coverage_Cutoff = 90.0,
                       identity_Cutoff = 0.0,
                       debugMode = False,
                       cluster_Num = defaultCluster,
                       queueSize = 16 ):

    This routine generates a trimmed assembly file and runs best_hit.pl on
    it to identify the best hits, then looks for possible joins in the
    assembly.  Any results are written to an output file.
    """

    def __init__(self, assemblyFile, \
                       BES_FES_File, \
                       outputFile, \
                       trimLength      = 140000, \
                       coverage_Cutoff = 90.0, \
                       identity_Cutoff = 0.0, \
                       debugMode       = False, \
                       cluster_Num     = defaultCluster, \
                       queueSize       = 16 ):

        # Do they exist?
        testFile(assemblyFile)
        testFile(BES_FES_File)

        # Incoming variables
        self.assemblyFile    = assemblyFile
        self.BES_FES_File    = BES_FES_File
        self.outputFile      = outputFile
        self.trimLength      = trimLength
        self.coverage_Cutoff = coverage_Cutoff
        self.identity_Cutoff = identity_Cutoff
        self.debugMode       = debugMode
        self.cluster_Num     = cluster_Num
        self.queueSize       = queueSize

        # Echoing the options to the user
        self.makeOptionsString()
        stdout.write( ''.join(self.optionsList) )

        # Generating the directory information
        self.basePath = abspath(curdir)
        self.tmpPath  = join( self.basePath, 'pj.%s'%generateTmpDirName() )

        # Adding the path to the
        if ( split(self.assemblyFile)[0] == '' ):
            self.assemblyFile = join( self.basePath, self.assemblyFile )
        #####
        if ( split(self.BES_FES_File)[0] == '' ):
            self.BES_FES_File = join( self.basePath, self.BES_FES_File )
        #####

        # Initializing file names
        self.trimmedAssemblyFile = join( self.tmpPath, \
                                  generateTrimmedFileName( self.assemblyFile ) )
        self.bestHit_outputFile  = join( self.tmpPath, \
                                        createBestHitFileName(self.outputFile) )

        # Initializing other variables
        self.localScaffoldSizes = {}
        self.screenedPlacements = []
        self.possibleJoins      = {}

        self.scaffold_placs = {}  # All placements in a scaffold
        self.BACid_placs    = {}  # Scaffolds where BAC_id places

        # performJoin parameters
        self.DNA_best_hit_keywords = None
        self.allowMultipleTopHits  = False

        # BAC name
        self.nameSeparators             = {}
        self.nameSeparators['illumina'] = '/'
        self.nameSeparators['sanger']   = '.'

        # Determining the separator string
        self.sepString = self.nameSeparators[ self.canonicalNameSeparator() ]
        
    #####

    def canonicalNameSeparator(self):
        # Obtaining one name
        BAC_name = None
        for record in iterFASTA(open(self.BES_FES_File,'r')):
            BAC_name = record.id
            print record.description
            break
        #####
        # Determining which separator is best
        for sepType, sepString in self.nameSeparators.items():
            splitName = parseString( BAC_name, sepString )
            if ( len(splitName) > 1 ):
                return sepType
            #####
        #####
        # If none is found, provide error message
        raise ValueError('Could not determine separator type for %s\n'%BAC_name)

    def performJoin(self, keyWords=None, aMTH=False):

        # Input Parameters
        self.DNA_best_hit_keywords = keyWords
        self.allowMultipleTopHits  = aMTH

        # Step 1:  Create temp directory
        self.create_tmp_dir()

        # Step 2:  Trim the assembly file
        self.trimAssemblyFile()

        # Step 3:  Run best_hit.pl
        self.runBestHit()

        # Step 4:  Screen all best_hit placements
        self.screenPlacements()

        # Step 5: Find joins
        self.findJoins()

        # Step 6:  Write output file
        self.writeOutputFile()

        # Step 7:  Clean up the temporary directory
        self.clean_tmp()
        return

    def create_tmp_dir(self):
        stdout.write( '-Creating tmp directory %s\n'%self.tmpPath)
        deleteDir(self.tmpPath)
        mkdir( self.tmpPath, 0777 )
        testDirectory( self.tmpPath )
        return

    def clean_tmp(self):
        stdout.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

    def writeOutputFile(self):

        stdout.write( '-Generating %s\n'%self.outputFile )

        # Sort scaffolds by size
        DSU = [(value,key) for key,value in self.localScaffoldSizes.items()]
        DSU.sort(reverse=True)
        sepString = 20*'-' + '\n'

        # Write results to a file
        tempContainer = {}
        for scaffSize, scaffoldID in DSU:
            # Sorting the joins by start
            if ( not self.possibleJoins.has_key(scaffoldID) ): continue
            nPlacs = len( self.possibleJoins[scaffoldID] )
            DSU_plac = nPlacs * [None]
            n = 0
            for item in self.possibleJoins[scaffoldID]:
                tmpStart  = item.placs[0].scaffStart
                tmpString = item.generateString()
                DSU_plac[n] = ( tmpStart, tmpString )
                n += 1

                # Storing the pairing frequency information
                splitLine = tmpString.split(None)
                idList    = [splitLine[1],splitLine[6]]
                uniqueID  = '%s_|_%s'%(idList[0],idList[1])
                try:
                    tempContainer[uniqueID].add( tmpString )
                except KeyError:
                    tempContainer[uniqueID] = set([tmpString])
                #####

            #####
            DSU_plac.sort()
        #####

        # Preprocessing frequency information
        DSU = [(len(tmpList),ID) for ID, tmpList in tempContainer.items()]
        DSU.sort(reverse=True)

        # Forming the header string
        columnLabel, offset, formatting = getPossibleJoinFormat()

        # File header
        fileHeader = fileHeader_Class( columnLabel, offset )
        fileHeader.addText( ''.join( self.optionsList ) )

        # Output the syntenic join frequency file
        deleteFile(self.outputFile)
        outputHandle = open( self.outputFile, 'w' )
        outputHandle.write( fileHeader.generateHeaderString() )

        # Outputting the most frequent first
        screenIDs = set()
        for n in xrange(0, len(DSU)):

            # Reading in the globbed ID
            tmpNum, tmpID = DSU[n]

            # Getting a unique ID
            split_id = tmpID.split('_|_')
            split_id.sort()
            uniqueID = '%s%s'%(split_id[0],split_id[1])

            if ( uniqueID not in screenIDs ):

                # Writing number of joins
                outputHandle.write( '%d Joins\n'%tmpNum)

                # Sorting the output
                DSU_2 = []
                for line in tempContainer[tmpID]:
                    parsedLine = line.split(None)
                    scaffStart = int(parsedLine[4])
                    DSU_2.append( (scaffStart,line) )
                #####
                DSU_2.sort()

                # Write lines
                for tmpSize, line in DSU_2: outputHandle.write( line )

                # Write separator string
                outputHandle.write( sepString )

                # Adding the ID to the screen IDs
                screenIDs.add( uniqueID )
            #####
        #####
        outputHandle.close()
        return

    def findJoins(self):
        # Sort scaffolds by size
        DSU = [(value,key) for key,value in self.localScaffoldSizes.items()]
        DSU.sort(reverse=True)
        # Loop over all sorted scaffold IDs
        for scaffoldSize, scaffoldID in DSU:
            if ( not self.scaffold_placs.has_key(scaffoldID) ): continue
            for bestHitClass_1 in self.scaffold_placs[scaffoldID]:
                BAC_id = self.generate_BAC_ID( bestHitClass_1 )
                # Multiple placements for BAC_id?
                nBAC_ids = len( self.BACid_placs[BAC_id] )
                if ( nBAC_ids >= 2 ):
                    # Looping over all of the BACid placements
                    recName_1 = bestHitClass_1.BAC_recordName
                    for bestHitClass_2 in self.BACid_placs[BAC_id]:
                        #----------------------------------------
                        # Keeping pairs of placements that:
                        # (1)  Do not have the same record name.
                        #      Ensures we are not pairing a BAC
                        #      with itself.
                        # (2)  Are on a different scaffold.
                        #----------------------------------------
                        scaffID_2 = bestHitClass_2.scaffold
                        recName_2 = bestHitClass_2.BAC_recordName
                        if ( (recName_2 != recName_1) and
                             (scaffID_2 != scaffoldID) ):
                                 self.addJoin( bestHitClass_1, bestHitClass_2 )
                            #####
                        #####
                    #####
                #####
            #####
        #####
        return

    def addJoin(self, bestHitClass_1, bestHitClass_2):

        scaffoldID = bestHitClass_1.scaffold
        BAC_id  = self.generate_BAC_ID( bestHitClass_1 )
        scaff_1 = bestHitClass_1.scaffold
        size_1  = bestHitClass_1.scaffSize
        dir_1   = bestHitClass_1.placDir
        start_1 = bestHitClass_1.scaffStart
        end_1   = bestHitClass_1.scaffEnd
        ID_1    = bestHitClass_1.per_ID
        COV_1   = bestHitClass_1.per_coverage

        scaff_2 = bestHitClass_2.scaffold
        size_2  = bestHitClass_2.scaffSize
        dir_2   = bestHitClass_2.placDir
        start_2 = bestHitClass_2.scaffStart
        end_2   = bestHitClass_2.scaffEnd
        ID_2    = bestHitClass_2.per_ID
        COV_2   = bestHitClass_2.per_coverage
        try:
            self.possibleJoins[scaffoldID].append( \
            possibleJoinEntry(BAC_id, scaff_1, size_1, dir_1, start_1, end_1, ID_1, COV_1,\
                                      scaff_2, size_2, dir_2, start_2, end_2, ID_2, COV_2 )\
                                                  )
        except KeyError:
            self.possibleJoins[scaffoldID] = [\
            possibleJoinEntry(BAC_id, scaff_1, size_1, dir_1, start_1, end_1, ID_1, COV_1,\
                                      scaff_2, size_2, dir_2, start_2, end_2, ID_2, COV_2 )\
                                              ]
        #####
        return

    def screenPlacements(self):
        stdout.write( '-Screening %s\n'%self.bestHit_outputFile )
        for bestHitClass in iterBestHit(open(self.bestHit_outputFile,'r')):
            # Coverage? Identity?
            if ( (bestHitClass.per_coverage < self.coverage_Cutoff) or \
                 (bestHitClass.per_ID       < self.identity_Cutoff) ): continue
            # Small Scaffold?
            scaffoldID = bestHitClass.scaffold
            if ( (scaffoldID.rfind('R__') == -1) and \
                 (scaffoldID.rfind('L__') == -1) ):
                # Adjusting scaffold size value
                scaffoldSize = self.localScaffoldSizes[scaffoldID]
                bestHitClass.scaffSize = scaffoldSize
                self.addPlacement( bestHitClass )
            else:
                # Right placement
                if   ( (scaffoldID.rfind('R__') == 0) and
                       (bestHitClass.placDir == '+') ):
                    # Adjusting scaffold size
                    scaffoldID = scaffoldID[3:]
                    scaffoldSize = self.localScaffoldSizes[scaffoldID]
                    bestHitClass.scaffSize = scaffoldSize
                    bestHitClass.scaffold  = scaffoldID
                    # Adjusting the right placement (start, end)
                    delta = scaffoldSize - self.trimLength
                    bestHitClass.scaffStart += delta
                    bestHitClass.scaffEnd   += delta
                    self.addPlacement( bestHitClass )
                # Left placement
                elif ( (scaffoldID.rfind('L__') == 0) and
                       (bestHitClass.placDir == '-') ):
                    # Adjusting scaffold size
                    scaffoldID = scaffoldID[3:]
                    scaffoldSize = self.localScaffoldSizes[scaffoldID]
                    bestHitClass.scaffold  = scaffoldID
                    bestHitClass.scaffSize = scaffoldSize
                    self.addPlacement( bestHitClass )
                #####
            #####
        #####
        return

    def generate_BAC_ID(self, bestHitClass):
        return bestHitClass.BAC_recordName.split(self.sepString)[0]

    def addPlacement(self, bestHitClass):
    
        # Store the BAC placement
        BAC_id = self.generate_BAC_ID(bestHitClass)
        try:
            self.BACid_placs[BAC_id].append( bestHitClass )
        except KeyError:
            self.BACid_placs[BAC_id] = [ bestHitClass ]
        #####

        # Adding in the BAC placement
        scaff_id = bestHitClass.scaffold
        try:
            self.scaffold_placs[scaff_id].append( bestHitClass )
        except KeyError:
            self.scaffold_placs[scaff_id] = [ bestHitClass ]
        #####
        return

    def runBestHit(self):
        if ( not self.debugMode ):
            DNA_best_hit( self.BES_FES_File, \
                          self.trimmedAssemblyFile, \
                          nFastaFiles=self.queueSize, \
                          clusterNum=self.cluster_Num, \
                          outputFileName=self.bestHit_outputFile, \
                          keyWords=self.DNA_best_hit_keywords, \
                          allowMultipleTopHits=self.allowMultipleTopHits )
        #####
        return

    def trimAssemblyFile(self):
        # Generate the trimmed file
        chdir(self.tmpPath)
        self.trimmedAssemblyFile, self.localScaffoldSizes = \
                      trimAssemblyFile( self.assemblyFile, self.trimLength )
        self.trimmedAssemblyFile = join(self.tmpPath, self.trimmedAssemblyFile)
        chdir(self.basePath)
        return

    def makeOptionsString(self):
        self.optionsList     = 12 * ['']
        self.optionsList[0]  = '\nSettings:\n'
        self.optionsList[1]  = '---------\n'
        self.optionsList[2]  = '\t-ASSEMBLY        = %s\n'%self.assemblyFile
        self.optionsList[3]  = '\t-BES/FES FILE    = %s\n'%self.BES_FES_File
        self.optionsList[4]  = '\t-OUTPUT          = %s\n'%self.outputFile
        self.optionsList[5]  = '\t-TRIM LENGTH     = %d\n'%self.trimLength
        self.optionsList[6]  = '\t-COVERAGE CUTOFF = %f%s\n'% \
                                                     (self.coverage_Cutoff,r'%')
        self.optionsList[7]  = '\t-IDENTITY CUTOFF = %f%s\n'% \
                                                     (self.identity_Cutoff,r'%')
        self.optionsList[9]  = '\t-DEBUG MODE      = %s\n'%self.debugMode
        self.optionsList[10] = '\t-CLUSTER NUMBER  = %d\n'%self.cluster_Num
        self.optionsList[11] = '\t-QUEUE SIZE      = %d\n\n'%self.queueSize
        return

#####