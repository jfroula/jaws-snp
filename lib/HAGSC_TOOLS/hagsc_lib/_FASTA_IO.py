__author__ = "jjenkins"
__date__   = "$Mar 3, 2010 10:01:49 AM$"

__author__="jjenkins"
__date__ ="$Jun 7, 2010 12:29:52 PM$"

# Local library imports
from _helperFunctions import baseFileName
from _helperFunctions import isBzipFile
from _helperFunctions import isGzipFile
from _helperFunctions import deleteFile
from _helperFunctions import testFile

from _helperClasses import iterCounter

from _globalSettings import translationTable

# Python imports
from StringIO import StringIO

from os import listdir
from os import system
from os import curdir
from os import chdir

from subprocess import Popen
from subprocess import PIPE

from os.path import getmtime
from os.path import splitext
from os.path import abspath
from os.path import isfile
from os.path import split
from os.path import join

from math import fmod

from sys import stdout
from sys import stderr
from sys import maxint

from array import array

import re

from string import maketrans

########################################################################
def findBreaks( tmpSeq ):
    breakFinder = re.compile( r'N{5,}' ).finditer
    return [item.span() for item in breakFinder(tmpSeq)]

########################################################################
def findContigs( tmpSeq ):
    breakFinder = re.compile( r'N{5,}' ).finditer
    nBases = len(tmpSeq)
    d = [item.span() for item in breakFinder(tmpSeq)]
    d.insert(0,(0,0))
    d.append((nBases,nBases))
    nContig = 1
    return [ ( d[n][1], d[n+1][0] ) for n in xrange( len(d) - 1 ) ]

########################################################################
def revComp( seq ):
    return seq.translate(translationTable)[::-1]

########################################################################
def generateCloneFASTAFile(cloneNum):
    # Changing directories to the
    workingDir = abspath(curdir)
    cloneDir = '/mnt/mongo3/doeassem/%s/edit_dir'%cloneNum
    stderr.write( 'cd %s\n'%cloneDir )
    chdir( cloneDir )

    # Generating the clone fasta file
    stderr.write('orchid --print-fasta\n')
    system( 'orchid --print-fasta' )

    # Sorting all files in clone directory by creation date
    files = filter(isfile, listdir(cloneDir))
    files = [join(cloneDir, f) for f in files] # add path to each file
    files.sort( reverse=True, key=lambda x: getmtime(x) )

    # Capturing the newly generatedFASTA File name
    if ( splitext(files[0])[1] == '.fasta' ):
        cloneFASTAFile = files[0]
    else:
        stderr.write( 'ERROR:  %s is not a FASTA file'%files[0] )
        assert False
    #####

    # Changing back to the working directory
    chdir( workingDir )
    return cloneFASTAFile

########################################################################
class reorder_by_Size(object):

    def __init__(self, assemblyFile, qualFile=None, startIndex=0, \
                                                           scaffold_ID='super'):

        self.assemblyFile = assemblyFile
        self.qualFile     = qualFile
        self.startIndex   = startIndex
        self.scaffold_ID  = scaffold_ID

        self.baseName = baseFileName(self.assemblyFile)

        # Output Files
        self.fastaOutputFile = '%s_reindexed.fasta'%self.baseName
        self.translatorFile = '%s_translatorFile.dat'%self.baseName
        if ( self.qualFile != None ):
            self.qualOutputFile  = '%s_reindexed.qual'%self.baseName
        #####

        self.performRenumbering()

    def performRenumbering(self):

        # Indexing FASTA file
        fastaHandle = open( self.assemblyFile, 'r' )
        indexedFile = indexFASTAFile( fastaHandle )

        # Indexing Qual file
        if ( self.qualFile != None ):
            qualHandle  = open( self.qualFile, 'r' )
            indexedQualFile = indexQUALFile( qualHandle )
        #####

        # Sort by size
        scaffSizes = []
        for scaffID, filePos in indexedFile.items():
            scaffSizes.append( (len(pullSequence(fastaHandle,filePos)), scaffID) )
        #####
        scaffSizes.sort(reverse=True)

        # Output_file
        outputHandle     = open( self.fastaOutputFile, 'w')
        if ( self.qualFile != None ):
            outputQualHandle = open( self.qualOutputFile, 'w')
        #####
        transHandle      = open( self.translatorFile, 'w' )
        transHandle.write( 'old_ID\tnew_ID\n' )

        # Write the rest of the scaffolds
        scaffCounter = self.startIndex
        for size, scaffID in scaffSizes:
            # Form the record
            filePos = indexedFile[scaffID]
            tmpSeq = pullSequence(fastaHandle,filePos)
            new_ID = '%s_%d'%(self.scaffold_ID,scaffCounter)
            stderr.write( 'old_ID = %s\t|\tnew_ID = %s\n'%(scaffID, new_ID) )
            record = SeqRecord( seq = tmpSeq, \
                                id=new_ID, \
                                description='' )
            # Write the record
            writeFASTA( [record], outputHandle )
            transHandle.write( '%s\t%s\n'%(scaffID,new_ID) )

            if ( self.qualFile != None ):
                #Writing the QUAL file
                outputQualHandle.write( '>%s\n'%new_ID )
                filePos = indexedQualFile[scaffID]
                scores = pullScores( qualHandle, filePos )
                for tmpScores in scores:
                    outputQualHandle.write( '%s\n'%tmpScores )
                #####
            #####

            # Index the counter
            scaffCounter += 1
        #####

        # Closing the files
        outputHandle.close()
        fastaHandle.close()
        if ( self.qualFile != None ):
            qualHandle.close()
            outputQualHandle.close()
        #####

    #####

########################################################################
class equal_read_split( object ):

    """
    equal_read_split(readFile, base_outFileName, nFiles)

    Splits "readFile" into "nFiles" files that contain an equal number of
    reads.  Assumes that the read length is essentially constant.
    """

    #==============================================================
    def __init__(self, fastaFile, base_outFileName, nFiles):

        testFile(fastaFile)

        self.fastaFile        = fastaFile
        self.base_outFileName = base_outFileName
        self.nFiles           = nFiles
        self.scaffoldNames    = set()

        self.splitFile()

    #==============================================================
    def iterITEMS(self, tmpHandle ):
        # Skipping any blank lines or comments at the top of the file
        while True:
            line = tmpHandle.readline()
            if ( line == "" ):
                raise ValueError( 'Empty line prior to first record ' + \
                                  'encountered.  Exiting.\n')
                exit()
            #####
            if ( line[0] == ">" ): break
        #####
    
        # Pulling the data
        while True:
            if ( line[0] != ">" ):
                raise ValueError( "Records in Fasta files should " +
                                  "start with '>' character" )
            #####
            # Pulling the id
            id = line[1:].split(None,1)[0]
            # Reading the sequence
            tmpData = []
            line    = tmpHandle.readline()
            while True:
                # Termination conditions
                if ( (not line) or (line[0] == '>') ): break
                # Adding a line
                tmpData.append( line.strip() )
                line = tmpHandle.readline()
            #####
            yield {'id':id, 'data':''.join(tmpData)}
            # Stopping the iteration
            if ( not line ):
                return
            ####
        #####
    
    def splitFile(self):
        # Computing the target number of records for a file
        if ( isGzipFile(self.fastaFile) ):
            cmdString = r"cat %s | gunzip -c | grep -c '>'"%self.fastaFile
        elif ( isBzipFile(self.fastaFile) ):
            cmdString = r"cat %s | bunzip2 -c  | grep -c '>'"%self.fastaFile
        else:
            cmdString = r"grep -c '>' %s"%self.fastaFile
        #####
        nReads =  int( [item.strip() for item in Popen( cmdString, shell=True, stdout=PIPE ).stdout][0] )
        targetRecords = int( float(nReads) / float(self.nFiles) )
        # Assuming that the read length is essentially the same, then just split
        # the reads equally between files
        recordCounter = 0
        fileCounter   = 1
        tmpFileName   = '%s_%d.fasta'%(self.base_outFileName, fileCounter)
        tmpHandle     = open( tmpFileName, 'w' )
        x = iterCounter(1000000)
        if ( isGzipFile(self.fastaFile) ):
            cmdString = "cat %s | gunzip -c "%self.fastaFile
        elif ( isBzipFile(self.fastaFile) ):
            cmdString = "cat %s | bunzip2 -c "%self.fastaFile
        else:
            cmdString = "cat %s "%self.fastaFile
        #####
        tmpProcess = Popen( cmdString, shell=True, stdout=PIPE )
        for record in self.iterITEMS( tmpProcess.stdout ):
            # Writing the scaffold to the current file
            tmpHandle.write( '>%s\n%s\n'%(record['id'], record['data'] ) )
            x()
            # Incrementing the size
            recordCounter += 1
            if ( recordCounter >= targetRecords ):
                stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                              fileCounter, recordCounter, targetRecords) )
                tmpHandle.close()
                fileCounter += 1
                tmpFileName = '%s_%d.fasta'%(self.base_outFileName,fileCounter)
                tmpHandle   = open( tmpFileName, 'w' )
                recordCounter = 0
            #####
        #####
        stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                                    fileCounter, recordCounter, targetRecords) )
        tmpProcess.poll()
        return

#####

########################################################################
class equal_size_split( object ):

    """
    equal_size_split(fastaFile, base_outFileName, nFiles)

    Splits "fastaFile" into "nFiles" files that are as
    equally sized as possible.
    """

    def __init__(self, fastaFile, base_outFileName, nFiles, arachneReadFormat=False, noSort=False):

        testFile(fastaFile)

        self.fastaFile         = fastaFile
        self.base_outFileName  = base_outFileName
        self.nFiles            = nFiles
        self.arachneReadFormat = arachneReadFormat

        if ( noSort ):
            self.splitFile_noSort()
        else:
            self.splitFile()
        #####

    def splitFile_noSort(self):
        
        stderr.write( '\t\tSPLITTING WITHOUT SORTING\n\n' )
        
        # (1) Count all bases
        totalSize   = 0
        x = iterCounter(10000000)
        for seqRecord in iterFASTA( open(self.fastaFile) ):
            totalSize += len( seqRecord.seq )
            x()
        #####
        
        # Computing the target file size
        targetNumBases = int( float(totalSize) / float(self.nFiles) )
        
        # (2) Partition out reads
        sizeCounter = 0
        fileCounter = 1
        tmpFileName = '%s_%d.fasta'%(self.base_outFileName,fileCounter)
        tmpHandle   = open( tmpFileName, 'w' )
        for seqRecord in iterFASTA( open(self.fastaFile) ):
            # Writing the scaffold to the current file
            writeFASTA( [seqRecord], tmpHandle )
            # Incrementing the size
            sizeCounter += len(seqRecord)
            if ( sizeCounter > targetNumBases ):
                stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                              fileCounter, sizeCounter, targetNumBases) )
                tmpHandle.close()
                fileCounter += 1
                tmpFileName = '%s_%d.fasta'%(self.base_outFileName,fileCounter)
                tmpHandle   = open( tmpFileName, 'w' )
                sizeCounter = 0
            #####
        #####
        stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                              fileCounter, sizeCounter, targetNumBases) )
        # Closing the handle in the file dictionary
        return

    def splitFile(self):

        # (1) Order the scaffolds by size
        indexedFile = FASTAFile_dict( self.fastaFile, self.arachneReadFormat )
        DSU         = []
        totalSize   = 0
        x = iterCounter(100000)
        for scaffID, seqRecord in indexedFile.items():
            scaffSize = len( seqRecord )
            DSU.append( (scaffSize, scaffID) )
            totalSize += scaffSize
            x()
        #####
        DSU.sort( reverse=True )

        # Computing the target file size
        targetNumBases = int( float(totalSize) / float(self.nFiles) )

        # (2) Start with the largest scaffold and form the files
        sizeCounter = 0
        fileCounter = 1
        tmpFileName = '%s_%d.fasta'%(self.base_outFileName,fileCounter)
        tmpHandle   = open( tmpFileName, 'w' )
        for scaffSize, scaffID in DSU:
            # Writing the scaffold to the current file
            writeFASTA( [indexedFile[scaffID]], tmpHandle )
            # Incrementing the size
            sizeCounter += scaffSize
            if ( sizeCounter > targetNumBases ):
                stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                              fileCounter, sizeCounter, targetNumBases) )
                tmpHandle.close()
                fileCounter += 1
                tmpFileName = '%s_%d.fasta'%(self.base_outFileName,fileCounter)
                tmpHandle   = open( tmpFileName, 'w' )
                sizeCounter = 0
            #####
        #####
        stderr.write( '\t\tfileCounter: %d, Size: %d, Target: %d\n'%( \
                              fileCounter, sizeCounter, targetNumBases) )
        # Closing the handle in the file dictionary
        indexedFile._handle.close()
        del indexedFile
        return

#####

########################################################################
class createStatsFile( object ):

    def __init__(self, assemblyFile, numNs):
        self.assemblyFile = assemblyFile

        # Regular expression for adding commas using look aheads
        self.pattern = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))')

        # Breaking the contigs
        self.contigBreakFinder = re.compile( r'N{%d,}'%numNs ).finditer

        # Create the file
        self.createStatsFile()

    def createStatsFile(self):

        # Reading in the information
        perString    = '%'
        outputString = []
        
        isCompressed = False
        if ( isGzipFile(self.assemblyFile) ):
            tmpProcess   = Popen( 'cat %s | gunzip -c'%(self.assemblyFile), shell=True, stdout=PIPE )
            isCompressed = True
            oh           = tmpProcess.stdout
        elif ( isBzipFile(self.assemblyFile) ):
            tmpProcess   = Popen( 'cat %s | bunzip2 -c'%(self.assemblyFile), shell=True, stdout=PIPE )
            isCompressed = True
            oh           = tmpProcess.stdout
        else:
            oh = open(self.assemblyFile)
        #####
        
        # Printing out each record as they come
        DSU = []
        xx  = iterCounter(10000)
        for record in iterFASTA( oh ):
            # Pulling the size of the sequence
            nSize = len(record.seq)
            # Finding the number of breaks
            tmpSeq = str(record.seq)
            if ( tmpSeq.count('N') > 0 ):
                nContigs = len([item for item in self.contigBreakFinder(tmpSeq)]) + 1
            else:
                nContigs = 1
            #####
            # Creating the lines
            fileLine = []
            fileLine.append( (record.id).rjust(8) )
#             if ( record.id.split("_")[0] == 'super' ):
#                 fileLine.append( (record.id[6:]).rjust(8) )
#             elif ( record.id.split("_")[0] == 'scaffold' ):
#                 fileLine.append( (record.id[9:]).rjust(8) )
#             else:
#                 fileLine.append( (record.id).rjust(8) )
#             #####
            # Computing the number of contigs
            fileLine.append( ('%d'%nContigs).rjust(9) )
            # Computing the scaffold size
            scaffSize = re.sub( self.pattern, ',', '%d'%(len(tmpSeq)) )
            fileLine.append( (scaffSize).rjust(15) )
            basePairs = re.sub( self.pattern, ',', '%d'%(record.basePairs) )
            fileLine.append( (basePairs).rjust(13) )
            fileLine.append( ('00.00%').rjust(13) )
            fileLine.append( ('0.00x').rjust(13) )
            try:
                GC_per = 100.0 * float( record.G_bases + record.C_bases ) / float(record.basePairs)
            except ZeroDivisionError:
                GC_per = 0.0
            #####
            fileLine.append( (('%.2f%s\n'%(GC_per,perString)).rjust(9) ) )
            # Storing the line
            DSU.append( ( nSize, ''.join(fileLine) ) )
            xx()
        #####
        
        # Closing the handle
        if ( isCompressed ):
            tmpProcess.poll()
        else:
            oh.close()
        #####

        # Writing the output to a file
        stdout.write( '%s:\n\n'%(abspath( curdir )) )
        stdout.write( 'Scaffold  Contigs  Scaffold Size   Basepairs   Bp Coverage  HQ Coverage    GC %\n')
        stdout.write( '--------  -------  -------------  -----------  -----------  -----------    ----\n' )
        DSU.sort( reverse = True )
        for nSize, outputString in DSU:
            stdout.write( outputString )
        #####
        return

#####

########################################################################
def countBasePairs(seq):
    seq     = str(seq).upper()
    A_bases = seq.count('A') + seq.count('a')
    C_bases = seq.count('C') + seq.count('c')
    G_bases = seq.count('G') + seq.count('g')
    T_bases = seq.count('T') + seq.count('t')
    return ( A_bases + C_bases + G_bases + T_bases )

########################################################################
def computeGC(seq):
    seq      = str(seq).upper()
    GC_bases = float( seq.count('G') + seq.count('g') + seq.count('C') + seq.count('c') )
    AT_bases = float( seq.count('A') + seq.count('a') + seq.count('T') + seq.count('t') )
    if ( AT_bases > 0 ):
        return 100.0 * GC_bases / ( GC_bases + AT_bases )
    else:
        return 0.0
    #####

########################################################################
class scaffoldStatsClass(object):
    def __init__(self, scaffID, scaffSize, seqBases, GC_per, LC_bases):
        self.scaffID    = scaffID
        self.scaffSize  = scaffSize
        self.seqBases   = seqBases
        self.GC_per     = GC_per
        self.LC_bases   = LC_bases
        self.t2_bases   = None
        self.t4_bases   = None

    def add_t2_bases(self,t2_bases):
        self.t2_bases = t2_bases

    def add_t4_bases(self,t4_bases):
        self.t4_bases = t4_bases

    def generateString(self):
        t2_unique = 100.0 * float(self.t2_bases) / float(self.seqBases)
        t4_unique = 100.0 * float(self.t4_bases) / float(self.seqBases)
        LCper     = 100.0 * float(self.LC_bases) / float(self.seqBases)
        return '%s\t%d\t%d\t%d\t%d\t%5.2f\t%5.2f\t%5.2f\t%d\t%5.2f\n'%( self.scaffID, \
            self.scaffSize, self.seqBases, self.t2_bases, self.t4_bases, \
                                       t2_unique, t4_unique, self.GC_per, self.LC_bases, LCper )

########################################################################
def repetitiveContent( assemblyFile, t2File, t4File ):

    # Reading in the bases
    dataContainer = {}
    stderr.write( 'Parsing %s....\n'%assemblyFile )
    if ( isGzipFile(assemblyFile) ):
        assemH = Popen( 'cat %s | gunzip -c'%assemblyFile, shell=True, stdout=PIPE ).stdout
    elif ( isBzipFile( assemblyFile ) ):
        assemH = Popen( 'cat %s | bunzip2 -c'%assemblyFile, shell=True, stdout=PIPE ).stdout
    else:
        assemH = open(assemblyFile,'r')
    #####
    for record in iterFASTA( assemH ):
        scaffID    = record.id
        scaffSize  = len(record.seq)
        seqBases   = record.basePairs
        GC_per     = 100.0*float(record.GC_bases)/float(seqBases)
        dataContainer[scaffID] = scaffoldStatsClass( scaffID, scaffSize, \
                                                     seqBases, GC_per, \
                                                     record.LC_bases )
    #####

    # Conting t2 bases
    stderr.write( 'Parsing %s....\n'%t2File )
    if ( isGzipFile(t2File) ):
        t2_h = Popen( 'cat %s | gunzip -c'%t2File, shell=True, stdout=PIPE ).stdout
    elif ( isBzipFile( t2File ) ):
        t2_h = Popen( 'cat %s | bunzip2 -c'%t2File, shell=True, stdout=PIPE ).stdout
    else:
        t2_h = open(t2File,'r')
    #####
    for record in iterFASTA( t2_h ):
        scaffID    = record.id
        dataContainer[scaffID].add_t2_bases( record.basePairs )
    #####

    # Counting t4 bases
    stderr.write( 'Parsing %s....\n'%t4File )
    if ( isGzipFile(t4File) ):
        t4_h = Popen( 'cat %s | gunzip -c'%t4File, shell=True, stdout=PIPE ).stdout
    elif ( isBzipFile( t4File ) ):
        t4_h = Popen( 'cat %s | bunzip2 -c'%t4File, shell=True, stdout=PIPE ).stdout
    else:
        t4_h = open(t4File,'r')
    #####
    for record in iterFASTA( t4_h ):
        scaffID    = record.id
        dataContainer[scaffID].add_t4_bases( record.basePairs )
    #####

    # Output to a file
    filePre   = splitext( split(assemblyFile)[1] )[0]
    header = 10*['']
    header[0] = 'ScaffID'
    header[1] = 'scaffSize'
    header[2] = 'numBases'
    header[3] = 't2-bases'
    header[4] = 't4-bases'
    header[5] = r't2%'
    header[6] = r't4%'
    header[7] = r'GC%'
    header[8] = r'LC-bases'
    header[9] = r'LC%'
    stdout.write( '%s\n'%( '\t'.join(header) ) )
    for scaffID, statsClass in dataContainer.items():
        stdout.write( statsClass.generateString() )
    #####
    return

########################################################################
def generateTrimmedFileName(fastaFile):
    """
    generateTrimmedFileName( fastaFile )

    Generates the trimmed assembly file name.  Enables me to check to see
    whether I have generated this file elsewhere in the code.
    """
    fastaFilePath, fastaFileName = split(fastaFile)
    pre, extension = splitext(split(fastaFileName)[1])
    return '%s_trimmed%s' % (pre, extension)

########################################################################
def trimAssemblyFile(fastaFile, trimLength=140000, taLength=0, ignoreSet=None, outputFileName=None):
    """
    trimAssemblyFile(fastaFile, trimLength=140000, taLength=0, ignoreSet=None, outputFileName=None)

    Trims the left and right sides of a scaffold in a FASTA file
    and writes the trimmed portions to a file.  This function can also remove
    a length of sequence at the ends of the scaffolds (taLength) if there
    is low quality sequence on the ends
    """
    testFile(fastaFile)
    stderr.write('-Trimming: %s\n' % fastaFile)
    assemblyTempFile = outputFileName
    if ( outputFileName == None ):  assemblyTempFile = generateTrimmedFileName(fastaFile)
    deleteFile( assemblyTempFile )
    if ( ignoreSet == None ): ignoreSet=set()
    scaffoldSizes  = {}
    assemblyHandle = open( assemblyTempFile, 'w')
    for record in iterFASTA( open(fastaFile, 'r') ):
        if ( record.id in ignoreSet ): continue
        # Storing the scaffold size
        scaffoldSize = len(record.seq)
        scaffoldSizes[record.id] = scaffoldSize
        if ( scaffoldSize > (2*(trimLength+taLength)) ):
            # Creating the left record
            if ( taLength == 0 ):
                left_record = SeqRecord(record.seq[:trimLength])
            else:
                left_record = SeqRecord(record.seq[taLength:(trimLength+taLength)])
            #####
            left_record.id          = 'L__%s'%record.id
            left_record.description = record.description
            # Creating the right record
            if ( taLength == 0 ):
                right_record = SeqRecord(str(record.seq)[-trimLength:])
            else:
                right_record = SeqRecord(record.seq[-(trimLength+taLength):-(taLength)])
            #####
            right_record.id          = 'R__%s'%record.id
            right_record.description = record.description
            # Writing the records to the file
            writeFASTA([left_record, right_record], assemblyHandle)
        else:
            # Write the record directly to the temp file
            record.id = "W__%s"%record.id
            writeFASTA([record], assemblyHandle)
        #####
    #####
    assemblyHandle.close()
    return assemblyTempFile, scaffoldSizes

########################################################################
def pullScaffolds( fastaFile, outputFile, scaffoldFileName, maxSize=None, \
                   minSize=None, exclusionMode=False, transform=None, \
                   arachneReadNames=False, largeFileMode=False ):

    """
    pullScaffolds( fastaFile, outputFile, scaffoldFileName, maxSize=None, 
                   minSize=None, exclusionMode=False )

    Writes a set of scaffolds obtained from a fasta file to an
    output file.
    """

    # Testing the files
    testFile(fastaFile)
    testFile(scaffoldFileName)
    
    # Generating the size inclusion lambda function
    if ( (maxSize != None) and (minSize != None) ):
        includeRead = lambda n:(minSize < n < maxSize)
    elif(minSize != None):
        includeRead = lambda n:(minSize < n)
    elif(maxSize != None):
        includeRead = lambda n:(n < maxSize)
    else:
        includeRead = lambda n:True
    #####

    # Reading in the scaffold names
    filterSet = set()
    for line in open(scaffoldFileName,'r'):
        parsedLine = line.split(None)
        if ( parsedLine != [] ): filterSet.add( parsedLine[0] )
    #####

    # Indexing the FASTA file
    if ( not largeFileMode ):
        indexedFile = FASTAFile_dict( fastaFile, \
                                      arachneReadFormat=arachneReadNames )
    #####

    # Pulling the scaffolds
    deleteFile(outputFile)
    outputHandle = open( outputFile, 'w' )
    stderr.write( 'Writing Scaffolds to %s\n'%outputFile )
    
    x = iterCounter(100000)
    if ( exclusionMode ):
        excludedSet = set()
        # Using the iterFASTA for large files
        if ( largeFileMode ):    
            for record in iterFASTA(open(fastaFile), arachneReads=arachneReadNames):
                # Making sure it is not in the filter set
                scaffID = record.id
                if ( scaffID in filterSet ): 
                    excludedSet.add( scaffID )
                    continue
                #####
                if ( not includeRead( len(record.seq) ) ): continue
                if transform: record = transform(record)
                writeFASTA( [record], outputHandle )
                x()
            #####
            outputHandle.close()
        else:
            # Using the indexing for small files
            for scaffID in indexedFile.iterkeys():
                # Making sure it is not in the filter set
                if ( scaffID in filterSet ): 
                    excludedSet.add( scaffID )
                    continue
                #####
                # Screening based on size
                record = indexedFile[scaffID]
                if ( not includeRead( len(record.seq) ) ): continue
                if transform: record = transform(record)
                writeFASTA( [record], outputHandle )
                x()
            #####
            outputHandle.close()
        #####
        
        # Report any unexcluded to the user
        remainderReads = excludedSet.symmetric_difference(filterSet)
        if ( len(remainderReads) != 0 ):
            stderr.write( 'SCAFFOLDS NOT EXCLUDED:\n' )
            for item in remainderReads: stderr.write('\t%s\n'%item)
            return False
        else:
            stderr.write( 'ALL SCAFFOLDS EXCLUDED:\n' )
            return True
        #####
    else:
        notFoundSet = set()
        if ( largeFileMode ):
            for record in iterFASTA(open(fastaFile), arachneReads=arachneReadNames):
                scaffID = record.id
                if ( scaffID in filterSet ): 
                    if ( not includeRead( len(record.seq) ) ): continue
                    if transform: record = transform(record)
                    writeFASTA( [record], outputHandle )
                    filterSet.remove(scaffID)
                #####
                x()
            #####
        else:
            indexedSet  = set( indexedFile.keys() )
            for scaffID in filterSet:
                if ( scaffID in indexedSet ):
                    record = indexedFile[scaffID]
                    if ( not includeRead( len(record.seq) ) ): continue
                    if transform: record = transform(record)
                    writeFASTA( [record], outputHandle )
                else:
                    notFoundSet.add(scaffID)
                #####
                x()
            #####
        #####
        outputHandle.close()
        # Analyzing the output
        if ( largeFileMode ):
            if ( len(filterSet) == 0 ):
                return True
            else:
                stderr.write( 'SCAFFOLDS NOT FOUND:\n' )
                for item in filterSet: stderr.write('\t%s\n'%str(item))
                return False
            #####
        else:
            if ( len(notFoundSet) == 0 ):
                return True
            else:
                stderr.write( 'SCAFFOLDS NOT FOUND:\n' )
                for item in notFoundSet: stderr.write('\t%s\n'%str(item))
                return False
            #####
        #####
        
    #####


########################################################################
def pullScaffold( fastaFile, outputFile, scaffoldName, \
                  scaffStart, scaffEnd, reverseComp ):
    """
    pullScaffold( fastaFile, outputFile, scaffoldName, scaffStart, scaffEnd )

    Writes a specific scaffold obtained from a fasta file to an
    output file.
    """
    # Performing testing
    testFile(fastaFile)
    deleteFile(outputFile)

    # Pulling the scaffold
    indexedFile = FASTAFile_dict( fastaFile )
    indexedSet  = set( indexedFile.keys() )
    if ( scaffoldName in indexedSet ):
        outputHandle = open(outputFile,'w')
        record = indexedFile[scaffoldName]
        scaffLen = len(record.seq)
        scaffEnd = ((scaffEnd>scaffLen) and [scaffLen] or [scaffEnd])[0]
        if ( reverseComp ):
            record.seq = revComp( record.seq[scaffStart:scaffEnd] )
        else:
            record.seq = record.seq[scaffStart:scaffEnd]
        #####
        writeFASTA( [record], outputHandle )
        stderr.write( '%d bases written\n'%(scaffEnd-scaffStart) )
        outputHandle.close()
        return True
    else:
        # Did not find the scaffold
        stderr.write( 'ERROR: %s not in %s\n'%(scaffoldName,fastaFile) )
        return False
    #####

########################################################################
class split_and_screen(object):

    """
    split_and_screen(fastaFile, base_outFileName, nRecords, minLength=0)

    Splits "fastaFile" into individual files that
    contain no more than nRecords, ignoring sequences
    below minLength.  Default minlength is 0.
    """

    def __init__(self, fastaFile, base_outFileName, nRecords, minLength, maxLength):

        testFile(fastaFile)

        self.fastaFile        = fastaFile
        self.base_outFileName = base_outFileName
        self.nRecords         = nRecords
        self.minLength        = minLength
        self.maxLength        = maxLength

        self.splitFile()

    def splitFile(self):
        recordCounter  = 0
        outFileCounter = 1
        nScreened      = 0
        nTotal         = 0
        if ( self.minLength > 0 ):
            if ( self.maxLength > 0 ):
                lengthScreen = lambda x: ( self.minLength <= len(x.seq) <= self.maxLength )
            else:
                lengthScreen = lambda x: ( self.minLength <= len(x.seq) )
            #####
        else:
            if ( self.maxLength > 0 ):
                lengthScreen = lambda x: ( len(x.seq) <= self.maxLength )
            else:
                lengthScreen = lambda x: True
            #####
        #####
        outputFileName = '%s_%d.fasta' % (self.base_outFileName, \
                                          outFileCounter)
        stderr.write('Writing %s...\n' % outputFileName)
        outputHandle = open( outputFileName, 'w' )
        
        # Opening the file
        if ( isGzipFile(self.fastaFile) ):
            import gzip
            self.handle = gzip.open(self.fastaFile,'r')
        elif ( isBzipFile(self.fastaFile) ):
            import bz2
            self.handle = bz2.BZ2File(self.fastaFile,'r')
        else:
            self.handle   = open(self.fastaFile, 'r')
        #####

        if ( self.nRecords > 0 ):        
            for record in iterFASTA( self.handle ):
                if ( lengthScreen(record) ):
                    recordCounter += 1
                    writeFASTA( [record], outputHandle )
                    if (fmod(recordCounter, self.nRecords) == 0.0):
                        outputHandle.close()
                        outFileCounter += 1
                        outputFileName = '%s_%d.fasta' % (self.base_outFileName, \
                                                      outFileCounter)
                        stderr.write('Writing %s...\n' % outputFileName)
                        outputHandle = open( outputFileName, 'w' )
                    #####
                else:
                    nScreened += 1
                #####
                nTotal += 1
            #####
        else:
            for record in iterFASTA( self.handle ):
                if ( lengthScreen(record) ):
                    writeFASTA( [record], outputHandle )
                    recordCounter += 1
                else:
                    nScreened += 1
                #####
                nTotal += 1
            #####
        #####
        self.handle.close()
        outputHandle.close()

        # Reporting the statistics
        if ( self.minLength < 0 ): self.minLength = 0
        if ( self.minLength < 0 ): 
            self.maxLength = 'INF'
        else:
            self.maxLength = '%d'%self.maxLength
        outputString = '%s:  %d out of %d reads fell outside of the range of %d <= Length <= %s\n' % \
                             (self.fastaFile, nScreened, nTotal, self.minLength, self.maxLength)
        stderr.write(outputString)

#####

########################################################################
########################################################################
# CORE FASTQ FILE HANDLING ROUTINES
########################################################################
########################################################################
class iterFASTQ(object):

    def __init__( self, fastqFileHandle, \
                        headerSymbols=['@','+'],\
                        arachneReads=False ):

        if isinstance( fastqFileHandle, basestring ):
            raise TypeError( "Need a file handle, not a string " + \
                             "(i.e. not a filename)" )
        #####

        self._currentLineNumber = 0    
        self._handle            = fastqFileHandle
        self._hdSyms            = headerSymbols
        self.tmpRange           = range(4)

        if ( arachneReads ):
            self.buildFASTARecord = lambda seq, scaffID, descr: {'seq':seq, \
                                                                 'id':descr.strip(), \
                                                                 'description':scaffID}
            self.buildQUALRecord = lambda quals, scaffID, descr: {'quals':quals , \
                                                                  'id':descr.strip(), \
                                                                  'description':scaffID}
        else:
            self.buildFASTARecord = lambda seq, scaffID, descr: {'seq':seq, \
                                                                 'id':scaffID, \
                                                                 'description':descr.strip()}
            self.buildQUALRecord = lambda quals, scaffID, descr: {'quals':quals , \
                                                                  'id':scaffID, \
                                                                  'description':descr.strip()}
        #####

    def __iter__(self):
        return self

    def next(self):
        # Pulling the Next Four Lines
        elemList = []
        for i in self.tmpRange:
            line = self._handle.readline()
            self._currentLineNumber += 1
            if line:
                elemList.append( line.strip('\n') )
            else:
                elemList.append( None )
            #####
        #####

        # Check Lines For Expected Form
        numTrues = [bool(x) for x in elemList].count(True)
        numNones = elemList.count(None)
        # -- Check for acceptable end of file --
        if numNones == 4:
            raise StopIteration
        #####
        # Make sure we got 4 full lines of data
        assert numTrues == 4,\
               "** ERROR: It looks like I encountered a premature EOF or empty line.\n\
               Please check FastQ file near line #%s (plus or minus ~4 lines) and try again**" % (self._currentLineNumber)
        #####
        # Make sure we are in the correct start character
        assert elemList[0].startswith(self._hdSyms[0]),\
               "** ERROR: The 1st line in fastq element does not start with '%s'.\n\
               Please check FastQ file and try again **" % (self._hdSyms[0])
        #####
        # Make sure we are in the correct start character
        assert elemList[2].startswith(self._hdSyms[1]),\
               "** ERROR: The 3rd line in fastq element does not start with '%s'.\n\
               Please check FastQ file and try again **" % (self._hdSyms[1])

        # Send the data off
        readID = elemList[0][1:]
        
        return { 'seq':self.buildFASTARecord( elemList[1], readID, ''), \
                 'qual':self.buildQUALRecord(  elemList[3], readID, '' )  }

    def getNextReadSeq(self):
        try:
            return self.next()
        except StopIteration:
            return None
        #####

########################################################################
########################################################################
# CORE FASTA FILE HANDLING ROUTINES
########################################################################
########################################################################
class Seq( object ):

    """
    Read-only sequence object.

    Like normal python strings, the sequence object is immutable, but does
    allow Seq objects to be used as dictionary keys.

    The Seq object provides a number of string like methods (such as count,
    find, split and strip).
    """

    def __init__(self, data):
        """
        Create a Seq object.
        """
        # Strongly enforce typing:  string or unicode
        assert ( (type(data) == type("")) or (type(data) == type(u"")) )
        # Storing the data
        self._data       = data
        self.isSeqObject = True

    def __repr__(self):
        """
        Returns a (truncated) representation of the sequence for debugging.
        """
        if ( len(self._data) > 60 ):
            # Shows the last three letters as it is useful to see if there
            # is a stop codon at the end of a sequence.
            return "%s('%s...%s')" % ( self.__class__.__name__, \
                                       str(self)[:54], \
                                       str(self)[-3:] )
        else:
            return "%s(%s)" % ( self.__class__.__name__, repr(self._data) )
        #####

    def __str__(self):
        """
        Returns the full sequence as a python string, use str(my_seq).
        """
        return self._data

    def __len__(self):
        """
        Returns the length of the sequence, use len(my_seq).
        """
        return len( self._data )

    def __getitem__(self, index):
        """
        Returns a subsequence of single letter, use my_seq[index].
        """
        if isinstance(index, int):
            # Return a single letter as a string
            return self._data[index]
        else:
            # Return the (sub)sequence as another Seq object
            return Seq( self._data[index] )
        ####

    def __add__(self, other):
        """
        Add another sequence or string to this sequence.
        """
        # Checking to see if this is a Seq object
        if isinstance( other, Seq ):
            return self.__class__( str(self) + str(other) )
        elif isinstance( other, basestring ):
            return self.__class__( str(self) + other )
        else:
            raise TypeError
        #####

    def __radd__(self, other):
        """
        Adding a sequence on the left.
        """
        if isinstance( other, Seq ):
            return self.__class__( str(other) + str(self) )
        elif isinstance( other, basestring ):
            return self.__class__( other + str(self) )
        else:
            raise TypeError
        #####

    def revComp(self):
        """
        Reverse complements the sequence
        """
        return revComp(self._data)

    def count( self, sub, start=0, end=maxint ):
         """
         Non-overlapping count method, like that of a python string.

         This behaves like the python string method of the same name,
         which does a non-overlapping count!

         Returns an integer, the number of occurrences of substring
         argument sub in the (sub)sequence given by [start:end].
         Optional arguments start and end are interpreted as in slice
         notation.
         HOWEVER, please note because python strings and Seq objects do a
         non-overlapping search, this may not give the answer you expect!!
         """
         return str(self).count(sub, start, end)

#####

########################################################################
class SeqRecord(object):

    """
    A SeqRecord object holds a sequence and information about it.

    Main attributes:
      - id          - Identifier such as a locus tag (string)
      - seq         - The sequence itself (Seq object or similar)

    Additional attributes:
      - description - Additional text (string)
    """

    def __init__( self, seq, id = "<unk_id>", description = "<unk_descr>"):
        """
        Create a SeqRecord.
        """
        if (id is not None) and (not isinstance(id, basestring)):
            # Lots of existing code uses id=None... this may be a bad idea.
            raise TypeError("id argument should be a string")
        #####
        if not isinstance(description, basestring):
            raise TypeError("description argument should be a string")
        #####

        # Storing the values
        self._seq        = seq
        self.id          = id
        self.description = description

        self.baseAttrSet = set(["A_bases", "C_bases", "G_bases", \
                                "T_bases", "N_bases", "X_bases", \
                                "GC_bases", "AT_bases", "basePairs"])
    
    def __getattr__(self, name):
        if (name in self.baseAttrSet) and (name not in self.__dict__):
            self.countBases()
            return self.__dict__[name]
        else:
            raise AttributeError("No attribute '%s' in SeqRecord" % name)
        
    def countBases(self):
        # Computing useful quantities
        self.A_bases   = self.seq.count('A') + self.seq.count('a')
        self.C_bases   = self.seq.count('C') + self.seq.count('c')
        self.G_bases   = self.seq.count('G') + self.seq.count('g')
        self.T_bases   = self.seq.count('T') + self.seq.count('t')
        self.N_bases   = self.seq.count('N') + self.seq.count('n')
        self.X_bases   = self.seq.count('X') + self.seq.count('x')
        self.LC_bases  = self.seq.count('a') + self.seq.count('t') + self.seq.count('c') + self.seq.count('g')
        self.AT_bases  = self.A_bases + self.T_bases
        self.GC_bases  = self.G_bases + self.C_bases
        self.basePairs = self.AT_bases + self.GC_bases

    def _set_seq(self, value):
        self._seq = value

    seq = property( fget = lambda self : self._seq, \
                    fset = _set_seq, \
                    doc  = "The sequence, as a Seq or MutableSeq object." )

    def __getitem__(self, index):
        """
        Returns a sub-sequence or an individual letter.
        """
        if isinstance(index, int):
            return self.seq[index]
        #####
        raise ValueError, "Invalid index"

    def __iter__(self):
        """
        Iterate over the letters in the sequence.
        """
        return iter( self.seq )

    def __str__(self):
        """
        A human readable summary of the record and its annotation (string).
        """
        lines = []
        if ( self.id ): lines.append("ID: %s" % self.id)
        if ( self.description ):
            lines.append("Description: %s" % self.description)
        #####
        lines.append( repr(self.seq) )
        return '\n'.join( lines )

    def __repr__(self):
        """
        A concise summary of the record for debugging (string).
        Note that long sequences are shown truncated. Also note that any
        annotations, letter_annotations and features are not shown (as they
        would lead to a very long string).
        """
        return self.__class__.__name__ + "(seq=%s, id=%s, description=%s)"% \
               tuple(map(repr,(self._seq, self.id, self.description)))

    def format(self):
        """
        Returns the record as a string in the specified file format.
        The format should be a lower case string supported as an output
        format by Bio.SeqIO, which is used to turn the SeqRecord into a
        string.  e.g.
        """
        return self.__format__()

    def __format__(self):
        """
        Returns the record as a string in the specified file format.
        This method supports the python format() function added in
        Python 2.6/3.0.  The format_spec should be a lower case string
        supported by Bio.SeqIO as an output file format. See also the
        SeqRecord's format() method.
        """
        handle = StringIO()
        writeFASTA( [self], handle )
        return handle.getvalue()

    def __len__(self):
        """
        Returns the length of the sequence.
        For example, using Bio.SeqIO to read in a FASTA nucleotide file:
        """
        return len(self.seq)

    def __add__(self, other):
        """
        Add another sequence or string to this sequence.

        The other sequence can be a SeqRecord object, a Seq object, or a plain
        Python string. If you add a plain string or a Seq (like) object,
        the new SeqRecord will simply have this appended to the existing
        data. However, any per letter annotation will be lost:

        """
        if not isinstance(other, SeqRecord):
            # Assume it is a string or a Seq.
            return SeqRecord( (self.seq + other), id = self.id, \
                             description = self.description )
        #####

        # Adding two SeqRecord objects... must merge annotation.
        answer = SeqRecord( self.seq + other.seq )

        # Take common id/name/description/annotation
        if self.id == other.id:
            answer.id = self.id
        #####

        if self.description == other.description:
            answer.description = self.description
        #####

        return answer

    def __radd__(self, other):
        """Add another sequence or string to this sequence (from the left).

        This method handles adding a Seq object (or similar, e.g. MutableSeq)
        or a plain Python string (on the left) to a SeqRecord (on the right).
        """
        if isinstance(other, SeqRecord):
            raise RuntimeError("This should have happened via the __add__ of "
                               "the other SeqRecord being added!")
        #####
        return SeqRecord( ''.join([other,self.seq]), \
                          id = self.id, \
                          description = self.description )

#####

########################################################################
def iterFASTA( fastaFileHandle, arachneReads=False ):
    """Generator function to iterate over Fasta records (as SeqRecord objects).

    Call:  FastaIterator( fastaFileHandle, arachneReads )

    fastaFileHandle - input file handle
    """

    # Splitting pattern
    subPattern = re.compile( r'[\n\r ]' )
    subLine    = subPattern.sub

    if isinstance( fastaFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = fastaFileHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record ' + \
                              'encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ):
            break
        #####
    #####
    
    if ( arachneReads ):
        buildRecord = lambda lines, scaffID, descr: SeqRecord( Seq( ''.join(lines) ),
                                                               id          = descr.strip(),
                                                               description = scaffold_id )
    else:
        buildRecord = lambda lines, scaffID, descr: SeqRecord( Seq( ''.join(lines) ),
                                                               id          = scaffold_id,
                                                               description = descr.strip() )
    #####

    # Reading the contents of the FASTA file
    while True:

        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should " +
                              "start with '>' character" )
        #####

        # Pulling the id
        splitText = line[1:].split(None,1)

        try:
            scaffold_id = splitText[0]
        except IndexError:
            print line
            raise ValueError( "iterFASTA has detected an empty scaffold ID." +
                              " Make sure all scaffolds have IDs." )
        #####

        # Everything else is the description
        try:
            descr = splitText[1]
        except IndexError:
            descr = ''
        #####

        # Reading the sequence
        lines = []
        line  = fastaFileHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or \
                 (line[0] == '>') ): break

            # Removes internal and trailing whitespace and floating '\r'
            lines.append( subLine( '', line) )

            # Reading the next line
            line = fastaFileHandle.readline()
        #####

        # Yield up the record and wait for the next iteration
        yield buildRecord( lines, scaffold_id, descr )

        # Stopping the iteration
        if ( not line ): return

    #####

    assert False, 'Should never reach this line!!'

#####

########################################################################
class FastaWriter_class( object ):

    """
    Class to write Fasta format files.
    """

    def __init__(self, handle, wrap):

        # Defining the regular expression for cleaning
        self.subPattern = re.compile( r'[\n\r]' )
        self.clean      = self.subPattern.sub

        # Getting the handle
        self.handle = handle

        self.wrap = None
        if wrap:
            if wrap < 1:
                raise ValueError
            #####
        #####
        self.wrap = wrap

    def write_record(self, record):
        id          = self.clean( '', record.id )
        description = self.clean( '', record.description )
        if ( description and (description.split(None,1)[0] == id) ):
            title = description
        elif description:
            title = "%s %s" % (id, description)
        else:
            title = id
        #####
        self.handle.write(">%s\n" % title)
        try:
            data = str( record.seq )
        except AttributeError:
            if record.seq is None:
                raise TypeError("SeqRecord (id=%s) has None for its sequence." \
                                % record.id)
            else:
                raise TypeError("SeqRecord (id=%s) has an invalid sequence." \
                                % record.id)
            #####
        #####
        if self.wrap:
            for i in range( 0, len(data), self.wrap ):
                self.handle.write( data[i:i+self.wrap] + '\n' )
            #####
        else:
            self.handle.write( data + '\n' )
        #####
        return

    def write_records(self, records):
        count = 0
        for record in records:
            self.write_record(record)
            count += 1
        #####
        return count

    def write_file(self, records):
        count = self.write_records(records)
        return count

#####

########################################################################
def writeFASTA( sequences, handle, wrap=80 ):
    """
    Write complete set of sequences to a file.

     - sequences - A list (or iterator) of SeqRecord objects.
     - handle    - File handle object to write to.
     - format    - lower case string describing the file format to write.

    You should close the handle after calling this function.

    Returns the number of records written (as an integer).
    """

    if isinstance( handle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    if isinstance( sequences, SeqRecord ):
        raise ValueError( "Use a SeqRecord list/iterator, not just " + \
                          "a single SeqRecord" )
    #####

    # Writing the file
    count = FastaWriter_class( handle, wrap ).write_file( sequences )

    assert isinstance(count, int), "Internal error - the FASTA " \
           "writer should have returned the record count, not %s" \
           % ( repr(count) )

    return count

#####

########################################################################
class FASTAFile_dict(dict):
    """
    Read only dictionary interface to a FASTA file.
    Note - duplicate keys (record identifiers by default) are not allowed.
    If this happens, a ValueError exception is raised.

    This dictionary is read only. You cannot add or change values,
    pop values, nor clear the dictionary.
    """

    def __init__(self, filename, arachneReadFormat=False):

        #Initialize as empty dictionary
        dict.__init__(self)
        # Opening the file
        if ( isGzipFile(filename) ):
            import gzip
            self._handle = gzip.open(filename,'r')
        elif ( isBzipFile(filename) ):
            import bz2
            self._handle = bz2.BZ2File(filename,'r')
        else:
            self._handle   = open(filename, 'r')
        #####
        
        self.marker_re = re.compile( '^>' )
        self.arachneReadFormat = arachneReadFormat
        self._build()

    def _build(self):
        keySet = set()
        offset = 0
        x = iterCounter(1000000)
        while True:
            line = self._handle.readline()
            if not line : break
            if ( self.marker_re.match(line) ):
                if ( self.arachneReadFormat ):
                    try:
                        key = line[1:].strip().split(None)[1]
                    except IndexError:
                        stderr.write( line )
                        continue
                    #####
                else:
                    key = line[1:].strip().split(None)[0]
                #####
                if ( key in keySet ):
                    raise ValueError('Repeated key %s\n'%key)
                #####
                keySet.add(key)
                x()
                dict.__setitem__(self, key, offset)
            #####
            offset = self._handle.tell()
        #####
        return

    def __repr__(self):
        return "FASTAFile_dict('%s')" % (self._handle.name)

    def __str__(self):
        if self:
            return "{%s : SeqRecord(...), ...}" % repr(self.keys()[0])
        else:
            return "{}"
        #####

    def values(self):
        for key in self.__iter__():
            yield self.__getitem__(key)
        #####

    def items(self):
        for key in self.__iter__():
            yield key, self.__getitem__(key)
        #####

    def iteritems(self):
        for key in self.__iter__():
            yield key, self.__getitem__(key)
        #####

    def __getitem__(self, key):
        self._handle.seek( dict.__getitem__(self, key) )
        record = iterFASTA( self._handle ).next()
        if ( self.arachneReadFormat ):
            tmp                = record.id
            record.id          = record.description.strip()
            record.description = tmp
        #####
        assert (record.id == key), "Requested key %s, found record.id %s" % \
                                                    (repr(key), repr(record.id))
        return record

    def get(self, k, d=None):
        try:
            return self.__getitem__(k)
        except KeyError:
            return d
        #####

    def __setitem__(self, key, value):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def update(self, **kwargs):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def pop(self, key, default=None):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def popitem(self):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def clear(self):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def fromkeys(self, keys, value=None):
        raise NotImplementedError("An indexed a sequence file doesn't "
                                  "support this.")

    def copy(self):
        raise NotImplementedError("An indexed a sequence file doesn't "
                                  "support this.")

#####

########################################################################
def indexFASTAFile( fastaFileHandle ):
    """
    indexFASTAFile( fastaFileHandle )

    Indexes a FASTA file by storing the scaffold name and location in a file as
    a dictionary.  Enables rapid access to FASTA files without having to loop
    through them to get to the sequence information.
    """
    # Parsing fasta file to get line positions
    stderr.write( 'Indexing %s\n'%fastaFileHandle.name )
    line         = fastaFileHandle.readline()
    indexedFASTA = {}
    while line:
        if ( line[0] == '>' ):
            parsedLine = line.split(None)
            indexedFASTA[parsedLine[0][1:]] = fastaFileHandle.tell()
        #####
        line = fastaFileHandle.readline()
    #####
    return indexedFASTA

########################################################################
def pullSequence( fastaHandle, filePos ):
    fastaHandle.seek( filePos )
    seq   = []
    while True:
        line = fastaHandle.readline()
        if ( not line ): break
        try:
            if ( line[0] == '>' ): break
        except IndexError:
            continue
        #####
        seq.append( line[:-1] )
    #####
    return ''.join(seq)

########################################################################
########################################################################
# QUAL FILE HANDLING ROUTINES
########################################################################
########################################################################

########################################################################
class QUAL( object ):

    """
    Read-only qual object.

    Like normal python list, the sequence object is immutable, but does
    allow Seq objects to be used as dictionary keys.

    The Seq object is designed to function as a container for qual 
    scores.
    
    Fundamentally, we handle qual scores as arrays of unsigned integers.
    array('H')
    
    """

    def __init__(self, data):
        """
        Create a Seq object.
        """
        # Strongly enforce typing:  string or unicode
        assert ( (type(data) == type([])) or \
                 (type(data) == type(array('H',[]))) )
        # Storing the data
        self._data        = array( 'H', [int(item) for item in data] )
        self.isQualObject = True
        self.typeSet      = set( [ type(0), type('') ] )

    def __repr__(self):
        """
        Returns a (truncated) representation of the qual scores for debugging.
        """
        if ( len(self._data) > 50 ):
            # Shows the last three scores similar to the FASTA representation.
            return "%s('%s...%s')" % ( self.__class__.__name__, \
                                       ' '.join( ['%d'%item for item in self._data[:54]] ), \
                                       ' '.join( ['%d'%item for item in self._data[-3:]] ) )
        else:
            return "%s(%s)" % ( self.__class__.__name__, repr(self._data) )
        #####

    def __str__(self):
        """
        Returns the representation.
        """
        return self.__repr__()

    def __len__(self):
        """
        Returns the length of the sequence, use len(my_qual).
        """
        return len( self._data )

    def __getitem__(self, index):
        """
        Returns the score of single index, use my_qual[index].
        """
        if isinstance(index, int):
            # Return a single score
            return self._data[index]
        else:
            # Return the (sub)set of scores as another QUAL object
            return QUAL( self._data[index] )
        ####

    def append(self, other):
        """
        Add another score to this qual set.
        """
        # Adding a value to the list
        if ( type(other) in self.typeSet ):
            self._data.append( int(other) )
        else:
            raise TypeError
        #####
    
    def extend(self, other):
        """
        Add a list of scores to this qual set.
        """
        if isinstance( other, QUAL ):
            self._data.extend( other._data )
        elif ( type(other) == type([]) ):
            self._data.extend( [int(item) for item in other] )
        #####

    def revComp(self):
        """
        Reverse complements the qual set
        """
        return QUAL( [item for item in reversed(self._data)] )

#####

########################################################################
class QUALRecord(object):

    """
    A QualRecord object holds a set of qual scores and information about 
    them.

    Main attributes:
      - id          - Identifier such as a locus tag (string)
      - quals       - String qual scores (Qual object or list of integers)

    Additional attributes:
      - description - Additional text (string)
    """

    def __init__( self, qual, id = "<unk_id>", description = "<unk_descr>"):
        """
        Create a QualRecord.
        """
        if (id is not None) and (not isinstance(id, basestring)):
            # Lots of existing code uses id=None... this may be a bad idea.
            raise TypeError("id argument should be a string")
        #####
        if not isinstance(description, basestring):
            raise TypeError("description argument should be a string")
        #####

        # Storing the values
        if ( type(qual) == type([]) ):
            self._qual = QUAL( [int(item) for item in qual] )
        elif ( isinstance(qual, QUAL)  ):
            self._qual = qual
        else:
            raise TypeError
        #####
        self.id          = id
        self.description = description

    def _set_qual(self, value):
        self._qual = value

    qual = property( fget = lambda self : self._qual, \
                     fset = _set_qual, \
                     doc  = "The scores, as a Qual or MutableQual object." )

    def __getitem__(self, index):
        """
        Returns a sub-score set or an individual score.
        """
        if isinstance(index, int):
            return self.qual[index]  # Return single score
        else:
            return QUAL( self.qual[index] )  # Return the whole range
        #####
        raise ValueError, "Invalid index"

    def __iter__(self):
        """
        Iterate over the scores in the set.
        """
        return iter( self.qual )

    def __str__(self):
        """
        A human readable summary of the record and its annotation.
        """
        lines = []
        lines.append("ID: %s" % self.id)
        lines.append("Description: %s" % self.description)
        lines.append( repr(self.qual) )
        return '\n'.join( lines )

    def __repr__(self):
        """
        A concise summary of the record for debugging.
        Note that long score sets are shown truncated. Also note that any
        annotations, letter_annotations and features are not shown (as they
        would lead to a very long string).
        """
        return self.__class__.__name__ + "(qual=%s, id=%s, description=%s)"% \
               tuple(map(repr,(self._qual, self.id, self.description)))

    def __len__(self):
        """
        Returns the length of the score set.
        """
        return len(self.qual)

    def __add__(self, other):
        """
        Add another score set or list of scores to this sequence.

        The other sequence can be a QUALRecord object, a QUAL object, or a plain
        Python list.
        """
        if ( type(other) == type([]) ):
            # Assume it is a list.
            tmpList = self.qual._data
            tmpList.extend([int(item) for item in other])
            return QUALRecord( QUAL(tmpList), \
                              id = self.id, \
                              description = self.description )
        #####

        # Adding two SeqRecord objects... must merge annotation.
        answer = QUALRecord( self.qual.extend(other.qual) )

        # Take common id/name/description/annotation
        if self.id == other.id:
            answer.id = self.id
        #####

        if self.description == other.description:
            answer.description = self.description
        #####

        return answer

#####

########################################################################
def iterQUAL( qualFileHandle, arachneReads=False ):
    """Generator function to iterate over QUAL file handles (as QUALRecord 
       objects).

    Call:  iterQUAL( qualFileHandle, arachneReads )

    qualFileHandle:  MUST BE A FILE HANDLE!!
    """

    # Splitting pattern
    subLine = re.compile( r'[\n\r]' ).sub

    if isinstance( qualFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = qualFileHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record ' + \
                              'encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ):
            break
        #####
    #####
    
    # Changing things if we are looking at arachne formatted reads
    if ( arachneReads ):
        buildRecord = lambda scores, scaffID, descr: QUALRecord( QUAL( scores ),
                                                                 id          = descr.strip(),
                                                                 description = scaffold_id )
    else:
        buildRecord = lambda scores, scaffID, descr: QUALRecord( QUAL( scores ),
                                                                 id          = scaffold_id,
                                                                 description = descr.strip() )
    #####

    # Reading the contents of the FASTA file
    while True:

        if ( line[0] != ">" ):
            raise ValueError( "Records in QUAL files should " +
                              "start with '>' character" )
        #####

        # Pulling the id
        splitText = line[1:].split(None,1)

        try:
            scaffold_id = splitText[0]
        except IndexError:
            raise ValueError( "iterQUAL has detected an empty scaffold ID." +
                              " Make sure all scaffolds have IDs." )
        #####

        # Everything else is the description
        try:
            descr = splitText[1]
        except IndexError:
            descr = ''
        #####

        # Reading the scores
        lines = []
        line  = qualFileHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or \
                 (line[0] == '>') ) : break

            # Removes internal and trailing whitespace and floating '\r'
            lines.extend( [int(item) for item in subLine( '', line).split(None)] )

            # Reading the next line
            line = qualFileHandle.readline()
        #####

        # Yield up the record and wait for the next iteration
        yield buildRecord( lines, scaffold_id, descr )

        # Stopping the iteration
        if ( not line ):
            return
        ####

    #####

    assert False, 'Should never reach this line!!'

#####

########################################################################
class QUALWriter_class( object ):

    """
    Class to write QUAL format files.
    """

    def __init__(self, handle, wrap):

        if isinstance( handle, basestring ):
            raise TypeError( "Need a file handle, not a string " + \
                             "(i.e. not a filename)" )
        #####
        
        # Defining the regular expression for cleaning
        self.clean = re.compile( r'[\n\r]' ).sub

        # Getting the handle
        self.handle = handle

        self.wrap = None
        if wrap:
            if wrap < 1: raise ValueError
        #####
        self.wrap = wrap

    def write_record(self, record):
        id          = self.clean( '', record.id )
        description = self.clean( '', record.description )
        if ( description and (description.split(None,1)[0] == id) ):
            title = description
        elif description:
            title = "%s %s" % (id, description)
        else:
            title = id
        #####
        self.handle.write(">%s\n" % title)
        try:
            data = [item for item in record.qual]
        except AttributeError:
            if ( record.qual is None ):
                raise TypeError("QUALRecord (id=%s) has None for its sequence." \
                                % record.id)
            else:
                raise TypeError("QUALRecord (id=%s) has an invalid sequence." \
                                % record.id)
            #####
        #####
        if self.wrap:
            for i in range( 0, len(data), self.wrap ):
                self.handle.write( ' '.join( ['%d'%item for item in data[i:i+self.wrap]] ) + '\n' )
            #####
        else:
            self.handle.write( ' '.join(['%d'%item for item in data]) + '\n' )
        #####
        return

    def write_records(self, records):
        count = 0
        for record in records:
            self.write_record(record)
            count += 1
        #####
        return count

    def write_file(self, records):
        count = self.write_records(records)
        return count

#####

########################################################################
def writeQUAL( scores, handle, wrap=50 ):
    """
    Write complete set of scores to a file.

     - scores - A list (or iterator) of QUALRecord objects.
     - handle - File handle object to write to.

    You should close the handle after calling this function.

    Returns the number of records written (as an integer).
    """

    if isinstance( handle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####

    if isinstance( scores, QUALRecord ):
        raise ValueError( "Use a QUALRecord list/iterator, not just " + \
                          "a single QUALRecord" )
    #####

    # Writing the file
    count = QUALWriter_class( handle, wrap ).write_file( scores )

    assert isinstance(count, int), "Internal error - the QUAL " \
           "writer should have returned the record count, not %s" \
           % ( repr(count) )

    return count

#####

########################################################################
class QUALFile_dict(dict):
    """
    Read only dictionary interface to a QUAL file.
    Note - duplicate keys (record identifiers by default) are not allowed.
    If this happens, a ValueError exception is raised.

    This dictionary is read only. You cannot add or change values,
    pop values, nor clear the dictionary.
    """

    def __init__(self, filename, arachneReadFormat=False):

        #Initialize as empty dictionary
        dict.__init__(self)
        # Opening the file
        if ( isGzipFile(filename) ):
            import gzip
            self._handle = gzip.open(filename,'r')
        elif ( isBzipFile(filename) ):
            import bz2
            self._handle = bz2.BZ2File(filename,'r')
        else:
            self._handle   = open(filename, 'r')
        #####
        self.marker_re = re.compile( '^>' )
        self.arachneReadFormat = arachneReadFormat
        self._build()

    def _build(self):
        keySet = set()
        offset = 0
        x = iterCounter(1000000)
        while True:
            line = self._handle.readline()
            if not line : break
            if ( self.marker_re.match(line) ):
                if ( self.arachneReadFormat ):
                    try:
                        key = line[1:].strip().split(None)[1]
                    except IndexError:
                        stderr.write( line )
                        continue
                    #####
                else:
                    key = line[1:].strip().split(None)[0]
                #####
                if ( key in keySet ):
                    raise ValueError('Repeated key %s\n'%key)
                #####
                keySet.add(key)
                dict.__setitem__(self, key, offset)
                x()
            #####
            offset = self._handle.tell()
        #####
        return

    def __repr__(self):
        return "QUALFile_dict(file='%s')" % (self._handle.name)

    def __str__(self):
        if self:
            return "{%s : QUALRecord(...), ...}" % repr(self.keys()[0])
        else:
            return "{}"
        #####

    def values(self):
        for key in self.__iter__():
            yield self.__getitem__(key)
        #####

    def items(self):
        for key in self.__iter__():
            yield key, self.__getitem__(key)
        #####

    def iteritems(self):
        for key in self.__iter__():
            yield key, self.__getitem__(key)
        #####

    def __getitem__(self, key):
        self._handle.seek( dict.__getitem__(self, key) )
        record = iterQUAL( self._handle ).next()
        if ( self.arachneReadFormat ):
            tmp                = record.id
            record.id          = record.description.strip()
            record.description = tmp
        #####
        assert (record.id == key), "Requested key %s, found record.id %s" % \
                                                    (repr(key), repr(record.id))
        return record

    def get(self, k, d=None):
        try:
            return self.__getitem__(k)
        except KeyError:
            return d
        #####

    def __setitem__(self, key, value):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def update(self, **kwargs):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def pop(self, key, default=None):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def popitem(self):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def clear(self):
        raise NotImplementedError("An indexed a sequence file is read only.")

    def fromkeys(self, keys, value=None):
        raise NotImplementedError("An indexed a sequence file doesn't "
                                  "support this.")

    def copy(self):
        raise NotImplementedError("An indexed a sequence file doesn't "
                                  "support this.")

#####

########################################################################
def indexQUALFile( qualFileHandle ):
    """
    indexQUALFile( qualFileHandle )

    Indexes a QUAL file by storing the scaffold name and location in a file as
    a dictionary.  Enables rapid access to QUAL files without having to loop
    through them to get to the sequence information.
    """
    # Parsing fasta file to get line positions
    stderr.write( 'Indexing %s\n'%qualFileHandle.name )
    line         = qualFileHandle.readline()
    indexedQUAL = {}
    while line:
        if ( line[0] == '>' ):
            parsedLine = line.split(None)
            indexedQUAL[parsedLine[0][1:]] = qualFileHandle.tell()
        #####
        line = qualFileHandle.readline()
    #####
    return indexedQUAL

########################################################################
def pullScores( qualHandle, filePos ):
    qualHandle.seek( filePos )
    scores = []
    while True:
        line = qualHandle.readline()
        if ( not line ): break
        try:
            if ( line[0] == '>' ): break
        except IndexError:
            continue
        #####
        scores.append( line[:-1] )
    #####
    return scores





