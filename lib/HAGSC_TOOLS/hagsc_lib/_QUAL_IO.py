
__author__="mqualls"
__date__ ="$Oct 8, 2010 1:22:29 PM$"

"""Provides a dictionary interface to FASTA and Qual files.
fasta_file(handle) and qual_file(handle) return python dictionary interfaces to the underlying files
(except that writing to the dictionary is not supported)"""

from time import time

from sys import argv
from sys import stdout

import re

########################################################################

rx_find_fast = re.compile(r"(?m)^>.*$")
rx_find_fast_finditer = rx_find_fast.finditer

rx_parse_fast = re.compile(r"(?sm)^>(?P<id>[\041-\176]*)([ \t]+(?P<info>[\040-\176\t]+))?")#\s*(?P<data>.*?)((?=^>)|\Z)")
rx_parse_fast_match = rx_parse_fast.match

def scan_fastlike_rx(input, index=0):
    """Accepts a string and optional start index
    Yields the index of the > starting the next FASTA-like record"""
    for m in rx_find_fast_finditer(input, index):
        yield m.start(), m.group()

def parse_fastlike_rx(input):
    """Accepts a string containing a FASTA-like record
    Returns a tuple of the record id and description"""
    m = rx_parse_fast_match(input)
    return m and (m.group("id") or "", m.group("info") or "") or ("", "")

rx_parse_arachne = re.compile(r"^>((?P<info>[\w|]*)\s+)?(?P<A>\w+)[-](?P<B>\w+)[.](?P<C>\w+)[-](?P<D>[FRfr])")
rx_parse_arachne_match = rx_parse_arachne.match
def parse_arachne_rx(input):
    m = rx_parse_arachne_match(input)
    return (m.group("A"), m.group("B"), m.group("C"), m.group("D")), m.group("info") or ""

def extract_fastlike_string(input, header):
    """Accepts an input string and a FAST-like header from iterate_fastlike
    Returns the associated data payload"""

    return input[header[2][0]:header[2][1]]

#############################################################

def scan_fastlike_py(input, index=None):
    """Accepts an open file handle and optional start index
    Returns the index of the > starting the next FASTA-like record"""
    if index is not None: input.seek(index)
    while True:
        pos, line = input.tell(), input.readline()
        try:
            if line[0] == ">": yield (pos, line)
        except:
            break

def parse_fastlike_py(input):
    """Accepts a string containing a FASTA-like record
    Returns a tuple of the record id and description"""
    try:
        split = input.split(maxsplit=1)
        id, info = split[0][1:], split[1]
    except:
        id, info = input[1:], ""
    return (id.strip(), info.strip())

def parse_fastlike_py_r(input):
    """Accepts a string containing a FASTA-like record
    Returns a tuple of the record id and description"""
    try:
        split = input.rsplit(maxsplit=1)
        info, id = split[0][1:], split[1]
    except:
        id, info = input[1:], ""
    return (id.strip(), info.strip())

def extract_fastlike_file(input, header):
    """Accepts an open file handle and a FAST-like header from iterate_fastlike
    Returns the associated data payload"""
    save = input.tell()
    input.seek(header[2][0])
    result = input.read(header[2][1]-header[2][0])
    input.seek(save)
    return result

#########################################################

scan_fastlike = scan_fastlike_py
parse_fastlike = parse_fastlike_rx
parse_fastlike_r = parse_fastlike_py_r
parse_arachne = parse_arachne_rx
extract_fastlike = extract_fastlike_file

def lookahead_iterator(it):
    ahead, behind = None, None
    for x in it:
        behind, ahead = ahead, x
        if behind is not None:
            yield behind, ahead
    yield ahead, None

def iterate_fastlike(input, parser=parse_fastlike):
    """Accepts a string expected to contain FASTA-like data
    Yields a tuple of the record id, description, and a tuple of start and end indices of the data payload"""
    for behind, ahead in lookahead_iterator(scan_fastlike(input)):
        if not behind: break
        loc, record = behind
        aloc, arecord = ahead or (-1, None)
        id, info = parser(record)
        yield (id, info, (loc+len(record), aloc))

####################################################################

class objectify(object):
    def __init__(self, source):
        object.__setattr__(self, "_source", source)
        object.__setattr__(self, "__getitem__", source.__getitem__)
    
    def __repr__(self):
        return self._source

    def __getattr__(self, key):
        if key in self._source:
            return self.__getitem__(key)
        else:
            try:
                return self._source.__dict__.__getitem__(key)
            except AttributeError:
                raise KeyError(key)
            
    def __setattr__(self, key, value):
        self._source.__setitem__(key, value)

####################################################################

class dict_fastlike( dict ):
    """Dictionary built by indexing a string expected to contain FASTA-like records
    Reads from the input are lazy, done as needed when told to iterate or asked for a key"""
    def __init__(self, input, filter=None, parser=None, iter_only=False):
        self._input = input
        self._filter = filter or (lambda x: x)
        self._source = iterate_fastlike(input, parser or parse_fastlike)
        self._store = not iter_only

    def _build_iter(self):
        """Iterates over one record in the input and adds its information to the dictionary."""
        for record in self._source:
            if dict.__contains__(self, record[0]): raise ValueError("Duplicate key: %s" % record[0])
            if self._store: dict.__setitem__(self, record[0], record)
            yield record

    def __getitem__(self, key):
        record = dict.__getitem__(self, key)
        return self._build_return(record)

    def _build_return(self, record):
        dictionary = self._filter({"info":record[1], "data":(lambda : extract_fastlike(self._input, record)), "id":record[0]})
        return objectify(dictionary)
        
    def __contains__(self, key):
        """Checks the dictionary first, then iterates through the input looking for the key
        (updating the dictionary along the way)."""
        if not self._store: return False
        if dict.__contains__(self, key): return True
        for record in self._build_iter():
            if record[0] == key: return True
        return False

    def __missing__(self, key):
        if self.__contains__(key): return dict.__getitem__(self, key)
        raise KeyError("Key not found: %s"% key)

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default

    def index(self):
        """Preload all indices from the input."""
        for i in self._build_iter():
            pass

    def iterkeys(self):
        """First iterates through known keys, then the rest of the input."""
        for k in dict.iterkeys(self):
            yield k
        for record in self._build_iter():
            yield record[0]

    def iteritems(self):
        for k in dict.iterkeys(self):
            yield k, self.__getitem__(k)
        for record in self._build_iter():
            yield record[0], self._build_return(record)

    def itervalues(self):
        for k in dict.iterkeys(self):
            yield self.__getitem__(k)
        for record in self._build_iter():
            yield self._build_return(record)
            
    def keys(self):
        return [k for k in self.iterkeys()]

    def items(self):
        return [i for i in self.iteritems()]

    def values(self):
        return [v for v in self.itervalues()]

    def __repr__(self):
        return "{%s}" % ','.join(["%s : %s" % (k, v) for (k, v) in self.iteritems()])

    def __str__(self):
        return self.__repr__()

    def __setitem__(self, key, value):
        raise NotImplementedError("Writing to an input-backed dictionary is not supported.")

####################################################################

rx_dict_parse_qual = re.compile(r"(?sm)\s*(?P<data>[0-9]+)")
rx_dict_parse_qual_finditer = rx_dict_parse_qual.finditer
rx_dict_parse_qual_match = rx_dict_parse_qual.match
def dict_parse_qual_rx(input):
    quals = []
    for m in rx_dict_parse_qual_finditer(input):
        quals.append(int(m.group("data")))
    return quals

def dict_parse_qual_py(input):
    #return [int(x) for x in input.split()]
    return map( int, input.split() )

def dict_filter_qual(record):
    return { "id":record["id"], "info":record["info"], "data":(lambda : dict_parse_qual_py(record["data"]())) }

rx_dict_parse_fasta = re.compile(r"(?m)\s*(?P<data>[A-Za-z*-]+)")
def dict_parse_fasta_rx(input):
    return ''.join([m.group("data") for m in rx_dict_parse_fasta.finditer(input)])

def dict_parse_fasta_py(input):
    return ''.join(input.split())

def dict_filter_fasta(record):
    return { "id":record["id"], "info":record["info"], "data":(lambda : dict_parse_fasta_py(record["data"]())) }

def fastlike_file(file, filter=None, parser=None, iter_only=False):
    #map = mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
    #return dict_fastlike(map, filter, parser)
    return dict_fastlike(file, filter, parser, iter_only)

def qual_file(file, parser=None, iter_only=False):
    """Accepts an open file handle for a qual file, and optionally a function for parsing header lines
    Returns a dictionary interface to the qual file
    Returned records are a dictionary consisting of 'id' (string), 'info' (string), and 'data' (list of int)"""

    return fastlike_file(file, dict_filter_qual, parser, iter_only)

def fasta_file(file, parser=None, iter_only=False):
    """Accepts an open file handle for a FASTA file, and optionally a function for parsing header lines
    Returns a dictionary interface to the FASTA file
    Returned records are a dictionary consisting of 'id', 'info', and 'data'"""

    return fastlike_file(file, dict_filter_fasta, parser, iter_only)

##################################################################

def write_fasta(id, comment, data, handle, data_writer = (lambda h,d: h.write(d())) ):
    """Writes a single FASTA record (consisting of id, comment, and data as a lambda) to an open file handle"""
    handle.write(">%s %s\n" % (id, comment))
    try:
        data_writer(handle, data)
    except:
        print handle, data, data()
    handle.write("\n")

def qual_writer(handle, data):
    wrap = 30
    record = data()
    for i in xrange( 0, len(record), wrap ):
        handle.write( '%s\n'%( ' '.join( ['%d'%item for item in record[i:i+wrap]]) ) )
    #####
    return
                
def write_qual(id, comment, data, handle):
    """Writes a single Qual record (consisting of id, comment, and data as a lambda returning a list of integers) to an open file handle"""
    write_fasta(id, comment, data, handle, qual_writer)
                
def fasta_record_writer(write, handle, record):
    write(record._source["id"], record._source["info"], record._source["data"], handle)
    
def write_fasta_record(record, handle):
    fasta_record_writer(write_fasta, handle, record)
    
def write_qual_record(record, handle):
    fasta_record_writer(write_qual, handle, record)

def write_fasta_collection(collection, handle, record_writer=fasta_record_writer, data_writer=write_fasta):
    """Writes a fastlike_dict to an open file handle"""
    for record in collection:
        record_writer(data_writer, handle, record)

def write_qual_collection(collection, handle):
    """Writes a fastlike_dict of Qual records to an open file handle"""
    write_fasta_collection(collection, handle, data_writer=write_qual)

##################################################################

def real_main():
    argv = arg[1:]
    if len(argv) < 2:
        print "Usage: python qual.py [FASTA filename] [QUAL filename] <forward|reverse|arachne>"
        return

    reverse = len(argv) > 2 and argv[2] or ""
    if reverse == "reverse":
        parser = parse_fastlike_r
    elif reverse == "arachne":
        parser = parse_arachne
    else:
        parser = parse_fastlike

    start = time()

    fasta_handle = open(argv[0], "r")
    qual_handle  = open(argv[1], "r")
    fasta        = fasta_file(fasta_handle, parser)
    qual         = qual_file(qual_handle, parser)
                
    output = open("foo", "w")
    write_qual_collection([qual[k] for k,v in fasta.iteritems() if v.info.find("2 ends") > -1], output)

    qual_record = qual['QMM']
    write_qual_collection( [qual_record], output)
    
    print time() - start


def profile_main():
    from cProfile import Profile
    from pstats import Stats
    prof  = Profile().runctx("real_main()", globals(), locals())
    stats = Stats( prof ).sort_stats("time").print_stats(60)
    return

arg = argv
if __name__ == "__main__":
    profile_main()
    #real_main()