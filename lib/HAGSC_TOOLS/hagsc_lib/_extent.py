
def merge_extents(extents):
    if len(extents) == 0: return
    extents = extents[:]
    extents.sort(key=(lambda x : x[0]))
    working = extents.pop(0)
    for extent in extents:
        if extent[0] < working[1]:
            if extent[1] > working[1]: working = (working[0], extent[1])
        else:
            yield working
            working = extent
    yield working

def extent_size(extent):
    return extent[1] - extent[0]
        
def test_overlap(extent1, extent2):
    if extent2[0] < extent1[0]: extent1, extent2 = extent2, extent1
    return extent2[0] < extent1[1]
    
def place_overlap(exemplar, collection):
    return [extent for extent in collection if test_overlap(exemplar, extent)]
    
def test_containment(larger, smaller):
    return smaller[0] >= larger[0] and smaller[1] < larger[1]
    
def extent_margin(inside, outside):
    post = outside[1] - inside[1]
    pre = inside[0] - outside[0]
    if pre < 0: pre = 0
    if post < 0: post = 0
    return pre+post
    
