
from itertools import izip
from itertools import ifilter
from itertools import count

from _helperFunctions import try_index
from _helperFunctions import try_call
from _helperFunctions import get_or_set

from sys import stderr

import re


def csv_record(line, separator=','):
    if callable(separator):
        return [field.strip() for field in separator(line)]
    else:
        return [field.strip() for field in line.split(separator)]
    
    
def csv_offset_table(header):
    table = {}
    for offset, field in izip(count(0), header):
        table[field] = offset
    return table
    
    
def csv_fields(iterator, fields, typecasts, separator=',', format=None):
    if format is None:
        try:
            table = csv_offset_table(csv_record(iterator.next(), separator))
        except:
            return
    else:
        table = csv_offset_table(format)
        
    for line in iterator:
        record = csv_record(line, separator)
        try:
            yield [typecast(try_index(record, table[field])) for field, typecast in izip(fields, typecasts)]
        except:
            pass


rx_gff_directive = re.compile(r"##(?P<directive>[^\s]*)(\s+(?P<arguments>.*))?")
def gff_process_directive(text, handler):
    match = rx_gff_directive.match(text)
    if match:
        directive = match.group("directive")
        if directive:
            arguments = match.group("arguments") or ""
            handler[directive] = [x for x in arguments.split() if x]
            if directive == "FASTA": raise StopIteration
    return False
    

def gff_parse_attributes(text):
    attributes = {}
    kv_pairs = [x for x in text.split(';') if x]
    for pair in kv_pairs:
        try:
            key, value_text = pair.split('=')
            values = [v.strip() for v in value_text.split(',') if v]
            attributes[key.strip()] = values
        except:
            pass
    return attributes
    
            
def gff_is_comment(text):
    if text.find('>',0,1) == 0: raise StopIteration
    return text.find('#',0,1) == 0
       

gff_header = ["seqid", "source", "type", "start", "end", "score", "strand", "phase", "attributes"]
gff_types = [str, str, str, lambda x: int(x)-1, lambda x: int(x)-1, (lambda x: try_call(float,0.0,x)), (lambda x: x=='-' and -1 or 1), (lambda x: try_call(int,0,x)), gff_parse_attributes]
        
          
def gff_fields(iterator, directives=None):
    if directives is None: directives = {}
    filtered_comments = ifilter(lambda x: not(gff_is_comment(x)) or gff_process_directive(x, directives), iterator)
    
    for tuples in csv_fields(filtered_comments, gff_header, gff_types, '\t', gff_header):
        yield tuples


def gff_build_tree(id_entries, entries):
    for entry in entries:
        for parent_id in entry.get("Parent", []):
            parent = id_entries.setdefault(parent_id, {})
            children = parent.setdefault("Child", [])
            children.append(entry)
    return [x for x in id_entries.itervalues() if "Parent" not in x]
         
         
def assign(a, k, b):
    a[k] = b

    
def gff_structures(iterator, metadata={}):
    items = []
    id_items = {}
    yield_flag = [False]
    
    directives = callback_dict()
    directives.watch["#"] = (lambda d,k,v: (assign(yield_flag, 0, True) and False))
    directives.watch_all = (lambda d,k,v: (assign(metadata, k, v) and False))
    
    for record in gff_fields(iterator, directives):
        if yield_flag[0]:
            for root in gff_build_tree(id_items, items):
                yield root
            items = []
            id_items = {}
            yield_flag = [False]
        entry = {}
        entry.update(izip(gff_header, record))
        entry.update(entry["attributes"])
        del entry["attributes"]
        items.append(entry)
        try:
            id_items[entry["ID"][0]] = entry
        except:
            pass
    for root in gff_build_tree(id_items, items):
        yield root
        

class callback_dict(dict):
    watch = {}
    watch_all = None
    
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.watch = {}
        self.watch_all = None
        
    def __setitem__(self, key, value):
        if key in self.watch:
            if self.watch[key](self, key, value):
                dict.__setitem__(self, key, value)
        elif self.watch_all:
            if self.watch_all(self, key, value):
                dict.__setitem__(self, key, value)
        else:
            dict.__setitem__(self, key, value)
            
def print_gff_tree(gff, depth=0, sentinel=None):
    if sentinel is None: sentinel = set()
    fingerprint = " ".join([str(x) for x in [gff["type"], gff["seqid"], gff["start"], gff["end"], gff.get("ID", "NoID")]])
    print depth*"\t" + fingerprint + (fingerprint in sentinel and "OOO" or "")
    if fingerprint not in sentinel:
        sentinel.add(fingerprint)
        for child in gff.get("Child", []):
            print_gff_tree(child, depth+1, sentinel)
            
            