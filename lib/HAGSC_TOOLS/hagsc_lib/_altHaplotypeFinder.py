__author__="jjenkins"
__date__ ="$Apr 29, 2010 10:29:06 AM$"

from _globalSettings import mask_repeats_hash_path

from _helperClasses import histogramClass

from _helperFunctions import generateTmpDirName
from _helperFunctions import testDirectory
from _helperFunctions import baseFileName
from _helperFunctions import iterTextFile
from _helperFunctions import deleteFile
from _helperFunctions import deleteDir
from _helperFunctions import testFile

from _jobQueueing import remoteServer4

from _FASTA_IO import FASTAFile_dict
from _FASTA_IO import writeFASTA
from _FASTA_IO import iterFASTA

from _bestHit import DNA_best_hit
from _bestHit import iterBestHit

from os.path import abspath
from os.path import join

from sys import stdout

from os import curdir
from os import mkdir

########################################################################
class alternativeHaplotypeFinder(object):

    def __init__(self, assemblyFile, \
                       cutoffSize, \
                       keyWords=None, \
                       queueSize=16, \
                       clusterNum=101, \
                       covCutoff=95.0, \
                       ID_cutoff=95.0 ):

        # Initial tests
        testFile( assemblyFile )

        # Inputs
        self.assemblyFile = assemblyFile
        self.cutoffSize   = cutoffSize
        self.blatKeyWords = keyWords
        self.queueSize    = queueSize
        self.clusterNum   = clusterNum
        self.covCutoff    = covCutoff
        self.ID_cutoff    = ID_cutoff

        # Splitting the fileNames
        pre = baseFileName( self.assemblyFile )

        # FileNames
        self.bestHitsOutputFile = '%s_altHap_bestHits.out'%pre
        self.smallFileName      = '%s_small.fasta'%pre
        self.largeFileName      = '%s_large.fasta'%pre
        self.altHapOutputFile   = '%s_altHaps.dat'%pre

        # Echo the options to the user
        self.makeOptionsString()
        stdout.write( ''.join(self.optionsList) )

    def makeOptionsString(self):
        self.optionsList    = 10 * ['']
        self.optionsList[0] = '\nSettings:\n'
        self.optionsList[1] = '---------\n'
        self.optionsList[2] = '\t-ASSEMBLY             = %s\n'%self.assemblyFile
        self.optionsList[3] = '\t-CUTOFF_SIZE          = %d\n'%self.cutoffSize
        self.optionsList[4] = '\t-QUEUE SIZE           = %d\n'%self.queueSize
        if ( type(self.clusterNum) == type('') ):
            self.optionsList[5] = '\t-CLUSTER NUMBER       = %s\n'%self.clusterNum
        elif ( type(self.clusterNum) == type(0) ):
            self.optionsList[5] = '\t-CLUSTER NUMBER       = %d\n'%self.clusterNum
        #####
        self.optionsList[6] = '\t-COVERAGE CUTOFF      = %.2f%s\n'% \
                                                           (self.covCutoff,r'%')
        self.optionsList[7] = '\t-IDENTITY CUTOFF      = %.2f%s\n'% \
                                                           (self.ID_cutoff,r'%')
        self.optionsList[8] = '\t-BEST HIT OUTPUT FILE = %s\n'%self.bestHitsOutputFile
        self.optionsList[9] = '\t-ALT HAP OUTPUT FILE  = %s\n'%self.altHapOutputFile
        return

    def scaffLengthDist(self, tmpCutoff, nBins):
        # Determine the scaffold length distribution
        lengthDistribution = histogramClass(0, tmpCutoff, nBins)

        for record in iterFASTA( open( self.assemblyFile, 'r') ):
            stdout.write( '%s\n'%record.id )
            tmpLen = len(record.seq)
            if ( tmpLen <= tmpCutoff ): lengthDistribution.addData( tmpLen )
        #####
        stdout.write( lengthDistribution.generateOutputString() )
        tmpHandle = open( 'sizeDist.tmp','w')
        tmpHandle.write( lengthDistribution.generateOutputString() )
        tmpHandle.close()
        return

    def findAltHapScaffolds(self):

        # (1) Segregate the files
        self.segregateScaffolds()

        # (2) Run BestHit
        self.runBestHit()

        # (3) Find Alternative Haplotypes
        self.findAltHaps()

    def segregateScaffolds(self):
        # Segregation of scaffolds
        smallFileHandle = open( self.smallFileName, 'w' )
        largeFileHandle = open( self.largeFileName, 'w' )
        for record in iterFASTA( open(self.assemblyFile,'r') ):
            if ( len(record.seq) > self.cutoffSize ):
                writeFASTA( [record], largeFileHandle )
                print '%s: large'%record.id
            else:
                writeFASTA( [record], smallFileHandle )
                print '%s: small'%record.id
            #####
        #####
        smallFileHandle.close()
        largeFileHandle.close()
        return

    def runBestHit(self):
        # Finding the small scaffolds in the large
        DNA_best_hit( self.smallFileName, \
                      self.largeFileName, \
                      nFastaFiles    = self.queueSize, \
                      clusterNum     = self.clusterNum, \
                      outputFileName = self.bestHitsOutputFile, \
                      keyWords       = self.blatKeyWords )
        return

    def findAltHaps(self):
        finalFileHandle = open( self.altHapOutputFile, 'w' )
        for record in iterBestHit( open(self.bestHitsOutputFile,'r') ):
            if ( (record.per_ID       > self.ID_cutoff) and \
                 (record.per_coverage > self.covCutoff) ):
                finalFileHandle.write( record.generateString() )
            #####
        #####
        finalFileHandle.close()
        return

################################################################################
class scaffold_altHapFinder( object ):

    def __init__(self):

        self.length    = 0
        self.num_bases = 0

        self.t1 = 0.0
        self.t2 = 0.0
        self.t3 = 0.0
        self.t4 = 0.0

        self.GC = 0
        self.AT = 0
#####

################################################################################
class kmer_altHapFinder(object):
    """

    repMasker_call( FASTA_File, mer = 24 )

    """

    def __init__( self, FASTA_File, \
                        joinFile, \
                        mer = 24, \
                        clusterNum = 101 ):

        # Does the file exist?
        testFile( FASTA_File )
        testFile( joinFile )

        # Incoming variables
        self.fastaFile     = FASTA_File
        self.joinFile      = joinFile
        self.mer           = mer
        self.clusterNum    = clusterNum

        # Opening the FASTA file
        self.indexedFile = FASTAFile_dict( self.fastaFile )

        # Setting up the temporary directory
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Initializing the scaffold Ordering
        self.scaffOrder = []

        # Creating the output Files
        self.baseFileName = baseFileName(self.fastaFile)
        self.maskRepeatsHashPath = mask_repeats_hash_path
        self.baseOutFileName = join( self.tmpPath, self.baseFileName )
        self.perlFile = join( self.basePath, 'kmer_alt_hap.pl' )

        # Main output file names
        self.rawOutputFile = join( self.basePath, \
                                   '%s_rawData.out'%self.baseFileName )
        self.rawFileHandle = open( self.rawOutputFile, 'w' )
        self.rawFileHandle.write('maksee\tmakser\tlength\tnumBases\tGC%\tt1%\n')
        self.rawFileHandle.write('------\t------\t------\t--------\t---\t---\n')

        # Initializing other variables
        self.z_option = 0
        self.AT_bases = 0
        self.GC_bases = 0
        self.N_bases  = 0

        # Initializing other variables
        self.indexDict = {}

    def perform_repMasker(self):

        # (1) Index the file to a dictionary
        self.create_tmp_dir()

        # (2) Read in the join file
        self.readJoinFile()

        # (3) Gather sequence statistics
        self.countFASTABasePairs()

        # (4) Mask sequence
        self.maskingSequence()

        # (5)  Taking out the trash
        self.clean_tmp()

        return

    def create_tmp_dir(self):
        stdout.write( '\t-Creating tmp directory\n')
        deleteDir( self.tmpPath )
        mkdir( self.tmpDir, 0777 )
        testDirectory( self.tmpDir )
        return

    def readJoinFile(self):
        stdout.write( '\t-Reading Join File\n')
        for record in iterTextFile( open(self.joinFile,'r'), ' 10000 ' ):
            self.scaffOrder.append( [] )
            for scaffIndex in record:
                self.scaffOrder[-1].append( ( (scaffIndex[:2]=='rc') and \
                                              ['super_%s'%scaffIndex[2:]] or \
                                              ['super_%s'%scaffIndex] \
                                             )[0] )
            #####
        #####
        return

    def generateAltHapClasses(self):
        stdout.write( '\t-Indexing FASTA File\n' )
        for scaffID in self.indexedFile.keys():
            self.indexDict[scaffID] = scaffold_altHapFinder()
        #####
        return

    def countFASTABasePairs(self):
        stdout.write( '\t-Counting FASTA Base Pairs\n' )
        # Initialize the
        self.generateAltHapClasses()
        n_mers = 0
        for scaffID, scaffClass in self.indexDict.items():
            # Store base counting information
            record = self.indexedFile[scaffID]
            self.indexDict[scaffID].AT = record.AT_bases
            self.indexDict[scaffID].GC = record.GC_bases
            self.indexDict[scaffID].num_bases = record.AT_bases + record.GC_bases
            self.indexDict[scaffID].length = record.AT_bases + record.GC_bases + \
                                             record.N_bases
            n_mers += self.indexDict[scaffID].num_bases
        #####

        # Determining the amount of memory to allocate (-z option)
        self.z_option = min( 1800, max( int(n_mers*2/1000000), 2 ) )
        return

    def maskingSequence(self):
        for scaffList in self.scaffOrder:
            for n in xrange( ( len(scaffList) - 2 ) ):
                # Getting the names
                scaff_n   = scaffList[n]
                scaff_np1 = scaffList[n+1]
                scaff_np2 = scaffList[n+2]
                # Analyze the scaffolds
                self.analyzeScaffoldPair( scaff_n, scaff_np1 )
                self.analyzeScaffoldPair( scaff_n, scaff_np2 )
            #####
        #####
        # Closing the FASTA File
        self.fastaHandle.close()
        self.rawFileHandle.close()
        return

    def analyzeScaffoldPair(self, scaff_1, scaff_2):
        # Write scaffolds to individual temp files
        scaffList = [scaff_1, scaff_2]
        fileNames = {}
        fileNames[scaff_1] = join(self.tmpPath,'%s_tmp.fasta'%scaff_1)
        fileNames[scaff_2] = join(self.tmpPath,'%s_tmp.fasta'%scaff_2)
        for scaffID in scaffList:
            record = self.indexedFile[scaffID]
            tmpFASTAFile = fileNames[scaffID]
            deleteFile(tmpFASTAFile)
            writeFASTA( [record], open( tmpFASTAFile, 'w' ) )
        #####

        # Generate Command List
        commandList  = []
        commandString = 5 * ['']
        commandString[0] = self.maskRepeatsHashPath
        commandString[1] = '-t 1'
        commandString[2] = '-m %d'%self.mer
        commandString[3] = '-z %dm'%self.z_option
        # First command
        commandString[4] = '-H %s %s'%(fileNames[scaff_1],fileNames[scaff_2])
        commandList.append( ' '.join(commandString) )
        # Second command
        commandString[4] = '-H %s %s'%(fileNames[scaff_2],fileNames[scaff_1])
        commandList.append( ' '.join(commandString) )
        # Run jobs
        remoteServer4( commandList, 4, clusterNum=self.clusterNum, \
                       perlFile=self.perlFile )

        # Count masked bases
        for scaffID in scaffList:

            # Reading in the sequence
            outputFileName = '%s.kmermasked'%fileNames[scaffID]
            record = iterFASTA( open(outputFileName, 'r') ).next()

            # Counting bases
            maskedBases    = float( record.AT_bases + record.GC_bases )
            unmaskedBases  = float( self.indexDict[scaffID].num_bases )
            unique_percent = 100.0 * maskedBases / unmaskedBases

            # Recording the total
            self.indexDict[scaffID].t1 = unique_percent

            # Writing the result to a file
            scaffClass = self.indexDict[scaffID]
            GC_per     = 100.0 * float(scaffClass.GC) / float(scaffClass.num_bases)
            if ( scaffID == scaff_1 ):
                outputString = '%s\t%s\t%d\t%d\t%.2f\t%.2f\n'%(scaff_1, \
                                                               scaff_2, \
                                                               scaffClass.length, \
                                                               scaffClass.num_bases, \
                                                               GC_per, \
                                                               scaffClass.t1 )
            else:
                outputString = '%s\t%s\t%d\t%d\t%.2f\t%.2f\n'%(scaff_2, \
                                                               scaff_1, \
                                                               scaffClass.length, \
                                                               scaffClass.num_bases, \
                                                               GC_per, \
                                                               scaffClass.t1 )
            #####
            print outputString
            self.rawFileHandle.write( outputString )
            self.rawFileHandle.flush()
        #####

        # Take out trash
        deleteFile( self.perlFile )
        for scaffID in scaffList:
            tmpFASTAFile = join( self.tmpPath, '%s_tmp.fasta'%scaffID )
            deleteFile( tmpFASTAFile )
            tmpFASTAFile = join( self.tmpPath, '%s_tmp.fasta.kmermasked'%scaffID )
            deleteFile( tmpFASTAFile )
        #####
        return

    #####

    def clean_tmp(self):
        stdout.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

    #####
