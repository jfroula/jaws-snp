__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

import re

commify_re = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub

#==============================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )

#==============================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True

#==============================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True

#==============================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True

#==============================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )

#==============================================================
def printAndExec( cmd, oh_cmdLog, execute=True ):
    stderr.write( '%s\n\n'%cmd )
    oh_cmdLog.write( '%s\n\n'%cmd )
    try:
        if ( execute ): system( cmd )
    except KeyboardInterrupt:
        print( "KeyboardInterrupt detected, stopping the execution" )
        exit()
    #####
    return

#==============================================================
def convertIlluminaReadName( line ):
    try:
        splitLine = line[1:].split(None)
        baseName  = splitLine[0].replace(":","_").replace("-","_")
        ext       = splitLine[1][0]
    except IndexError:
        splitLine = line[1:].split('/')
        baseName  = splitLine[0].replace(":","_").replace("-","_")
        ext       = splitLine[1]
    #####
    return "%s-R%s"%( baseName, ext )

#==============================================================
def createTmpDirectory():
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpDir   = generateTmpDirName()
    tmpPath  = join( basePath, tmpDir )
    deleteDir(tmpPath)
    mkdir(tmpDir, 0o777)
    testDirectory(tmpDir)
    return tmpDir, tmpPath, basePath

#==============================================================
def skip_FASTQ_Lines( tmpHandle, nSkip ):
    for n in range( 4 * int( nSkip ) ): tmpHandle.readline()

#==============================================================
def write_tmp_fastq_file( tmpFASTQ, fastqHandle, readsPerBatch ):
    tmpFASTQHandle = open( tmpFASTQ, 'w' )
    readCounter    = 0
    stderr.write( '\t-Writing the temporary FASTQ\n' )
    while ( readCounter < readsPerBatch ):
        elemList = 4*['']
        for i in range(4):
            line = fastqHandle.readline().decode('utf-8')
            if line:
                elemList[i] = str(line)
            else:
                elemList[i] = None
            #####
        #####
        # Stoping iteration since we are at the end of the file
        if ( elemList.count(None) == 4 ): 
            notDone = False
            break
        #####
        # Screening for N's in the sequence
        if ( elemList[1].count('N') == 0 ):
            # Writing the elements to the file
            for item in elemList: tmpFASTQHandle.write( item )
            readCounter      += 1
        #####
    #####
    tmpFASTQHandle.close()
    return readCounter

#==============================================================
def renameFASTAReads( orig_UNCONV_FASTA, origFASTA, totalSeqExtracted, oh_cmdLog ):
    stderr.write( '\t-Renaming reads (FASTA)\n' )
    oh = open( origFASTA, "w" )
    for line in open( orig_UNCONV_FASTA ):
        if ( line[0] != '>' ): 
            totalSeqExtracted += len(line) - 1  # The -1 is for the carriage return
            oh.write( line )
        else:
            oh.write( ">%s\n"%( convertIlluminaReadName(line) ) )
        #####
    #####
    oh.close()
    printAndExec( "rm -f %s"%orig_UNCONV_FASTA, oh_cmdLog, execute=True )

#==============================================================
def renameQUALReads( orig_UNCONV_QUAL, origQUAL, oh_cmdLog ):
    stderr.write( '\t-Renaming reads (QUAL)\n' )
    oh = open( origQUAL, "w" )
    for line in open( orig_UNCONV_QUAL ):
        if ( line[0] != '>' ): 
            oh.write( line )
        else:
            oh.write( ">%s\n"%( convertIlluminaReadName(line) ) )
        #####
    #####
    oh.close()
    printAndExec( "rm -f %s"%orig_UNCONV_QUAL, oh_cmdLog, execute=True )
    
