
from math import log
from math import floor
from math import ceil
from array import array
from itertools import chain
from itertools import count
from itertools import izip
from itertools import izip_longest
import struct

upper_bases = "_ATCGXN"
lower_bases = "_atcgxn"
all_bases = "_ATCGXNatcgxn"

def compact(collection, shift):
    result = 0
    for item in collection:
        try:
            result = (result << shift) | item
        except:
            result = (result << shift)
    return result

def group(collection, stride):
    i = iter(chain(collection, [None] * (stride-1)))
    try:
        while True:
            yield [i.next() for x in xrange(0,stride)]
    except:
        pass
        
def group_from_end(collection, stride):
    temp = [x for x in group(reversed(collection), stride)]
    [x.reverse() for x in temp]
    temp.reverse()
    return temp
    
def struct_format(seq_array):
    return "%d%s"%(len(seq_array), seq_array.typecode)
        
bytes_per_word = array('L').itemsize

def compact_sequence(seq, bases=all_bases):
    a = array('L')
    bits_per_base = int(ceil(log(len(bases), 2)))
    bases_per_word = int(floor(8.0 / bits_per_base * bytes_per_word))
    sequence = [bases.find(char) for char in seq]
    sequence = [(s>0) and s or 0 for s in sequence]
    a.extend(compact(x, bits_per_base) for x in group(sequence, bases_per_word))
    format = struct_format(a)
    return format, struct.pack(format, *a)
    
def expand(number, bytes, shift):
    mask = int(2**shift-1)
    bins = int(floor(bytes*8.0/shift))
    return ((number >> ((i-1)*shift)) & mask for i in xrange(bins, 0, -1))
    
def expand_sequence(data, bases=all_bases):
    bits_per_base = int(ceil(log(len(bases), 2)))
    bases_per_word = int(floor(8.0 / bits_per_base * bytes_per_word))
    
    result = []
    for number in struct.unpack(*data):
        result.extend(expand(number, bytes_per_word, bits_per_base))
    return ''.join(bases[i] for i in result if bases[i] != '_')
    
############################################

def translator(code, default=None):
    return [x in code and code.find(x) or default for x in [chr(i) for i in xrange(0,256)]]
    
def itranslate(seq, translator, length=[0]):
    for x in seq:
        length[0] += 1
        try:
            yield translator[x]
        except:
            yield None
            
def icompact(seq, code=upper_bases, length=[0]):
    bits_per_base = int(ceil(log(len(code), 2)))
    bases_per_word = int(floor(8.0 / bits_per_base * bytes_per_word))
    
    for x in group(itranslate(seq,translator(code),length), bases_per_word):
        yield compact(x, bits_per_base), length[0]
    
############################################

class hypercube:
    def __init__(self, type, dimensions, fill=0):
        self.dimensions = dimensions[:]
        self._factors = len(self.dimensions)*[0]
        self._length = 1
        for i in xrange(0,len(self.dimensions)):
            self._factors[i] = self._length
            self._length *= self.dimensions[i]
        self._factors.reverse()
        self._array = type(self._length*[fill])
        
    def _true_key(self, multikey):
        return sum(key*factor for key, factor in izip(multikey, self._factors))
        
    def __getitem__(self, multikey):
        if len(multikey) < len(self.dimensions) or None in multikey:
            return self.subspace(multikey)
        try:
            return self._array[self._true_key(multikey)]
        except IndexError:
            raise KeyError(multikey)
        
    def __setitem__(self, multikey, value):
        self._array[self._true_key(multikey)] = value
        
    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default
            
    def subspace(self, multikey):
        return cubeview(self, [x for x,i in izip_longest(multikey, xrange(len(self.dimensions)))])
        
        
class cubeview:
    def __init__(self, cube, override):
        self._cube = cube
        self._override = override
    
    def _true_key(self, multikey):
        keys = []
        keyiter = iter(multikey)
        for over in self._override:
            if over is None:
                try:
                    keys.append(keyiter.next())
                except StopIteration:
                    keys.append(None)
            else:
                keys.append(over)
        return keys
        
    def __getitem__(self, multikey):
        return self._cube[self._true_key(multikey)]
        
    def __setitem__(self, multikey, value):
        self._cube[self._true_key(multikey)] = value
        
    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default
            
    def subspace(multikey):
        cubeview(self._cube, self._true_key(multikey))
    
        
class sparse_cube:
    def __init__(self, stride, type, dimensions, fill=0):
        self.height = len(dimensions)/stride
        self._metablock = lambda layer: hypercube(list, dimensions[layer*stride:layer*stride+stride], None)
        self._datablock = lambda layer: hypercube(type, dimensions[layer*stride:layer*stride+stride], fill)
        self._block = lambda layer: layer >= self.height and self._datablock(layer) or self._metablock(layer)
        self._subspaces = lambda multikey: group(multikey, stride)
        self._root = self._block(0)
        
    def __setitem__(self, multikey, value):
        node = self._root
        depth = 0
        for subspace in self._subspaces(multikey):
            if depth >= self.height:
                node[subspace] = value
            else:
                next = node.get(subspace)
                if next is None:
                    node[subspace] = self._block(depth+1)
                    next = node.get(subspace)
                node = next
                depth += 1
                
    def __getitem__(self, multikey):
        node = self._root
        for subspace in self._subspaces(multikey):
            node = node[subspace]
        return node
        
def array_type(code):
    return (lambda x: array(code, x))
        
############################################

def node(data, score):
    return (data, score, ())
    
def join_nodes(nodes, data=None):
    result = (data, sum(n[1] for n in nodes if n is not None), tuple(n for n in nodes if n is not None))
    if result[0] is None and len(result[2]) == 1:
        return result[1]
    return result

def huff_tree(symbols):
    symbols = [node(symbol, count) for symbol, count in symbols]
    while len(symbols) > 1:
        symbols.sort(key=lambda x : x[1])
        a,b = symbols.pop(), symbols.pop()
        symbols.append(join_nodes([a,b]))
    return symbols

        
    
############################################
             
     
def real_main():
    seq = "ATATCGGTGTACCAGTAGACAGATAGAGTATGACAGATGTAGTGACAGATCGATGACACAGATAGACCAAAGAGGGTTGGAACCCACAGTGACCCGATATTTAACTGGTAAAAAAAAAATCCGCGCAT"
    compact = compact_sequence(seq)
    result = expand_sequence(compact)
    
    print seq
    print compact
    print result
    
    print huff_tree(zip("ABCDEF", [1,12,3,40,5,6]))
    
    print huff_tree([('b',1), ('p',1), ('`',2), ('m',2), ('o',3), ('d',3), ('a',4), ('i',4), ('r',5), ('u',5), ('j',3), ('l',6), ('s',6), ('e',8), ('_',12)])

    
def profile_main():
    from cProfile import Profile
    from pstats import Stats
    prof  = Profile().runctx("real_main()", globals(), locals())
    stats = Stats( prof ).sort_stats("time").print_stats(60)
    return

if __name__ == "__main__":
    #profile_main()
    real_main()