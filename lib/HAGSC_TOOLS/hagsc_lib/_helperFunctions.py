
__author__  = "Jerry Jenkins"
__date__    = "$Nov 30, 2009 10:59:03 AM$"

# Local Library Imports
from os.path import splitext
from os.path import abspath
from os.path import isfile
from os.path import split
from os.path import isdir
from os.path import join

from shutil import rmtree

from sys import stdout, stderr

from os import listdir
from os import curdir
from os import remove
from os import getpid
from os import uname

import re

import subprocess

#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False

#=========================================================================
def Gzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | gunzip -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "gzip -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "gzip-c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False

#==============================================================
def generateTilingPath(BES_list):

    # Initializing the main variables
    graph         = {}  #key=baseName, value=overlapDist
    overlapSets   = []  # List of overlapping sets for use in generating the graph
    BES_to_retain = []
        
    # Generating the start position dictionary
    BES_start = dict( [ (bacID,(bacStart,bacEnd)) for bacStart, bacEnd, bacID in BES_list] )
        
    # Creating the graph
    nullSet         = set()
    nBES            = len(BES_list)
    BES_list.sort()
    for n in xrange( nBES - 1 ):
        sn,en,bnn = BES_list[n]
        for m in xrange( (n+1), nBES ):
            sm,em,bnm = BES_list[m]
            # Finding the overlapping sets
            if ( not isOverlapped(sn,en,sm,em) ): continue
            # Adding the new pair to the sets
            overlapSets.append( set( [bnn, bnm] ) )
            # Collapsing the overlapped sets
            notDone = True
            while ( notDone ):
                notDone = False
                nOverlapSets = len(overlapSets)
                for k in xrange( nOverlapSets - 1 ):
                    for i in xrange( (k+1), nOverlapSets ):
                        if ( overlapSets[k].intersection(overlapSets[i]) != nullSet ):
                            collapseSets = True
                            collapse     = [k,i]
                            collapse.sort()
                            overlapSets[collapse[0]].update(overlapSets[collapse[1]])
                            overlapSets.pop( collapse[1] )
                            notDone = True
                            break
                        #####
                    #####
                    if ( notDone ): break
                #####
            #####
            # Computing the overlap distance
            tmpList = [sn,en,sm,em]
            tmpList.sort()
            overlapDist = tmpList[2] - tmpList[1]
            # Adding the elements to the graph
            if ( graph.has_key(bnn) ):
                graph[bnn][bnm] = overlapDist
            else:
                graph[bnn] = {}
                graph[bnn][bnm] = overlapDist
            #####
            if ( graph.has_key(bnm) ):
                graph[bnm][bnn] = overlapDist
            else:
                graph[bnm] = {}
                graph[bnm][bnn] = overlapDist
            #####
        #####
    #####

    # Computing the shortest path through the graph
    if ( len(overlapSets) > 0 ):
        # In this case we have overlapping sets, so we need to compute the minimum
        # tiling path across the regions
        for tmpSet in overlapSets:
            if ( len(tmpSet) > 1 ):
                # Pulling out the start and end node
                tmpList = [(BES_start[item][0],BES_start[item][1],item) for item in tmpSet]
                tmpList.sort()
                # Computing the minimum tiling path
                x = shortestPath(graph,tmpList[0][2],tmpList[-1][2],visited=[],distances={},predecessors={})
                # Retaining the BES
                for item in x[1]: BES_to_retain.append( item )
            else:
                # This means that we have an overlap set with just one member
                print "HALT:"
                print "Somehow we wound up with an overlap set with just one member"
                print "Check what you are doing!!!"
                print tmpSet
                assert False
            #####
        #####
    else:
        # If there are no overlapping sets, this means that all of the
        # BES on the scaffold are not connected, so we just write them
        # out to the file.
        for item in BES_list: BES_to_retain.append( item[2] )
    #####
    return BES_to_retain

#==============================================================
def shortestPath(graph,start,end,visited=[],distances={},predecessors={}):
    """Find the shortest path between start and end nodes in a graph"""
    from sys import maxint
    # Detect if it's the first time through, set current distance to zero
    if not visited: distances[start]=0
    if start==end:
        # We've found our end node, now find the path to it, and return
        path=[]
        while end != None:
            path.append(end)
            end=predecessors.get(end,None)
        #####
        return distances[start], path[::-1]
    #####
    # Process neighbors as per algorithm, keep track of predecessors
    for neighbor in graph[start]:
        if neighbor not in visited:
            neighbordist = distances.get(neighbor,maxint)
            tentativedist = distances[start] + graph[start][neighbor]
            if tentativedist < neighbordist:
                distances[neighbor] = tentativedist
                predecessors[neighbor] = start
            #####
        #####
    #####
    # Neighbors processed, now mark the current node as visited
    visited.append(start)
    # Finds the closest unvisited node to the start
    unvisiteds  = dict( (k, distances.get(k,maxint) ) for k in graph if k not in visited )
    closestnode = min( unvisiteds, key=unvisiteds.get )
    # Now we can take the closest node and recurse, making it current
    return shortestPath(graph,closestnode,end,visited,distances,predecessors)

#=========================================================================
def isOverlapped(s1,e1,s2,e2):
    return ( not ( (s2>e1) or (s1>e2) ) )

#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        stderr.write( '%s is a bzip2 file\n'%fileName )
        return True
    except IOError:
        pass
    #####
    return False

#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        stderr.write( '%s is a gzip file\n'%fileName )
        return True
    except IOError:
        return False
    #####

#------------------------------
# Commification
commify_re = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
def commify(tmpInt):
    return commify_re( ',', '%d'%(tmpInt) )

#--------------------
# String matching
#--------------------
def Boyer_Moore_Match(pattern, text):
    matches = []
    m = len(text)
    n = len(pattern)
    rightMostIndexes = preprocessForBadCharacterShift(pattern)
    alignedAt = 0
    while alignedAt + (n - 1) < m:
        for indexInPattern in xrange(n-1, -1, -1):
            indexInText = alignedAt + indexInPattern
            x = text[indexInText]
            y = pattern[indexInPattern]
            if indexInText >= m:
                break
            #####
            if x != y:
                r = rightMostIndexes.get(x)
                if not r:
                    alignedAt = indexInText + 1
                else:
                    shift = indexInText - (alignedAt + r)
                    alignedAt += (shift > 0 and shift or alignedAt + 1)
                #####
                break
            elif indexInPattern == 0:
                matches.append(alignedAt)
                alignedAt += 1
            #####
        #####
    #####
    return matches

def preprocessForBadCharacterShift(pattern):
    map = { }
    for i in xrange( len(pattern)-1, -1, -1 ):
        c = pattern[i]
        if c not in map:
            map[c] = i
        #####
    #####
    return map

#---------------
# File Handling
#---------------

def validate_file(filename, mode, errors=[]):
    if (mode == 'r' or mode == 'a') and (not isfile(filename)):
        errors.append("%s does not exist."%filename)
        return None, errors
    try:
        handle = open(filename, mode)
        if handle is None: raise IOError
        return handle, errors
    except:
        errors.append("Could not open %s for %s"%(filename, mode))
        return None, errors
        
def validate_files(name_mode_tuples):
    handles, errors = [], []
    for name, mode in name_mode_tuples:
        handle, errors = validate_file(name, mode, errors)
        handles.append(handle)
    return handles, errors

def generateTmpDirName( dirNum=None, tmpDirName=None ):
    if ( (dirNum==None) and (tmpDirName==None) ):
        return r'tmp.%s.%s'%( uname()[1], getpid() )
    elif ( (dirNum==None) and (tmpDirName!=None) ):
        return tmpDirName
    else:
        return r'tmp.%s.%d'%( uname()[1], dirNum )
    #####

def baseFileName(fileName):
    return splitext(split(fileName)[1])[0]
    
def removeFilenameExtension(filename):
    return splitext(abspath(filename))[0]
    
def filenameExtension(filename):
    try:
        return splitext(abspath(filename))[1]
    except:
        return ""
        
def createBestHitFileName(fastaFile):
    pre, ext = splitext( split(fastaFile)[1] )
    return 'bestHit_%s.out'%pre

def createBLATHitFileName(fastaFile):
    pre, ext = splitext( split(fastaFile)[1] )
    return 'BLAThit_%s.out'%pre

def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True

def deleteFile(fileName):
    if ( isfile(fileName) ): remove(fileName)
    return True

def testFile(fileName):
    if ( not isfile( fileName ) ):
        raise IOError( 'File not found: %s'%fileName )
    #####
    return True

def testDirectory(dirName):
    if ( not isdir( dirName ) ):
        raise IOError( 'Directory not found: %s'%dirName )
    return True

def getFiles(extension, directory=abspath(curdir), appendFullPath=True):
    """
    getFiles( extension, directory = abspath(curdir), appendFullPath = True ):

    Returns a list of files in "directory" with an
    extension that matches "extension".  This function
    returns a list of files with the full path appended
    by default.
    """
    testDirectory(directory)
    if (appendFullPath):
        return [join(directory, entity) for entity in listdir(directory) \
            if (isfile(join(directory, entity)) and \
                (entity[-len(extension):] == extension))]
    else:
        return [entity for entity in listdir(directory) \
            if (isfile(join(directory, entity)) and \
                (entity[-len(extension):] == extension))]
    #####

#-------------------
# Utility Functions
#-------------------

def viewObjectProperties(testObject):
    """
    viewObjectProperties( testObject )

    Provides a snapshot view of the document strings for all
    functions in an object.
    """
    f_screen = lambda x:(x[0] != '_')
    f_out    = lambda x:'%s:\t%s\n\n' % (x, getattr(testObject, x) )
    outputString = map( f_out, filter( f_screen, dir(testObject) ) )
    stdout.write(''.join(outputString))
    return

def debugComment(outputString, debugMode):
    """
    debugComment( outputString, \
                  debugMode )

    Useful function for debugging and testing codes
    """
    if (debugMode): stdout.write( outputString )
    return
    
def get_or_set(dictionary, key, default=None):
    try:
        return dictionary[key]
    except:
        dictionary[key] = default()
        return dictionary[key]
        
def try_index(container, index, default=None):
    try:
        return container[index]
    except:
        return default
        
def try_call(func, default, *args, **kwargs):
    try:
        return func(*args, **kwargs)
    except:
        return default
        
#----------------------------
# File Contents Manipulation 
#----------------------------

def parseString(tempString, sep=' '):
    """
    parseString( tempString, \
                 tempSep=' ' )

    A workhorse of many of my reading and parsing codes.  This
    implementation is the fastest that I can produce, if you can
    make one faster please let me know.
    """
    return [entity.strip() for entity in tempString.split(sep) \
            if (entity not in '\t\n')]

def iterTextFile( textFileHandle, sep=' '):
    """
    iterTextFile( textFileHandle, sep=' ')

    Splits and serves up the contents of a text file using "sep" as the
    main separator.
    """
    if isinstance( textFileHandle, basestring ):
        raise TypeError( "Need a file handle, not a string " + \
                         "(i.e. not a filename)" )
    #####
    if ( sep in set([' ', '\t']) ):
        for line in textFileHandle:
            parsedLine = line.split(None)
            if ( parsedLine == [] ): continue
            yield parsedLine
        #####
    else:
        for line in textFileHandle:
            parsedLine = parseString( line, sep )
            if ( parsedLine == [] ): continue
            yield parsedLine
        #####
    #####
    return

########################################################################
comma_splitPattern = re.compile( r',' )
commaSplitFun      = comma_splitPattern.split
########################################################################

def commaSplit(tempString):
    x = commaSplitFun(tempString)
    return ( (x[-1] in '\n') and [x[:-1]] or [x] )[0]

def fixFile( textFileName ):
    """
    fixFile( textFileName )

    Replaces the \r that the Macintosh sometimes puts at the end of a text
    line in a text file with a \n character and returns the new file name.
    """

    fixedFileName = 'fixed_%s'%textFileName
    tmpHandle = open( fixedFileName, 'w' )
    for line in open(textFileName,'r'):
        tmpHandle.write( line.replace('\r','\n') )
    #####
    tmpHandle.close()
    return fixedFileName
