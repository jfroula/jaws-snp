__author__="jjenkins"
__date__ ="$Aug 23, 2010 11:25:23 PM$"

from _bestHit import DNA_best_hit

from _jobQueueing import remoteServer4
from _jobQueueing import extractScaffoldNames

from _batchProcessing import batch_run_class

from _helperClasses import histogramClass
from _helperClasses import iterCounter

from _helperFunctions import generateTmpDirName
from _helperFunctions import testDirectory
from _helperFunctions import baseFileName
from _helperFunctions import parseString
from _helperFunctions import isBzipFile
from _helperFunctions import isGzipFile
from _helperFunctions import deleteFile
from _helperFunctions import deleteDir
from _helperFunctions import testFile
from _helperFunctions import commify

from _blatObject import iterBLAT

from _FASTA_IO import FASTAFile_dict
from _FASTA_IO import iterFASTA
from _FASTA_IO import writeFASTA

from _QUAL_IO import objectify
from _QUAL_IO import write_qual_record

from _bestHit import iterBestHit, best_extent

from os.path import realpath
from os.path import abspath
from os.path import split
from os.path import join
from os.path import isfile

from os import curdir
from os import system
from os import mkdir
from os import chdir

from math import log10
from math import sqrt

from sys import stdout
from sys import stderr

import subprocess

import re

#=======================================================================
class scaffoldClass( object ):

    def __init__(self, record, sepString):
    
        # Initializing the sequence and clone coverage dictionary
        self.scaffID    = record.scaffold
        self.scaffSize  = record.scaffSize
        self.placements = [record]
        self.sepString  = sepString

        # Local Variables
        self.insertLengths = []
        self.readPairs     = []

    def addPlacement(self, record):
        self.placements.append(record)
        return
    
    def addPair(self, record_1, record_2 ):
        self.readPairs.append( (record_1, record_2) )
        return
        
    def _cloneExtent(self, record1, record2):
        if record1.scaffStart < record2.scaffStart:
            return record1.scaffStart, record2.scaffEnd
        else:
            return record2.scaffStart, record1.scaffEnd

    def build_record(self, id, data="", info=""):
        return objectify({"id":id, "info":info, "data":(lambda : data)})

    def goodPairTest(self, record_1, record_2):
        if ( record_1.scaffStart < record_2.scaffStart ):
            return (record_1.placDir == '+' and record_2.placDir == '-')
        else:
            return (record_2.placDir == '+' and record_1.placDir == '-')
        #####

    def pair_dist(self, record_1, record_2):
        if ( record_1.scaffStart < record_2.scaffStart ):
            return abs( record_1.scaffStart - record_2.scaffEnd )
        else:
            return abs( record_2.scaffStart - record_1.scaffEnd )
        #####

#=======================================================================
class localCounterClass(object):

    def __init__(self):
        self.N_improperlyOriented = 0
        self.goodPlacs   = 0
        self.goodPairs   = 0
        self.belowCutoff = 0
        self.tooLongPair = 0
        self.N_chimeras       = 0
        self.N_small_chimeras = 0
        self.N_possibleJoins  = 0
        self.N_ambiguous      = 0

    def increment_Ambiguous(self):
        self.N_ambiguous += 1
        return

    def increment_goodPlacs(self):
        self.goodPlacs += 1
        return

    def increment_belowCutoff(self):
        self.belowCutoff += 1
        return

    def increment_goodPairs(self):
        self.goodPairs += 2
        return

    def increment_tooLongPair(self):
        self.tooLongPair += 2
        return

    def increment_Improper(self):
        self.N_improperlyOriented += 2
        return

    def increment_chimeric(self):
        self.N_chimeras += 2
        return

    def increment_small_chimeric(self):
        self.N_small_chimeras += 2
        return

    def increment_possibleJoin(self):
        self.N_possibleJoins += 2
        return

#=======================================================================
class readAlignmentClass( object ):

    def __init__(self,match,ID,COV,blatClass,line):
        self.readDict = {}
        self.readDict[match] = [(ID,COV,blatClass,line)]
    
    def addAlignment(self,match,ID,COV,blatClass,line):
        try:
            self.readDict[match].append( (ID,COV,blatClass,line) )
        except KeyError:
            self.readDict[match] = [(ID,COV,blatClass,line)]
        #####
    
    def bestMatches(self):
        DSU = [(k,v) for k,v in self.readDict.iteritems()]
        DSU.sort(reverse=True)
        return DSU[0][1]

#=======================================================================
class alignmentSorting( object ):

    def __init__(self,match,ID,COV,blatClass,line):
        self.alignDict         = {}
        self.alignDict[blatClass.Q_name] = readAlignmentClass(match,ID,COV,blatClass,line)
        
    def addAlignment(self,match,ID,COV,blatClass,line):
        try:
            self.alignDict[blatClass.Q_name].addAlignment(match,ID,COV,blatClass,line)
        except KeyError:
            self.alignDict[blatClass.Q_name] = readAlignmentClass(match,ID,COV,blatClass,line)
        #####
    
    def evaluateAlignments(self,oh,maxInsertSize):
        keyList = [item for item in self.alignDict.iterkeys()]
        # If there is just one of the pair that aligns
        if ( len(keyList) == 1 ):
            tmpKey = keyList[0]
            topMatches = [item for item in self.alignDict[tmpKey].bestMatches()]
            if ( len(topMatches) > 1 ):
                # If ambiguous
                oh.write( '%s\tAMBIGUOUS\n'%tmpKey )
                return
            else:
                # Otherwise write the line
                oh.write( topMatches[0][3] )
                return
            #####
        # Do both reads in the pair align?
        else:
            key_1        = keyList[0]
            topMatches_1 = self.alignDict[key_1].bestMatches()
            key_2        = keyList[1]
            topMatches_2 = self.alignDict[key_2].bestMatches()
            if ( (len(topMatches_1)>1) or (len(topMatches_2)>1) ):
                # Looping over the possible pairings and look for good pairs
                for ID_1,COV_1,blatClass_1,line_1 in topMatches_1:
                    for ID_2,COV_2,blatClass_2,line_2 in topMatches_2:
                        # Is the pair on the same scaffold?
                        if ( blatClass_1.T_name == blatClass_1.T_name ):
                            # Are they withnin the maxInsertSize of one another?
                            posList = [blatClass_1.T_start,blatClass_1.T_end,blatClass_2.T_start,blatClass_2.T_end]
                            posList.sort()
                            localInsertSize = posList[-1] - posList[0]
                            if ( localInsertSize <= maxInsertSize ):
                                oh.write( line_1 )
                                oh.write( line_2 )
                                return
                            #####
                        #####
                    #####
                ####
                # If we make it to here, then we did not find a good pair placement
                # Now we need to look at whether we have two good alignments greater than the cutoff for the pair
                if ( len(topMatches_1) > 1 ):
                    oh.write( '%s\tAMBIGUOUS\n'%key_1 )
                else:
                    oh.write( topMatches_1[0][3] )
                #####
                if ( len(topMatches_2) > 1 ):
                    oh.write( '%s\tAMBIGUOUS\n'%key_2 )
                else:
                    oh.write( topMatches_2[0][3] )
                #####
                return
            else:
                # If there is just one alignment each, then write them out
                oh.write( topMatches_1[0][3])
                oh.write( topMatches_2[0][3])
                return
            #####
        #####

#=======================================================================
class pairAssessment_call( object ):

    def __init__(self, pairsFile, \
                       assemblyFile, \
                       ID_cutoff, \
                       covCutoff, \
                       min_BLAT_score, \
                       numPairs, \
                       maxInsertSize, \
                       arachneReadNames, \
                       nRep_masking, \
                       num24Mers, \
                       noMasking,\
                       nFastaFiles, \
                       cluster):
                       
        # Initial error checking
        testFile(pairsFile)
        testFile(assemblyFile)

        # User input
        self.pairsFile        = realpath(pairsFile)
        self.assemblyFile     = realpath(assemblyFile)
        self.ID_cutoff        = ID_cutoff
        self.covCutoff        = covCutoff
        self.min_BLAT_score   = min_BLAT_score
        self.numPairs         = numPairs
        self.maxInsertSize    = maxInsertSize
        self.arachneReadNames = arachneReadNames
        self.nRep_masking     = nRep_masking
        self.num24Mers        = num24Mers
        self.noMasking        = noMasking
        self.nFastaFiles      = nFastaFiles
        self.cluster          = cluster
        
        # Creating the pair length and placement filter
        self.pair_lengthFilter = lambda x: (abs(x) <= self.maxInsertSize)
        self.pair_placFilter   = lambda record: ( \
                                      (record.per_ID >= self.ID_cutoff) and \
                                      (record.per_coverage >= self.covCutoff) )

        # Read separator type
        self.nameSeparators                 = {}
        self.nameSeparators['illumina']     = '/'
        self.nameSeparators['sanger']       = '.'
        self.nameSeparators['new_illumina'] = '-R'
        
        # Determining the separator string
        self.sepString = self.nameSeparators[ self.canonicalNameSeparator() ]
        stderr.write( '============\n' )
        stderr.write( 'SEP STRING:  %s\n'%self.sepString)
        stderr.write( '============\n\n' )

        # Sorted placements dictionary
        self.sortedPlacements = {}

        # Generating a temporary directory name        
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )
        
        # Tmp assembly file
        self.assemblyFile_tmp = join( self.tmpPath, split(self.assemblyFile)[1] )

        # Masked file name
        if ( self.noMasking ):
            self.maskedGenomeSuffix   = ''
            self.maskedGenomeFileName = '%s'%self.assemblyFile_tmp
        else:
            self.maskedGenomeSuffix   = '.t%d.masked'%self.nRep_masking
            self.maskedGenomeFileName = '%s%s'%( self.assemblyFile_tmp, self.maskedGenomeSuffix )
        #####
        
        # Output pair file
        self.reducedPairFile = join( self.tmpPath, 'reducedPairFile.fasta' )
        
        # BLAT file
        self.blatOutputFile = join( self.tmpPath, 'BLAT_hit_%s.blat'%baseFileName(self.assemblyFile) )

        # BWA file
        self.bwaOutputFile = join( self.tmpPath, '%s.bwaBestHit'%baseFileName(self.assemblyFile) )

        # Sorted scaffold sizes
        self.scaffSizes          = {}
        self.sortedScaffoldSizes = []
        
        self.runAll = True

        # Running the main code
        self.analyzePairs()
    
    def canonicalNameSeparator(self):

        # Opening the FASTA file
        if ( isBzipFile(self.pairsFile) ):
            tmpProcess = subprocess.Popen( 'cat %s | bunzip2 -c'%self.pairsFile, shell=True, stdout=subprocess.PIPE)
        elif ( isGzipFile(self.pairsFile) ):
            tmpProcess = subprocess.Popen( 'cat %s | gunzip -c'%self.pairsFile, shell=True, stdout=subprocess.PIPE)
        else:
            tmpProcess = subprocess.Popen( 'cat %s'%self.pairsFile, shell=True, stdout=subprocess.PIPE)
        #####
        fastaHandle = tmpProcess.stdout

        # Obtaining one name
        readName = None
        for record in iterFASTA(fastaHandle, self.arachneReadNames):
            readName = record.id
            break
        #####
        # Determining which separator is best
        for sepType, sepString in self.nameSeparators.items():
            splitName = parseString( readName, sepString )
            if ( len(splitName) > 1 ):
                return sepType
            #####
        #####
        tmpProcess.kill()
        # If none is found, provide error message
        raise ValueError('Could not determine separator type for %s\n'%readName)

    def analyzePairs(self):
    
        # Creating the temporary directory
        if ( self.runAll ): self.create_tmp_dir()
        
        # Lightly masking the assembly
        if ( self.runAll ): self.maskGenome()
        
        # Pull the test set of pairs
        if ( self.runAll ): self.pullPairs()
        
        # BLAT the pairs on the genome
        if ( self.runAll ): self.runBLATHit()
        
        # Sorting the placements and generating statistics
        if ( self.runAll ): self.sortPairPlacements()
        
        # Cleaning up the mess
        if ( self.runAll ): self.clean_tmp()
        
        return
    
    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def maskGenome(self):

        # Copying the genome to the temporary directory
        chdir (self.tmpPath)
        if ( isfile(self.assemblyFile) ):
            cmd = 'ln -s %s'%self.assemblyFile
        elif ( isfile( join(self.basePath, self.assemblyFile) ) ):
            cmd = 'ln -s %s'%join(self.basePath, self.assemblyFile)
        #####
        stderr.write( 'Making a link to the genome in the tmp directory:\n  %s\n'%cmd )
        system( cmd )
        chdir(self.basePath)
        
        # Checking to see if we need masking
        if ( self.noMasking ): return

        # Mask the genome
        tmpList =['/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/EXBIN/mask_repeats_hash']
        tmpList.append( '-m 24' )
        tmpList.append( '-t %d'%self.nRep_masking ) 
        tmpList.append( '-z %s'%self.num24Mers )
        tmpList.append( '-s %s'%self.maskedGenomeSuffix)
        tmpList.append( '-k 50000' )
        tmpList.append( '-L' )
        tmpList.append( '-H %s'%self.assemblyFile_tmp )
        tmpList.append( '%s'%self.assemblyFile_tmp )
        cmd = ' '.join(tmpList)
        stderr.write( '\nMasking Command:  %s\n'%cmd )
        system( cmd )
        #####
        return
        
    def pullPairs(self):
        # Pulling the pairs
        pair_dict = {}
        self.reducedPairHandle = open( self.reducedPairFile, 'w' )
        nCounter = 0
        addOne   = iterCounter(10000)
        
        # Opening the FASTA file
        if ( isBzipFile(self.pairsFile) ):
            tmpProcess = subprocess.Popen( 'cat %s | bunzip2 -c'%self.pairsFile, shell=True, stdout=subprocess.PIPE)
        elif ( isGzipFile(self.pairsFile) ):
            tmpProcess = subprocess.Popen( 'cat %s | gunzip -c'%self.pairsFile, shell=True, stdout=subprocess.PIPE)
        else:
            tmpProcess = subprocess.Popen( 'cat %s'%self.pairsFile, shell=True, stdout=subprocess.PIPE)
        #####
        fastaHandle = tmpProcess.stdout
        
        for record in iterFASTA( fastaHandle, self.arachneReadNames ):
            canName = parseString( record.id, self.sepString )[0]
            try:
                writeFASTA( [record, pair_dict[canName]], self.reducedPairHandle )
                nCounter += 2
                if ( nCounter >= self.numPairs ): break
                addOne()
                addOne()
            except KeyError:
                pair_dict[canName] = record
            #####
        #####
        self.reducedPairHandle.close()
        tmpProcess.kill()
        return
    
    def runBLATHit(self):
    
        # Changing into temporary directory
        chdir(self.tmpDir)
        
        # Blatting the pair files
#         blatFrontEnd = '/mnt/local/EXBIN/blat -noHead -mask=lower -tileSize=12 -minScore=%d %s %s stdout'%(self.min_BLAT_score,self.maskedGenomeFileName,self.reducedPairFile)
#         awkCMD       = "awk '{ ID=(($13-$12)<($17-$16))?100*($1+$3)/($13-$12):100*($1+$3)/($17-$16);COV=100*($13-$12)/$11;if ((ID>%s) && (COV>%s)) print }'"%(self.ID_cutoff,self.covCutoff)
#         cmd          = '%s | %s > tmp.blat'%(blatFrontEnd,awkCMD)
#         stderr.write( '\nBLAT Command:  %s\n'%cmd )
#         system( cmd )
        # Blatting the pair files
        blatCommand = 'blat -noHead -tileSize=18 -extendThroughN %(target)s %(query)s stdout'
        awkFilter   = "awk '{ ID=((\$13-\$12)<(\$17-\$16))?100*(\$1+\$3)/(\$13-\$12):100*(\$1+\$3)/(\$17-\$16);COV=100*(\$13-\$12)/\$11;if ((ID>%s) && (COV>%s)) print }'"%(self.ID_cutoff,self.covCutoff)
        outCMD      = 'gzip -c > %(tmpOutput)s'
        alignCMD    = '%s | %s | %s'%(blatCommand, awkFilter, outCMD)
        # Calling batch processing 
        stderr.write( '\nBLAT Command:  %s\n'%alignCMD )
        batch_run_class(self.reducedPairFile,self.maskedGenomeFileName,self.nFastaFiles,self.cluster, 'tmp.blat', alignCMD).executeCases()

        # Labeling the mildly redundant reads
        tmpDict = {}
        querySizeMultiple = 2
        lineSet = set()
        for blatClass in iterBLAT( open('tmp.blat') ):
            
            # Screening for multiple equivalent lines
            if ( str(blatClass) in lineSet ): continue
            lineSet.add( str(blatClass) )

            # Initializing the output string
            outputString    = 12 * ['']
            outputString[0] = blatClass.Q_name
            outputString[1] = '%d'%blatClass.Q_size
            outputString[2] = '%d'%blatClass.Q_start
            outputString[3] = '%d'%blatClass.Q_end

            # Size of match divided by shorter of read sizes
            Q_length = float( blatClass.Q_end - blatClass.Q_start )
            T_length = float( blatClass.T_end - blatClass.T_start )
            Q_size   = float( blatClass.Q_size )
            T_size   = float( blatClass.T_size )
            
            # Computing the score of the alignment
            totalMatchSize = blatClass.match + blatClass.repMatch

            # ID = max % of identical bases in the placement length
            # of the query/target
            ID = 100.0 * float(totalMatchSize) / min( Q_length, T_length )
            ID = min( 100.0, id )

            # Coverage = max % of the total query/target length that places
            COV = 100.0 * max( (Q_length/Q_size), (T_length/T_size) )

            start, stop = best_extent( Q_length, \
                                  blatClass.blockSizes, \
                                  blatClass.T_starts, \
                                  querySizeMultiple )
            outputString[4]  = '%s'%blatClass.strand
            outputString[5]  = '%s'%blatClass.T_name
            outputString[6]  = '%d'%blatClass.T_size
            outputString[7]  = '%d'%start
            outputString[8]  = '%d'%stop
            outputString[9]  = '%d'%totalMatchSize
            outputString[10] = '%5.2f'%ID
            outputString[11] = '%5.2f\n'%COV
            baseName = blatClass.Q_name.split(self.sepString)[0]
            try:
                tmpDict[baseName].addAlignment( totalMatchSize, ID, COV, blatClass, '\t'.join(outputString) )
            except KeyError:
                tmpDict[baseName] = alignmentSorting( totalMatchSize, ID, COV, blatClass, '\t'.join(outputString) )
            #####
        #####
        
        # Looking for pairs where both reads have 
        oh = open( self.blatOutputFile, 'w' )
        for baseName, tmpClass in tmpDict.iteritems():  tmpClass.evaluateAlignments(oh,self.maxInsertSize)
        oh.close()
        
        # Moving back up a directory
        chdir(self.basePath)
        return
    
    def sortPairPlacements(self):
        # Pulling the scaffold sizes
        stderr.write( 'Pulling scaffold sizes\n' )
        self.scaffSizes = dict( [(record.id,len(record.seq)) for record in iterFASTA(open(self.assemblyFile_tmp)) ] )
        
        # Pulling the pair names
        N_total_pairReads = len( extractScaffoldNames(self.reducedPairFile) )
        
        # Initializing the placement output lists
        ambiguousReads          = []
        belowCutoffReads        = []
        chimericReads           = []
        chimericSmallReads      = []
        possibleJoinReads       = []
        tooLongReads            = []
        improperlyOrientedReads = []
        goodPairPlacements      = []

        # Initializing the counter class
        counterClass = localCounterClass()

        # Global insert sizes
        insertLengths = []
        
        # Setting up the main dictionary
        unpaired = {}
        
        # Accumuating statistics
        listType = type([])
        stderr.write( '\t-Accumulating Pair Statistics...\n')
        for record_1 in iterBestHit( open(self.blatOutputFile,'r') ):
        
            # Reading in the ambiguously placed reads
            if ( type(record_1) == listType ):
                counterClass.increment_Ambiguous()
                ambiguousReads.append( record_1[0] )
                continue
            #####

            # Sorted records for use later in computing
            if ( self.pair_placFilter(record_1) ):
                # Sort the placement
                try:
                    self.sortedPlacements[record_1.scaffold].addPlacement(record_1)
                except KeyError:
                    self.sortedPlacements[record_1.scaffold] = scaffoldClass(record_1,self.sepString)
                #####
                # Increment the good placement counter
                counterClass.increment_goodPlacs()
                # Extracting the canonical name
                canonicalName = record_1.BAC_recordName.split(self.sepString)[0]
                # Accumulating the unpaired reads
                try:
                    record_2 = unpaired[canonicalName]
                except KeyError:
                    # Add the name to the unpaired list and continue
                    unpaired[canonicalName] = record_1
                    continue
                #####
                # Accumulating the statistics and screening length
                if ( self.goodPairTest(record_1, record_2) ):
                    pairDist = self.pair_dist( record_1, record_2 )
                    if ( self.pair_lengthFilter(pairDist) ):
                        # Adding the length to the insert length
                        insertLengths.append( pairDist )
                        counterClass.increment_goodPairs()
                        goodPairPlacements.append( self.pairString(record_1, record_2) )
                        self.sortedPlacements[record_1.scaffold].addPair(record_1, record_2 )
                    else:
                        counterClass.increment_tooLongPair()
                        tooLongReads.append( self.pairString(record_1, record_2) )
                    #####
                else:
                    # Determining improper orientation
                    if ( record_1.scaffold == record_2.scaffold ):
                        counterClass.increment_Improper()
                        improperlyOrientedReads.append( self.pairString(record_1, record_2) )
                    else:
                        # Checking to see if the chimeric pair are both in long
                        # scaffolds, or do they occur in short stubby scaffolds
                        if ( (self.scaffSizes[record_1.scaffold] > 50000) and \
                             (self.scaffSizes[record_2.scaffold] > 50000) ):
                            # Calculate the possible join insert size
                            if ( record_1.placDir == '-' ):
                                size_1 = record_1.scaffStart
                            else:
                                size_1 = self.scaffSizes[record_1.scaffold] - record_1.scaffEnd
                            #####
                            if ( record_2.placDir == '-' ):
                                size_2 = record_2.scaffStart
                            else:
                                size_2 = self.scaffSizes[record_2.scaffold] - record_2.scaffEnd
                            #####
                            tmp_insertSize = size_1 + size_2
                            if ( tmp_insertSize <= self.maxInsertSize ):
                                # Increment the possible joins
                                counterClass.increment_possibleJoin()
                                possibleJoinReads.append( self.pairString(record_1, record_2) )
                            else:
                                # CHIMERIC!!!
                                counterClass.increment_chimeric()
                                chimericReads.append( self.pairString(record_1, record_2) )
                            #####
                        else:

                            # Calculate the possible join insert size
                            if ( record_1.placDir == '-' ):
                                size_1 = record_1.scaffStart
                            else:
                                size_1 = self.scaffSizes[record_1.scaffold] - record_1.scaffEnd
                            #####
                            if ( record_2.placDir == '-' ):
                                size_2 = record_2.scaffStart
                            else:
                                size_2 = self.scaffSizes[record_2.scaffold] - record_2.scaffEnd
                            #####
                            tmp_insertSize = size_1 + size_2
                            if ( tmp_insertSize <= self.maxInsertSize ):
                                # Increment the possible joins
                                counterClass.increment_possibleJoin()
                                possibleJoinReads.append( self.pairString(record_1, record_2) )
                            else:
                                # CHIMERIC!!!
                                counterClass.increment_small_chimeric()
                                chimericSmallReads.append( self.pairString(record_1, record_2) )
                            #####
                        #####
                    #####
                #####
                # Freeing the name from the unpaired set
                unpaired.pop(canonicalName)
            else:
                # Counting reads that place below cutoff
                counterClass.increment_belowCutoff()
                belowCutoffReads.append( str(record_1) )
            #####
        #####
        
        # Keeping the memory overhead low
        unpaired = None

        # Computing the total number of placements
        N_ambiguous     = counterClass.N_ambiguous
        N_goodPlacs     = counterClass.goodPlacs
        N_total_placs   = N_goodPlacs + N_ambiguous
        N_didNotPlace   = N_total_pairReads - N_total_placs

        # Computing the breakdown of placements
        N_pairedReads   = counterClass.goodPairs
        N_impOrient     = counterClass.N_improperlyOriented
        N_chimeric      = counterClass.N_chimeras
        N_small_chimera = counterClass.N_small_chimeras
        N_possibleJoins = counterClass.N_possibleJoins
        N_tooLongPair   = counterClass.tooLongPair
        N_unpairedReads = N_goodPlacs - ( N_pairedReads + N_impOrient + \
                                          N_chimeric    + N_small_chimera + \
                                          N_possibleJoins + N_tooLongPair )
        N_total_pairs = N_goodPlacs - N_unpairedReads

        # Finding the natural bounds for the computations
        showInsertStats = True
        if ( insertLengths != [] ):
            tmpMinVal    = min( insertLengths )
            tmpMaxVal    = max( insertLengths )
            try:
                boundsFinder = histogramClass( tmpMinVal, tmpMaxVal, 10000 )
                map( boundsFinder.addData, insertLengths )
                newMinVal, newMaxVal = boundsFinder.findingBounds( 0.002 )
                # Downselecting the data to the new bounds
                newInsertLengths = filter( lambda x: ( newMaxVal >= x >= newMinVal ), \
                                                                         insertLengths )
                del insertLengths
        
                # Computing the average value
                N_pairs         = float( len( newInsertLengths ) )
                averageDistance = float( sum( map(abs, newInsertLengths) ) ) / N_pairs
        
                # Generating the standard deviation
                stdDev = 0.0
                for x in newInsertLengths:
                    dx = float(abs(x)) - averageDistance
                    stdDev += dx * dx
                #####
                stdDev = sqrt( stdDev / N_pairs )
        
                # Generating the histogram of insert sizes
                min_pair_length = newMinVal
                max_pair_length = newMaxVal
                histClass = histogramClass( min_pair_length, max_pair_length, 200 )
                map( histClass.addData, newInsertLengths )
            except ValueError:
                showInsertStats = False
            #####
        else:
            showInsertStats = False
        #####

        # Generating the header string
        cnst_1 = 100.0 / float(N_total_pairReads)
        if ( showInsertStats ):
            try: 
                pair_constant = 100.0 / float(N_total_pairs)
            except ZeroDivisionError:
                pair_constant = 0.0
            #####
        else:
            pair_constant = 0.0
        #####

        headerString = []
        headerString.append( '-------\n')
        headerString.append( 'Assembly File:  %s\n'%self.assemblyFile )
        headerString.append( 'Pairs File:     %s\n'%self.pairsFile )
        headerString.append( 'BLAT Hit File:  %s\n'%self.blatOutputFile )
        headerString.append( '-------\n')
        headerString.append( '%s\tTotal Reads\n'%commify(N_total_pairReads) )
        headerString.append( '-------\nTotal Placement Summary:\n')
        tmpPer = cnst_1 * float(N_goodPlacs)
        headerString.append( '%s\t\tGood Placements(>=%d%% ID, >=%d%% cov) (%.2f%%)\n'%( \
                                                                     commify(N_goodPlacs), \
                                                                           self.ID_cutoff, \
                                                                           self.covCutoff, \
                                                                                   tmpPer) )
        tmpPer = cnst_1 * float(N_ambiguous)
        headerString.append( '%s\t\t Ambiguous Placements (%.2f%%)\n'%(commify(N_ambiguous),tmpPer))
        tmpPer = cnst_1 * float(N_total_placs)
        headerString.append( '%s\t Total Placements (Good+Ambiguous) (%.2f%%)\n'%(commify(N_total_placs),tmpPer))





        tmpPer = cnst_1 * float(N_didNotPlace)
        headerString.append( '-------\n%s\t Did not place, or were below the cutoff (%.2f%%)\n'%(commify(N_didNotPlace),tmpPer))
        
        headerString.append( '-------\n')
        tmpPer = cnst_1 * float(N_unpairedReads)
        headerString.append( '%s\t Unpaired reads (%.2f%%)\n'%(commify(N_unpairedReads), tmpPer) )
                                                                       
        headerString.append( '-------\nGood-Pair Placement Summary:\n')
        
        headerString.append( '%s Total paired reads (Good-Unpaired)\n'%(commify(N_total_pairs)) )
        
        tmpPer = pair_constant * float(N_pairedReads)
        headerString.append( '%s\t\tGood paired reads (%.2f%% of total pairs)\n'%(commify(N_pairedReads), tmpPer) )
        tmpPer = pair_constant * float(N_chimeric)
        headerString.append( '%s\t\tChimeric paired reads (both scaffolds >50kb) (%.2f%% of total pairs)\n'%(\
                                                           commify(N_chimeric), \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_small_chimera)
        headerString.append( '%s\t\tChimeric paired reads (1+ in scaffolds <50kb scaffolds) (%.2f%% of total pairs)\n'%(\
                                                      commify(N_small_chimera), \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_possibleJoins)
        headerString.append( '%s\t\tReads forming possible joins (%.2f%% of total pairs)\n'%(\
                                                      commify(N_possibleJoins), \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_impOrient)
        headerString.append( '%s\t\tImproperly oriented paired reads (%.2f%% of total pairs)\n'%(\
                                                          commify(N_impOrient), \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_tooLongPair)
        tmpSize = int( float(self.maxInsertSize) / 1000.0 )
        headerString.append( '%s\t\tPaired reads with insert size >%dkb (%.2f%% of total pairs)\n'%(\
                                                        commify(N_tooLongPair), \
                                                                       tmpSize,\
                                                                       tmpPer) )

        if ( showInsertStats ):
            headerString.append( '-------\n')
            headerString.append( 'Mean Insert Size = %.2f\n'%averageDistance )
            headerString.append( 'STD Insert Size  = %.2f\n'%stdDev )
        #####
        headerString.append( '-------\n\n')

        # Writing to the placement length and scaffID histogram summary file
        tmpHandle    = stdout
        outputString = []
        if ( showInsertStats ):
            outputString.append( 'Histogram of Insert Sizes:\n' )
            outputString.append( '--------------------------\n' )
            outputString.append( histClass.generateOutputString() )
        #####
        
        outputString.append( '\n\nGood Pair Placements:\n')
        outputString.append( ''.join(goodPairPlacements) )
        outputString.append( '\nReads Placing below %d%% ID, %d%% cov:\n'%( \
                                               self.ID_cutoff, self.covCutoff) )
        outputString.append( ''.join(belowCutoffReads) )
        outputString.append( '\nChimeric Paired Reads (both placing in scaffolds >= 100kb):\n')
        outputString.append( ''.join(chimericReads) )
        outputString.append( '\nChimeric Paired Reads (1+ placing in a scaffold < 100kb):\n')
        outputString.append( ''.join(chimericSmallReads) )
        outputString.append( '\nPossibleJoin Reads:\n')
        outputString.append( ''.join(possibleJoinReads) )
        tmpSize = int( float(self.maxInsertSize) / 1000.0 )
        outputString.append( '\nProperly placed paired reads >%dkb from one another:\n'%(tmpSize))
        outputString.append( ''.join(tooLongReads) )
        outputString.append( '\nImproperly Oriented Paired Reads:\n')
        outputString.append( ''.join(improperlyOrientedReads) )
        outputString.append( '\nAmbiguously Placed Reads:\n' )
        outputString.append( ''.join(['%s\n'%item for item in ambiguousReads]) )

        # Write initial information to a file
        tmpHandle.write( ''.join( headerString) )
        tmpHandle.write( ''.join( outputString) )
        tmpHandle.close()
        return

    def pairString(self,record_1,record_2):
        return '%s\t%s\t%d\t%d\t%d\t%s\t%s\t%s\t%d\t%d\t%d\t%s\n'%( \
                                                    record_1.BAC_recordName, \
                                                    record_1.scaffold, \
                                                    record_1.scaffSize, \
                                                    record_1.scaffStart, \
                                                    record_1.scaffEnd, \
                                                    record_1.placDir, \
                                                    record_2.BAC_recordName, \
                                                    record_2.scaffold, \
                                                    record_2.scaffSize, \
                                                    record_2.scaffStart, \
                                                    record_2.scaffEnd, \
                                                    record_2.placDir )

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        system( 'rm -rf %s'%self.tmpPath )
        return
        
    def goodPairTest(self, record_1, record_2):
        if ( record_1.scaffold == record_2.scaffold ):
            if ( record_1.scaffStart < record_2.scaffStart ):
                return (record_1.placDir == '+' and record_2.placDir == '-')
            else:
                return (record_2.placDir == '+' and record_1.placDir == '-')
            #####
        else:
            return False
        #####

    def pair_dist(self, record_1, record_2):
        if ( record_1.BAC_recordName.split(self.sepString)[1] == '1' ):
            fac = 1
        else:
            fac = -1
        #####
        if ( record_1.scaffStart < record_2.scaffStart ):
            return fac * abs( record_1.scaffStart - record_2.scaffEnd )
        else:
            return -1 * fac * abs( record_2.scaffStart - record_1.scaffEnd )
        #####

#####
