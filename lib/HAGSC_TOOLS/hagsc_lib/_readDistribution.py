__author__="jjenkins"
__date__ ="$Aug 23, 2010 11:25:23 PM$"

from _jobQueueing import extractScaffoldNames

from _helperClasses import histogramClass

from _helperFunctions import baseFileName
from _helperFunctions import parseString
from _helperFunctions import testFile

from _FASTA_IO import FASTAFile_dict

from _QUAL_IO import objectify
from _QUAL_IO import write_qual_record

from _bestHit import iterBestHit

from math import log10
from math import sqrt

from sys import stdout

import re

########################################################################
#   # Original Email Messages
#    /home/t2c0/AIP1/GT/GPNF.FULL.pairs.fna and the REF is the reference scaffolds
#    Sometimes it would just be a reference organelle.
#    The idea would be to place pairs and then evaluate them.
#    insert size and std dev
#    all placed pairs
#    good placed pairs (out of all placed pairs)
#    pairs that one end places and the other doesn't
#    and then think about distribution and how to calculate avergae clone
#    coverage (of the good ones obviously).
#    You probably want an option to lightly mask the query.
#    Sizes of the BAC ends
#    chimeric sequences???
#    binning the size range
#    Insert size
#    Average/Distribution of sizes
#    Coverage across reference sequence
#    Flexible pairing and nomenclature
#    Not all will hit
#    only look at where both place properly aligned
#
#    Assumptions:
#    (1)  We will be running DNA_best_hit, and there will be only one placement
#         in the reference sequence for a given BAC/fosmid end.
#    (2)  Naming of the BACs is the same for all files in a session
#
# REMAINING:
# ----------
#    Icing:
#    (1)  Have the option to lightly mask the query
#
# DONE
# ----
#    To report in a main output file:
#    (1)  Both place, proper direction
#    (2)  Both place, wrong direction, wrong scaffold, or something like that
#    (3)  Only one placement
#    (4)  No placements
#    (5)  Orphaned BAC ends (only one end occurs in the file, no mate)
#
#    Computations:
#    (1)  Enable very flexible pairing nomenclature (regular expressions)
#    (2)  Compute distributions of length (start of one plac and start of second plac)
#         for good placements
#    (3)  Compute density of placements along each scaffold
#    (4)  Compute coverage across the reference sequence (clone and read)
#    (5)  Identification of chimeric sequences (sequences that place in more
#         than one place).
########################################################################

########################################################################
class scaffoldClass( object ):

    def __init__(self, record, sepString):
    
        from numpy import zeros
        self.zeros = zeros

        # Initializing the sequence and clone coverage dictionary
        self.scaffID    = record.scaffold
        self.scaffSize  = record.scaffSize
        self.placements = [record]
        self.sepString  = sepString

        # Local Variables
        self.insertLengths = []
        self.readPairs     = []

    def addPlacement(self, record):
        self.placements.append(record)
        return
    
    def addPair(self, record_1, record_2 ):
        self.readPairs.append( (record_1, record_2) )
        return
        
    def _cloneExtent(self, record1, record2):
        if record1.scaffStart < record2.scaffStart:
            return record1.scaffStart, record2.scaffEnd
        else:
            return record2.scaffStart, record1.scaffEnd

    def build_record(self, id, data="", info=""):
        return objectify({"id":id, "info":info, "data":(lambda : data)})


    def getQUAL(self,type_1):
        mainQUAL = self.zeros( self.scaffSize )
        if ( type_1 == 'read' ):
            for record in self.placements:
                m = (record.scaffStart-1)
                n = record.scaffEnd
                mainQUAL[m:n] += 1
            #####
        elif ( type_1 == 'pair' ):
            for record_1, record_2 in self.readPairs:
                m = (record_1.scaffStart-1)
                n = record_1.scaffEnd
                mainQUAL[m:n] += 1
                m = (record_2.scaffStart-1)
                n = record_2.scaffEnd
                mainQUAL[m:n] += 1
            #####
        elif ( type_1 == 'clone' ):
            for record_1, record_2 in self.readPairs:
                m,n = self._cloneExtent(record_1, record_2)
                mainQUAL[m:n] += 1
            #####
        #####
        return self.build_record( self.scaffID, data=mainQUAL.tolist() )
        
    def covHist(self,type_1,type_2,kmer):
        if ( type_2 == 'scaff' ):
            # Building the scaffold coverage histograms
            size = self.scaffSize
            bins = int( float(size) / float(kmer) )
            mainHist = histogramClass(1,size,bins)
            if ( type_1 == 'read' ):
                for record in self.placements:
                    mainHist.addExtent( (record.scaffStart, record.scaffEnd) )
                #####
            elif ( type_1 == 'clone' ):
                for rec1, rec2 in self.readPairs:
                    mainHist.addExtent( self._cloneExtent(rec1, rec2)    )
                #####
            elif ( type_1 == 'pair' ):
                for rec1, rec2 in self.readPairs:
                    mainHist.addExtent(  (rec1.scaffStart, rec1.scaffEnd) )
                    mainHist.addExtent(  (rec2.scaffStart, rec2.scaffEnd) )
                #####
            #####
        elif ( type_2 == 'bp' ):
            # Pulling the qual record
            if ( type_1 == 'read' ):
                qualRecord = self.getQUAL(type_1)
            elif ( type_1 == 'clone' ):
                qualRecord = self.getQUAL(type_1)
            elif ( type_1 == 'pair' ):
                qualRecord = self.getQUAL(type_1)
            #####
            # Building the histogram
            print type(qualRecord.data())
            print qualRecord.data()[0:10]
            minHist = min(qualRecord.data())
            maxHist = max(qualRecord.data())
            mainHist = histogramClass( minHist, maxHist, (maxHist-minHist) )
            map( mainHist.addData, qualRecord.data() )
        #####
        return mainHist
        
    def goodPairTest(self, record_1, record_2):
        if ( record_1.scaffStart < record_2.scaffStart ):
            return (record_1.placDir == '+' and record_2.placDir == '-')
        else:
            return (record_2.placDir == '+' and record_1.placDir == '-')
        #####

    def BAC_dist(self, record_1, record_2):
        if ( record_1.scaffStart < record_2.scaffStart ):
            return abs( record_1.scaffStart - record_2.scaffEnd )
        else:
            return abs( record_2.scaffStart - record_1.scaffEnd )
        #####

#####

########################################################################
class localCounterClass(object):

    def __init__(self):
        # Initializing the counters for improperly oriented ends
        self.N_improperlyOriented = 0

        self.goodPlacs = 0
        self.goodPairs = 0
        self.belowCutoff = 0
        self.tooLongPair = 0

        # Initializing the counter for chimeric sequences
        self.N_chimeras = 0
        self.N_small_chimeras = 0

    def increment_goodPlacs(self):
        self.goodPlacs += 1
        return

    def increment_belowCutoff(self):
        self.belowCutoff += 1
        return

    def increment_goodPairs(self):
        self.goodPairs += 2
        return

    def increment_tooLongPair(self):
        self.tooLongPair += 2
        return

    def increment_Improper(self):
        self.N_improperlyOriented += 2
        return

    def increment_chimeric(self):
        self.N_chimeras += 2
        return

    def increment_small_chimeric(self):
        self.N_small_chimeras += 2
        return
#####

########################################################################
class new_read_distribution( object ):

    def __init__(self, BAC_fileName, \
                       assemblyFile, \
                       bestHitFileName, \
                       maxInsertSize, \
                       ID_cutoff, \
                       covCutoff, \
                       summaryFileName = None, \
                       plateStatistics = False, \
                       pairStatistics  = False, \
                       bpCovHist       = False, \
                       scaffCovHist    = False, \
                       histBinWidth    = 1000, \
                       QUAL_read_cov   = False, \
                       QUAL_pair_cov   = False, \
                       QUAL_clone_cov  = False ):

        # Initial error checking
        testFile(BAC_fileName)
        testFile(assemblyFile)

        # User input
        self.BAC_fileName    = BAC_fileName
        self.assemblyFile    = assemblyFile
        self.bestHitFileName = bestHitFileName
        self.maxInsertSize   = maxInsertSize
        self.ID_cutoff       = ID_cutoff
        self.covCutoff       = covCutoff
        
        # User controls of output
        self.plateStatistics = plateStatistics
        self.pairStatistics  = pairStatistics
        self.bpCovHist       = bpCovHist
        self.scaffCovHist    = scaffCovHist
        self.histBinWidth    = histBinWidth
        self.QUAL_read_cov   = QUAL_read_cov
        self.QUAL_pair_cov   = QUAL_pair_cov
        self.QUAL_clone_cov  = QUAL_clone_cov

        # Summary File name
        if ( summaryFileName == None ):
            self.summaryFileName = '%s.summary'%(baseFileName(bestHitFileName))
        else:
            self.summaryFileName = summaryFileName
        #####

        # Creating the BAC length and placement filter
        self.BAC_lengthFilter = lambda x: (x <= self.maxInsertSize)
        self.BAC_placFilter   = lambda record: ( \
                                      (record.per_ID >= self.ID_cutoff) and \
                                      (record.per_coverage >= self.covCutoff) )

        # BAC name
        self.nameSeparators             = {}
        self.nameSeparators['illumina'] = '/'
        self.nameSeparators['sanger']   = '.'
        self.nameSeparators['newIllumina']   = '-R'

        # Determining the separator string
        self.sepString = self.nameSeparators[ self.canonicalNameSeparator() ]

        # Sorted placements dictionary
        self.sortedPlacements = {}
        
        # Sorted scaffold sizes
        self.scaffSizes          = {}
        self.sortedScaffoldSizes = []

        # Regular expression for finding libraries
        self.libParser = re.compile( r'([A-Z]+)([0-9]+)[-]' ).findall

        # Running the main code
        self.analyzeBACs()

    def analyzeBACs(self):
        # Sorting the placements and generating statistics
        if ( self.pairStatistics ):
            self.sortPairPlacements()
        else:
            self.sortReadPlacements()
        #####
        
        # Report histograms or qual files if required
        if ( self.pairStatistics ):
            if ( self.bpCovHist ):
                self.computeCoverageHistograms('pair','bp')
                self.computeCoverageHistograms('clone','bp')
            #####
            if ( self.scaffCovHist ):
                self.computeCoverageHistograms('pair','scaff')
                self.computeCoverageHistograms('clone','scaff')
            #####
            if ( self.QUAL_read_cov ):
                self.computeQUALCov('read')
            #####
            if ( self.QUAL_pair_cov ):
                self.computeQUALCov('pair')
            #####
            if ( self.QUAL_clone_cov ):
                self.computeQUALCov('clone')
            #####
        else:
            if ( self.bpCovHist ):
                self.computeCoverageHistograms('read','bp')
            #####
            if ( self.scaffCovHist ):
                self.computeCoverageHistograms('read','scaff')
            #####
            if ( self.QUAL_read_cov ):
                self.computeQUALCov('read')
            #####
        #####
        return
    
    # Computing coverage histograms
    def computeCoverageHistograms(self,type_1,type_2):
        stdout.write( '\n\t-Computing %s %s Coverage Histograms...\n'%(type_1,type_2) )
        output = open( '%s_%s_%s_Hist.dat'%(baseFileName(self.summaryFileName),type_1,type_2), 'w' )
        for scaffSize, scaffID in self.sortedScaffoldSizes:
            try:
                mainHist = self.sortedPlacements[scaffID].covHist(type_1,type_2,self.histBinWidth)
            except KeyError:
                continue
            #####
            output.write( '\n%s, %d bp : %s-%s Coverage Histogram\n' % (scaffID,scaffSize,type_1,type_2) )
            output.write( mainHist.generateOutputString() )
            output.write( '----------------------------\n' )
        #####
        return

    # Computing coverage as qual file
    def computeQUALCov(self,type_1):
        stdout.write( '\n\t-Outputting %s coverage in QUAL format...\n'%type_1)
        output = open( '%s_%s.qual'%(baseFileName(self.summaryFileName),type_1), 'w' )
        for scaffSize, scaffID in self.sortedScaffoldSizes:
            try:
                mainQUAL = self.sortedPlacements[scaffID].getQUAL(type_1)
            except KeyError:
                continue
            #####
            write_qual_record(mainQUAL, output)
        #####
        return

    def assemblySizeHistogram(self):
        # Generating Histogram of assembly scaffold sizes
        stdout.write( '\t-Generating the Histogram of log(scaffoldSize) for assembly\n')
        indexedFile    = FASTAFile_dict( self.assemblyFile )
        self.scaffSizes     = {}
        scaffSize_list = []
        for scaffID, record in indexedFile.items():
            scaffSize = len(record.seq)
            self.scaffSizes[scaffID] = scaffSize
            self.sortedScaffoldSizes.append( (scaffSize,scaffID) )
            scaffSize_list.append( log10(scaffSize) )
        #####
        self.sortedScaffoldSizes.sort(reverse=True)
        
        minScaffSize = min(scaffSize_list)
        maxScaffSize = max(scaffSize_list)
        if ( minScaffSize == maxScaffSize ):
            minScaffSize = 0.90 * minScaffSize
            maxScaffSize = 1.1 * maxScaffSize
        #####
        scaffSizeHist = histogramClass( minScaffSize, maxScaffSize, 100 )
        map( scaffSizeHist.addData, scaffSize_list )
        tmpHandle = open( 'log_scaffSizeHist.dat', 'w')
        tmpHandle.write( scaffSizeHist.generateOutputString() )
        tmpHandle.close()
        return

    def sortReadPlacements(self):
        stdout.write( '\nRead Analysis:\n')
        
        # Initializing the no_match set
        stdout.write( '\t-Pulling read names\n')
        BAC_name_set = set()
        BAC_name_set = BAC_name_set.union(\
                                        extractScaffoldNames(self.BAC_fileName))
        N_total_BACs = len( BAC_name_set )
        BAC_name_set = None
        
        # Generating the assembly size histogram
        self.assemblySizeHistogram()
        
        # Initializing the placement output lists
        belowCutoffReads        = []
        goodReadPlacements      = []
        
        # Initializing the counter class
        counterClass = localCounterClass()

        # Accumuating statistics
        stdout.write( '\t-Accumulating Read Statistics...\n')
        for record_1 in iterBestHit( open(self.bestHitFileName,'r') ):
            # Sorted records for use later in computing
            if ( self.BAC_placFilter(record_1) ):
                # Sort the placement
                try:
                    self.sortedPlacements[record_1.scaffold].addPlacement( \
                                                                       record_1)
                except KeyError:
                    self.sortedPlacements[record_1.scaffold] = scaffoldClass( \
                                                                     record_1, \
                                                                 self.sepString)
                #####
                goodReadPlacements.append( self.readString(record_1) )
                # Increment the good placement counter
                counterClass.increment_goodPlacs()
            else:
                # Counting reads that place below cutoff
                counterClass.increment_belowCutoff()
                belowCutoffReads.append( str(record_1) )
            #####
        #####

        # Computing the placements
        N_goodPlacs   = counterClass.goodPlacs
        N_belowCutoff = counterClass.belowCutoff
        N_total_placs = N_goodPlacs + N_belowCutoff
        N_didNotPlace = N_total_BACs - N_total_placs

        # Generating the header string
        cnst_1 = 100.0 / float(N_total_BACs)

        headerString = []
        headerString.append( '-------\n')
        headerString.append( '%d\tTotal Reads\n'%N_total_BACs )
        headerString.append( '-------\nTotal Placement Summary:\n')
        tmpPer = cnst_1 * float(N_goodPlacs)
        headerString.append( '%d\t Good Placements(>=%d%% ID, %d%% cov) (%.2f%%)\n'%( \
                                                                  N_goodPlacs, \
                                                               self.ID_cutoff, \
                                                               self.covCutoff, \
                                                                       tmpPer) )
        tmpPer = cnst_1 * float(N_belowCutoff)
        headerString.append( '%d\t Placed, but below Cutoff (%.2f%%)\n'%(\
                                                                N_belowCutoff, \
                                                                       tmpPer) )
        tmpPer = cnst_1 * float(N_didNotPlace)
        headerString.append( '%d\t Did not place (%.2f%%)\n'%(N_didNotPlace,tmpPer))
        headerString.append( '-------\n')

        # Writing to the placement length and scaffID histogram summary file
        tmpHandle    = open( self.summaryFileName, 'w' )
        outputString = []
        outputString.append( '\n\nGood Read Placements:\n')
        outputString.append( ''.join(goodReadPlacements) )
        outputString.append( '\nReads Placing below %d%% ID, %d%% cov:\n'%( \
                                               self.ID_cutoff, self.covCutoff) )
        outputString.append( ''.join(belowCutoffReads) )

        # Write initial information to a file
        tmpHandle.write( ''.join( headerString) )
        tmpHandle.write( ''.join( outputString) )
        tmpHandle.close()
        
        return

    def sortPairPlacements(self):
        stdout.write( '\nRead Analysis:\n')
        # Initializing the unpaired set
        unpairedBACs = {}

        # Initializing the no_match set
        stdout.write( '\t-Pulling read names\n')
        BAC_name_set = set()
        BAC_name_set = BAC_name_set.union(\
                                        extractScaffoldNames(self.BAC_fileName))
        N_total_BACs = len( BAC_name_set )

        # Generating the assembly size histogram
        self.assemblySizeHistogram()

        # Counting Orphaned reads
        stdout.write( '\t-Counting orphaned reads\n')
        orphanedReads = {}
        for readName in BAC_name_set:
            canonicalName = readName.split(self.sepString)[0]
            try:
                orphanedReads[canonicalName] += 1
            except KeyError:
                orphanedReads[canonicalName] = 0
            #####
        #####
        N_orphanedReads   = 0
        orphanedReadNames = []
        for canonicalName, val in orphanedReads.items():
            if ( val == 0 ):
                N_orphanedReads += 1
                orphanedReadNames.append(canonicalName)
            elif ( val > 1 ):
                stdout.write( "WARNING:  Read name %s has %d reads associated with it\n"%(canonicalName,(val+1)) )
            #####
        #####
        BAC_name_set = None # Clearing the memory

        # Initializing the placement output lists
        belowCutoffReads        = []
        chimericReads           = []
        chimericSmallReads      = []
        tooLongReads            = []
        improperlyOrientedReads = []
        goodPairPlacements      = []

        # Initializing the counter class
        counterClass = localCounterClass()

        # Global insert sizes
        insertLengths = []

        # Setting up the plate statistics
        if ( self.plateStatistics ):
            plateStatistics = {}
            libNames = set()
        #####

        # Accumuating statistics
        stdout.write( '\t-Accumulating Pair Statistics...\n')
        for record_1 in iterBestHit( open(self.bestHitFileName,'r') ):

            # Pulling the library and plate number
            if ( self.plateStatistics ):
                libName, plateNum = self.libParser( record_1.BAC_recordName )[0]
                plateNum = int(plateNum)
            #####

            # Sorted records for use later in computing
            if ( self.BAC_placFilter(record_1) ):
                # Sort the placement
                try:
                    self.sortedPlacements[record_1.scaffold].addPlacement( \
                                                                       record_1)
                except KeyError:
                    self.sortedPlacements[record_1.scaffold] = scaffoldClass( \
                                                                     record_1, \
                                                                 self.sepString)
                #####
                # Increment the good placement counter
                counterClass.increment_goodPlacs()
                if ( self.plateStatistics ):
                    if ( libName in libNames ):
                        try:
                            plateStatistics[libName][plateNum].increment_goodPlacs()
                        except KeyError:
                            plateStatistics[libName][plateNum] = localCounterClass()
                            plateStatistics[libName][plateNum].increment_goodPlacs()
                        #####
                    else:
                        libNames.add( libName )
                        plateStatistics[libName] = {}
                        plateStatistics[libName][plateNum] = localCounterClass()
                        plateStatistics[libName][plateNum].increment_goodPlacs()
                    #####
                #####
                # Canonical Name
                canonicalName = record_1.BAC_recordName.split(self.sepString)[0]
                # Accumulating the unpaired BACs
                try:
                    record_2 = unpairedBACs[canonicalName]
                except KeyError:
                    # Add the name to the unpaired list and continue
                    unpairedBACs[canonicalName] = record_1
                    continue
                #####
                # Accumulating the statistics and screening length
                if ( self.goodPairTest(record_1, record_2) ):
                    BACdist = self.BAC_dist( record_1, record_2 )
                    if ( self.BAC_lengthFilter(BACdist) ):
                        # Adding the length to the insert length
                        insertLengths.append( BACdist )
                        counterClass.increment_goodPairs()
                        if ( self.plateStatistics ):
                            if ( libName in libNames ):
                                try:
                                    plateStatistics[libName][plateNum].increment_goodPairs()
                                except KeyError:
                                    plateStatistics[libName][plateNum] = localCounterClass()
                                    plateStatistics[libName][plateNum].increment_goodPairs()
                                #####
                            else:
                                libNames.add( libName )
                                plateStatistics[libName] = {}
                                plateStatistics[libName][plateNum] = localCounterClass()
                                plateStatistics[libName][plateNum].increment_goodPairs()
                            #####
                        #####
                        goodPairPlacements.append( self.pairString(record_1, \
                                                                     record_2) )
                        self.sortedPlacements[record_1.scaffold].addPair( \
                                                            record_1, record_2 )
                    else:
                        counterClass.increment_tooLongPair()
                        tooLongReads.append( self.pairString(record_1, \
                                                                     record_2) )
                    #####
                else:
                    # Determining improper orientation
                    if ( record_1.scaffold == record_2.scaffold ):
                        counterClass.increment_Improper()
                        improperlyOrientedReads.append( self.pairString( \
                                                           record_1, record_2) )
                    else:
                        # Checking to see if the chimeric pair are both in long
                        # scaffolds, or do they occur in short stubby scaffolds
                        if ( (self.scaffSizes[record_1.scaffold] > 100000) and \
                             (self.scaffSizes[record_2.scaffold] > 100000) ):
                            # CHIMERIC!!!
                            counterClass.increment_chimeric()
                            chimericReads.append( self.pairString(record_1, \
                                                                     record_2) )
                        else:
                            # CHIMERIC IN SHORT SCAFFOLD!!!
                            counterClass.increment_small_chimeric()
                            chimericSmallReads.append( self.pairString( \
                                                            record_1,record_2) )
                        #####
                    #####
                #####
                # Freeing the name from the unpaired set
                unpairedBACs.pop(canonicalName)
            else:
                # Counting reads that place below cutoff
                counterClass.increment_belowCutoff()
                if ( self.plateStatistics ):
                    if ( libName in libNames ):
                        try:
                            plateStatistics[libName][plateNum].increment_belowCutoff()
                        except KeyError:
                            plateStatistics[libName][plateNum] = localCounterClass()
                            plateStatistics[libName][plateNum].increment_belowCutoff()
                        #####
                    else:
                        libNames.add( libName )
                        plateStatistics[libName] = {}
                        plateStatistics[libName][plateNum] = localCounterClass()
                        plateStatistics[libName][plateNum].increment_belowCutoff()
                    #####
                #####
                belowCutoffReads.append( str(record_1) )
            #####
        #####
        
        # Keeping the memory overhead low
        unpairedBACs = None

        # Computing the total number of placements
        N_goodPlacs     = counterClass.goodPlacs
        N_belowCutoff   = counterClass.belowCutoff
        N_total_placs   = N_goodPlacs + N_belowCutoff
        N_didNotPlace   = N_total_BACs - N_total_placs

        # Computing the breakdown of placements
        N_pairedReads   = counterClass.goodPairs
        N_impOrient     = counterClass.N_improperlyOriented
        N_chimeric      = counterClass.N_chimeras
        N_small_chimera = counterClass.N_small_chimeras
        N_tooLongPair   = counterClass.tooLongPair
        N_unpairedReads = N_goodPlacs - ( N_pairedReads + N_impOrient + \
                                          N_chimeric    + N_small_chimera + \
                                                                  N_tooLongPair)
        N_total_pairs = N_goodPlacs - N_unpairedReads

        # Finding the natural bounds for the computations
        showInsertStats = True
        if ( insertLengths != [] ):
            tmpMinVal    = min( insertLengths )
            tmpMaxVal    = max( insertLengths )
            boundsFinder = histogramClass( tmpMinVal, tmpMaxVal, 10000 )
            map( boundsFinder.addData, insertLengths )
            newMinVal, newMaxVal = boundsFinder.findingBounds( 0.001 )
    
            # Downselecting the data to the new bounds
            newInsertLengths = filter( lambda x: ( newMaxVal >= x >= newMinVal ), \
                                                                     insertLengths )
            del insertLengths
    
            # Computing the average value
            N_pairs         = float( len( newInsertLengths ) )
            averageDistance = float( sum( newInsertLengths ) ) / N_pairs
    
            # Generating the standard deviation
            stdDev = 0.0
            for x in newInsertLengths:
                dx = float(x) - averageDistance
                stdDev += dx * dx
            #####
            stdDev = sqrt( stdDev / N_pairs )
    
            # Generating the histogram of insert sizes
            minBAC_length = newMinVal
            maxBAC_length = newMaxVal
            histClass = histogramClass( minBAC_length, maxBAC_length, 100 )
            map( histClass.addData, newInsertLengths )
        else:
            showInsertStats = False
        #####

        # Generating the header string
        cnst_1 = 100.0 / float(N_total_BACs)
        if ( showInsertStats ):
            pair_constant = 100.0 / N_total_pairs
        else:
            pair_constant = 0.0
        #####

        headerString = []
        headerString.append( '-------\n')
        headerString.append( 'Assembly File:  %s\n'%self.assemblyFile )
        headerString.append( 'Reads File:     %s\n'%self.BAC_fileName )
        headerString.append( 'Best Hit File:  %s\n'%self.bestHitFileName )
        headerString.append( '-------\n')
        headerString.append( '%d\tTotal Reads\n'%N_total_BACs )
        headerString.append( '%d\tOrphaned reads\n'%N_orphanedReads )
        headerString.append( '-------\nTotal Placement Summary:\n')
        tmpPer = cnst_1 * float(N_total_placs)
        headerString.append( '%d\t Total Placements (%.2f%%)\n'%(N_total_placs,tmpPer))
        tmpPer = cnst_1 * float(N_goodPlacs)
        headerString.append( '%d\t\tGood Placements(>=%d%% ID, >=%d%% cov) (%.2f%%)\n'%( \
                                                                  N_goodPlacs, \
                                                               self.ID_cutoff, \
                                                               self.covCutoff, \
                                                                       tmpPer) )
        tmpPer = cnst_1 * float(N_belowCutoff)
        headerString.append( '%d\t\tPlaced below Cutoff (%.2f%%)\n'%(\
                                                                N_belowCutoff, \
                                                                       tmpPer) )
        tmpPer = cnst_1 * float(N_didNotPlace)
        headerString.append( '%d\t Did not place (%.2f%%)\n'%(N_didNotPlace,tmpPer))
        
        headerString.append( '-------\n')
        tmpPer = cnst_1 * float(N_unpairedReads)
        headerString.append( '%d\t Unpaired reads (%.2f%%)\n'%(\
                                                              N_unpairedReads, \
                                                                       tmpPer) )
                                                                       
        headerString.append( '-------\nGood-Pair Placement Summary:\n')
        
                                                                       
        headerString.append( '%d Total paired reads\n'%(N_total_pairs) )
        
        tmpPer = pair_constant * float(N_pairedReads)
        headerString.append( '%d\t\tGood pairs (%.2f%%)\n'%(N_pairedReads, \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_chimeric)
        headerString.append( '%d\t\tChimeric paired reads (both scaffolds >100kb) (%.2f%%)\n'%(\
                                                                   N_chimeric, \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_small_chimera)
        headerString.append( '%d\t\tChimeric paired reads (1+ in scaffolds <100kb scaffolds) (%.2f%%)\n'%(\
                                                                N_small_chimera, \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_impOrient)
        headerString.append( '%d\t\tImproperly oriented paired reads (%.2f%%)\n'%(\
                                                                  N_impOrient, \
                                                                       tmpPer) )
        tmpPer = pair_constant * float(N_tooLongPair)
        tmpSize = int( float(self.maxInsertSize) / 1000.0 )
        headerString.append( '%d\t\tPaired reads with insert size >%dkb (%.2f%%)\n'%(\
                                                                N_tooLongPair, \
                                                                       tmpSize,\
                                                                       tmpPer) )

        if ( showInsertStats ):
            headerString.append( '-------\n')
            headerString.append( 'Mean Insert Size = %.2f\n'%averageDistance )
            headerString.append( 'STD Insert Size  = %.2f\n'%stdDev )
        #####
        headerString.append( '-------\n\n')

        # Outputting plate statistics
        if ( self.plateStatistics ):
            for libName, libPlates in plateStatistics.iteritems():
                # Sorting plate numbers
                DSU = []
                for plateNum, tmpClass in libPlates.iteritems():
                    DSU.append( (plateNum,tmpClass) )
                #####
                DSU.sort()
                # Generating the output
                headerString.append( '-------\n')
                headerString.append( 'library:\t%s\n'%libName )
                headerString.append( '\tPlate\t%GoodPairs\n' )
                headerString.append( '\t-----\t----------\n' )
                libHist = histogramClass( 0, 100, 100 )
                for plateNum, tmpClass in DSU:
                    perGoodPairs   = 100.0 * float(tmpClass.goodPairs) / 768.0
                    headerString.append( '\t%d\t%.2f%%\n'%(plateNum,  \
                                                               perGoodPairs, \
                                                                             ) )
                    libHist.addData(perGoodPairs)
                #####
                headerString.append( libHist.generateOutputString() )
            #####
            headerString.append( '-------\n')
        #####

        # Writing to the placement length and scaffID histogram summary file
        tmpHandle    = open( self.summaryFileName, 'w' )
        outputString = []
        if ( showInsertStats ):
            outputString.append( 'Histogram of Insert Sizes:\n' )
            outputString.append( '--------------------------\n' )
            outputString.append( histClass.generateOutputString() )
        #####
        
        outputString.append( '\n\nGood Pair Placements:\n')
        outputString.append( ''.join(goodPairPlacements) )
        outputString.append( '\nReads Placing below %d%% ID, %d%% cov:\n'%( \
                                               self.ID_cutoff, self.covCutoff) )
        outputString.append( ''.join(belowCutoffReads) )
        outputString.append( '\nChimeric Paired Reads (both placing in scaffolds >= 100kb):\n')
        outputString.append( ''.join(chimericReads) )
        outputString.append( '\nChimeric Paired Reads (1+ placing in a scaffold < 100kb):\n')
        outputString.append( ''.join(chimericSmallReads) )
        tmpSize = int( float(self.maxInsertSize) / 1000.0 )
        outputString.append( '\nProperly placed paired reads >%dkb from one another:\n'%(tmpSize))
        outputString.append( ''.join(tooLongReads) )
        outputString.append( '\nImproperly Oriented Paired Reads:\n')
        outputString.append( ''.join(improperlyOrientedReads) )
        outputString.append( '\nReads with no mate:\n' )
        outputString.append( '\n'.join(orphanedReadNames) )

        # Write initial information to a file
        tmpHandle.write( ''.join( headerString) )
        tmpHandle.write( ''.join( outputString) )
        tmpHandle.close()
        return

    def readString(self,record_1):
        return '%s\t%s\t%d\t%d\t%d\t%s\n'%( record_1.BAC_recordName, \
                                            record_1.scaffold, \
                                            record_1.scaffSize, \
                                            record_1.scaffStart, \
                                            record_1.scaffEnd, \
                                            record_1.placDir )

    def pairString(self,record_1,record_2):
        return '%s\t%s\t%d\t%d\t%d\t%s\t%s\t%s\t%d\t%d\t%d\t%s\n'%( \
                                                    record_1.BAC_recordName, \
                                                    record_1.scaffold, \
                                                    record_1.scaffSize, \
                                                    record_1.scaffStart, \
                                                    record_1.scaffEnd, \
                                                    record_1.placDir, \
                                                    record_2.BAC_recordName, \
                                                    record_2.scaffold, \
                                                    record_2.scaffSize, \
                                                    record_2.scaffStart, \
                                                    record_2.scaffEnd, \
                                                    record_2.placDir )

    def canonicalNameSeparator(self):
        # Obtaining one name
        BAC_name = None
        for record in iterBestHit(open(self.bestHitFileName,'r')):
            BAC_name = record.BAC_recordName
            break
        #####
        # Determining which separator is best
        for sepType,sepString in self.nameSeparators.items():
            splitName = parseString( BAC_name, sepString )
            if ( len(splitName) > 1 ):
                return sepType
            #####
        #####
        # If none is found, provide error message
        raise ValueError('Could not determine separator type for %s\n'%BAC_name)

    def goodPairTest(self, record_1, record_2):
        if ( record_1.scaffold == record_2.scaffold ):
            if ( record_1.scaffStart < record_2.scaffStart ):
                return (record_1.placDir == '+' and record_2.placDir == '-')
            else:
                return (record_2.placDir == '+' and record_1.placDir == '-')
            #####
        else:
            return False
        #####

    def BAC_dist(self, record_1, record_2):
        if ( record_1.scaffStart < record_2.scaffStart ):
            return abs( record_1.scaffStart - record_2.scaffEnd )
        else:
            return abs( record_2.scaffStart - record_1.scaffEnd )
        #####

#####

