__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from _helperFunctions import testFile, testDirectory, deleteDir, deleteFile

from _helperFunctions import generateTmpDirName, baseFileName, getFiles
 
from _helperFunctions import isGzipFile, isBzipFile
 
from _jobQueueing import remoteServer4
 
from _globalSettings import gzip_path

from os.path import join, abspath, curdir

from os import mkdir,system

from sys import stderr, stdout

import subprocess

import gzip

########################################################################
class batch_base_class(object):

    def __init__(self, fastaFile, \
                       nFastaFiles, \
                       outputFileName=None ):

        # Make sure the files exist
        testFile(fastaFile)

        # Reading in the control variables
        self.fastaFile         = fastaFile
        self.nFastaFiles       = nFastaFiles

        # Generating the directory information
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                     baseFileName(self.fastaFile) )

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def isBzipFile(self):
        try:
            import bz2
            x = bz2.BZ2File(self.fastaFile).readline()
            return True
        except IOError:
            pass
        #####
        return False
    
    def isGzipFile(self):
        try:
            import gzip
            x = gzip.GzipFile(self.fastaFile).readline()
            return True
        except IOError:
            return False
        #####
    
    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        
        if ( self.isGzipFile() ): 
            cmdString = 'cat %s | gunzip -c '%( self.fastaFile)
        elif ( self.isBzipFile() ): 
            cmdString = 'cat %s | bunzip2 -c '%( self.fastaFile)
        else:
            cmdString ='cat %s '%( self.fastaFile)
        #####
        
        recordN  = int((subprocess.Popen( '%s | grep -c ">"'%(cmdString), shell=True, stdout=subprocess.PIPE ).stdout).readline())
        perFile  = int(recordN/self.nFastaFiles) + 1
        awkFront = "awk -v n=1 -v outName=%s/tmp -v perFile=%d "%(self.tmpPath,perFile)
        awkPrint = "function myprint(s){ for(i=1; i <= length(s); i+=80){print substr(s,i,80)>outName n\".fasta\"}}"
        awkCMD   = "%s'BEGIN{RS=\">\"}; %s;{if(length($2)==0){NR=0; next}if(NR%%perFile==0){print \">\"$1>outName n\".fasta\";myprint($2);close(outName n\".fasta\");n++;next}{print \">\"$1>outName n\".fasta\";myprint($2)}}'"%(awkFront,awkPrint) 
        splitCMD = "%s|%s"%(cmdString,awkCMD)
        system(splitCMD)

        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

########################################################################
class batch_run_class( batch_base_class ):
    def __init__(self, queryFile, \
                       targetFile, \
                       nFastaFiles, \
                       clusterNum, \
                       outputFileName, \
                       formattedCommand ):

        ## Initializing the best hit base class
        batch_base_class.__init__( self, \
                                   queryFile, \
                                   nFastaFiles )
        
        # Reading in the information
        self.queryFile        = queryFile
        self.targetFile       = targetFile
        self.clusterNum       = clusterNum
        self.outputFileName   = outputFileName
        self.formattedCommand = formattedCommand
        
    def executeCases(self):
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Break the fasta file into little pieces
        self.splitFASTA()
        # Step 4:  BLAT Fastas
        self.sendToCluster()
        # Step 5: Parse Output
        self.parseOutput()
        # Step 7: Cleanup
        self.clean_tmp()
        return

    def sendToCluster(self):
    
        stderr.write( '\t-Aligning Reads\n')

        # (1)  Set up the command list
        commandList = []
        stringFormatDict = { 'target':self.targetFile, 'query':None, 'tmpOutput':None }
        for queryFile in getFiles( 'fasta', self.tmpPath ):
            testFile( queryFile )
            localOutputFile = '%s.align'%queryFile
            stringFormatDict['query'] = queryFile
            stringFormatDict['tmpOutput'] = localOutputFile
            commandList.append( self.formattedCommand % stringFormatDict )
            stderr.write(self.formattedCommand % stringFormatDict)
            stderr.write("\n")
        #####
        # (2)  Run the cases
        perlFile = 'batch_run.pl'
        remoteServer4( commandList, self.nFastaFiles, self.clusterNum, perlFile )
        deleteFile(perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        deleteFile(self.outputFileName)
        oh = open( self.outputFileName, 'w' )
        for alignFile in getFiles( 'align', self.tmpPath ):
            if ( isGzipFile(alignFile) ):
                tmpProcess = subprocess.Popen("gzip -f -d -c %s"%(alignFile), shell=True, stdout=subprocess.PIPE)
            elif ( isBzipFile(alignFile) ):
                tmpProcess = subprocess.Popen("bzip2 -f -d -c %s"%(alignFile), shell=True, stdout=subprocess.PIPE)
            else:
                tmpProcess = subprocess.Popen("cat %s"%(alignFile), shell=True, stdout=subprocess.PIPE)
            #####
            alignHandle = tmpProcess.stdout
            map( oh.write, alignHandle )
            tmpProcess.poll()
        #####
        oh.close()
        return

#==============================================================
def real_main():

    # Controlling parameters
    queryFASTA     = ''
    targetFASTA    = ''
    nFastaFiles    = 50
    clusterNum     = 101
    outputFileName = ''
    
    main_run_class( queryFASTA, \
                    targetFASTA, \
                    nFastaFiles, \
                    clusterNum, \
                    outputFileName, \
                    formattedCommand ).executeCases()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
