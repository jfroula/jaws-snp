
__author__  = "Jerry Jenkins"
__date__    = "$Nov 30, 2009 10:59:03 AM$"

# Python Library Imports
from time import asctime

from sys import stdout
from sys import stderr

##==============================================================
#class Bzip_class:
#    '''This class will take a string and create a file with that name.'''
#    def __init__(self,outFile,compVal=7):
#        import subprocess
#        self.proc  = subprocess.Popen("bzip2 -c -%d > %s"%(compVal,outFile), shell=True, stdin=subprocess.PIPE)
#    #####
#    def write(self, outString):
#        self.proc.stdin.write(outString)
#    #####
#    def close(self):
#        self.proc.kill()
#    #####
    
#==============================================================
class IntervalTree(object):

    __slots__ = ('intervals', 'left', 'right', 'center')

    def __init__(self, intervals, depth=32, minbucket=96, _extent=None, maxbucket=8192):
   
        depth -= 1
        if (depth == 0 or len(intervals) < minbucket) and len(intervals) > maxbucket:
            self.intervals = intervals
            self.left = self.right = None
            return 

        left, right = _extent or \
               (min(i.start for i in intervals), max(i.stop for i in intervals))
        center = (left + right) / 2.0
        
        self.intervals = []
        lefts, rights  = [], []

        for interval in intervals:
            if interval.stop < center:
                lefts.append(interval)
            elif interval.start > center:
                rights.append(interval)
            else: # overlapping.
                self.intervals.append(interval)
        
        self.left   = lefts  and IntervalTree(lefts,  depth, minbucket, (left,  center)) or None
        self.right  = rights and IntervalTree(rights, depth, minbucket, (center, right)) or None
        self.center = center
        
    def find(self, start, stop):
        """find all elements between (or overlapping) start and stop"""
        overlapping = [i for i in self.intervals if i.stop >= start and i.start <= stop]

        if self.left and start <= self.center:
            overlapping += self.left.find(start, stop)

        if self.right and stop >= self.center:
            overlapping += self.right.find(start, stop)

        return overlapping
        
#==============================================================
class IntervalClass(object):
    def __init__(self,start,stop,label):
        self.start = start
        self.stop  = stop
        self.label = label

#==================================
class negativeBreakerClass(object):
    
    def __init__(self,negBreakerFile):
        self.negBreakerFile = negBreakerFile
        self.unbroken_to_broken = {}
        self.broken_to_unbroken = {}
        self.mapFile()
    
    def mapFile(self):
        readLines = False
        for line in open(self.negBreakerFile):
            if ( readLines ):
                splitLine     = line.split(': ')
                unbrokenScaff = 'super_%s'%splitLine[0]
                scaffIDs      = ['super_%s'%scaffID for scaffID in splitLine[1].split(None)]
                self.unbroken_to_broken[unbrokenScaff] = []
                for scaffID in scaffIDs:
                    self.unbroken_to_broken[unbrokenScaff].append( scaffID )
                    self.broken_to_unbroken[scaffID] = unbrokenScaff
                #####
            else:
                if ( line.strip() == 'Super translations:' ):
                    readLines = True
                    continue
                #####
            #####
        #####
    
    def map_broken_to_unbroken(self,brokenID):
        return self.broken_to_unbroken[brokenID]

    def map_unbroken_to_broken(self,unbrokenID):
        return self.unbroken_to_broken[unbrokenID]

#=========================
class ESThit_File(object):

    def __init__(self,ESThit_file):
        self.ESThit_file = ESThit_file
        self.goodSequencesPerScaffold = {}
        self.chimericSequences        = set()
        self.invertedSequences        = set()
        self.notFoundSequences_gt50   = set()
        self.notFoundSequences_lt50   = set()

        self.parseSet = set( ['Number of good sequences per scaffold:', \
                              'List of chimeric sequences:', \
                              'List of inverted sequences:', \
                              'List of not found sequences that has coverage >= 50%:', \
                              'List of not found sequences that has coverage < 50%:'] )
        
        self.stateMap = {'Number of good sequences per scaffold:':'goodSequences', \
                         'List of chimeric sequences:':'chimeric', \
                         'List of inverted sequences:':'inverted', \
                         'List of not found sequences that has coverage >= 50%:':'notFound_gt50', \
                         'List of not found sequences that has coverage < 50%:':'notFound_lt50'}
        
        self.functionMap = {'goodSequences':self.parseGoodSequences, \
                            'chimeric':self.parseChimeric, \
                            'inverted':self.parseInverted, \
                            'notFound_gt50':self.parseNotFound_gt50, \
                            'notFound_lt50':self.parseNotFound_lt50, \
                            'noState':self.noState }
        
        self.parseFile()
    
    def noState(self,strippedLine):
        return
    
    def parseGoodSequences(self,strippedLine):
        splitLine = strippedLine.split(None)
        self.goodSequencesPerScaffold[splitLine[0]] = int(splitLine[1])
        return

    def parseChimeric(self,strippedLine):
        splitLine = strippedLine.split(None)
        self.chimericSequences.add(splitLine[0])
        return

    def parseInverted(self,strippedLine):
        splitLine = strippedLine.split(None)
        self.invertedSequences.add(splitLine[0])
        return

    def parseNotFound_gt50(self,strippedLine):
        splitLine = strippedLine.split(None)
        self.notFoundSequences_gt50.add(splitLine[0])
        return
        
    def parseNotFound_lt50(self,strippedLine):
        splitLine = strippedLine.split(None)
        self.notFoundSequences_lt50.add(splitLine[0])
        return
    
    def parseFile(self):
        currentState = 'noState'
        for line in open( self.ESThit_file, 'r' ):
            # Looking for a state change
            strippedLine = line.strip()
            if ( strippedLine in self.parseSet ):
                currentState = self.stateMap[strippedLine]
                continue
            #####
            # Parsing the line
            self.functionMap[currentState](strippedLine)
        #####
        return

########################################################################
def iterCounter( maxValue ):
    return newIterCounter(maxValue).next

def newIterCounter( maxValue ):
    # Commification
    import re
    commify_re = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
    commify = lambda tmpInt: commify_re( ',', '%d'%(tmpInt) )
    from math import fmod
    outputText = '%s reads processed\n'
    tmpCounter = 1
    readCutoff = 10
    while (True ):
        tmpCounter += 1
        if ( not int(fmod( tmpCounter, readCutoff )) ):
            stderr.write( outputText%commify( tmpCounter ) )
            readCutoff = min(10 * readCutoff, maxValue )
        #####
        yield
    #####
    return

########################################################################
class fileHeader_Class(object):
    
    """
        Generates file headers that include a time stamp.
    """
    
    def __init__(self, columnHeaders, offset, tabDelimited=False):
        self.columnHeaders  = columnHeaders
        self.offset         = offset
        self.strLen         = len(columnHeaders)
        self.additionalText = None
        self.tabDelimited   = tabDelimited
    
    def addText(self, inputString):
        self.additionalText = inputString
        return
    
    def generateHeaderString(self):
        
        ## Writing the header string ##
        text       = (self.strLen + 1) * ['']
        underscore = (self.strLen + 1) * ['']
        for n in xrange( self.strLen ):
            if ( self.tabDelimited ):
                text[n] = '%s\t'%self.columnHeaders[n]
                tempSize = len(self.columnHeaders[n])
                underscore[n] = '%s\t'%(tempSize*'-')
            else:
                text[n] = self.columnHeaders[n].ljust( self.offset[n] )
                tempSize = len(self.columnHeaders[n])
                underscore[n] = ( (tempSize*'-').ljust( self.offset[n] ) )
            #####
        #####
        text[self.strLen]       = '\n'
        underscore[self.strLen] = '\n'
        
        # Setting the time string
        timeString = '\nStart Time: %s\n\n'%asctime()
        
        if ( self.additionalText != None ):
            
            return ''.join( [ self.additionalText, \
                             timeString, \
                             ''.join(text), \
                             ''.join(underscore) ] )
        else:
            return ''.join( [ timeString, \
                             ''.join(text), \
                             ''.join(underscore) ] )
        #####
                
        return
#####

########################################################################
class histogramClass(object):
    
    """
    Used to compute histogram.
    """
    
    def __init__(self, minVal, maxVal, nBins):
    
        if ( minVal == maxVal ):
            raise ValueError( 'Minimum and Maximum Values are equal in histogram')
        #####
    
        self.minVal = minVal
        self.maxVal = maxVal
        self.nBins  = int(nBins)
        self.dx     = (self.maxVal - self.minVal) / float( self.nBins )
        
        self.initialize()
    
    def initialize(self):
    
        # Forming the header string
        self.strLen     = 6
        columnLabel     = self.strLen * ['']
        columnLabel[0]  = 'Midpoint'
        columnLabel[1]  = 'Counts'
        columnLabel[2]  = 'cumCounts'
        columnLabel[3]  = 'NormHist'
        columnLabel[4]  = 'cumHist'
        columnLabel[5]  = ''
        
        self.offset          = [15 for n in xrange(self.strLen)]
        
        self.headerString = fileHeader_Class( columnLabel, \
                                        self.offset).generateHeaderString()
        
        # Initializing the arrays    
        self.histogram = self.nBins * [0]
        self.cumCounts = self.nBins * [0]
        self.normHist  = self.nBins * [0.0]
        self.cumHist   = self.nBins * [0.0]
        self.height    = 0
        return
    
    def __repr__(self):
        return self.generateOutputString()
        
    def __str__(self):
        return self.generateOutputString()
    
    def selectBin(self, x_value):
        if x_value == self.maxVal: return self.nBins - 1
        else: return int(  ( x_value - self.minVal ) / self.dx )
    
    def addData(self, x_value):
        index = self.selectBin(x_value)
        try:
            self.histogram[index] += 1
        except IndexError:
            if ( index > self.nBins ):
                raise ValueError( '%f produced an index out of range: %d > %d\n'% (x_value, index, self.nBins) )
    
    def addExtent(self, extent):
        indices = range(self.selectBin(extent[0]), self.selectBin(extent[1]))
        if not indices: indices = [self.selectBin(extent[0])]
        for index in indices:
            try:
                self.histogram[index] += 1
            except IndexError:
                if ( index > self.nBins ):
                    raise ValueError( '%f produced an index out of range: %d > %d\n'% (extent[1], index, self.nBins) )


    def normalizeHist(self):
        if not self.histogram: return
        
        # Normalize Histogram
        totalArea     = sum([(self.dx*float(item)) for item in self.histogram])
        self.normHist = [float(item)/totalArea for item in self.histogram]
    
        ## Cumulative distribution ##
        self.cumCounts    = self.nBins * [0]
        self.cumCounts[0] = self.histogram[0]
        
        self.cumHist      = self.nBins * [0.0]
        self.cumHist[0]   = self.normHist[0] * self.dx
        
        self.height = float(self.histogram[0])
        for n in xrange( 1, self.nBins ):
            self.cumCounts[n] = self.cumCounts[n-1] + self.histogram[n]
            self.cumHist[n]   = self.cumHist[n-1] + self.normHist[n] * self.dx
            if self.histogram[n] > self.height: self.height = float(self.histogram[n])
        #####
        return
    
    def findingBounds(self, alpha):
        # Looping through to find a more natural upper and lower bound
        self.normalizeHist()
        lowerBound = alpha
        upperBound = 1.0 - alpha
        minVal = None
        maxVal = None
        noMinVal = True
        noMaxVal = True
        for n in xrange( self.nBins ):
            # Looking at the lower bound
            if ( noMinVal and (self.cumHist[n] > lowerBound)  ):
                if ( (n-1) > 0 ):
                    minVal = self.minVal + float(n-1) * self.dx
                else:
                    minVal = self.minVal
                #####
                noMinVal = False
            #####
            
            # Looking at the upper bound
            if ( noMaxVal and (self.cumHist[n] > upperBound) ):
                maxVal = self.minVal + float(n) * self.dx
                noMaxVal = False
                break
            #####
        #####
        return minVal, maxVal
    
    def generateGraphString(self, index, char, width=20):
        if ( self.histogram[index] == 0 ):
            tmpStr = '|'
        else:
            numVals = self.height > 0 and max(1, int(width*(self.histogram[index]/self.height))) or 0
            tmpStr = '|%s'%( char * numVals )
        #####
        return tmpStr.ljust(width+1)
    
    def generateOutputString(self):
        
        # Check to see if there is anything in the histogram
        if ( sum(self.histogram) == 0 ):
            return ''.join( [self.headerString, 'EMPTY HISTOGRAM\n\n'] )
        #####
        
        # Normalize data
        self.normalizeHist()

        # Generating the formatting statements
        formatting      = ['%12.4e' for n in xrange(self.strLen)]
        formatting[0]   = '%12.2f'
        formatting[1]   = '%d'
        formatting[2]   = '%d'
        formatting[5]   = '|%s'
        
        # Generating the output string
        outputString = self.nBins * ['']
        for n in xrange( self.nBins ):
            # Generating the string (midpoint value)
            x_val = self.minVal + float(n) * self.dx + 0.50*self.dx
            tempString    = (self.strLen+1) * ['']
            tempString[0] = (formatting[0] % x_val            ).strip().ljust(self.offset[0])
            tempString[1] = (formatting[1] % self.histogram[n]).strip().ljust(self.offset[1])
            tempString[2] = (formatting[2] % self.cumCounts[n]).strip().ljust(self.offset[2])
            tempString[3] = (formatting[3] % self.normHist[n] ).strip().ljust(self.offset[3])
            tempString[4] = (formatting[4] % self.cumHist[n]  ).strip().ljust(self.offset[4])
            tempString[5] =  self.generateGraphString(n, "#", width=50)
            tempString[6] = '\n'
            outputString[n] = ''.join( tempString )
        #####
    
        return ''.join( [self.headerString, ''.join( outputString )] )

    def generateOutputDict(self):
        
        # Check to see if there is anything in the histogram
        outputDict = { 'midpoint':[], 'counts':[], 'hist':[] }
        if ( sum(self.histogram) == 0 ):
            return outputDict
        else:
            self.normalizeHist()
        #####
        
        for n in xrange( self.nBins ):
            # Generating the string (midpoint value)
            x_val = self.minVal + float(n) * self.dx + 0.50*self.dx
            outputDict['midpoint'].append( x_val )
            outputDict['counts'].append( self.histogram[n] )
            outputDict['hist'].append( self.generateGraphString(n, "#", width=50) )
        #####
    
        return outputDict

#####

########################################################################
class weightedAverageClass(object):
    
    """
    Computes an average in the form of
    sum(x*y)/sum(y)
    """
    def __init__(self):
        self.sum_xy = 0.0
        self.sum_y  = 0.0
    
    def initializeValues(self):
        self.sum_xy = 0.0
        self.xum_y  = 0.0
        return
    
    def addValues(self, x_value, y_value):
        self.sum_xy += x_value * y_value
        self.sum_y  += y_value
        return
    
    def getWeightedAverage(self):
        if ( self.sum_y != 0.0 ):
            return float( self.sum_xy ) / float( self.sum_y )
        else:
            stdout.write( 'Current sum(y) is zero.' )
            return None
    #####
#####

