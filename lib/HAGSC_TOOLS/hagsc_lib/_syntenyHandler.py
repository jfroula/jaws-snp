
__author__="jjenkins"
__date__ ="$Mar 14, 2010 11:13:57 PM$"

from _helperClasses import fileHeader_Class

from _helperFunctions import deleteFile

from _possibleJoin_call import getPossibleJoinFormat

from sys import stdout

########################################################################
class syntenyEntry(object):
    
    def __init__(self, scaffID, scaffSize, scaffStart, scaffEnd, placDir, \
                       geneID, chrID, chrStart, chrEnd ):
        # Assembly Info
        self.scaffID    = scaffID
        self.scaffSize  = scaffSize
        self.scaffStart = scaffStart
        self.scaffEnd   = scaffEnd
        self.placDir    = placDir
        
        # synteny identifiers
        self.geneID   = geneID
        self.chrID    = chrID
        self.chrStart = chrStart
        self.chrEnd   = chrEnd

        self.attributes = ['scaffID', 'scaffSize', 'scaffStart', 'scaffEnd', \
                           'placDir', 'geneID', 'chrID', 'chrStart', 'chrEnd']

    def generateString(self):
        # Building the string
        columnLabel, offset, formatting = getSyntenicFormatting()
        strLen     = len(offset)
        tmpData    = strLen * ['']
        tmpData[0] = self.scaffID
        tmpData[1] = self.scaffSize
        tmpData[2] = self.scaffStart
        tmpData[3] = self.scaffEnd
        tmpData[4] = self.placDir
        tmpData[5] = self.geneID
        tmpData[6] = self.chrID
        tmpData[7] = self.chrStart
        tmpData[8] = self.chrEnd

        # Building the string
        outputString = (strLen + 1) * ['']
        for n in xrange( strLen ):
            outputString[n] = ( formatting[n] % \
                                getattr( self, self.attributes[n] ) \
                              ).ljust( offset[n] )
        #####
        outputString[strLen] = '\n'
        return ''.join( outputString )


########################################################################
def getSyntenicFormatting():
    strLen          = 9
    columnLabel     = strLen * ['']
    columnLabel[0]  = 'Scaff'
    columnLabel[1]  = 'Size'
    columnLabel[2]  = 'Start'
    columnLabel[3]  = 'End'
    columnLabel[4]  = 'Dir'
    columnLabel[5]  = 'Gene_id'
    columnLabel[6]  = 'Chromo'
    columnLabel[7]  = 'Start'
    columnLabel[8]  = 'End'

    offset    = [15 for n in xrange(strLen)]
    offset[0] = 25  # scaffID
    offset[5] = 20  # geneID

    formatting      = ['%s' for n in xrange(strLen)]
    formatting[1]   = '%d' # size
    formatting[2]   = '%d' # start
    formatting[3]   = '%d' # end
    formatting[7]   = '%d' # start
    formatting[8]   = '%d' # end

    return columnLabel, offset, formatting


########################################################################
def getJoinsFileName( fileBase ):
    return '%s_joins.dat'%fileBase

########################################################################
def getFrequencyFileName( fileBase ):
    return '%s_freq.dat'%fileBase

########################################################################
def writeSyntenyFile( synEntries, synFileName ):
    # Quick checks of input
    if ( not (type(synEntries) == type([])) ):
        raise ValueError('synEntries must be of type LIST')

    # Sorting scaffold ID's by size
    scaffoldSizes_dict = {}
    decoratedScaff     = {}
    for synClass in synEntries:
        scaffID   = synClass.scaffID
        scaffSize = synClass.scaffSize
        scaffoldSizes_dict[scaffID] = scaffSize
        try:
            decoratedScaff[scaffID].append( (synClass.scaffStart, synClass) )
        except KeyError:
            decoratedScaff[scaffID] = [(synClass.scaffStart, synClass)]
        #####
    #####
    DSU = [(value, key) for key, value in scaffoldSizes_dict.items()]
    DSU.sort(reverse=True)

    # Formatting Information
    columnLabel, offset, formatting = getSyntenicFormatting()

    # Generating the file header
    deleteFile(synFileName)
    fileHeader = fileHeader_Class( columnLabel, offset )
    synHandle = open( synFileName, 'w' )
    synHandle.write( fileHeader.generateHeaderString() )

    # Write the file
    sepString = ''.join( [ 20*'-', '\n' ] )
    stdout.write( 'Writing %s\n'%synFileName)
    for scaffSize, scaffID in DSU:
        # Writing output string to file
        placList = decoratedScaff[scaffID]
        placList.sort()
        outputList = [synClass[1].generateString() for synClass in placList]
        synHandle.write( ''.join(outputList) )
        synHandle.write( sepString )
    #####
    synHandle.close()
    return
#####

########################################################################
def iterSyntenyFile( fileHandle ):
    """
    iterSyntenyFile( fileHandle )

    Generator function to iterate over entries in a synteny file, returning
    syntenyEntry objects.
    """

    for line in fileHandle:

        parsedLine = line.split(None)

        # Skipping lines
        if ( len(parsedLine) > 8 ):
            if ( parsedLine[0] == 'Scaff' ): continue
            if ( parsedLine[0][0] == '-' ): continue
        else:
            continue
        #####

        scaffID    = parsedLine[0]
        scaffSize  = int(parsedLine[1])
        scaffStart = int(parsedLine[2])
        scaffEnd   = int(parsedLine[3])
        placDir    = parsedLine[4]
        geneID     = parsedLine[5]
        chrID      = parsedLine[6]
        chrStart   = int(parsedLine[7])
        chrEnd     = int(parsedLine[8])

        # Yield up the record and wait for next iteration
        yield syntenyEntry( scaffID, scaffSize, scaffStart, scaffEnd, placDir, \
                            geneID, chrID, chrStart, chrEnd )
    #####

    # Completed reading the file
    return

#####

########################################################################
class syntenyData_class(object):
    def __init__(self):
        self.placements  = []
        self.DSU         = []

        self.headSynteny = None
        self.tailSynteny = None

        self.headStart = None
        self.tailStart = None

        self.scaffID = None

    def addPlacement(self, syntenyEntryClass):
        if ( self.scaffID == None ):
            self.scaffID = syntenyEntryClass.scaffID
        else:
            if ( self.scaffID != syntenyEntryClass.scaffID ):
                raise ValueError( 'ScaffoldID mismatch\n %s %s\n\n'%( \
                                       self.scaffID, syntenyEntryClass.scaffID))
            #####
        #####
        self.placements.append( syntenyEntryClass )

    def assignSynteny(self, nBuffer=5):
        # Sorting synteny by scaffold start
        self.DSU = []
        for plac in self.placements:
            self.DSU.append( ( plac.scaffStart, plac ) )
        #####
        self.DSU.sort()

        if ( self.DSU != [] ):

            # Looking at the first and last 'nBuffer' placememts to determine
            # synteny
            nPlacs  = len(self.DSU)

            #-----------------------
            # Assigning head synteny
            #-----------------------
            hitDict = {}
            lower   = 0
            upper   = min( nPlacs, nBuffer )
            for m in xrange( lower, upper ):
                try:
                    hitDict[self.DSU[m][1].chrID] += 1
                except KeyError:
                    hitDict[self.DSU[m][1].chrID] = 1
                #####
            #####
            DSU_freq = [(value,key) for key,value in hitDict.items()]
            DSU_freq.sort( reverse=True )

            # Check for concensus synteny
            if ( len(DSU_freq) == 1 ):
                # If there is only one, then return
                self.headSynteny = DSU_freq[0][1]
            elif ( DSU_freq[0][0] > DSU_freq[1][0] ):
                # If there is concensus, then return
                self.headSynteny = DSU_freq[0][1]
            else:
                # If there is no clear concensus, then return empty
                self.headSynteny = None
            #####

            # Assigning the start value
            if ( self.headSynteny != None ):
                self.headStart = self.DSU[0][1].chrStart
            #####

            #-----------------------
            # Assigning tail synteny
            #-----------------------
            hitDict = {}
            lower = max( 0, (nPlacs-nBuffer) )
            upper = nPlacs
            for m in xrange( lower, upper ):
                try:
                    hitDict[self.DSU[m][1].chrID] += 1
                except KeyError:
                    hitDict[self.DSU[m][1].chrID] = 1
                #####
            #####
            DSU_freq = [(value,key) for key,value in hitDict.items()]
            DSU_freq.sort( reverse=True )

            # Check for concensus synteny
            if ( len(DSU_freq) == 1 ):
                # If there is only one, then return
                self.tailSynteny = DSU_freq[0][1]
            elif ( DSU_freq[0][0] > DSU_freq[1][0] ):
                # If there is concensus, then return
                self.tailSynteny = DSU_freq[0][1]
            else:
                # If there is no clear concensus, then return empty
                self.tailSynteny = None
            #####

            if ( self.tailSynteny != None ):
                self.tailStart = self.DSU[-1][1].chrStart
            #####

            return

        #####

    def getSynteny(self, scaffStartTemp, scaffSize):
        # Retrieving head synteny information
        if ( scaffStartTemp < (scaffSize/2) ):
            return self.headSynteny
        else:
            return self.tailSynteny
        #####
    #####

#####

########################################################################
def findSyntenicJoins( synEntries, possibleJoins, outputFileBase, \
                                                           synIgnoreList = [] ):

    # Create searchable structure with synteny
    synScaffoldData = {}
    for item in synEntries:
        scaffID = item.scaffID
        try:
            synScaffoldData[scaffID].addPlacement( item )
        except KeyError:
            synScaffoldData[scaffID] = syntenyData_class()
            synScaffoldData[scaffID].addPlacement( item )
        #####
    #####

    # Assign Synteny
    unclearFileHandle = open( 'ambiguousSynteny.out','w' )
    assignedSynteny   = open( 'assignedSynteny.out', 'w' )
    for scaffID in synScaffoldData.keys():
        synScaffoldData[scaffID].assignSynteny()
        if ( synScaffoldData[scaffID].headSynteny == None ):
            unclearFileHandle.write( 'Scaffold HEAD: %s\n'%scaffID )
            unclearFileHandle.write( repr(synScaffoldData[scaffID].DSU) )
            unclearFileHandle.write( '\n-----------------\n' )
        else:
            assignedSynteny.write( '%s\n'%scaffID )
            assignedSynteny.write( '\tHEAD:  %s\n'%synScaffoldData[scaffID].headSynteny )
        #####
        if ( synScaffoldData[scaffID].tailSynteny == None ):
            unclearFileHandle.write( 'Scaffold TAIL: %s\n'%scaffID )
            unclearFileHandle.write( repr(synScaffoldData[scaffID].DSU) )
            unclearFileHandle.write( '\n-----------------\n' )
        else:
            assignedSynteny.write( '\tTAIL:  %s\n'%synScaffoldData[scaffID].tailSynteny )
            assignedSynteny.write( '-----------------------\n' )
        #####
    #####
    unclearFileHandle.close()
    assignedSynteny.close()

    # Sorting possible joins and scaffold ID's by size
    sortedPossibleJoins = {}
    scaffoldSizes       = {}
    for item in possibleJoins:
        scaffID    = item.placs[0].scaff
        scaffSize  = item.placs[0].scaffSize
        scaffStart = item.placs[0].scaffStart
        try:
            sortedPossibleJoins[scaffID].append( (scaffStart, item) )
        except KeyError:
            sortedPossibleJoins[scaffID] = [ (scaffStart, item) ]
        #####
        scaffoldSizes[scaffID] = scaffSize
    #####
    DSU = [ (value,key) for key,value in scaffoldSizes.items() ]
    DSU.sort( reverse = True )

    # Querying synteny with possibleJoins
    dataContainer = {}
    frequencyList = {}
    for scaffSize, scaff_id in DSU:
        # Sorting join list
        placList = sortedPossibleJoins[scaff_id]
        placList.sort()
        # Querying synteny
        for scaffStart, joinClass in placList:
            try:
                scaffID_1    = joinClass.placs[0].scaff
                scaffStart_1 = joinClass.placs[0].scaffStart
                scaffSize_1  = joinClass.placs[0].scaffSize
                syn_1 = synScaffoldData[scaffID_1].getSynteny(scaffStart_1,\
                                                                    scaffSize_1)

                scaffID_2    = joinClass.placs[1].scaff
                scaffStart_2 = joinClass.placs[1].scaffStart
                scaffSize_2  = joinClass.placs[1].scaffSize
                syn_2  = synScaffoldData[scaffID_2].getSynteny(scaffStart_2, \
                                                                    scaffSize_2)

                # Do we have shared synteny?
                if ( syn_1 == syn_2 ):

                    # Checking to see if synteny is in the ignoreList
                    if ( syn_1 in synIgnoreList ): continue

                    # Adding synteny to the line
                    line = joinClass.generateString()
                    line = ''.join( [line[:-1],('%s'%(syn_1)).ljust(10),'\n'] )

                    # Storing the line by scaffold id
                    try:
                        dataContainer[scaffID_1].append( line )
                    except KeyError:
                        dataContainer[scaffID_1] = [line]
                    #####

                    # Storing the pairing frequency information
                    splitLine = line.split(None)
                    idList    = [splitLine[1],splitLine[6]]
                    uniqueID  = '%s_|_%s'%(idList[0],idList[1])
                    try:
                        frequencyList[uniqueID].add( line )
                    except KeyError:
                        frequencyList[uniqueID] = set([line])
                    #####

                #####
            except KeyError:
                pass
            #####
        #####
    #####

    # Add synteny to the output file
    outputHandle = open( getJoinsFileName( outputFileBase ), 'w' )
    columnLabel, offset, formatting = getPossibleJoinFormat()
    columnLabel.append( 'Synteny' )
    offset.append( 10 )

    # Output the syntenic joins
    fileHeader = fileHeader_Class( columnLabel, offset )
    outputHandle.write( fileHeader.generateHeaderString() )
    sepString = ''.join( [ 20*'-', '\n' ] )
    for tmpNum, scaffID in DSU:
        outputList = []
        writeList  = True
        try:
            for line in dataContainer[scaffID]: outputList.append(line)
            outputList.append( sepString )
        except KeyError:
            writeList = False
        #####
        if ( writeList ): outputHandle.write( ''.join(outputList) )
    #####
    outputHandle.close()

    # Output the syntenic join frequency file
    outputHandle = open( getFrequencyFileName( outputFileBase ), 'w' )
    outputHandle.write( fileHeader.generateHeaderString() )

    # Preprocessing frequency information
    tempContainer = {}
    for ID, lineList in frequencyList.items():
        for line in lineList:
            try:
                tempContainer[ID].add( line )
            except KeyError:
                tempContainer[ID] = set([line])
            #####
        #####
    #####
    DSU = [(len(tmpList),ID) for ID, tmpList in tempContainer.items()]
    DSU.sort(reverse=True)

    # Outputting the most frequent first
    screenIDs = set()
    counter = 1
    for n in xrange(0,len(DSU)):

        # Reading in the globbed ID
        tmpNum, tmpID = DSU[n]

        # Getting a unique ID
        split_id = tmpID.split('_|_')
        split_id.sort()
        uniqueID = '%s%s'%(split_id[0],split_id[1])

        if ( uniqueID not in screenIDs ):

            # Writing number of joins
            outputHandle.write( '%d Joins for join #%d\n'%(tmpNum, counter))
            counter += 1

            # Sorting the output
            DSU_2 = []
            for line in tempContainer[tmpID]:
                parsedLine = line.split(None)
                scaffStart = int(parsedLine[4])
                DSU_2.append( (scaffStart,line) )
            #####
            DSU_2.sort()

            # Write lines
            for tmpSize, line in DSU_2: outputHandle.write( line )

            # Writing the Synteny Information
            for scaff_ID in split_id:

                # Sorting the synteny objects by scaffold start
                DSU_3 = [(item.scaffStart, item) for item in synEntries \
                                                  if (item.scaffID == scaff_ID)]
                DSU_3.sort()
                nItems = len(DSU_3)

                if ( nItems < 50 ):
                    # Writing the first 25 lines of the head
                    outputHandle.write( '\nScaffold: %s\n'%scaff_ID )
                    for m in xrange( 0, nItems ):
                        outputHandle.write( DSU_3[m][1].generateString() )
                    #####
                else:

                    # Writing the first 25 lines of the head
                    outputHandle.write( '\nHead:  %s\n'%scaff_ID )
                    for m in xrange( 0, min( 25, nItems ) ):
                        outputHandle.write( DSU_3[m][1].generateString() )
                    #####

                    # Writing the last 25 lines of the tail
                    outputHandle.write( '\nTail:  %s\n'%scaff_ID )
                    for m in xrange( max(0,(nItems-25)), nItems ):
                        outputHandle.write( DSU_3[m][1].generateString() )
                    #####

                #####

            #####

            # Write separator string
            outputHandle.write( sepString )

            # Adding the ID to the screen IDs
            screenIDs.add( uniqueID )
        #####
    #####
    outputHandle.close()

    return
