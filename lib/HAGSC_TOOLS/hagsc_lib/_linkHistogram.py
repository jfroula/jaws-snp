__author__="jjenkins"
__date__ ="$Jul 21, 2010 8:31:13 AM$"

from _helperFunctions import iterTextFile
from _helperClasses import histogramClass

from sys import stdout

class assemblyLinkHistogram( object ):

    def __init__(self, linkFile, numBins=500):
        self.linkFile = linkFile
        self.numBins  = numBins
        self.generateHistogram()

    def generateHistogram(self):
        tmpHandle = open( self.linkFile, 'r' )
        tmpHandle.readline()
        linkList = []
        for parsedLine in iterTextFile( tmpHandle, '\t' ):
            linkList.append( int(parsedLine[6]) )
        #####
        tmpHist = histogramClass( min(linkList), max(linkList), self.numBins )
        map( tmpHist.addData, linkList )
        stdout.write( tmpHist.generateOutputString() )
        return


if ( __name__ == '__main__' ):

    linkFile = '/mnt/raid0/disk33/WGS/mim8x/assem4/assembly.links'

    assemblyLinkHistogram( linkFile )

