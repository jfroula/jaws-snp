
"""
Contains global settings that are unique to the HudsonAlpha Institute for
Biotechnology Genome Sequencing Center.
"""

__author__ = 'jjenkins'
__date__   = '$Feb 24, 2010 2:06:27 PM$'

from string import maketrans

# Locations of system dependent executables
bestHit_path  = r'/mnt/raid2/SEQ/bin/best_hit.pl'
blastall_path = r'/mnt/local/EXBIN/blastall'
blatEXE       = r'source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/BLAT_ENV/;blat'

zcat_path     = r'/usr/bin/zcat'
gzip_path     = r'/usr/bin/gzip'

mask_repeats_hash_path = r'/mnt/local/EXBIN/mask_repeats_hash'

# BestHit settings
defaultCluster = 101
defaultNumCPUs = 4

# Nucleotide Mapping for reverse complementing DNA
translationTable = maketrans("ATCG", "TAGC")
nucMap = { 'A':'T', \
           'T':'A', \
           'C':'G', \
           'G':'C', \
           'X':'X', \
           'N':'N', \
           'a':'t', \
           't':'a', \
           'c':'g', \
           'g':'c', \
           'x':'x', \
           'n':'n'  }
