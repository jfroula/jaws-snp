
__author__="jjenkins"
__date__ ="$Jan 13, 2010 5:02:27 PM$"

## Python Library Imports ##
from xml.etree import cElementTree

########################################################################
class hsp_Class(object):

    def __init__(self):
        # dir()-ing the hsp class returns the following
        # ---------------------------------------------
        # ['align_length', 'bits', 'expect', 'frame',
        # 'gaps', 'identities', 'match', 'num_alignments',
        # 'positives', 'query', 'query_end', 'query_start',
        # 'sbjct', 'sbjct_end', 'sbjct_start', 'score', 'strand']
        self.align_length   = None
        self.bits           = None
        self.expect         = None
        self.gaps           = None
        self.identities     = None
        self.match          = None
        self.num_alignments = None
        self.positives      = None
        self.query          = None
        self.query_end      = None
        self.query_frame    = None
        self.query_start    = None
        self.sbjct          = None
        self.sbjct_end      = None
        self.sbjct_frame    = None
        self.sbjct_start    = None
        self.score          = None
    
    def strand(self):
        # Direction of alignment of the query sequence on the target
        dq = self.query_end - self.query_start
        dt = self.sbjct_end - self.sbjct_start
        if ( dq * dt < 0 ): return '-'
        return '+'

#####

########################################################################
class alignment_Class(object):

    def __init__(self):
        # dir()-ing the alignment class returns the following #
        # ['__doc__', '__init__', '__module__', '__str__', 'accession', 'hit_def',
        # 'hit_id', 'hsps', 'length', 'title']
        self.accession = None
        self.hit_def   = None
        self.hit_id    = None
        self.length    = None
        self.title     = None
        self.hsps      = []

    def add_HSP(self):
        self.hsps.append( hsp_Class() )
        return

#####

########################################################################
class blastRecord_Class(object):

    def __init__(self):
        # dir()-ing the blast_record class returns the following
#        ['__doc__', '__init__', '__module__', 'alignments', 'application', 'blast_cutoff',
#        'database', 'database_length', 'database_letters', 'database_name', 'database_sequences',
#        'date', 'descriptions', 'dropoff_1st_pass', 'effective_database_length',
#        'effective_hsp_length', 'effective_query_length', 'effective_search_space',
#        'effective_search_space_used', 'expect', 'filter', 'frameshift', 'gap_penalties',
#        'gap_trigger', 'gap_x_dropoff', 'gap_x_dropoff_final', 'gapped', 'hsps_gapped',
#        'hsps_no_gap', 'hsps_prelim_gapped', 'hsps_prelim_gapped_attemped', 'ka_params',
#        'ka_params_gap', 'matrix', 'multiple_alignment', 'num_good_extends', 'num_hits',
#        'num_letters_in_database', 'num_seqs_better_e', 'num_sequences', 'num_sequences_in_database',
#        'posted_date', 'query', 'query_id', 'query_length', 'query_letters', 'reference', 'sc_match',
#        'sc_mismatch', 'threshold', 'version', 'window_size']

        # Currently used
        self.query         = None
        self.query_id      = None
        self.query_length  = None
        self.query_letters = None
        self.alignments    = []

        # Not currently reading these in
        self.application        = None
        self.blast_cutoff       = None
        self.database           = None
        self.database_length    = None
        self.database_letters   = None
        self.database_name      = None
        self.database_sequences = None
        self.date               = None
        self.descriptions       = None

        self.dropoff_1st_pass               = None
        self.effective_database_length      = None
        self.effective_hsp_length           = None
        self.effective_query_length         = None
        self.effective_search_space         = None
        self.effective_search_space_used    = None

        self.expect              = None
        self.filter              = None
        self.frameshift          = None
        self.gap_penalties       = None
        self.gap_trigger         = None
        self.gap_x_dropoff       = None
        self.gap_x_dropoff_final = None

        self.gapped                      = None
        self.hsps_gapped                 = None
        self.hsps_no_gap                 = None
        self.hsps_prelim_gapped          = None
        self.hsps_prelim_gapped_attemped = None

        self.ka_params                 = None
        self.ka_params_gap             = None
        self.matrix                    = None
        self.multiple_alignment        = None
        self.num_good_extends          = None
        self.num_hits                  = None
        self.num_letters_in_database   = None
        self.num_seqs_better_e         = None
        self.num_sequences             = None
        self.num_sequences_in_database = None
        self.posted_date               = None
        self.reference                 = None
        self.sc_match                  = None
        self.sc_mismatch               = None
        self.threshold                 = None
        self.version                   = None
        self.window_size               = None

    #####

    def add_Alignment(self):
        self.alignments.append( alignment_Class() )
        return

#####

########################################################################
class parse_XML(object):

    def __init__(self):

        # Initializing the blast record
        self.blast_record = None

        # Setting reader toggle mapping
        self.functionMap = { 'Iteration':{'start':self.iterationToggle, \
                                          'end'  :self.yieldToggle}, \
                             'Iteration_hits':{'start': self.iterHitToggle, \
                                               'end'  : self.iterHitToggle}, \
                             'Hit':{'start': self.alignmentToggle, \
                                    'end'  : False}, \
                             'Hsp':{'start': self.hspToggle, \
                                    'end'  : self.hspToggle}, \
                            }

        # Using sets enables faster searches
        self.elementSet = set( self.functionMap.keys() )

        # Setting the XML to object property mappings
        self.blastRecordHeaderMap = { \
                            'Iteration_query-def' : [ 'query',        False ], \
                            'Iteration_query-ID'  : [ 'query_id',     False ], \
                            'Iteration_query-len' : [ 'query_length', int   ], \
                                     }

        self.alignmentHeaderMap = {'Hit_accession' : ['accession', int   ], \
                                   'Hit_def'       : ['hit_def',   False ], \
                                   'Hit_id'        : ['hit_id',    False ], \
                                   'Hit_len'       : ['length',    int   ] \
                                   }

        self.hspMap = {'Hsp_align-len'   : ['align_length', int   ], \
                       'Hsp_bit-score'   : ['bits',         float ], \
                       'Hsp_evalue'      : ['expect',       float ], \
                       'Hsp_query-frame' : ['query_frame',  int   ], \
                       'Hsp_hit-frame'   : ['sbjct_frame',  int   ], \
                       'Hsp_gaps'        : ['gaps',         int   ], \
                       'Hsp_identity'    : ['identities',   int   ], \
                       'Hsp_midline'     : ['match',        False ], \
                       'Hsp_positive'    : ['positives',    int   ], \
                       'Hsp_qseq'        : ['query',        False ], \
                       'Hsp_query-to'    : ['query_end',    int   ], \
                       'Hsp_query-from'  : ['query_start',  int   ], \
                       'Hsp_hseq'        : ['sbjct',        False ], \
                       'Hsp_hit-to'      : ['sbjct_end',    int   ], \
                       'Hsp_hit-from'    : ['sbjct_start',  int   ], \
                       'Hsp_score'       : ['score',        int   ], \
                       }

        # Initializing toggles
        self.readBlastRecordHeader = False
        self.readAlignmentHeader   = False
        self.readAlignments        = False
        self.readHSP               = False
        self.yieldingBlastRecord   = False

        return

    #####

    def iterationToggle(self):
        self.readBlastRecordHeader = not self.readBlastRecordHeader

        # Initializing the blast class
        if ( self.readBlastRecordHeader ):
            self.blast_record = None
            self.blast_record = blastRecord_Class()

            # Turn everything else off
            self.readAlignmentHeader   = False
            self.readAlignments        = False
            self.readHSP               = False
        #####

    def iterHitToggle(self):
        self.readAlignmentHeader = not self.readAlignmentHeader

        # Setting the other states explicitly
        if ( self.readAlignmentHeader ):
            self.readBlastRecordHeader = False
            self.readAlignments        = False
            self.readHSP               = False
        #####

    def alignmentToggle(self):
        self.readAlignments = not self.readAlignments

        # Adding another alignment object
        if ( self.readAlignments ):
            self.blast_record.add_Alignment()
            # Setting the other states explicitly
            self.readAlignmentHeader   = True
            self.readBlastRecordHeader = False
            self.readHSP               = False
        #####

    def hspToggle(self):
        self.readHSP = not self.readHSP

        if ( self.readHSP ): self.blast_record.alignments[-1].add_HSP()

        # Setting the other states explicitly
        self.readBlastRecordHeader = False
        self.readAlignmentHeader   = False
        self.readAlignments        = False

    def yieldToggle(self):
        self.yieldingBlastRecord = not self.yieldingBlastRecord
        return

    def parse(self, blastFileHandle):

        # Checking the input
        if isinstance( blastFileHandle, basestring ):
            raise TypeError( "Need a file handle, not a string " + \
                             "(i.e. not a filename)" )
        #####

        # Creating the iterator
        self.cTreeParser = cElementTree.iterparse( blastFileHandle, \
                                                       events=('start', 'end') )

        # Initializing the triggers
        self.readBlastRecordHeader = False
        self.readAlignmentHeader   = False
        self.readAlignments        = False
        self.readHSP               = False
        self.yieldingBlastRecord   = False

        # Looping over the blast RecordIterators
        conver = False
        attr   = None
        for event, elem in self.cTreeParser:

            # Anything to work with?
            if ( elem.text != None ):

                try:

                    # Reading Blast Record Header
                    anythingHappening = False
                    if ( self.readBlastRecordHeader ):

                        attr   = self.blastRecordHeaderMap[elem.tag][0]
                        conver = self.blastRecordHeaderMap[elem.tag][1]
                        anythingHappening = True

                    elif( self.readAlignmentHeader and self.readAlignments ):

                        attr   = self.alignmentHeaderMap[elem.tag][0]
                        conver = self.alignmentHeaderMap[elem.tag][1]
                        anythingHappening = True

                    elif ( self.readHSP ):

                        attr   = self.hspMap[elem.tag][0]
                        conver = self.hspMap[elem.tag][1]
                        anythingHappening = True

                    #####

                    if ( anythingHappening ):

                        # Performing the conversion on the values
                        value = ( conver and [conver( elem.text )] or \
                                  [elem.text])[0]

                        # Applying the attributes
                        if ( self.readBlastRecordHeader ):

                            setattr( self.blast_record, attr, value )

                        elif( self.readAlignmentHeader and self.readAlignments ):

                            setattr( self.blast_record.alignments[-1], attr, value )

                        elif ( self.readHSP ):

                            setattr( self.blast_record.alignments[-1].hsps[-1], \
                                                                   attr, value )

                        #####

                        # Resetting the conversion values
                        conver = False
                        attr   = None

                    #####

                except KeyError:

                    pass

                #####

            #####

            if ( elem.tag in self.elementSet ):

                # Triggering the event
                if ( self.functionMap[elem.tag][event] ):
                    self.functionMap[elem.tag][event]()
                #####

                # Yielding the blast record when done
                if ( self.yieldingBlastRecord ):
                    self.yieldToggle()
                    yield self.blast_record
                #####

            #####

            ## Clearing the element ##
            elem.clear()

        #####
        # Clearing the parser when finished
        self.cTreeParser = None
    #####
#####
