
from _FASTA_IO import generateTrimmedFileName
from _FASTA_IO import trimAssemblyFile

from _helperFunctions import testFile
from _helperFunctions import createBestHitFileName

from _bestHit import DNA_best_hit
from _bestHit import iterBestHit

from os.path import split
from os.path import splitext

from sys import stdout

__author__="jjenkins"
__date__ ="$May 10, 2010 12:40:49 PM$"

########################################################################
class findingOverlappingEnds( object ):

    def __init__(self, fastaFile_1, fastaFile_2, ignoreSet_1 = None, \
                       ignoreSet_2 = None, trimSize=50000, endSlop=1000, \
                                    minOverlapLength=1000, bestHit_kwargs = {}):

        # Testing and grabbing the fileNames
        testFile( fastaFile_1 )
        testFile( fastaFile_2 )
        self.fastaFile_1      = fastaFile_1
        self.fastaFile_2      = fastaFile_2
        self.selfOverlap = False
        if ( self.fastaFile_1 == self.fastaFile_2 ): self.selfOverlap = True

        # Extracting the join parameters
        self.trimSize         = trimSize
        self.endSlop          = endSlop
        self.minOverlapLength = minOverlapLength

        # Pulling key word arguements
        self.bestHit_kwargs   = bestHit_kwargs

        # Scaffolds to ignore (Nifty little and/or trick!!)
        self.ignoreSet_1 = (ignoreSet_1 == None) and ignoreSet_1 or set()
        self.ignoreSet_2 = (ignoreSet_2 == None) and ignoreSet_2 or set()

        # Generating the trimmed file names
        self.trimmedFileName_1 = generateTrimmedFileName( self.fastaFile_1 )
        self.trimmedFileName_2 = generateTrimmedFileName( self.fastaFile_2 )

        pre_1, ext_1           = splitext(split(self.fastaFile_1)[1])
        self.bestHitOutputFile = createBestHitFileName(self.fastaFile_1)
        self.filteredJoinFile  = '%s_filteredJoins.dat'%pre_1

        self.findOverlap()

    def findOverlap(self):

        # (1) Create the trimmed file
        trimAssemblyFile( self.fastaFile_1, trimLength=self.trimSize, \
                                                    ignoreSet=self.ignoreSet_1 )
        if ( not self.selfOverlap ):
            trimAssemblyFile( self.fastaFile_2, trimLength=self.trimSize, \
                                                    ignoreSet=self.ignoreSet_2 )
        #####

        # (2) BLAT all possible pairs against one another
        self.runBestHit()

        # (3) Filter Joins
        self.filterJoins()
        return

    def runBestHit(self):
        stdout.write( 'Running Best Hit\n')
        if ( self.selfOverlap ):
            DNA_best_hit( self.trimmedFileName_1,
                          self.trimmedFileName_1,
                          nFastaFiles=28,
                          clusterNum=101,
                          outputFileName=self.bestHitOutputFile,
                          **self.bestHit_kwargs )
        else:
            DNA_best_hit( self.trimmedFileName_1,
                          self.trimmedFileName_2,
                          nFastaFiles=28,
                          clusterNum=101,
                          outputFileName=self.bestHitOutputFile,
                          **self.bestHit_kwargs )
        #####
        return

    def filterJoins(self):
        stdout.write( 'Filtering Joins\n')
        connectivity     = {}
        nodeSet          = set()
        sepString        = '_|_'
        for record in iterBestHit(open(self.bestHitOutputFile,'r')):
            # Filtering on placement
            scaffMax = record.scaffSize - self.endSlop
            BACMax   = record.BAC_size  - self.endSlop

            # Filtering by length and coverage
            if ( (record.numBases >= self.minOverlapLength) ):
                pass
            else:
                continue
            #####

            # End Slop Filtering
            makeJoin = False
            if ( record.BAC_recordName[0]=='L' ):
                if ( record.BAC_start<=self.endSlop ):
                    if ( record.scaffold[0]=='R' ):
                        if ( (record.scaffEnd>=scaffMax) and \
                             (record.placDir == '+') ):
                            makeJoin = True
                        #####
                    elif( record.scaffold[0]=='L' ):
                        if ( (record.scaffStart<=self.endSlop) and \
                               (record.placDir == '-') ):
                            makeJoin = True
                        #####
                    elif( ( (record.scaffStart<=self.endSlop) and \
                            (record.placDir == '-') ) or \
                          ( (record.scaffEnd>=scaffMax) and \
                            (record.placDir == '+') ) ):
                        makeJoin = True
                    #####
                #####
            elif ( record.BAC_recordName[0]=='R' ):
                if ( record.BAC_start>=BACMax ):
                    if ( record.scaffold[0]=='R' ):
                        if ( (record.scaffEnd>=scaffMax) and \
                             (record.placDir == '-') ):
                            makeJoin = True
                        #####
                    elif( record.scaffold[0]=='L' ):
                        if ( (record.scaffStart<=self.endSlop) and \
                               (record.placDir == '+') ):
                            makeJoin = True
                        #####
                    elif( ( (record.scaffStart<=self.endSlop) and \
                            (record.placDir == '+') ) or \
                          ( (record.scaffEnd>=scaffMax) and \
                            (record.placDir == '-') ) ):
                        makeJoin = True
                    #####
                #####
            else:
                if ( record.BAC_start<=self.endSlop ):
                    if ( record.scaffold[0]=='R' ):
                        if ( (record.scaffEnd>=scaffMax) and \
                             (record.placDir == '+') ):
                            makeJoin = True
                        #####
                    elif( record.scaffold[0]=='L' ):
                        if ( (record.scaffStart<=self.endSlop) and \
                               (record.placDir == '-') ):
                            makeJoin = True
                        #####
                    elif( ( (record.scaffStart<=self.endSlop) and \
                            (record.placDir == '-') ) or \
                          ( (record.scaffEnd>=scaffMax) and \
                            (record.placDir == '+') ) ):
                        makeJoin = True
                    #####
                #####
                if ( record.BAC_start>=BACMax ):
                    if ( record.scaffold[0]=='R' ):
                        if ( (record.scaffEnd>=scaffMax) and \
                             (record.placDir == '-') ):
                            makeJoin = True
                        #####
                    elif( record.scaffold[0]=='L' ):
                        if ( (record.scaffStart<=self.endSlop) and \
                               (record.placDir == '+') ):
                            makeJoin = True
                        #####
                    elif( ( (record.scaffStart<=self.endSlop) and \
                            (record.placDir == '+') ) or \
                          ( (record.scaffEnd>=scaffMax) and \
                            (record.placDir == '-') ) ):
                        makeJoin = True
                    #####
                #####
            #####

            # Creating the decorated connectivity matrix
            if ( makeJoin ):
                scaff_1  = record.BAC_recordName
                scaff_2  = record.scaffold
                nodeSet.add( scaff_1 )
                nodeSet.add( scaff_2 )
                try:
                    connectivity[scaff_1].append( (scaff_2, \
                                                   record.generateString()) )
                except KeyError:
                    connectivity[scaff_1] = [(scaff_2, \
                                              record.generateString())]
                #####
                try:
                    connectivity[scaff_2].append( (scaff_1, \
                                                   record.generateString()) )
                except KeyError:
                    connectivity[scaff_2] = [(scaff_1, \
                                              record.generateString())]
                #####
            #####

        #####

        # What is the join distribution?
        distJoins = {}
        for node in nodeSet:
            nJoins = len(connectivity[node])
            try:
                distJoins[nJoins] += 1
            except KeyError:
                distJoins[nJoins] = 1
            #####
        #####

        # Join distribution
        stdout.write( 'Join Distribution:\n' )
        for key,val in distJoins.items(): stdout.write( '%d\t%d\n'%(key,val) )
        stdout.write( '%d\n'%sum( [val for key,val in distJoins.items()] ) )

        # Extracting unique Joins
        filteredHandle   = open( self.filteredJoinFile, 'w' )
        uniqueJoins      = set()
        for key,valList in connectivity.items():
            for valTuple in valList:
                joinList = [key,valTuple[0]]
                joinList.sort()
                uniqueID = sepString.join(joinList)
                if ( uniqueID not in uniqueJoins ):
                    uniqueJoins.add( uniqueID )
                    filteredHandle.write( valTuple[1] )
                #####
            #####
        #####
        filteredHandle.close()
        stdout.write( 'Num unique joins\n' )
        stdout.write( '%d\n'%len(uniqueJoins) )
        return

#####
