__author__="jjenkins"
__date__ ="$Nov 18, 2010 10:37:47 PM$"

# HAGSC Library Imports
from _jobQueueing import extractScaffoldNames

from _FASTA_IO import countBasePairs
from _FASTA_IO import FASTAFile_dict
from _FASTA_IO import QUALFile_dict
from _FASTA_IO import writeFASTA
from _FASTA_IO import writeQUAL

from _helperClasses import histogramClass, iterCounter

from _helperFunctions import deleteDir, generateTmpDirName, testDirectory

from _bestHit import iterBestHit

# Python Library Imports
from random import sample, random

from os.path import realpath, isfile, split, splitext, abspath, join

from os import system, chdir, curdir, mkdir

from math import log, sqrt

from sys import stderr, stdout

import re

#=========================================================================
def isGzipFile( fileName ):
    try:
        import gzip
        x = gzip.open(fileName).read()
        return True
    except IOError:
        pass
    #####
    return False
    
#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).read()
        return True
    except IOError:
        pass
    #####
    return False

#=========================================================================
class reduceReadPool(object):

    def __init__(self, fastaFile, qualFile, targetCoverage, arachneReads, numKmers, \
                       merLength, minKmerFreq, maxKmerFreq, peakFreq, \
                       assemblyDirectory, FWHM, keepReadNames):
                       
        # Pulling the information
        self.fastaFile         = realpath(fastaFile)
        self.qualFile          = realpath(qualFile)
        self.targetCoverage    = targetCoverage
        self.arachneReads      = arachneReads
        self.numKmers          = numKmers
        self.merLength         = merLength
        self.minKmerFreq       = minKmerFreq
        self.maxKmerFreq       = maxKmerFreq
        self.peakFreq          = peakFreq
        self.assemblyDirectory = assemblyDirectory
        self.FWHM              = FWHM
        self.keepReadNames     = keepReadNames
        self.baseFileName      = splitext( split(self.fastaFile)[1] )[0]

        # Setting up the tmp path
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )
        tmpFASTA      = join( self.tmpPath, 'tmp.fasta' )
        tmpQUAL       = join( self.tmpPath, 'tmp.fasta.qual' )

        # Testing to see if the files are compressed
        self.compression = False
        if ( isBzipFile(fastaFile) ):
            # Generating the directory information
            self.compression = True
            # Creating the tmp directory
            self.create_tmp_dir()
            # Decompressing the file into the tmp direcory
            stdout.write('\n------------\nDecompressing FASTA and QUAL into tmp directory\n')
            system( 'cat %s | bunzip2 -c > %s'%(fastaFile,tmpFASTA) )
            system( 'cat %s | bunzip2 -c > %s'%(qualFile,tmpQUAL) )
        #####

        if ( isGzipFile(fastaFile) ):
            # Generating the directory information
            self.compression = True
            # Creating the tmp directory
            self.create_tmp_dir()
            # Decompressing the file into the tmp direcory
            stdout.write('\n------------\nDecompressing FASTA and QUAL into tmp directory\n')
            system( 'cat %s | gunzip -c > %s'%(fastaFile,tmpFASTA) )
            system( 'cat %s | gunzip -c > %s'%(qualFile,tmpQUAL) )
        #####
        
        if ( self.compression ):
            # Re-extracting the base file name
            self.baseFileName = splitext( self.baseFileName )[0]
            # Resetting the FASTA and QUAL files
            self.fastaFile = tmpFASTA
            self.qualFile  = tmpQUAL
        #####

        # Performing the reduction
        self.performPoolReduction()
        
        # Cleaning up
        if ( self.compression ): self.clean_tmp()
    
    def performPoolReduction(self):

        # Making sure the files exist
        if ( not (isfile(self.fastaFile) and isfile(self.qualFile)) ):
            raise ValueError( 'FILES NOT FOUND:\n %s\n %s\n'%(self.fastaFile,self.qualFile) )
        #####
        
        # Reduce the dataset
        reduceDataSet( self.fastaFile, self.qualFile, self.arachneReads, self.targetCoverage, self.merLength, self.numKmers, \
                       self.minKmerFreq, self.maxKmerFreq, self.peakFreq, self.baseFileName, \
                       self.FWHM, self.keepReadNames, self.assemblyDirectory )
        
    def create_tmp_dir(self):
        stderr.write( '\n--------------------\nCreating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def clean_tmp(self):
        stderr.write( '\n--------------------\nRemoving Temporary directory\n')
        deleteDir(self.tmpPath)
        return
        
    #####

#=========================================================================
def reduceDataSet( readsFASTAFile, readsQUALFile, arachneReads, targetCoverage, merLength, numKmers, \
                   minKmerFreq, maxKmerFreq, peakFreq, baseFileName, FWHM, keepReadNames, \
                   assemblyDirectory ):

    # Indexing the reads file
    stderr.write( '\n----------------------\nIndexing the fasta file: %s\n'%readsFASTAFile )
    indexedFASTA = FASTAFile_dict( readsFASTAFile, arachneReads )

    # Determining the sepString
    if ( not keepReadNames ): sepString  = pullSepString( indexedFASTA.keys()[0] )
    
    stderr.write( '\n----------------------\nIndexing the qual file: %s\n'%readsQUALFile )
    indexedQUAL = QUALFile_dict( readsQUALFile, arachneReads )

    # Generating the set of pair names
    splitter         = re.compile( '(-R)' ).split
    canonicalNameSet = set( [splitter(readID)[0] for readID in indexedFASTA.iterkeys()] )
    nOriginalPairs = len(canonicalNameSet)

    # Screening all read pairs for pairs that are selectable
    kmerCov, finalPairSet = screenAllPairs( indexedFASTA, \
                                            readsFASTAFile, \
                                            canonicalNameSet, \
                                            merLength, \
                                            numKmers, \
                                            minKmerFreq, \
                                            maxKmerFreq, \
                                            peakFreq, \
                                            baseFileName, \
                                            FWHM )
    nFinalPairs = len(finalPairSet)
    
    if ( targetCoverage.count(',') > 0 ):
        coverages = [int(item) for item in targetCoverage.split(',')]
    else:
        coverages = [int(targetCoverage)]
    #####
    
    for tmpCoverage in coverages:
    
        # Compute the number of read pairs to pull
        stderr.write( 'kmer cov.       = %.2fx\n'%kmerCov )
        if ( tmpCoverage < kmerCov ):
            stderr.write( 'Target coverage = %.2f\n'%tmpCoverage )
            nReadPairs = int( float(nFinalPairs) * ( tmpCoverage / kmerCov ) )
            stderr.write( 'Original Set    = %d\n'%nOriginalPairs )
            stderr.write( 'Screened Set    = %d\n'%nFinalPairs )
            stderr.write( 'Reduced Set     = %d\n'%nReadPairs )
        else:
            stderr.write( 'Target coverage (%.2f) is greater than kmer coverage (%.2f)\n'%(tmpCoverage,kmerCov) )
            nReadPairs = int(nOriginalPairs)
            stderr.write( 'nReadPairs      = %d\n'%nReadPairs )
        #####
        
        # Randomly select read pairs
        stderr.write( '\n--------------------\nGenerating random read set\n' )
        x                  = iterCounter(100000)
        finalBase = '%s.cov_%d'%(baseFileName, tmpCoverage)
        fastaOutputFile = open( '%s.fasta'%finalBase, 'w' )
        qualOutputFile  = open( '%s.fasta.qual'%finalBase, 'w' )
        try:
            for canName in sample( finalPairSet, nReadPairs ):
                for m in xrange(1,3):
                    # Pull records
                    readID = '%s-R%d'%(canName,m)
                    fasta_record = indexedFASTA[readID]
                    qual_record  = indexedQUAL[readID]
                    
                    # Renaming the reads
                    read_id_altered = readID
                    altered_desc    = fasta_record.description
                    if ( not keepReadNames ):
                        # Example:  >ZTX01-A02.y1d-s CHEM: term DYE: big TEMPLATE: ZTX01-A02 DIRECTION: rev
                        read_id_altered, template, tmpDir = fix_illumina_name(readID, sepString)
                        altered_desc = "CHEM: term DYE: big TEMPLATE: %s DIRECTION: %s"%(template,tmpDir)
                    #####
    
                    # Writing the reads        
                    fasta_record.id          = read_id_altered
                    fasta_record.description = altered_desc
                    writeFASTA([fasta_record], fastaOutputFile)
             
                    # Writing the reads        
                    qual_record.id          = read_id_altered
                    qual_record.description = altered_desc
                    writeQUAL([qual_record], qualOutputFile)
                #####
                x()
            #####
        except ValueError:
            for canName in finalPairSet:
                for m in xrange(1,3):
                    # Pull records
                    readID = '%s-R%d'%(canName,m)
                    fasta_record = indexedFASTA[readID]
                    qual_record  = indexedQUAL[readID]
                    
                    # Renaming the reads
                    read_id_altered = readID
                    altered_desc    = fasta_record.description
                    if ( not keepReadNames ):
                        # Example:  >ZTX01-A02.y1d-s CHEM: term DYE: big TEMPLATE: ZTX01-A02 DIRECTION: rev
                        read_id_altered, template, tmpDir = fix_illumina_name(readID, sepString)
                        altered_desc = "CHEM: term DYE: big TEMPLATE: %s DIRECTION: %s"%(template,tmpDir)
                    #####
    
                    # Writing the reads        
                    fasta_record.id          = read_id_altered
                    fasta_record.description = altered_desc
                    writeFASTA([fasta_record], fastaOutputFile)
             
                    # Writing the reads        
                    qual_record.id          = read_id_altered
                    qual_record.description = altered_desc
                    writeQUAL([qual_record], qualOutputFile)
                #####
                x()
            #####
        #####
        fastaOutputFile.close()
        qualOutputFile.close()
    
        # Writing the coverageHistograms to an output file
        mainOutputFile = '%s.cov_%d.comparison'%(baseFileName, tmpCoverage)
        tmpHandle = open( mainOutputFile, 'w' )
        tmpHandle.close()
    
        # Generating the output files
        histHashFile = '%s.tmpHistHash.out'%baseFileName
        executeAndEcho( 'python -m gc_histogram %s %s.fasta >> %s'%( readsFASTAFile, \
                                                                     finalBase, \
                                                                     mainOutputFile) )
        executeAndEcho( '/mnt/local/EXBIN/histogram_hash -z 500m -m 24 -o %s %s.fasta'%( histHashFile, \
                                                                                         finalBase ) )
        executeAndEcho( 'python -m kmer_hist %s -s 10000 >> %s'%(histHashFile, mainOutputFile) )
        executeAndEcho( r'echo "============" >> %s'%mainOutputFile )
        executeAndEcho( 'python -m kmer_hist %s.kmerHist -s 10000 >> %s'%( baseFileName, \
                                                                           mainOutputFile) )
        executeAndEcho( 'rm %s'%histHashFile )
    
    
        # Move files to appropriate locations
        if ( assemblyDirectory ):  setUpAssemblyDirectory( '%s.cov_%d.assembly'%(baseFileName,tmpCoverage), \
                                                           baseFileName, tmpCoverage )
    
    #####

    # Removing the kmerHist and scores file
    executeAndEcho( 'rm -f %s.kmerHist'%baseFileName )
    executeAndEcho( 'rm -f %s.scores'%baseFileName )
    
    return

#===========================================================================
def screenAllPairs( indexedFASTA, readsFASTAFile, canonicalNameSet, merLength, numKmers, \
                    minKmerFreq, maxKmerFreq, peakFreq, baseFileName, FWHM):
    # Generating kmer histogram
    executeCode     = True
    kmerDistOutFile = '%s.kmerHist'%baseFileName
    stderr.write( '\n--------------\nGenerating kmer histogram file:  %s\n'%kmerDistOutFile )
    executeAndEcho( '/mnt/local/EXBIN/histogram_hashn -m %d -z %s -o %s %s'%( merLength, numKmers, \
                                                                              kmerDistOutFile, \
                                                                              readsFASTAFile ), executeCode )

    # Generating kmer frequencies
    kmerFreqOutFile = '%s.kmerFreqTmp'%baseFileName
    stderr.write( '\n--------------\nGenerating kmer frequencies:  %s\n'%kmerFreqOutFile )
    executeAndEcho( '/mnt/local/EXBIN/histogram_hashn -m %d -z %s -w 1 -o %s %s'%( merLength, numKmers, \
                                                           kmerFreqOutFile, readsFASTAFile), executeCode )

    # Generating kmer scores
    stderr.write( '\n--------------\nGenerating kmer scores\n' )
    maxValue, kmerScores = generateKmerFreqScores( kmerDistOutFile, kmerFreqOutFile, baseFileName, minKmerFreq, \
                                                   maxKmerFreq, peakFreq, FWHM )
                                                   
    # Removing the kmer frequency file
    if ( executeCode ):  executeAndEcho(  'rm -f %s'%kmerFreqOutFile, executeCode )
    
    # Computing minimum nonzero score
    minimumScore = min( [item for item in kmerScores.itervalues() if (item != 0.0)] )
    
    # Filtering read pairs based on score
    stderr.write( '\n------------------\nFiltering all read pairs based on kmer score\n' )
    finalPairSet = []
    nPass        = 0
    nFail        = 0
    x = iterCounter(100000)
    for canName in canonicalNameSet:
        x()
        readScore = [0,0]
        for m in xrange(1,3):
            # First read in the pair
            readID     = '%s-R%d'%(canName,m)
            try:
                # May not have a pair, need to check it
                record     = indexedFASTA[ readID ]
            except KeyError:
                continue
            #####
            tmpSeq     = str(record.seq).upper()
            nSize      = len(tmpSeq)
            nMers      = 0.0
            totalScore = 0.0
            for n in xrange( (nSize-24) ):
                tmpMer = tmpSeq[n:(n+24)]
                if ( tmpMer.count('N') == 0 ):
                    # Scoring a 24mer regularly
                    totalScore += kmerScores[tmpMer]
                else:
                    # Scoring a 24mer with an N using the minimum score
                    totalScore += minimumScore
                #####
                nMers += 1.0
            #####
            try:
                readScore[m-1] = totalScore / nMers
            except ZeroDivisionError:
                readScore[m-1] = 0.0
            #####
        #####
        # If one fails, they both fail
        if ( (random() <= readScore[0]) and (random() <= readScore[1]) ):
            finalPairSet.append( canName )
            nPass += 1
        else:
            nFail += 1
        #####
    #####
    
    # Need to scale the maximum kmer coverage value based on the number of pairs that passed
    perPass = 100.0 * float(nPass) / float(nPass + nFail)
    perFail = 100.0 * float(nFail) / float(nPass + nFail)
    stderr.write( 'nPass Pairs        = %d (%.1f%%)\n'%(nPass,perPass) )
    stderr.write( 'nFail Pairs        = %d (%.1f%%)\n'%(nFail,perFail) )
    stderr.write( 'kmer Cov.          = %.3f\n'%maxValue)
    maxValue = float(nPass) / float(len(canonicalNameSet)) * maxValue
    stderr.write( 'adjusted kmer Cov. = %.3f\n'%maxValue)
    
    return maxValue, finalPairSet

#=========================================================
def generateKmerFreqScores( histHashFile, kmerFreqOutFile, baseFileName, minKmerFreq, maxKmerFreq, \
                            peakFreq, FWHM_input):

    #======================================
    # FINDING PEAK IN HISTOGRAM
    
    # Reading in and normalizing the histogram
    histData    = []
    totalSeq    = 0.0
    for line in open( histHashFile, 'r' ):
        splitLine = line.split(None)
        kmerFreq  = int(splitLine[0])
        if ( kmerFreq == 1 ): continue  # Skip mer frequencies of 1, by convention
        nKmers   = int( splitLine[1] )
        binSeq   = float(kmerFreq) * float(nKmers)
        totalSeq += binSeq
        histData.append( [ kmerFreq, nKmers, binSeq ] )
    #####
    cnst_1 = 1.0 / totalSeq
    n      = 0
    for kmerFreq, nKmers, binSeq in histData:
        perSeq = cnst_1*binSeq
        histData[n] = ( kmerFreq, nKmers, perSeq, n )
        n += 1
    #####

    # Finding the peak frequency
    if ( peakFreq > 0 ):
        # The frequency may not be one of the frequencies listed
        # In that case we have to pick the closest one
        diff_x = [abs(item[0]-peakFreq) for item in histData]
        n = diff_x.index( min(diff_x) )
        maxPerSeq = histData[n][2]
        maxFreq   = histData[n][0]
        maxIndex  = n
    else:
        # Setting up screening function
        if ( minKmerFreq > 0 ):
            if ( maxKmerFreq > 0 ):
                freqScreen = lambda kmerFreq: (kmerFreq > minKmerFreq) and (kmerFreq < maxKmerFreq)
            else:
                freqScreen = lambda kmerFreq: kmerFreq > minKmerFreq
            #####
        else:
            if ( maxKmerFreq > 0 ):
                freqScreen = lambda kmerFreq: kmerFreq < maxKmerFreq
            else:
                freqScreen = lambda kmerFreq: True
            #####
        #####
        
        # In this case we have to find the maximum frequency over the range
        DSU = []
        for kmerFreq, nKmers, perSeq, n in histData:
            if ( freqScreen(kmerFreq) ):
                DSU.append( (perSeq, kmerFreq, n) )
            #####
        #####
        DSU.sort( reverse=True )
        maxPerSeq = DSU[0][0]
        maxFreq   = DSU[0][1]
        maxIndex  = DSU[0][2]
    #####
    
    # Finding the full width at half max
    halfMax  = maxPerSeq / 2.0
    try:
        # Finding RHS
        n_RHS = maxIndex
        tmpLen = len(histData)
        while True:
            if ( histData[n_RHS][2] < halfMax ): break
            n_RHS += 1
            if ( n_RHS > tmpLen ):
                n_RHS = tmpLen - 1
                break
            #####
        #####
    except IndexError:
        stdout.write( '\n\n-----------------------------------\n')
        stdout.write( 'During the search for half of the maximum value on the left side of the peak, \n' )
        stdout.write( 'the search algorithm reached the left boundary of the distribution.  This \n' )
        stdout.write( 'typically indicates that your kmer histogram has a maximum value on the end \n' )
        stdout.write( 'of the distribution.  You should plot the distribution and consider a cutoff \n' )
        stdout.write( 'on the lower end of the distribution.\n\nThe code is now exiting.\n' )
        stdout.write( '-----------------------------------\n\n')
        exit()
    #####
    try:
        # Finding LHS
        n_LHS = maxIndex
        while True:
            if ( histData[n_LHS][2] < halfMax ): break
            n_LHS -= 1
            if ( n_LHS < 0 ):
                n_LHS = 0
                break
            #####
        #####
    except IndexError:
        stdout.write( '\n\n-----------------------------------\n')
        stdout.write( 'During the search for half of the maximum value on the right side of the peak, \n' )
        stdout.write( 'the search algorithm reached the right boundary of the distribution.  This \n' )
        stdout.write( 'typically indicates that your kmer histogram has a maximum value on the end \n' )
        stdout.write( 'of the distribution.  You should plot the distribution and consider a cutoff \n' )
        stdout.write( 'on the upper end of the distribution.\n\nThe code is now exiting.\n' )
        stdout.write( '-----------------------------------\n\n')
        exit()
    #####
    
    # Computing the full width at half maximum
    if ( FWHM_input < 0 ):
        FWHM = histData[n_RHS][0] - histData[n_LHS][0]
    else:
        FWHM = FWHM_input
    #####
    # Computing the cutoff frequency
    sigma      = float( FWHM ) / ( 2.0 * sqrt( 2.0 * log(2.0) ) )
    cutoffFreq = int( maxFreq + 4.0 * sigma )
    if ( maxKmerFreq > 0 ): cutoffFreq = min( [cutoffFreq, maxKmerFreq] )

    # Some output to the user    
    stderr.write( 'maxFreq          = %d\n'%maxFreq )
    stderr.write( 'maxPerSeq        = %.4f\n'%maxPerSeq )
    stderr.write( 'FWHM             = %d\n'%FWHM )
    stderr.write( 'maxIndex         = %d\n'%maxIndex )
    if ( minKmerFreq < 0 ):
        stderr.write( 'lower cutoffFreq = 0\n' )
    else:
        stderr.write( 'lower cutoffFreq = %d\n'%minKmerFreq )
    #####
    stderr.write( 'upper cutoffFreq = %d\n'%cutoffFreq )

    #======================================
    # COMPUTING THE KMER SCORES
    
    # Controlling parameters
    biggestValue = 1.0   # Largest value of 1/P(x)
    
    # Computing the thresholded 1/P(x) inverted distribution up to the max value
    newDist  = len(histData)*[0.0] # Initialized to zero
    FWHM_RHS = histData[n_RHS][0]
    for n in xrange(cutoffFreq):
        try:
            kmerFreq, nKmers, perSeq, m = histData[n]
        except IndexError:
            break
        #####
        # Computing relative score
        if ( kmerFreq < minKmerFreq ):  # Everything below the minimum frequency is eliminated
            newDist[n] = 0.0
        elif ( minKmerFreq <= kmerFreq < maxFreq ):  # Left side of the peak value
            if ( perSeq > 0.0 ):
                newDist[n] = min( biggestValue, 1.0 / perSeq )  # Caping the largest value
            else:
                newDist[n] = biggestValue
            #####
        elif ( maxFreq <= kmerFreq <= cutoffFreq ):  # Right side of peak value to cutoff, same as peak value
            newDist[n] = min( biggestValue, 1.0 / histData[maxIndex][2] )
        elif ( kmerFreq > cutoffFreq ):  # Everything beyond the cutoff frequency is eliminated
            newDist[n] = 0.0
        #####
    #####
    max_newDist = max(newDist)
    for n in xrange(len(newDist)): newDist[n] = newDist[n] / max_newDist
    
    # Generating the secreening function
    if ( maxKmerFreq > 0 ):
        freqScreen = lambda kmerFreq: (kmerFreq >= cutoffFreq) and (kmerFreq < maxKmerFreq)
    else:
        freqScreen = lambda kmerFreq: kmerFreq >= cutoffFreq
    #####

    # Generating the kmer frequency score vector
    perNominal   = 0.20  # Percent of nominal value for repetitive portion of kmerDistribution
    kmerFreqScores    = {}
    # Handling the case of f=1
    kmerFreqScores[1] = 0
    if ( minKmerFreq <= 1 ):  kmerFreqScores[1] = biggestValue
    # Handling the remaining cases
    cnst_2            = perNominal * float(cutoffFreq) / biggestValue
    for kmerFreq, nKmers, perSeq, n in histData:
        # Error cutoff
        if ( kmerFreq < minKmerFreq ):  # Everything below the minimum frequency is eliminated
            kmerFreqScores[kmerFreq] = 0.0
        elif ( minKmerFreq <= kmerFreq < cutoffFreq ):  # Between min and cutoff frequencies
            # Choose normalized score from above
            kmerFreqScores[kmerFreq] = newDist[n]
        elif ( freqScreen(kmerFreq) ):  # Between cutoff and max, if we are using a max.
            # kmer score is inversely proportion to frequency
            kmerFreqScores[kmerFreq] = cnst_2 / float(kmerFreq)
        else:  # Freq is larger than maxFreq, so ignore
            kmerFreqScores[kmerFreq] = 0.0
        #####
    #####
    
    # Outputting the scores to a file
    DSU = []
    for tmpFreq, tmpScore in kmerFreqScores.iteritems():
        DSU.append( (tmpFreq, tmpScore) )
    #####
    DSU.sort()
    scoresFile = '%s.scores'%baseFileName
    stderr.write( '\n------------\nWriting the kmer scores to:  %s\n'%scoresFile )
    tmpHandle = open( scoresFile, 'w' )
    for tmpFreq, tmpScore in DSU: tmpHandle.write( '%d\t%.8f\n'%(tmpFreq,tmpScore) )
    tmpHandle.close()

    # Loading the kmer scores into a dictionary
    kmerScores = {}
    for line in open(kmerFreqOutFile, 'r'):
        kmer, tmpFreq = line.split(None)
        kmerScores[kmer] = kmerFreqScores[int(tmpFreq)]
    #####
    
    return maxFreq, kmerScores

#==============================================================
def fix_illumina_name(readName, sepString):
    baseName, readExt = readName.split(sepString)
    fixedName = baseName.replace(':','_')
    fixedName = fixedName.split('#')[0]
    tmpDir = 'unk'
    if ( readExt == '1' ): 
        tmpDir = 'fwd'
    elif ( readExt == '2' ):
        tmpDir = 'rev'
    #####
    return ('%s-R%s'%(fixedName,readExt), fixedName, tmpDir)

#==============================================================
def pullSepString(scaffID):
    if ( scaffID.count('-R') > 0 ):
        return '-R'
    elif( scaffID.count('/') > 0 ):
        return '/'
    else:
        print "Unable to determine the separation string for %s"%scaffID
        assert False
    #####

#=========================
def extractSeqAndQual(readsFASTAFile, readsQUALFile, baseFileName, keepReadNames, targetCoverage):

    # Reading all of the base read names for the pairs
    contigReadNameSet = {}
    qualOutputFiles   = {}
    fastaOutputFiles  = {}
    fastaDicts        = {}
    
    stderr.write( '\n-----------------------\nEXTRACT SEQ AND QUAL READS FILE = %s\n'%baseFileName )
    
    # Getting the dictionary of indexed FASTA files
    print '\n--------------------\nIndexing FASTA file'
    fastaDict = FASTAFile_dict( '%s.r_temp.fasta'%baseFileName )
    print '\n--------------------\nIndexing FASTA/QUAL files'
    qualDict  = QUALFile_dict(  readsQUALFile )

    # Opening the output files
    fastaOutputFile = open( '%s.cov_%d.fasta'%(baseFileName,targetCoverage), 'w' )
    qualOutputFile  = open( '%s.cov_%d.fasta.qual'%(baseFileName,targetCoverage), 'w' )
    
    # Determining the sepString
    if ( not keepReadNames ):
        tmpScaffID = fastaDict.keys()[0]
        sepString  = pullSepString(tmpScaffID)
    #####

    # Pulling and renaming the reads    
    x = iterCounter(100000)
    for scaffID in fastaDict.iterkeys():

        # Pull records
        fasta_record = fastaDict[scaffID]
        qual_record  = qualDict[scaffID]

        # Renaming the reads
        scaffold_id_altered = scaffID
        altered_desc        = fasta_record.description
        if ( not keepReadNames ):
            # Example:  >ZTX01-A02.y1d-s CHEM: term DYE: big TEMPLATE: ZTX01-A02 DIRECTION: rev
            scaffold_id_altered, template, tmpDir = fix_illumina_name(scaffID, sepString)
            altered_desc = "CHEM: term DYE: big TEMPLATE: %s DIRECTION: %s"%(template,tmpDir)
        #####

        # Writing the reads        
        fasta_record.id = scaffold_id_altered
        fasta_record.description = altered_desc
        writeFASTA([fasta_record], fastaOutputFile)

        qual_record.id = scaffold_id_altered
        qual_record.description = altered_desc
        writeQUAL([qual_record], qualOutputFile)
        x()
    #####

    # Closing the output files    
    qualOutputFile.close()
    fastaOutputFile.close()
    
    return

#####

#=========================
def loadReadSet(readFile):
    return set([line.strip() for line in open(readFile, 'r')])

#=========================
def executeAndEcho(cmd, runCommand=True):
    stderr.write( '%s\n'%cmd )
    if ( runCommand ): system( cmd )
    return

#=================================
def setUpAssemblyDirectory(newDir, baseFileName, targetCoverage):
    
    # For debugging purposes
    executeSetup = True

    # (1)  Make a new directory
    executeAndEcho( 'mkdir %s'%newDir, executeSetup )

    # (2)  Add the underlying directories
    executeAndEcho( 'mkdir %s/chromat_dir'%newDir, executeSetup )
    executeAndEcho( 'mkdir %s/edit_dir'%newDir, executeSetup )
    executeAndEcho( 'mkdir %s/phd_dir'%newDir, executeSetup )
    
    # (3)  Move the files to the appropriate places
    fastaFile = '%s.cov_%d.fasta'%(baseFileName, targetCoverage)
    executeAndEcho(  'mv -f %s %s/edit_dir/'%(fastaFile, newDir), executeSetup )
    
    qualFile = '%s.cov_%d.fasta.qual'%(baseFileName, targetCoverage)
    executeAndEcho(  'mv -f %s %s/edit_dir/'%(qualFile, newDir), executeSetup )

    # (4)  Move ancillary files into the edit_dir
    executeAndEcho(  'cp -f %s.kmerHist %s/edit_dir/'%(baseFileName, newDir), executeSetup )
    executeAndEcho(  'cp -f %s.scores %s/edit_dir/'%(baseFileName, newDir), executeSetup )

    mainOutputFile = '%s.cov_%d.comparison'%(baseFileName, targetCoverage)
    executeAndEcho(  'mv -f %s %s/edit_dir/'%(mainOutputFile, newDir), executeSetup )

    return
