#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/22/14"

from hagsc_lib import iterBestHit

from os.path import realpath, isfile, abspath, join, isdir, split

from os import curdir, getpid, uname, mkdir, chmod, chdir, system

from sys import stderr

from shutil import rmtree

from optparse import OptionParser

import subprocess

#==============================================================
def writeHeader( breakType, oh, startTuple, line, prev_line ):

    # Writing the header
    oh.write( '-----------------------------------------------------\n')
    oh.write( 'BREAK_TYPE: %s\n'%breakType )
    oh.write( '%s'%prev_line )
    oh.write( '%s'%line )
    return

#==============================================================
def baseFileName(fileName):
    return splitext(split(fileName)[1])[0]

#==============================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )

#==============================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True

#==============================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True

#==============================================================
def create_tmp_dir( tmpPath, deleteOld=True ):
    stderr.write( '\n\t-Creating tmp directory\n\n')
    # If the directory exists, delete it and make another one
    try:
        testDirectory(tmpPath)
        if deleteOld:
            deleteDir(tmpPath)
            mkdir( tmpPath, 0o777 )
            chmod(tmpPath, 0o777 )
            testDirectory(tmpPath)
            return
        else:
            return
        #####
    except IOError:
        mkdir( tmpPath, 0o777 )
        chmod(tmpPath, 0o777 )
        testDirectory(tmpPath)
        return
    #####

#==============================================================
def clean_tmp(tmpPath):
    stderr.write( '\t-Removing Temporary directory\n\n')
    deleteDir(tmpPath)
    return

#==============================================================
def real_main():
    
    # Defining the program options
    usage = "usage: %prog [target fasta] [output file] [options]"

    parser = OptionParser(usage)

    per_ID = 90
    parser.add_option( '-i', \
                       "--per_ID", \
                       type    = "int", \
                       help    = "Percent Identity.  Default: %d"%per_ID, \
                       default = per_ID )

    per_COV = 90
    parser.add_option( '-v', \
                       "--per_COV", \
                       type    = "int", \
                       help    = "Percent Coverage.  Default: %d"%per_COV, \
                       default = per_COV )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        targetFASTA = realpath(args[0])
        if ( not isfile(targetFASTA) ): parser.error( '%s can not be found'%targetFASTA )
        mainOutFile = realpath(args[1])
        per_ID  = float( options.per_ID  )
        per_COV = float( options.per_COV )
    #####
    
    # Generating the directory information
    basePath   = abspath(curdir)
    tmpDir     = generateTmpDirName()
    tmpPath    = join( basePath, tmpDir )
    
    #=================================================================
    # Step 1:  Create temporary directory
    create_tmp_dir( tmpPath )
    chdir( tmpPath )
    
    #=================================================================
    # Step 2:  Align the markers
    cmd = '/mnt/local/EXBIN/blat -noHead -extendThroughN %s /home/t3c2/AIP4/JWJ_ANALYSIS/Sorghum/Sbi1_map/Sbi1_map.fasta tmp.blat'%targetFASTA
    stderr.write( '%s\n'%cmd )
    system( cmd )
    
    #=================================================================
    # Step 3:  Converting alignments to bestHit format
    cmd = 'convertToBestHit.py tmp.blat -o tmp.bestHit'
    stderr.write( '%s\n'%cmd )
    system( cmd )
    
    #=================================================================
    # Step 4:  Parse the markers and write to a file
    groupDict    = {}
    outputString = 8*['']
    scaffSizes   = {}
    scaffPlacs   = {}
    for record in iterBestHit( open('tmp.bestHit', 'r') ):
        # Screening for quality
        if ( (record.per_ID < per_ID) or (record.per_coverage < per_COV) ): continue
        splitName = record.BAC_recordName.split('|')
        markerID  = splitName[0]
        mapGroup  = splitName[1]
        mapPos    = float(splitName[2])
        # Generating the output string
        outputString[0] = mapGroup
        outputString[1] = '%.4f'%mapPos
        outputString[2] = record.scaffold
        outputString[3] = '%d'%record.scaffSize
        outputString[4] = '%d'%record.scaffStart
        outputString[5] = '%d'%record.scaffEnd
        outputString[6] = record.placDir
        outputString[7] = '%s\n'%markerID
        finalString = '\t'.join(outputString)
        # Storing scaffold sizes
        scaffSizes[record.scaffold] = int( record.scaffSize )
        # Storing the scaffold
        try:
            scaffPlacs[record.scaffold].append( (record.scaffStart,mapGroup,finalString) )
        except KeyError:
            scaffPlacs[record.scaffold] = [(record.scaffStart,mapGroup,finalString)]
        #####
        # Storing markers
        try:
            groupDict[mapGroup].append( (mapPos,record.scaffold,record.scaffSize,record.scaffStart,mapGroup,finalString) )
        except KeyError:
            groupDict[mapGroup] = [(mapPos,record.scaffold,record.scaffSize,record.scaffStart,mapGroup,finalString)]
        #####
    #####
    
    # Writing the results to an output file sorted by scaffold
    outputHandle_Scaff = open( mainOutFile, 'w' )
    DSU_scaff = [ (value,key) for key,value in scaffSizes.iteritems() ]
    DSU_scaff.sort(reverse=True)
    for scaffSize, scaffID in DSU_scaff:
        # Pulling the placement list
        placList = scaffPlacs[scaffID]
        placList.sort()
        # Looking for the most abundant map group
        tmpCmp = {}
        for scaffStart,mapGroup,finalString in placList:
            try:
                tmpCmp[mapGroup] += 1
            except KeyError:
                tmpCmp[mapGroup] = 1
            #####
        #####
        DSU_cmp = [(value,key) for key, value in tmpCmp.iteritems()]
        DSU_cmp.sort(reverse=True)
        targetGrp = DSU_cmp[0][1]
        # Write the list
        outputHandle_Scaff.write( '===========================================\n')
        for scaffStart, mapGroup, finalString in placList:
            if ( mapGroup != targetGrp ):
                outputHandle_Scaff.write( ''.join( ['**',finalString] ) )
            else:
                outputHandle_Scaff.write( finalString )
            #####
        #####
    #####
    outputHandle_Scaff.close()
    
    # Finding all of the breaks
    outPath, outTmp = split(mainOutFile)
    print outPath
    print outTmp
    print join( outPath, '.'.join(['.'.join(outTmp.split('.')[:-1]),'breaks']) )
    oh_break   = open( join( outPath, '.'.join(['.'.join(outTmp.split('.')[:-1]),'breaks']) ), 'w' )
    maxPosJump = 20
    for line in open( mainOutFile ):
    
        # Did we hit a new line?        
        if ( line[0] == '=' ):
            prev_MapPos     = None
            prev_LG         = None
            prev_scaffStart = None
            prev_line       = None
            continue
        #####
        
        # Parse the line
        splitLine  = line.split(None)
        LG         = int( splitLine[0].replace('**','') )
        mapPos     = float( splitLine[1] )
        scaffStart = int( splitLine[4] )
        
        # Is this the first line in a set?
        if ( prev_MapPos == None ):
            prev_MapPos     = mapPos
            prev_LG         = LG
            prev_scaffStart = scaffStart
            prev_line       = line
            continue
        #####
        
        # Looking for breaks
        if ( LG != prev_LG ):
        
            # Has the linkage group changed?
            startTuple = sorted( [prev_scaffStart, scaffStart] )
            writeHeader( 'LG_CHANGE', oh_break, startTuple, line, prev_line )
            
        elif ( abs( prev_MapPos - mapPos ) > maxPosJump ):
        
            # Has there been a significant jump in position
            startTuple = sorted( [prev_scaffStart, scaffStart] )
            writeHeader( 'DISCONTINUITY', oh_break, startTuple, line, prev_line )
        
        #####
        
        # Updating the previous variables
        prev_MapPos     = mapPos
        prev_LG         = LG
        prev_scaffStart = scaffStart
        prev_line       = line

    #####

    oh_break.close()
    
    #=================================================================
    # Step 5:  Cleaning up after myself
    chdir('../')
    clean_tmp(tmpPath)    

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
