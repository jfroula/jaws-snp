#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="12-April-2011"

# Local Library Imports
from hagsc_lib import ESThit_File
from hagsc_lib import writeFASTA
from hagsc_lib import FASTAFile_dict

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout
from sys import stderr

#==============================
def checkFile( fileName, parser ):
    if ( not isfile(fileName) ): 
        parser.error( '%s can not be found'%ESThit_outputFile )
    else:
        return fileName
    #####

#===============
def real_main():

    # Defining the program options
    usage = "usage: %prog [EST_hit output file] [EST fasta] [options]"

    parser = OptionParser(usage)

    parser.add_option( "-c", \
                       "--pullChimeric", \
                       action  = 'store_true', \
                       dest    = 'pullChimeric', \
                       help    = "Pulls all chimeric ESTs" )
    parser.set_defaults( pullChimeric = False )

    parser.add_option( "-i", \
                       "--pullInverted", \
                       action  = 'store_true', \
                       dest    = 'pullInverted', \
                       help    = "Pulls all inverted ESTs" )
    parser.set_defaults( pullInverted = False )

    parser.add_option( "-g", \
                       "--pullNotFound_gt50", \
                       action  = 'store_true', \
                       dest    = 'pullNotFound_gt50', \
                       help    = "Pulls all not found ESTs >= 50%" )
    parser.set_defaults( pullNotFound_gt50 = False )

    parser.add_option( "-n", \
                       "--pullNotFound_lt50", \
                       action  = 'store_true', \
                       dest    = 'pullNotFound_lt50', \
                       help    = "Pulls all not found ESTs < 50%" )
    parser.set_defaults( pullNotFound_lt50 = False )

    parser.add_option( "-f", \
                       "--pullFound", \
                       action  = 'store_true', \
                       dest    = 'pullFound', \
                       help    = "Pulls all found ESTs" )
    parser.set_defaults( pullFound = False )

    parser.add_option( "-o", \
                       "--outputStats", \
                       action  = 'store_true', \
                       dest    = 'outputStats', \
                       help    = "Output good hits/scaff to stdout" )
    parser.set_defaults( outputStats = False )
    
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        ESThit_outputFile = checkFile( args[0], parser )
        EST_fasta         = checkFile( args[1], parser )
    #####

    # Getting the EST hit file set up
    ESThit_object = ESThit_File( ESThit_outputFile )

    # Parsing the options
    test_set = set()
    if ( options.pullChimeric ):      test_set.add('chimeric')
    if ( options.pullInverted ):      test_set.add('inverted')
    if ( options.pullNotFound_gt50 ): test_set.add('notFound_gt50')
    if ( options.pullNotFound_lt50 ): test_set.add('notFound_lt50')
    if ( options.pullFound ):         test_set.add('found')
    
    # Checking to see if there is anything to screen
    if ( test_set != set() ):
        indexedFASTA = FASTAFile_dict( EST_fasta )
        fullSet  = set( indexedFASTA.keys() )
        set_dict = {}
        set_dict['chimeric']      = set([item for item in ESThit_object.chimericSequences])
        set_dict['inverted']      = set([item for item in ESThit_object.invertedSequences])
        set_dict['notFound_gt50'] = set([item for item in ESThit_object.notFoundSequences_gt50])
        set_dict['notFound_lt50'] = set([item for item in ESThit_object.notFoundSequences_lt50])
        set_dict['found']         = fullSet.difference( set_dict['chimeric'] ).difference( \
                                                   set_dict['inverted'] ).difference( \
                                                   set_dict['notFound_gt50'] ).difference( \
                                                   set_dict['notFound_lt50'] )
        for scaffID in indexedFASTA.iterkeys():
            for test in test_set:
                if ( scaffID in set_dict[test] ):
                    writeFASTA( [indexedFASTA[scaffID]], stdout )
                #####
            #####
        #####
    #####
    
    # Outputting stats
    if ( options.outputStats ):
        for scaffID, numHits in ESThit_object.goodSequencesPerScaffold.iteritems():
            stdout.write( '%s\t%d\n'%(scaffID,numHits) )
        #####
    #####

    return
    
#####

if ( __name__ == '__main__' ):
    real_main()


