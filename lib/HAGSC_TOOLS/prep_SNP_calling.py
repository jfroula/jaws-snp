#!/usr/common/usg/languages/python/2.7-anaconda/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/19/18"

# Change to this for Cori /usr/common/software/python/2.7-anaconda-4.4/bin/python


# from hagsc_lib import testFile, testDirectory
# from hagsc_lib import iterFASTA, writeFASTA
# from hagsc_lib import deleteDir, deleteFile
# from hagsc_lib import generateTmpDirName
# from hagsc_lib import equal_size_split, baseFileName, getFiles
# 
# from gp_lib import create_sh_file, submitJob
# 
# from os.path import join, abspath, curdir,  realpath, isfile
# 
# 
# from sys import stderr, stdout
# 
# from os import mkdir, system

from optparse import OptionParser

from os.path import split, splitext, isfile, join, realpath

from sys import stdout

from os import system

#==============================================================
def echo_and_execute( cmd ):
    stdout.write( '\t-Executing:  %s\n' % cmd )
    system( cmd )
    return

#==============================================================
def real_main():
    pass

    # Defining the program options
    usage = "usage: %prog [Assembly FASTA] [INDEX name] [options]"

    parser = OptionParser(usage)

    def_percentMasking = 95
    parser.add_option( '-p', \
                       "--percentMasking", \
                       type    = "int", \
                       help    = "Percent of bases that remain unmask in Assembly FASTA.  Default: %d"%def_percentMasking, \
                       default = def_percentMasking )

    defSize = 24
    parser.add_option( '-m', \
                       "--kmerSize", \
                       type    = "int", \
                       help    = "kmer size:  Default=%d"%defSize, \
                       default = defSize )

    def_hashSize = '2g'
    parser.add_option( '-z', \
                       "--hashSize", \
                       type    = "str", \
                       help    = "kmer hash size:  Default=%s"%def_hashSize, \
                       default = def_hashSize )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        assemblyFASTA = realpath(args[0])
        if ( not isfile(assemblyFASTA) ): parser.error( '%s can not be found'%assemblyFASTA )

        INDEX_file = realpath(args[1])

    #####

    # (1)  Run the histogram
    basePath, assemblyFASTA = split(assemblyFASTA)
    histHashFileName = join( basePath, "%s.histHash" % splitext(assemblyFASTA)[0] )
    cmd = 'histogram_hashn -m %d -z %s -g -o %s %s' % ( options.kmerSize, options.hashSize, histHashFileName, assemblyFASTA )
    echo_and_execute( cmd )
    
    # (2)  Extract the "-t" value
    t_prev = None
    for line in open(histHashFileName):
        s = line.split(None)
        try:
            cumPer = float(s[3])
        except IndexError:
            continue
        #####
        if ( cumPer >= options.percentMasking ):
            final_t_value = t_prev
            break
        #####
        t_prev = int(s[0])
    #####
    
    # (3)  Perform the masking
    maskedFASTA = join( basePath, "%s.t%dmasked.fasta" % ( splitext(assemblyFASTA)[0], final_t_value ) )
    cmd = 'mask_repeats_hashn -m %d -z %s -t %d -L %s > %s' % ( options.kmerSize, options.hashSize, final_t_value, assemblyFASTA, maskedFASTA )
    echo_and_execute( cmd )

    # (4)  Indexing the masked FASTA file
    cmd = 'source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV/;bwa index -a bwtsw -p %s %s' % ( INDEX_file, maskedFASTA )
    echo_and_execute( cmd )

    # (4)  Set up the snps.config file with default values
    if ( not isfile('snps.config') ):
        oh = open( 'snps.config', 'w' )
        oh.write( 'RUNID          Place_RUNID_here\n' )
        oh.write( 'LIB_ID         Place_LIBID_here\n' )
        oh.write( 'REFERENCE      %s\n' % maskedFASTA )
        oh.write( 'ASSEMINDEX     %s\n' % INDEX_file )
        oh.write( 'READS          PATH/LIBID.prepped.fastq.bz2\n' )
        oh.write( 'MASKFILTER     0\n' )
        oh.write( 'GAPFILTER      25\n' )
        oh.write( 'EMAIL          YOUR_EMAIL_HERE\n' )
        oh.write( 'ALIGNER        MEM\n' )
        oh.write( 'CALLER         GATK\n' )
        oh.write( 'NUM_PAIRS      20000000\n' )
        oh.write( 'NUM_JOBS       30\n' )
        oh.write( 'ALIGN_ONLY     False\n' )
        oh.write( 'ALIGN_MEMORY   10G\n' )
        oh.write( 'COMBINED_FASTQ False\n' )
        oh.close()
    else:
        stdout.write( 'snps.config FILE ALREADY EXISTS\n')
        stdout.write( 'Exiting without overwriting the existing file\n')
    #####
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
