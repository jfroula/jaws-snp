#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$July 20, 2011"

# Local Library Imports
from hagsc_lib import remoteServer4
from hagsc_lib import defaultCluster
from hagsc_lib import deleteFile
from hagsc_lib import iterFASTA

# Python Library Imports
from optparse import OptionParser

from sys import stdout, stderr

from os.path import isfile, splitext, split

from os import system

import subprocess

#==========================================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [ASSEMBLY FASTA] [CONTAMINANT FASTA]"
    parser = OptionParser(usage)

    defSize = 18
    parser.add_option( '-m', \
                       "--kmerSize", \
                       type    = "int", \
                       help    = "kmer size:  Default=%d"%defSize, \
                       default = defSize )

    hashSize = '100m'
    parser.add_option( '-z', \
                       "--hashSize", \
                       type    = "str", \
                       help    = "kmer hash size:  Default=%s"%hashSize, \
                       default = hashSize )

    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        for n in xrange(2):
            if (not isfile(args[n])):
                parser.error( '%s can not be found'%args[n] )
            #####
        #####
        assemblyFile    = args[0]
        contaminantFile = args[1]
    #####
    
    # (1)  Masking the assembly file
    cmd = "mask_repeats_hash -B 10000 -m %d -t 1 -s \"\" -z %s -H %s %s"%(options.kmerSize, options.hashSize, contaminantFile, assemblyFile)

    # (2)  Write the header
    header = 6*['']
    header[0] = 'ScaffID'
    header[1] = 'scaffSize'
    header[2] = 'unMaskedBases'
    header[3] = 'maskedBases'
    header[4] = r'%maskedBases'
    stdout.write( '%s\n'%( '\t'.join(header) ) )

    # (2)  Perform the assessment
    stderr.write( 'Masking and parsing %s \n'%assemblyFile )
    tmpProcess = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    for record in iterFASTA(tmpProcess.stdout):
        scaffID       = record.id
        scaffSize     = len(record.seq)
        seqBases      = record.basePairs
        Ns            = record.N_bases
        Xs            = record.X_bases
        unMaskedBases = scaffSize - Ns
        maskedPer     = 100.0 * float(Xs) / float(unMaskedBases)
        stdout.write( '%s\t%d\t%d\t%d\t%.2f\n'%(record.id,scaffSize,seqBases,Xs,maskedPer) )
    #####
    tmpProcess.poll()

    # Output to stdout
    return

#####

if ( __name__ == '__main__' ):
    real_main()
