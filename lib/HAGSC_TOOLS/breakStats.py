
from os import popen
from os import system
from sys import argv

import re

rx_filename = re.compile(r"(.*/)*(?P<organism>[^.]+).*break.*-(?P<break>(\d+)|(map))")

rx_fastastats = re.compile(
	r"(?s).*scaffold total:\s*(?P<scaff_total>\d+).*contig total:\s*(?P<contig_total>\d+).*scaffold sequence total:\s*(?P<scaff_seq_total>[0-9.]+)\s*(?P<scaff_seq_unit>\w+).*contig sequence total:\s*(?P<contig_seq_total>[0-9.]+)\s*(?P<contig_seq_unit>\w+).*scaffold N/L50:\s*(?P<scaff_n_50>\d+)/(?P<scaff_l_50>[0-9.]+)\s*(?P<scaff_l_unit>\w+).*contig N/L50:\s*(?P<contig_n_50>\d+)/(?P<contig_l_50>[0-9.]+)\s*(?P<contig_l_unit>\w+).*scaffolds >\s*(?P<cutoff>.+):\s*(?P<num_cutoff>\d+).*main genome in scaffolds > \d+ ..?:\s*(?P<perc_cutoff>[0-9.]+)")

header_fields = ["organism", "break"]
    
fields = ["scaff_total", "contig_total", "scaff_seq_total", "scaff_seq_unit", "contig_seq_total", "contig_seq_unit", "scaff_n_50", "scaff_l_50", "scaff_l_unit", "contig_n_50", "contig_l_50", "contig_l_unit", "cutoff", "num_cutoff", "perc_cutoff"]

dir = ""
if len(argv) > 1: dir = argv[1]
command = "ls -x %s/%s" % (dir, "*.*break*.fasta")
filenames = popen(command, 'r').read().split()
print filenames

"""outfiles = {}
for filename in filenames:
    header_match = rx_filename.match(filename)
    organism = header_match.group("organism")
    if organism not in outfiles:
        outfiles[organism] = open("%s_fasta_stats.csv"%organism, 'w')
        outfiles[organism].write( ''.join( [';', ', '.join(header_fields), ', ', ', '.join(fields), '\n'] ) )"""

outfile = open("%s/all_fasta_stats.csv"%dir, 'w')
outfile.write( ''.join( [';', ', '.join(header_fields), ', ', ', '.join(fields), '\n'] ) )

for filename in filenames:
    print filename
    
    header_match = rx_filename.match(filename)
    organism = header_match.group("organism")
    #outfile = outfiles[organism]
    
    header = [header_match.group(field) for field in header_fields]
    outfile.write(', '.join(header)+', ')
    
    stats_filename = filename+".stats"
    system("fasta_stats %s > %s"%(filename, stats_filename))
    
    file = open(stats_filename, 'r')
    text = file.read()
    print text
    
    match = rx_fastastats.search(text)
    print match.groups()
    stats = [match.group(field) for field in fields]
    outfile.write(', '.join(stats)+'\n')
