#!/usr/common/usg/languages/python/2.7-anaconda/bin/python3
import subprocess

import os

from itertools import cycle

from collections import deque

from hapipe.functional import partial, mapV

from sys import argv, stdin, stdout, stderr

from os import system, curdir

from os.path import abspath

from hapipe import cluster

from time import time, sleep

import re

# Regular Expression
commify_re  = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
#=========================================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )
#=========================================================================
def iter_batchFasta( handle ):
    # Stat Variables
    global tot96mer, totReads, rejected
    rejected    = 0
    tot96mer    = 0
    totReads    = 0
    excludeDict = {}
    # Checking input        
    while True:
        line = handle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if (line != "") : break
    #####
    # Pulling and grouping Reads
    while True:
        if ( line == "" ): return
        # Pulling Data
        try:
            mer, name, seq = line.split(None)
        except ValueError:
            line = handle.readline()
            continue
        #####
        totReads += 1
        # Still the same mer 
        try:
            if mer == currentMer:
                try:
                    # Is the read an exact match to another read in the group
                    if excludeDict[seq]:
                        # Yes. Count it as a duplicate and move to the next read
                        oh_final.write('%s-R1\n%s-R2\n'%(name, name))
                        rejected += 1
                        line = handle.readline()
                        continue
                    #####
                except KeyError:
                    # No. All it to the list and dict to be processed
                    excludeDict[seq] = name
                    merList.append( ( name, seq ) )
                    line = handle.readline()
                    continue
                #####    
            else:
                listLen = len(merList)
                # Builds the bin 
                if listLen < 500 : 
                    currentMer = mer
                    tot96mer += 1
                    merList.append( ( name, seq ) )
                    line = handle.readline()
                    continue
                #####
                # Return the List and restarts the excludeDict
                yield merList
                merList  = []
                excludeDict = {}
                line = handle.readline()
                if ( not line ): return 
            #####   
        except NameError:
            # Getting the fist mer
            currentMer = mer
            tot96mer += 1
            merList    = []
            merList.append( ( name, seq ) )
            line = handle.readline()
            continue       
        #####
    #####

#=========================================================================
def writeAndEcho( outputString, oh ):
    oh.write( outputString )
    stdout.write( outputString )
    return 

#=========================================================================
def screenSingle(nameSet, readList, ID1, ID2, size2):
    # Add the name to the global set
    nameSet. add(ID2)
    for group in readList:
        # Find the local set and add the new name
        if ID1 not in group[0] : continue
        group[0].add(ID2)
        # Who is bigger?
        winTest = sorted([(size2, ID2), group[1]])
        group[1] = winTest[-1]
        break
    #####
    return
#=========================================================================
def screenIndex(readList, ID1):
    for index in range(len(readList)):
        if ID1 not in readList[index][0]: continue
        IDIndex = index
        break
    #####
    return IDIndex
#=========================================================================
def commands(fastaIndex, cpu_n, basePath):
    # Setting up the run commands
    blatCmd   = '/mnt/local/EXBIN/blat -noHead -t=dna -q=dna -tileSize=18 -extendThroughN tmp-%d.fasta tmp-%d.fasta stdout'%(fastaIndex, fastaIndex)
    awkCmd    = r"awk '{ if ( (\$10!=\$14) && (\$12<=1) && (\$16<=1) && (((\$1+\$4)/\$11) >= 0.98) ) print \$10,\$11,\$14,\$15 }'"
    sshCmd    = 'ssh -a -n -x -e none -o UsePrivilegedPort=no -o ServerAliveInterval=20 %s "cd %s'%(cpu_n, basePath) 
    cmdString = '%s ; %s | %s"'%(sshCmd, blatCmd, awkCmd)
    return (cmdString)

#=========================================================================
def makeCommands(batchSize, batch_iterator, cpu):
    for fastaIndex in range(batchSize):
        yield commands( fastaIndex, batch_iterator, cpu )
#==========================================================================
def logAndReport( cmdString ):
    log_out = open('predup.cmgLog','a')
    stderr.write(cmdString)
    log_out.write(cmdString)
    log_out.close()
#==========================================================================
def runCommand( cmdString, fastaIndex, readBatch):
    # Writing to the fasta file
    tmp_oh = open( 'tmp-%d.fasta'%fastaIndex, 'w' )
    for name, seq in readBatch: tmp_oh.write('>%s\n%s\n'%(name,seq))
    tmp_oh.close()
    process = subprocess.Popen( cmdString, shell=True, stdout=subprocess.PIPE )
    return  ([(line for line in process.stdout),  process, cmdString])
#=========================================================================
def newRunCommand( cmdString):
    # Making a New call for a call that failed
    process = subprocess.Popen( cmdString, shell=True, stdout=subprocess.PIPE )
    return  [(line for line in process.stdout),  process, cmdString]
#=========================================================================
def real_main():

    # Global variables for stats
    global oh_final, rejected
    
    # Process Parameters
    batchSize = int(argv[1])  
    currentCluster = argv[2]
    
    # Stats information
    screened = 0
    retained = 0
    batchNum = batchSize
    
    # Capturing the standard input
    oh_stdin       = stdin
    batch_iterator = iter_batchFasta( oh_stdin )    
    
    # Acquiring locks for the necessary machines
    cpu     = cluster.acquire_cpus( currentCluster, batchSize )
    badDict = {}
    badSet  = set()
    tmpDict = {}
    for num in range(batchSize): tmpDict[num] = True 
    
    # Starting our timer    
    start = time()

    # Opening up the files needed
    exclusionFile = 'excludedDuplicateReads.dat'
    oh_final = open(exclusionFile , 'w')
    log_oh   = open('predup.cmgLog','w')
    log_oh.close()
    
    # Initializing the running jobs
    basePath    = abspath(curdir)
    
    # Starting Job Queue
    commandList    = [ commands(n, cpu[n], basePath) for n in range(batchSize) ]
    param          = zip( cycle(commandList), cycle(range(batchSize) ), batch_iterator )
    jobList        = mapV( runCommand, param )
    runningJobs    = deque( next(jobList) for n in range(batchSize) )

    print("\tBlat and Screen")
    count = 0
    while True:
        try:
            # Pull the output and the process
            x    = runningJobs.popleft()
            proc = x[1]
        except IndexError:
            break
        #####
        # Screen the output
        nameSet = set()
        readList = []
        for line in x[0]:
            line = line.decode('utf-8')
            try:
                queryID, querySize, targetID, targetSize = line.split(None)
            except ValueError:
                continue
            #####
            qInSet = queryID in nameSet
            tInSet = targetID in nameSet
            nameSet. add(queryID)
            # Both are in a set already          
            if (qInSet and tInSet):
                # Get the tuple index
                targetIndex= screenIndex(readList, targetID)
                queryIndex = screenIndex(readList, queryID) 
                # Are they the same?
                if (queryIndex == targetIndex): continue
                # Collapse the lists
                newSet   = readList[targetIndex][0].union(readList[queryIndex][0])
                winTest  = sorted([readList[targetIndex][1],readList[queryIndex][1]])
                newWin   = winTest[-1]
                for popIndex in sorted([targetIndex, queryIndex], reverse=True): readList.pop(popIndex)
                readList.append([newSet, newWin])
            # Only the query is in a set
            elif(qInSet):
                screenSingle(nameSet, readList, queryID, targetID, targetSize)
            # Only the target is in a set
            elif(tInSet):
                screenSingle(nameSet, readList, targetID, queryID, querySize)               
            # Need a new tuple
            else:# Check sizes and make a new tuple and append it to the list
                # Add names to the sets
                newSet = set([queryID, targetID])
                nameSet.add(queryID)
                nameSet.add(targetID)
                # Which is bigger
                winTest =  sorted([(querySize, queryID), (targetSize, targetID)])
                winner  = winTest[-1]
                # Add it to the list
                readList.append([newSet, winner])
            #####
        #####
        # Remove the uniqe names from the set 
        for group in readList: nameSet.remove(group[1][1])
        screened += len(nameSet)
        # Print the duplicate read names
        for name in nameSet: oh_final.write('%s-R1\n%s-R2\n'%(name, name))
        oldString = x[2]
        fastaID   = int((oldString.split(None)[20].split('-')[1]).replace('.fasta',''))
        # Kill the process(Prevents Zombie process) or restarts it if it failed
        if (proc.poll() != 0):
            badCPU    = oldString.split(None)[10]
            badSet.add(badCPU)
            if (tmpDict[fastaID]):
                tmpDict[fastaID] = False
                try:
                    newCPU = badDict[badCPU]
                    if (newCPU in badSet): raise KeyError
                except KeyError:
                    newCPU = str(cluster.acquire_cpus( currentCluster, 1)[0])
                    badDict[badCPU] = newCPU
                #####
                newCMD = oldString.replace(badCPU, newCPU)
                runningJobs.append(newRunCommand(newCMD))
                logAndReport("Blat on %s was restarted on %s\n"%(badCPU, newCPU))
                continue
            else:
                system('mv tmp-%d.fasta badGroup-%d.fasta'%(fastaID, batchNum))
                logAndReport('moved tmp-%d.fasta to badGroup-%d.fasta\n'%(fastaID, batchNum))
            #####
        #####    
        # Prints an update every 100,000 reads 
        if (batchNum%200 == 0):
            totRej =  float(rejected)+ float(screened)
            perdup = totRej/ float(totReads)*100.0
            print("\tReads Processed: %s\n\tDuplicates found: %s\n\tPercent duplicate %.2f\n"%(commify(totReads), commify(int(totRej)), perdup))
        #####
        # Are there any more job?
        try:
            sleep(.05)
            runningJobs.append(next(jobList))
            tmpDict[fastaID] = True
            batchNum += 1       
        except StopIteration:
            pass
        #####
    #####
    # Clean up and sumation
    oh_final.close()
    system('rm -f tmp-*.fasta')
    oh_sum   = open('predupSummary.dat', 'w')
    binAvg   = (float(totReads) - float(rejected))/float(batchNum)
    totRejec = rejected + screened
    retained = totReads - totRejec
    unique   = float(retained)/float(totReads)*100
    runTime  = time() - start
    writeAndEcho( "===============================\n", oh_sum )
    writeAndEcho( "SUMMARY:\n", oh_sum )
    writeAndEcho( "Total 96mers\t%s\n"%commify(int(tot96mer)), oh_sum )
    writeAndEcho( "Total Pairs\t%s\n"%commify(int(totReads)), oh_sum )
    writeAndEcho( "Pairs Rejected\t%s\n"%commify(int(totRejec)), oh_sum )
    writeAndEcho( "Pairs Retained\t%s\n"%commify(int(retained)), oh_sum )
    writeAndEcho( "Percent Unique\t%.2f\n"%unique, oh_sum )
    writeAndEcho( "Avg Batch Size\t%.2f\n"%binAvg, oh_sum )
    writeAndEcho( "Run Time\t%.2fs\n"%runTime, oh_sum )
    writeAndEcho( "===============================\n", oh_sum )
    oh_sum.close()
    return
    
#==============================================================    
if ( __name__ == '__main__' ):
    real_main()