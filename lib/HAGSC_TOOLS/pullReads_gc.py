#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Nov 8, 2010 9:50:17 AM$"

from hagsc_lib import isGzipFile, isBzipFile

# Python Library Imports
from optparse import OptionParser

from sys import stdout
from sys import stderr

from os.path import isfile

import re

from math import fmod

import subprocess

#=====================================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA file] [GC% lower bound] [GC% upper bound]"

    parser = OptionParser(usage)

    parser.add_option( "-e", \
                       "--excludeReadsLessThanThis", \
                       type    = 'int', \
                       help    = "Exclude reads less than this length.  Default=Include entire read.", \
                       default = -1 )

    parser.add_option( "-i", \
                       "--initialBases", \
                       type    = 'int', \
                       help    = "Number of initial bases to examine for GC content.  Not used as a screening criteria!!  Reads can have fewer bases than this and still be used in the computation.  Default=Include entire read.", \
                       default = 10000000 )
                       
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileName
        if ( isfile(args[0]) ):
            fastaFile = args[0]
        else:
            parser.error( 'File:  %s is not found.'%args[0] )
        #####
        
        if ( float(args[1]) > 0.0 ):
            lower_GC = float(args[1])
        else:
            parser.error( 'GC%% lower bound must be > 0.0' )
        #####
        
        if ( float(args[2]) > 0.0 ):
            upper_GC = float(args[2])
        else:
            parser.error( 'GC%% upper bound must be > 0.0' )
        #####
        
    #####
    
    if ( options.initialBases > 0 ):
        initialBases = options.initialBases
    else:
        parser.error( 'Initial bases must be > 0' )
    #####

    # Looking for the exclusion    
    excludeReads = False
    minReadSize  = -1
    if ( options.excludeReadsLessThanThis > 0 ):
        excludeReads = True
        minReadSize  = options.excludeReadsLessThanThis
    #####
    
    # Regular expression for finding GC
    commify = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))')
    
    # Outputting a list of reads
    tmpCounter = 1
    readCutoff = 100

    # Opening a handle to the file
    if ( isGzipFile(fastaFile) ):
        fastaFileHandle = subprocess.Popen( 'cat %s | gunzip -c'%fastaFile, shell=True, stdout=subprocess.PIPE ).stdout
    elif ( isBzipFile(fastaFile) ):
        fastaFileHandle = subprocess.Popen( 'cat %s | bunzip2 -c'%fastaFile, shell=True, stdout=subprocess.PIPE ).stdout
    else:
        fastaFileHandle = open( fastaFile, 'r' )
    #####
    seq        = []
    tmpCounter = 0

    # Skipping any blank lines or comments at the top of the file
    while True:
        line = fastaFileHandle.readline()
        if ( line[0] == ">" ):
            break
        #####
    #####

    # Reading the contents of the FASTA file
    while True:

        # Pulling the id
        splitText = line[1:].split(None,1)
        try:
            scaffold_id = splitText[0]
        except IndexError:
            raise ValueError( "gc_histogram has detected an empty scaffold ID." +
                              " Make sure all scaffolds have IDs." )
        #####

        # Reading the sequence
        seq = []
        line  = fastaFileHandle.readline()
        while True:
            # Termination conditions
            if not line : break
            if (line[0] == '>'): break
            # Removes internal and trailing whitespace and floating '\r'
            seq.append( line.strip().upper() )
            # Reading the next line
            line = fastaFileHandle.readline()
        #####
        
        # Checking the read size
        tmpSeq = ''.join(seq)
        nSize  = len( tmpSeq )
        if ( excludeReads and ( nSize < minReadSize ) ):
            if ( not line ):
                break
            else:
                continue
            #####
        #####
        
        # Computing the GC content
        nA     = tmpSeq[:initialBases].count('A')
        nC     = tmpSeq[:initialBases].count('C')
        nG     = tmpSeq[:initialBases].count('G')
        nT     = tmpSeq[:initialBases].count('T')
        nBases = nA + nC + nG + nT
        gcPer = 100 * float( nG + nC ) / float( nBases )
        if ( (gcPer >= lower_GC) and (gcPer <= upper_GC) ):
            stdout.write( '%s\n'%scaffold_id )
        #####
        # Providing an update to the user
        tmpCounter += 1
        if ( fmod( tmpCounter, readCutoff ) == 0.0 ): 
            tmpStr = commify.sub( ',', '%d'%(tmpCounter) )
            stderr.write( '%s reads processed\n'%tmpStr )
            if ( readCutoff < 1000000 ): readCutoff *= 10
        #####
        # Stopping the iteration
        if ( not line ):
            break
        ####
    #####

    return

#####
    
def profile_main():
    from cProfile import Profile
    from pstats import Stats
    prof  = Profile().runctx("real_main()", globals(), locals())
    stats = Stats( prof ).sort_stats("time").print_stats(20)
    return
#####

if ( __name__ == '__main__' ):
    real_main()
