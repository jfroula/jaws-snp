__author__="jjenkins"
__date__ ="$Oct 1, 2010 9:25:51 AM$"

from hagsc_lib import generateTrimmedFileName
from hagsc_lib import mask_repeats_hash_path
from hagsc_lib import generateCloneFASTAFile
from hagsc_lib import bestHit_headerString
from hagsc_lib import trimAssemblyFile
from hagsc_lib import fileHeader_Class
from hagsc_lib import blastn_bestHit
from hagsc_lib import histogramClass
from hagsc_lib import defaultCluster
from hagsc_lib import remoteServer4
from hagsc_lib import baseFileName
from hagsc_lib import DNA_best_hit
from hagsc_lib import iterBestHit
from hagsc_lib import deleteFile

from os.path import isfile
from os.path import isdir

import re

# Python Library Imports
from optparse import OptionParser

########################################################################
class placementPair( object ):

    def __init__(self, hit_n, hit_m):

        self.hit_n = hit_n
        self.hit_m = hit_m
        self.assessment   = None
        self.bridgeLength = None

        # Evaluate pair automaticly
        self.assessPair()

    def assessPair(self):

        # Who comes first?
        if ( self.hit_n.scaffStart < self.hit_m.scaffStart ):
            lowHit  = self.hit_n
            highHit = self.hit_m
        else:
            lowHit  = self.hit_m
            highHit = self.hit_n
        #####

        # Do the alignments overlap?
        if ( highHit.scaffStart > lowHit.scaffEnd ):
            # Pull the clone contig type
            lowHitType = None
            if ( lowHit.BAC_recordName[0] != 'C' ):
                lowHitType = lowHit.BAC_recordName[0]
            #####
            highHitType = None
            if ( highHit.BAC_recordName[0] != 'C' ):
                highHitType = highHit.BAC_recordName[0]
            #####
            # Pulling the placement direction
            low_dir  = lowHit.placDir
            high_dir = highHit.placDir
            # Check to see if they are in the proper direction
            if ( (lowHitType != None) and (highHitType != None) ):
                # If they are same type, the direction must be opposite else you
                # have overlapping clones
                if ( (lowHitType == highHitType) and (low_dir != high_dir) ):
                    # However, the lowHit must point '+' for 'R' and '-' for 'L'
                    if ( ( (lowHitType == 'R') and (low_dir == '+') ) or \
                         ( (lowHitType == 'L') and (low_dir == '-') ) ):
                        self.assessment   = 'good'
                        self.bridgeLength = highHit.scaffStart - lowHit.scaffEnd
                        return
                    else:
                        self.assessment = 'Clone_contigs_overlap'
                        return
                    #####
                # If they are opposite type, the direction must be same else you
                # have overlapping clones
                elif ( (lowHitType != highHitType) and (low_dir == high_dir) ):
                    if ( ( (lowHitType == 'R') and (low_dir == '+') ) or \
                         ( (lowHitType == 'L') and (low_dir == '-') ) ):
                        self.assessment   = 'good'
                        self.bridgeLength = highHit.scaffStart - lowHit.scaffEnd
                        return
                    else:
                        self.assessment = 'Clone_contigs_overlap'
                        return
                    #####
                else:
                    self.assessment = 'Clone_contigs_overlap'
                    return
                #####
            # Covers the case where the low hit is a small contig
            elif( (lowHitType == None) and (highHitType != None) ):
                # If high is L then it must point +
                if ( ( (highHitType == 'L') and (high_dir == '+') ) or \
                     ( (highHitType == 'R') and (high_dir == '-') ) ):
                    self.assessment   = 'good'
                    self.bridgeLength = highHit.scaffStart - lowHit.scaffEnd
                    return
                else:
                    self.assessment = 'Clone_contigs_overlap'
                    return
                #####
            # Covers the case where the high hit is a small contig
            elif( (lowHitType != None) and (highHitType == None) ):
                # If high is L then it must point +
                if ( ( (lowHitType == 'R') and (low_dir == '+') ) or \
                     ( (lowHitType == 'L') and (low_dir == '-') ) ):
                    self.assessment   = 'good'
                    self.bridgeLength = highHit.scaffStart - lowHit.scaffEnd
                    return
                else:
                    self.assessment = 'Clone_contigs_overlap'
                    return
                #####
            # Covers the case where both are small contigs
            elif( (lowHitType == None) and (highHitType == None) ):
                self.assessment   = 'good'
                self.bridgeLength = highHit.scaffStart - lowHit.scaffEnd
                return
            #####
        else:
            self.assessment = 'Placements_Overlap'
            return
        #####


########################################################################
class bridgeGaps_class( object ):

    def __init__(self, assemblyFile, cloneNum, cloneFASTAFile, clusterNum, \
                       numProcs, kmer_percent, ta_Length, \
                       trim_length, placType):

        self.assemblyFile   = assemblyFile
        self.cloneNum       = cloneNum
        self.cloneFASTAFile = cloneFASTAFile
        self.clusterNum     = clusterNum
        self.numProcs       = numProcs
        self.kmer_percent   = kmer_percent
        self.ta_Length      = ta_Length
        self.trim_length    = trim_length
        self.placType       = placType

        # Initializing the output file names
        self.trimmedCloneFile = None
        self.baseName         = baseFileName(self.assemblyFile)
        if ( self.cloneNum != None ):
            self.placOutputFile   = 'clone%s_%s_gapFiller.%s_out'%(self.cloneNum, \
                                                                self.baseName, \
                                                                 self.placType )
            self.bridgeOutputFile = 'clone%s_%s_%s_gapFiller.gap'%(self.cloneNum, \
                                                                self.baseName, \
                                                                 self.placType )
            self.assemPlacOutputFile = 'clone%s_%s_kmerHits.out'%(self.cloneNum, \
                                                                 self.baseName )
        else:
            fastaBase = baseFileName(self.cloneFASTAFile)
            self.placOutputFile   = '%s_%s_gapFiller.%s_out'%(fastaBase, \
                                                                    self.baseName, \
                                                                 self.placType )
            self.bridgeOutputFile = '%s_%s_%s_gapFiller.gap'%(fastaBase, \
                                                                self.baseName, \
                                                                 self.placType )
            self.assemPlacOutputFile = '%s_%s_kmerHits.out'%(fastaBase, \
                                                                 self.baseName )
        #####

        # Assessment related variables
        self.goodPairs    = []
        self.flaggedPairs = []

        # Regular Expression for extracting contig indicees
        self.pattern = re.compile( 'Contig(\d+)' )

        # Run the code
        self.findBridges()

    def findBridges(self):

        # (1) Generate the FASTA File
        if ( self.cloneNum != None ):
            self.cloneFASTAFile = generateCloneFASTAFile(self.cloneNum)
        #####

        # (2) Trim the FASTA File
        self.trimFASTAFile()

        # (3) Best hit the FASTA File
        self.bestHitFASTAFile()

        # Sorting out the hits
        self.findingGoodPairs()

        # Placing assembly back on clone
        self.kmer_HitAssembly()
        
        return

    def kmer_HitAssembly(self):
    
        # Generate %masked on assembled scaffold list
        maskedOutputFile = '%s.kmermasked'%self.assemblyFile
        deleteFile( maskedOutputFile )
        outputString = 7 * ['']
        outputString[0] = mask_repeats_hash_path
        outputString[1] = '-t 1'
        outputString[2] = '-u 2'  # Masking only off of unique kmers
        outputString[3] = '-m 24'
        outputString[4] = '-z 3m'
        outputString[5] = '-F'
        outputString[6] = '-H %s %s'%(self.cloneFASTAFile,self.assemblyFile)
        commandList = [' '.join(outputString)]
        remoteServer4( commandList, 1, clusterNum=101, perlFile='masking.pl' )
        deleteFile( 'masking.pl' )
        
        # Reading in the output from the assembly masking
        maskedContigs = {}
        localHist = histogramClass(0.0, 100.0, 100)
        for line in open( maskedOutputFile, 'r'):
            contigID, masked_per = line.split(None)
            masked_per = float(masked_per[:-1])
            maskedContigs[contigID] = masked_per
            localHist.addData( masked_per )
        #####
        deleteFile( maskedOutputFile )
        
        # Output the histogram to the output file
        histFileName = 'scaffold_perMaskedHist.dat'
        deleteFile( histFileName )
        tmpHandle = open( histFileName, 'w' )
        tmpHandle.write( localHist.generateOutputString() )
        tmpHandle.close()

        # Developing the header
        strLen          = 2
        columnLabel     = strLen * ['']
        columnLabel[0]  = 'assembledContig'
        columnLabel[1]  = r'kmer%'
        tmpHeader = fileHeader_Class( columnLabel, [], \
                                      tabDelimited=True ).generateHeaderString()
        
        # Output to the user
        outputHandle = open( self.assemPlacOutputFile, 'w' )
        outputHandle.write( tmpHeader  )
        for contigID, maskedPer in maskedContigs.iteritems():
            if ( maskedPer >= self.kmer_percent ):
                outputHandle.write( '%s\t%.2f\n'%(contigID,maskedPer) )
            #####
        #####
        outputHandle.close()
        return

    def trimFASTAFile(self):
        # Trim the ends off of the clone file
        trimAssemblyFile( self.cloneFASTAFile, \
                          trimLength = self.trim_length, \
                          taLength   = self.ta_Length)
        self.trimmedCloneFile = generateTrimmedFileName(self.cloneFASTAFile)
        return

    def bestHitFASTAFile(self):
        # Performing the placements
        if ( self.placType == 'blat' ):
            DNA_best_hit( self.trimmedCloneFile, \
                          self.assemblyFile, \
                          nFastaFiles=self.numProcs, \
                          clusterNum=self.clusterNum, \
                          cleanUp=True, \
                          allowMultipleTopHits=True, \
                          keyWords = {'minScore':30}, \
                          outputFileName= self.placOutputFile )
        elif ( self.placType == 'blast' ):
            blastn_bestHit( self.trimmedCloneFile, \
                            self.assemblyFile, \
                            nFastaFiles=self.numProcs, \
                            clusterNum=self.clusterNum, \
                            cleanUp=True, \
                            cov_cutoff=85.0, \
                            ID_cutoff=90.0, \
                            outputFileName= self.placOutputFile )
        #####
        return

    def findingGoodPairs(self):
        # Sorting the hits by assembled contig name
        matchSets = {}
        for record in iterBestHit( open(self.placOutputFile,'r') ):
            assembledContigName = record.scaffold
            try:
                matchSets[assembledContigName].append( record )
            except KeyError:
                matchSets[assembledContigName] = [ record ]
            #####
        #####

        # Finding all good pairs for each assembled contig
        goodPairs    = {}
        flaggedPairs = {}
        for assembledContigName, hitList in matchSets.items():
            # Checking the size
            nHits = len(hitList)
            if ( nHits < 2 ): continue
            # Loop over all pairs
            for n in xrange( nHits - 1 ):
                hit_n = hitList[n]
                nIndex = int( self.pattern.findall( hit_n.BAC_recordName )[0] )
                for m in xrange( n+1, nHits ):
                    hit_m = hitList[m]
                    mIndex = int( self.pattern.findall( hit_m.BAC_recordName )[0] )
                    putativePair = placementPair(hit_n, hit_m)
                    if ( putativePair.assessment == 'good' ):
                        try:
                            goodPairs[nIndex].append( (hit_n, hit_m, putativePair.bridgeLength) )
                        except KeyError:
                            goodPairs[nIndex] = [ (hit_n, hit_m, putativePair.bridgeLength) ]
                        #####
                        try:
                            goodPairs[mIndex].append( (hit_n, hit_m, putativePair.bridgeLength) )
                        except KeyError:
                            goodPairs[mIndex] = [ (hit_n, hit_m, putativePair.bridgeLength) ]
                        #####
                    else:
                        try:
                            flaggedPairs[putativePair.assessment].append( (hit_n, hit_m) )
                        except KeyError:
                            flaggedPairs[putativePair.assessment] = [ (hit_n, hit_m) ]
                        #####
                    #####
                #####
            #####
        #####

        # Sorting the good hits
        goodHitIndicees = goodPairs.keys()
        goodHitIndicees.sort()
        goodHitOutputString = []
        numGoodHits         = 0
        pairSet             = set()
        outputStringFormat = '%s\t%d\t%d\t%s\t%d\t%d\t%s\t%d\t%d\t%s\n'
        for goodHitIndex in goodHitIndicees:
            numGoodHits += len( goodPairs[goodHitIndex] )
            for hit_n, hit_m, bridgeLength in goodPairs[goodHitIndex]:
                s1 = hit_n.BAC_recordName
                s2 = hit_m.BAC_recordName
                tmpPairName = self.generatePairName(s1, s2)
                if ( tmpPairName not in pairSet ):
                    tmpString = outputStringFormat%( hit_n.BAC_recordName, \
                                                     hit_n.scaffStart, \
                                                     hit_n.scaffEnd, \
                                                     hit_m.BAC_recordName, \
                                                     hit_m.scaffStart, \
                                                     hit_m.scaffEnd, \
                                                     hit_n.scaffold, \
                                                     hit_n.scaffSize, \
                                                     bridgeLength, \
                                                     'good')
                    goodHitOutputString.append( tmpString )
                    pairSet.add(tmpPairName)
                #####
            #####
        #####

        # Writing the flagged hits
        flaggedHitOutputString = []
        numFlaggedHits         = 0
        pairSet                = set()
        for flaggedReason, hitList in flaggedPairs.items():
            flaggedHitOutputString.append( '%s\n'%( 50*'=' ) )
            flaggedHitOutputString.append( 'Reason = %s\n'%flaggedReason )
            numFlaggedHits += len(hitList)
            bridgeLength = 0
            for hit_n, hit_m in hitList:
                s1 = hit_n.BAC_recordName
                s2 = hit_m.BAC_recordName
                tmpPairName = self.generatePairName(s1, s2)
                if ( tmpPairName not in pairSet ):
                    tmpString = outputStringFormat%( hit_n.BAC_recordName, \
                                                     hit_n.scaffStart, \
                                                     hit_n.scaffEnd, \
                                                     hit_m.BAC_recordName, \
                                                     hit_m.scaffStart, \
                                                     hit_m.scaffEnd, \
                                                     hit_n.scaffold, \
                                                     hit_n.scaffSize, \
                                                     bridgeLength, \
                                                     flaggedReason)
                    flaggedHitOutputString.append( tmpString )
                    pairSet.add(tmpPairName)
                #####
            #####
        #####

        # Opening the output file
        outputFileHandle = open( self.bridgeOutputFile, 'w' )

        # Writing the header
        outputFileHandle.write( 'CLONE BRIDGING PARAMETERS:\n' )
        outputFileHandle.write( '\t-assemblyFile = %s\n'%self.assemblyFile )
        outputFileHandle.write( '\t-cloneNumber  = %s\n'%self.cloneNum )
        outputFileHandle.write( '\t-trimLength   = %d\n'%self.trim_length )
        outputFileHandle.write( '\t-ta_Length    = %d\n'%self.ta_Length )
        outputFileHandle.write( '\t-placType     = %s\n'%self.placType )
        outputFileHandle.write( '\t-clusterNum   = %d\n'%self.clusterNum )
        outputFileHandle.write( '\t-numProcs     = %d\n'%self.numProcs )
        outputFileHandle.write( '\t-Good Hits    = %d\n'%numGoodHits )
        outputFileHandle.write( '\t-Flagged Hits = %d\n\n'%numFlaggedHits )

        # Writing the good hits
        outputFileHandle.write( '%s\n'%( 50*'=' ) )
        outputFileHandle.write( 'GOOD HITS\n')
        outputFileHandle.write( '%s\n'%( 50*'=' ) )
        outputFileHandle.write( self.localHeaderString() )
        for tmpStr in goodHitOutputString: outputFileHandle.write( tmpStr )

        # Writing the flagged hits
        outputFileHandle.write( '\n%s\n'%( 50*'=' ) )
        outputFileHandle.write( 'FLAGGED HITS\n')
        outputFileHandle.write( '%s\n'%( 50*'=' ) )
        outputFileHandle.write( self.localHeaderString() )
        for tmpStr in flaggedHitOutputString: outputFileHandle.write( tmpStr )

        # Closing the output file
        outputFileHandle.close()

        return

    def generatePairName(self, s1, s2):
        x = [s1,s2]
        x.sort()
        return '_|_'.join(x)

    def localHeaderString(self):
        # Header Information
        strLen          = 12
        columnLabel     = strLen * ['']
        columnLabel[0]  = 'cloneContig[1]'
        columnLabel[1]  = 'start[2]'
        columnLabel[2]  = 'end[3]'
        columnLabel[3]  = 'cloneContig[4]'
        columnLabel[4]  = 'start[5]'
        columnLabel[5]  = 'end[6]'
        columnLabel[6]  = 'assemContig[7]'
        columnLabel[7]  = 'size[8]'
        columnLabel[8]  = 'bridgeLength[9]'
        columnLabel[9]  = 'type[10]'
        return fileHeader_Class( columnLabel, [], \
                                     tabDelimited=True).generateHeaderString()

    def bestHitAssembly(self):
        # Performing the placements
        tmpOutputName = 'tmpOutput.out'
        DNA_best_hit( self.assemblyFile, \
                      self.cloneFASTAFile, \
                      nFastaFiles=self.numProcs, \
                      clusterNum=self.clusterNum, \
                      cleanUp=True, \
                      outputFileName = tmpOutputName, \
                      tmpDirNum=2)
        # Applying the constraints
        outputHandle = open( self.assemPlacOutputFile, 'w' )
        outputHandle.write( bestHit_headerString() )
        for record in iterBestHit( open(tmpOutputName, 'r') ):
            if ( record.per_ID >= self.ID_percent ):
                outputHandle.write( record.generateString() )
            #####
        #####
        outputHandle.close()
        deleteFile(tmpOutputName)
        return
        
########################################################################
def real_main():

    # Defining the program options
    usage = "usage: %prog [assemblyFile] ([HAGSC CLONE NUMBER] OR [FASTA FILE]) [options]"

    parser = OptionParser(usage)

    parser.add_option( "-L", \
                       "--trimLength", \
                       type    = 'int', \
                       help    = "Number of bases to trim off the ends of the clone for placement, Default: 500", \
                       default = 500 )

    parser.add_option( "-t", \
                       "--throwAwayLength", \
                       type    = 'int', \
                       help    = "Num bases to ignore on clone contig ends" + \
                                 "Default: 200", \
                       default = 200 )

    parser.add_option( "-c", \
                       "--clusterNum", \
                       type    = 'int', \
                       help    = "Cluster to Use, Default: %d"%defaultCluster, \
                       default = defaultCluster )

    parser.add_option( "-p", \
                       "--numCPUs", \
                       type    = 'int', \
                       help    = "Number of CPUs, Default: 1", \
                       default = 1 )

    parser.add_option( "-k", \
                       "--kmer_per", \
                       type    = 'float', \
                       help    = "Kmer% cutoff, Default: 25.0", \
                       default = 25.0 )

    parser.add_option( "-b", \
                       "--useBLAST", \
                       action  = 'store_true', \
                       dest    = 'useBLAST', \
                       help    = "Uses blast for placements instead of blat:  Default=False" )
    parser.set_defaults( useBLAST = False )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Testing the assembly file
        if ( not isfile(args[0]) ):
            parser.error( 'File not found:  %s'%args[0] )
        #####
        assemblyFile = args[0]
        # Determining whether we have a FASTA file or a clone number
        if ( isfile(args[1]) ):
            cloneFASTAFile = args[1]
            cloneNum       = None
        else:
            # Testing the clone number
            tmpDir = '/mnt/mongo3/doeassem/%s/edit_dir'%args[1]
            if ( not isdir(tmpDir) ):
                parser.error( 'Clone directory not found: %s'%tmpDir )
            #####
            cloneNum       = int( args[1] )
            cloneFASTAFile = None
        #####
    #####

    if ( options.clusterNum > 0):
        clusterNum = options.clusterNum
    else:
        parser.error( 'Cluster number must be > zero.' )
    #####

    if ( options.numCPUs > 0):
        numProcs = options.numCPUs
    else:
        parser.error( 'Number of CPUs be > 0' )
    #####

    if ( options.kmer_per >= 0):
        kmer_percent = options.kmer_per
    else:
        parser.error( 'ID% must be > 0' )
    #####

    if ( options.throwAwayLength >= 0):
        ta_Length = options.throwAwayLength
    else:
        parser.error( 'Throw away length must be >= zero.' )
    #####

    if ( options.trimLength > 0):
        trim_length = int(options.trimLength)
    else:
        parser.error( 'Clone contig trim length must be > 0.' )
    #####

    placType = ( options.useBLAST and ['blast'] or ['blat'] )[0]

    bridgeGaps_class( assemblyFile, cloneNum, cloneFASTAFile, clusterNum, \
                      numProcs, kmer_percent, ta_Length, trim_length, placType )

########################################################################
if ( __name__ == '__main__' ):
    real_main()
