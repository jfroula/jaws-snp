#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Oct 17, 2013$"
#=========================================================================
# EDIT LOG
# This program assumes that you have done nothing to the file
#      
#      
#      
#=========================================================================
from optparse import OptionParser
from sys import  stderr
from os import system
import subprocess
import bz2
from array import array
from os.path import isfile
from hagsc_lib import isGzipFile, isBzipFile
from hagsc_lib import iterCounter, revComp
#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    if ( isGzipFile(fileName) ): return 'cat %s | gunzip -c '%( fileName)
    if ( isBzipFile(fileName) ): return 'cat %s | bunzip2 -c '%( fileName)
    return 'cat %s '%( fileName)
#==============================================================
def real_main():
    usage = "usage: %prog [FASTA FILE][BASE NAME][READ LENGTH][options]"
    
    parser = OptionParser(usage)
    
    windowDefault = 15
    parser.add_option( '-w', \
                       "--window", \
                       type    = 'int', \
                       help    = 'How many bases from the terminal event to be examined.Default: %d'%(windowDefault), \
                       default = windowDefault )
                       
    kmerDefault = 12
    parser.add_option( '-k', \
                       "--kmerSize", \
                       type    = 'int', \
                       help    = 'Size of kmer to use. Default: %d'%(kmerDefault), \
                       default = kmerDefault )
    
    qualDefault = 30
    parser.add_option( '-q', \
                       "--qualScore", \
                       type    = 'int', \
                       help    = 'Quality score to use in clip command. Default: %d'%(qualDefault), \
                       default = qualDefault )
                                          
    rangeDefault = 100
    parser.add_option( '-e', \
                       "--effect", \
                       type    = 'int', \
                       help    = 'Number of positions to use to determine effect of kmers. Default: %d.'%(rangeDefault), \
                       default = rangeDefault )
                       
    examineDefault = 10
    parser.add_option( '-p', \
                       "--plot", \
                       type    = 'int', \
                       help    = 'Number of effective kmers to plot. Default: %d.'%(examineDefault), \
                       default = examineDefault )
                       
                       
    # Parsing the arguements
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # User influenced variables
        unclippedCMD  = testFile(args[0]) 
        baseName      = args[1] 
        maxReadLen    = int(args[2])
        windowSize    = options.window
        kmerSize      = options.kmerSize
        if (kmerSize>windowSize):parser.error( 'Window size (-w) must be larger than kmer size (-k)' )
        clippedCMD    = '/mnt/local/EXBIN/clip -B 100000 -b -c %d -L 500 -f %d %s'%(windowSize, options.qualScore, args[0])
        PKTrange      = options.effect
        examineNumber = options.plot
        wholeReadKmer = maxReadLen + 1 - kmerSize
        clipReadKmers = windowSize + 1 - kmerSize
    #####
    
    # Variables used in program
    terminalEvent = 0.0
    termList      = array('L', [0]*maxReadLen)
    PKDict        = {}
    PKTDict       = {}
    unclipDict    = {}
    effectedList  = []
    x             = iterCounter(1000000)
    
    # Quick functions to save time
    createPair    = lambda mer: '_'.join(sorted([mer,revComp(mer)]))
    changeName    = lambda kmer: kmer.replace("_","-")
    splitLine     = lambda lineData: lineData.split(None)[0]
    padDict       = {0:lambda seq:seq}
    getR          = lambda lineData: lineData.split('-R')[1]
    
    # Starts the clipping
    unclipProc = subprocess.Popen( unclippedCMD, shell=True, stdout=subprocess.PIPE ).stdout
    clipProc = subprocess.Popen( clippedCMD, shell=True, stdout=subprocess.PIPE ).stdout
    
    # Read in clipped and unclipped file tabulates P(k), P(k|T), and P(T)
    while(True):
        # Pull the clipped record
        clippedLine       = clipProc.readline()
        if (clippedLine   == ''): break
        readNameC         = splitLine(clippedLine[1:])
        clippedLine       = clipProc.readline()
        readSeqC          = splitLine(clippedLine)
        
        # Was the read clipped or is it an R2 read: if yes then throw it out
        if ((len(readSeqC)  == maxReadLen)or(getR(readNameC) == '2')): continue
        
        # Pull the unclipped record
        unclippedLine         = unclipProc.readline()
        if ((unclippedLine == '') and (len(unclipDict) == 0)): break
        readNameU             = splitLine(unclippedLine[1:])
        unclippedLine         = unclipProc.readline()
        readSeqU              = splitLine(unclippedLine)
        unclipDict[readNameU] = readSeqU
        
        # Are unclipped and clipped reads together
        while(readNameU != readNameC):
            try:
                # how I seen this unclipped read yet
                readSeqU  = unclipDict[readNameC]
                readNameU = readNameC
            except KeyError:
                # Keep the read, it may be found later
                unclipDict[readNameU] = readSeqU
                unclippedLine         = unclipProc.readline()
                if (unclippedLine     == ''): break
                readNameU             = splitLine(unclippedLine[1:])
                unclippedLine         = unclipProc.readline()
                readSeqU              = splitLine(unclippedLine)
            #####
        #####                      
        # Calculate P(k) for the read
        for i in xrange(wholeReadKmer):
            kmer    = readSeqU[i:(i+kmerSize)]
            kmerKey = createPair(kmer)
            try:
                PKDict[kmerKey][i] += 1
            except KeyError:
                PKDict[kmerKey]     = array('L', [0]*maxReadLen)
                PKTDict[kmerKey]    = array('L', [0]*maxReadLen)
                PKDict[kmerKey][i] += 1
            #####
        #####
        
        # Were bases clipped off the front end?: Prevents counts from becoming out of sync    
        startVal = readSeqU.find(readSeqC)
        try:
            # No, calculate P(k)
            readSeq = padDict[startVal](readSeqC)
        except KeyError:
            # Yes, pad the read then calculate P(k)
            readSeq = ''.join(['X'*startVal,readSeqC])
        #####
        # Increment P(T)
        readLen                           = len(readSeq)
        terminalEvent                    += 1
        termList[readLen-(kmerSize + 1)] += 1
        
        # Pull off window size chunk of base off end of read and generate kmers for P(k|T)
        realTermSeq = readSeq[-windowSize:]
        for i in xrange(clipReadKmers):
            mer                    = realTermSeq[i:i+kmerSize]
            kmerKey                = createPair(mer)
            loc                    = readLen -windowSize +i
            PKTDict[kmerKey][loc] += 1
        ##### 
        x()
    #####
   
    # Read in clipped file tabulates P(k|T) data and P(T)
    stderr.write('\t***%d prematurely terminated read***\n'%(terminalEvent))
    terminalEvent = float(terminalEvent)
    
    # write P(t)
    stderr.write('\tWriting P(t)\n')
    pt_oh    = open('%s_PT.dat'%baseName,'w')
    for i in xrange(maxReadLen): pt_oh.write('%d %.4e\n'%((i+1),float(termList[i])/terminalEvent))
    pt_oh.close()
     
    # write data and tabulate the effect of the kmers
    stderr.write('\tWriting P(k), P(k|t), and P(k|T)/P(k)\n')
    pkt_oh   = open('%s_PKT.dat'%baseName,'w')
    pk_oh    = open('%s_PK.dat'%baseName,'w')
    pktpk_oh = open('%s_PKT_PK.dat'%baseName,'w')
    x        = iterCounter(1000000)
    
    for k,v in PKDict.iteritems():
        outStrPK = ''
        for i in xrange(maxReadLen):    
            PK          = float(v[i])/terminalEvent
            # Does the read occur enough to make it significant
            if (PK < 5.0e-7):
                # No, then it is 0
                PK   = 0
            #####
            outStrPK    = ' '.join([outStrPK,'%.4e'%PK])
        #####
        pk_oh.write('%s\t%s\n'%(k,outStrPK))
        x()
    #####
    pk_oh.close()
    x        = iterCounter(1000000)
    for k,v in PKTDict.iteritems():
        PKList      = PKDict[k]
        outStrPKT   = ''
        outStrPKTPK = ''
        effect      = 0
        for i in xrange(maxReadLen):
            PK          = float(PKList[i])/terminalEvent    
            PKT         = float(v[i])/terminalEvent
            if (i<PKTrange): effect += v[i]
            if (PK < 5.0e-7):
                PKTPK = 0.0
            else:
                PKTPK = float(v[i])/float(PKList[i])
            #####
            outStrPKT   = ' '.join([outStrPKT,'%.4e'%PKT])
            outStrPKTPK = ' '.join([outStrPKTPK,'%.4e'%PKTPK])
        #####
        pkt_oh.write('%s\t%s\n'%(k,outStrPKT))
        pktpk_oh.write('%s\t%s\n'%(k, outStrPKTPK))
        # Tabulate the effect this kmer has on the read set
        effectedList.append((effect,k))
        x()
    #####
    pkt_oh.close()
    pktpk_oh.close()

    subprocess.Popen('bzip2 %s_PK.dat'%baseName, shell=True, stdout=subprocess.PIPE)
    subprocess.Popen('bzip2 %s_PKT.dat'%baseName, shell=True, stdout=subprocess.PIPE)
    subprocess.Popen('bzip2 %s_PKT_PK.dat'%baseName, shell=True, stdout=subprocess.PIPE)
    
    # Keeps only so many of the most impacting kmers
    effectedList.sort(reverse = True)
    
    num_oh = open('%skmerImpact.dat'%baseName,'w')
    for item in effectedList: num_oh.write('%s %d\n'%(item[1],item[0]))
    num_oh.close()
    
    effectedList = effectedList[:examineNumber]
    
    stderr.write('\tPlotting top kmers\n')
    
    # Plot the kmers
    oh  = open('plotted.pk','w')
    oh1 = open('plotted.pkt','w')
    for item in effectedList:
        kmer        = item[1]
        kills       = item[0]
        oh.write("%s\t"%(kmer))
        oh1.write("%s\t"%(kmer))
        PKList      = PKDict[kmer]
        PKTList     = PKTDict[kmer]
        outName     = changeName(kmer)
        tmp1_oh     = open('plot.pk.dat','w')
        tmp2_oh     = open('plot.pkt.dat','w')
        tmp3_oh     = open('plot.pktpk.dat','w')
        for i in xrange(maxReadLen):
            PK  = float(PKList[i])/terminalEvent    
            PKT = float(PKTList[i])/terminalEvent
            if (PK >= 5.0e-7):
                PKTPK = float(PKTList[i])/float(PKList[i])
            else:
                PKTPK = 0.0
            #####
            tmp1_oh.write('%d %.4e\n'%((i+1),PK))
            oh.write('%.4e '%(PK))
            tmp2_oh.write('%d %.4e\n'%((i+1),PKT))
            oh1.write('%.4e '%(PKT))
            tmp3_oh.write('%d %.4e\n'%((i+1), PKTPK))
        #####
        oh.write('\n')
        oh1.write('\n')
        tmp1_oh.close()
        tmp2_oh.close()
        tmp3_oh.close()
        plot_oh = open('plot.dat','w')
        plot_oh.write("reset\nset output \"impactKmer.%s.png\"\nset terminal png enhanced\nset terminal png nocrop size 1280,1280 font 'Helvetica Bold'\n"%(outName))
        plot_oh.write("set multiplot title \"%s\\n(Reads Affected: %d(%.2f%%))\" layout 2,2\nunset key\nset xrange[1:%d]\nset grid x y\nset style fill solid .5\n"%(outName,kills,(kills*100.0/terminalEvent),maxReadLen))
        plot_oh.write("set title \"P(k)\"\nplot \"plot.pk.dat\" using 1:2 axis x1y1 with lines\n")
        plot_oh.write("set title \"P(T)\"\nplot \"%s_PT.dat\" using 1:2 axis x1y1 with lines\n"%(baseName))
        plot_oh.write("set title \"P(k|T)\"\nplot \"plot.pkt.dat\" using 1:2 axis x1y1 with lines\n")
        plot_oh.write("set yrange [0:1]\nset title \"P(k|T)/P(k)\"\nplot \"plot.pktpk.dat\" using 1:2 axis x1y1 with boxes\nunset multiplot\n")
        plot_oh.close()
        system('gnuplot plot.dat')
    #####
    oh.close()
    oh1.close()
    system("rm -f plot.pk.dat plot.pkt.dat plot.pktpk.dat")
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()
