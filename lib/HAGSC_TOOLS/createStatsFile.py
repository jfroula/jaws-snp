#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Aug 24, 2010 1:23:56 AM$"

# Local Library Imports
from hagsc_lib import createStatsFile
from hagsc_lib import testFile

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE]"

    parser = OptionParser(usage)

    numNs_def = 5
    parser.add_option( '-n', \
                       "--numNs", \
                       type    = "int", \
                       help    = "Minimum number of N's to consider a gap.  Default: %d"%numNs_def, \
                       default = numNs_def )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if ( not isfile(args[0]) ):
            parser.error( '%s can not be found'%args[0] )
        #####
        fasta_file_in    = args[0]
        testFile( fasta_file_in )
    #####

    # Pushing the information to an underlying function
    createStatsFile( fasta_file_in, options.numNs )
    
    return

if ( __name__ == '__main__' ):
    real_main()
