#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import generateTmpDirName, deleteDir, testDirectory, isBzipFile, isGzipFile

from os.path import realpath, abspath, isfile, join
from os import curdir, mkdir, system, chdir

from sys import stderr, stdout

from optparse import OptionParser

import subprocess

#==============================================================
def printAndExecute( cmd, execute=True ):
    stdout.write( "%s\n"%cmd )
    if ( execute ): system( cmd )
    return 

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [LIB_ID] [options]"

    parser = OptionParser(usage)

    parser.add_option( "-c", \
                       "--complementReads", \
                       action  = 'store_true', \
                       dest    = 'complementReads', \
                       help    = "Complement pairs during extraction:  Default=No complementing." )
    parser.set_defaults( complementReads = False )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTA = realpath(args[0])
        if ( not isfile(FASTA) ): parser.error( '%s can not be found'%FASTA )

        LIB_ID = realpath(args[1])
    #####

    # Create temporary directory
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpPath  = join( basePath, generateTmpDirName() )
    deleteDir(tmpPath)
    mkdir(tmpPath, 0777)
    testDirectory(tmpPath)
    
    # Changing the directory
    chdir( tmpPath )
    
    # Find the pairs
    if ( isGzipFile(FASTA) ):
        firstChar = [item for item in subprocess.Popen("cat %s | gunzip -c | head -1 | cut -b-1"%FASTA, shell=True, stdout=subprocess.PIPE).stdout]
        if (firstChar == ">"):
            cmd = 'cat %s | gunzip -c | grep ">" | cut -b2- | cut -f1 -d"-" | sort -S 30G | uniq -c | grep " 2 " | cut -b9- | bzip2 -c > baseNames.dat.bz2'%FASTA
        else:
            cmd = 'cat %s | gunzip -c | awk \'{if(NR%%4==1){print}}\' | cut -b2- | cut -f1 -d"-" | sort -S 30G | uniq -c | grep " 2 " | cut -b9- | bzip2 -c > baseNames.dat.bz2'%FASTA
        #####
    elif ( isBzipFile(FASTA) ):
        firstChar = [item for item in subprocess.Popen("cat %s | bunzip2 -c | head -1 | cut -b-1"%FASTA, shell=True, stdout=subprocess.PIPE).stdout]
        if (firstChar == ">"):
            cmd = 'cat %s | bunzip2 -c | grep ">" | cut -b2- | cut -f1 -d"-" | sort -S 30G | uniq -c | grep " 2 " | cut -b9- | bzip2 -c > baseNames.dat.bz2'%FASTA
        else:
            cmd = 'cat %s | bunzip2 -c | awk \'{if(NR%%4==1){print}}\' | cut -b2- | cut -f1 -d"-" | sort -S 30G | uniq -c | grep " 2 " | cut -b9- | bzip2 -c > baseNames.dat.bz2'%FASTA
        #####
    else:
        firstChar = [item for item in subprocess.Popen("cat %s | head -1 | cut -b-1"%FASTA, shell=True, stdout=subprocess.PIPE).stdout]
        if (firstChar == ">"):
            cmd = 'cat %s | grep ">" | cut -b2- | cut -f1 -d"-" | sort -S 30G | uniq -c | grep " 2 " | cut -b9- | bzip2 -c > baseNames.dat.bz2'%FASTA
        else:
            cmd = 'cat %s | awk \'{if(NR%%4==1){print}}\' | cut -b2- | cut -f1 -d"-" | sort -S 30G | uniq -c | grep " 2 " | cut -b9- | bzip2 -c > baseNames.dat.bz2'%FASTA
        #####
    #####

    printAndExecute( cmd, execute=True )

    # Generate the R1/R2 reads
    cmd = "cat baseNames.dat.bz2 | bunzip2 -c | awk '{ print $1\"-R1\" }' > tmp.R1.dat"
    printAndExecute( cmd, execute=True )
    cmd = "cat baseNames.dat.bz2 | bunzip2 -c | awk '{ print $1\"-R2\" }' > tmp.R2.dat"
    printAndExecute( cmd, execute=True )
    
    # Extracting the reads from the clipped file
    frontEnd = '/mnt/local/EXBIN/extract_seq_and_qual'
    if ( options.complementReads ): frontEnd = '/mnt/local/EXBIN/extract_seq_and_qual -c'
    cmd = '%s -i tmp.R1.dat -o tmp.R1.fasta.bz2 %s'%(frontEnd,FASTA)
    printAndExecute( cmd, execute=True )
    cmd = '%s -i tmp.R2.dat -o tmp.R2.fasta.bz2 %s'%(frontEnd,FASTA)
    printAndExecute( cmd, execute=True )
    
    # Renaming the FASTA files
    cmd = 'cat tmp.R1.fasta.bz2 | bunzip2 -c | sed "s\-R\/\g" | bzip2 -c > tmp.R1.fixed.fasta.bz2'
    printAndExecute( cmd, execute=True )
    cmd = 'cat tmp.R2.fasta.bz2 | bunzip2 -c | sed "s\-R\/\g" | bzip2 -c > tmp.R2.fixed.fasta.bz2'
    printAndExecute( cmd, execute=True )
    
    # Moving the fasta files to the working directory
    cmd = "mv -f tmp.R1.fixed.fasta.bz2 %s.R1.fasta.bz2"%(LIB_ID)
    printAndExecute( cmd, execute=True )
    cmd = "mv -f tmp.R2.fixed.fasta.bz2 %s.R2.fasta.bz2"%(LIB_ID)
    printAndExecute( cmd, execute=True )

    # Changing directory
    chdir( basePath )
    
    # Removing the temporary directory
    deleteDir(tmpPath)
    
    return

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
