#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$July 20, 2011"

# Local Library Imports
from hagsc_lib import repetitiveContent
from hagsc_lib import testFile

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from os import system

from sys import stderr

#====================================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [ASSEMBLY FILE]"
    parser = OptionParser(usage)

    zValue = '2g'
    parser.add_option( "-z", \
                       "--zValue", \
                       type    = 'str', \
                       help    = "-z value for mask_repeats_hash.  Default: %s"%zValue, \
                       default = zValue )

    kValue = 50000
    parser.add_option( "-k", \
                       "--kValue", \
                       type    = 'int', \
                       help    = "Cutoff size for scaffolds in masking (-k option).  Default: %d"%kValue, \
                       default = kValue )

    mValue = 24
    parser.add_option( "-m", \
                       "--mValue", \
                       type    = 'int', \
                       help    = "Kmer size (-m option).  Default: %d"%mValue, \
                       default = mValue )

    parser.add_option( '-Z', \
                       "--purgeHash", \
                       action  = "store_true", \
                       dest    = 'purgeHash', \
                       help    = "Purge single count kmers on hash overflow:  Default=No purging." )
    parser.set_defaults( purgeHash = False )

    parser.add_option( '-a', \
                       "--assessOnly", \
                       action  = "store_true", \
                       dest    = 'assessOnly', \
                       help    = "Only perform the assessment, and do no masking:  Default=Masking is performed." )
    parser.set_defaults( assessOnly = False )

    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if (not isfile(args[0])):
            parser.error( '%s can not be found'%args[n] )
        #####
        assemblyFile = args[0]
    #####
    
    z = options.zValue
    k = options.kValue
    m = options.mValue
    
    if ( not options.assessOnly ):
        # Running the t2 masking
        t       = 2
        ext     = '.t%d.masked'%t
        t2_file = '%s%s'%(assemblyFile,ext)
        if ( options.purgeHash ):
            cmd     = 'mask_repeats_hashn -Z -k %d -t %d -m %d -s %s -z %s -H %s %s'%(k,t,m,ext,z,assemblyFile,assemblyFile)
        else:
            cmd     = 'mask_repeats_hashn -k %d -t %d -m %d -s %s -z %s -H %s %s'%(k,t,m,ext,z,assemblyFile,assemblyFile)
        #####
        stderr.write( '%s\n'%cmd )
        system( cmd )
    
        # Running the t4 masking
        t       = 4
        ext     = '.t%d.masked'%t
        t4_file = '%s%s'%(assemblyFile,ext)
        if ( options.purgeHash ):
            cmd     = 'mask_repeats_hashn -Z -k %d -t %d -m %d -s %s -z %s -H %s %s'%(k,t,m,ext,z,assemblyFile,assemblyFile)
        else:
            cmd     = 'mask_repeats_hashn -k %d -t %d -m %d -s %s -z %s -H %s %s'%(k,t,m,ext,z,assemblyFile,assemblyFile)
        #####
        stderr.write( '%s\n'%cmd )
        system( cmd )
    else:
        # Making the filenames
        t       = 2
        ext     = '.t%d.masked'%t
        t2_file = '%s%s'%(assemblyFile,ext)

        t       = 4
        ext     = '.t%d.masked'%t
        t4_file = '%s%s'%(assemblyFile,ext)
    #####

    # Pushing the information to an underlying function
    repetitiveContent( assemblyFile, t2_file, t4_file )

    return

#####f

if ( __name__ == '__main__' ):
    real_main()
