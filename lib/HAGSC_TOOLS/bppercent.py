
from hagsc_lib import fasta_file, write_fasta_record
from hagsc_lib import histogramClass

from optparse import OptionParser
from sys import stdout

def main():
    cmd = OptionParser()
    cmd.add_option("-g", "--histogram", action="store_true", dest="histogram", default=False)
    cmd.add_option("--bins", action="store", type="float", default=100)
    cmd.add_option("-t", "--tag", action="store_true", dest="tag", default=False)
    cmd.add_option("--min", action="store", type="float", default=-0.1)
    cmd.add_option("--max", action="store", type="float", default=1.1)
    options, args = cmd.parse_args()
    
    fastas = [fasta_file(open(filename, 'r'), iter_only=True) for filename in args]
    
    if options.histogram:
        histo = histogramClass(0, 100, options.bins)
        for scaffold, percent in percents(fastas):
            histo.addData(100*percent)
        print histo.generateOutputString()
    elif options.tag:
        for scaffold, percent in percents(fastas):
            if options.min < percent < options.max:
                scaffold.info = '\t'.join((scaffold.info, "bp=%f"%percent))
                write_fasta_record(scaffold, stdout)
    else:
        for scaffold, percent in percents(fastas):
            if options.min < percent < options.max: print "%s\t%s"% (scaffold.id, percent)
        
    
def percents(fastas):
    for fasta in fastas:
        for scaffold in fasta.itervalues():
            sequence = scaffold.data()
            bps = sum(sequence.count(x) for x in ['A','T','C','G'])
            percent = float(bps) / (bps+sequence.count('X'))
            yield scaffold, percent
            
    
if __name__ == "__main__":
    main()
