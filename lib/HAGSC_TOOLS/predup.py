#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott"
__date__ ="$July 12, 2013$"

from optparse import OptionParser
from os import system
from os.path import abspath, realpath, isfile
from hagsc_lib import isGzipFile, isBzipFile
from sys import stderr
import subprocess
#=========================================================================

def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    if ( isGzipFile(fileName) ): return 'cat %s | gunzip -c '%( fileName)
    if ( isBzipFile(fileName) ): return 'cat %s | bunzip2 -c '%( fileName)
    return 'cat %s '%( fileName)

#==============================================================
def printAndExec(cmd, execute=True):
    """This function will take a command and print it to the screen and execute the command. If fasle is given for the second argument then the command will only be printed to the screen."""
    stderr.write("%s\n"%cmd)
    if (execute): system( cmd )
    return
#==============================================================
def real_main():
    # Defining the program options
    usage = "usage: %prog [FASTA][options]"
    
    parser = OptionParser(usage)
    
    parser.add_option( '-n', \
                       "--nProcs", \
                       type    = 'int', \
                       help    = 'Number of processors to use. Default:  50.', \
                       default = 50 )
                       
    parser.add_option( '-c', \
                       "--cluster", \
                       type    = 'str', \
                       help    = 'cluster to use:  Default ssd.', \
                       default = "ssd" )
    # Parsing the arguements
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling FASTA File
        fastaFile = realpath( args[0] )
        frontCMD  = testFile( fastaFile )
        nProcs    = options.nProcs
        cluster   = options.cluster
    #####
    
    # is the file a fastq or a fasta
    formatTest = [item.strip() for item in subprocess.Popen("%s | head -4"%frontCMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout]
    if (formatTest[2] == "+"):
        insertedCMD = "awk '{if(NR%4==1){print \">\" substr($1,2) }if(NR%4==2){print}}' |"
    else:
        insertedCMD = " "
    #####
    
    
    
    # Building the awk command It is broken into part for easy understanding
    awkBeg  = 'BEGIN {RS = "-R1";n = 0;j = n = split("A C G T", t);for (i = 0; ++i <= n;){map[t[i]] = t[j--]}};'
    awkFun  = 'function reverseComp(s){p = "";for(i=length(s); i > 0; i--) { p = p map[substr(s, i, 1)] };return p};'
    awkChk  = '{split($2 , wholeName, "-");name = substr(wholeName[1], 2);if (substr(name, 1, 1) == "" ){prevName = substr($0, 2);next}{if (name != prevName){print \"The FASTA file is not interleaved. Use the interleaver to fix this and then try again\";print prevName"\\n"name;exit}'
    awkBld  = 'seq1=reverseComp($1);seq2=$3;mer1=substr($1, 1, 48);mer2=substr(seq2, 1, 48);mer=mer1 mer2;insert="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN";'
    awkOut  = 'print mer"\\t"name"\\t"seq1 insert seq2}prevName = substr($4, 2)}'
    awkCMD  = "awk '%s%s%s%s%s'"%(awkBeg, awkFun, awkChk, awkBld, awkOut)

    printAndExec('%s | %s %s | sort -k1 -S 30g -T /home/t4c1/WORK/cplott_ANALYSIS/| LFPE_predup.py %d %s'%(frontCMD, insertedCMD, awkCMD, nProcs, cluster))
    
#==============================================================    
if ( __name__ == '__main__' ):
    real_main()