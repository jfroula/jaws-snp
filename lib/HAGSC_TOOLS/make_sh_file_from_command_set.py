#!/usr/common/software/python/2.7-anaconda-4.4/bin/python/
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  1/23/18"

from gp_lib import create_sh_file

from optparse import OptionParser

from os.path import isfile

#==============================================================
def real_main():
    
   # Defining the program options
    usage = "usage: %prog [CMD FILE] [SH FILENAME]"

    parser = OptionParser(usage)

    def_cpuTime = '12:00:00'
    parser.add_option( "-t", \
                       "--cpuTime", \
                       type    = 'str', \
                       help    = "CPU time.  Default: %s" % def_cpuTime, \
                       default = def_cpuTime )

    def_memory = '10G'
    parser.add_option( "-m", \
                       "--memory", \
                       type    = 'str', \
                       help    = "Memory.  Default: %s" % def_memory, \
                       default = def_memory )

    parser.add_option( '-s', \
                       "--suppressOutput", \
                       action  = "store_true", \
                       dest    = 'suppressOutput', \
                       help    = "Do not output stdout/stderr to a file:  Default=Output is not written." )
    parser.set_defaults( suppressOutput = False )

    def_emailAddress = None
    parser.add_option( "-e", \
                       "--emailAddress", \
                       type    = 'str', \
                       help    = "Email to send messages from the SLURM submitter.  Default: %s" % def_emailAddress, \
                       default = def_emailAddress )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if ( not isfile(args[0]) ): parser.error( '%s can not be found'%args[0] )
        cmdFile    = args[0]
        shFileName = args[1]
    #####
    
    # Reading the command list
    cmdList = [ line.strip() for line in open(cmdFile) ]
    
    # Making the sh file
    create_sh_file( cmdList, options.cpuTime, options.memory, shFileName, options.suppressOutput, emailAddress=options.emailAddress, ncores=1 )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
