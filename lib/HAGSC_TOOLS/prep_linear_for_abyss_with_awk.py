#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org modified by Lori Howell Handley 8/7/15"
__date__ ="$${date} ${time}$"

from hagsc_lib import isGzipFile, isBzipFile, generateTmpDirName, deleteDir, testDirectory

from os.path import realpath, abspath, isfile, join
from os import curdir, mkdir, system, chdir

from sys import stderr, stdout

from optparse import OptionParser

#==============================================================
def printAndExecute( cmd, execute=True ):
    stdout.write( "%s\n"%cmd )
    if ( execute ): system( cmd )
    return 

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [LIB_ID] [options]"

    parser = OptionParser(usage)

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTA = realpath(args[0])
        if ( not isfile(FASTA) ): parser.error( '%s can not be found'%FASTA )

        LIB_ID = realpath(args[1])
    #####

    # Create temporary directory
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpPath  = join( basePath, generateTmpDirName() )
    deleteDir(tmpPath)
    mkdir(tmpPath, 0777)
    testDirectory(tmpPath)
    
    # Changing the directory
    chdir( tmpPath )
    
    # Preparing the R1/R2 readnames for extract seq and qual
    cmdList = []
    #####
    cmdList.append( '/mnt/local/EXBIN/clip -b -B 10000 -f 25 -L 0 -c 75 %s'%FASTA )
    #####
    cmdList.append( 'awk -f /mnt/raid2/SEQ/sharedPythonLibrary/prep_linear_for_abyss.awk ')
    cmd = ' | '.join( cmdList )
    system( cmd )
        
    # Moving the fasta files to the working directory
    cmd = "mv -f R1.fasta.bz2 %s.R1.fasta.bz2"%(LIB_ID)
    printAndExecute( cmd, execute=True )
    cmd = "mv -f R2.fasta.bz2 %s.R2.fasta.bz2"%(LIB_ID)
    printAndExecute( cmd, execute=True )
    
    # Changing directory
    chdir( basePath )
    
    # Removing the temporary directory
    deleteDir(tmpPath)
    
    return

#==============================================================
if ( __name__ == '__main__' ):
    real_main()

