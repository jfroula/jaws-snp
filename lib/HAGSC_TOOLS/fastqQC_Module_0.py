#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$May 21, 2014$"
#=========================================================================
# EDIT LOG - 6/2/14
# Needs to be optimized for PC 77&78 and running upto 8 on each machine
# Add options to different contamination and a way to report it
# Add option to sam tools to calculate insert size
# Set up standard Genome directory
#=========================================================================
import subprocess

import re

from time import time

from sys import  stderr

from os import system

from os.path import isdir, isfile

from optparse import OptionParser

from hagsc_lib import iterCounter

from itertools import izip

from random import random

from math import sqrt

import numpy as np

#=========================================================================
# Regular expressions and formats
commify_re  = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
findStuff   = re.compile( r'(1+)' ).finditer
laneSF         = "{:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17} {:17}\n"
#=========================================================================
# Useful Dictionaries
doesPass_dict = {True:'1',False:'0'}

#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "pbzip2  -p2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "pbzip2  -p2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False

#=========================================================================
def Pbzip_file(outFile, offset, mode='r', compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "awk 'BEGIN{for(n=0;n<120;n++){ord[sprintf(\"%%c\",n)]=n-%d}}function convert(s){p = \"\";for(i=1; i <= length(s); i++) { p = p ord[substr(s, i, 1)]\" \" };return p}{if(NR%%2==1){print $0}if(NR%%2==0){print convert($1)}}'|pbzip2 -p2 -z -c -%d"%(offset,compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "awk 'BEGIN{for(n=0;n<120;n++){ord[sprintf(\"%%c\",n)]=n-%d}}function convert(s){p = \"\";for(i=1; i <= length(s); i++) { p = p ord[substr(s, i, 1)]\" \" };return p}{if(NR%%2==1){print $0}if(NR%%2==0){print convert($1)}}'|pbzip2 -p2 -z -c -%d"%(offset,compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False        

#=========================================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): return False
    return True

#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True

#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        return True
    except IOError:
        return False
    #####

#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        return True
    except IOError:
        return False
    #####

#========================================================================= 
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )

#=========================================================================        
def doNothing( seq, qual):
    return
    
#=========================================================================  
class stats(object):
    def __init__(self,readLength, offset, winMax, charToAscii, intToChar):
        
        self.totalReads     = 0
        self.charToAscii    = charToAscii
        self.Range          = xrange(readLength)
        self.gBaseTotal     = [0]*readLength
        self.pBaseTotal     = [0]*readLength
        
        self.genomeCompList = []
        self.phixCompList   = []
        self.genomeQualDist = []
        self.phixQualDist   = []
        
        tmpDict = dict( [ (chr(val),0) for val in range((offset),(winMax+1))] )
        
        for i in self.Range: 
            self.genomeCompList.append( {'A':0,'T':0,'G':0,'C':0,'N':0} )
            self.phixCompList.append(   {'A':0,'T':0,'G':0,'C':0,'N':0} )
            self.genomeQualDist.append( dict([(k,v) for k,v in tmpDict.iteritems()]) )
            self.phixQualDist.append(   dict([(k,v) for k,v in tmpDict.iteritems()]) )
        #####
        
        self.testSet = set()
        for i in xrange(20):self.testSet.add(intToChar[i])
        self.GenomeArray = np.zeros(readLength)
        self.phixArray   = np.zeros(readLength)
    #####
    def countGenome(self, seq, qual):
        
        badSet = set()
        counter = 0
        for f,b in izip(self.genomeQualDist,qual):
            f[b] += 1
            if (b in self.testSet): badSet.add(counter)
            counter += 1
        #####
        self.GenomeArray[:counter] += 1
        counter = 0 
        for f,b in izip(self.genomeCompList,seq):
            if (counter in badSet): 
                counter += 1
                continue
            #####
            f[b]    += 1
            counter += 1
        #####
        

    def countPhix(self, seq, qual):
        
        badSet = set()
        counter = 0
        for f,b in izip(self.phixQualDist,qual):
            f[b] += 1
            if (b in self.testSet): badSet.add(counter)
            counter += 1
        #####
        self.phixArray[:counter] += 1
        counter = 0
        for f,b in izip(self.phixCompList,seq):
            if (counter in badSet): 
                counter += 1
                continue
            #####
            f[b]    += 1
            counter += 1
        #####
        

    def writeStats(self, genome_stats, phix_stats):
        
        genomeString = []
        genomeString.append('%s'%(laneSF.format("#","TotQual","A's","T's","G's","C's","N's","TotBP","TotQual","TotQual_phix","A's_phix","T's_phix","G's_phix","C's_phix","N's_phix","TotBP_phix","TotQual_phix")))
        totbases     = 0
        for item in self.Range:
            
            gtot          = self.genomeCompList[item]['A']+self.genomeCompList[item]['T']+self.genomeCompList[item]['G']+self.genomeCompList[item]['C']+self.genomeCompList[item]['N']
            ptot          = self.phixCompList[item]['A']+self.phixCompList[item]['T']+self.phixCompList[item]['G']+self.phixCompList[item]['C']+self.phixCompList[item]['N']
            
            genomeQualTot = 0
            for k,v in self.genomeQualDist[item].iteritems():genomeQualTot += v*self.charToAscii[k]
            
            phixQualTot   = 0
            for k,v in self.phixQualDist[item].iteritems():  phixQualTot   += v*self.charToAscii[k]

            genomeString.append('%s'%(laneSF.format("%s"%(item+1),genomeQualTot,self.genomeCompList[item]['A'],\
                                                    self.genomeCompList[item]['T'],self.genomeCompList[item]['G'],\
                                                    self.genomeCompList[item]['C'],self.genomeCompList[item]['N'],\
                                                    gtot,self.GenomeArray[item],phixQualTot,self.phixCompList[item]['A'],\
                                                    self.phixCompList[item]['T'],self.phixCompList[item]['G'],\
                                                    self.phixCompList[item]['C'],self.phixCompList[item]['N'],ptot,self.phixArray[item])))
        #####
        
        genome_stats_oh = open(genome_stats,'w')
        genome_stats_oh.write( ''.join(genomeString) )
        genome_stats_oh.close()
        
        

#=========================================================================
class phredHist(object):        
    def __init__(self, readLength, intToChar, winMax, offset):
        
        
        self.binNum               = int(readLength/10)+1
        
        self.genome_hist          = {}
        self.phix_hist            = {}
        
        for i in range(self.binNum):
            self.genome_hist[i]   = 0
            self.phix_hist[i]     = 0
        #####
        
        self.total_genome_bases   = 0
        self.total_genome_reads   = 0
        # A HQ base is one that has a phred score of 20 or greater
        self.hq_genome_bases      = 0
        # a Passing Read is any read that has 40 or more HQ bases
        self.passing_genome_reads = 0
        # a good_bad_base is a base that is HQ but in a failing read
        self.genome_good_bad_base = 0
        
        self.total_phix_bases     = 0
        self.total_phix_reads     = 0
        # A HQ base is one that has a phred score of 20 or greater
        self.hq_phix_bases        = 0
        # a Passing Read is any read that has 40 or more HQ bases
        self.passing_phix_reads   = 0
        # a good_bad_base is a base that is HQ but in a failing read
        self.phix_good_bad_base   = 0
        
        # Set of charaters that are failing quality
        bad_qual_str = ''
        self.testSet = set()
        for i in xrange(20):
            self.testSet.add(intToChar[i])
            bad_qual_str =  bad_qual_str + intToChar[i]
        #####
        
        # Will find all the LQ bases in fastq space
        self.findLQ = re.compile( r'([%s])'%(bad_qual_str) ).findall

    def incrementGenome(self,qual):
        
        # Add the reads and bases for total
        qLen                           = len(qual)
        self.total_genome_bases       += qLen
        self.total_genome_reads       += 1
        
        # Find the LQ base
        HQ                             = qLen - len(self.findLQ(qual))
        
        self.hq_genome_bases          += HQ

        self.genome_hist[int(HQ/10)]  += 1
        
        if(HQ >= 40):
            self.passing_genome_reads += 1
        else:
            self.genome_good_bad_base += HQ
        #####
      

    def incrementPhix(self,qual):
        qLen                         = len(qual)
        self.total_phix_bases       += qLen
        self.total_phix_reads       += 1
        
        HQ                           = qLen - len(self.findLQ(qual))
        
        self.hq_phix_bases          += HQ
        
        self.phix_hist[int(HQ/10)]  += 1
        
        if(HQ > 39):
            self.passing_phix_reads += 1
        else:
            self.phix_good_bad_base += HQ
        #####
    

    def writePhredHist(self, genome_phred, phix_phred):
    
        sf             = "{:3}-{:3} {:12}  {:>5}%  |{:50}\n"
        genomePhred    = ['Phred20       Reads  %ofReads\n-------    ---------  --------\n']
        
        phixPhred      = ['Phred20       Reads  %ofReads\n-------    ---------  --------\n']
        
        for item in range(self.binNum): 
            
            binGNum = self.genome_hist[item]
            perGNum = float(100.0*binGNum)/float(self.total_genome_reads)
            gXNum   = int(perGNum/2)
            if(int(perGNum)%2 == 1): 
                gXNum += 1
            #####
            
            genomePhred.append( "%s"%( sf.format( 10*item, (10*item+9), binGNum, '%.1f'%perGNum, 'X'*gXNum ) ) )
        #####
        
        genomePhred.append("\nNumber of reads: %s\nTotal bases: %s\nTotal Phred20 bases: %s\n"%(commify(self.total_genome_reads),commify(self.total_genome_bases),commify(self.hq_genome_bases)))
        genomePhred.append("Average length: %.1f\nPhred Average: %.1f\n"%(float(self.total_genome_bases)/float(self.total_genome_reads),float(self.hq_genome_bases)/float(self.total_genome_reads)))
        if (self.passing_genome_reads < 1):
            passing = 0.0
        else:
            passing = float( self.hq_genome_bases - self.genome_good_bad_base )/float( self.passing_genome_reads )
        #####
        genomePhred.append("Phred Average without failures: %.1f\nPercent failed: %.1f%%\n"%(passing,float((self.total_genome_reads - self.passing_genome_reads)*100.0)/float( self.total_genome_reads )))
        
        genome_phred_oh = open(genome_phred,'w')
        genome_phred_oh.write(''.join(genomePhred))
        genome_phred_oh.close()
        
        genomeAHQ = float(self.hq_genome_bases)/float(self.total_genome_reads)
        phixAHQ   = 0
        if(self.total_phix_reads > 0):
            for item in range(self.binNum):
                binPNum = self.phix_hist[item]
                perPNum = float(100.0*binPNum)/float(self.total_phix_reads)
                pXNum   = int(perPNum/2)
                if(int(perPNum)%2 == 1): 
                    pXNum += 1
                #####
                phixPhred.append(   "%s"%( sf.format( 10*item, (10*item+9), binPNum, '%.1f'%perPNum, 'X'*pXNum ) ) )
            #####
        
            phixPhred.append("\nNumber of reads: %s\nTotal bases: %s\nTotal Phred20 bases: %s\n"%(commify(self.total_phix_reads),commify(self.total_phix_bases),commify(self.hq_phix_bases)))
            phixPhred.append("Average length: %d\nPhred Average: %.1f\n"%(float(self.total_phix_bases)/float(self.total_phix_reads),float(self.hq_phix_bases)/float(self.total_phix_reads)))
            if (self.passing_phix_reads < 1):
                passing = 0.0
            else:
                passing = float( self.hq_phix_bases - self.phix_good_bad_base )/float( self.passing_phix_reads )
            #####
            phixPhred.append("Phred Average without failures: %.1f\nPercent failed: %.1f%%\n"%(passing,float((self.total_phix_reads - self.passing_phix_reads)*100.0)/float( self.total_phix_reads )))
    
            phix_phred_oh = open(phix_phred,'w')
            phix_phred_oh.write(''.join(phixPhred))
            phix_phred_oh.close()
            phixAHQ = float(self.hq_phix_bases)/float(self.total_phix_reads)
        else:
            phixPhred.append("\nNumber of reads: %s\nTotal bases: %s\nTotal Phred20 bases: %s\n"%(0.0,0.0,0.0))
            phixPhred.append("Average length: %d\nPhred Average: %.1f\n"%(0,0.0))
            phixPhred.append("Phred Average without failures: %.1f\nPercent failed: %.1f%%\n"%(0.0,0.0))
    
            phix_phred_oh = open(phix_phred,'w')
            phix_phred_oh.write(''.join(phixPhred))
            phix_phred_oh.close()
            phixAHQ = 0.0
        #####
        
        AVGreadLength = float(self.total_genome_bases+self.total_phix_bases)/float(self.total_genome_reads+self.total_phix_reads)
        return [self.hq_genome_bases, self.total_genome_reads, self.hq_phix_bases, self.total_phix_reads]
    #####

#=========================================================================
class readData(object):
    def __init__(self, baseName, sepString, referenceFileList= None, orgList = None, nPairs = 100000):
        self.baseName    = baseName
        self.sepString   = sepString
        self.refSeqList  = referenceFileList
        self.orgList     = orgList
        self.readDict    = {}
        self.nPairs      = nPairs
        self.addData     = True
        self.firstPass   = True
        self.addDataDict = {True:{True:self.addData_real, False:self.appendData_real}, False:{True:self.addData_fake, False:self.addData_fake}}
        self.readCounts  = {'1':0, '2':0}
        return
    #####
    def addData_real(self, baseName, name, index, seq, qual):
        self.readDict[baseName]=[(index,[name, seq, qual])]
        self.readCounts[index] += 1
        if (self.readCounts[index] == self.nPairs) : 
            self.firstPass = False
            self.switch()
        #####
        
        return
    ##### 
    def appendData_real(self, baseName, name, index, seq, qual):
        
        try:
            self.readDict[baseName].append((index,[name, seq, qual]))
        except KeyError:
            return
        #####
        
        self.readCounts[index] += 1
        if (self.readCounts[index] == self.nPairs) : self.switch()
        return
    #####
    def addData_fake(self, baseName, name, index, seq, qual):
        # This function is just a place holder when the class has enough reads
        return
    #####
    def switch(self):
        self.addData = not self.addData
    
    #####
    def makeOutString(self):
        outString = []
        if((self.readCounts['2'] == 0) or (self.readCounts['1'] == 0)):
            for key in self.readDict:
                line = self.readDict[key]
                line.sort()
                outString.append('%s\n%s\n+\n%s\n%s\n%s\n+\n%s\n'%(line[0][1][0],line[0][1][1],line[0][1][2]))
            #####
        else:
            for key in self.readDict:
                line = self.readDict[key]
                line.sort()
                outString.append('%s\n%s\n+\n%s\n%s\n%s\n+\n%s\n'%(line[0][1][0],line[0][1][1],line[0][1][2],line[1][1][0],line[1][1][1],line[1][1][2]))
            #####
        #####
        return ''.join(outString)    

    def runAnalysis(self):
        # build the output string
        outString = self.makeOutString()
        
        # Is there a reference
        if ( self.refSeqList != None ):
            # index the refSeq
            counter  = 1
            distList = []
            hitSet   = set()
            # What is the insert size distribution
            for ref in self.refSeqList:
                system( '/opt/bowtie2/bowtie2-build %s refSeq_%d'%(ref , counter) )
                bowTieCmd  = '/opt/bowtie2-2.1.0/bowtie2 -p 6 --quiet --local refSeq_%d - '%(counter)
                bowTieProc = subprocess.Popen("%s | /opt/samtools/samtools view -F 4 -S - | awk '{insert = ($9<0) ? $9 * -1 : $9 ; print insert\"\t\"$1}' "%bowTieCmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                bowTieOut  = bowTieProc.communicate(input = outString)[0].strip().split("\n")
                insertDist = [int(item.split("\t")[0]) for item in bowTieOut]
                namesSet   = set([item.split("\t")[1].split(self.sepString)[0] for item in bowTieOut])
                hitSet     = hitSet.union(namesSet)
                totalN     = len(insertDist)
                gbar       = 0
                g2bar      = 0
                for point in insertDist:
                    gbar  += totalN * point
                    g2bar += point*point*totalN
                #####
                sigma = sqrt(g2bar - (gbar * gbar))
                distList.append([gbar, sigma, insertDist, ref])
                counter += 1
            #####
            # print the distribution 
            dist_oh = open('%s_insertDist.dat'%(self.baseName), 'w')
            dist_oh.write("INSERT DISTRIBUTION\n")
            for data in distList:
                sigma          = data[1]
                mean           = data[0]
                rawData        = data[2]
                reference      = data[3]  
                localHistorams = np.histogram(rawData, bins=50)
                dist_oh.write('====================\n')
                dist_oh.write('REFERENCE: %s\n'%(reference))
                dist_oh.write('Mean : %.2f\n'%(mean))
                dist_oh.write('Sigma: %.2f\n'%(sigma))
                for hist,bins in izip(localHistorams[0],localHistorams[1]): dist_oh.write("%.2f\t|%s\n"%(bins, "X"*int(hist*100/len(rawData))))
                dist_oh.write('====================\n')
            #####
            system( 'rm -f refSeq_*' )
            dist_oh.write("\n\n")
            dist_oh.close()
            
            keyset = self.readDict.keys()
            
            for key in keyset:
                if (key in hitSet): self.readDict.pop(key)
            
            outString = self.makeOutString()
        #####
        
        # Is there an orgList
        if ( self.orgList != None ):
            
            
            
            if (isfile( '%s_insertDist.dat'%(self.baseName) )):
                contam_oh = open('%s_insertDist.dat'%(self.baseName), 'a')
            else:
                contam_oh = open('%s_insertDist.dat'%(self.baseName), 'w')
            #####
            
            contam_oh.write('CONTAMINANT PROFILE\n====================\n')
            contam_oh.write('REFERENCE-%.2f(%s)\n'%(float(len(hitSet))/float(self.nPairs), commify( len(hitSet) ) ) )
            
            unaligned  = self.nPairs - len(hitSet)
            counter    = 1
            contamDict = {}
            for contam in self.orgList:
                # Align the reads
                system( '/opt/bowtie2/bowtie2-build %s contamSeq_%d'%(contam , counter) )
                bowTieCmd  = '/opt/bowtie2-2.1.0/bowtie2 -p 6 --quiet --local contamSeq_%d - '%(counter)
                bowTieProc = subprocess.Popen("%s | /opt/samtools/samtools view -F 4 -S - | awk '{print $1}' "%bowTieCmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                
                # Build the hit set and write the result
                bowTieResult = bowTieProc.communicate(input = outString)[0].strip().split(None)
                insertDist   = set([item.split(self.sepString)[0] for item in bowTieResult])               
                contam_oh.write('%s-%.2f(%s)\n'%(contam,float(len(insertDist))*100.0/float(self.nPairs), commify( len(insertDist) ) ) )
                unaligned -= len(insertDist)
                
                # Remove the hits to avoid repeat hits
                for key in keyset:
                    if (key in hitSet): 
                        try:
                            self.readDict.pop(key)
                        except KeyError:
                            continue
                        #####
                    #####
                #####
                stderr.write("======================================\n\n%s\n\n======================================"%(len(self.readDict)))
                outString = self.makeOutString()
                
                counter += 1
            #####
            contam_oh.write('UNALIGNED-%.2f(%s)\n'%(float(unaligned)*100.0/float(self.nPairs), commify( unaligned ) ) )
            contam_oh.write('====================\n')
            contam_oh.close()
            system( 'rm -f contamSeq_*' )
        #####
        
        return
    #####
    
#=========================================================================
def real_main():
    # Defining the program options
    usage = "usage: %prog [FASTQ FILE(S)] [BASE NAME]"

    parser = OptionParser(usage)
    
    parser.add_option( '-s', \
                       "--noSeqQual", \
                       action  = "store_false", \
                       dest    = 'noSeqQual', \
                       help    = "Do not create fasta and qual files.  Default:  Will create fasta and qual." )
    parser.set_defaults( noSeqQual = True )
    
    parser.add_option( '-U', \
                       "--NUGEN", \
                       action  = "store_true", \
                       dest    = 'NUGEN', \
                       help    = "Run an ecoli screen for NUGEN96 runs.  Default:  Will run just a phix screen." )
    parser.set_defaults( NUGEN = False )
    
    defaultRandSam = 5
    parser.add_option( '-r', \
                       "--RandSamp", \
                       type    = 'int', \
                       help    = 'Percentage of reads sampled for sequence and quality composition: Default:  %d%%'%(defaultRandSam), \
                       default = defaultRandSam )
                       
    parser.add_option( '-R', \
                       "--RefSeq", \
                       type    = 'str', \
                       help    = 'Reference Genome(s) to align library to get insert size:  Does NOT Calculate Insert Size', \
                       default = None )
    
                       
    parser.add_option( '-c', \
                       "--ContamSeq", \
                       type    = 'str', \
                       help    = 'Know Contaminant, return percentage of reads that hit contaminant:  Does NOT Run Contaninant Profile', \
                       default = None )
                       
    defaultnPairs = 100000
    parser.add_option( '-n', \
                       "--nPairs", \
                       type    = 'int', \
                       help    = 'Number of reads to used for RefSeq and ContamSeq analysis: Default:  %s'%(commify(defaultnPairs)), \
                       default = defaultnPairs )
    
    defaultoffSet = -1
    parser.add_option( '-o', \
                       "--offSet", \
                       type    = 'int', \
                       help    = 'Phred Encoding Value: Default:  %d'%(defaultoffSet), \
                       default = defaultoffSet )
                       
    parser.add_option( '-S', \
                       "--Separator", \
                       type    = 'str', \
                       help    = 'Give know Separator String to skip analysis:  Default: None', \
                       default = None )
                       
    defaultLen = -1
    parser.add_option( '-l', \
                       "--length", \
                       type    = 'int', \
                       help    = 'Max Length of the reads: Default:  %d'%(defaultLen), \
                       default = defaultLen )
                   
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        fastqList  = []
        fastqs     = args[0].split(',')
        
        if(fastqs[0].split(".")[-1]=="gz"):
            unzipCMD = "| gunzip -c"
        elif(fastqs[0].split(".")[-1]=="bz2"):
            unzipCMD = "| bunzip2 -c"
        else:
            unzipCMD = " "
        #####
        
        
        baseName   = args[1]
        # bowtie and awk command stuff
        if ( not options.NUGEN):
            if(testDirectory('/opt/samtools-0.1.18/')):
                bowTieCMD      = "/home/raid2/LINUXOPT/bowtie2-2.2.6/bowtie2 -p 6 --quiet --local -x /home/t5c2/IlluminaRaw/Genomes/Phix/phix -"
                filterCMD      = "/home/raid2/LINUXOPT/samtools-1.2/bin/samtools view -F 4 - | awk '{print $1}'"
                contamCMD      = '%s | %s'%(bowTieCMD, filterCMD)
            else:
                bowTieCMD      = "/home/raid2/LINUXOPT/bowtie2-2.2.6/bowtie2 -p 6 --quiet --local -x /home/t5c2/IlluminaRaw/Genomes/Phix/phix -"
                filterCMD      = "/home/raid2/LINUXOPT/samtools-1.2/bin/samtools view -F 4 - | awk '{print $1}'"
                contamCMD      = '%s | %s'%(bowTieCMD, filterCMD)
            #####
            BlatStart    = time()
            r1_frontProc = subprocess.Popen("cat %s  %s| awk '{print $0; print $0 | \"pbzip2 -p3 -9 > %s_R1.fastq.bz2\"}' | %s "%(fastqs[0], unzipCMD, baseName, contamCMD), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
            contamList   = r1_frontProc.communicate()[0].strip().split("\n")
            contamList   = [item for item in contamList]
            blatTime     = time()-BlatStart
            fastqList.append("cat %s_R1.fastq.bz2 | bunzip2 -c"%(baseName))
            
            if(r1_frontProc.poll() != 0):
                stderr.write('SCREENING ON R1 FAILED\n')
                assert False
            #####
            
            if(len(fastqs)>1): 
                BlatStart    = time()
                r2_frontProc = subprocess.Popen("cat %s  %s| awk '{print $0; print $0 | \"pbzip2 -p3 -9 > %s_R2.fastq.bz2\"}' | %s "%(fastqs[1],  unzipCMD, baseName, contamCMD), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                contamList_1 = r2_frontProc.communicate()[0].strip().split("\n")
                contamList_1 = [item for item in contamList_1]
                contamList  += contamList_1
                blatTime    += time()-BlatStart
                fastqList.append("cat %s_R2.fastq.bz2 | bunzip2 -c"%(baseName))
                
                if(r2_frontProc.poll() != 0):
                    stderr.write('SCREENING ON R2 FAILED\n')
                    assert False
                #####
            #####
            contamSet = set(contamList)
            
        else:
            if(testDirectory('/opt/samtools-0.1.18/')):
                bowTieCMD      = "/home/raid2/LINUXOPT/bowtie2-2.2.6/bowtie2 -p 6 --quiet --local -x /home/t5c2/IlluminaRaw/Genomes/Ecoli/ecoli_module0 -"
                filterCMD      = "/home/raid2/LINUXOPT/samtools-1.2/bin/samtools view -F 4 - | awk '{print $1}'"
                contamCMD      = '%s | %s'%(bowTieCMD, filterCMD)
            else:
                bowTieCMD      = "/home/raid2/LINUXOPT/bowtie2-2.2.6/bowtie2 -p 6 --quiet --local -x /home/t5c2/IlluminaRaw/Genomes/Ecoli/ecoli_module0 -"
                filterCMD      = "/home/raid2/LINUXOPT/samtools-1.2/bin/samtools view -F 4 - | awk '{print $1}'"
                contamCMD      = '%s | %s'%(bowTieCMD, filterCMD)
            #####
            BlatStart    = time()
            r1_frontProc = subprocess.Popen("cat %s  %s| awk '{print $0; print $0 | \"pbzip2 -p3 -9 > %s_R1.fastq.bz2\"}' | %s "%(fastqs[0], unzipCMD, baseName, contamCMD), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
            contamList   = r1_frontProc.communicate()[0].strip().split("\n")
            contamList   = [item for item in contamList]
            contamSet    = set(contamList)
            blatTime     = time()-BlatStart
            fastqList.append("cat %s_R1.fastq.bz2 | bunzip2 -c"%(baseName))
            
            if(r1_frontProc.poll() != 0):
                stderr.write('SCREENING ON R1 FAILED\n')
                assert False
            #####
            
            if(len(fastqs)>1): 
                BlatStart    = time()
                r2_frontProc = subprocess.Popen("cat %s  %s| awk '{print $0; print $0 | \"pbzip2 -p3 -9 > %s_R2.fastq.bz2\"}' | %s "%(fastqs[1],  unzipCMD, baseName, contamCMD), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                contamList_1 = r2_frontProc.communicate()[0].strip().split("\n")
                contamList_1 = [item for item in contamList_1]
                contamList_1 = set(contamList_1)
                contamSet    = contamSet.intersection(contamList_1)
                
                blatTime    += time()-BlatStart
                fastqList.append("cat %s_R2.fastq.bz2 | bunzip2 -c"%(baseName))
                
                if(r2_frontProc.poll() != 0):
                    stderr.write('SCREENING ON R2 FAILED\n')
                    assert False
                #####
            #####
        #####
        
        statsCallDict_genome = dict([ (i,doNothing) for i in xrange(100)])
        statsCallDict_phix   = dict([ (i,doNothing) for i in xrange(100)])
        
        if (options.RefSeq):
            refSeqList = options.RefSeq.split(None)
            for ref in refSeqList: testFile(ref)
        #####
        
        if (options.ContamSeq):
            contamSeqList = options.ContamSeq.split(None)
            for contam in contamSeqList: testFile(contam)
        #####
    
    #####
           
    if ( ( options.offSet > 0 ) and ( options.Separator != None ) ):    
        offset    = options.offSet
        if (offset == 33):
            winMax = 80
        elif (offset == 64):
            winMax = 120
        else:
            raise IOError('UNKNOWN QUAL SCORE ENCODING')
        #####
        
        sepString = options.Separator
        if (sepString == "space"): sepString = " " 
        
        readLength  = options.length
        
#         headCmd     = "head -4000 | awk '{if(NR%4==2){x += 1 ; total += length($1)}}END{AVG = total/x; split(AVG,decimal,\".\"); if((length(decimal)>1)&&(decimal[2]>0)){AVG = int(AVG+1); print AVG; exit}print int(AVG) }'"
#         sepProcess  = subprocess.Popen( '%s | %s'%(fastqList[0], headCmd), shell=True, stdout=subprocess.PIPE)
#         readLength  = int(sepProcess.communicate()[0].strip())
                
    else:
        headCmd     = 'head -4000'
        sepProcess  = subprocess.Popen( '%s | %s'%(fastqList[0], headCmd), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
        # Use the 1000 reads pulled to determine phred encoding and sepString
        testQual  = ''
        sepString = None
        while(True):
            try:    
                lines = [     sepProcess.stdout.readline().decode('utf-8') ]
                lines.append( sepProcess.stdout.readline().decode('utf-8') )
                lines.append( sepProcess.stdout.readline().decode('utf-8') )
                lines.append( sepProcess.stdout.readline().decode('utf-8') )
                testQual   = ''.join([testQual,lines[3].split(None)[0]])
                readLength = len(lines[3].split(None)[0]) 
            except IndexError:
#                 valueList = []
#                 for value in testQual: valueList.append(ord(value)) 
#                 valueList.sort()
#                 maxVal = valueList[-1]
#                 minVal = valueList[0]
#                 if( (minVal >= 33) and (maxVal <= 76) ):
#                     winMax = 80
#                     offset = 33
#                 elif( (minVal >= 54) and (maxVal <= 114) ):
#                     winMax = 120
#                     offset = 64
#                 else:
#                     raise IOError('UNKNOWN QUAL SCORE ENCODING')
#                 ##### 
                offset = 33
                winMax = 80
                break 
            #####
            if ( sepString == None ):
                for line in lines:
                    if ( line[0] == "@" ):
                        readEnd = line.strip().split(" ")[0][-2:]
                        sepString = " "
                        if ( (readEnd == "/1") or (readEnd == "/2") ): sepString = "/"
                        break
                    else:
                        continue
                    #####
                #####
            #####
        #####
        readLength  = options.length
        stderr.write("===========================================================================\nDO NOT WORRY THE PROGRAM DID NOT FAIL. IT WAS JUST CHECKING PHRED ENCODING.\n===========================================================================\n\n")
    #####
    #=========================================================================
    # USE THE CODE ABOVE TO figure out what type of PHRED score conversion to use
    charToAscii = dict( [ (chr(val),(int(val)-offset)) for val in range(0,120)] )
    asciiToStr  = dict( [ (chr(val),"%d"%(int(val)-offset)) for val in range(0,120)] )
    intToChar   = dict( [ ((int(val)-offset),chr(val)) for val in range((offset),(winMax+1))] )
    
    #=========================================================================
    # make the out Handles. It is here becasue the qual output need the offset form the qual encoding
    
    outHandleDict  = { '1':[None,None,None,None,None,None,None,None], \
                       '2':[None,None,None,None,None,None,None,None] }
    if ( options.NUGEN ):
        if ( len(fastqs) > 1 ):
            baseName1             = '%s_R1'%baseName
            outHandleDict['1'][0] = '%s_ecoli.fastq.bz2'%(baseName1)
            outHandleDict['1'][1] = '%s_genome.fastq.bz2'%(baseName1)
            outHandleDict['1'][4] = '%s.phredHist'%(baseName1)
            outHandleDict['1'][5] = '%s.tmp.stats'%(baseName1)
            outHandleDict['1'][6] = '%s_ecoli.phredHist'%(baseName1)
            outHandleDict['1'][7] = '%s_ecoli.tmp.stats'%(baseName1)
            
            baseName2             = '%s_R2'%baseName
            outHandleDict['2'][0] = '%s_ecoli.fastq.bz2'%(baseName2)
            outHandleDict['2'][1] = '%s_genome.fastq.bz2'%(baseName2)
            outHandleDict['2'][4] = '%s.phredHist'%(baseName2)
            outHandleDict['2'][5] = '%s.tmp.stats'%(baseName2)
            outHandleDict['2'][6] = '%s_ecoli.phredHist'%(baseName2)
            outHandleDict['2'][7] = '%s_ecoli.tmp.stats'%(baseName2)
            
            if ( options.noSeqQual ):
                outHandleDict['1'][2] = '%s.fasta.bz2'%(baseName1)
                outHandleDict['1'][3] = '%s.qual.bz2'%(baseName1)
                
                outHandleDict['2'][2] = '%s.fasta.bz2'%(baseName2)
                outHandleDict['2'][3] = '%s.qual.bz2'%(baseName2)
            #####
        else:
            baseName1             = '%s_R1'%baseName
            outHandleDict['1'][0] = '%s_ecoli.fastq.bz2'%(baseName1)
            outHandleDict['1'][1] = '%s_genome.fastq.bz2'%(baseName1)
            outHandleDict['1'][4] = '%s.phredHist'%(baseName1)
            outHandleDict['1'][5] = '%s.tmp.stats'%(baseName1)
            outHandleDict['1'][6] = '%s_ecoli.phredHist'%(baseName1)
            outHandleDict['1'][7] = '%s_ecoli.tmp.stats'%(baseName1)
            if(options.noSeqQual):
                outHandleDict['1'][2] = '%s.fasta.bz2'%(baseName1)
                outHandleDict['1'][3] = '%s.qual.bz2'%(baseName1)
            #####
        #####
    else:                      
        if ( len(fastqs) > 1 ):
            baseName1             = '%s_R1'%baseName
            outHandleDict['1'][0] = '%s_phix.fastq.bz2'%(baseName1)
            outHandleDict['1'][1] = '%s_genome.fastq.bz2'%(baseName1)
            outHandleDict['1'][4] = '%s.phredHist'%(baseName1)
            outHandleDict['1'][5] = '%s.tmp.stats'%(baseName1)
            outHandleDict['1'][6] = '%s_phix.phredHist'%(baseName1)
            outHandleDict['1'][7] = '%s_phix.tmp.stats'%(baseName1)
            
            baseName2             = '%s_R2'%baseName
            outHandleDict['2'][0] = '%s_phix.fastq.bz2'%(baseName2)
            outHandleDict['2'][1] = '%s_genome.fastq.bz2'%(baseName2)
            outHandleDict['2'][4] = '%s.phredHist'%(baseName2)
            outHandleDict['2'][5] = '%s.tmp.stats'%(baseName2)
            outHandleDict['2'][6] = '%s_phix.phredHist'%(baseName2)
            outHandleDict['2'][7] = '%s_phix.tmp.stats'%(baseName2)
            
            if ( options.noSeqQual ):
                outHandleDict['1'][2] = '%s.fasta.bz2'%(baseName1)
                outHandleDict['1'][3] = '%s.qual.bz2'%(baseName1)
                
                outHandleDict['2'][2] = '%s.fasta.bz2'%(baseName2)
                outHandleDict['2'][3] = '%s.qual.bz2'%(baseName2)
            #####
        else:
            baseName1             = '%s_R1'%baseName
            outHandleDict['1'][0] = '%s_phix.fastq.bz2'%(baseName1)
            outHandleDict['1'][1] = '%s_genome.fastq.bz2'%(baseName1)
            outHandleDict['1'][4] = '%s.phredHist'%(baseName1)
            outHandleDict['1'][5] = '%s.tmp.stats'%(baseName1)
            outHandleDict['1'][6] = '%s_phix.phredHist'%(baseName1)
            outHandleDict['1'][7] = '%s_phix.tmp.stats'%(baseName1)
            if(options.noSeqQual):
                outHandleDict['1'][2] = '%s.fasta.bz2'%(baseName1)
                outHandleDict['1'][3] = '%s.qual.bz2'%(baseName1)
            #####
        #####
    #####
        
    #=========================================================================
    # Build the awk command needed to pull the reads    
    if(sepString == " "):
        awkFastqCmd = "awk '{if(NR%%4==1){name = $0; LocIndex=substr($2,1,1)}if(NR%%4==2){seq=$1}if(NR%%4==3){next}if(NR%%4==0){print name\"\\t\"LocIndex\"\\t\"seq\"\\t\"$1}}'"%()
    else:
        awkFastqCmd = "awk '{if(NR%%4==1){name = $0; split($1,var,\"/\");LocIndex=var[2]}if(NR%%4==2){seq=$1}if(NR%%4==3){next}if(NR%%4==0){print name\"\\t\"LocIndex\"\\t\"seq\"\\t\"$1}}'"%()
    #####
    splitLine = lambda line : line.split("\t")
  
    phixBuff         = []
    fastQBuff        = []
    fastABuff        = []
    qualBuff         = []
    index            = ''
    globalStart      = time()
    for file_handle in fastqList:
        x              = iterCounter(100000)
        fastqHandle    = subprocess.Popen( '%s | %s'%( file_handle, awkFastqCmd ),shell=True, stdout=subprocess.PIPE )
        statsClass     = stats( readLength, offset, winMax, charToAscii, intToChar )
        
        for i in range(options.RandSamp): 
            statsCallDict_genome[i] = statsClass.countGenome
            statsCallDict_phix[i]   = statsClass.countPhix
        #####
        
        phredHistClass = phredHist( readLength, intToChar, winMax, offset )
        
        
        if( index == ''): 
            index = '1'
        else:
            index = '2'
        #####
        
        gFastq_oh      = Bzip_file(outHandleDict[index][1],'w')
        pFastq_oh      = Bzip_file(outHandleDict[index][0],'w')
        if(options.noSeqQual):
            fasta_oh   = Bzip_file(outHandleDict[index][2],'w')
            qual_oh    = Pbzip_file(outHandleDict[index][3], offset, 'w')
        #####
        
        totReads  = 0
        totBases  = 0
        totPhix   = 0
        totGBases = 0
        
        for line in fastqHandle.stdout:
            name, index, seq, qual = splitLine( line )
            qual      = qual.strip()
            baseName  = name.split( sepString )[0][1:]            
            
            totReads += 1
            totBases += len(seq)
            
            if( baseName in contamSet ):
                statsCallDict_phix[int(100*random())]( seq, qual )
                phredHistClass.incrementPhix(  qual )       
                totPhix += 1
                pFastq_oh.write( '%s\n%s\n+\n%s\n'%( name, seq, qual ) )
            else:
                totGBases += len(seq)
                statsCallDict_genome[int(100*random())]( seq, qual )
                phredHistClass.incrementGenome( qual )
                gFastq_oh.write( '%s\n%s\n+\n%s\n'%( name, seq, qual ) )
                if(options.noSeqQual):
                    fasta_oh.write( '>%s\n%s\n'%( name[1:], seq  ) )
                    qual_oh.write( '>%s\n%s\n'%( name[1:], qual ) )
                #####
            #####

            #x()
        #####
        endTime = time()-globalStart
        statsClass.writeStats(         outHandleDict[index][5], outHandleDict[index][7] )
        hqGbase, gReads, hqPbase, pReads = phredHistClass.writePhredHist( outHandleDict[index][4], outHandleDict[index][6] )
        gFastq_oh.close()
        pFastq_oh.close()
        if(options.noSeqQual):
            fasta_oh.close()
            qual_oh.close()
        #####
        # Needed for the overall stats
        AVGreadLength = float(totBases)/totReads
        genomeAHQ     = float(hqGbase)/gReads
        sepArg        = sepString
        if (sepArg == " "): sepArg = "space"
        print ("%d %d %.1f %.1f %d %d %d %d %d %s %s"%(totReads, totBases, AVGreadLength, genomeAHQ, hqGbase, gReads, hqPbase, pReads, totGBases, offset, sepArg))
        fastqHandle.poll()
    #####                
    #stderr.write('%.2f\n'%(endTime+blatTime))
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()    
    
