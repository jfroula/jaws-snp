#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2016$"
#=========================================================================
# EDIT LOG
# 10/11/16 added better _ control in the catagory names, It will ow remove exrta _ from the name
# 10/11/16 Will not remove str from the end of numbers to allow for convertion inside the script
# 08/09/17 Added change suggested by Chris Beecroft for handling contamscreening data: making a list of dicts with {scaffold:DATA;scaffold_name:NAME}
#
#=========================================================================

from os.path import realpath, isdir, isfile, join, abspath, curdir

from os import system, fsync, getpid, uname, mkdir, chdir

from sys import stderr, stdout

from optparse import OptionParser

import subprocess

import re

import getpass

from time import sleep

import sdm_curl 

from sdm_curl import Curl

import time

from hagsc_lib import testFile
#=========================================================================
def real_main():
    # Defining the program options
    usage = "usage: %prog [CONFIG FILE] [options]"

    parser = OptionParser(usage)

#     nReads = -1
#     parser.add_option( '-n', \
#                        "--nReads", \
#                        type    = 'int', \
#                        help    = 'Number of reads to extract: Default:  Extract all.', \
#                        default = nReads )
#                        
    contamFASTA = None
    parser.add_option( "-s", \
                       "--stopStr", \
                       type    = 'str', \
                       help    = 'A string once encountered will stop processing of METADATA FILE.  Default:  None', \
                       default = contamFASTA )
    
    contamFASTA = None
    parser.add_option( "-k", \
                       "--skipStr", \
                       type    = 'str', \
                       help    = 'Skip all lines with this string in the METADATA FILE.  Default:  None', \
                       default = contamFASTA )
    
    parser.add_option( '-t', \
                       "--testMetaData", \
                       action  = "store_true", \
                       dest    = 'testMetaData', \
                       help    = "Reads MetaData and prints it to the screen to manual verification before a large job DOES NOT ARCHIVE DATA.  Default:  Does not print to the screen DOES archive data." )
    parser.set_defaults( testMetaData = False )
    
    parser.add_option( '-e', \
                       "--showConfigExample", \
                       action  = "store_true", \
                       dest    = 'showConfigExample', \
                       help    = "Shows an example config file and exits program." )
    parser.set_defaults( showConfigExample = False )
    
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        if (options.showConfigExample):
            print "FILE	LOCATION	DESTINATION	META_DATA	ORIGFILE	FILETYPE	STATE\n"+\
                  "myFile   .   SNPS/TESTING/   myMeta.dat  7647.1.78849.GAGTGG.fastq.gz	fastq,fastq.gz		UNPROCESSED"
            exit()
        else:
            parser.error( "Incorrect number of arguments.  " + \
                          "View usage using --help option." )
        #####
    else:
        # Pulling CONFIG file
        CONFIGfile = args[0]
        
        if (False):
            curl = Curl('https://sdm-dev.jgi-psf.org',appToken='TESTTOKEN')
        else:
            curl = Curl('https://sdm2.jgi-psf.org',appToken='GPSANPU3AVSW2KUGDO3A79L18H2K3OGK')
            x=1
        #####
        stopStr = None
        if (options.stopStr != None ):
            stopStr = options.stopStr
            
            # Is the string to exist of not
            if (options.stopStr[0] == "!"):
                # If not found
                findStop = lambda i: True if i.count("%s"%(stopStr[1:]))==0 else False
            else:
                # If found
                findStop = lambda i: True if i.count("%s"%(stopStr))!=0 else False
            #####
        #####
        
        skipStr = None
        if (options.skipStr != None ):
            skipStr = options.skipStr
            
            # Is the string to exist of not
            if (options.skipStr[0] == "!"):
                # If not found
                findSkip = lambda i: True if i.count("%s"%(skipStr[1:]))==0 else False
            else:
                # If found
                findSkip = lambda i: True if i.count("%s"%(skipStr))!=0 else False
            #####
        #####
        # This will allow the unformated line data to be formated and then collected
        lineFormatter    = lambda i: "\t" if i.count("\t")==1 else "  " 
        valueFormat      = re.compile('[0-9][0-9][0-9] [A-Za-z]{1,}')
    #####
    
    changeStateDict = {}
    # Use a config file 
    # fileName location destination additionalMetaDataFile ORIG_FILE File Type state(UNPROCESSED/FAILED/PROCESSED:ID)
    report_oh = open('archiverReport.%s.report'%(time.strftime("%H.%M.%S_%d-%m-%Y")), "w") 
    for line in open(CONFIGfile):
        data = line.split(None)
        
        # Has the file been archived or failed
        if (data[6] not in ["UNPROCESSED","FAILED"]): 
            changeStateDict[data[0]]=data
            continue
        
        if (data[6] == "FAILED"): 
            stderr.write("!!!!!!!!!!%s HAS AN ERROR PLEASE CLEAR THE ERROR AND RERUN\nPROCEEDING WITH OTHER FILES!!!!!!!!!!\n"%(data[0]))    
            changeStateDict[data[0]]=data
            continue
        #####
        
        testFile("%s/%s"%(abspath(data[1]),data[0]))
        
        # If not then attempt to archive the file
        prepdict       = {}
        proceedWithArc = True
        
        
        META_NAME = "hudson_alpha_summary"
        ORIG_FILE = data[4].split(",")
        FILE_TYPE = data[5].split(",")
        
        isExtractStats = False
        runSpecial     = False
        
        if (data[3].find("extractionStats") > 0): isExtractStats = True
        
        if (data[3] != "-"):
            for line in open(data[3]):
                # Is the line formatted correctly
                if ( ( line.count(":") == 0 ) and (( line.count("\t") == 1 ) or ( line.count("  ") >= 1 )) ):
                    
                    newpattern = re.compile(lineFormatter(line)).search(line)
                    myLine = list(line)
                    myLine[newpattern.start(0)] = ":" 
                    
                    line = "".join(myLine)
                #####
                if ( (stopStr != None) and ( findStop(line)) ): break
                if ( (skipStr != None) and ( findSkip(line)) ): continue
                if ( (line.split(":")[0] == "Contaminant Summary") and (isExtractStats) ): 
                    prepdict["Contaminant Summary"] = []
                    runSpecial = True
                
                try:
                    catagory, value = line.split(":")
                    catagory        = catagory.replace(" ","_").replace("|","_").replace("\t","_").replace(".","").lower()
                    
                    if (catagory.count("__")>0):
                        while (True):
                            catagory = catagory.replace("__","_")
                            if (catagory.count("__")==0): break
                        #####
                    #####
                    
                    value           = value.strip()
                    if (valueFormat.search(value) != None):
                        value = value.split(None)[0]
                    #####
                                        
                    # If the value is an int then it need to be converted
                    try:
                        
                        tmpVal = ''.join(value.split(","))
                        if (tmpVal.count(".")==1):
                            value = float(tmpVal)
                        else:
                            value  = int(tmpVal)
                        #####
                        
                    except ValueError:
                        value = value.replace("/","_").replace(" ","_").replace("|","_").replace("\t","_").lower()
                    #####
                    
                    if (runSpecial):
                        prepdict["Contaminant Summary"].append({"scaffold":val,"scaffold_name":catagory})
                    else:
                        prepdict[catagory] = value
                    #####
                except (ValueError, KeyError) as e:
                    continue
                #####
            #####
            
            if ( None in [META_NAME, ORIG_FILE, FILE_TYPE]):
                stderr.write("!!!!!!!!!!!!!!!!!!!!\n%s DOES NOT HAVE ONE OF THE FOLLOWING DEFINED IN THE METADATA FILE:\n\tmeta_data_name\n\torig_file\n\tfile_type\nSKIPPING THIS FILE. ADD THE MISSING INFORMATION AND RELAUNCH\n!!!!!!!!!!!!!!!!!!!!"%(data[0]))
                proceedWithArc = False
                break
            #####
            
            if (ORIG_FILE[0] == "NONE"):
                copyprocdict = {}
            else:
                copyproc     = [item.strip() for item in subprocess.Popen("jamo report meta_search all filename %s"%(ORIG_FILE[0]) ,shell=True, stdout=subprocess.PIPE).stdout][0] 
                copyprocdict = dict([(item.split(":")[0],item.split(":")[1]) for item in copyproc.split("+")])          
                copyprocdict['illumina_sdm_seq_unit_id'] = int(copyprocdict['illumina_sdm_seq_unit_id'])
            #####
            copyprocdict[META_NAME] = prepdict
        #####
        
        
        if (not proceedWithArc): continue
            
            
        inputFile = [] #[[item.strip() for item in subprocess.Popen("jamo info filename %s | awk '{print $4}'"%(ORIG_FILE),shell=True, stdout=subprocess.PIPE).stdout][0]]
        if (ORIG_FILE[0] != "NONE"):
            for f in ORIG_FILE : inputFile.append([item.strip() for item in subprocess.Popen("jamo info all filename %s | awk '{print $4}'"%(f),shell=True, stdout=subprocess.PIPE).stdout][0])
        #####
        
        if (options.testMetaData):
            print "ALL META DATA BEING ADDED TO JAMO FOR %s"%(data[0])
            for k,v in copyprocdict.iteritems():
                if (k==META_NAME):
                    print "\t%s"%(k)
                    for l,w in v.iteritems():print "\t\t%s: %s"%(l,w)
                    continue
                #####
                print "\t%s: %s"%(k,v)
            continue
        #####
            
            
        # Now that we have the metadata lets start archiving
        try:
            system("chmod 666 %s/%s"%(abspath(data[1]),data[0]))
            if (data[2][-1] != "/"): data[2] = "%s/"%(data[2])
            
            if (ORIG_FILE != "NONE"):    
                response = curl.post('api/metadata/file', file = '%s/%s'%(abspath(data[1]),data[0]), inputs = inputFile, file_type = FILE_TYPE, metadata = copyprocdict, destination = '%s'%(data[2]), put_mode="Replace_If_Newer", local_purge_days = 90, backup_services = [1])
            else:
                response = curl.post('api/metadata/file', file = '%s/%s'%(abspath(data[1]),data[0]), file_type = FILE_TYPE, metadata = copyprocdict, destination = '%s'%(data[2]), put_mode="Replace_If_Newer", local_purge_days = 90, backup_services = [1])
            #####
            
            report_oh.write("%s-%s\n"%(data[0],response['metadata_id']))
            
            changeStateDict[data[0]]=data
            changeStateDict[data[0]][6]="ARCHIVED"
        except (sdm_curl.CurlHttpException) as e:
            
            print e 
            changeStateDict[data[0]][6]="FAILED"
            #system("chmod 660  %s/%s"%(abspath(data[1]),data[0]))
                        
            #try:
            #    redoList[library]["R2"] = copyprocdict
            #except KeyError:
            #    redoList[library]       = {}
            #    redoList[library]["R2"] = copyprocdict
            #####  
        #####
    #####
    report_oh.close()
    
    if (not options.testMetaData):
        # Update the config file
        rewrite = open("%s"%(CONFIGfile),"w")
        for k,v in changeStateDict.iteritems(): rewrite.write("%s\n"%('\t'.join(v)))
        rewrite.close()
    
        stderr.write("::::::::::::::::::::::::::::::\nREMEMBER ALLOW TIME FOR THE\nCOPY TO JAMO TO COMPLETE BEFORE\nDELETING THE STORED FILES\n::::::::::::::::::::::::::::::")
    #####
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()