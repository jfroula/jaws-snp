#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from optparse import OptionParser

from os.path import realpath, abspath, curdir, join, isdir

from hagsc_lib import testFile, testDirectory, iterFASTA, writeFASTA, revComp, SeqRecord
from hagsc_lib import remoteServer4, deleteFile, deleteDir, FASTAFile_dict, iterCounter

from os import system, mkdir

#=====================================================================
def parseMarkerName(markerName):
    splitName = markerName.split('|')
    if ( len(splitName) == 5 ):
        return {'markerID':splitName[0], \
                'LG':splitName[1], \
                'mapPos':splitName[2], \
                'contigList':[splitName[4]], \
                'altHap':False }
    else:
        return {'markerID':splitName[0], \
                'LG':splitName[1], \
                'mapPos':splitName[2], \
                'contigList':[splitName[4],splitName[5]], \
                'altHap':True }
    #####

#=====================================================================
def create_tmp_dir(tmpPath, tmpDir, deleteExistingDir=True):
    if ( deleteExistingDir ):
        if ( isdir(tmpPath) ): deleteDir(tmpPath)
        mkdir(tmpDir, 0777)
    #####
    testDirectory(tmpDir)
    return

#==============================================================
def initialSetup(logFile,makeNewDirs,tmpDir,segPath):
    oh_log = open(logFile,'w')
    # Creating a temporary directory
    basePath = abspath(curdir)
    tmpPath  = join( basePath, tmpDir )
    create_tmp_dir( tmpPath, tmpPath, deleteExistingDir=makeNewDirs )
    oh_log.write( 'CREATING TEMPORARY DIRECTORY:  %s\n'%tmpPath )
    for LG in xrange(1,19):
        tmpDir = join( tmpPath, 'LG_%d'%LG )
        create_tmp_dir( tmpDir, tmpDir, deleteExistingDir=makeNewDirs )
        oh_log.write( '\tCREATING TEMPORARY DIRECTORY:  %s\n'%tmpDir )
    #####
    oh_log.close()
    return tmpPath

#==============================================================
def alignMarkersToClones( markerFile, cloneFASTAFile, nJobs, clusterNum, cloneMarkerAlignmentFile, logFile ):
    system( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/alignMarkersToClones.py %s %s %d %s %s %s'%(markerFile, \
                                                                 cloneFASTAFile, nJobs, clusterNum, cloneMarkerAlignmentFile, logFile) )
    
#==============================================================
def screenHeterozygousClones(tmpPath, cloneMarkerAlignmentFile, cloneFASTAFile, logFile, segPath):
    """
    This code does the following things:
      (1)  Find and set aside all clone sequence that has markers from multiple linkage groups.
      (2)  Writes a file containing all heterozygous clones (heterozygousClones.dat)
      (3)  Partitions all blat alignments into the separate linkage groups (LG%s.clone.alignments.blat)
      (4)  Segregates clones into linkage groups (LG%s.clones.fasta)
      (5)  Creates a clone bottom drawer
    """
    # Opening the log file
    oh_log = open( logFile, 'a')

    # Output to the user
    oh_log.write( 'CHECKING FOR HETEROZYGOUS CLONES\n' )

    # Read in the alignments
    clone_LG_dict = {}
    tmpLGSet      = set()
    for line in open(cloneMarkerAlignmentFile):
        splitLine        = line.split(None)
        cloneID          = splitLine[13].split("_")[0]
        parsedMarkerName = parseMarkerName(splitLine[9])
        LG               = parsedMarkerName['LG']
        try:
            clone_LG_dict[cloneID].add(LG)
        except KeyError:
            clone_LG_dict[cloneID] = set([LG])
        #####
        tmpLGSet.add( LG )
    #####
    
    # Find heterozygous clones
    heteroCloneSet = set( [ k for k,v in clone_LG_dict.iteritems() if (len(v)>1) ] )
    
    # Removing heterozygous clones from the dictionary
    map( clone_LG_dict.pop, heteroCloneSet )

    # Writing out the heterozygous clones to a file
    heteroCloneOutputFile = join( tmpPath, 'heterozygousClones.dat' )
    oh = open( heteroCloneOutputFile, 'w' )
    for cloneID in heteroCloneSet:
        outputString = '\t%s\n'%cloneID
        oh.write( outputString )
        oh_log.write( outputString )
    #####
    oh.close()

    # Removing heterozygous alignments and partitioning the alignments into linkage groups
    oh_log.write( 'SCREENING ALIGNMENTS\n' )
    align_LG_handles = {}
    for LG in tmpLGSet: align_LG_handles[LG] = open( join(tmpPath,'LG_%s/LG%s.clone.alignments.blat'%(LG,LG)), 'w' )
    for line in open(cloneMarkerAlignmentFile):
        splitLine = line.split(None)
        cloneID   = splitLine[13].split("_")[0]
        if ( cloneID in heteroCloneSet ): continue
        parsedMarkerName = parseMarkerName(splitLine[9])
        LG        = parsedMarkerName['LG']
        align_LG_handles[LG].write( line )
    #####
    for LG in tmpLGSet: align_LG_handles[LG].close()

    # Segregate clones into linkage groups
    oh_log.write( 'SEGREGATING CLONES TO A LINKAGE GROUP AND BUILDING CLONE BOTTOM DRAWER\n' )
    clone_LG_handles = {}
    for LG in tmpLGSet: clone_LG_handles[LG] = open( join(tmpPath, 'LG_%s/LG%s.clones.fasta'%(LG,LG)), 'w' )
    oh_BD = open( join( segPath, 'clone_bottomDrawer.fasta' ), 'w' )
    for record in iterFASTA(open(cloneFASTAFile)):
        cloneID = record.id.split("_")[0]
        if ( cloneID in heteroCloneSet ): continue
        try:
            LG = list(clone_LG_dict[cloneID])[0]
            writeFASTA( [record], clone_LG_handles[LG] )
        except KeyError:
            writeFASTA( [record], oh_BD )
            oh_log.write( '\tNO MARKER ALIGNMENTS FOR: %s\n'%record.id )
        #####
    #####
    for LG in tmpLGSet: clone_LG_handles[LG].close()
    oh_log.close()
    oh_BD.close()

#==============================================================
def collapseContigsOnClones(tmpPath, segPath, logFile, clusterNum, merCountCutoff, kmerSize, ID_cutoff, COV_cutoff):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'COLLAPSING CONTIGS ONTO CLONES\n' )
    oh_log.close()
    cmdList = []
    libPath = '/mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass'
    for LG in xrange(1,19): 
        cmdList.append( 'python %s/new_collapseContigsOnClones.py %s %s %s %d %d %d %d %d'%(libPath, tmpPath, segPath, logFile, LG, merCountCutoff, kmerSize, ID_cutoff, COV_cutoff) )
    #####
    plFile = 'collapsingContigsOnClones.pl'
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile=plFile )
    deleteFile(plFile)

#==============================================================
def splittingContigsAndClones(basePath, logFile):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'PERFORMING THE GENERALIZED COLLAPSE OF THE CLONE/CONTIGS\n' )
    oh_log.close()
    for LG in xrange(1,19):
        print LG
        # Opening the clone only file
        cloneOnlyFASTA = join( basePath, 'LG_%d/LG%d.clone.preCollapse.fasta'%(LG,LG) )
        oh_clone       = open( cloneOnlyFASTA, 'w' )
        # Opening the contig only file
        contigOnlyFASTA = join( basePath, 'LG_%d/LG%d.contig.preCollapse.fasta'%(LG,LG) )
        oh_contig       = open( contigOnlyFASTA, 'w' )
        # Looping over the whole file
        cloneContigFASTA = join(basePath,'LG_%d/LG%d.clone.contig.fasta'%(LG,LG))
        for r in iterFASTA(open(cloneContigFASTA)):
            if ( r.id.count("CLONE") > 0 ):
                writeFASTA( [r], oh_clone )
            else:
                writeFASTA( [r], oh_contig )
            #####
        #####
        oh_clone.close()
        oh_contig.close()
    #####
    
#==============================================================
def markerFreeCollapsing( basePath, merCountCutoff, logFile, clusterNum, kmerSize, minAlignLength, minIdentity, alignType, overHang, maxGapSize ):

    oh_log = open( logFile, 'a' )
    oh_log.write( 'COLLAPSING %s ON %s\n'%(alignType,alignType) )
    oh_log.close()

    pythonPath = '/mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass'
    
    # This variable controls whether or not we run both the small-on-large and large-on-small alignments
    alignBothWays = 0

    cmdList = []
    for LG in xrange(1,19): 

        # Output files for the analysis
        if ( alignType == 'CLONES' ):
            
            basePath_LG         = join( basePath, 'LG_%d'%LG )
            alignmentGroupFile  = join( basePath_LG, 'LG%d.alignmentGroups_cutoff=%d_cloneCollapse.dat'%(LG,merCountCutoff) )
            pairCountsFile      = join( basePath_LG, 'LG%d.%dmer_pairCounts_cloneCollapse.dat'%(LG,kmerSize) )
            collapseSummaryFile = join( basePath_LG, 'LG%d.collapseSummary.cloneCollapse.dat'%(LG) )
            removedItemsFile    = join( basePath_LG, 'LG%d.removed_clones.dat'%(LG) )
            mergedFASTA         = join( basePath_LG, 'LG%d.mergedClones.fasta'%(LG) )
            # Input FASTA
            inputFASTA          = join( basePath_LG, 'LG%d.clone.preCollapse.fasta'%(LG) )
            suffix              = 'CLONES'
            
        elif ( alignType == 'CONTIGS' ):
        
            basePath_LG         = join( basePath, 'LG_%d'%LG )
            alignmentGroupFile  = join( basePath_LG, 'LG%d.alignmentGroups_cutoff=%d_contigCollapse.dat'%(LG,merCountCutoff) )
            pairCountsFile      = join( basePath_LG, 'LG%d.%dmer_pairCounts_contigCollapse.dat'%(LG,kmerSize) )
            collapseSummaryFile = join( basePath_LG, 'LG%d.collapseSummary.contigCollapse.dat'%(LG) )
            removedItemsFile    = join( basePath_LG, 'LG%d.removed_contigs.dat'%(LG) )
            mergedFASTA         = join( basePath_LG, 'LG%d.mergedContigs.fasta'%(LG) )
            # Input FASTA
            inputFASTA          = join( basePath_LG, 'LG%d.contig.preCollapse.fasta'%(LG) )
            # Alignment parameter
            alignBothWays       = 1
            suffix              = 'CONTIGS'

        elif ( alignType == 'COMBINED' ):
        
            basePath_LG         = join( basePath, 'LG_%d'%LG )
            alignmentGroupFile  = join( basePath_LG, 'LG%d.alignmentGroups_cutoff=%d_combineCollapse.dat'%(LG,merCountCutoff) )
            pairCountsFile      = join( basePath_LG, 'LG%d.%dmer_pairCounts_combineCollapse.dat'%(LG,kmerSize) )
            collapseSummaryFile = join( basePath_LG, 'LG%d.collapseSummary.combineCollapse.dat'%(LG) )
            removedItemsFile    = join( basePath_LG, 'LG%d.removed_combined.dat'%(LG) )
            mergedFASTA         = join( basePath_LG, 'LG%d.mergedCombined.fasta'%(LG) )
            # Input FASTA
            inputFASTA          = join( basePath_LG, 'LG%d.combined.fasta'%(LG) )
            suffix              = 'COMBINED'

        elif ( alignType == 'BOTTOMDRAWER' ):
        
            basePath_LG         = join( basePath, 'BD' )
            alignmentGroupFile  = join( basePath_LG, 'BD.alignmentGroups_cutoff=%d.dat'%(merCountCutoff) )
            pairCountsFile      = join( basePath_LG, 'BD.%dmer_pairCounts_combineCollapse.dat'%(kmerSize) )
            collapseSummaryFile = join( basePath_LG, 'BD.collapseSummary.dat' )
            removedItemsFile    = join( basePath_LG, 'BD.removed.dat' )
            mergedFASTA         = join( basePath_LG, 'BD.merged.fasta' )
            # Input FASTA
            inputFASTA          = join( basePath_LG, 'BD.fasta' )
            # Alignment parameter
            alignBothWays       = 1
            # Setting the suffix
            suffix              = 'BD'
            # Setting up the command
            cmd = 'python %s/generalizedCollapser.py %s %d %s %s %s %s %s %s %d %d %d %d %d %s'%(pythonPath, basePath_LG, merCountCutoff, alignmentGroupFile, \
                                                                                                 pairCountsFile, collapseSummaryFile, removedItemsFile, \
                                                                                                 mergedFASTA, inputFASTA, minAlignLength, alignBothWays, minIdentity, \
                                                                                                 overHang, maxGapSize, suffix)
            system( cmd )
            return

        #####
        cmdList.append( 'python %s/generalizedCollapser.py %s %d %s %s %s %s %s %s %d %d %d %d %d %s'%(pythonPath, basePath_LG, merCountCutoff, alignmentGroupFile, \
                                                                                                       pairCountsFile, collapseSummaryFile, removedItemsFile, \
                                                                                                       mergedFASTA, inputFASTA, minAlignLength, alignBothWays, minIdentity, \
                                                                                                       overHang, maxGapSize, suffix) )
    #####
    
    plFile = 'collapse_items_%s.pl'%alignType
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile=plFile )
    deleteFile(plFile)

#==============================================================
def segregateMarkers( tmpPath, logFile, markerFASTA ):

    oh_log = open( logFile, 'a' )
    oh_log.write( 'SEGREGATING MARKERS ONTO LINKAGE GROUPS\n' )
    oh_log.close()
    
    # Opening the output files
    oh_dict = {}
    for LG in xrange(1,19):
        oh_dict[str(LG)] = open( join( tmpPath, 'LG_%d/LG%d.markers.fasta'%(LG,LG) ), 'w' )
    #####
    
    # Segregating the markers
    x = iterCounter( 1000000 )
    for r in iterFASTA( open(markerFASTA) ):
        s = parseMarkerName( r.id )
        writeFASTA( [r], oh_dict[ s['LG'] ] )
        x()
    #####
    
    # Closing the output files
    for LG in xrange(1,19): oh_dict[str(LG)].close()

#==============================================================
def realignMarkers( tmpPath, logFile, clusterNum, alignType, segPath ):

    oh_log = open( logFile, 'a' )
    oh_log.write( 'REALIGNING MARKERS\n' )
    oh_log.close()

    # Realigning the markers
    frontCommand = '/mnt/local/EXBIN/blat -noHead'
    cmdList = []
    for LG in xrange(1,19):
        # Aligning the markers
        markerFile     = join( tmpPath, "LG_%d/LG%d.markers.fasta"%(LG,LG) )
        # Setting up the target and output files
        if   ( alignType == 'CLONES' ):
            targetFile     = join( tmpPath, "LG_%d/LG%d.mergedClones.fasta"%(LG,LG) )
            blatOutputFile = join( tmpPath, "LG_%d/LG%d.mergedClones.blat"%(LG,LG) )
        elif ( alignType == 'CONTIGS' ):
            targetFile     = join( tmpPath, "LG_%d/LG%d.mergedContigs.fasta"%(LG,LG) )
            blatOutputFile = join( tmpPath, "LG_%d/LG%d.mergedContigs.blat"%(LG,LG) )
        elif ( alignType == 'COMBINED' ):
            targetFile     = join( tmpPath, "LG_%d/LG%d.mergedCombined.fasta"%(LG,LG) )
            blatOutputFile = join( tmpPath, "LG_%d/LG%d.mergedCombined.blat"%(LG,LG) )
        elif ( alignType == 'NEW_CONTIGS' ):
            targetFile     = join( segPath, "LG%d.contigs.new.fasta"%(LG) )
            blatOutputFile = join( segPath, "LG%d.contigs.new.blat"%(LG) )
        elif ( alignType == 'NEW_CLONES' ):
            targetFile     = join( tmpPath, "LG_%d/LG%d.clones.new.fasta"%(LG,LG) )
            blatOutputFile = join( tmpPath, "LG_%d/LG%d.clones.new.blat"%(LG,LG) )
        elif ( alignType == 'COLLAPSED' ):
            targetFile     = join( tmpPath, "LG_%d/LG%d_phalliiSynteny/LG%d_rebroken_halliiSynteny.adjHapFixed.fasta"%(LG,LG,LG) )
            blatOutputFile = join( tmpPath, "LG_%d/LG%d_phalliiSynteny/LG%d_rebroken_halliiSynteny.adjHapFixed.blat"%(LG,LG,LG) )
        #####
        # Setting up the BLAT command
        blatCommand    = "rm -rf %s ; %s %s %s stdout | awk \'{x=(\$11-\$1)+\$2+\$6+\$8;if(x==0)print}\' > %s"%( blatOutputFile, frontCommand, targetFile, markerFile, blatOutputFile )
        # Building the final command list
        cmdList.append( blatCommand )
    #####
    plFile = 'reAlignMarkers_%s.pl'%alignType
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile=plFile )
    deleteFile(plFile)

#==============================================================
def findRedundant(tmpPath, logFile, clusterNum):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'ELIMINATING REDUNDANT CONTIGS\n' )
    oh_log.close()
    cmdList = []
    for LG in xrange(1,19): 
        cmdList.append( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findingRedundantContigs.py %d %s'%(LG,tmpPath) )
    #####
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile='findingRedundant.pl' )
    deleteFile('findingRedundant.pl')

#==============================================================
def identifyHaplotypeGroups(tmpPath, logFile, clusterNum, alignType, minMarkers):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'IDENTIFYING HAPLOTYPE GROUPS\n' )
    oh_log.close()
    cmdList = []
    for LG in xrange(1,19):
        if   ( alignType == 'CLONES' ):
            fastaFile        = join( tmpPath, "LG_%d/LG%d.mergedClones.fasta"%(LG,LG) )
            blatFile         = join( tmpPath, "LG_%d/LG%d.mergedClones.blat"%(LG,LG) ) 
            outputGroupsFile = join( tmpPath, "LG_%d/LG%d.mergedClones.cloneGroups.dat"%(LG,LG) )
        elif ( alignType == 'CONTIGS' ):
            fastaFile        = join( tmpPath, "LG_%d/LG%d.mergedContigs.fasta"%(LG,LG) )
            blatFile         = join( tmpPath, "LG_%d/LG%d.mergedContigs.blat"%(LG,LG) ) 
            outputGroupsFile = join( tmpPath, "LG_%d/LG%d.mergedContigs.contigGroups.dat"%(LG,LG) )
        elif ( alignType == 'COMBINED' ):
            fastaFile        = join( tmpPath, "LG_%d/LG%d.mergedCombined.fasta"%(LG,LG) )
            blatFile         = join( tmpPath, "LG_%d/LG%d.mergedCombined.blat"%(LG,LG) ) 
            outputGroupsFile = join( tmpPath, "LG_%d/LG%d.mergedCombined.combinedGroups.dat"%(LG,LG) )
        #####
        cmdList.append( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/identifyHaplotypeGroups.py %d %s %s %s %s %d'%(LG,tmpPath, \
                                                                                                   fastaFile,blatFile,outputGroupsFile, minMarkers) )
    #####
    plFile = 'identifyingHaplotypeGroups.%s.pl'%alignType
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile=plFile )
    deleteFile( plFile )

#==============================================================
def collapseOnMarkers(tmpPath, logFile, clusterNum, alignType, min_ID, minAlignLength, overHang, maxGapSize):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'MERGING HAPLOTYPES\n' )
    oh_log.close()
    cmdList = []
    pythonPath = '/mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass'
    for LG in xrange(1,19):
        if ( alignType == 'CLONES' ):
            contigFASTA   = join( tmpPath, "LG_%d/LG%d.mergedClones.fasta"%(LG,LG)            )
            blatFile      = join( tmpPath, "LG_%d/LG%d.mergedClones.blat"%(LG,LG)             ) 
            markerFASTA   = join( tmpPath, "LG_%d/LG%d.markers.fasta"%(LG,LG)                 )
            redundantFile = join( tmpPath, "LG_%d/LG%d.clone_clone_redundant.dat"%(LG,LG)     )
            summaryFile   = join( tmpPath, "LG_%d/LG%d.clone_clone_summary.dat"%(LG,LG)       )
            groupsFile    = join( tmpPath, "LG_%d/LG%d.mergedClones.cloneGroups.dat"%(LG,LG) )
            outputFASTA   = join( tmpPath, "LG_%d/LG%d.mergedClones.cloneGroups.fasta"%(LG,LG) )
            basePath_LG   = join( tmpPath, 'LG_%d'%LG )
            runBothAlignments = 0
        elif ( alignType == 'CONTIGS' ):
            contigFASTA   = join( tmpPath, "LG_%d/LG%d.mergedContigs.fasta"%(LG,LG)            )
            blatFile      = join( tmpPath, "LG_%d/LG%d.mergedContigs.blat"%(LG,LG)             ) 
            markerFASTA   = join( tmpPath, "LG_%d/LG%d.markers.fasta"%(LG,LG)                 )
            redundantFile = join( tmpPath, "LG_%d/LG%d.contig_contig_redundant.dat"%(LG,LG)     )
            summaryFile   = join( tmpPath, "LG_%d/LG%d.contig_contig_summary.dat"%(LG,LG)       )
            groupsFile    = join( tmpPath, "LG_%d/LG%d.mergedContigs.contigGroups.dat"%(LG,LG) )
            outputFASTA   = join( tmpPath, "LG_%d/LG%d.mergedContigs.contigGroups.fasta"%(LG,LG) )
            basePath_LG   = join( tmpPath, 'LG_%d'%LG )
            runBothAlignments = 1
        elif ( alignType == 'COMBINED' ):
            contigFASTA   = join( tmpPath, "LG_%d/LG%d.mergedCombined.fasta"%(LG,LG)            )
            blatFile      = join( tmpPath, "LG_%d/LG%d.mergedCombined.blat"%(LG,LG)             ) 
            markerFASTA   = join( tmpPath, "LG_%d/LG%d.markers.fasta"%(LG,LG)                 )
            redundantFile = join( tmpPath, "LG_%d/LG%d.combined_redundant.dat"%(LG,LG)     )
            summaryFile   = join( tmpPath, "LG_%d/LG%d.combined_summary.dat"%(LG,LG)       )
            groupsFile    = join( tmpPath, "LG_%d/LG%d.mergedCombined.combinedGroups.dat"%(LG,LG) )
            outputFASTA   = join( tmpPath, "LG_%d/LG%d.mergedCombined.combinedGroups.fasta"%(LG,LG) )
            basePath_LG   = join( tmpPath, 'LG_%d'%LG )
            runBothAlignments = 1
        #####
        
        cmdList.append( 'python %s/mergeHaplotypes.py %d %s %s %s %s %s %s %s %s %d %d %d %d %d %s'%( pythonPath, \
                                                                                                      LG,         \
                                                                                                      basePath_LG,   \
                                                                                                      markerFASTA,   \
                                                                                                      contigFASTA,   \
                                                                                                      blatFile,      \
                                                                                                      redundantFile, \
                                                                                                      summaryFile,   \
                                                                                                      groupsFile,    \
                                                                                                      outputFASTA,   \
                                                                                                      min_ID,        \
                                                                                                      minAlignLength, \
                                                                                                      runBothAlignments, \
                                                                                                      overHang, \
                                                                                                      maxGapSize, \
                                                                                                      alignType ) )
    #####
    
    plFile = 'collapseOnMarkers.%s.pl'%alignType
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile=plFile )
    deleteFile(plFile)

#==============================================================
def combine_clones_contigs( tmpPath, logFile ):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'MERGING CLONES AND CONTIGS\n' )
    oh_log.close()
    cmdList = []
    for LG in xrange(1,19):
        print LG
        basePath_LG   = join( tmpPath,     'LG_%d'%LG )
        cloneFASTA    = join( basePath_LG, 'LG%d.mergedClones.cloneGroups.fasta'%LG )
        contigFASTA   = join( basePath_LG, 'LG%d.mergedContigs.contigGroups.fasta'%LG )
        combinedFASTA = join( basePath_LG, 'LG%d.combined.fasta'%LG )
        oh = open( combinedFASTA, 'w' )
        for r in iterFASTA(open(cloneFASTA)):
            r.id = '%s_CLONE'%r.id
            writeFASTA( [r], oh )
        #####
        for r in iterFASTA(open(contigFASTA)):
            r.id = '%s_CONTIG'%r.id
            writeFASTA( [r], oh )
        #####
        oh.close()
    #####

#==============================================================
def scaffolding(tmpPath, logFile, clusterNum):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'SCAFFOLDING CONTIGS\n' )
    oh_log.close()
    cmdList = []
    for LG in xrange(1,19): 
        cmdList.append( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/scaffolding.py %d %s'%(LG,tmpPath) )
    #####
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile='scaffolding.pl' )
    deleteFile('scaffolding.pl')

#==============================================================
def badJoinFinder( tmpPath, logFile, clusterNum ):
    oh_log = open( logFile, 'a' )
    oh_log.write( 'LOOKING FOR BAD JOINS\n' )
    oh_log.close()
    cmdList = []
    for LG in xrange(1,19):
        cmdList.append( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/badJoinFinder.py %d %s'%(LG,tmpPath) )
    #####
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile='badJoinFinder.pl' )
    deleteFile('badJoinFinder.pl')

#==============================================================
def panicumHalliiSynteny( halliiFASTAFile, merSize, tmpPath, logFile, clusterNum ):
    
    oh_log = open( logFile, 'a' )
    oh_log.write( 'EVALUATING PANICUM HALLII SYNTENY\n' )
    oh_log.close()

    # Making the temporary directory
    A_B_Path     = join( tmpPath, 'A_B_genomes' )
    A_FASTA_name = join( A_B_Path, 'tmpFASTA_A_genome_LGs.fasta' )
    B_FASTA_name = join( A_B_Path, 'tmpFASTA_B_genome_LGs.fasta' )

    # Linkage group to chromosome
    global LG_to_Chr
    
    # Collapsing the assemblies into one file
    if ( not isdir(A_B_Path) ):
        print '\t-Collapsing assemblies into two files based on which genome they belong to'
        system( 'rm -rf %s ; mkdir %s'%(A_B_Path,A_B_Path) )
        oh = {'a':open( A_FASTA_name, 'w' ), 'b':open( B_FASTA_name, 'w' ) }
        for LG in xrange(1,19):
            trans = '>LG%d_'%LG
            print '\t\tLG%d'%LG
            # Creating the path to the temporary directory
            synDirPath = join( tmpPath, 'LG_%d/LG%d_phalliiSynteny'%(LG,LG) )
            system( 'rm -rf %s ; mkdir %s'%(synDirPath,synDirPath) )
            scaff_oh = open( join(synDirPath,'LG%d.scaffolds.dat'%LG), 'w' )
            # Loading the genome
            genome = LG_to_Chr[LG][-1]
            for line in open( join(tmpPath,'LG_%d/LG%d_scaffolding/LG%d.assembledScaffolds_rebroken.fasta'%(LG,LG,LG)) ):
                if ( line[0] == '>' ):
                    line = ''.join( [line.replace('>',trans).strip(), '\n'] )
                    scaff_oh.write( line[1:] )
                #####
                oh[genome].write( line )
            #####
            scaff_oh.close()
        #####
        oh['a'].close()
        oh['b'].close()
    #####
    
    # Hallii Synteny    
    cmdList = []
    for LG in xrange(1,19):
        cmdList.append( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/phallii_synteny.py %d %s %s %d'%(LG,tmpPath,halliiFASTAFile,merSize) )
    #####
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile='phalliiSynteny.pl' )
    deleteFile('phalliiSynteny.pl')

#==============================================================
def collapseAdjacentHaplotypes( minMatch, maxOverHang, tmpPath, logFile, clusterNum ):

    # Writing to the log file
    oh_log = open( logFile, 'a' )
    oh_log.write( 'COLLAPSING ADJACENT HAPLOTYPES\n' )
    oh_log.close()

    # Realigning the markers
    cmdList = []
    for LG in xrange(1,19):
        synDirPath       = join( tmpPath, 'LG_%d/LG%d_phalliiSynteny'%(LG,LG) )
        finalJoinedFASTA = join( synDirPath, "LG%d_rebroken_halliiSynteny.fasta"%LG )
        fixedFASTA       = join( synDirPath, "LG%d_rebroken_halliiSynteny.adjHapFixed.fasta"%LG )
        # Building the collapsing adjacent haplotypes command
        collapse_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/fixingScaffolds.py %s %s %s %s %d %d"%(finalJoinedFASTA,fixedFASTA,synDirPath,LG,minMatch,maxOverHang)
        cmdList.append( collapse_cmd )
    #####
    remoteServer4( cmdList, len(cmdList), clusterNum, perlFile='collapseAdjacent.pl' )
    deleteFile('collapseAdjacent.pl')

#==============================================================
def finalChromosomeConstruction( tmpPath, logFile, clusterNum ):

    # Writing to the log file
    oh_log = open( logFile, 'a' )
    oh_log.write( 'FINAL CHROMOSOME CONSTRUCTION\n' )
    oh_log.close()

    # Linkage group to chromosome
    global LG_to_Chr
    
    # Realigning the markers
    cmdList = []
    for LG in xrange(1,19):

        # Chromosome Directory
        buildDirPath = join( tmpPath, 'LG_%d/LG%d_finalChromosomes'%(LG,LG) )
        
        # Generate bestHit file
        inputBLAT         = join( buildDirPath, 'LG%d_rebroken_halliiSynteny.adjHapFixed.posFixed.blat'%LG )
        bestHitOutputFile = join( buildDirPath, "LG%d_rebroken_halliiSynteny.adjHapFixed.posFixed.bestHit"%LG)
        bestHit_cmd       = 'convertToBestHit.py -c %s -o %s'%(inputBLAT,bestHitOutputFile)
        
        # Building the splitBrokenMarkerFile command
        assemblyFile = join( buildDirPath, 'LG%d.halliiSynteny.adjHapFixed.markerFixed.fasta'%LG )
        splitMarkercmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/splitBrokenMarkerPlacements.py %d %s %s %s %s"%(LG,LG_to_Chr[LG],bestHitOutputFile,buildDirPath,assemblyFile)
        
        print "---"
        print LG
        print LG_to_Chr[LG]
        print bestHitOutputFile
        print buildDirPath
        print assemblyFile

        # Setting the chromosome build command
        build_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/buildingChromosomes.py %d %s %s %s"%(LG,LG_to_Chr[LG],buildDirPath,assemblyFile)
        cmdList.append( "%s ; %s ; %s"%( bestHit_cmd, splitMarkercmd, build_cmd ) )
        
    #####
    
    # The ssd machines did not seem to have the proper numpy
    # library last time I ran this code, so I am not running this
    # Portion on the SSD cluster, just locally.  Doesn't take that
    # long to run.
    for cmd in cmdList: system( cmd )

    assert False

#     remoteServer4( cmdList, 18, clusterNum, perlFile='finalConstruction.pl' )
#     deleteFile('finalConstruction.pl')
    
    # Constructing the chromosomes individually
    for LG in xrange( 1, 19 ):

        # Setting up the run
        buildDirPath = join( tmpPath, 'LG_%d/LG%d_finalChromosomes'%(LG,LG) )
        CHR          = LG_to_Chr[LG]
        markerSet    = '%s_rebroken_halliiSynteny'%CHR
        pre          = join( buildDirPath, markerSet )

        fastaFile    = join( tmpPath, "LG_%d/LG%d_phalliiSynteny/LG%d_rebroken_halliiSynteny.fasta"%(LG,LG,LG) )

        # Pulling in the sequence
        indexFASTA = FASTAFile_dict( fastaFile )
        orderFile  = '%s_Group_%d_locallyOrdered.dat'%(pre,LG)
        scaffSet   = set([item for item in indexFASTA.iterkeys()] )
        outputSeq  = []
        x = iterCounter(1000)
        for line in open(orderFile):
            parsedLine = line.split(None)
            if ( parsedLine[0][0] == "-" ): continue
            if ( parsedLine[0] == "Pearsons" ): break
            scaffID = parsedLine[0]
            direc   = parsedLine[1]
            tmpSeq  = str(indexFASTA[scaffID].seq)
            if ( direc == "rev" ): tmpSeq = revComp( tmpSeq )
            outputSeq.append( tmpSeq )
            scaffSet.remove( scaffID )
            x()
        #####
        
        # Writing out the sequence
        oh = open( join( buildDirPath, "%s.final.joined.chromosome.fasta"%CHR ), "w" )
        sepSeq = 10000*"N"
        rec = SeqRecord( id = CHR, seq=sepSeq.join(outputSeq), description='' )
        writeFASTA( [rec], oh )
        
        # Writing the leftovers to the end of the scaffold
        for scaffID in scaffSet: writeFASTA( [indexFASTA[scaffID]], oh )
        oh.close()
        
    #####

#==============================================================
def finalizeRelease( tmpPath, logFile, segPath, clusterNum, cloneFASTAFile, markerFASTA ):
    
    # Reversing the dictionary    
    global LG_to_Chr
    Chr_to_LG = dict( [(v,k) for k,v in LG_to_Chr.iteritems()] )
    Chr_list = []
    for n in xrange(1,10):
        Chr_list.append( "Chr%sa"%( str(n).zfill(2) ) )
        Chr_list.append( "Chr%sb"%( str(n).zfill(2) ) )
    #####
    
    # Step 1: Set up the release directory
    print "setting up the release directory"
    releasePath = join( tmpPath, 'releaseDirectory' )
    if ( not isdir(releasePath) ): system( "mkdir %s"%releasePath )
    
    # Step 2:  Find all of the bottom drawer clone names
    print "Find all of the bottom drawer clone names"
    full_clone_set = set( [r.id.split('_')[0] for r in iterFASTA(open(cloneFASTAFile))] )
    clone_het_set  = set([line.strip() for line in open(join(tmpPath,'heterozygousClones.dat'))])
    clone_hit_set  = set( [line.split(None)[13].split('_')[0] for line in open(join(tmpPath,'blatPerfectHits_newMarkerSet_on_Clones.blat'))] )
    BD_clone_set   = full_clone_set.difference( clone_het_set.union(clone_hit_set) )
    
    # Step 3:  Writing the final chromosome, BD_contig, and BD_clone FASTA files
    BD_File = join(segPath,"bottomDrawerContigs.fasta")
    
    BD_contig_file = join( releasePath, "bottomDrawer_contig_unscreened.fasta" )
#    BD_contig_oh   = open( BD_contig_file, 'w' )
    
    BD_clone_file = join( releasePath, "bottomDrawer_clone_unscreened.fasta" )
#    BD_clone_oh   = open( BD_clone_file, 'w' )
    
    release_file = join(releasePath,"finalRelease.fasta")
#    release_oh   = open( release_file, 'w' )
    
#     for CHR in Chr_list:
#         LG = Chr_to_LG[CHR]
#         buildDirPath = join( tmpPath, 'LG_%d/LG%d_finalChromosomes'%(LG,LG) )
#         fixedFASTA   = join( buildDirPath, "%s.final.joined.fixed.chromosome.fasta"%CHR )
#         for record in iterFASTA( open(fixedFASTA) ):
#             if ( record.id == CHR ):
#                 writeFASTA( [record], release_oh )
#             else:
#                 writeFASTA( [record], BD_contig_oh )
#             #####
#         #####
#     #####
#     release_oh.close()
    
#     # Writing the unmarked contigs to the bottom drawer file
#     print "Writing the unmarked contigs to the bottom drawer file"
#     for record in iterFASTA(open(BD_File)):
#         writeFASTA( [record], BD_contig_oh )
#     #####
#     BD_contig_oh.close()
    
#     # Writing the unmarked clones to the bottom drawer file
#     print "Writing the unmarked clones to the bottom drawer file"
#     for r in iterFASTA(open(cloneFASTAFile)):
#         if ( r.id.split('_')[0] in BD_clone_set ):  writeFASTA( [r], BD_clone_oh )
#     #####
#     BD_clone_oh.close()
    
    # Step 4:  Make plots of the map 
    print "Making plots of the map"
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/makeMapPlots.py %s %s %s %s"%( markerFASTA, release_file, logFile, releasePath )
#    system( alignment_cmd )
    
    # Step 5:  Making PNG dot plots
    print "Making dot plots"
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/makeFinalPlots.py %s"%( releasePath )
#    system( alignment_cmd )

    # Step 5.5:  Find all overlapping clones on clones in the bottom drawer
    print 'Find all overlapping contigs on clones in the bottom drawer'
    contigs_on_clones_file = join( releasePath, "BD_contigs_on_clones_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_contig_file,BD_clone_file,25,clusterNum,contigs_on_clones_file)
#    system( alignment_cmd )
    
    # Step 6:  Find all overlapping contigs on clones in the bottom drawer
    print 'Find all overlapping contigs on clones in the bottom drawer'
    contigs_on_clones_file = join( releasePath, "BD_contigs_on_clones_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_contig_file,BD_clone_file,25,clusterNum,contigs_on_clones_file)
#    system( alignment_cmd )
    
    # Step 7:  Find all overlapping contigs on release in the bottom drawer
    print 'Find all overlapping contigs on release in the bottom drawer'
    contigs_on_release_file = join( releasePath, "BD_contigs_on_release_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_contig_file,release_file,25,clusterNum,contigs_on_release_file)
#    system( alignment_cmd )
    
    # Step 8:  Find all overlapping clones on release in the bottom drawer
    print 'Find all overlapping clones on release in the bottom drawer'
    clones_on_release_file = join( releasePath, "BD_clones_on_release_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_clone_file,release_file,25,clusterNum,clones_on_release_file)
#    system( alignment_cmd )
    
    # Step 9:  Making the final bottom drawer set
    print "Making Bottom Drawer Files"
    contig_on_clones_exclude_set  = set( [line.split(None)[1] for line in open(contigs_on_clones_file)  if ( line.split(None)[0] == "REDUNDANT:" )] )
    contig_on_release_exclude_set = set( [line.split(None)[1] for line in open(contigs_on_release_file) if ( line.split(None)[0] == "REDUNDANT:" )] )
    clones_on_release_exclude_set = set( [line.split(None)[1] for line in open(clones_on_release_file)  if ( line.split(None)[0] == "REDUNDANT:" )] )
    BD_exclude_set                = contig_on_clones_exclude_set.union( contig_on_release_exclude_set.union(clones_on_release_exclude_set) )
    new_BD_file                   = join(releasePath,"final_bottomDrawerContigs_new.fasta")
#    new_oh                        = open( new_BD_file, "w" )
#     usedSet                       = set()
#     for record in iterFASTA(open(BD_contig_file)):
#         if ( (record.id in BD_exclude_set) or (record.id in usedSet) ): continue
#         usedSet.add( record.id )
#         writeFASTA( [record], new_oh )
#     #####
#     for record in iterFASTA(open(BD_clone_file)):
#         if ( (record.id in BD_exclude_set) or (record.id in usedSet) ): continue
#         usedSet.add( record.id )
#         writeFASTA( [record], new_oh )
#     #####
#     new_oh.close()
    
    # Step 10:  Collapse the bottom drawer contigs/clones
    print "collapsing the bottom drawer contigs/clones"
    cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/collapseBottomDrawer.py %s %s"%( new_BD_file, releasePath )
    system( cmd )

    assert False
    
#     # Step 8:  Locating all integrated clones
#     cloneLocPath = join( releasePath, 'locatingClones' )
#     if ( not isdir(cloneLocPath) ): system( "mkdir %s"%cloneLocPath )
#     cmdList = []
#     for LG in xrange(1,19):
#         cmdList.append( 'python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findAllClones.py %d %s %s %s %s'%( LG, tmpPath, releasePath, release_file, cloneLocPath ) )
#     #####
#     system( cmdList[0] )
#     assert False
#     remoteServer4( cmdList, 18, clusterNum, perlFile='cloneFinder.pl' )
#     deleteFile('cloneFinder.pl')
    
    return
    

#==============================================================
def generalizedCollapser():

    # Step 5.5:  Find all overlapping clones on clones in the bottom drawer
    print 'Find all overlapping contigs on clones in the bottom drawer'
    contigs_on_clones_file = join( releasePath, "BD_contigs_on_clones_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_contig_file,BD_clone_file,25,clusterNum,contigs_on_clones_file)
#    system( alignment_cmd )
    
    # Step 6:  Find all overlapping contigs on clones in the bottom drawer
    print 'Find all overlapping contigs on clones in the bottom drawer'
    contigs_on_clones_file = join( releasePath, "BD_contigs_on_clones_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_contig_file,BD_clone_file,25,clusterNum,contigs_on_clones_file)
#    system( alignment_cmd )
    
    # Step 7:  Find all overlapping contigs on release in the bottom drawer
    print 'Find all overlapping contigs on release in the bottom drawer'
    contigs_on_release_file = join( releasePath, "BD_contigs_on_release_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_contig_file,release_file,25,clusterNum,contigs_on_release_file)
#    system( alignment_cmd )
    
    # Step 8:  Find all overlapping clones on release in the bottom drawer
    print 'Find all overlapping clones on release in the bottom drawer'
    clones_on_release_file = join( releasePath, "BD_clones_on_release_new.dat" )
    alignment_cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/findRedundantBottomDrawer.py %s %s %d %s %s"%(BD_clone_file,release_file,25,clusterNum,clones_on_release_file)
#    system( alignment_cmd )
    
    # Step 9:  Making the final bottom drawer set
    print "Making Bottom Drawer Files"
    contig_on_clones_exclude_set  = set( [line.split(None)[1] for line in open(contigs_on_clones_file)  if ( line.split(None)[0] == "REDUNDANT:" )] )
    contig_on_release_exclude_set = set( [line.split(None)[1] for line in open(contigs_on_release_file) if ( line.split(None)[0] == "REDUNDANT:" )] )
    clones_on_release_exclude_set = set( [line.split(None)[1] for line in open(clones_on_release_file)  if ( line.split(None)[0] == "REDUNDANT:" )] )
    BD_exclude_set                = contig_on_clones_exclude_set.union( contig_on_release_exclude_set.union(clones_on_release_exclude_set) )
    new_BD_file                   = join(releasePath,"final_bottomDrawerContigs_new.fasta")
#    new_oh                        = open( new_BD_file, "w" )
#     usedSet                       = set()
#     for record in iterFASTA(open(BD_contig_file)):
#         if ( (record.id in BD_exclude_set) or (record.id in usedSet) ): continue
#         usedSet.add( record.id )
#         writeFASTA( [record], new_oh )
#     #####
#     for record in iterFASTA(open(BD_clone_file)):
#         if ( (record.id in BD_exclude_set) or (record.id in usedSet) ): continue
#         usedSet.add( record.id )
#         writeFASTA( [record], new_oh )
#     #####
#     new_oh.close()
    
    # Step 10:  Collapse the bottom drawer contigs/clones
    print "collapsing the bottom drawer contigs/clones"
    cmd = "python /mnt/raid2/SEQ/sharedPythonLibrary/assemblingSwitchgrass/collapseBottomDrawer.py %s %s"%( new_BD_file, releasePath )
    system( cmd )

    assert False


#==============================================================
def real_main():
    
    # Defining the program options
    usage = 'usage: %prog [clone FASTA] [Phallii assembly] [log file]'

    parser = OptionParser(usage)

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( 'Incorrect number of arguments.  ' + \
                      'View usage using --help option.' )
    else:
        # SW clones FASTA file
        cloneFASTAFile = realpath( args[0] )
        testFile( cloneFASTAFile )

        # Hallii assembly FASTA file
        halliiFASTAFile = realpath( args[1] )
        testFile( halliiFASTAFile )

        # Log file
        logFile = realpath( args[2] )
        
    #####
    
    # Writing to the log file
    oh_log = open( logFile, 'a' )
    oh_log.write( 'FINAL CHROMOSOME CONSTRUCTION\n' )
    oh_log.close()

    # Linkage group to chromosome
    global LG_to_Chr
    LG_to_Chr = { 1:'Chr01a', \
                  9:'Chr01b', \
                  3:'Chr02a', \
                  4:'Chr02b', \
                 18:'Chr03a', \
                 16:'Chr03b', \
                 12:'Chr04a', \
                 15:'Chr04b', \
                  8:'Chr05a', \
                  6:'Chr05b', \
                 14:'Chr06a', \
                  7:'Chr06b', \
                 10:'Chr07a', \
                 17:'Chr07b', \
                 11:'Chr08a', \
                 13:'Chr08b', \
                  2:'Chr09a', \
                  5:'Chr09b' }

    #------------------------------------------------
    # Hard coding certain variables
    tmpDir      = 'build.V2.assembly'
    segPath     = '/home/t4c1/WORK/JWJ_ANALYSIS/Switchgrass/assemblingSwitchgrass/building_final_V2/segregatedContigs'
    markerFASTA = '/home/t4c1/WORK/JWJ_ANALYSIS/Switchgrass/assemblingSwitchgrass/building_final_V2/new_translated_marker_set.fasta'
    
    #------------------------------------------------
    # (1) Initial Setup and opening the log file
    print "STAGE 1:  INITIAL SETUP"
    tmpPath                  = initialSetup(logFile,False,tmpDir,segPath)
    cloneMarkerAlignmentFile = join( tmpPath, 'blatPerfectHits_newMarkerSet_on_Clones.blat' )
    
    #------------------------------------------------
    # (2) Align the markers to the clones
    print "STAGE 2:  ALIGNING MARKERS TO CLONES"
    nJobs                    = 25
    clusterNum               = 'jer'
    if ( False ): alignMarkersToClones( markerFASTA, cloneFASTAFile, nJobs, clusterNum, cloneMarkerAlignmentFile, logFile )

    #------------------------------------------------
    # (3) Screening heterozygous clones and segregate onto linkage groups
    print "STAGE 3:  SCREENING HETEROZYGOUS AND SEGREGATING CLONES TO LINKAGE GROUPS"
    if ( False ): screenHeterozygousClones(tmpPath, cloneMarkerAlignmentFile, cloneFASTAFile, logFile, segPath)

    #------------------------------------------------
    #------------------------------------------------
    # HERE IS WHERE I STARTED THE FINAL ASSEMBLY PROCESS
    #------------------------------------------------
    #------------------------------------------------
    # (3.5) Restarting the whole process
    print "STAGE 3.5:  ALIGNING MARKERS TO THE NEWLY COLLAPSED CONTIGS AND CLONES"
    if ( False ): realignMarkers( tmpPath, logFile, clusterNum, 'NEW_CONTIGS', segPath )
    if ( False ): realignMarkers( tmpPath, logFile, clusterNum, 'NEW_CLONES', segPath )

    #------------------------------------------------
    # (4) Collapse redundant contigs on clones.  Only collapsing contigs that fully align with the clones
    print "STAGE 4:  FINDING CONTIGS REDUNDANT TO CLONES"
    merCountCutoff = 75
    kmerSize       = 50
    ID_cutoff      = 95
    COV_cutoff     = 95
    if ( False ): collapseContigsOnClones(tmpPath, segPath, logFile, clusterNum, merCountCutoff, kmerSize, ID_cutoff, COV_cutoff) 
    
    #------------------------------------------------
    # (5) Splitting contigs and clones up before collapsing
    print "STAGE 5:  SPLITTING CONTIGS AND CLONES BEFORE COLLAPSING"
    if ( False ): splittingContigsAndClones( tmpPath, logFile )
    
    #------------------------------------------------
    # (6) Realign markers to syntenic blocks
    print "STAGE 6:  SEGREGATING MARKERS ONTO LINKAGE GROUPS"
    if ( False ): segregateMarkers( tmpPath, logFile, markerFASTA )
    
    #===================================================================================
    #===================================================================================

    #------------------------------------------------
    # (7) Performing a generalized collapsing with clones
    print "STAGE 7:  COLLAPSING CLONES ON CLONES"
    merCountCutoff = 2000
    minAlignLength = 2000
    minIdentity    = 70
    overHang       = 5000
    maxGapSize     = 10000
    if ( False ): markerFreeCollapsing( tmpPath, merCountCutoff, logFile, clusterNum, kmerSize, minAlignLength, minIdentity, 'CLONES', overHang, maxGapSize )

    #------------------------------------------------
    # (8) Realign markers to newly collapsed clones
    print "STAGE 8:  ALIGNING MARKERS TO THE NEWLY COLLAPSED CLONES"
    if ( False ): realignMarkers( tmpPath, logFile, clusterNum, 'CLONES', segPath )
    # COMMAND ABOVE IS RUNNING!!

    #------------------------------------------------
    # (9) Identifying marker driven haploytype groups
    print "STAGE 9:  IDENTIFYING HAPLOTYPE GROUPS CLONES"
    minMarkers = 10
    if ( False ): identifyHaplotypeGroups( tmpPath, logFile, clusterNum, 'CLONES', minMarkers )

    #------------------------------------------------
    # (10) Merging the haplotype groups on markers
    print "STAGE 10:  MERGING THE HAPLOTYPE GROUPS CLONES"
    min_ID         = 70
    minAlignLength = 10000
    overHang       = 5000
    maxGapSize     = 50000
    if ( False ): collapseOnMarkers( tmpPath, logFile, clusterNum, 'CLONES', min_ID, minAlignLength, overHang, maxGapSize )
    
    #===================================================================================
    #===================================================================================

    #------------------------------------------------
    # (11) Performing a generalized collapsing
    print "STAGE 11:  COLLAPSING CONTIGS ON CONTIGS"
    merCountCutoff = 150
    minAlignLength = 400
    minIdentity    = 70
    overHang       = 150
    maxGapSize     = 10000
    if ( False ): markerFreeCollapsing( tmpPath, merCountCutoff, logFile, clusterNum, kmerSize, minAlignLength, minIdentity, 'CONTIGS', overHang, maxGapSize )

    #------------------------------------------------
    # (12) Realign markers to syntenic blocks
    print "STAGE 12:  ALIGNING MARKERS TO THE NEWLY COLLAPSED CONTIGS"
    if ( False ): realignMarkers( tmpPath, logFile, clusterNum, 'CONTIGS', segPath )
    
    #------------------------------------------------
    # (13) Performing a generalized collapsing
    print "STAGE 13:  IDENTIFYING HAPLOTYPE GROUPS CONTIGS"
    minMarkers = 5
    if ( False ): identifyHaplotypeGroups( tmpPath, logFile, clusterNum, 'CONTIGS', minMarkers )
    
    #------------------------------------------------
    # (14) Performing a generalized collapsing
    print "STAGE 14:  MERGING THE HAPLOTYPE GROUPS CONTIGS"
    min_ID         = 70
    minAlignLength = 400
    overHang       = 150
    maxGapSize     = 10000
    if ( False ): collapseOnMarkers( tmpPath, logFile, clusterNum, 'CONTIGS', min_ID, minAlignLength, overHang, maxGapSize )
    
    #===================================================================================
    #===================================================================================

    #------------------------------------------------
    # (15) Performing a generalized collapsing
    print "STAGE 15:  MERGING THE CLONES AND CONTIGS FILE"
    if ( False ): combine_clones_contigs( tmpPath, logFile )

    #------------------------------------------------
    # (16) Performing a generalized collapsing
    print "STAGE 16:  COLLAPSING COMBINED ON COMBINED"
    merCountCutoff = 1000
    minAlignLength = 1000
    minIdentity    = 70
    maxGapSize     = 200000
    if ( False ): markerFreeCollapsing( tmpPath, merCountCutoff, logFile, clusterNum, kmerSize, minAlignLength, minIdentity, 'COMBINED', overHang, maxGapSize )
    # COMMAND ABOVE IS RUNNING!!
    
    #------------------------------------------------
    # (17) Realign markers to syntenic blocks
    print "STAGE 17:  ALIGNING MARKERS TO THE NEWLY COLLAPSED COMBINATION OF CLONES AND CONTIGS"
    if ( False ): realignMarkers( tmpPath, logFile, clusterNum, 'COMBINED', segPath )

    #------------------------------------------------
    # (18) Performing a generalized collapsing
    print "STAGE 18:  IDENTIFYING HAPLOTYPE GROUPS FOR THE NEWLY COMBINED CLONES AND CONTIGS"
    minMarkers = 10
    if ( False ): identifyHaplotypeGroups( tmpPath, logFile, clusterNum, 'COMBINED', minMarkers )

    #------------------------------------------------
    # (19) Performing a generalized collapsing
    print "STAGE 19:  MERGING THE HAPLOTYPE GROUPS CLONES AND CONTIGS"
    min_ID         = 70
    minAlignLength = 1000
    overHang       = 500
    maxGapSize     = 400000
    if ( False ): collapseOnMarkers( tmpPath, logFile, clusterNum, 'COMBINED', min_ID, minAlignLength, overHang, maxGapSize )

    #===================================================================================
    #===================================================================================
    
    #------------------------------------------------
    # (20) Scaffold the remaining sets
    print "STAGE 20:  SCAFFOLDING THE CONTIGS"
    if ( False ): scaffolding( tmpPath, logFile, clusterNum )
    
    #------------------------------------------------
    # (21) Looking for bad joins
    print "STAGE 21:  FINDING BAD JOINS"
    if ( False ): badJoinFinder( tmpPath, logFile, clusterNum )

    #------------------------------------------------
    # (22) Hallii Synteny
    print "STAGE 22:  EVALUATING PANICUM HALLII SYNTENY"
    merSize = 31
    if ( False ): panicumHalliiSynteny(halliiFASTAFile, merSize, tmpPath, logFile, clusterNum )

    #------------------------------------------------
    # (23) Collapse adjacent contig haplotypes
    print "STAGE 23:  COLLAPSING ADJACENT CONTIG HAPLOTYPES"
    minMatch    = 100
    maxOverHang = 1000
    if ( False ): collapseAdjacentHaplotypes( minMatch, maxOverHang, tmpPath, logFile, clusterNum )
    
    #------------------------------------------------
    # (24) Realign markers to syntenic blocks
    print "STAGE 24:  REALIGNING MARKERS TO COLLAPSED SCAFFOLDS"
    if ( False ): realignMarkers( tmpPath, logFile, clusterNum, 'COLLAPSED', segPath )
    
    #------------------------------------------------
    # (25) Build Chromosomes
    print "STAGE 25:  BUILDING THE CHROMOSOMES"
    if ( True ): finalChromosomeConstruction( tmpPath, logFile, clusterNum )

    assert False

    #------------------------------------------------
    # (26) Creating the Bottom Drawer and finalizing the release
    print "STAGE 26:  CREATING THE BOTTOM DRAWER AND FINALIZING THE RELEASE"
    if ( False ): finalizeRelease( tmpPath, logFile, segPath, clusterNum, cloneFASTAFile, markerFASTA )









    #------------------------------------------------
    # (20) Performing a generalized collapsing
    print "STAGE 20:  COLLAPSING BOTTOM DRAWER WITH ITSELF"
    merCountCutoff = 1000
    minAlignLength = 400
    minIdentity    = 70
    if ( False ): markerFreeCollapsing( tmpPath, merCountCutoff, logFile, clusterNum, kmerSize, minAlignLength, minIdentity, 'BOTTOMDRAWER' )
    #################################
    # THE ABOVE HAS ALREADY BEEN DONE
    #################################


    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
