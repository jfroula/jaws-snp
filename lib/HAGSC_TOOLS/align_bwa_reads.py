#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import generateTmpDirName, deleteDir, testDirectory
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, writeFASTA
from hagsc_lib import iterCounter

from os.path import realpath, abspath, isfile, join
from os import curdir, mkdir, system, chdir

from sys import stderr, stdout, stdin

from optparse import OptionParser

import subprocess

#==============================================================
def printAndExecute( cmd, execute=True ):
    stdout.write( "%s\n"%cmd )
    if ( execute ): system( cmd )
    return 

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA/FASTQ] [ASSEMBLY] [options]"

    parser = OptionParser(usage)

    nAlignsOut = 3
    parser.add_option( "-n", \
                       "--nAlignsOut", \
                       type    = 'float', \
                       help    = "Number of alignments to write out for each read:  Default: %d"%nAlignsOut, \
                       default = nAlignsOut )

#     numberToSkip = 
#     parser.add_option( "-s", \
#                        "--numberToSkip", \
#                        type    = 'float', \
#                        help    = "Number of reads to skip:  Default: %d"%numberToSkip, \
#                        default = numberToSkip )
# 
#     parser.add_option( "-c", \
#                        "--revCompReads", \
#                        action  = 'store_true', \
#                        dest    = 'revCompReads', \
#                        help    = "Reverse complement the pairs:  Default=False" )
#     parser.set_defaults( revCompReads = False )


    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTA = realpath(args[0])
        isSTDIN = (FASTA == 'stdin')
        if ( (not isSTDIN) and (not isfile(FASTA)) ): parser.error( '%s can not be found'%FASTA )
        
        ASSEMBLY = realpath(args[1])
        if ( not isfile(ASSEMBLY) ): parser.error( '%s can not be found'%ASSEMBLY )

    #####
    
    # Create temporary directory
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpPath  = join( basePath, generateTmpDirName() )
    deleteDir(tmpPath)
    mkdir(tmpPath, 0777)
    testDirectory(tmpPath)
    
    # Changing the directory
    chdir( tmpPath )
    
    # Pulling out a certain number of pairs
    if ( isSTDIN ):
        FASTA = join( tmpPath, 'reads.fasta' )
        oh = open( FASTA, 'w' )
        for line in stdin: oh.write( line )
        oh.close()
    #####
    
    # Indexing the genome
    cmd = '/mnt/local/EXBIN/bwa index -p INDEX -a bwtsw %s'%ASSEMBLY
    printAndExecute( cmd, execute=True )
    
    # Performing the alignments
    cmd = '/mnt/local/EXBIN/bwa aln -t 6 INDEX %s > tmp.sai'%FASTA
    printAndExecute( cmd, execute=True )
    
    # Creating the sam file
    p = subprocess.Popen( '/mnt/local/EXBIN/bwa samse -n %d INDEX tmp.sai %s'%(nAlignsOut,FASTA), shell=True, stdout=subprocess.PIPE )
    for line in p.stdout: stdout.write(line)
    p.poll()
    
    # Changing directory
    chdir( basePath )
    
    # Removing the temporary directory
    deleteDir(tmpPath)
    
    return
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
