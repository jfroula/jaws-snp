#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  9/16/13"

from hagsc_lib import trimAssemblyFile

import re

from os.path import realpath, isfile

from optparse import OptionParser

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [trimLength] [outputFileName] [options]"

    parser = OptionParser(usage)

    ignoreFile = None
    parser.add_option( "-n", \
                       "--ignoreFile", \
                       type    = 'str', \
                       help    = "File of scaffolds/contigs to ignore.  Default: Include all", \
                       default = ignoreFile )

    lowQLength = 0
    parser.add_option( '-L', \
                       "--lowQLength", \
                       type    = "int", \
                       help    = "Number of low quality bases to discard on the ends of contigs.  Default: 0", \
                       default = lowQLength )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        fastaFile = realpath(args[0])
        if ( not isfile(fastaFile) ): parser.error( '%s can not be found'%fastaFile )

        trim = int(args[1])

        outFileName = realpath(args[2])
    #####
    
    # Reading in the ignore set
    iSet = None
    if ( ignoreFile != None ):
        if ( not isfile(ignoreFile) ): parser.error( '%s can not be found'%ignoreFile )
        iSet = set([line.strip() for line in open(ignoreFile)])
    #####
    
    # Performing the trimming
    trimAssemblyFile(fastaFile, trimLength=trim, taLength=lowQLength, ignoreSet=iSet, outputFileName=outFileName)

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
