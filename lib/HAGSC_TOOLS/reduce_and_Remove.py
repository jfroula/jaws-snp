#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott"
__date__ ="$May 29, 2013$"

# Python Library Imports
from hagsc_lib import isGzipFile, isBzipFile, deleteDir
from hagsc_lib import iterFASTA, iterQUAL, generateTmpDirName
from hagsc_lib import testDirectory, iterCounter, deleteFile

from optparse import OptionParser

import subprocess

from sys import stderr, stdout

from os.path import isfile, abspath, join

from os import system, curdir, mkdir

#==============================================================

def printAndExec( cmd, execute=True ):
    print cmd
    if (execute): system( cmd )
    return

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTAFILE] [Coverage] [base output name] [options]"

    parser = OptionParser(usage)

                       
    parser.add_option( "-t", \
                       "--repeat", \
                       type    = 'int', \
                       help    = "Number of repetitions for a n-mer to be repetitive.  Default: 1000", \
                       default = 1000 )
                       
    parser.add_option( "-m", \
                       "--kmerSize", \
                       type    = 'int', \
                       help    = "The size of the kmer to be used in the masking.  Default: 24", \
                       default = 24 )
                       
    parser.add_option( "-n", \
                       "--numberOfReads", \
                       type    = 'int', \
                       help    = "Number of reads to use. Default: All Reads", \
                       default = False )
                       
    parser.add_option( "-l", \
                       "--lowerGC", \
                       type    = 'int', \
                       help    = "Lower saturation limit for GC histogram.  Default: 1.5 standard deviations from the mean", \
                       default = False )
                       
    parser.add_option( "-u", \
                       "--upperGC", \
                       type    = 'int', \
                       help    = "Upper saturation limit for GC histogram.  Default: 1.5 standard deviations from the mean", \
                       default = False )
                       
    parser.add_option( "-c", \
                       "--noComp", \
                       action  = 'store_true',\
                       help    = "Do not compliment the final read set. Default = False", \
                       default = False )
    
    parser.add_option( "-q", \
                       "--fastq", \
                       action  = 'store_true',\
                       help    = "The input file is a fastq. Default = False", \
                       default = False )
                       
                       
    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )

    else:
        # Do the files exist
        if isfile(args[0]): 
            fastaFile = args[0]
        else:
            parser.error( 'Fasta File:  %s is not found.'% args[0])
        #####
        
        coverage = args[1]
        baseName = args[2]
    #####
    if ( options.numberOfReads ):
        nReads = options.numberOfReads * 2
        if(options.fastq): 
            nReads = nReads * 2
        #####
    #####
    if ( options.repeat > 0 ):
        repeat = options.repeat
    else:
        parser.error( 'Repeat must be > 0' )
    #####
    if ( options.kmerSize > 0 ):
        kmerSize = options.kmerSize
    else:
        parser.error( 'kmer Size must be > 0' )
    #####

    if (options.lowerGC):
        if ( options.lowerGC > 0 ):
            lowerGC = options.lowerGC
        else:
            parser.error( 'Lower GC must be > 0' )
        #####
        if ( options.upperGC > lowerGC  ):
            repeat = options.upperGC
            saturation = True
        else:
            parser.error( 'Upper GC must be > Lower GC ' )
        #####
    else:
        saturation = False
    #####
    
    #=======================================================
    # STEP 1: Create a tmp space and names
    basePath   = abspath(curdir)
    tmpDir     = generateTmpDirName()
    tmpPath    = join( basePath, tmpDir )
    stderr.write( '\n\t-Creating tmp directory %s\n'%tmpPath)
    deleteDir(tmpPath)
    mkdir( tmpPath, 0777 )
    testDirectory(tmpPath)
    fasta20x    = '%s/%s.%s.fasta'%(tmpPath, baseName, coverage)
    noRepNames  = '%s/%s.%s.noRepeats.dat'%(tmpPath, baseName, coverage)
    noRepHist   = '%s/%s.%s.noRepeats.comp.histHash'%(basePath, baseName, coverage)
    noRepPNG    = '%s/%s.%s.noRepeats.comp.png'%(basePath, baseName, coverage)
    #=======================================================
    # STEP 2: Pull the reads
    # Building command
    if ( options.numberOfReads ):
        # Setting up the fasta command
        if ( isGzipFile(fastaFile) ):
            fastacmd = 'cat %s | gunzip -c | head -%d > %s '%( fastaFile , nReads, fasta20x)
        elif ( isBzipFile(fastaFile) ):
            fastacmd = 'cat %s | bunzip2 -c| head -%d > %s '%( fastaFile , nReads, fasta20x)
        else:
            fastacmd = 'cat %s| head -%d > %s '%( fastaFile , nReads, fasta20x)
        #####
        if(options.fastq):
            stderr.write( '\n\t-Pulling Reads\n' )
            system(fastacmd)
            system("cat %s | awk '{if(NR%%4==1){print substr($1,2)}}' > %s/includeList.dat"%(fasta20x, tmpPath))
            noRepCMD = "/mnt/local/EXBIN/mask_repeats_hash -B 1000000 -m %d -t %d -z 8g -s '' -F %s |  tr \"%%\" \" \" | awk '{ count += 1; if(count %% 1000000 == 0){print count/1000000\",000,000 reads processed\" > \"/dev/stderr\"}; if ($2 >= 85) print $1 }' | cut -f1 -d\"-\" | sort | uniq -c | grep \" 2 \" | awk '{print $2\"-R1\\n\" $2\"-R2\"}' > %s"%(kmerSize, repeat, fasta20x, noRepNames)
        else:
            stderr.write( '\n\t-Pulling Reads\n' )
            system(fastacmd)
            system('cat %s | grep ">" | cut -b2- > %s/includeList.dat'%(fasta20x, tmpPath))
            # Pulling the reads
            noRepCMD = "/mnt/local/EXBIN/mask_repeats_hash -B 1000000 -m %d -t %d -z 8g -s '' -F %s |  tr \"%%\" \" \" | awk '{ count += 1; if(count %% 1000000 == 0){print count/1000000\",000,000 reads processed\" > \"/dev/stderr\"}; if ($2 >= 85) print $1 }' | cut -f1 -d\"-\" | sort | uniq -c | grep \" 2 \" | awk '{print $2\"-R1\\n\" $2\"-R2\"}' > %s"%(kmerSize, repeat, fasta20x, noRepNames)
        #####
    else:
        noRepCMD = "/mnt/local/EXBIN/mask_repeats_hash -B 1000000 -m %d -t %d -z 8g -s '' -F %s |  tr \"%%\" \" \" | awk '{ count += 1; if(count %% 1000000 == 0){print count/1000000\",000,000 reads processed\" > \"/dev/stderr\"}; if ($2 >= 85) print $1 }' | cut -f1 -d\"-\" | sort | uniq -c | grep \" 2 \" | awk '{print $2\"-R1\\n\" $2\"-R2\"}' > %s"%(kmerSize, repeat, fastaFile, noRepNames)
    #####
    printAndExec( noRepCMD , execute = True)

    #=======================================================
    # STEP 3: Remove the over represented mers
    noRepFasta = '%s/%s.%s.noRepeats.comp.fastq.bz2'%(basePath, baseName, coverage)
    extractCMD  = '/mnt/local/EXBIN/extract_seq_and_qual -c -b'
    if (options.noComp):
        noRepFasta  = '%s/%s.%s.noRepeats.uncomp.fastq.bz2'%(basePath, baseName, coverage)
        noRepHist   = '%s/%s.%s.noRepeats.uncomp.histHash'%(basePath, baseName, coverage)
        noRepPNG    = '%s/%s.%s.noRepeats.uncomp.png'%(basePath, baseName, coverage)
        extractCMD  = '/mnt/local/EXBIN/extract_seq_and_qual -b'
    #####
    if (options.numberOfReads):
        noRepCMD   = "%s -i %s/includeList.dat -x %s -o %s %s"%(extractCMD,tmpPath, noRepNames, noRepFasta, fastaFile)
    else:
        noRepCMD = "%s -x %s -o %s %s"%(extractCMD,noRepNames, noRepFasta, fastaFile)
    printAndExec( noRepCMD , execute = True)
    
    #=======================================================
    # STEP 4: Build the histHash, kmerTree, and gc.png files
    histCMD = '/mnt/local/EXBIN/histogram_hash -g -B 500000 -m 24 -z 8g -o %s %s'%( noRepHist, noRepFasta)
    if (saturation):
        pngCMD  = 'merplot_gc.py %s %s -l %d -u %d'%( noRepHist, noRepPNG, lowerGC, upperGC )
    else:
        pngCMD  = 'merplot_gc.py %s %s'%( noRepHist, noRepPNG )
    #####
    printAndExec( histCMD , execute = True)
    printAndExec( pngCMD , execute = True)
    
    #=======================================================
    # STEP 5: Clean up
    stderr.write( '\t-Removing Temporary directory\n' )
    deleteDir( tmpPath )

#==============================================================    
if ( __name__ == '__main__' ):
    real_main()
