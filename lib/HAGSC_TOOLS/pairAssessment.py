#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Aug 23, 2010 11:56:10 PM$"

# Local Library Imports
from hagsc_lib import pairAssessment_call
from hagsc_lib import defaultCluster
from hagsc_lib import testFile

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

def real_main():

    # Defining the program options
    usage = "usage: %prog [PAIRS FILE] [ASSEMBLY] [options]"

    parser = OptionParser(usage)

    default_z = '2g'
    parser.add_option( "-z", \
                       "--num24Mers", \
                       type    = 'str', \
                       help    = "Size of sparse hash for mask_repeats_hash. " + \
                                 "Default %s."%default_z, \
                       default = default_z )
    
    defaultInsertSize = 50
    parser.add_option( "-b", \
                       "--maxInsertSize", \
                       type    = 'int', \
                       help    = "Maximum Insert Size Allowed in (IN KB!!!): " + \
                                 "Default %dKB."%defaultInsertSize, \
                       default = defaultInsertSize )

    default_t = 50
    parser.add_option( "-t", \
                       "--nRep_masking", \
                       type    = 'int', \
                       help    = "Number of repetitions for a 24-mer to be highly repetitive. " + \
                                 "Default %d."%default_t, \
                       default = default_t )

    defaultNumPairs = 200000
    parser.add_option( "-p", \
                       "--numPairs", \
                       type    = 'int', \
                       help    = "Number of pairs to pull:  Default %d."%defaultNumPairs,
                       default = defaultNumPairs )

    default_min_perID = 90.0
    parser.add_option( "-i", \
                       "--identity_Cutoff", \
                       type    = 'float', \
                       help    = "Minimum ID: Default %.1f"%default_min_perID,
                       default = default_min_perID )

    default_min_perCov = 90.0
    parser.add_option( "-v", \
                       "--coverage_Cutoff", \
                       type    = 'float', \
                       help    = "Minimum Coverage: Default %.1f"%default_min_perCov,
                       default = default_min_perCov )

    default_score = 35
    parser.add_option( "-s", \
                       "--MinBLATScore", \
                       type    = 'int', \
                       help    = "Minimum BLAT Score: Default %d"%default_score,
                       default = default_score )

    parser.add_option( "-N", \
                       "--noMasking", \
                       action  = 'store_true', \
                       dest    = 'noMasking', \
                       help    = "Do not mask the genome:  Default=False" )
    parser.set_defaults( noMasking = False )

    parser.add_option( "-T", \
                       "--arachneReadNames", \
                       action  = 'store_true', \
                       dest    = 'arachneReadNames', \
                       help    = "Use ARACHNE style read naming:  Default=False" )
    parser.set_defaults( arachneReadNames = False )
    
    default_nFiles = 10
    parser.add_option( "-n", \
                       "--nFastaFiles", \
                       type    = 'int', \
                       help    = "Number of files for batch processing %d"%default_nFiles,
                       default = default_score )
                       
    default_cluster = 'ssd'
    parser.add_option( "-c", \
                       "--cluster", \
                       type    = 'str', \
                       help    = "Cluster to use. " + \
                                 "Default %s."%default_cluster, \
                       default = default_cluster )
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        for n in xrange(2):
            if (not isfile(args[n])):
                parser.error( '%s can not be found'%args[n] )
            #####
        #####
        pairsFile = args[0]
        testFile( pairsFile )

        assemblyFile = args[1]
        testFile( assemblyFile )
    #####

    # Converting insert size and bin width to KB
    options.maxInsertSize = 1000 * options.maxInsertSize

    # Pushing the information to an underlying function
    pairAssessment_call( pairsFile, \
                         assemblyFile, \
                         options.identity_Cutoff, \
                         options.coverage_Cutoff, \
                         options.MinBLATScore, \
                         options.numPairs, \
                         options.maxInsertSize, \
                         options.arachneReadNames, \
                         options.nRep_masking, \
                         options.num24Mers, \
                         options.noMasking, \
                         options.nFastaFiles, \
                         options.cluster)

    return

#####f

if ( __name__ == '__main__' ):
    real_main()

