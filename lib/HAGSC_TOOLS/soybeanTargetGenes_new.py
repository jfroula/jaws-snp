#! /apps/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import iterFASTA, testFile, generateTmpDirName, baseFileName, deleteDir
from hagsc_lib import testDirectory, equal_size_split, getFiles, remoteServer4, deleteFile

from optparse import OptionParser

from sys import stdout, stderr

from os.path import realpath, isfile, abspath, join

from os import system, curdir, mkdir

########################################################################
class setup_base_class(object):

    def __init__(self, fastaFile, \
                       nFastaFiles=8, \
                       outputFileName=None, \
                       arachneReadFormat=False ):

        # Make sure the files exist
        testFile(fastaFile)

        # Reading in the control variables
        self.fastaFile         = fastaFile
        self.nFastaFiles       = nFastaFiles
        self.arachneReadFormat = arachneReadFormat

        # Generating the directory information
        self.basePath = abspath(curdir)
        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                     baseFileName(self.fastaFile) )

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        equal_size_split( self.fastaFile, \
                          self.baseOutFileName, \
                          self.nFastaFiles, \
                          self.arachneReadFormat )
        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

########################################################################
class main_run_class( setup_base_class ):
    def __init__(self, queryFile, \
                       targetFile, \
                       nFastaFiles, \
                       clusterNum, \
                       outputFileName, \
                       arachneReadFormat = False ):

        ## Initializing the best hit base class
        setup_base_class.__init__( self, \
                                   queryFile, \
                                   nFastaFiles, \
                                   arachneReadFormat )
        
        # Reading in the information
        self.queryFile         = queryFile
        self.targetFile        = targetFile
        self.clusterNum        = clusterNum
        self.outputFileName    = outputFileName
        self.arachneReadFormat = arachneReadFormat

    def executeCases(self):
        # Step 1:  Create temporary directory
        self.create_tmp_dir()
        # Step 2:  Break the fasta file into little pieces
        self.splitFASTA()
        # Step 4:  BLAT Fastas
        self.sendToCluster()
        # Step 5: Parse Output
        self.parseOutput()
        # Step 7: Cleanup
        self.clean_tmp()
        return

    def sendToCluster(self):
        stderr.write( '\t-BLATing Reads\n')
        # (1)  Set up the command list
        commandList = []
        frontCommand = '/mnt/local/EXBIN/blat -extendThroughN'
        for fastaFile in getFiles( 'fasta', self.tmpPath ):
            testFile(fastaFile)
            localOutputFile = '%s.blat'%fastaFile
            commandList.append( '%s %s %s %s'%( frontCommand, self.targetFile, fastaFile,  localOutputFile ) )
        #####
        
        # (2)  Run the cases
        perlFile = 'blat_run.pl'
        remoteServer4( commandList, self.nFastaFiles, self.clusterNum, perlFile )
        deleteFile(perlFile)
        return

    def parseOutput(self):
        stderr.write( '\t-Parsing output...\n')
        # Looping over gzip files
        deleteFile(self.outputFileName)
        bestHitFileHandle = open( self.outputFileName, 'w' )
        for blatFile in getFiles( 'blat', self.tmpPath ):
            blatHandle = open(blatFile)
            map(bestHitFileHandle.write, blatHandle )
            blatHandle.close()
        #####
        bestHitFileHandle.close()
        return

#==============================================================
class chromosome(object):
    def __init__(self,tmpDict):
        self.CHR         = tmpDict['CHR']
        self.genes       = {}
        self.geneOrder   = {}
        self.activeGenes = {}
        self.addGene(tmpDict)
    
    def addGene(self,tmpDict):
        # Adding gene information
        geneName = tmpDict['geneName']
        try:
            self.genes[geneName].addExon( tmpDict )
        except KeyError:
            self.genes[geneName] = gene(tmpDict )
        #####
        # Gene order information
        exonStart = tmpDict['exonStart']
        try:
            self.geneOrder[geneName] = min( self.geneOrder[geneName], exonStart )
        except KeyError:
            self.geneOrder[geneName] = exonStart
        #####
        return
    
    def addAlignment(self,alignment,tmpDict):
        geneName = tmpDict['geneName']
        self.genes[geneName].addAlignment(alignment,tmpDict)
        self.activeGenes[geneName] = self.geneOrder[geneName]
    

#==============================================================
class gene(object):
    def __init__(self,tmpDict):
        self.geneID    = tmpDict['geneName']
        self.exons     = {}
        self.exonOrder = []
        self.contigSet = set()
        self.exonSet   = set()
        self.addExon(tmpDict)
    
    def addExon(self,tmpDict):
         exonID    = tmpDict['exonID']
         exonStart = tmpDict['exonStart']
         self.exons[exonID] = exon(tmpDict)
         if ( exonID in self.exonSet ): return
         self.exonSet.add(exonID)
         self.exonOrder.append( (exonStart, exonID) )

    def addAlignment(self,alignment,tmpDict):
        self.exons[tmpDict['exonID']].addAlignment(alignment,tmpDict)
        self.contigSet.add( tmpDict['contigID'] )

#==============================================================
class exon(object):
    def __init__(self,tmpDict):
        self.exonID    = tmpDict['exonID']
        self.exonStart = tmpDict['exonStart']
        self.exonEnd   = tmpDict['exonEnd']
        self.alignment = None
    
    def addAlignment(self,alignment,tmpDict):
        self.alignment = str(alignment)

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [Region (ex. Gm09)] [options]"

    parser = OptionParser(usage)

    ID = 90.0
    parser.add_option( "-i", \
                       "--ID", \
                       type    = "float", \
                       help    = "Exon alignment identity: Default: %.2f"%ID, \
                       default = ID )
                       
    COV = 75.0
    parser.add_option( "-v", \
                       "--COV", \
                       type    = "float", \
                       help    = "Exon coverage: Default: %.2f"%COV, \
                       default = COV )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        targetFASTA = realpath(args[0])
        if ( not isfile(targetFASTA) ): parser.error( '%s can not be found'%targetFASTA )
        targetCHR   = args[1]
    #####
    
    #========================================================================
    # Step 1:  Perform the best hit alignment
    targetExons  = '/home/t4c1/WORK/JWJ_ANALYSIS/Soybean/stuffForJane/targetRegionExons_new.fasta'
    nFastaFiles  = 1
    clusterNum   = 101
    blatFileName = 'tmpHit.blat'
    main_run_class( targetExons, \
                    targetFASTA, \
                    nFastaFiles, \
                    clusterNum, \
                    blatFileName ).executeCases()
    bestHitOutputFile = 'tmpBestHit.out'
    system( 'python -m convertToBestHit -o %s %s'%(bestHitOutputFile, blatFileName) )
    system( 'rm -f %s'%blatFileName )
    
    # Parsing the gene information    
    geneDict  = {}
    geneOrder = {}
    for record in iterFASTA( open(targetExons) ):
        # Splitting the name
        exonStart, exonEnd, geneName, CHR, strand = record.id.split('|')
        tmpDict = {'CHR':CHR, 'geneName':geneName, 'exonStart':int(exonStart), 'exonEnd':int(exonEnd), 'exonID':record.id}
        if ( CHR[:2] != 'Gm' ): continue
        try:
            geneDict[CHR].addGene(tmpDict)
        except KeyError:
            geneDict[CHR] = chromosome(tmpDict)
        #####
    #####
    
    # Parsing the alignments
    alignmentDict  = {}
    chrSet         = set()
    activeGeneDict = {}
    for line in open(bestHitOutputFile):
        # Screening for ID and coverage
        splitLine = line.split(None)
        ID        = float(splitLine[10])
        COV       = float(splitLine[11])
        if ( (ID < options.ID) or (COV < options.COV) ): continue
        # Pulling the exon information
        exonName   = splitLine[0]
        exonStart, exonEnd, geneName, CHR, strand = splitLine[0].split('|')
        if ( CHR != targetCHR ): continue
        tmpDict   = {'CHR':CHR, 'geneName':geneName, 'exonStart':int(exonStart), 'exonEnd':int(exonEnd), 'exonID':splitLine[0], 'contigID':splitLine[5]}
        alignment = '%s\t%s\t%s\t%s\t%s\t%s\n'%( splitLine[0], splitLine[5], splitLine[7], splitLine[8], \
                                             splitLine[10], splitLine[11] )
        geneDict[CHR].addAlignment(alignment,tmpDict)
    #####
    system( 'rm -f %s'%bestHitOutputFile )
    
    # Writing out to the user
    CHR_names = [ ( int(chrID[2:]), chrID ) for chrID in geneDict.iterkeys() ]
    CHR_names.sort()
    noHits = True
    for tmpINT, CHR in CHR_names:
        # Sorting the active genes
        DSU_a = [ (value, key) for key, value in geneDict[CHR].activeGenes.iteritems() ]
        if ( DSU_a == [] ): continue
        noHits = False
        # Sorting all the genes in the target region
        DSU = [ (tmpStart, geneID) for geneID, tmpStart in geneDict[CHR].geneOrder.iteritems()]
        DSU.sort()
        for geneStart, geneName in DSU:
            # Sort the exons
            geneDict[CHR].genes[geneName].exonOrder.sort()
            pre = ""
            nContigs = len(geneDict[CHR].genes[geneName].contigSet)
            if ( nContigs > 1 ): pre = "**"
            stdout.write( "===============================\n" )
            stdout.write( '%s  %s\n'%(pre, geneName) )
            stdout.write( "\texonID\t\t\t\t\tcontigID\tctgStrt\tctgEnd\tID\tCOV\n" )
            stdout.write( "\t------\t\t\t\t\t--------\t-------\t------\t--\t---\n" )
            tmpNumer  = 0.0
            tmpDenom  = 0.0
            nExons    = len(geneDict[CHR].genes[geneName].exonOrder)
            contigSet = set()
            gStart    = 1e6
            gEnd      = -1
            nExonsAligned = 0
            for exonStart, exonID in geneDict[CHR].genes[geneName].exonOrder:
                # Output to the file
                tmpAln = geneDict[CHR].genes[geneName].exons[exonID].alignment
                if ( tmpAln == None ):
                    outputString = '%s\t%s\tNone\n'%(pre,exonID)
                else:
                    outputString = '%s\t%s'%(pre,tmpAln)
                    nExonsAligned += 1
                #####
                stdout.write( outputString )

                # Performing the scoring
                splitAlignment = outputString.split(None)
                if ( nContigs > 1 ): splitAlignment.pop(0)
                
                #(c) Computing the exon alignment score
                splitGeneName = splitAlignment[0].split("|")
                eStart        = int(splitGeneName[0])
                eEnd          = int(splitGeneName[1])
                exonBases     = float( abs(eEnd - eStart + 1 ) )
                
                if ( tmpAln == None ):
                    #(c) Computing the exon alignment score
                    tmpDenom += exonBases
                else:
                    #(a) Making sure we are on a single contig
                    contigSet.add( splitAlignment[1] )
                    #(b) Pulling the exon start and end on the clone
                    cStart  = int(splitAlignment[2])
                    cEnd    = int(splitAlignment[3])
                    if ( cStart < gStart ): gStart = cStart
                    if ( cEnd > gEnd ):     gEnd = cEnd
                    #(c) Computing the exon alignment score
                    ID            = float(splitAlignment[4]) / 100.0
                    COV           = float(splitAlignment[5]) / 100.0
                    tmpNumer     += ID * COV * exonBases
                    tmpDenom     += exonBases
                #####
            #####
            # Computing the final score
            try:
                geneScore     = 100.0 * tmpNumer / tmpDenom
            except ZeroDivisionError:
                geneScore = 0.0
            #####
            # Writing out the final string
            if ( len(contigSet) == 0 ):
                stdout.write( "SCORE:\t%s\tNO_ALIGNED_EXONS\t\t\tnExons=%d/%d\texonBases=%d\tgeneScore=%.3f\n"%(geneName,nExonsAligned,nExons,tmpDenom,geneScore) )
            elif ( len(contigSet) == 1 ):
                extentOnClone = gEnd - gStart + 1
                stdout.write( "SCORE:\t%s\t%s\tstart=%d\tend=%d\tnExons=%d/%d\texonBases=%d\tgeneScore=%.3f\n"%(geneName,''.join(contigSet),gStart,gEnd,nExonsAligned,nExons,tmpDenom,geneScore) )
            elif ( len(contigSet) > 1 ):
                stdout.write( "SCORE:\t%s\tMULTIPLE_CLONE_CONTIGS\t\t\tnExons=%d/%d\texonBases=%d\tgeneScore=%.3f\n"%(geneName,nExonsAligned,nExons,tmpDenom,geneScore) )
            #####
        #####
    #####
    
    # If we don't get a hit on anything
    if ( noHits ):
        stdout.write( "\n\n--------------------\nEXONS FROM TARGET REGION %s DID NOT ALIGN TO THE ASSEMBLY\n--------------------\n\n"%targetCHR )
    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
