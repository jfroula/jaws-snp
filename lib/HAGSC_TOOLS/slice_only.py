#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott"
__date__ ="$Apr 26, 2013$"

# Python Library Imports
from optparse import OptionParser

from hagsc_lib import generateTmpDirName
from hagsc_lib import testDirectory
from hagsc_lib import iterCounter
from hagsc_lib import deleteFile
from hagsc_lib import deleteDir

from sys import stderr
from sys import stdout
from sys import argv

from os.path import isfile, realpath
from os.path import abspath
from os.path import join

from os import system
from os import curdir
from os import mkdir

import subprocess

import getpass

import time

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [reads FASTA] [Out File Base] [lowerKmer] [options]"

    parser = OptionParser(usage)


    defVal = 85.0
    parser.add_option( "-p", \
                       "--maskPercent", \
                       type    = 'float', \
                       help    = "Masked percentage for read.  Default: %.1f"%defVal, \
                       default = defVal )
                       
    parser.add_option( "-u", \
                       "--upperBound", \
                       type    = 'int', \
                       help    = "Sets the upper kmer bound." )
    defaultKmer = 24
    parser.add_option( "-m", \
                       "--kmerSize", \
                       type    = 'int', \
                       help    = "Sets the kmer size. Default: %d"%(defaultKmer),\
                       default = defaultKmer )
                       
    parser.add_option( "-r", \
                       "--pairReads", \
                       action  = 'store_true',\
                       help    = "Will only output read pairs that are found in the kmer bounds. Default is False",\
                       default = False )
                       
    
    parser.add_option( "-L", \
                       "--List", \
                       action  = 'store_true',\
                       help    = "Outputs a list of read names instead of sequence for a Linear file. Default is False",\
                       default = False )
    
    parser.add_option( "-k", \
                       "--kmerTree", \
                       type    = 'str', \
                       help    = "Defines the kmerTree file to be used.",\
                       default = False )
                       
    parser.add_option( "-z", \
                       "--zNum", \
                       type    = 'str', \
                       help    = "When not using a kmerTree defines the -z value. Default is 4g",\
                       default = '4g' )
                       
    parser.add_option( "-Z", \
                       "--emptyTable", \
                       action  = 'store_true',\
                       help    = "When using -z, will empty table of single kmers when table gets full. Default is False",\
                       default = False )
                                                             
    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )

    else:
        readsFile = realpath(args[0])
        # Pulling the fileNames
        if (not isfile(readsFile) ): parser.error( 'File:  %s is not found.'%readsFile )
        
        # Reading the outfile base name
        outFileBaseName = args[1]

        # Pulling the lowerKmer
        lowerKmer = int( args[2] )
        if ( lowerKmer < 1 ): parser.error( 'lowerKmer must be > 1:  %s'%args[2] )
    #####
    # Prepping to build the command
    maskOptions     = []
    if ( options.maskPercent > 0 ):
        maskPercent = options.maskPercent
    else:
        parser.error( 'maskPercent must be > 0' )
    if (options.upperBound):
        upperKmer = options.upperBound
        if ( upperKmer < 1 ): parser.error( 'upperKmer must be > 1:  %s'%args[4] )
        if ( upperKmer < lowerKmer ): parser.error( 'upperKmer must be >= lowerKmer' )
        maskOptions.append( '-u %d'%upperKmer )
    #####
    
    # Creating a temporary space and names
    basePath   = abspath(curdir)
    tmpDir     = generateTmpDirName()
    tmpPath    = join( basePath, tmpDir )
    stderr.write( '\n\t-Creating tmp directory %s\n'%tmpPath)
    deleteDir(tmpPath)
    mkdir( tmpPath, 0777 )
    testDirectory(tmpPath)
    tmpHead        = join(basePath, outFileBaseName)     
    # Finding the reads
    maskHashCommand = '/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/EXBIN/mask_repeats_hashn'
    maskOptions.append( '-B 10000' )
    maskOptions.append( '-t %d'%lowerKmer )
    maskOptions.append( '-F'              )
    if(options.emptyTable): maskOptions.append( "-Z ")
    # Is there a kmerTree
    if (options.kmerTree):
        if (not isfile(options.kmerTree) ): parser.error( 'File:  %s is not found.'%kmerTree )
        maskOptions.append( '-S %s'%options.kmerTree  )
        maskCommand = "%s %s %s" % ( maskHashCommand, ' '.join(maskOptions), readsFile )
    else:
        maskOptions.append( '-z %s'%options.zNum )
        maskOptions.append( '-m %d'%(options.kmerSize))
        maskCommand = "%s %s %s" % ( maskHashCommand, ' '.join(maskOptions), readsFile )
    #####
    if (options.pairReads):
        # Are both the reads of the pair found in the bounds?
        cmdString    = "%s | tr \"%%\" \" \"| awk '{ if ($2>%.2f) print $1 }'"%( maskCommand, maskPercent )
        print cmdString
        maskProcess  = subprocess.Popen( cmdString, shell=True, stdout=subprocess.PIPE )
        oh       = open('%s.dat'%tmpHead, 'w')
        readDict = {}
        for line in maskProcess.stdout:
            name, num  = line.split('-R')
            try:
                readDict[name].add(num)
            except KeyError:
                readDict[name] = set(num)
                continue
            #####
            if len(readDict[name]) < 2 : continue
            oh.write('%s-R1\n%s-R2\n'%(name, name))
            readDict.pop(name)
        #####
    else:
        # Do not care if the reads are paired or not in the bounds
        baseName       = '%s.reads_%s'%(tmpHead,'%s-%s'%(lowerKmer, upperKmer))
        cmdString    = "%s | tr '%%' ' '| awk '{ if ($2>%.2f) print $1 }' > %s"%( maskCommand, maskPercent, '%s/tmp.dat'%tmpPath )
        print cmdString
        system( cmdString )
        if ((options.List)):
            system('mv %s %s'%('%s/tmp.dat'%tmpPath, '%s.dat'%(tmpHead)))
        else:
            # Pulling the reads
            cmdString = '/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/EXBIN/extract_seq_and_qual -i %s/tmp.dat -o %s.fasta.bz2 %s'%(tmpPath, baseName, readsFile)
            print cmdString
            system(cmdString)
    #####

    deleteDir( tmpDir )    
    return
#####
    
#==============================================================    
if ( __name__ == '__main__' ):
    real_main()

