#!/usr/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2013$"
#=========================================================================
# EDIT LOG
#
#
#
#
#=========================================================================
from time import time
from sys import  stderr, stdin, stdout, argv
from os import system
import subprocess
from optparse import OptionParser
from hagsc_lib import iterFASTA
#=========================================================================
class ScaffoldClass(object):
    def __init__(self, name, seqLen):
        self.name         = name
        self.seqLen       = seqLen
        self.intervalDict = {}
        self.bestHitDict = {}
    #####
        
#=========================================================================
class intervalClass(object):
    def __init__(self,start,end,fragID):
        self.fragID               = fragID
        start, end                = sorted([start,end])
        self.intervalDict         = {}
        self.intervalDict[fragID] = [(start,end)]

    def addInterval(self,start,end,fragID):
        start, end = sorted([start,end])
        try:
            self.intervalDict[fragID].append( (start,end) )
            self.collapseIntervals(fragID)
        except KeyError:
            self.intervalDict[fragID] = [(start,end)]
        #####
        return
        
    def isOverlapped(self,s1,e1,s2,e2):
        return not ( (s2>e1) or (s1>e2) )

    def collapseIntervals(self,fragID):
        notDone = True
        while (notDone):
            notDone    = False
            nIntervals = len( self.intervalDict[fragID] )
            intervals   = self.intervalDict[fragID]
            for i in range(nIntervals-1):
                s1, e1 = intervals[i]
                for j in range( (i+1), nIntervals ):
                    s2, e2 = intervals[j]
                    if ( self.isOverlapped(s1,e1,s2,e2) ):
                        # Compute the new bound
                        sortedBounds = sorted([s1,e1,s2,e2])
                        ns = sortedBounds[0]
                        ne = sortedBounds[-1]
                        # Collapsing the intervals
                        self.intervalDict[fragID][i] = (ns,ne)
                        self.intervalDict[fragID].pop(j)
                        notDone = True
                        break
                    #####
                #####
                if ( notDone ): break
            #####
        #####
        return
    #####

    def computeComposition(self, totalBases):
        alignBases = sum([ (end-start+1) for start,end in self.intervalDict[self.fragID] ])
        return 100.0 * float(alignBases) / float( totalBases )
    
    #def computeComposition(self, totalBases):
    #    alignBases = sum( [ (end-start+1) for start,end in self.intervalDict.values() ] )
    #    return 100.0 * float(alignBases) / float( totalBases )
        
#=========================================================================
def real_main():
    # Parse the output
    qFasta        = argv[1]
    allDBTypes    = set()
    allDBTypes.add(argv[2])
    #totalBases   = argv[1]
    #dbType       = argv[2]
    #scaffID      = argv[3]
    scaffoldDict  = {}
    
    for record in iterFASTA(open(qFasta)):
        record.id
        record.seq
        scaffoldDict[record.id] = ScaffoldClass(record.id, len(record.seq))
    #####
    # Performing the analysis
    for line in map( bytes.decode, stdin ):
        # Screening out the comment lines
        if ( line[0] == "#" ): continue
        # Splitting the line
        splitLine = line.split(None)
        if ( splitLine == [] ): continue
        # Pulling off the information
        try:
            fragID, hitID, perID, alignLength, mismatch, gaps, qStart, qStop, tStart, tStop, eValue, score, dbType = splitLine
        except ValueError:
            continue
        #####
        
        allDBTypes.add(dbType)
        
        # Splitting the fragment ID
        #scaffID, contigID = fragID.split('|')
        # Adding the interval to the class
        try:
            scaffoldDict[fragID].intervalDict[dbType.split('_')[0]].addInterval( int(qStart), int(qStop), fragID )
        except KeyError:
            scaffoldDict[fragID].intervalDict[dbType.split('_')[0]] = intervalClass( int(qStart), int(qStop), fragID )
        #####
        # Incrementing the best hit dictionary
        score = float(score)
        try:
            if ( score > scaffoldDict[fragID].bestHitDict[dbType][0] ): scaffoldDict[fragID].bestHitDict[dbType] = (score,hitID)
        except KeyError:
            scaffoldDict[fragID].bestHitDict[dbType] = (score,hitID)
        #####
    #####
    
    allDBTypes = list(allDBTypes)
    allDBTypes.sort()
    
    for scaf in scaffoldDict:
        outputString = [scaf]
        totBases     = scaffoldDict[scaf].seqLen
        outputString.append(str(totBases))
        for db in allDBTypes:
            outputString.append(db)
            try:
                alignedPer = scaffoldDict[scaf].intervalDict[db].computeComposition(totBases)
                outputString.append( '%.2f'%alignedPer )
                outputString.append( scaffoldDict[scaf].bestHitDict[db][1] )
            except KeyError:
                outputString.append( '0.0' )
                outputString.append( 'None' )
            #####
        #####
        stdout.write("%s\n"%("\t".join(outputString)))
    #####

#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()