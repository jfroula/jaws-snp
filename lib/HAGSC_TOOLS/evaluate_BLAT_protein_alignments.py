#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  2/25/16"

# Python Library Imports
from optparse import OptionParser

from sys import argv, stdout, stdin

from os.path import isfile

#==============================================================
def checkFile( fileName, parser ):
    if ( not isfile(fileName) ): 
        parser.error( '%s can not be found'%fileName )
    else:
        return fileName
    #####

#==============================================================
def real_main():
    
    # Defining the program options
    usage = "usage: %prog [BLAT or stdin] [options]"

    # Parsing the arguements
    parser = OptionParser(usage)
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        if ( args[0] == 'stdin' ):
            inputBLATFile = 'stdin'
        else:
            inputBLATFile = checkFile( args[0], parser )
        #####
    #####

    # Opening the blat file
    if ( inputBLATFile == 'stdin' ):
        oh = stdin
    else:
        oh = open( inputBLATFile )
    #####
    
    hitDict = {}
    for line in oh:
        s        = line.split(None)
        match    = int(s[0])
        repMatch = int(s[2])
        Q_id     = s[9]
        try:
            hitDict[Q_id].append( ( (match+repMatch), s ) )
        except KeyError:
            hitDict[Q_id] = [ ( (match+repMatch), s ) ]
        #####
    #####
    
    # Closing the blat file
    if ( inputBLATFile != 'stdin' ): oh.close()
    
    for Q_id, tmpList in hitDict.iteritems():
        tmpList.sort(reverse=True)
        s          = tmpList[0][1]
        match      = int(s[0])
        repMatch   = int(s[2])
        strand     =     s[8]
        Q_id       =     s[9]
        Q_size     = int(s[10])
        Q_start    = int(s[11])
        Q_end      = int(s[12])
        T_id       =     s[13]
        T_size     = int(s[14])
        T_start    = int(s[15])
        T_end      = int(s[16])
        totalMatch = match + repMatch
        ID         = 100.0 * float(totalMatch) / float( Q_end - Q_start )
        COV        = 100.0 * float( Q_end - Q_start ) / float( Q_size )
        
        outputString = [Q_id]
        outputString.append( '%d'%Q_size  )
        outputString.append( '%d'%Q_start )
        outputString.append( '%d'%Q_end   )
        outputString.append( strand       )
        outputString.append( T_id         )
        outputString.append( '%d'%T_size  )
        outputString.append( '%d'%T_start )
        outputString.append( '%d'%T_end   )
        outputString.append( '%d'%(totalMatch) )
        outputString.append( '%.2f'%ID    )
        outputString.append( '%.2f\n'%COV   )
        
        stdout.write( '\t'.join(outputString) )

    #####
    
    

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
