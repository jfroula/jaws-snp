#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 12, 2014$"
#=========================================================================
# EDIT LOG
#
#
#
#
#=========================================================================
from time import time
from sys import  stderr, stdout
from os import system
import subprocess
from optparse import OptionParser
from hagsc_lib import iterCounter, isGzipFile, isBzipFile, testFile
from hagsc_lib import iterFASTA, writeFASTA, iterQUAL, writeQUAL, revComp
from os.path import realpath, isdir, isfile, join, abspath, curdir
import gzip

#=========================================================================
# Regular expressions
import re
commify_re          = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
findStuff           = re.compile( r'(1+)' ).finditer
doesPass_dict       = {True:'1',False:'0'}
charToAscii         = dict( [(int(val)-33,chr(val)) for val in range(0,120)] )
asciiToInt          = dict( [(chr(val),int(val)-33) for val in range(33,120)] )
#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True
#=========================================================================
def pullBaseName( tmpLine ):
    if ( tmpLine.count('.') > 0 ):
        x = tmpLine.split(None)[0].split('.')
        x.append( '.' )
        return x
    elif ( tmpLine.count('-R') > 0 ):
        x = tmpLine.split(None)[0].split('-R')
        x.append( '-R' )
        return x
    #####

#=========================================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
        
#=========================================================================        
def customClip( seq, ucQual ):
    # Convert the qual to ints
    windowSize = 20
    qualCutOff = 25
    minVal     = windowSize * qualCutOff
    qual       = ucQual
    
    # find HQ window
    qualLen     = len(qual) - windowSize + 1
    clippedList = ''
    for i in range( qualLen ):clippedList = clippedList+doesPass_dict[sum(qual[i:i+windowSize]) >= minVal]

    HQRanges = []
    for item in findStuff( clippedList ):HQRanges.append(item.span()) 
    
    try:
        clipStart = HQRanges[0][0] 
        clipEnd   = HQRanges[-1][1] + 19
        if (clipEnd +1 == qualLen +windowSize): clipEnd += 1
    except IndexError:
        return 
    #####
    
    # Return the clipped read    
    return seq[clipStart:clipEnd], [ charToAscii[item] for item in ucQual[clipStart:clipEnd]]
        
#=========================================================================def real_main():
def real_main():
    # Defining the program options
    usage = "usage: %prog [FASTQ(A) FILE] [BASE NAME] [options]"

    parser = OptionParser(usage)
    
    minLength = 80
    parser.add_option( "-l", \
                       "--minLength", \
                       type    = "int", \
                       help    = "Discard any fakeOverlap reads shorter than MINLENGTH. Default: %s"%minLength, \
                       default = minLength )
                       
    nOverlap = 75
    parser.add_option( "-k", \
                       "--overlap", \
                       type    = "int", \
                       help    = "Number of bases to overlap. Default: %s"%nOverlap, \
                       default = nOverlap )

    nOverlap = -1
    parser.add_option( "-n", \
                       "--nReads", \
                       type    = "int", \
                       help    = "Number of reads to make. Default: all", \
                       default = nOverlap )
                       
    excludable = '2'                   
    parser.add_option( "-e", \
                       "--excludeReadNum", \
                       type    = "str", \
                       help    = "Read number to exclude '1' or '2'. Default '2'", \
                       default = excludable )
                       
    excludable = None                   
    parser.add_option( "-q", \
                       "--qualFile", \
                       type    = "str", \
                       help    = "Qual used in clipping. Default None", \
                       default = excludable )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    
    elif (options.qualFile == None):
        # only fasta given
        
        if(testFile(args[0])):
            FASTAFILE = args[0]
        else:
            parser.error( '%s NOT FOUND.'%args[0] )
        #####
        oh_olFastq    = gzip.open("%s.fakeOverlap.fastq.gz"%args[1],"w")
        overlap       = options.overlap
        rawReadLength = (options.minLength - overlap)*2+overlap
        nReads        = options.nReads
        counter       = iterCounter(1000000)
        excludeRead   = options.excludeReadNum
        awkCmd        = "awk '{if(NR%%4==1){name=$1; readNumber = substr($1,length($1),1)}if(NR%%4==2){seq = $1}if(NR%%4==0){qual = $1; if(readNumber != %s){print name, seq, qual} } }'"%(excludeRead)
        
        if ( isGzipFile(FASTAFILE) ):
            fastaSub = 'cat %s | gunzip -c | %s'%( FASTAFILE, awkCmd )
        elif ( isBzipFile(FASTAFILE) ):
            fastaSub = 'cat %s | bunzip2 -c | %s '%( FASTAFILE, awkCmd )
        else:
            fastaSub = 'cat %s | %s'%( FASTAFILE, awkCmd )
        #####
        
        # Starting FASTA process
        fastaProc      = subprocess.Popen( fastaSub, shell=True, stdout=subprocess.PIPE).stdout
        fastaDict      = {}
        readsConverted = 0
        
        while True:

            try:
                name, seq, qual = fastaProc.next().split(None)
            except StopIteration:
                break
            #####
            
            covQual = [asciiToInt[item] for item in qual]
            
            try:
                seq, qual = customClip(seq, covQual)
            except TypeError:
                continue
            #####
            
            qual      = ''.join(qual)
            readLen   = len(seq)
            
            if (readLen < rawReadLength): continue
            if (readLen%2 == 1):
                seq     = seq[:-1]
                qual    = qual[:-1]
                readLen = len(seq)
            #####

            overlapRange        = (readLen - overlap)/2
            fbaseName, ext, sep = pullBaseName( name )
            
            R1name = '%s_ffo/1'%(fbaseName)
            R1seq  = str(seq[:-overlapRange])
            R1qual = str(qual[:-overlapRange])
            R2name = '%s_ffo/2'%(fbaseName)
            R2seq  = revComp(str(seq[overlapRange:]))
            R2qual = str(qual[overlapRange:][::-1])
            oh_olFastq.write('%s\n%s\n+\n%s\n%s\n%s\n+\n%s\n'%(R1name,R1seq,R1qual,R2name,R2seq,R2qual))
            readsConverted += 2
            counter()
            counter()
            if ((nReads > 0)and(readsConverted >= nReads)):
                break
            #####
        ##### 
    
    
    else:
    
        if(testFile(args[0])):
            FASTAFILE = args[0]
        else:
            parser.error( '%s NOT FOUND.'%args[0] )
        #####
        
        if(testFile(options.qualFile)):
            QUALFILE  = options.qualFile
        else:
            parser.error( '%s NOT FOUND.'%options.qualFile )
        #####
        oh_olFastq    = gzip.open("%s.fakeOverlap.fastq.gz"%args[1],"w")
        overlap       = options.overlap
        rawReadLength = (options.minLength - overlap)*2+overlap
        nReads        = options.nReads
        counter       = iterCounter(1000000)
        excludeRead   = options.excludeReadNum
        
        if ( isGzipFile(FASTAFILE) ):
            fastaSub = 'cat %s | gunzip -c'%( FASTAFILE )
        elif ( isBzipFile(FASTAFILE) ):
            fastaSub = 'cat %s | bunzip2 -c'%( FASTAFILE )
        else:
            fastaSub = 'cat %s'%( FASTAFILE )
        #####
    
        if ( isGzipFile(QUALFILE) ):
            qualSub  = 'cat %s | gunzip -c'%( QUALFILE )
        elif ( isBzipFile(QUALFILE) ):
            qualSub  = 'cat %s | bunzip2 -c'%( QUALFILE )
        else:
            qualSub  = 'cat %s'%( QUALFILE )
        #####
        
        fastaProc = iterFASTA( subprocess.Popen( fastaSub, shell=True, stdout=subprocess.PIPE).stdout )
        fastaDict = {}
        
        # Starting QUAL process
        qualProc       = iterQUAL( subprocess.Popen( qualSub, shell=True, stdout=subprocess.PIPE).stdout )
        qualDict       = {}
        readsConverted = 0        
        
        while True:
            try:
                f_record  = next( fastaProc )
                q_record  = next( qualProc )
            except StopIteration:
                break
            #####
            
            if (f_record.id != q_record.id): continue
            
            if (f_record.id[-1] == excludeRead):     continue
            
            seq          = f_record.seq
            qual         = [ int(item) for item in q_record.qual ]
            
            try:
                seq, qual    = customClip(seq, qual)
            except TypeError:
                continue
            #####
            
            readLen      = len(seq)
            if (readLen < rawReadLength): continue
            
            if (readLen%2 == 1):
                seq     = seq[:-1]
                qual    = qual[:-1]
                readLen = len(seq)
            #####

            overlapRange = (readLen - overlap)/2
            
            fbaseName, ext, sep = pullBaseName( f_record.id )
            R1name = '%s_ffo/1'%(fbaseName)
            R1seq  = str(seq[:-overlapRange])
            R1qual = ''.join(qual[:-overlapRange])
            R2name = '%s_ffo/2'%(fbaseName)
            R2seq  = revComp(str(seq[overlapRange:]))
            R2qual = ''.join(qual[overlapRange:][::-1])
            oh_olFastq.write('@%s\n%s\n+\n%s\n@%s\n%s\n+\n%s\n'%(R1name,R1seq,R1qual,R2name,R2seq,R2qual))
            readsConverted += 2
            counter()
            counter()
            if ((nReads > 0)and(readsConverted >= nReads)):
                break
            #####
        ##### 
    #####
                   
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()
