
from sys import stderr
from optparse import OptionParser

from itertools import chain
from itertools import repeat
from itertools import count
from itertools import izip
from itertools import izip_longest

from hagsc_lib import fasta_file
from hagsc_lib import validate_files
from hagsc_lib import group

def main():
    parser = OptionParser("usage: %prog [options] [FILES] \n %prog --help for options")
    parser.add_option("-c", "--case", action="store_true", dest="case", default=False,
        help="Use case-sensitive sequence comparison.")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False,
        help="Print sequence of scaffolds that differ.  Default prints only scaffold names.")
    parser.add_option("-x", "--max", action="store", dest="max", type="int", default=0,
        help="With the verbose option, print at most this number of lines from each mismatching scaffold.")
    (options, args) = parser.parse_args()
    
    if len(args) < 2:
        parser.print_usage()
        return
    filenames = args
    
    handles, errors = validate_files([(filename, 'r') for filename in filenames])
    if errors:
        print >>stderr, errors
        return
        
    if options.case:
        norm = lambda x: x
    else:
        norm = lambda x: x.upper()
    
    fastas = [fasta_file(handle) for handle in handles]
        
    keys = [fasta.keys() for fasta in fastas]
    [k.sort() for k in keys]
    
    for scaffolds in zip_align(keys):
        records = [id and fasta.get(id, None) or None for fasta, id in izip(fastas, scaffolds)]
        data = [record and norm(record.data()) or "" for record in records]
        dataSet = set(data)
        if (len(dataSet) > 1):
            for scaffold in scaffolds:
                if scaffold:
                    print scaffold
                    break
            if options.verbose: print_aligned(data, options.max)
  

def safe_index(list, item):
    try:
        return list.index(item)
    except:
        return -1
        
    
def zip_align(listlist):
    iters = [chain(x, repeat(None)) for x in listlist]
    delayed_iters = []
    items = [None] * len(iters)
    while True:
        for index, i in izip(count(0), iters):
            if i not in delayed_iters: items[index] = i.next()
        delayed_iters = []
        unique_items = set(items)
        if len(unique_items) == 1:
            if None in unique_items:
                break
            else:
                yield tuple(items)
        else:
            item_indices = [max(safe_index(x,s) for x in listlist) for s in unique_items]
            order = [x[0] for x in sorted(zip(unique_items, item_indices), key=lambda x: x[1])]
            delayed_iters = [x[0] for x in zip(iters, items) if x[1] != order[0]]
            yield tuple([x == order[0] and order[0] or None for x in items])
            

def print_aligned(collection, maximum=0, width=80):
    num = 0
    iters = [group(full_seq, width) for full_seq in collection]
    for short_seq in izip_longest(*iters):
        seqs = [line and ''.join(char for char in line if char) or "" for line in short_seq]
        seq_set = set(seqs)
        if len(seq_set) > 1:
            num += 1
            print '\n'.join(seqs)
            print "------------------------"
        if maximum and (num > maximum): break
    
    
if ( __name__ == "__main__" ):
    main()