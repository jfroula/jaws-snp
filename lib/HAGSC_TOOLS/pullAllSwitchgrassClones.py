#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import writeFASTA, iterFASTA

from optparse import OptionParser

from sys import stdout

from os import chdir, curdir, system, walk

from os.path import abspath, join, getmtime

import subprocess

#==============================================================
def all_files_under(path):
    # Iterates through all files that are under the given path and 
    # yields full path names
    for cur_path, dirnames, filenames in walk(path):
        for filename in filenames:
            yield join(cur_path, filename)
        #####
    #####
    return

#==============================================================
def pullSequence(cloneFASTA, oh, cloneID):
    totalSeq = 0
    for record in iterFASTA(open(cloneFASTA)):
        if ( len(record.seq) < 5000 ): continue
        record.id = '_'.join( [cloneID, record.id, 'SW_CLONE'] )
        writeFASTA( [record], oh )
        totalSeq += len(record.seq)
    #####
    return totalSeq

#==============================================================
def real_main():

    # Defining the program options
    usage = 'usage: %prog [Clone FASTA]'

    parser = OptionParser(usage)

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( 'Incorrect number of arguments.  ' + \
                      'View usage using --help option.' )
    else:
        # Pulling the library base
        cloneOutputFile = args[0]
    #####
    
    oh          = open( cloneOutputFile, 'w' )
    cmd         = "cat /mnt/raid2/MB/config/MB_status.txt | grep Switchgrass | grep -E '(FINISHED|PREFIN_COMPLETE)' | grep -v -E '(BAC_ENDS|FOS_ENDS)' | awk '{ if ( $5==\"BAC\" || $5==\"WGABAC\" ) print }'"
    tmpProcess  = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    workingPath = abspath(curdir)
    for line in tmpProcess.stdout:
        # Pulling the file information
        splitLine = line.split(None)
        cloneID = splitLine[0]
        status  = splitLine[8]
        # Pulling the cloneID
        cloneID = cloneID.strip()
        stdout.write( '\tCLONE ID:  %s\t%s\n'%(cloneID,status) )
        # Changing into the directory
        tmpPath = '/mnt/mongo3/doeassem/%s/edit_dir'%cloneID
        chdir(tmpPath)
        
        if ( status == "FINISHED" ):
            cloneFASTA = join( tmpPath, '%s.final.fasta'%cloneID )
            try:
                pullSequence(cloneFASTA, oh, cloneID)
            except IOError:
                # Exporting the latest version
                system( 'orchid --print-fasta-tagged' )
                # Finding the most recently modified file
                cloneFASTA = max( all_files_under(tmpPath), key=getmtime)        
                if ( pullSequence(cloneFASTA, oh, cloneID) == 0 ):
                    print "NO TAGGED SEQUENCE"
                    # Exporting the latest version
                    system( 'orchid --print-fasta' )
                    # Finding the most recently modified file
                    cloneFASTA = max( all_files_under(tmpPath), key=getmtime)
                    pullSequence(cloneFASTA, oh, cloneID)
                #####
            #####
        elif ( status == 'PREFIN_COMPLETE' ):
            # Exporting the latest version
            system( 'orchid --print-fasta-tagged' )
            # Finding the most recently modified file
            cloneFASTA = max( all_files_under(tmpPath), key=getmtime)        
            # Renaming the clone sequences
            if ( pullSequence(cloneFASTA, oh, cloneID) == 0 ):
                print "NO TAGGED SEQUENCE"
                # Exporting the latest version
                system( 'orchid --print-fasta' )
                # Finding the most recently modified file
                cloneFASTA = max( all_files_under(tmpPath), key=getmtime)
                pullSequence(cloneFASTA, oh, cloneID)
            #####
        #####
    #####
    chdir(workingPath)
    oh.close()


#==============================================================
if ( __name__ == '__main__' ):
    real_main()
