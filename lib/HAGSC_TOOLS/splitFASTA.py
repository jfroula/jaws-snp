#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Apr 20, 2010 3:58:20 PM$"

from hagsc_lib import iterFASTA, writeFASTA

from os.path import splitext

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout

def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE]"

    parser = OptionParser(usage)

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Checking for problems
        if ( not isfile(args[0]) ):
            parser.error( 'File not found:  %s'%args[0] )
        #####
        fasta_file_in = args[0]
    #####

    # Splitting the FASTA file    
    for record in iterFASTA(open(fasta_file_in)):
        outputHandle = open( "%s.fasta"%(record.id), 'w' )
        writeFASTA( [record], outputHandle )
        outputHandle.close()
    #####
    stdout.write( 'Successfully split scaffolds %s\n\n'%fasta_file_in )
    return

if ( __name__ == '__main__' ):
    real_main()


