#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/18/17"

from hagsc_lib import generateTmpDirName, Bzip_file, deleteDir

from gp_lib import jobFinished, create_sh_file, submitJob
from gp_lib import genePool_queue_limiter

from optparse import OptionParser

from os.path import isfile, isdir, join, splitext, split

from pacbio_lib import splitPBread_cmdGen, removeTriangleReads_cmdGen, create_tmp_dir

from sys import stdout, stderr

from os import getcwd

debug = False

#==============================================================
def real_main():
    
    # Defining the program options
    usage = "usage: %prog [Input FOFN] [options]"

    parser = OptionParser(usage)

    def_basesPerBatch = 20000000
    parser.add_option( '-B', \
                       "--basesPerBatch", \
                       type    = "int", \
                       help    = "Number of basepairs per core to analyze.  Default: %d"%def_basesPerBatch, \
                       default = def_basesPerBatch )

    def_jobsInQueue = 20
    parser.add_option( '-n', \
                       "--jobsInQueue", \
                       type    = "int", \
                       help    = "Number of jobs to be allowed into the queue at any one time.  Default: %d"%def_jobsInQueue, \
                       default = def_jobsInQueue )

    (options, args) = parser.parse_args()
    
    #======================================================================
    # Reading in the FOFN input files and checking to see if they all exist
    inputFOFN = args[0]
    if ( isfile(inputFOFN) ):
        inputFiles = []
        for item in open(inputFOFN):
            tmpFileName = item[:-1]
            if ( not isfile(tmpFileName) ):
                parser.error( 'FOFN FILE NOT FOUND: %s\nCHECK TO MAKE SURE ALL FILES IN THE FOFN EXIST!'%tmpFileName )
            #####
            inputFiles.append( tmpFileName )
        #####
    else:
        parser.error( 'INPUT FOFN FILE NOT FOUND: %s'%inputFOFN )
    #####
    
    #======================================================================
    # Setting values
    cwd           = getcwd()
    basesPerBatch = int(options.basesPerBatch)
    jobsInQueue   = int(options.jobsInQueue)
    supressOutput = False
    Time          = "12:00:00"
    Memory        = "10G"
    
    #======================================================================
    # Setting up the temporary directory
    tmpDirPath = join( cwd, generateTmpDirName() )
    if ( debug ): tmpDirPath = join( cwd, 'tmpDirectory' )  # For Debugging Purposes
    if ( not isdir(tmpDirPath) ): create_tmp_dir( tmpDirPath )
    
    #======================================================================
    # Splitting the reads
    if ( True ):
        stderr.write( "\t-Splitting FASTA Files:\n" )
        splitPBcmds = splitPBread_cmdGen( tmpDirPath, inputFiles, basesPerBatch )
        # Submitting the jobs locally
        jobID_set     = set()
        nCmds         = len(splitPBcmds)
        for n in xrange( nCmds ):
            cmdList    = [ splitPBcmds[n] ]
            baseName   = splitext(split(inputFiles[n])[-1])[0]
            shFileName = join( tmpDirPath, '%s_splitPBFile.sh'%baseName )
            create_sh_file( cmdList, Time, Memory, shFileName, supressOutput )
            cmdList.append( 'rm -f %s'%shFileName )
            jobID      = submitJob( shFileName )
            jobID_set.add( jobID )
        #####
        # Waiting for the jobs to finish
        jobFinished( jobID_set )
    #####
    
    #======================================================================
    # Removing Triangle Reads
    if ( True ):
        stderr.write( "\t-Identifying and removing triangle reads from data set...\n" )
        removeTriangleReadCmds = removeTriangleReads_cmdGen( tmpDirPath, inputFiles )
        # Submitting the jobs as an array
        jobID_set     = set()
        nCmds         = len(removeTriangleReadCmds)
        jobList       = []
        for n in xrange( nCmds ):
            tmpPre, cmd = removeTriangleReadCmds[n]
            cmdList    = [ cmd ]
            shFileName = join( tmpDirPath, '%s_removeTriangles.sh'%tmpPre )
            cmdList.append( 'rm -f %s'%shFileName )
            create_sh_file( cmdList, Time, Memory, shFileName, supressOutput )
            jobList.append( shFileName )
        #####
        genePool_queue_limiter( jobList, jobsInQueue )
    #####

    #======================================================================
    # Gathering it all back together
    if ( True ):
        stderr.write( "\t-Gathering it all back together\n" )
        for inputFile in inputFiles:
            baseName   = splitext(split(inputFile)[-1])[0]
            oh_LPNTR = Bzip_file( '%s_LPNTR.fasta.bz2' % baseName, 'w' )
            oh_tri   = Bzip_file( '%s_triangleReads.fasta.bz2' % baseName, 'w' )
            for line in open( join( tmpDirPath, 'splitFASTA_%s.dat'%baseName ) ):
                tmpFastaFile = line[:-1]
                baseID            = splitext(split(tmpFastaFile)[-1])[0]
                stderr.write( '\t%s\n'%baseID )
                # Adding to the triangle reads file
                triangleReadsFile = join( tmpDirPath, 'triangle_reads_%s.fasta'%baseID )
                map( oh_tri.write, open( triangleReadsFile, 'r' ) )
                # Adding to the LPNTR file
                LPNTR_File        = join( tmpDirPath, 'LPNTR_%s.fasta'%baseID )
                map( oh_LPNTR.write, open( LPNTR_File, 'r' ) )
            #####
            oh_LPNTR.close()
            oh_tri.close()
        #####
        # Removing the temporary directory
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir( tmpDirPath )
    #####
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
