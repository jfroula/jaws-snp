#!/usr/common/usg/languages/python/2.7-anaconda/bin/python3
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Aug 16, 2013$"

from os.path import isfile, abspath, curdir, join, isdir

from os import getpid, uname, mkdir, chdir, system

from optparse import OptionParser

from hapipe import cluster

# from hapipe.functional import mapV

from sys import stderr, stdout

from itertools import cycle

from collections import deque

from shutil import rmtree

import subprocess

# from time import time

#==============================================================
# lambda functions
terminate = lambda x: (not x) or (x[0] == '>')

#==============================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True

#==============================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True

#==============================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True

#==============================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )

#==============================================================
def createTmpDirectory():
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpDir   = generateTmpDirName()
    tmpPath  = join( basePath, tmpDir )
    deleteDir(tmpPath)
    mkdir(tmpDir, 0o777)
    testDirectory(tmpDir)
    return tmpDir, tmpPath, basePath

#==============================================================
def iterItems( tmpHandle ):
    # Stripped down iterator for reads files
    while True:
        line = tmpHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        splitLine = line[1:].split(None)
        id = splitLine[0]
        try:
            desc = ' '.join( splitLine[1:] )
        except IndexError:
            desc = ''
        #####
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( terminate(line) ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield {'id':id, 'data':''.join(tmpData), 'desc':desc}
        # Stopping the iteration
        if ( not line ):
            return
        ####
    #####
    assert False, 'Should never reach this line!!'

#=========================================================================
def writeRecord( tmpRec, oh ):
    wrap = 100
    nBases = len( tmpRec['data'] )
    oh.write('>%s %s\n'%(tmpRec['id'],tmpRec['desc']))
    for i in range( 0, nBases, wrap ):
        oh.write( tmpRec['data'][i:i+wrap] + '\n' )
    #####
    return True

#=========================================================================
def printAndExec( cmd, execute=True ):
    stderr.write( '%s\n'%cmd )
    try:
        if ( execute ): system( cmd )
    except KeyboardInterrupt:
        print( "KeyboardInterrupt detected, stopping the execution" )
        exit()
    #####
    return
    
#=========================================================================
def getCommand(cpu_n, basePath, tmpFasta, dbtype):
    # Setting up the run commands

    # SSH command
    sshCMD = 'ssh -a -n -x -e none -o UsePrivilegedPort=no -o ServerAliveInterval=20 %s "cd %s'%(cpu_n, basePath) 

    # Alignment filtering command
    # Filters for a bitscore of >=300, and then appends the dbtype to the end of the alignment
    alignmentFilter = "awk '{if (\$12>=300) print}' | sed 's/$/ %s/g'"

    # BLASTX Parameters
    # a = 7  enables up to 7 processors to be used for the blast
    # Q = 11 is the bacterial/archeal translation table
    # f = 14 is the neighborhood score to seed an alignment
    # W = 3  is the seed size for seeding alignments
    # -F 'm S' -U:  "m" means masking is on, "S" means we are using SEG to mask low complexity protein sequences, and -U means we are masking lower case letters.
    # e = 1 sets the upper bound on the E-value
    # m = 8 is the single line summary output format
    # b = 10000 truncates the report to this many alignments
    # v = 10000 number of database sequences to show
    parameterSet = "-p blastx -a 7 -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout"
    prok1CMD     = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/microbialDB_1 | %s"%(parameterSet,tmpFasta,alignmentFilter%'prok')
    prok2CMD     = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/microbialDB_2 | %s"%(parameterSet,tmpFasta,alignmentFilter%'prok')
    prok3CMD     = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/microbialDB_3 | %s"%(parameterSet,tmpFasta,alignmentFilter%'prok')
    prok4CMD     = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/microbialDB_4 | %s"%(parameterSet,tmpFasta,alignmentFilter%'prok')
    prok5CMD     = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/microbialDB_5 | %s"%(parameterSet,tmpFasta,alignmentFilter%'prok')
    mitoCMD      = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/mitochondrionDb | %s"%(parameterSet,tmpFasta,alignmentFilter%'mito')
    chloroCMD    = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/plastidDb | %s"%(parameterSet,tmpFasta,alignmentFilter%'chloro')
    fungCMD      = "/mnt/local/EXBIN/blastall %s -i %s -d /home/l3d43/screeningData/fungalProt | %s"%(parameterSet,tmpFasta,alignmentFilter%'fungal')
    # MEGABLAST Parameters
    # a = 7 enables up to 7 processors to be used for blast
    # b = 0 means show all alignments
    # f = T show full ID in the output
    # D = 3 essentially equivalent ot "-m 8" in blast
    rdnaCMD   = "/mnt/local/EXBIN/megablast -i %s -d /home/l3d43/screeningData/rdna.fasta -a 7 -b 0 -f T -D 3 -o stdout | %s"%(tmpFasta,alignmentFilter%'rdna')
    
    # Returning the appropriate command
    if ( dbtype == 'prok_1'   ): 
        return '%s ; %s"'%(sshCMD,prok1CMD)
    elif ( dbtype == 'prok_2'   ): 
        return '%s ; %s"'%(sshCMD,prok2CMD)
    elif ( dbtype == 'prok_3'   ): 
        return '%s ; %s"'%(sshCMD,prok3CMD)
    elif ( dbtype == 'prok_4'   ): 
        return '%s ; %s"'%(sshCMD,prok4CMD)
    elif ( dbtype == 'prok_5'   ): 
        return '%s ; %s"'%(sshCMD,prok5CMD)
    elif ( dbtype == 'mito'   ): 
        return '%s ; %s"'%(sshCMD,mitoCMD)
    elif ( dbtype == 'chloro' ):
        return '%s ; %s"'%(sshCMD,chloroCMD)
    elif ( dbtype == 'rdna'   ): 
        return '%s ; %s"'%(sshCMD,rdnaCMD)
    elif ( dbtype == 'fungal'   ): 
        return '%s ; %s"'%(sshCMD,fungCMD)
    else:
        assert False
    #####
    
#==========================================================================
def runCommand( cmdString, recID, dbtype ):
    process = subprocess.Popen( cmdString, shell=True, stdout=subprocess.PIPE )
    return  ([(line for line in process.stdout),  process, cmdString, recID, dbtype])

#==========================================================================
def buildJobList( dbList, cpu, tmpContigsFile, tmpPath, numProcs, noLengthScreen, minLength, maxLength ):
    nextCPU = cycle( range(numProcs) )
    for record in iterItems( open(tmpContigsFile) ):
        nBases = len(record['data'])
        if ( (not noLengthScreen) and ( (nBases<minLength) or (nBases>maxLength) ) ): continue        
        n = next( nextCPU )
        tmpFileName = join( tmpPath, 'tmpFile_%d.fasta'%n )
        oh = open( tmpFileName, 'w' )
        writeRecord( record, oh )
        oh.close()
        # Running the 4 tests
        for dbtype in dbList:
            cmd = getCommand( cpu[n], tmpPath, tmpFileName, dbtype )
            yield runCommand( cmd, record['id'], dbtype )
        #####
    #####

#=========================================================================    
class intervalClass(object):
    def __init__(self,start,end,fragID):
        start, end                = sorted([start,end])
        self.intervalDict         = {}
        self.intervalDict[fragID] = [(start,end)]
        self.fragID               = fragID

    def addInterval(self,start,end,fragID):
        start, end = sorted([start,end])
        try:
            self.intervalDict[fragID].append( (start,end) )
            self.collapseIntervals(fragID)
        except KeyError:
            self.intervalDict[fragID] = [(start,end)]
        #####
        return
        
    def isOverlapped(self,s1,e1,s2,e2):
        return not ( (s2>e1) or (s1>e2) )

    def collapseIntervals(self,fragID):
        notDone = True
        while (notDone):
            notDone    = False
            nIntervals = len( self.intervalDict[fragID] )
            intervals   = self.intervalDict[fragID]
            for i in range(nIntervals-1):
                s1, e1 = intervals[i]
                for j in range( (i+1), nIntervals ):
                    s2, e2 = intervals[j]
                    if ( self.isOverlapped(s1,e1,s2,e2) ):
                        # Compute the new bound
                        sortedBounds = sorted([s1,e1,s2,e2])
                        ns = sortedBounds[0]
                        ne = sortedBounds[-1]
                        # Collapsing the intervals
                        self.intervalDict[fragID][i] = (ns,ne)
                        self.intervalDict[fragID].pop(j)
                        notDone = True
                        break
                    #####
                #####
                if ( notDone ): break
            #####
        #####
        return

    def computeComposition(self,totalBases):
        #try:
        #    alignBases = sum( [ (end-start+1) for start,end in self.intervalDict.values() ] )
        #except ValueError:
        alignBases = sum( [ (end-start+1) for start,end in list(self.intervalDict[self.fragID]) ])
        #####
        return 100.0 * float(alignBases) / float( totalBases )
    #####        
    
#=========================================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA FILE] [OUTPUT FILE] [options]"

    parser = OptionParser(usage)

    defClutser = 'ssd'
    parser.add_option( "-c", \
                       "--cluster", \
                       type    = 'str', \
                       help    = "Cluster to run the blastxs on.  Default: %s"%defClutser, \
                       default = defClutser )
                       
    numProcs = 10
    parser.add_option( "-p", \
                       "--numProcs", \
                       type    = 'int', \
                       help    = "Number of pc's to use.  Default: %d"%numProcs, \
                       default = numProcs )
                       
    parser.add_option( '-f', \
                       "--prokScreen", \
                       action  = "store_true", \
                       dest    = 'prokScreen', \
                       help    = "Perform the prokaryote protein screen.  Default:  No prokaryotic screen." )
    parser.set_defaults( prokScreen = False )

    parser.add_option( '-s', \
                       "--smallProkScreen", \
                       action  = "store_true", \
                       dest    = 'smallProkScreen', \
                       help    = "Perform the prokaryote protein screen with 20% of the full prokaryote protein screen.  Default:  No prokaryotic screen." )
    parser.set_defaults( smallProkScreen = False )


    parser.add_option( '-L', \
                       "--noLengthScreen", \
                       action  = "store_true", \
                       dest    = 'noLengthScreen', \
                       help    = "Do not screen contigs for length.  Default:  1,000bp <= contig <= 100,000bp" )
    parser.set_defaults( noLengthScreen = False )

    parser.add_option( '-R', \
                       "--rDNA_only", \
                       action  = "store_true", \
                       dest    = 'rDNA_only', \
                       help    = "Only screen rDNA.  Default: Screen rdna, chloro, and mito" )
    parser.set_defaults( rDNA_only = False )

    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )

    else:
        fastaFile = abspath( args[0] )
        testFile( fastaFile )
        outputFile = abspath( args[1] )
        currentCluster = options.cluster
        numProcs       = options.numProcs
    #####
    
    #=================================================================
    # Creating a temporary directory
    tmpDir, tmpPath, basePath = createTmpDirectory()
    chdir( tmpPath )
    
    # Break into contigs
    tmpContigsFile = join( tmpPath, 'tmp.contigs.fasta' )
    printAndExec( 'breakIntoContigs.py %s %s'%(fastaFile,tmpContigsFile) )
    
    minLength = 1000
    maxLength = 100000
    
    # Computing the blasted bases
    totalBases = {}
    gcDict     = {}
    for record in iterItems( open(tmpContigsFile) ):
        scaffID, contigID = record['id'].split('|')
        nBases = len(record['data'])
        GC     = record['data'].upper().count('G') + record['data'].upper().count('C')
        if ( (not options.noLengthScreen) and ((nBases<minLength) or (nBases>maxLength)) ): continue
        try:
            totalBases[scaffID] += len(record['data'])
        except KeyError:
            totalBases[scaffID] = len(record['data'])
        #####
        try:
            gcDict[scaffID] += GC
        except KeyError:
            gcDict[scaffID] = GC
        #####
    #####
    for scaffID in gcDict.keys():  gcDict[scaffID] = 100.0 * float(gcDict[scaffID]) / float(totalBases[scaffID])
    
    # Build the commands
    stderr.write( '\tSTARTING THE BLAST\n' )
    cpu    = cluster.acquire_cpus( currentCluster, numProcs )
    if ( options.rDNA_only ):
        dbList = ['rdna']
    else:
        dbList = ['rdna', 'mito', 'chloro', 'fungal']
        if (options.smallProkScreen): dbList = dbList + ['prok_1']
        if ( options.prokScreen )   : dbList = dbList + ['prok_1','prok_2','prok_3','prok_4','prok_5']
    #####
    
    # Setting up the job iterator
    jobList = buildJobList( dbList, cpu, tmpContigsFile, tmpPath, numProcs, options.noLengthScreen, minLength, maxLength )
    
    # Writing the header of the output file
    oh_report = open(outputFile, 'w')
    mainString = ['scaffID']
    mainString.append( 'screenedBases' )
    mainString.append( 'GC_per' )
    perStr = '%'
    for item in dbList: mainString.append( r'%s%s'%(item,perStr) )
    for item in dbList: mainString.append( 'top_%s'%item )
    underline = [len(item)*'-' for item in mainString]
    oh_report.write( '%s\n'%('\t'.join(mainString)) )
    oh_report.write( '%s\n'%('\t'.join(underline)) )
    oh_report.close()
    
    # Parse the output
    intervalDict = {}
    bestHitDict  = {}
    runningJobs  = deque( next(jobList) for n in range(numProcs) )
    currentScaff = None
    breakMe      = False
    while True: 
        # Waiting on the job to finish
        try:
            # Pull the output and the process
            x = runningJobs.popleft()
        except IndexError:
            breakMe = True
        #####
        # Resetting the first scaffold
        if ( currentScaff == None ): currentScaff = x[3].split('|')[0]
        # Performing the analysis
        for line in map( bytes.decode, x[1].stdout ):
            # Screening out the comment lines
            if ( line[0] == "#" ): continue
            # Splitting the line
            splitLine = line.split(None)
            if ( splitLine == [] ): continue
            # Pulling off the information
            try:
                fragID, hitID, perID, alignLength, mismatch, gaps, qStart, qStop, tStart, tStop, eValue, score, dbType = splitLine
            except ValueError:
                continue
            #####
            # Splitting the fragment ID
            scaffID, contigID = fragID.split('|')
            # Adding the interval to the class
            if ( scaffID not in intervalDict ): intervalDict[scaffID] = {}
            try:
                intervalDict[scaffID][dbType.split('_')[0]].addInterval( int(qStart), int(qStop), fragID )
            except KeyError:
                intervalDict[scaffID][dbType.split('_')[0]] = intervalClass( int(qStart), int(qStop), fragID )
                #intervalDict[scaffID][dbType.split('_')[0]].addInterval( int(qStart), int(qStop), fragID )
            #####
            # Incrementing the best hit dictionary
            score = float(score)
            if ( scaffID not in bestHitDict ): bestHitDict[scaffID] = {}
            try:
                if ( score > bestHitDict[scaffID][dbType][0] ): bestHitDict[scaffID][dbType] = (score,hitID)
            except KeyError:
                bestHitDict[scaffID][dbType] = (score,hitID)
            #####
        #####
        # Popping the run and launching the next job
        x[1].poll()
        try:
            runningJobs.append( next(jobList) )
        except StopIteration:
            pass
        #####
        # Write out the scaffold information when we have a change in scaffold
        if ( (currentScaff != x[3].split('|')[0]) or (breakMe ==True)):
            # ScaffID
            outputString = [currentScaff]
            # Screened Bases
            outputString.append(str(totalBases[currentScaff]))
            # GC content
            outputString.append('%.2f'%(gcDict[currentScaff]))
            try:
                tmpDict      = intervalDict[currentScaff]
                for dbType in dbList:
                    try:
                        alignedPer = tmpDict[dbType].computeComposition(totalBases[currentScaff])
                        outputString.append( '%.2f'%alignedPer )
                    except KeyError:
                        outputString.append( '0.0' )
                    #####
                #####
                for dbType in dbList:
                    try:
                        outputString.append( bestHitDict[currentScaff][dbType][1] )
                    except KeyError:
                        outputString.append( 'None' )
                    #####
                #####
            except KeyError:
                outputString.extend( len(dbList)*['0.0'] )
                outputString.extend( len(dbList)*['None'] )
            #####
            oh_report = open(outputFile, 'a')
            oh_report.write( '%s\n'%( '\t'.join(outputString) ) )
            oh_report.close()
            try:
                intervalDict.pop(currentScaff)
            except KeyError:
                pass
            #####
            try:
                bestHitDict.pop(currentScaff)
            except KeyError:
                pass
            #####
            stderr.write( '%s completed\n--------------\n'%(currentScaff) )
            # Resetting the current scaffold
            currentScaff = x[3].split('|')[0]
        #####
        if (breakMe): break
    #####
    
    # Back into the working directory
    chdir( basePath )
    
    # Taking out the trash
    deleteDir( tmpPath )
    
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()
