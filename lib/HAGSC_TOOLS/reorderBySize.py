#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Oct 6, 2010 10:21:46 AM$"

# Local Library Imports
from hagsc_lib import reorder_by_Size

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

def real_main():

    # Defining the program options
    usage = "usage: %prog [fastaFile]"

    parser = OptionParser(usage)

    parser.add_option( "-q", \
                       "--qualFile", \
                       type    = 'string', \
                       help    = "Qual file, Default: None", \
                       default = None )

    parser.add_option( "-i", \
                       "--startIndex", \
                       type    = 'int', \
                       help    = "Start index for supers, Default: 0", \
                       default = 0 )

    parser.add_option( "-s", \
                       "--identifierString", \
                       type    = 'string', \
                       help    = "Scaffold identifier, Default: super", \
                       default = 'super' )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) != 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Testing the fasta file
        if ( not isfile(args[0]) ):
            parser.error( 'File not found  %s'%args[0] )
        #####
        fastaFile = args[0]
    #####

    if ( options.startIndex < 0):
        parser.error( 'ID% must be > 0' )
    #####

    # Pushing the information to an underlying function
    reorder_by_Size( fastaFile, \
                     qualFile=options.qualFile, \
                     startIndex=options.startIndex, \
                     scaffold_ID=options.identifierString )

    return

if ( __name__ == '__main__' ):
    real_main()

