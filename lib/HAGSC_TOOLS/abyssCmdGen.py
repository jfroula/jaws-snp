#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott"
__date__ ="$July 25, 2013$"
from optparse import OptionParser
from os.path import realpath

#==============================================================
def real_main():
        
    usage = "usage: %prog [NAME] [LIBS(comma separated)] [DIRECTORY] [options]"
    
    parser = OptionParser(usage)
    
    defKVal = 56
    parser.add_option( "-k", \
                       "--kValue", \
                       type    = 'int', \
                       help    = "k value to use in the assembly.  Default: %d"%defKVal, \
                       default = defKVal )
    defNVal = 5                   
    parser.add_option( "-n", \
                       "--nValue", \
                       type    = 'int', \
                       help    = "n value to use in the assembly.  Default: %d"%defNVal, \
                       default = defNVal )
                       
    parser.add_option( "-p", \
                       "--pairLibs", \
                       type    = 'str', \
                       help    = "Comma separated list of the pair libraries  to use.",\
                       default = False )
                       
    parser.add_option( "-d", \
                       "--assemDir", \
                       type    = 'str', \
                       help    = "The name of the assembly directory. Default is k50",\
                       default = 'k56' )
    
    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    
    else:
        # set the variables
        orgName  = args[0]
        workDir  = realpath(args[2])
        assemDir = realpath(options.assemDir)
        kValue   = str(options.kValue)
        LIBS     = args[1].split(',')
        libStr   = ' '.join(LIBS)
        libFiles = ''
        nValue   = options.nValue
        for lib in LIBS: libFiles = libFiles + " %s='%s/%s.R1.fasta.bz2 %s/%s.R2.fasta.bz2'"%(lib, workDir, lib, workDir, lib)
    #####
    
    if (options.pairLibs):
        MP      = options.pairLibs.split(',')
        mpStr   = ' '.join(MP)
        mpFiles = ''
        for lib in MP: libFiles = libFiles + " %s='%s/%s.R1.fasta.bz2 %s/%s.R2.fasta.bz2'"%(lib, workDir, lib, workDir, lib)
        cmd = "rm -r %s; mkdir %s; /opt/abyss-1.3.7/bin/abyss-pe -j 6 -C %s k=%s n=%d name=%s lib='%s' mp='%s' %s %s | tee abyss_k%s_%s_assembly.out"%(assemDir, assemDir,assemDir, kValue, nValue,orgName, libStr, mpStr, libFiles, mpFiles,  kValue, orgName)
    else:
        cmd = "rm -r %s; mkdir %s; /opt/abyss-1.3.7/bin/abyss-pe -j 6 -C %s k=%s n=%d name=%s lib='%s' %s  | tee abyss_k%s_%s_assembly.out"%(assemDir, assemDir,assemDir, kValue, nValue,orgName, libStr, libFiles, kValue, orgName)
    #####
    print cmd
    oh = open('abyssCommand.dat', 'w')
    oh.write(cmd)
    oh.close
#==============================================================    
if ( __name__ == '__main__' ):
    real_main()