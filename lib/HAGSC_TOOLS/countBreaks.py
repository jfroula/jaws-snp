#!/usr/common/usg/languages/python/2.7-anaconda/bin/python
__author__="jjenkins"
__date__ ="10-November-2014"

# Local Library Imports
from hagsc_lib import iterFASTA

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout, stdin, stderr

import re

#==============================
def checkFile( fileName, parser ):
    if ( not isfile(fileName) ): 
        parser.error( '%s can not be found'%fileName )
    else:
        return fileName
    #####

#===============
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [options]"

    parser = OptionParser(usage)

    parser.add_option( "-E", \
                       "--eachSeparately", \
                       action  = 'store_true', \
                       dest    = 'eachSeparately', \
                       help    = "Enumerate gaps on each scaffold:  Default=False" )
    parser.set_defaults( eachSeparately = False )

    parser.add_option( "-S", \
                       "--showBreaks", \
                       action  = 'store_true', \
                       dest    = 'showBreaks', \
                       help    = "Enumerate gaps on each scaffold:  Default=False" )
    parser.set_defaults( showBreaks = False )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        if ( args[0] == 'stdin' ):
            FASTA_inputFile = 'stdin'
        else:
            FASTA_inputFile = checkFile( args[0], parser )
        #####
    #####
    
    # Regular expression for finding breaks
    contigBreakFinder = re.compile( r'[ACTGactg]{1}(N{1,})[ACTGactg]{1}' ).findall
    
    # Parsing the output
    if ( FASTA_inputFile == 'stdin' ): 
        FASTA_oh = stdin
    else:
        FASTA_oh = open( FASTA_inputFile )
    #####

    stderr.write( '\t-Counting Breaks...\n')
    if ( options.showBreaks ):
        contigBreakFinder = re.compile( r'[ACTGactg]{1}([Nn]{5,})[ACTGactg]{1}' ).finditer
        for r in iterFASTA( FASTA_oh ):
            for item in contigBreakFinder(str(r.seq)):
                start, end = item.span()
                start += 1
                end   -= 1
                stdout.write( '%s\t%d\t%d\n'%(r.id,start,end) )
            #####
        #####
    else:
        if ( options.eachSeparately ):
            for r in iterFASTA( FASTA_oh ):
                nBreaks = len(contigBreakFinder(str(r.seq)))    
                stdout.write( '%s\t%d\n'%(r.id,nBreaks) )
            #####
        else:
            nBreaks = sum( [len(contigBreakFinder(str(r.seq))) for r in iterFASTA( FASTA_oh )] )
            stdout.write( 'TotalBreaks\t%d\n'%nBreaks)
        #####
    #####
    
    if ( FASTA_inputFile != 'stdin' ): FASTA_oh.close()
    
#####

if ( __name__ == '__main__' ):
    real_main()


