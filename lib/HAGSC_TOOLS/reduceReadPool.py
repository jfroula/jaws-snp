#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="$Nov 18, 2010 10:37:47 PM$"

# HAGSC Library Imports
from hagsc_lib import reduceReadPool

# Python Library Imports
from os.path import realpath
from os.path import isfile

from optparse import OptionParser

from sys import stderr

#===============
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA file] [QUAL file] [options]"

    parser = OptionParser(usage)

    targetCoverage = '30'
    parser.add_option( "-g", \
                       "--targetCoverage", \
                       type    = 'string', \
                       help    = "Target coverage (x) for final set  Default=%s"%targetCoverage, \
                       default = targetCoverage )

    minKmerFreq = 2
    parser.add_option( "-L", \
                       "--minKmerFreq", \
                       type    = 'int', \
                       help    = "Minimum kmer freq used to score reads.  Default=%d\n"%minKmerFreq, \
                       default = minKmerFreq )

    maxKmerFreq = -1
    parser.add_option( "-U", \
                       "--maxKmerFreq", \
                       type    = 'int', \
                       help    = "Maximum kmer freq used to score reads.  Default=No maximum", \
                       default = maxKmerFreq )

    peakFreq = -1
    parser.add_option( "-P", \
                       "--peakFreq", \
                       type    = 'int', \
                       help    = "Coverage level in initial set as estimated from kmer histogram.  Default is automated selection.", \
                       default = peakFreq )

    FWHM = -1
    parser.add_option( "-F", \
                       "--FWHM", \
                       type    = 'int', \
                       help    = "Full width at half maximum of the kmer peak.  Use this with the -P option for broad distributions.  Default is automated selection.", \
                       default = FWHM )

    numKmers = '1000m'
    parser.add_option( "-z", \
                       "--numKmers", \
                       type    = 'str', \
                       help    = "Number of possible n-mers (k, m, or g suffix).  Used in generating kmer histogram.  Default=%s"%numKmers, \
                       default = numKmers )

    merLength = 24
    parser.add_option( "-m", \
                       "--merLength", \
                       type    = 'int', \
                       help    = "Mer length.  Used in generating kmer histogram.  Default=%d"%merLength, \
                       default = merLength )

    parser.add_option( "-T", \
                       "--arachneReadNames", \
                       action  = 'store_true', \
                       dest    = 'arachneReadNames', \
                       help    = "Use ARACHNE style read naming:  Default=False" )
    parser.set_defaults( arachneReadNames = False )

    parser.add_option( "-A", \
                       "--assemblyDirectory", \
                       action  = 'store_true', \
                       dest    = 'assemblyDirectory', \
                       help    = "Setup an assembly directory:  Default=False" )
    parser.set_defaults( assemblyDirectory = False )

    parser.add_option( "-K", \
                       "--keepReadNames", \
                       action  = 'store_true', \
                       dest    = 'keepReadNames', \
                       help    = "Do not modify read names:  Default=Treats as illumina and adds -R1/-R2 and description to reads." )
    parser.set_defaults( keepReadNames = False )


    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fasta file
        readsFASTA = realpath(args[0])
        if ( not isfile(readsFASTA) ):
            parser.error( 'Reads File:  %s is not found.'%readsFASTA )
        #####
        # Pulling the qual file
        qualFile = realpath(args[1])
        if ( not isfile(qualFile) ):
            parser.error( 'FASTA File:  %s is not found.'%qualFile )
        #####
    #####
   
    # Calling the object 
    reduceReadPool( readsFASTA, \
                    qualFile, \
                    options.targetCoverage, \
                    options.arachneReadNames, \
                    options.numKmers, \
                    options.merLength, \
                    options.minKmerFreq, \
                    options.maxKmerFreq, \
                    options.peakFreq, \
                    options.assemblyDirectory, \
                    options.FWHM, \
                    options.keepReadNames )

#####

#=============================
if ( __name__ == '__main__' ):
    pass
    real_main()
#####

