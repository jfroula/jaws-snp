#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  3/4/15"

from hagsc_lib import isGzipFile, isBzipFile

from os.path import isfile, realpath

from os import system

from sys import stdout

from optparse import OptionParser

#==============================================================
def printAndExecute( cmd, execute=True ):
    stdout.write( "%s\n"%cmd )
    if ( execute ): system( cmd )
    return 

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTQ/FASTA] [LIB_ID] [options]"

    parser = OptionParser(usage)

    indexedFASTA = "-1"
    parser.add_option( '-i', \
                       "--indexedFASTA", \
                       type    = "str", \
                       help    = "Name of the indexed FASTA  Default: No alignments", \
                       default = indexedFASTA )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 2 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTQ = realpath(args[0])
        if ( not isfile(FASTQ) ): parser.error( '%s can not be found'%FASTQ )

        LIB_ID = args[1]
        
        INDEX = options.indexedFASTA
        
    #####
    cmd = '/mnt/local/EXBIN/extract_seq_and_qual -b %s | awk -f /mnt/raid2/SEQ/sharedPythonLibrary/FASTQ_split.awk OUTBASE=%s INDEX=%s'%(FASTQ,LIB_ID,INDEX)
    printAndExecute( cmd )
    return

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
