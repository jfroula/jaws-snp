#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="12-April-2011"

# Local Library Imports
from hagsc_lib import histogramClass

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout
from sys import stderr
from sys import stdin

def real_main():

    # Defining the program options
    usage = "usage: %prog [Data file name] [options]"

    parser = OptionParser(usage)

    parser.add_option( "-l", \
                       "--lowerBound", \
                       type    = 'float', \
                       help    = "Histogram lower bound.  Default: 10% below lowest value.", \
                       default = None )

    parser.add_option( "-u", \
                       "--upperBound", \
                       type    = 'float', \
                       help    = "Histogram upper bound.  Default: 10% above highest value.", \
                       default = None )

    defaultBins = 100
    parser.add_option( "-b", \
                       "--numBins", \
                       type    = 'int', \
                       help    = "Number of bins in the histogram.  Default: %d bins"%defaultBins, \
                       default = defaultBins )

    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        dataFile = args[0]
        if ( dataFile == 'stdin' ):
            oh = stdin
        else:
            if ( not isfile(dataFile) ): parser.error( '%s can not be found'%dataFile )
            oh = open(dataFile,'r')
        #####
    #####
    
    # Creating the histogram
    tmpData = []
    for line in oh:
        try:
            tmpData.append( float(line.strip()) )
        except ValueError:
            stdout.write( 'IGNORING LINE:\n%s----\n'%line )
        #####
    #####
    oh.close()
    
    minValue = min(tmpData)
    if ( options.lowerBound == None ): 
        if ( minValue > 0 ):
            lowerBound = 0.90 * minValue
        else: 
            lowerBound = 1.10 * minValue
        #####
    else:
        lowerBound = options.lowerBound
    #####

    maxValue = max(tmpData)
    if ( options.upperBound == None ): 
        if ( maxValue > 0 ):
            upperBound = 1.10 * maxValue
        else: 
            upperBound = 0.90 * maxValue
        #####
    else:
        upperBound = options.upperBound
    #####

    # Make the histogram    
    histClass = histogramClass( lowerBound, upperBound, options.numBins )
    
    # Filter the data if possible
    filterFun = lambda x: (lowerBound <= x <= upperBound)
    map( histClass.addData, filter( filterFun, tmpData ) )
    stdout.write( histClass.generateOutputString() )
    return
    
#####

if ( __name__ == '__main__' ):
    real_main()


