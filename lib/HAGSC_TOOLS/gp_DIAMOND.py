#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/24/15"

from hagsc_lib import equal_size_split, baseFileName, getFiles
from hagsc_lib import testFile, testDirectory
from hagsc_lib import iterFASTA, writeFASTA
from hagsc_lib import deleteDir, deleteFile
from hagsc_lib import generateTmpDirName

from os.path import join, abspath, curdir,  realpath, isfile

from gp_lib import create_sh_file, submitJob

from optparse import OptionParser

from sys import stderr, stdout

from os import mkdir, system

#==============================================================
class setup_base_class(object):

    def __init__(self, fastaFile, \
                       nFastaFiles, \
                       outputFileName=None ):

        # Make sure the files exist
        testFile(fastaFile)

        # Reading in the control variables
        self.fastaFile         = fastaFile
        self.nFastaFiles       = nFastaFiles

        # Generating the directory information
        self.basePath = abspath(curdir)

        self.tmpDir   = generateTmpDirName()
        self.tmpPath  = join( self.basePath, self.tmpDir )

        # Generating the base output file name
        self.baseOutFileName = join( self.tmpPath, \
                                     baseFileName(self.fastaFile) )

    def create_tmp_dir(self):
        stderr.write( '\n\t-Creating tmp directory\n')
        deleteDir(self.tmpPath)
        mkdir(self.tmpDir, 0777)
        testDirectory(self.tmpDir)
        return

    def splitFASTA(self):
        stderr.write( '\t-Splitting FASTA file\n')
        equal_size_split( self.fastaFile, \
                          self.baseOutFileName, \
                          self.nFastaFiles, \
                          arachneReadFormat=False, \
                          noSort=True )
        return

    def clean_tmp(self):
        stderr.write( '\t-Removing Temporary directory\n')
        deleteDir(self.tmpPath)
        return

#==============================================================
class main_run_class( setup_base_class ):
    def __init__(self, queryFile, \
                       targetFile, \
                       nFastaFiles, \
                       outputFileName, \
                       blastParameters, \
                       blastType, \
                       memory, \
                       runTime ):

        ## Initializing the best hit base class
        setup_base_class.__init__( self, \
                                   queryFile, \
                                   nFastaFiles )
        
        # Reading in the information
        self.queryFile       = queryFile
        self.targetFile      = targetFile
        self.outputFileName  = outputFileName
        self.blastParameters = blastParameters
        self.blastType       = blastType
        self.memory          = memory
        self.runTime         = runTime

    def executeCases(self):

        # Step 1:  Create temporary directory
        self.create_tmp_dir()

        # Step 2:  Break the fasta file into little pieces
        self.splitFASTA()
        
        # Step 4:  BLAT Fastas
        self.sendToCluster()
        
#         # Step 5:  Removing the tmp directory
#         self.clean_tmp()

        return

    def sendToCluster(self):
    
        stderr.write( '\t-BLASTing Reads\n')
        
        # Setting up the sh file parameters
        supressOutput = False
        Time          = self.runTime
        Memory        = self.memory
        
        # Setting up the front command
        frontCommand  = '/global/dna/projectdirs/plant/geneAtlas/TEST_DMND/diamond/diamond %s %s' % ( self.blastType, self.blastParameters )
        
        # Writing the job IDs to a file
        job_ID_File = join( self.tmpPath, 'jobIDs.dat')
        oh_jobID = open( job_ID_File, 'w' )
        
        # Setting the final parse filename
        finalParse_sh = join( self.tmpPath, 'finalParse.sh' )

        # Generating and submitting the jobs
        queryList = [queryFile for queryFile in getFiles( 'fasta', self.tmpPath )]
        jobList   = []
        nQueries  = len(queryList)
        for n in xrange( nQueries ):

            queryFile = queryList[n]
            testFile( queryFile )

            localOutputFile = join( self.tmpPath, '%s.blast'%queryFile )
            
            cmdList = []
            cmdList.append( '%s -q %s -d %s | gzip -c > %s.gz'%( frontCommand, queryFile, self.targetFile, localOutputFile ) )
            
            # Creating the sh file name
            shFileName = join( self.tmpPath, "gp_DIAMOND_%d.sh"%n )

            # Adding the job to a file
            if ( n == (nQueries - 1) ):
                oh_jobID.close()
                cmdList.append( 'python -c \"from gp_lib import jobFinished;jobFinished(set([line[:-1] for line in open(\'%s\')]))\"'%job_ID_File )
                cmdList.append( 'sbatch %s' % finalParse_sh )
                create_sh_file( cmdList, Time, Memory, shFileName, supressOutput )
                submitJob( shFileName )
            else:
                create_sh_file( cmdList, Time, Memory, shFileName, supressOutput )
                oh_jobID.write( '%s\n' % submitJob( shFileName ) )
            #####
            
        #####
        
        # Writing the parsing script
        cmdList = ['python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/parse_gp_BLAT_output.py %s %s'%(self.tmpPath, self.outputFileName ) ]
#         cmdList.append ( 'rm -rf %s' % self.tmpPath )
        create_sh_file( cmdList, Time, Memory, finalParse_sh, supressOutput )
        
        return

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [query fasta] [target fasta] [output file] [options]"

    parser = OptionParser(usage)

    blastType = "blastx"
    parser.add_option( '-x', \
                       "--blastType", \
                       type    = "str", \
                       help    = "BLAST type.  Default: %s"%blastType, \
                       default = blastType )

    blastParameters = "--sensitive --evalue 1e-03 --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qframe sframe"
    parser.add_option( '-p', \
                       "--blastParameters", \
                       type    = "str", \
                       help    = "BLAST parameters.  Default: %s"%blastParameters, \
                       default = blastParameters )

    nJobs = 50
    parser.add_option( '-n', \
                       "--nJobs", \
                       type    = "int", \
                       help    = "Number of jobs on the cluster.  Default: %d"%nJobs, \
                       default = nJobs )

    memory = "10G"
    parser.add_option( '-m', \
                       "--memory", \
                       type    = "str", \
                       help    = "Memory per job.  Default: %s"%memory, \
                       default = memory )

    runTime = "11:00:00"
    parser.add_option( '-t', \
                       "--runTime", \
                       type    = "str", \
                       help    = "Max time per job.  Default: %s"%runTime, \
                       default = runTime )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        queryFASTA = realpath(args[0])
        if ( not isfile(queryFASTA) ): parser.error( '%s can not be found'%queryFASTA )

        targetFASTA = realpath(args[1])
        if ( not isfile(targetFASTA) ): parser.error( '%s can not be found'%targetFASTA )

        outputFileName = realpath(args[2])
    #####

    # Controlling parameters
    main_run_class( queryFASTA, \
                    targetFASTA, \
                    options.nJobs, \
                    outputFileName, \
                    options.blastParameters, \
                    options.blastType, \
                    options.memory, \
                    options.runTime ).executeCases()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
