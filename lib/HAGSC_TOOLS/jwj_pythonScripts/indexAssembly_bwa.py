#!/usr/common/usg/languages/python/2.7-anaconda/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/24/15"

from optparse import OptionParser

from gp_lib import genePool_submitter

from os.path import isfile, realpath, abspath, curdir, join, split

from os import system

#==============================================================
def IndexingFASTA( fastaFile, index_ID, Hours, Memory, basePath, runLocally ):
    
    # Relevant command list
    cmdList = ["source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV/"]
    cmdList.append( "bwa index -a bwtsw -p %s %s\n"%( index_ID, fastaFile ) )
    
    if ( runLocally ):
        for cmd in cmdList: print cmd
    else:
        # Running the job
        maxTime   = "%d:00:00"%Hours
        maxMemory = Memory
        fastaBase = '.'.join( split(fastaFile)[-1].split('.')[:-1] )
        sh_File   = join( basePath, 'indexFASTA.%s.sh' % fastaBase )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=True, delete_sh_file=False, waitUntilDone=False )
    #####
    
#==============================================================
def real_main():
    
    # Defining the program options
    usage = "usage: %prog [FASTA File] [bwa INDEX] [options]"

    parser = OptionParser(usage)

    runTime_def = 1
    parser.add_option( '-t', \
                       "--runTime_hours", \
                       type    = "int", \
                       help    = "Maximum run time in hours.  Default: %d hours"%runTime_def, \
                       default = runTime_def )

    memory_def = '1G'
    parser.add_option( '-m', \
                       "--memory_GB", \
                       type    = "str", \
                       help    = "Maximum memory.  Default: %s"%memory_def, \
                       default = memory_def )
    
    parser.add_option( "-L", \
                       "--runLocally", \
                       action  = 'store_true', \
                       dest    = 'runLocally', \
                       help    = "Perform the indexing on the local machine:  Default=False" )
    parser.set_defaults( runLocally = False )

    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:

        FASTA = realpath( args[0] )
        if ( not isfile( FASTA ) ):
            writeAndEcho( 'Fasta file not found %s'%FASTA )
        #####
        basePath = abspath( curdir )
        INDEX = join( basePath, args[1] )
    #####
    
    # Indexing the assembly
    IndexingFASTA( FASTA, INDEX, options.runTime_hours, options.memory_GB, abspath( curdir ), options.runLocally )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
