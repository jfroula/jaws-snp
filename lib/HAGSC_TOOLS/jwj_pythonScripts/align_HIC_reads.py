#!/usr/common/usg/languages/python/2.7-anaconda/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from hic_lib import parseConfigFile, writeAndEcho, throwError, jobTypeSet, runPath

from gp_lib import genePool_submitter

from optparse import OptionParser

from os.path import abspath, join

from os import curdir, system

from sys import stdout

#==============================================================
def real_main():
    
    # Defining the program options
    usage  = "usage: %prog [options]"

    parser = OptionParser(usage)

    restartJob_def = 'align_and_split'
    parser.add_option( "-s", \
                       "--restartJob", \
                       type    = 'str', \
                       help    = "Start at this point in the pipeline.  Default: %s"%restartJob_def, \
                       default = restartJob_def )

    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Finding the base path
    basePath = abspath( curdir )
    
    # Setting the config file name
    configFile = join( basePath, 'hic.config' )
    
    # Read the config file
    configDict, cmd_log = parseConfigFile( configFile, basePath )
    
    # Creating the initial BAM file to start the compuation off
    if ( options.restartJob not in jobTypeSet ):
        throwError( '%s is not a valid step in the SNP/INDEL calling pipeline'%options.restartJob )
    #####
    nextStep = options.restartJob
    
    # Checking for interleaved reads or not
    if ( len(configDict['READS']) == 2 ): throwError("ALIGNER CAN ONLY ACCEPT INTERLEAVED READS")
    
    # Building the command set
    cmdList = ['python %s/HiC_read_aligner/HiC_job_launcher.py %s %s %s'%( runPath, basePath, configFile, nextStep) ]
    
    # Writing to the log file
    writeAndEcho( '\n-----------------------\n- Starting the computation:', cmd_log )
    for cmd in cmdList: writeAndEcho( cmd, cmd_log )
    
    # Running the jobs locally
    for cmd in cmdList: system( cmd )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
    