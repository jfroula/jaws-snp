__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="1/29/18$"

from sys import stdin, stdout

#==============================================================
def real_main():
    
    testDict = {}
    prevID   = None
    for line in stdin:
        if ( line[0] == "@" ):
            stdout.write( line )
            continue
        #####
        readID = line.split(None)[0]
        try:
            testDict[readID]
            continue
        except KeyError:
            testDict[readID] = None
            if ( prevID != None ): testDict.pop( prevID )
            stdout.write( line )
        #####
        prevID = readID
    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
