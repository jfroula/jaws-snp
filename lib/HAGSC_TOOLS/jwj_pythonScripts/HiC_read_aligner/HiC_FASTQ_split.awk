#=============================================
BEGIN{
    
    # Initializing the breaking up
    nPairs       = 0
    fileNumber   = 1

    fastqFile_R1 = basePath"/"RUN_ID"."fileNumber".R1.fastq.gz"
    system( "rm "fastqFile_R1 )

    fastqFile_R2 = basePath"/"RUN_ID"."fileNumber".R2.fastq.gz"
    system( "rm "fastqFile_R2 )
    
}

#=============================================
function exists( file, line )
{
    if ( ( getline line < file ) == -1 )
    {
            return 0
    }
    else {
            close(file)
            return 1
    }
}

#=============================================
function write_sh_file( shFile, fastqFile, outputBAM_File )
{
    # Writing the body of the sh file
    print "#!/bin/bash" > shFile
    print "## Run the command from the current working directory" > shFile
    print "#SBATCH -D ." > shFile
    print "## specify an email address" > shFile
    print "#SBATCH --mail-user "EMAIL_ADDRESS > shFile
    print "## specify when to send the email" > shFile
    print "#SBATCH --mail-type=FAIL" > shFile
    print "#SBATCH -t "ALIGN_TIME > shFile
    print "#SBATCH --mem="ALIGN_MEMORY > shFile
    print "#SBATCH -N 1" > shFile
    print "## Output files" > shFile
    print "#SBATCH -e " shFile".stderr" > shFile
    print "#SBATCH -o " shFile".stdout" > shFile
    print "## Job Starts Here" > shFile
    print "source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/HIC_ENV" > shFile

    # Writing the commands to the file
    print "rm -f "outputBAM_File

    print "bwa mem -A 1 -B 4 -E 50 -L 0 -t 4 "INDEX" <(cat "fastqFile" | gunzip -c ) | samtools view -Shb - > "outputBAM_File > shFile

#    print "bwa mem -A 1 -B 4 -E 50 -L 0 -t 4 "INDEX" <(cat "fastqFile" | gunzip -c ) | python "runPath"/HiC_read_aligner/screenBamFile.py  | samtools view -Shb - > "outputBAM_File > shFile

#    print "rm -f "fastqFile > shFile
#    print "rm -f "shFile > shFile
#    print "rm -f "shFile".stderr" > shFile
#    print "rm -f "shFile".stdout" > shFile
    
    # Closing the sh file
    close ( shFile )
    
}

#=============================================
# MAIN CODE
{

    # Reading in the pair information
    if ( NR%8 == 1 ) { 
        read1 = $0
        gsub( /(-R)/, "/", read1 ) # Converts the separator string in the readID
    }
    if ( NR%8 == 2 ) { seq1  = $0 }
    if ( NR%8 == 4 ) { qual1 = $0 }
    if ( NR%8 == 5 ) {
        read2 = $0 
        gsub( /(-R)/, "/", read2 ) # Converts the separator string in the readID
    }
    if ( NR%8 == 6 ) { seq2  = $0 }
    if ( NR%8 == 0 ) {
        
        # Writing the reads to their respective files
        qual2  = $0
        
        # Writing the R1 file
        print read1"\n"seq1"\n+\n"qual1 | " gzip -c > " fastqFile_R1

        # Writing the R2 file
        print read2"\n"seq2"\n+\n"qual2 | " gzip -c > " fastqFile_R2

        nPairs = nPairs + 1

        if ( nPairs >= pairsPerFile ){
            
            # Closing the fastq files
            close ( " gzip -c > " fastqFile_R1 )
            close ( " gzip -c > " fastqFile_R2 )
            
            # Writing the R1 sh file
            R1_suffix = "R1"
            shFile_R1 = basePath"/"RUN_ID"."fileNumber"."R1_suffix".sh"
            system( "rm "shFile_R1 )
            outputBAM_R1               = basePath"/"RUN_ID"."fileNumber"."R1_suffix".bam"
            bamFileList_R1[fileNumber] = outputBAM_R1
            write_sh_file( shFile_R1, fastqFile_R1, outputBAM_R1 )
            system( "sbatch "shFile_R1 )

            # Writing the R2 sh file
            R2_suffix = "R2"
            shFile_R2 = basePath"/"RUN_ID"."fileNumber"."R2_suffix".sh"
            system( "rm "shFile_R2 )
            outputBAM_R2               = basePath"/"RUN_ID"."fileNumber"."R2_suffix".bam"
            bamFileList_R2[fileNumber] = outputBAM_R2
            write_sh_file( shFile_R2, fastqFile_R2, outputBAM_R2 )
            system( "sbatch "shFile_R2 )

            # Making the new filenames
            nPairs     = 0
            fileNumber = fileNumber + 1
            fastqFile_R1 = basePath"/"RUN_ID"."fileNumber".R1.fastq.gz"
            system( "rm -f "fastqFile_R1 )
            
            fastqFile_R2 = basePath"/"RUN_ID"."fileNumber".R2.fastq.gz"
            system( "rm -f "fastqFile_R2 )
            
#             # Debugging purposes only
#             if ( fileNumber >= 3 ){ exit }

        }
    }
}

#=============================================
END{
    
    # Checking to see if the last file was actually written
    if ( exists(fastqFile_R1) == 1 ){

        # Closing the fastq files
        close ( " gzip -c > " fastqFile_R1 )
        close ( " gzip -c > " fastqFile_R2 )
        
        # Writing the R1 sh file
        R1_suffix = "R1"
        shFile_R1 = basePath"/"RUN_ID"."fileNumber"."R1_suffix".sh"
        system( "rm -f "shFile_R1 )
        outputBAM_R1               = basePath"/"RUN_ID"."fileNumber"."R1_suffix".bam"
        bamFileList_R1[fileNumber] = outputBAM_R1
        write_sh_file( shFile_R1, fastqFile_R1, outputBAM_R1 )
        system( "sbatch "shFile_R1 )

        # Writing the R2 sh file
        R2_suffix = "R2"
        shFile_R2 = basePath"/"RUN_ID"."fileNumber"."R2_suffix".sh"
        system( "rm -f "shFile_R2 )
        outputBAM_R2               = basePath"/"RUN_ID"."fileNumber"."R2_suffix".bam"
        bamFileList_R2[fileNumber] = outputBAM_R2
        write_sh_file( shFile_R2, fastqFile_R2, outputBAM_R2 )
        system( "sbatch "shFile_R2 )

    }
    
    # Launching the watcher
    shFile_wait = basePath"/"RUN_ID".bam_wait.sh"
    system( "rm -f "shFile_wait )
    print "#!/bin/bash" > shFile_wait
    print "## Run the command from the current working directory" > shFile_wait
    print "#SBATCH -D ." > shFile_wait
    print "## specify an email address" > shFile_wait
    print "#SBATCH --mail-user "EMAIL_ADDRESS > shFile_wait
    print "## specify when to send the email" > shFile_wait
    print "#SBATCH --mail-type=FAIL" > shFile_wait
    print "#SBATCH -t 36:00:00" > shFile_wait
    print "#SBATCH --mem=1G" > shFile_wait
    print "#SBATCH -N 1" > shFile_wait
    print "## Output files" > shFile_wait
    print "#SBATCH -e " shFile_wait".stderr" > shFile_wait
    print "#SBATCH -o " shFile_wait".stdout" > shFile_wait
    print "## Job Starts Here" > shFile_wait
    print "source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/HIC_ENV" > shFile_wait
    print "python "runPath"/HiC_read_aligner/waiting.py "RUN_ID > shFile_wait
    print "python "runPath"/HiC_read_aligner/HiC_job_launcher.py "basePath" "configFile" "nextStep > shFile_wait
    close ( shFile_wait )
    system( "sbatch "shFile_wait )

    # Writing the names of the R1 fastq files to the output file
    bam_FOFN_R1 = basePath"/full_bam_file_list_R1.dat"
    system( "rm -f "bam_FOFN_R1 )
    for ( fileNum in bamFileList_R1 ){
        print bamFileList_R1[fileNum] > bam_FOFN_R1
    }
    close( bam_FOFN_R1 )

    # Writing the names of the R2 fastq files to the output file
    bam_FOFN_R2 = basePath"/full_bam_file_list_R2.dat"
    system( "rm -f "bam_FOFN_R2 )
    for ( fileNum in bamFileList_R2 ){
        print bamFileList_R2[fileNum] > bam_FOFN_R2
    }
    close( bam_FOFN_R2 )

}
