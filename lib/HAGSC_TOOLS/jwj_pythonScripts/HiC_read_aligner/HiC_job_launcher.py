__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  8/5/15"

from hagsc_lib import isGzipFile, isBzipFile

from gp_lib import genePool_submitter, create_sh_file, submitJob

from hic_lib import parseConfigFile, runPath, writeAndEcho

from sys import argv, stderr

from os.path import join

from os import chdir

#==============================================================
def real_main():
    
    # Read the basic information
    basePath    = argv[1]
    configFile  = argv[2]
    snpStep     = argv[3]
    
    # Changing to the base directory
    chdir( basePath )
    
    # Read the config file
    configDict, cmd_log = parseConfigFile( configFile, basePath )
    
    # Important files
    GATKbam             = join( basePath, '%s.gatk.bam'%configDict['RUNID'] )
    
    # Setting up the primary scaffolds file
    primaryScaffsFile = join( basePath, 'primaryScaffolds.dat' )
    
    # Run the commands
    if ( snpStep == 'align_and_split' ):

        # Setting the next step in the computation        
        nextStep = 'merge_bam_files'

        # Changing to the base directory
        chdir( basePath )

        # Order the scaffolds by size
        stderr.write( '- Building alignment command\n' )
        
        # Building the awk command
        varString = ' '.join( ['-v nextStep=\"%s\"'            % nextStep, \
                               '-v configFile=\"%s\"'          % configFile, \
                               '-v RUN_ID=\"%s\"'              % configDict['RUNID'], \
                               '-v pairsPerFile=\"%d\"'        % configDict['NUM_PAIRS'], \
                               '-v basePath=\"%s\"'            % basePath, \
                               '-v runPath=\"%s\"'             % runPath, \
                               '-v INDEX=\"%s\"'               % configDict['ASSEMINDEX'], \
                               '-v ALIGN_MEMORY=\"%s\"'        % configDict['ALIGN_MEMORY'], \
                               '-v ALIGN_TIME=\"%s\"'          % configDict['ALIGN_TIME'], \
                               '-v EMAIL_ADDRESS=\"%s\"'       % configDict['EMAIL']] )
        splitCmd = join( runPath, 'HiC_read_aligner/HiC_FASTQ_split.awk' )
        awkCmd  = 'awk %s -f %s'%( varString, splitCmd )
        if ( configDict['COMBINED_FASTQ'] ):
            cmdList = [ 'bzcat -k -c %s | %s'%( configDict['READS'][0], awkCmd ) ]
        else:
            if ( isGzipFile(configDict['READS'][0]) ):
                cmdList = [ 'cat %s | gunzip -c | %s'%( configDict['READS'][0], awkCmd ) ]
            elif ( isBzipFile(configDict['READS'][0]) ):
                cmdList = [ 'cat %s | bunzip2 -c | %s'%( configDict['READS'][0], awkCmd ) ]
            else:
                cmdList = [ 'cat %s | %s'%( configDict['READS'][0], awkCmd ) ]
            #####
        #####
        
        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Aligning reads to create the split bam files:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )
        
        # Running the job
        maxTime   = '12:00:00'
        maxMemory = '10G'
        sh_File   = join( basePath, '%s_align_and_split.sh'%configDict['RUNID'] )
        
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    elif ( snpStep == 'merge_bam_files' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Setting the timing variables
        maxTime   = '12:00:00'
        maxMemory = '30G'
        
        # Opening the jobID output file
        jobID_file = join( basePath, '%s.merge_bam.jobIDs.dat'%(configDict['RUNID']) )
        jobID_oh   = open( jobID_file, 'w' )
        suppressOutput = False

        #---------------------------
        # Merging the R1 bam files
        R1_bam_files = [line.strip() for line in open(join( basePath, 'full_bam_file_list_R1.dat'))]
        R1_joinedBamList = ' '.join( R1_bam_files )
        cmdList          = [ 'source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/HIC_ENV', \
                             'samtools merge -n -f %s.R1.merged.bam %s'%( configDict['RUNID'], R1_joinedBamList ), \
                             'rm -f %s.*.R1.*' % configDict['RUNID'] ]
        sh_File = join( basePath, '%s.R1.merging.sh'%( configDict['RUNID'] ) )
        create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'] )
        job_ID = submitJob( sh_File )
        jobID_oh.write( '%s\n'%job_ID )

        #---------------------------
        # Merging the R2 bam files
        R2_bam_files = [line.strip() for line in open(join( basePath, 'full_bam_file_list_R2.dat'))]
        R2_joinedBamList = ' '.join( R2_bam_files )
        cmdList          = [ 'source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/HIC_ENV', \
                             'samtools merge -n -f %s.R2.merged.bam %s'%( configDict['RUNID'], R2_joinedBamList ), \
                             'rm -f %s.*.R2.*' % configDict['RUNID'] ]
        sh_File = join( basePath, '%s.R2.merging.sh'%( configDict['RUNID'] ) )
        create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'] )
        job_ID = submitJob( sh_File )
        jobID_oh.write( '%s\n'%job_ID )

        # Closing the jobID file
        jobID_oh.close()

#         # Launching the waiting job
#         cmdList = [ 'python %s/HiC_read_aligner/waitingForJobs.py %s'%( runPath, jobID_file ), \
#                     'python %s/HiC_read_aligner/HiC_job_launcher.py %s %s %s'%(runPath, basePath, configFile, nextStep ) ]
#         sh_File = join( basePath, '%s.merge.waiting.sh'%configDict['RUNID'] )
#         genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )
    
    #####
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
