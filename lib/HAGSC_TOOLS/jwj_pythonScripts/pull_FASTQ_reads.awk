{
    # Reading the readID
    if(NR%4==1){readID=$0}

    # Reading the sequence
    if(NR%4==2){seq=$0}

    if(NR%4==0){

        # Storing the quality score
        qual=$0

        # Changing the readID from "-R' to "/""
        gsub(/(-R)/, "/", readID)

        # Writing the output
        if( substr(readID,length(readID),1) == READPOST ) {
            print readID"\n"seq"\n+\n"qual
        }

    }

}
