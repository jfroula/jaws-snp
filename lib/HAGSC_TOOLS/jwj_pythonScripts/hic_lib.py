__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/28/15"

from hagsc_lib import isGzipFile, isBzipFile, commify, IntervalClass, IntervalTree
from hagsc_lib import iterFASTA, histogramClass, fileHeader_Class

from gp_lib import jobFinished, submitJob, create_sh_file

from math import ceil, floor, log

from os.path import join, isfile

from sys import stdout, stderr

from numpy import array

from os import system

import subprocess

import re

#==============================================================
# Default values
runPath          = '/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts'
def_numPairs     = 5000000
def_ALIGN_MEMORY = '15G'
def_ALIGN_TIME   = '8:00:00'

#==============================================================
# Possible job types
jobTypeSet = set( ['align_and_split', \
                   'merge_bam_files'] )

#==============================================================
def writeAndEcho( tmpStr, cmdLogFile ):
    stdout.write( '%s\n'%tmpStr )
    oh = open( cmdLogFile, 'a' )
    oh.write( '%s\n'%tmpStr )
    oh.close()

#==============================================================
def echoAndExecute( tmpCmd, cmdLogFile, execute=True ):
    stdout.write( '%s\n'%tmpCmd )
    oh = open( cmdLogFile, 'a' )
    oh.write( '%s\n'%tmpCmd )
    oh.close()
    if ( execute ): system( tmpCmd )

#==============================================================
def throwError( tmpString ):
    stderr.write( '-------------------------\n' )
    stderr.write( 'ERROR:  %s\n'%tmpString )
    stderr.write( 'Halting execution\n' )
    stderr.write( '-------------------------\n' )
    assert False
    
#========================
def parseConfigFile( configFile, basePath ):
    
    # Checking for the config file
    if ( not isfile(configFile) ):
        throwError( 'parseConfigFile could not find config file %s'%configFile )
    #####

    # Initializing the key set
    keySet = set(['RUNID', \
                  'LIB_ID', \
                  'REFERENCE', \
                  'ASSEMINDEX', \
                  'READS', \
                  'EMAIL', \
                  'NUM_PAIRS', \
                  'ALIGN_MEMORY', \
                  'ALIGN_TIME', \
                  'COMBINED_FASTQ'])
    
    # Parsing the config file and pulling the key:value relationships
    configDict = {}
    for line in open( configFile ):
        
        # Breaking out the key:value pair
        key, value   = line.split(None)
        
        # Adding the key:value pair
        if ( key == 'READS' ):
            configDict[key] = value.split(';')
        elif ( key in set(['NUM_PAIRS']) ): 
            configDict[key] = int( value )
        elif ( key == 'COMBINED_FASTQ' ):
            if ( value == 'True' ):
                configDict[key] = True
            elif ( value == 'False' ):
                configDict[key] = False
            else:
                throwError( 'Invalid value=%s for key=%s'%(value,key) )
            #####
        else:
            configDict[key] = value
        #####
        
        # Checking for any keys that are mislabeled
        try:
            keySet.remove(key)
        except KeyError:
            throwError( 'parseConfigFile encountered an unrecognized key: %s'%key )
        #####
        
    #####

    #-----------------------------------    
    # Creating the command log filename
    try:
        cmd_log = join( basePath, '%s_cmdLog.dat'%configDict['RUNID'] )
        if ( not isfile(cmd_log) ):
            # Clearing the command log file
            oh = open( cmd_log, 'w' )
            oh.close()
        else:
            writeAndEcho( "================================", cmd_log )
            writeAndEcho( "BEGINNING NEW RUN",                cmd_log )
            writeAndEcho( "================================", cmd_log )
        #####
        # Writing to the command log
        writeAndEcho( '- EXECUTING: parseConfigFile', cmd_log )
    except KeyError:
        throwError( 'Unable to read RUNID' )
    #####

    #-----------------------------------    
    # Checking the aligner memory
    try:
        x = configDict['ALIGN_MEMORY']
    except KeyError:
        # Default aligner is MEM
        configDict['ALIGN_MEMORY'] = def_ALIGN_MEMORY
        writeAndEcho( '- Using the default alignment memory %s'%def_ALIGN_MEMORY, cmd_log )
        keySet.remove('ALIGN_MEMORY')
    #####

    #-----------------------------------    
    # Checking the aligner time
    try:
        x = configDict['ALIGN_TIME']
    except KeyError:
        # Default aligner is MEM
        configDict['ALIGN_TIME'] = def_ALIGN_TIME
        writeAndEcho( '- Using the default alignment time %s'%def_ALIGN_TIME, cmd_log )
        keySet.remove('ALIGN_TIME')
    #####

    #-----------------------------------
    # Checking the number of pairs
    try:
        x = configDict['NUM_PAIRS']
    except KeyError:
        # Default caller is VARSCAN
        configDict['NUM_PAIRS'] = def_numPairs
        writeAndEcho( '- Using the default number of pairs per file %d'%def_numPairs, cmd_log )
        keySet.remove('NUM_PAIRS')
    #####

    #-----------------------------------
    # Checking to see if I have a combined set of FASTQ files I am using
    try:
        x = configDict['COMBINED_FASTQ']
    except KeyError:
        # Default value is False
        configDict['COMBINED_FASTQ'] = False
        writeAndEcho( '- Using the default value for COMBINED_FASTQ=False', cmd_log )
        keySet.remove('COMBINED_FASTQ')
    #####

    #-----------------------------------    
    # Checking for missing keys
    if ( len(keySet) > 0 ):
        strList = ['parseConfigFile detected that the following keys are missing in your snps.config file\n']
        for key in keySet:
            strList.append( '\t%s\n'%key )
        #####
        strList.append( 'Please add these keys to your config file and resubmit the snp calling job.')
        throwError( ''.join(strList) )
    #####
    
    #-----------------------------------    
    # Checking to see if the reference exists
    if ( not isfile(configDict['REFERENCE']) ):
        throwError( 'REFERENCE genome can not be located %s'%(configDict['REFERENCE']) )
    #####

    #-----------------------------------    
    # Checking to see if the INDEX exists
    if ( not isfile('%s.pac'%configDict['ASSEMINDEX']) ):
        throwError( 'ASSEMINDEX bwa index can not be located %s'%(configDict['ASSEMINDEX']) )
    #####

    #-----------------------------------    
    # Checking to see if the reads exist
    if ( not configDict['COMBINED_FASTQ'] ):
        for item in configDict['READS']:
            if ( not isfile(item) ):
                throwError( 'READS file can not be located %s'%(item) )
            #####
        #####
    #####

    return ( configDict, cmd_log )

