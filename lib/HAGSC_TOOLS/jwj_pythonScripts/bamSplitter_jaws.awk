#=================================
BEGIN{
    # Initializing the global variables
    n        = 1
    header[n]= 0
    # Reading in the primary scaffolds
    split("", primaryScaffs) # Initializes as a blank array
    while ( (getline < PRIMARY_SCAFFS_FILE) > 0 ){
        print $1, $2
        primaryScaffs[$1] = "GRP_"$2
        headerArray["GRP_"$2] = 0
    }
}

#=================================
# Main splitting code
{
    # Pulling the header information
    if ( ($1 == "@SQ") || ($1 == "@PG") ){
        # Storing the header for later
        header[n] = $0
        n++
    }else{
        # Determining the output bam file name
        GRP_ID = primaryScaffs[$3]
        outputBAM_File = basePath"/"RUN_ID"."GRP_ID"."fileNumber".bam"
        # Writing entries to the bam file
        if ( headerArray[GRP_ID] == 0 ){
            # Resetting the header information
            headerArray[GRP_ID] = 1
            system( "rm "outputBAM_File" | echo no file to remove" )
            # Writing the header the first time through
            for ( i=1; i<n; i++ ){
                print header[i] | "samtools view -Sbh - > "outputBAM_File
            }
            print $0 | "samtools view -Sbh - > "outputBAM_File
        }else{
            # Writing the alignments
            print $0 | "samtools view -Sbh - > "outputBAM_File
        }
    }
}

#=================================
# LAUNCHING THE SORTING JOBS
#END{
#    
#    # Launching the sorting jobs
#    for ( GRP_ID in headerArray ){
#
#        # Writing the sorting scripts
#        shFile = basePath"/"RUN_ID "."fileNumber"."GRP_ID".sorting.sh"
#        system( "rm "shFile" | echo no file to remove" )
#        print "#!/bin/bash" > shFile
#        print "samtools sort -m 4G "basePath"/"RUN_ID"."GRP_ID"."fileNumber".bam "basePath"/"RUN_ID"."GRP_ID"."fileNumber".sorted" > shFile
#        
#        # Closing the sh file
#        close ( shFile )
#        
#        # Launching the sorting job
#        system( "bash "shFile )
#    }
#
##    if ( lastJob == 1 ){
##
##        # Writing the .sh file
##        shFile = basePath"/"RUN_ID".watching.sh"
##        system( "rm "shFile" | echo no file to remove" )
##        print "#!/bin/bash" > shFile
##        print "python "runPath"/snpCalling/waiting.py "RUN_ID > shFile
##        print "python "runPath"/snpCalling/snpJobLauncher.py "basePath" "configFile" "nextStep > shFile
##
##        # Closing the sh file
##        close ( shFile )
##
##        # Launching the sorting job
##        system( "bash "shFile )
##
##    }
#    
#}
