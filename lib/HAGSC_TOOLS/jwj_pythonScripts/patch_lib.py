__author__='Robert Herring, rherring@hudsonalpha.org'
__date__ ='Created:  10/20/15'

from gp_lib      import genePool_submitter
from hagsc_lib   import iterFASTA, iterFASTQ, FASTAFile_dict, iterCounter, writeFASTA, SeqRecord

from os          import getcwd, mkdir, chdir, getpid, uname, system, stat, listdir
from sys         import stderr, stdout, stdin
from glob        import glob
from os.path     import join, isdir, isfile
from datetime    import datetime
from collections import defaultdict

import re
import time
import string
import pickle
import random
import subprocess

import multiprocessing as mp
#==============================================================
# Global
#==============================================================
contigFinder  = re.compile( r'[ATCGacgt]+' ).finditer

gapFinder     = re.compile( r'[ATCGatcg][Nn]+[ATCGatcg]' ).finditer

cigar_parse   = re.compile( r'(\d+)(\w)' ).findall

LC            = set ('atcg')

trans         = string.maketrans('ACGTacgt', 'TGCAtgca')

blatDict      = { 'params'            :'-minMatch=3 -tileSize=8 -maxIntron=10000 -repMatch=10000 -noHead -minIdentity=75 -extendThroughN -mask=lower', \
                  'target'            :None, \
                  'query'             :None, \
                  'awkFilterCmd'      :"awk \'{ if (($1 >= $11*(80/100)) && (($17-$16)<= $11*(115/100)) && ($12<= $11*(15/100))) {print $10,$14,$15,$16,$17} }\'", \
                  'findGapCrossersCmd':None, \
                  'blatOut'           :None }
                 
blatCmd       = "bzcat -kc %(target)s | blat stdin %(query)s %(params)s stdout |  %(awkFilterCmd)s | sort -k2,2 -S 2G | %(findGapCrossersCmd)s | bzip2 -c > %(blatOut)s"

#==============================================================
def countBases( tmpSeq ):
    s = tmpSeq.upper()
    return s.count('A')+s.count('T')+s.count('C')+s.count('G')

#==============================================================
def revComp(kmer):
    trans = string.maketrans('ACGTacgt', 'TGCAtgca')
    return kmer.translate(trans)[::-1]
    
#==============================================================
def lexKmer( kmer ):
    return sorted([kmer, revComp(kmer)])[0]
    
#==============================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )
    
#==============================================================
def CreateOrFindTmpPath(basePath):
    try:
        tmpDir = findTmpDir(basePath)
    except ValueError:   
        tmpDir = generateTmpDirName()
        mkdir(tmpDir)
    basePath   = getcwd()
    tmpPath    = join(basePath,tmpDir)
    return tmpPath
    
#==============================================================
def findTmpDir(basePath):
    if int(subprocess.check_output('/bin/ls %s | grep "tmp.*.*"|wc -l'%basePath,shell=True))==0:
        raise ValueError('tmp dir does not exist!')
    else:
        tmp_dirs = [subprocess.check_output('ls | grep "tmp.*.*"',shell=True)]
        if len(tmp_dirs) > 1:
            raise ValueError( 'More than one temp directory in this location!\n')
        #####
        else:
            return (tmp_dirs[0]).strip()
        #####
    #####
   
#==============================================================
def writeAndEcho( tmpStr, cmdLogFile ):
    stdout.write( '%s\n'%tmpStr )
    oh = open( cmdLogFile, 'a' )
    oh.write( '%s\n'%tmpStr )
    oh.close()
    
#==============================================================
def echoAndExecute( tmpCmd, cmdLogFile, execute=True ):
    stdout.write( '%s\n'%tmpCmd )
    oh = open( cmdLogFile, 'a' )
    oh.write( '%s\n'%tmpCmd )
    oh.close()
    if ( execute ): system( tmpCmd )
    
#==============================================================
def throwError( tmpString ):
    stderr.write( '-------------------------\n' )
    stderr.write( 'ERROR:  %s\n'%tmpString )
    stderr.write( 'Halting execution\n' )
    stderr.write( '-------------------------\n' )
    assert False

#==============================================================
class fastqRecord(object):
    def __init__(self,id,seq,qual):
        self.id   = id
        self.seq  = seq
        self.qual = qual

#==============================================================
class fastaRecord(object):
    def __init__(self,id,seq):
        self.id   = id
        self.seq  = seq

#==============================================================
def iterFASTA_local( tmpHandle ):
    # Skipping any blank lines or comments at the top of the file
    while True:
        line = tmpHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        id = line[1:].split(None,1)[0]
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or (line[0] == '>') ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield fastaRecord( id, ''.join(tmpData) )
        
        # Stopping the iteration
        if ( not line ): return
    #####
    assert False, 'Should never reach this line!!'

#==============================================================
def iterFASTQ_local( tmpHandle ):
    # Skipping any blank lines or comments at the top of the file
    while True:
        id   = tmpHandle.readline()[1:-1]
        seq  = tmpHandle.readline()[:-1]
        d    = tmpHandle.readline()[0]
        qual = tmpHandle.readline()[:-1]
        if ( not qual ): break
        if ( d != '+' ):
            stdout.write( 'SOMETHING WENT HORRIBLY WRONG\n')
            stdout.write( '%s\n'%id )
            stdout.write( '%s\n'%seq )
            stdout.write( '%s\n'%d )
            stdout.write( '%s\n'%qual )
            assert False
        #####
        yield fastqRecord( id, seq, qual )
    #####
    assert False, 'Should never reach this line!!'

#==============================================================
def univIter( fileName ):
    
    # Determining the file type
    ext = fileName.split('.')[-1]
    
    # Determining the compression type
    if ( ext == 'bam' ):
        p  = subprocess.Popen( 'samtools view %s | awk \'{print ">"$1"\\n"$10}\'' % fileName, shell=True, stdout=subprocess.PIPE )
        oh = p.stdout
    elif ( ext == 'bz2' ):
        p  = subprocess.Popen( 'cat %s | bunzip2 -c'%fileName, shell=True, stdout=subprocess.PIPE )
        oh = p.stdout
    elif ( ext == 'gz' ):
        p  = subprocess.Popen( 'cat %s | gunzip -c'%fileName, shell=True, stdout=subprocess.PIPE )
        oh = p.stdout
    else:
        oh = open( fileName )
    #####
    
    stdout.write( "\n\t---------------\n" )
    stdout.write( "\t-FILENAME:    %s\n"%fileName )
    stdout.write( "\t-EXTENSION:   %s\n"%ext )
    
    # Determining which iterator to use:
    line1 = oh.readline()
    if ( line1[0] == '@' ):
        
        stdout.write( "\t-FILE_FORMAT: FASTQ\n" )
        id   = line1[1:-1]
        seq  = oh.readline()[:-1]
        d    = oh.readline()[0]
        qual = oh.readline()[:-1]
        yield fastqRecord( id, seq, qual )
        for r in iterFASTQ_local( oh ): yield r
        
    elif ( line1[0] == '>' ):
        
        stdout.write( "\t-FILE_FORMAT: FASTA\n" )
        line2 = oh.readline()
        yield fastaRecord( line1[1:].split(None)[0], line2[:-1] )
        for r in iterFASTA_local( oh ): yield r
        
    else:
        
        stdout.write( "SOMETHING WENT HORRIBLY WRONG\n" )
        stdout.write( '%s\n'%fileName )
        stdout.write( '%s\n'%ext )
        stdout.write( '%s\n'%line1 )
        assert False
        
    #####
    
    # Closing the file handles
    if ( ext in ['bam', 'bz2', 'gz'] ):
        p.poll()
    else:
        oh.close()
    #####
    
    return 
        
#==============================================================
# Possible job types
jobTypeSet = set( ['splitPBreads', \
                   'removeTriangles', \
                   'kmerizeAssembly', \
                   'blatKmers', \
                   'blatGaps', \
                   'alignPB', \
                   'createMap' , \
                   'runCanu', \
                   'alignPatches', \
                   'patchAssembly'])

#==============================================================
# Config file parser
def parseConfigFile( configFile, basePath ):
    
    # Checking for the config file
    if ( not isfile(configFile) ):
        throwError( 'parseConfigFile could not find config file %s'%configFile )
    #####

    # Initializing the key set
    keySet = set(['ASSEMBLY', \
                  'MASKED_ASSEMBLY', \
                  'MAX_PB_BASES', \
                  'PB_FOFN', \
                  'STATUS', \
                  'RUN_ID', \
                  'EMAIL', \
                  'PROG_PATH', \
                  'KMER_SIZE', \
                  'MAX_READS_TO_GAP', \
                  'MIN_SCAFF_SIZE', \
                  'MAX_ISLAND_SIZE', \
                  'GAP_DISTANCE', \
                  'MIN_MAPPINGS', \
                  'REQUIRED_CANU_COV', \
                  'N_PROC_FOR_CANU', \
                  'N_PROC_FOR_BWA'])

    # Parsing the config file and pulling the key:value relationships
    configDict = {}
    for line in open( configFile ):
        
        # Breaking out the key:value pair
        key, value = line.split(None)
        
        # Adding the key:value pair
        configDict[key] = value
        
        # Checking for any keys that are mislabeled
        try:
            keySet.remove(key)
        except KeyError:
            throwError( 'parseConfigFile encountered an unrecognized key: %s'%key )
        #####
    #####
    
    # Creating the command log filename
    try:
        cmd_log = join( basePath, 'cmdLog_%s.dat'%configDict['RUN_ID'] )
        if ( not isfile(cmd_log) ):
            # Clearing the command log file
            oh = open( cmd_log, 'w' )
            oh.close()
        #####
        # Writing to the command log
        writeAndEcho( '- EXECUTING: parseConfigFile', cmd_log )
    #####
    except KeyError:
        throwError( 'Unable to read RUN_ID' )
    #####
        
    # Checking for left over keys
    if ( len(keySet) > 0 ):
        strList = ['parseConfigFile detected that the following keys are missing in your config file\n']
        for key in keySet:
            strList.append( '\t%s\n'%key )
        #####
        strList.append( 'Please add these keys to your config file and resubmit the job.\n\n')
        throwError( ''.join(strList) )
    #####
    
    # Checking the status of the run
    if ( configDict['STATUS'] == 'PROCESSED' ):
        throwError( 'This run has already been processed with status %s'%(configDict['STATUS']) )
    #####
    
    return ( configDict, cmd_log )

#==============================================================
def splitPBreads( tmpPath, FOFN, MAX_PB_BASES, cmd_log ):
    
    # Generating a list of command lists for each pb file
    cmdList = []
    for PB_fastq in [ line.strip() for line in open(FOFN) ]:
        input        = [ tmpPath,  PB_fastq, MAX_PB_BASES ]
        input_string = ','.join(["\'{0}\'".format(x) for x in input])
        cmd          = 'python -c "import patch_lib;patch_lib.{0}({1})"'.format( 'splitPBfile', input_string )
        cmdList.append( [cmd] )
    #####

    # Writing the commands to the log file
    writeAndEcho( '\n-----------------------\n- SPLITTING PB FILES:', cmd_log )
    for cmd in cmdList:
        writeAndEcho( ''.join(cmd), cmd_log )
    #####
        
    # Return commands
    return cmdList
    
#==============================================================
def splitPBfile( tmpPath, PbioFastq, MAX_PB_BASES ):
    
    # Pulling the fofn file name
    pb_fn = PbioFastq.split('/')[-1][:-6]
    
    # Starting my tmp fasta file generation
    tmpfilenum          = 1
    tmpFasta            = 'tmpFile_%s_%d.fasta'% ('.'.join((PbioFastq.split('/')[-1]).split('.')[:-1]), tmpfilenum)
    tmpFastaPath        = join( tmpPath,tmpFasta )
    fastaHandle         = open( tmpFastaPath, 'w' ) 
    fastaNameFile       = join( tmpPath,'preTriangleFiles.dat' )
    fastaNameFileHandle = open(fastaNameFile,'a')
    seqLength           = 0
    lastReadID          = None
    for r in univIter( PbioFastq ):
        readID     = r.id.split('/')[-2]
        tmpSeq     = str(r.seq)
        readLength = len(tmpSeq)
        
        # First read or same readID, only want to consider closing file when new readID encountered so they aren't split up
        if (lastReadID == None) or (lastReadID == readID):
            fastaHandle.write('>%s\n%s\n'%( r.id, tmpSeq ))
            seqLength += readLength
            lastReadID = readID
            continue
        #####
        else: # new readID
            seqLength += readLength
            lastReadID = readID
            if ( seqLength < int(MAX_PB_BASES) ):
                fastaHandle.write('>%s\n%s\n'%( r.id, tmpSeq ))
                continue
            #####
        #####
        # Closes file that has reached size limit and zips it up
        fastaHandle.close()
        fastaNameFileHandle.write( '%s\n'%tmpFastaPath )
        #####
        # Initiates new tmpFiles to drop into
        tmpfilenum    += 1
        seqLength      = readLength
        tmpFasta       = 'tmpFile_%s_%d.fasta'% ('.'.join((PbioFastq.split('/')[-1]).split('.')[:-1]), tmpfilenum)
        tmpFastaPath   = join( tmpPath, tmpFasta )
        fastaHandle    = open( tmpFastaPath, 'w' )
        fastaHandle.write('>%s\n%s\n'%( r.id, tmpSeq ))
    #####
                    
    # Closes out final tmpFile
    fastaHandle.close()
    fastaNameFileHandle.write( '%s\n'%tmpFastaPath )
    fastaNameFileHandle.close()

#==============================================================
def returnTriangleCmds( tmpPath ):
    fileNames = [ x.strip() for x in open( '%s/preTriangleFiles.dat'%tmpPath ) ] 
    cmdList   = []
    for fn in fileNames:
        cmdList.append( [ 'python -c "import patch_lib; patch_lib.writeTriangleFreeFasta( \'%s\', \'%s\' )"'%( fn, tmpPath) ] )
    #####
    return cmdList
               
#==============================================================
def writeTriangleFreeFasta( fastaFile, tmpPath ):

    fastaNameFileHandle = open('%s/fastaNameFile.dat'%tmpPath, 'a')
    triangle_reads_oh   = open( '%s/triangle_reads.dat'%tmpPath, 'a' )
    
    # This file will be used later to create a pb.fastq using extract seq and qual from original files
    reads_utilized_oh   = open( '%s/reads_utilized.dat'%tmpPath, 'a' )
    fasta_dict          = {}
    for r in iterFASTA( open( fastaFile ) ):
        fasta_dict[ r.id ] = str( r.seq )
    #####

    blatCMD   = "module load blat;blat -noHead -minMatch=3 -tileSize=8 -repMatch=10000 -minIdentity=75 -extendThroughN %s %s stdout"%( fastaFile, fastaFile )
    awkCMD    = "awk 'function abs(v) {return v < 0 ? -v : v} { if ( ($9==\"-\") && ($10==$14) && (abs($12-$16)<=75) && (abs($13-$17)<=75) && ( (($2+$6)/($13-$12))<(55/100) ) && (($13-$12)>400)) {print $10}}'"
    cmd       = "%s | %s"%( blatCMD, awkCMD )
    bad_reads = set( [ x.strip() for x in subprocess.check_output( cmd, shell=True ).splitlines() ] )

    # I am writing over the previous file that, potentially, contained triangle reads
    oh       = open( fastaFile, 'w' )
    sizeDict = defaultdict(list)
    for id, seq in fasta_dict.iteritems():
        if id in bad_reads: continue
        readID = id.split('/')[-2]
        sizeDict[readID].append( (len(seq), id) )
    #####
    fastaNameFileHandle.write( '%s.bz2\n'%fastaFile )
    
    # Running through readID/size dictionary and pulling largest non triangle read for each readID then writing to file
    for readID, tmpList in sizeDict.iteritems():
        tmpList.sort( reverse = True )
        largest_read = tmpList[0][1]
        oh.write( '>%s\n%s\n'%( largest_read, fasta_dict[largest_read] ) )
        reads_utilized_oh.write( '%s\n'%largest_read )
    #####
    oh.close()
    for bad_read in bad_reads:
        triangle_reads_oh.write( '%s\n'%bad_read )
    #####
    triangle_reads_oh.close()
    reads_utilized_oh.close()
    system( 'bzip2 %s'%fastaFile )

#==============================================================
def kmerizeAssembly( GAP_DISTANCE, MASKED_ASSEMBLY, MIN_SCAFF_SIZE, KMER_SIZE, MAX_ISLAND_SIZE, tmpPath, cmd_log, nextStep, quit ):

    # Generating command list
    CmdList         = [ 'module load python' ]
    input           = [ GAP_DISTANCE, KMER_SIZE, MAX_ISLAND_SIZE, MIN_SCAFF_SIZE, MASKED_ASSEMBLY, tmpPath ]
    input_string    = ','.join(["\'{0}\'".format(x) for x in input])
    kmerizeCmd      = 'python -c "import patch_lib;patch_lib.{0}({1})"'.format( 'kmerize', input_string )
    CmdList.append( kmerizeCmd )

    # Writing the commands to the log file
    writeAndEcho( '\n-----------------------\n- GENERATING KMERS FROM THE ASSEMBLY:', cmd_log )
    for cmd in CmdList:
        writeAndEcho( cmd, cmd_log )
    #####
    
    # Return commands
    return CmdList
    
#==============================================================
def kmerize( GAP_DISTANCE, KMER_SIZE, MAX_ISLAND_SIZE, min_scaff_size, masked, tmpPath ):
    
    # Definitions
    MAX_ISLAND_SIZE   = int( MAX_ISLAND_SIZE )
    GAP_DISTANCE      = int( GAP_DISTANCE )
    KMER_SIZE         = int( KMER_SIZE )
    kmer_to_positions = defaultdict(list)

    for r in iterFASTA( open(masked) ):
        gapID       = 0
        rID         = r.id
        tmpSeq      = str(r.seq)
        # We don't want any puny scaffolds here
        if len(tmpSeq) >= int(min_scaff_size):
            contigList  = [item.span() for item in contigFinder(tmpSeq)]
            contigSizes = [x[1]-x[0] for x in contigList]
            contigCount = len(contigList)
            if contigCount == 1: # No gaps
                ROG_jump     = False
                LOG_jump     = False
                ROG_end      = None
                LOG_start    = None
                contigSTART  = contigList[0][0]
                contigEND    = contigList[0][1]
                tmpKmersDict = create2xKmers( KMER_SIZE, contigSTART, contigEND, LOG_start, ROG_end, ROG_jump, LOG_jump, tmpSeq, rID, gapID )
                for key,tmpList in tmpKmersDict.iteritems():
                    kmer_to_positions[key].extend(tmpList)
                #####
            #####
            else: # Contains at least one gap
                for n in xrange(contigCount):
                    ROG_jump  = 0
                    LOG_jump  = 0
                    ROG_end   = None
                    LOG_start = None
                    contigSTART     = contigList[n][0]
                    contigEND       = contigList[n][1]
                    #####
                    
                    # Developing LOG range
                    if n != (contigCount -1): # last contig doesn't have LOG data (only gap on left side)
                        LOG_start = max( (contigEND - GAP_DISTANCE), contigSTART )
                        m         = n + 1
                        while ( m < ( contigCount - 1 ) ) and ( contigSizes[m] <= MAX_ISLAND_SIZE ):
                            LOG_jump += 1
                            m        += 1
                        #####
                    #####
                            
                    # Developing ROG range
                    if n != 0: # first contig doesn't have ROG data (only gap on right side)
                        ROG_end = min( (contigSTART + GAP_DISTANCE), contigEND )
                        m       = n - 1
                        while ( m >= 0 ) and ( contigSizes[m] <= MAX_ISLAND_SIZE ):
                            ROG_jump += 1
                            m        -= 1
                        #####
                    #####
    
                    # Bringing this information into the lex. kmer dict
                    tmpKmersDict = create2xKmers( KMER_SIZE, contigSTART, contigEND, LOG_start, ROG_end, ROG_jump, LOG_jump, tmpSeq, rID, gapID )
                    for key, tmpList in tmpKmersDict.iteritems():
                        kmer_to_positions[key].extend(tmpList)                
                    #####
                    gapID += 1
                #####
            #####
        #####
    #####
    
    # This is how I normally run for assemblies
    total       = 0
    not_unique  = 0
    not_gap_adj = 0
    lowercase   = 0
    not_LR      = 0
    
    kmerFile    = join( tmpPath, 'tmp_kmers.fasta' )
    kmerFile_oh = open ( kmerFile, 'w')
    for key, tmpList in kmer_to_positions.iteritems():
    
        total += 1
    
        # Writing only the unique kmers into a fasta file
        if ((len(tmpList)) > 1):
            not_unique += 1
            continue
        
        # Excluding any "other" kmers that aren't gap adjacent
        if "|O" in tmpList[0]:
            not_gap_adj += 1
            continue
        
        # Exclude all of the soft-masked 100mers from the file
        if (len(set(key).intersection(LC)) > 0):
            lowercase += 1
            continue
        
        # Only want gap adjacent kmers
        if ("|L" not in tmpList[0]) and ("|R" not in tmpList[0]):
            not_LR += 1
            continue
                
        kmerFile_oh.write('>%s\n%s\n' % (tmpList[0], key))
    #####
    
    print "Total %s"%(total)
    print "Not unique %s"%(not_unique)
    print "Not gap adj %s"%(not_gap_adj)
    print "Not LR %s"%(not_LR)
    kmerFile_oh.close()
    
#==============================================================
def create2xKmers( KMER_SIZE, contigSTART, contigEND, LOG_start, ROG_end, ROG_jump, LOG_jump, tmpSeq, rID, gapID ):
    # Remember that the gapID always refers to the one on right side of contig
    # Therefore if I find a right of gap kmer, it is on the right side of (gapID -1)
    KMER_SIZE = int(KMER_SIZE)
    halfSize  = int(KMER_SIZE/2)
    tmpDict   = defaultdict(list)
    s = contigSTART
    e = s + KMER_SIZE
    end = contigEND
    while e <= end:
        lexmer = lexKmer( tmpSeq[s:e] )
        kmer_name = '%s|%s'%(rID,s)
        if (ROG_end) and (s < ROG_end):
            kmer_name += '|R_%s'%( gapID - 1 )
            for x in range(ROG_jump):
                kmer_name += '|R_%s'%( gapID - (2+x) )
            #####
        #####
        if (LOG_start) and (s > LOG_start):
            kmer_name += '|L_%s'%( gapID )
            for x in range(LOG_jump):
                kmer_name += '|L_%s'%( gapID + (1+x) )
            #####
        #####
        if (LOG_start > ROG_end) and (s > ROG_end) and (s < LOG_start):
            kmer_name += '|O'
        #####
        s += halfSize
        e = s + KMER_SIZE
        tmpDict[lexmer].append(kmer_name)
    #####
    return tmpDict
    
#==============================================================

def returnKmerBlatCmds( tmpPath, cmd_log, basePath, assemblyFile, minMappings ):

    # Generate a blat cmd for every PB fastq file
    blatCmdsList  = []
    fastaNameFile = open( '%s/fastaNameFile.dat' %tmpPath ,'r')
    for line in fastaNameFile:
        tmpList           = [ 'module load blat\n' ]
        fastaFile         = line.strip()
        fastaName         = (fastaFile.split('/')[-1])[:-10]
        blatDict['query'] = join(tmpPath, "tmp_kmers.fasta")

        # Building awk and blat commands
        blatDict['findGapCrossersCmd'] = 'python %s %s %s' %( '/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/gapCrossReadFinder.py', minMappings, tmpPath )
        blatDict['blatOut']            = '%s/%s.blat.bz2'%(tmpPath, fastaName)
        blatDict['target']             = fastaFile
        blatCommand                    = blatCmd %blatDict
        tmpList.append( blatCommand )
        blatCmdsList.append( tmpList )
    #####
    # Writing out the blat commands to log
    writeAndEcho( '\n-----------------------\n- RUNNING KMER BLATS AGAINST PB READS:', cmd_log )
    for tmpList in blatCmdsList:
        for cmd in tmpList:
            writeAndEcho( cmd, cmd_log)
        #####
    #####
   
    # Return blat commands
    return blatCmdsList
    
#==============================================================

class Scaffold(object):
    
    def __init__(self, scaffID ):
        self.scaffID         = scaffID
        self.gapDict         = defaultdict(list)
        self.pbReadsUtilized = set()
        self.blat_info_list  = []
    
    def add_blat( self, pbRead, gap ):
        self.gapDict[gap].append(pbRead)
            
    def return_parent_gap_dict(self, MAX_ISLAND_SIZE, assemblyIndexDict):
        scaffseq     = str( assemblyIndexDict[self.scaffID].seq )
        contigSpans  = [ x.span() for x in contigFinder(scaffseq) ]
        contigSizes  = [ x[1]-x[0] for x in contigSpans ]
        contigString = ''
        for x in contigSizes:
            if x > int(MAX_ISLAND_SIZE):
                contigString += 'C'
            else:
                contigString += 'I'
            #####
        #####
        parent_gap_finder = re.compile( r'CI+C' ).finditer
        island_spans      = [x.span() for x in parent_gap_finder(contigString)]
        parent_gap_dict   = defaultdict(list)
        for island_span in island_spans:
            parent_gap_dict[ '%s_%s_%s'%(self.scaffID,island_span[0], island_span[1] - 1) ] = [ '%s_%s'%(self.scaffID,x) for x in range(island_span[0], island_span[1] - 1) ]
        #####
        return parent_gap_dict
        
    def return_blat_info( self, basePath, tmpPath, progPath, assemblyFile, FLANK_DISTANCE, assemblyIndexDict, MAX_READS_TO_GAP, readsDict, REQUIRED_CANU_COV ):
        
        # Pulling scaffold sequence into memory
        scaffSeq = str( assemblyIndexDict.get(self.scaffID).seq )
        
        # Generating gap spans and contig spans
        gapSpans             = [x.span() for x in gapFinder(scaffSeq)]
        contigSpans          = [x.span() for x in contigFinder(scaffSeq)]
        contigRights         = [x[1] for x in contigSpans]
        reversedContigRights = [x for x in reversed(contigRights)]
        contigLefts          = [x[0] for x in contigSpans]
      
        # Creating a file to know where pbReads should be aligning
        pbRead_to_gap_path = '%s/pbRead_to_gap.dat'%tmpPath
        pbRead_to_gap_oh   = open(pbRead_to_gap_path, 'a')

        for gapID, pbReadList in self.gapDict.iteritems():
        
            pbReadSet = set(pbReadList)
                
            if ( len(pbReadSet) <= int(MAX_READS_TO_GAP) ) and ( len(pbReadSet) >=  int(REQUIRED_CANU_COV) ):
                                                                
                # Writing to my pbRead to gap file
                for pbRead in pbReadList:
                    pbRead_to_gap_oh.write('%s\t%s\n'%(pbRead,gapID))
                #####
                
                # Define initial gap flanking boundaries
                gap       = gapID.rsplit('_',1)[1]
                try:
                    gapSpan   = gapSpans[int(gap)]
                #####
                except IndexError:
                    continue
                #####
                spanLeft  = (gapSpan[0] - FLANK_DISTANCE)
                spanRight = (gapSpan[1] + FLANK_DISTANCE)

                # Adjusting boundaries if they fall outside contig boundaries, or on an N
                if (spanLeft < 0):
                    spanLeft = 0
                #####
                if scaffSeq[spanLeft] == 'N':
                    for x in contigLefts:
                        if x > spanLeft: # Searching for the next known base, which will be the leftmost of a contig
                            spanLeft = x
                            break
                        #####
                    #####
                #####
                if ( spanRight >= len(scaffSeq) ):
                    spanRight = len(scaffSeq) - 1
                #####
                if scaffSeq[spanRight] == 'N':
                    for x in reversedContigRights:
                        if x < spanRight: # Searching for the next known base, which will be the rightmost of a contig
                            spanRight = x
                            break
                        #####
                    #####
                #####
                # Generating my target sequence
                target_seq = scaffSeq[spanLeft:spanRight]
                  
                # Creating target fasta file
                print "Creating target fasta for %s"%gapID
                target_file_path = '%s/%s.fasta'%( tmpPath, gapID )
                target_oh        = open( target_file_path, 'w')
                target_oh.write('>%s\n%s\n'%( gapID, target_seq ) )
                target_oh.close()
                
                # Creating path to query names and query fasta path
                query_file_path    = '%s/%s_query.fasta'%( tmpPath, gapID )
                query_file_path_oh = open(query_file_path, 'w')
                for pbRead in pbReadSet:
                    readSeq = readsDict[pbRead]
                    if len(readSeq) == 0:
                        print "Empty Read!!!!!!!!!!!!!!!!!!!!!!!"
                    query_file_path_oh.write('>%s\n%s\n'%(pbRead, readSeq))
                query_file_path_oh.close()

                # My gap start will change when I clip out the gap flanking region (won't be the same size), need to account for this
                newGapStart = gapSpan[0] - spanLeft
                newGapEnd   = gapSpan[1] - spanLeft
                self.blat_info_list.append( (newGapStart, newGapEnd, gapID, target_file_path, query_file_path) )
                print "Gapstart: %s\tGapend: %s\tgapID: %s\n"%(newGapStart, newGapEnd, gapID)
            #####
        #####
        return self.blat_info_list

#==============================================================

def runGapCrossingBlat( gapSTART, gapEND, gapID, tmpPath, query_file_path, target_file_path ):
    
    # This awk cmd ensures that the read alignment starts BEFORE the gap and ends AFTER the gap, ensuring that it actually crosses
    screenCmd  = "python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/gapCrosser.py %s %s | awk '{count[$1]++} END{for (x in count) {print \"%s\", x >> \"%s/%s.gap.blat\"}}'"%( gapSTART, gapEND, gapID, tmpPath, gapID )
    rmFilesCmd = "rm -f %s %s"%( target_file_path, query_file_path )
    
    blat_cmd   = "module load blat; blat -minMatch=3 -tileSize=8 -maxIntron=20000 -repMatch=10000 -noHead -minIdentity=75 -extendThroughN -mask=lower %s %s stdout | %s; %s"%( target_file_path,  query_file_path, screenCmd, rmFilesCmd )
    subprocess.call(blat_cmd, shell=True)
    
#==============================================================


def returnGapCrossingBlatCmds( basePath, assemblyFile, tmpPath, FOFN, cmd_log, MAX_READS_TO_GAP, MAX_ISLAND_SIZE, REQUIRED_CANU_COV ):

    # Clean up my blat files
    if not isfile( '%s/blat.bz2'%(tmpPath) ):
        system( 'find %s -name "tmp*.blat.bz2" -exec cat {} >> %s/blat.bz2 \;'%(tmpPath, tmpPath))
        system( 'find %s -name "tmp*.blat.bz2" -exec rm -f {} \;'%(tmpPath) )
    #####

    # Need to combine all of my tmp pb.fasta files
    if not isfile('%s/pb.fasta'%tmpPath):
        # Chris Plott removed "/bin/ls %s/pbFiles| grep "tmpFile""  and replaced it with /bin/ls %s/tmpFile*.fasta.bz2
        subprocess.call('for x in $(/bin/ls %s/tmpFile*.fasta.bz2); do bzcat -kc $x >> %s/pb.fasta; done'%( tmpPath, tmpPath ), shell=True)
    #####
    
    # Defining distance from gap edge to grab for blat 
    FLANK_DISTANCE = 10000
    
    # Creating an index dictionary for the assembly and pb reads
    assemblyIndexDict = FASTAFile_dict( join(basePath,assemblyFile) )
    
    
    # This is slowing down my file writing with IO, just going to bring all reads into memory and request more MEM on genepool
    readsDict = defaultdict(str)
    for r in iterFASTA(open('%s/pb.fasta'%tmpPath)):
        readsDict[r.id] = str(r.seq)
    #####
        
    # Assigning reads to gap flanking fragments
    # One time this wreaked havoc on me so I check for poorly formatted gapCrosser file now
    # I am writing to this one file from multiple processes, sometimes it mixes things up
    true_scaffolds = set([r.id for r in iterFASTA(open(assemblyFile))])
    weird_lines_in_gapCrossers_oh = open('weird_lines_in_gap_crosser.dat', 'w')
    scaffold_dict = {}
    for line in open( join(tmpPath,'gapCrossers.dat'), 'r' ):
        try:
            scaffID = line.strip().split()[1].rsplit('_', 1)[0]
            if scaffID not in true_scaffolds:
                weird_lines_in_gapCrossers_oh.write('%s\n'%(line)) 
                continue
            pbRead  = line.strip().split()[0]
            gap     = line.strip().split()[1]
            try:
                scaffold_dict[scaffID].add_blat( pbRead, gap )
            except KeyError:
                scaffold_dict[scaffID] = Scaffold( scaffID )
                scaffold_dict[scaffID].add_blat( pbRead, gap )
            #####
        #####
        except IndexError:
            weird_lines_in_gapCrossers_oh.write('%s\n'%(line))
        #####
    #####
    weird_lines_in_gapCrossers_oh.close()

    # Need to generate and return my parent data
    parent_gap_oh = open('%s/parent_gaps.dat'%tmpPath, 'w')
    for scaffID, scaffold in scaffold_dict.iteritems():
        parent_gap_dict = scaffold.return_parent_gap_dict( MAX_ISLAND_SIZE, assemblyIndexDict )
        for contigSpan, tmpList in parent_gap_dict.iteritems():
            for gapID in tmpList:
                parent_gap_oh.write('%s\t\t%s\n'%(contigSpan,gapID))
            #####  
        #####
    #####
    parent_gap_oh.close()

    all_cmds_list = []
    masterCmdList = []
    progPath      = "/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir"
    
    for scaffID, scaffold in scaffold_dict.iteritems():
        blat_info_list = scaffold.return_blat_info( basePath, tmpPath, progPath, assemblyFile, FLANK_DISTANCE, assemblyIndexDict, MAX_READS_TO_GAP, readsDict, REQUIRED_CANU_COV )
        for gapSTART, gapEND, gapID, target_file_path, query_file_path in blat_info_list:
            all_cmds_list.append( "python -c \"import patch_lib; patch_lib.runGapCrossingBlat('%s', '%s', '%s', '%s', '%s', '%s')\"\n"%( gapSTART, gapEND, gapID, tmpPath, query_file_path, target_file_path) )
        #####
    #####
    
    # I am splitting the commands up so I don't overload queue system
    command_chunk_size = 20
    index              = 0
    cmdCount           = len(all_cmds_list)
    while index < cmdCount:
        masterCmdList.append(all_cmds_list[index:(index + command_chunk_size)])
        index += command_chunk_size
    #####
    
    # Writing out the blat commands to log
    writeAndEcho( '\n-----------------------\n- FINDING GAP CROSSING READS:', cmd_log )
    for tmpList in masterCmdList:
        for cmd in tmpList:
            writeAndEcho( cmd, cmd_log)
        #####
    #####
    
    # Clearing out old file
    if isdir( '%s/gap_blat_array_files'%tmpPath ):
        system('rm -rf %s/gap_blat_array_files'%tmpPath)
    #####

    return masterCmdList
    
#==============================================================

def returnBWAcmds( tmpPath, cmd_log, basePath, assemblyFilePath, n_proc ):
    
    # Moving assembly to tmpPath
    assemblyFile = assemblyFilePath.split('/')[-1]
    if not isfile('%s/%s'%(tmpPath, assemblyFile)):
        system('cp %s %s'%(assemblyFilePath, tmpPath))
    #####

    # First splitting the assembly into contigs
    min_contig_size = 1
    system('python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/breakIntoContigs.py %s -m %s > %s/contigs.fasta'%(assemblyFilePath, min_contig_size, tmpPath))
    
    # Next pulling contig ends
    system('python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/trimEnds.py %s/contigs.fasta 10000 %s/contigs.trimmed.fasta'%(tmpPath, tmpPath))
    
    # Index the contig ends
    index_cmd = "module load bwa/0.7.12; bwa index %s/contigs.trimmed.fasta"%(tmpPath)
    system(index_cmd)
    
    # The prefix will be contigs.trimmed.fasta
    prefix = 'contigs.trimmed.fasta'

    # Creating output directory for bam files
    if not isdir('%s/bam'%(tmpPath)):
        system('mkdir %s/bam'%(tmpPath))
    #####
    system('rm -f %s/bam/*'%(tmpPath))

    # Generate a bwa cmd for every fasta file, filtering non hits
    cmdCount      = 0
    bwaCmdsList   = []
    fastaNameFile = open( '%s/fastaNameFile.dat' %tmpPath ,'r')
    for line in fastaNameFile:
        tmpList   = [ 'cd %s\n'%(tmpPath), 'module load bwa/0.7.12\n', 'module load samtools\n' ]
        fastaFile = line.strip()
        bwaCMD    = "bzcat %s | bwa mem -t %s -x pacbio %s /dev/stdin | samtools view -Sb -F 4 /dev/stdin > %s/bam/%s_bwa.bam"%(fastaFile, n_proc, prefix, tmpPath, cmdCount)
        tmpList.append( bwaCMD )
        bwaCmdsList.append( tmpList )
        cmdCount += 1
    #####
    # Writing out the bwa commands to log
    writeAndEcho( '\n-----------------------\n- ALIGNING PB READS TO ASSEMBLY:', cmd_log )
    for tmpList in bwaCmdsList:
        for cmd in tmpList:
            writeAndEcho( cmd, cmd_log)
        #####
    #####
   
    # Return bwa commands
    return bwaCmdsList

#==============================================================

def returnCanuCmds( basePath, tmpPath, assemblyFilePath, FOFN, min_scaff_size, required_canu_cov, pb_fofn, n_proc, cmd_log ):
    
    '''This function will split the assembly evenly for canu jobs'''
    
    cmdsList       = []
    scaffold_sizes = []
    scaffold_lists = []
    totalBases     = 0
    for scaffold in iterFASTA(open(assemblyFilePath, 'r')):
        scaff_length = len(scaffold.seq)
        totalBases  += len(scaffold.seq)
        if scaff_length >= int(min_scaff_size):
            scaffold_sizes.append((scaff_length, scaffold.id))
        #####
    #####
    # Figuring out how to split up these files into jobs
    # This is goofy, I'll need to change this later
    total_procs_to_use = 10000
    job_count          = int( total_procs_to_use/float(n_proc) )
    target_size        = totalBases/float(job_count)

    scaffold_sizes.sort(reverse=True)
    current_size = 0
    tmpList      = []
    for scaffTuple in scaffold_sizes:
        tmpList.append( scaffTuple[1] )
        current_size += scaffTuple[0]
        if current_size >= target_size:
            current_size = 0
            scaffold_lists.append( tmpList )
            tmpList = []
        #####
    #####
    for scaffIDList in scaffold_lists:
        scaffIDstring = '|'.join(scaffIDList)
        cmdsList.append( ["python -c\"import patch_lib; patch_lib.runScaffoldCanu('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')\""%( basePath, tmpPath, assemblyFilePath, FOFN, scaffIDstring, pb_fofn, required_canu_cov, n_proc )] )
        #####
    #####
    
    # Writing out the blat commands to log
    writeAndEcho( '\n-----------------------\n- GENERATING CANU CONSENSUS:', cmd_log )
    for tmpList in cmdsList:
        for cmd in tmpList:
            writeAndEcho( cmd, cmd_log)
        #####
    #####
    
    # Creating a directory to work in
    system('rm -rf %s/canu'%(tmpPath))
    system('mkdir %s/canu'%(tmpPath))
    
    return cmdsList

#==============================================================


def runScaffoldCanu( basePath, tmpPath, assemblyFilePath, FOFN, scaffIDstring, pb_fofn, required_canu_cov, n_proc ):

    # Scaffolds to run    
    scaffIDList = scaffIDstring.split('|')
    scaffIDSet  = set(scaffIDList)
    
    
    # Bringing in my reads to gap dictionary
    reads_to_gap_dict = pickle.load( open( "%s/reads_to_gaps_dict.pickle"%(tmpPath), "rb" ) )

    # Bringing in my pac bio reads as either corrected reads or primary reads from pbio data
    readsIndexDict = FASTAFile_dict( '%s/pb.fasta'%tmpPath )
    
    for scaffID in scaffIDSet:
    
        for gapID, readSet in reads_to_gap_dict.iteritems():
        
            scaff, gapNum = gapID.rsplit('_', 1)
            
            if scaff != scaffID: continue
            
            if len(readSet) < int(required_canu_cov): continue
            
            # Creating a working directory
            print "There were enough reads, creating a directory..."
            system('mkdir %s/canu/%s'%(tmpPath, gapID))
            
            # Writing out my fasta
            read_sizes = []
            reads_fasta_oh = open('%s/canu/%s/reads.fasta'%(tmpPath, gapID), 'w')
            for read in readSet:
                seq = str(readsIndexDict[read].seq)
                read_sizes.append(len(seq))
                reads_fasta_oh.write('>%s\n%s\n'%(read, seq))
            #####
            reads_fasta_oh.close()
            
            # Using average read length as the estimated contig size for gap filling (skewed for multigaps, should fix later if causing problems)
            average_read_length = sum(read_sizes)/float(len(read_sizes))

            # Generating and running my canu command
            maxMemory = 5*int(n_proc)
            canuCmd   = 'canu -p %s -d %s/canu/%s genomeSize=%s corMinCoverage=0 corMhapSensitivity=high minOverlapLength=500 useGrid=0 maxMemory=%s maxThreads=%s -pacbio-raw %s/canu/%s/reads.fasta'%( gapID, tmpPath, gapID, 40000, maxMemory, n_proc, tmpPath, gapID)

            cmdOH     = open('%s/canu/%s/cmd'%(tmpPath, gapID), 'w')
            cmdOH.write('%s'%canuCmd)
            cmdOH.close()
                    
            system('module load canu; %s'%canuCmd)
        
            # Pulling out my consensus sequence
            consensusFile = '%s/canu/%s/%s.contigs.fasta'%(tmpPath, gapID, gapID)
            all_consensus_reads = []
            biggest       = 0
            bestSeq       = ''
            if isfile(consensusFile):
                if int(stat(consensusFile).st_size) > 0:
                    for r in iterFASTA(open(consensusFile)):
                        all_consensus_reads.append(str(r.seq))
                        if len(str(r.seq)) > biggest:
                            bestSeq = str(r.seq)
                            biggest = len(str(r.seq))
                        #####
                    #####
                #####
            #####
            unassembledFile = '%s/canu/%s/%s.unassembled.fasta'%(tmpPath, gapID, gapID)
            if (isfile(unassembledFile)):
                if int(stat(unassembledFile).st_size) > 0:
                    for r in iterFASTA(open(unassembledFile)):
                        reads_utilized = int(((str(r.description)).split(None))[1].split('=')[1])
                        if (reads_utilized > 0):
                            all_consensus_reads.append(str(r.seq))
                        if (reads_utilized > 0) and (len(str(r.seq)) > biggest):
                            bestSeq = str(r.seq)
                            biggest = len(str(r.seq))
                        #####
                    #####
                #####
            #####
            if (len(bestSeq) == 0):
                system('rm -rf %s/canu/%s'%(tmpPath, gapID))
                continue
            #####
            else:
                # Now I'm writing all of my consensus pieces to file
                consensusOH = open('%s/%s_patches.dat'%(tmpPath, scaffID), 'a')
                for i, seq in enumerate(all_consensus_reads):
                    consensusOH.write('>%s_%s\n%s\n'%(gapID, i, seq))
                #####
                consensusOH.close()
            #####
            
            # Now I need to get rid of this folder because canu creates alot of temp files
            system('rm -rf %s/canu/%s'%(tmpPath, gapID))
        #####
    #####
#==============================================================

def return_patch_align_cmd( basePath, tmpPath, assemblyFilePath ):
    
    # Combing all of my patches from canu
    if not isfile('%s/patches.fasta'%(tmpPath)):
        system('find %s -name "*patches.dat" -exec cat {} >> %s/patches.fasta \;'%(tmpPath, tmpPath))
        system('find %s -name "*patches.dat" -exec rm -f {} \;'%(tmpPath))
    #####
    
    # First break the assembly into contigs
    minContigSize = 1
    system( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/breakIntoContigs.py %s -m %d > %s/contigs.fasta'%(assemblyFilePath, minContigSize, tmpPath) )
    
    # Trimming the contig ends
    clip_size = 1000
    system( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/trimEnds.py %s/contigs.fasta %d %s/contigs.trimmed.fasta'%(tmpPath, clip_size, tmpPath) )
    
    # Break the clipped ends into 100mers
    merSize = 100
    oh      = open( '%s/trimmedMarkers.fasta'%(tmpPath), 'w' )
    merSet  = set()
    x       = iterCounter(100000)
    halfMer = merSize / 2
    for r in iterFASTA(open('%s/contigs.trimmed.fasta'%(tmpPath))):
        tmpSeq = str(r.seq).upper()
        nBases = len(tmpSeq)
        for n in xrange( int(nBases/50) ):
            start = halfMer * n
            end   = halfMer * (n+2)
            if ( end > nBases ): break
            markerSeq = tmpSeq[halfMer*n:halfMer*(n+2)]
            if ( (markerSeq in merSet) or (revComp(markerSeq) in merSet) ): continue
            tmpRecord = SeqRecord( id="%s_%d"%(r.id,n), seq=sorted([markerSeq,revComp(markerSeq)])[0], description=''  )
            writeFASTA( [tmpRecord], oh)
            x()
        #####
    #####
    oh.close()
    #####
    
    # Indexing the patches fasta
    system('cd %s;module load bwa; bwa index -p GAP_FILLERS -a bwtsw %s/patches.fasta'%(tmpPath, tmpPath))
    
    # Generating the alignment command
    align_cmd = 'module load bwa; rm -f %s/trimmedMarkers.sai ; cd %s; bwa aln -t 32 GAP_FILLERS %s/trimmedMarkers.fasta > %s/trimmedMarkers.sai'%(tmpPath, tmpPath, tmpPath, tmpPath)
    
    return align_cmd
    
#==============================================================

def parseSam( basePath, tmpPath, assemblyFilePath ):
    
    progPath      = "/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir"
    
    if not isfile('%s/trimmedMarkers.sam'%(tmpPath)):
        cmd = 'module load bwa; module load samtools; cd %s; bwa samse -n 255 GAP_FILLERS %s/trimmedMarkers.sai %s/trimmedMarkers.fasta | samtools view -S -F 4 /dev/stdin > %s/trimmedMarkers.sam'%(tmpPath, tmpPath, tmpPath, tmpPath)
        system(cmd)
    #####
    
    parseScaffolds = re.compile( r'(scaffold_\d+\|\d+)' ).findall
    parseFront     = re.compile( r'(L|R|W)' ).findall
    parseContig    = re.compile( r'\|(contig=\d+)_(\d+)' ).findall
    moleculoDict   = {}
    x              = iterCounter(100000)
    for line in open( '%s/trimmedMarkers.sam'%(tmpPath) ):

        x()

        # Pulling the information
        # R__scaffold_1|contig=1_1
        splitLine    = line.split(None)
        moleculoID   = splitLine[2]
        contigID     = splitLine[0].rsplit('_', 1)[0]

#         scaffoldList = parseScaffolds( splitLine[0] )
#         RLW          = parseFront( splitLine[0] )[0]
#         tmpContig    = parseContig( splitLine[0] )[0]
#         contigID = "%s__%s|%s"%( RLW, '-'.join(scaffoldList), tmpContig[0] )
        if ( moleculoID in moleculoDict ):
            try:
                moleculoDict[moleculoID][contigID] += 1
            except KeyError:
                moleculoDict[moleculoID][contigID] = 1
            #####
        else:
            moleculoDict[moleculoID] = {}
            moleculoDict[moleculoID][contigID] = 1
        #####

        # Creating the temporary dictionary
        tmpDict = {}
        for item in splitLine[11:]:
            key, dummy, value = item.split(':')
            tmpDict[key] = value
        #####

        # Writing the information to the dictionary
        try:
            for item in tmpDict['XA'][:-1].split(';'): 
                splitItem  = item.split(',')
                moleculoID = splitItem[0]
                score      = splitItem[2]
                if ( moleculoID in moleculoDict ):
                    try:
                        moleculoDict[moleculoID][contigID] += 1
                    except KeyError:
                        moleculoDict[moleculoID][contigID] = 1
                    #####
                else:
                    moleculoDict[moleculoID] = {}
                    moleculoDict[moleculoID][contigID] = 1
                #####
            #####
        except KeyError:
            pass
        #####
    #####

    # Writing the reads file
    oh = open( '%s/PACBIO_to_marker_mapping_with_counts.dat'%(tmpPath), 'w' )
    moleculoSet = set()
    for moleculoID, tmpDict in moleculoDict.iteritems():
        for contigID, nCount in tmpDict.iteritems():
            if int(nCount) >= 3:
                moleculoSet.add(moleculoID)
                oh.write( '%s\t%s\t%d\n'%( moleculoID, contigID, nCount ) )
            #####
        #####
    #####
    oh.close()
    
    # writing out the pbRead names
    oh = open( '%s/PACBIO_Names.fasta'%(tmpPath), 'w' )
    for r in iterFASTA(open('%s/patches.fasta'%(tmpPath))):
        if ( r.id in moleculoSet ): 
            writeFASTA( [r], oh )
        #####
    #####
    oh.close()
#####
    
#==============================================================
def alignmentStats( alignment ):
    
    splitLine  = alignment.split(None)
    
    matches    = int( splitLine[0] )
    repMatches = int( splitLine[2] )
    
    totalMatch = matches + repMatches
    
    strand = splitLine[8]
    
    inserts = int(splitLine[5]) + int(splitLine[7])
    Q_id    = splitLine[9]
    Q_size  = int( splitLine[10] )
    Q_start = int( splitLine[11] )
    Q_end   = int( splitLine[12] )
    Q_len   = int( abs( Q_start - Q_end ) )

    T_id    = splitLine[13]
    T_size  = int( splitLine[14] )
    T_start = int( splitLine[15] )
    T_end   = int( splitLine[16] )
    T_len   = int( abs( T_start - T_end ) )

    ID_percent = 100.0 * float(totalMatch) / float( min( Q_len, T_len ) )
    
    Q_COV = 100.0 * float(Q_len) / float(Q_size)
    T_COV = 100.0 * float(T_len) / float(T_size)
    COV   = max( Q_COV, T_COV )
    
    return { 'totalMatch':totalMatch, \
             'Q_id':Q_id, 'Q_size':Q_size, 'Q_start':Q_start, 'Q_end':Q_end, \
             'T_id':T_id, 'T_size':T_size, 'T_start':T_start, 'T_end':T_end, \
             'ID_percent':ID_percent, 'strand':strand, 'COV':COV, 'inserts':inserts }

#==============================================================

def fillGap( prevKey, currentKey, bigContigDict, pbReadList, pbReadIndex, actualGapSize, tmpPath, pid ):
            
    alignLength = 1000
    minOverlap  = 400
    maxOverhang = 200
    minIdentity = 98.0
    
    # Writing each contig to a file
    firstContig  = bigContigDict[prevKey]
    fr           = SeqRecord( id='firstContig', seq=firstContig[-alignLength:], description='' )
    writeFASTA( [fr], open('%s/firstContig_%s.fasta'%(tmpPath, pid),'w') )

    # Writing each contig to a file
    secondContig = bigContigDict[currentKey]
    sr           = SeqRecord( id='secondContig', seq=secondContig[:alignLength], description='' )
    writeFASTA( [sr], open('%s/secondContig_%s.fasta'%(tmpPath, pid),'w') )
    
    # Write out the pb reads to a file
    oh         = open( '%s/tmpReads_%s.fasta'%(tmpPath, pid), 'w' )
    
    for pbReadID in pbReadList:
        r          = pbReadIndex[pbReadID]
        # Writing regular read
        writeFASTA( [r], oh )
        # Writing revComp read
        r.seq = revComp(str(r.seq).upper())
        r.id  = '%s_rc'%r.id
        writeFASTA( [r], oh )
    #####
    oh.close()
    
    # Checking to see if we wrote any reads, if not then return false
    if len(pbReadList) == 0:
        return ( False, '', '', '' )
    #####
        
    # Align all pbReads to the left side of the gap
    cmd        = 'blat -extendThroughN -noHead %s/firstContig_%s.fasta %s/tmpReads_%s.fasta stdout'%(tmpPath, pid, tmpPath, pid)
    x          = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    anchorDict = {}
    for line in x.stdout:

        # Computing the alignment statistics
        tmpAlign = alignmentStats(line)

        # Reading in the blat alignment information
        if ( tmpAlign['strand'] == '-' ): continue
        
        if tmpAlign['inserts'] > 100: continue
        
        # Moleculo Info
        readID      = tmpAlign['Q_id']
        readLength  = tmpAlign['Q_size']
        readStart   = tmpAlign['Q_start']
        readEnd     = tmpAlign['Q_end']
        
        # Contig Info
        contigID    = tmpAlign['T_id']
        contigLngth = tmpAlign['T_size']
        contigStart = tmpAlign['T_start']
        contigEnd   = tmpAlign['T_end']
        
        # At least one end of the contig must be within 200bp of the scaffold gap end
        if ( (contigStart > maxOverhang) and ((contigLngth-contigEnd) > maxOverhang) ): continue
        
        # Screening for short anchor distances
        leftAnchorDist = readEnd - readStart
        if ( leftAnchorDist < minOverlap ):continue
        
        # Checking the quality of the alignment
        if ( tmpAlign['ID_percent'] < minIdentity ): continue
        
        # Storing the information
        if ( readID in anchorDict ):
            if ( leftAnchorDist > anchorDict[readID][0][0] ): anchorDict[readID][0] = (leftAnchorDist,line)
        else:
            anchorDict[readID] = [(leftAnchorDist,line),(0,None)]
        #####
    #####
    x.poll()

    # Align all pbReads to the right side of the gap
    cmd = 'blat -extendThroughN -noHead %s/secondContig_%s.fasta %s/tmpReads_%s.fasta stdout'%(tmpPath, pid, tmpPath, pid)
    x   = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    for line in x.stdout:

        # Computing the alignment statistics
        tmpAlign = alignmentStats(line)

        # Reading in the blat alignment information
        if ( tmpAlign['strand'] == '-' ): continue
        
        if tmpAlign['inserts'] > 100: continue
        
        # Moleculo Info
        readID      = tmpAlign['Q_id']
        readLength  = tmpAlign['Q_size']
        readStart   = tmpAlign['Q_start']
        readEnd     = tmpAlign['Q_end']
        
        # Contig Info
        contigID    = tmpAlign['T_id']
        contigLngth = tmpAlign['T_size']
        contigStart = tmpAlign['T_start']
        contigEnd   = tmpAlign['T_end']
        
        # At least one end of the contig must be within 200bp of their end
        if ( (contigStart > maxOverhang) and ((contigLngth-contigEnd) > maxOverhang) ): continue
        
        # Screening for short anchor distances
        rightAnchorDist = readEnd - readStart
        if ( rightAnchorDist < minOverlap ): continue

        # Checking the quality of the alignment
        if ( tmpAlign['ID_percent'] < minIdentity ): continue

        # Storing th einformation
        if ( readID in anchorDict ):
            if ( rightAnchorDist > anchorDict[readID][1][0] ): anchorDict[readID][1] = (rightAnchorDist,line)
        else:
            anchorDict[readID] = [(0,None),(rightAnchorDist,line)]
        #####
    #####
    x.poll()
    
    
    # Choosing the best anchored sequence
    DSU = []
    for readID, tmpList in anchorDict.iteritems():
        leftAnchor, leftBLAT  = tmpList[0]
        if ( leftBLAT == None ): continue
        rightAnchor, rightBLAT = tmpList[1]
        if ( rightBLAT == None ): continue
        DSU.append( ((leftAnchor+rightAnchor),readID,leftBLAT,rightBLAT) )
    #####
    
    # If nothing to do then return false
    if ( DSU == [] ): return (False,'','','')
    DSU.sort(reverse=True)
    dsu_oh = open('DSU_output.dat', 'a')
    for tmpTuple in DSU:
        dsu_oh.write('\n\nANCHOR = %s\nREADID = %s\nleftBLAT = %s\nrightBLAT = %s\n\n'%(tmpTuple[0], tmpTuple[1], tmpTuple[2], tmpTuple[3]))
    totalAnchorDist, tmpPBreadID, leftBLAT, rightBLAT = DSU[0]
    dsu_oh.close()

    # Anchoring the sequence
    leftBLAT_split = leftBLAT.split(None)
    L_contigStart  = int(leftBLAT_split[15]) + len(firstContig) - alignLength
    L_contigEnd    = int(leftBLAT_split[16]) + len(firstContig) - alignLength
    L_readStart    = int(leftBLAT_split[11])
    L_readEnd      = int(leftBLAT_split[12])
    
    rightBLAT_split = rightBLAT.split(None)
    R_contigStart   = int(rightBLAT_split[15])
    R_contigEnd     = int(rightBLAT_split[16])
    R_readStart     = int(rightBLAT_split[11])
    R_readEnd       = int(rightBLAT_split[12])

    # Performing the patching
    if ( tmpPBreadID[-3:] == '_rc' ):
        PBreadSeq   = revComp( str(pbReadIndex[tmpPBreadID[:-3]].seq).upper() )
    else:
        PBreadSeq   = str(pbReadIndex[tmpPBreadID].seq).upper()
    #####
    
    leftContigSeq = firstContig[:L_contigStart]
    rghtContigSeq = secondContig[R_contigEnd:]
    nBases        = len(PBreadSeq)

    # This would mean that we have a mixed up gap and I don't want to fix it!    
    if ( L_readStart >= R_readEnd ): return (False,'','','')
    
    # Extracting the patched sequence
    patchedSeq    = PBreadSeq[L_readStart:R_readEnd]

    return ( True, leftContigSeq, patchedSeq, rghtContigSeq )
    
#==============================================================

def patch_scaffolds( tmpTuple ):
    
    pid = getpid()
    
    tmpPath, assemblyFilePath, scaffList = tmpTuple
    
    print scaffList
    
    print "Creating index dictionaries..."
        
    # Indexing the moleculo
    pbIndex = FASTAFile_dict('%s/PACBIO_Names.fasta'%(tmpPath))
    
    # Breaking the contigs into their pieces
    minContigLength = 1
    
    
    sepString = '%s\n' % ( 50 * '=' )
    
    outputOrder = ['CHR_ID',\
                   'GapNum', \
                   'GapType', \
                   'GapStart', \
                   'GapEnd', \
                   'Contig_L', \
                   'Contig_R', \
                   'GapSize', \
                   'SmallCntBases', \
                   'N_moleculo', \
                   'PatchedBases', \
                   'BasesGained', \
                   'Status', \
                   'molecReads']
                   
                   
                   
    # Log and output file
    logFile      = '%s/patching.log'%(tmpPath)
    
    # Regular expressions
    contigBreakFinder = re.compile( r'[ATCGatcg][Nn]+[ATCGatcg]' ).finditer
    pullID            = re.compile( r'(W|L|R)__(Araip.B\d+)\|contig=(\d+)' ).findall

    # Reading in the moleculo alignments
    pbReadDict = {}
    print "Creating pbReadDict..."
    for line in open('%s/PACBIO_to_marker_mapping_with_counts.dat'%(tmpPath)):
        splitLine  = line.split(None)
        pbReadID   = splitLine[0]
        markerID   = splitLine[1]
        orient     = markerID.split('__')[0]
        scaffID    = (markerID[3:]).split('|')[0]
        contigNum  = markerID.split('=')[1]
        strand     = '+'
        # Working on one scaff at a time
        if ( pbReadID in pbReadDict ):
            try:
                pbReadDict[pbReadID][scaffID].append( (int(contigNum),orient,strand) )
            except KeyError:
                pbReadDict[pbReadID][scaffID] = [ (int(contigNum),orient,strand) ]
            #####
        else:
            pbReadDict[pbReadID]        = {}
            pbReadDict[pbReadID][scaffID] = [ (int(contigNum),orient,strand) ]
        #####
    #####
    
    print "pbReadDict keys: %s"%(len(pbReadDict.keys()))
    
    # Determining which gaps are being filled
    print "Creating gapCloserDict..."
    gapCloserDict = {}
    RW            = set( 'RW' )
    LW            = set( 'LW' )
    for pbReadID, tmpDict in pbReadDict.iteritems():
        for scaffID, tmpList in tmpDict.iteritems():
            # Determining which gaps are closed
            tmpList.sort()
            for i in xrange( len(tmpList) - 1 ):
                contigNum_i, orient_i, strand_i = tmpList[i]
                contigNum_j, orient_j, strand_j = tmpList[i+1]
                if ( ( (contigNum_j - contigNum_i) == 1) and ( (orient_i in RW)  and (orient_j in LW) ) ):
                    gapKey = "%s|contig=%d_%s|contig=%d"%(scaffID,contigNum_i,scaffID,contigNum_j)
                    if ( scaffID in gapCloserDict ):
                        try:
                            gapCloserDict[scaffID][gapKey].append( pbReadID )
                        except KeyError:
                            gapCloserDict[scaffID][gapKey] = [ pbReadID ]
                        #####
                    else:
                        gapCloserDict[scaffID] = {}
                        gapCloserDict[scaffID][gapKey] = [ pbReadID ]
                    #####
                #####
            #####
        #####
    #####
    print "gapCloserDict keys: %s"%(len(gapCloserDict))
                       
    for r in iterFASTA(open(assemblyFilePath)):
        scaffID = r.id
        if scaffID not in scaffList:
            print "%s not in scaffList"%(scaffID)
            continue
        #####
        if (scaffID not in gapCloserDict.keys()):
            print "Scaffold %s had no reads aligned..."%(scaffID)
            new_scaff_oh = open('%s/new_scaffs/%s.fasta'%(tmpPath, scaffID), 'w')
            new_scaff_oh.write('>%s\n%s\n'%(scaffID, r.seq))
            new_scaff_oh.close()
            continue
        #####
        
        # Pulling the sequence
        tmpSeq = str(r.seq)
        nBases = len(tmpSeq)
        
        # Creating the break indices
        d = [item.span() for item in contigBreakFinder(tmpSeq)]
        d.insert( 0, (0,0) )
        d.append( (nBases,nBases) )
                
        # Locating the small contigs among the large
        gapKey         = "$"
        prevLarge      = "$"
        nContig        = 1
        state          = None
        bigContigDict  = {}
        finalSeq       = []
        for n in xrange( len(d) - 1 ):
            # Finding the upstream and downstream N's
            upstream_Ns  = tmpSeq[d[n][0]:d[n][1]]
            downstrm_Ns  = tmpSeq[d[n+1][0]:d[n+1][1]]
            
            # Transitioning the state
            contigLength = d[n+1][0] - d[n][1]
            
            # Pulling the contigSeq
            contigSeq = tmpSeq[d[n][1]:d[n+1][0]]
            
            # Initializing the output dictionary
            outputDict = {'CHR_ID': scaffID, \
                          'GapNum':'%d'%n, \
                          'GapType':'%d'%0, \
                          'GapStart':'%d'%d[n][0], \
                          'GapEnd':'%d'%d[n][1], \
                          'Contig_L':'%d'%0, \
                          'Contig_R':'%d'%0, \
                          'GapSize':'%d'%0, \
                          'SmallCntBases':'%d'%0, \
                          'N_moleculo':'%d'%0, \
                          'PatchedBases':'%d'%0, \
                          'BasesGained':'%d'%0, \
                          'Status':'None', \
                          'molecReads':'' }


            if ( state == 'LARGE' ):
                outputDict['GapType'] = 'LL'
                # Creating the key
                newKey = "%s|contig=%d"%(scaffID,nContig)
                gapKey = "%s_%s"%(prevLarge,newKey) 
                bigContigDict[newKey] = contigSeq
                gapSize = len(upstream_Ns)
                # Getting some output
                outputDict['Contig_L']      = prevLarge
                outputDict['Contig_R']      = newKey
                outputDict['GapSize']       = '%d'%gapSize
                if ( gapKey in gapCloserDict[scaffID] ):
                    outputDict['N_moleculo'] = '%d'%len(gapCloserDict[scaffID][gapKey])
                    outputDict['molecReads'] = ','.join(gapCloserDict[scaffID][gapKey])
                    success, L_seq, fillerSeq, R_seq = fillGap( prevLarge, newKey, bigContigDict, gapCloserDict[scaffID][gapKey], pbIndex, gapSize, tmpPath, pid )
                    if ( success ):
                        patchedBases = len(fillerSeq)
                        outputDict['PatchedBases'] = '%d'%patchedBases
                        outputDict['BasesGained']  = '%d'%patchedBases
                        outputDict['Status'] = 'PATCHED'
                        finalSeq[-1] = L_seq
                        finalSeq.append( fillerSeq )
                        finalSeq.append( R_seq )
                    else:
                        outputDict['Status'] = 'NOALIGNS'                            
                        finalSeq.append( upstream_Ns )
                        finalSeq.append( contigSeq )
                    #####
                else:
                    outputDict['Status'] = 'NOREADS'
                    finalSeq.append( upstream_Ns )
                    finalSeq.append( contigSeq )
                #####
            # None -> LARGE
            elif ( state == None ):
                print "FIRST contig found"
                outputDict['GapType'] = 'NL'
                # Creating the key
                newKey = "%s|contig=%d"%(scaffID,nContig)
                bigContigDict[newKey] = contigSeq
                # Output info
                outputDict['Contig_L'] = 'None'
                outputDict['Contig_R'] = newKey
                outputDict['GapSize']  = '%d'%len(upstream_Ns)
                outputDict['Status']   = 'FIRST'
                # Adding the sequence
                finalSeq.append( contigSeq )
            #####
            
            # Writing to the main output file
            oh_main = open(logFile, 'a')
            oh_main.write( '%s\n'%( '\t'.join( [outputDict[item] for item in outputOrder] ) ) )
            stdout.write( '%s\n'%( '\t'.join( [outputDict[item] for item in outputOrder] ) ) )
            oh_main.close()
            
            #Updating the values
            prevLarge = "%s|contig=%d"%(scaffID,nContig)
            nContig  += 1
            state     = "LARGE"
            #####
        #####
    
        # Write the new sequence to a file
        newChromoSeq = ''.join( finalSeq )
        new_scaff_oh = open('%s/new_scaffs/%s.fasta'%(tmpPath, scaffID), 'w')
        new_scaff_oh.write('>%s\n%s\n'%(scaffID, newChromoSeq))
        new_scaff_oh.close()

    

#==============================================================
    
def make_patches( tmpPath, basePath, assemblyFilePath ):
    
    progPath      = "/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir"
    
    # Making a new directory to store scaffolds
    if not isdir('%s/new_scaffs'%(tmpPath)):
        system('mkdir %s/new_scaffs'%(tmpPath))
    #####

    # I need to separate my scaffolds into equal partitions so I can run them in parallel
    scaff_size_tuples = [(len(str(r.seq)), r.id) for r in iterFASTA(open(assemblyFilePath))]
    print "Scaff tuples: %s"%(len(scaff_size_tuples))
    total_seq         = sum([x[0] for x in scaff_size_tuples])
    target_seq        = total_seq / 32
    random.shuffle(scaff_size_tuples)
    scaff_lists = []
    tmpList     = []
    while True:
        if (sum([x[0] for x in tmpList]) > target_seq):
            print "Appended a list..."
            scaff_lists.append( (tmpPath, assemblyFilePath, [x[1] for x in tmpList]) )
            tmpList = []
            tmpList.append(scaff_size_tuples.pop())
        #####
        if  (len(scaff_size_tuples) == 0):
            scaff_lists.append( (tmpPath, assemblyFilePath, [x[1] for x in tmpList]) )
            break
        #####
        tmpList.append(scaff_size_tuples.pop())
    #####
    print 'Scaff lists count: %s'%(len(scaff_lists))
    
    total_scaffs = sum([len(x[2]) for x in scaff_lists])
    print "Total scaffs = %s"%total_scaffs
    
    
    # Creating worker pool
    print "Sending to pool..."
    pool = mp.Pool(processes=32)
    pool.imap(patch_scaffolds, scaff_lists)
    pool.close()
    pool.join()
    
#==============================================================
