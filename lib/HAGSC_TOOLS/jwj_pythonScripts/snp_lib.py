__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/28/15"

from hagsc_lib import isGzipFile, isBzipFile, commify, IntervalClass, IntervalTree
from hagsc_lib import iterFASTA, histogramClass, fileHeader_Class

from gp_lib import jobFinished, submitJob, create_sh_file

from math import ceil, floor, log

from os.path import join, isfile

from sys import stdout, stderr

from numpy import array

from os import system

import subprocess

import re

import os

#==============================================================
# Default values
#runPath          = '/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts'
def_numPairs     = 5000000
def_ALIGNER      = 'MEM'
def_CALLER       = 'VARSCAN'
def_NUM_JOBS     = 18
def_ALIGN_MEMORY = '10G'
def_TOTAL_BASES  = 1e20
def_READ_REGEX   = r'\"[a-zA-Z0-9]+_[0-9]+_[a-zA-Z0-9]+_[0-9]+_\([0-9]+\)_\([0-9]+\)_\([0-9]+\)\"'
def_MQ_CUTOFF    = 35
def_COVERAGE     = 'NORMAL'
def_STATUS       = 'UNPROCESSED'
def_TYPE         = 'ILLUMINA'

#==============================================================
# GATK and picard tools commands
#preGATKcmd = 'java -Xmx30g -XX:PermSize=4g -XX:MaxPermSize=4g -jar /usr/common/jgi/frameworks/GATK/2.5-2/GenomeAnalysisTK.jar'
#preGATKcmd   = 'java -Xmx30g -jar /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV/opt/gatk-3.6/GenomeAnalysisTK.jar'
#prePICARDcmd = 'java -Xmx30g -jar /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV/share/picard-2.17.2-0/picard.jar'
preGATKcmd   = 'GenomeAnalysisTK'
prePICARDcmd = os.environ['prePICARDcmd'] # this variable is exported as part of the conda or docker env

#==============================================================
# Possible job types
jobTypeSet = set( ['align_and_split', \
                   'merge_bam_files', \
                   'gatkQC', \
                   'merge_QC_bam_files', \
                   'insertSizeDist', \
                   'GC_Depth_plots', \
                   'call_SNPs', \
                   'snp_Filter', \
                   'createSNP_plots', \
                   'SNP_context_files', \
                   'call_INDELS', \
                   'INDEL_Filter', \
                   'INDEL_context_files', \
                   'writeSummaryFiles'] )

allowedCallers  = set( [ 'GATK', 'VARSCAN' ] )
allowedAligners = set( [ 'MEM',  'ALN'     ] )

#==============================================================
# Regular expression for finding lower case bases
findLowerCase = re.compile( r'([a-z]+)' ).finditer

#==============================================================
# Regular expression for finding gaps
findGaps = re.compile( r'[ACTGactg]{1}(N+)[ACTGactg]{1}' ).finditer

#==============================================================
def writeAndEcho( tmpStr, cmdLogFile ):
    stdout.write( '%s\n'%tmpStr )
    oh = open( cmdLogFile, 'a' )
    oh.write( '%s\n'%tmpStr )
    oh.close()

#==============================================================
def echoAndExecute( tmpCmd, cmdLogFile, execute=True ):
    stdout.write( '%s\n'%tmpCmd )
    oh = open( cmdLogFile, 'a' )
    oh.write( '%s\n'%tmpCmd )
    oh.close()
    if ( execute ): system( tmpCmd )

#==============================================================
def throwError( tmpString ):
    stderr.write( '-------------------------\n' )
    stderr.write( 'ERROR:  %s\n'%tmpString )
    stderr.write( 'Halting execution\n' )
    stderr.write( '-------------------------\n' )
    assert False
    
#==============================================================
def round_up( x, y ):
    return int( ceil(  x / float(y) ) ) * y

#==============================================================
def round_dwn( x, y ):
    return int( floor( x / float(y) ) ) * y
    
#==============================================================
def isBad_SNP(altFrac):
    if ( altFrac == None ): return False
    return ( (altFrac < 0.20) or (altFrac > 0.80) )

#==============================================================
class samSplitter:
    def __init__(self, line):
	    self.s          = line.split(None)
	    self.readID     = self.s[0]
	    self.flag       = int(self.s[1])
	    self.scaffID    = self.s[2]
	    self.scaffStart = int(self.s[3])
	    self.mapQ       = int(self.s[4])
	    self.cigar      = self.s[5]
	    self.rNext      = self.s[6]
	    self.pNext      = self.s[7]
	    self.insertSize = int(self.s[8])
	    self.sequence   = self.s[9]
	    self.qual       = self.s[10]
	    self.optDict    = dict( [(item.split(':')[0],item.split(':')[2]) for item in self.s[11:]] )

#========================
def parseConfigFile( configFile, basePath ):
    
    # Checking for the config file
    if ( not isfile(configFile) ):
        throwError( 'parseConfigFile could not find config file %s'%configFile )
    #####

    # Initializing the key set
    keySet = set(['RUNID', \
                  'LIB_ID', \
                  'REFERENCE', \
                  'ASSEMINDEX', \
                  'READS', \
                  'COVERAGE', \
                  'MASKFILTER', \
                  'GAPFILTER', \
                  'TYPE',\
                  'STATUS', \
                  'EMAIL', \
                  'ALIGNER', \
                  'CALLER', \
                  'NUM_PAIRS', \
                  'NUM_JOBS', \
                  'ALIGN_ONLY', \
                  'ALIGN_MEMORY', \
                  'TOTAL_BASES', \
                  'COMBINED_FASTQ', \
                  'READ_REGEX', \
                  'MQ_CUTOFF', \
                  'MERGE_ONLY' ])
    
    # Parsing the config file and pulling the key:value relationships
    configDict = {}
    for line in open( configFile ):
        
        # Breaking out the key:value pair
        key, value   = line.split(None)
        
        # Adding the key:value pair
        if ( key == 'READS' ):
            configDict[key] = value.split(';')
        elif ( key in set(['GAPFILTER','MASKFILTER', 'NUM_PAIRS', 'NUM_JOBS', 'TOTAL_BASES', 'MQ_CUTOFF']) ): 
            configDict[key] = int( value )
        elif ( key == 'ALIGN_ONLY' ):
            if ( value == 'True' ):
                configDict[key] = True
            elif ( value == 'False' ):
                configDict[key] = False
            else:
                throwError( 'Invalid value=%s for key=%s'%(value,key) )
            #####
        elif ( key == 'MERGE_ONLY' ):
            if ( value == 'True' ):
                configDict[key] = True
            elif ( value == 'False' ):
                configDict[key] = False
            else:
                throwError( 'Invalid value=%s for key=%s'%(value,key) )
            #####
        elif ( key == 'COMBINED_FASTQ' ):
            if ( value == 'True' ):
                configDict[key] = True
            elif ( value == 'False' ):
                configDict[key] = False
            else:
                throwError( 'Invalid value=%s for key=%s'%(value,key) )
            #####
        else:
            configDict[key] = value
        #####
        
        # Chedking for any keys that are mislabeled
        try:
            keySet.remove(key)
        except KeyError:
            throwError( 'parseConfigFile encountered an unrecognized key: %s'%key )
        #####
        
    #####

    #-----------------------------------    
    # Creating the command log filename
    try:
        cmd_log = join( basePath, '%s_cmdLog.dat'%configDict['RUNID'] )
        if ( not isfile(cmd_log) ):
            # Clearing the command log file
            oh = open( cmd_log, 'w' )
            oh.close()
        #####
        # Writing to the command log
        writeAndEcho( '- EXECUTING: parseConfigFile', cmd_log )
    except KeyError:
        throwError( 'Unable to read RUNID' )
    #####

    #-----------------------------------    
    # Checking the aligner memory
    try:
        x = configDict['ALIGN_MEMORY']
    except KeyError:
        # Default aligner is MEM
        configDict['ALIGN_MEMORY'] = def_ALIGN_MEMORY
        writeAndEcho( '- Using the default alignment memory %s'%def_ALIGN_MEMORY, cmd_log )
        keySet.remove('ALIGN_MEMORY')
    #####

    #-----------------------------------    
    # Checking the aligner memory
    try:
        x = configDict['TOTAL_BASES']
    except KeyError:
        # Default aligner is MEM
        configDict['TOTAL_BASES'] = def_TOTAL_BASES
        writeAndEcho( '- Using the default total bases %s'%def_TOTAL_BASES, cmd_log )
        keySet.remove('TOTAL_BASES')
    #####

    #-----------------------------------    
    # Checking the number of jobs
    try:
        x = configDict['NUM_JOBS']
    except KeyError:
        # Default aligner is MEM
        configDict['NUM_JOBS'] = def_NUM_JOBS
        writeAndEcho( '- Using the default number of jobs %d'%def_NUM_JOBS, cmd_log )
        keySet.remove('NUM_JOBS')
    #####
    
    #-----------------------------------    
    # Checking the aligner
    try:
        x = configDict['ALIGNER']
    except KeyError:
        # Default aligner is MEM
        configDict['ALIGNER'] = def_ALIGNER
        writeAndEcho( '- Using the default %s aligner'%def_ALIGNER, cmd_log )
        keySet.remove('ALIGNER')
    #####
    if ( configDict['ALIGNER'] not in allowedAligners ):
        throwError( 'Aligner %s is not a valid option'%configDict['ALIGNER'] )
    #####

    #-----------------------------------
    # Checking the caller
    try:
        x = configDict['CALLER']
    except KeyError:
        # Default caller is VARSCAN
        configDict['CALLER'] = def_CALLER
        writeAndEcho( '- Using the default %s caller'%def_CALLER, cmd_log )
        keySet.remove('CALLER')
    #####
    if ( configDict['CALLER'] not in allowedCallers ):
        throwError( 'SNP/INDEL caller %s is not a valid option'%configDict['CALLER'] )
    #####

    #-----------------------------------
    # Checking the number of pairs
    try:
        x = configDict['NUM_PAIRS']
    except KeyError:
        # Default caller is VARSCAN
        configDict['NUM_PAIRS'] = def_numPairs
        writeAndEcho( '- Using the default number of pairs per file %d'%def_numPairs, cmd_log )
        keySet.remove('NUM_PAIRS')
    #####

    #-----------------------------------
    # Checking to see if we only want to generate a bam file
    try:
        x = configDict['ALIGN_ONLY']
    except KeyError:
        # Default value is False
        configDict['ALIGN_ONLY'] = False
        writeAndEcho( '- Using the default value for ALIGN_ONLY=False', cmd_log )
        keySet.remove('ALIGN_ONLY')
    #####

    #-----------------------------------
    # Checking to see if we only want to generate a bam file
    try:
        x = configDict['MERGE_ONLY']
    except KeyError:
        # Default value is False
        configDict['MERGE_ONLY'] = False
        writeAndEcho( '- Using the default value for MERGE_ONLY=False', cmd_log )
        keySet.remove('MERGE_ONLY')
    #####

    #-----------------------------------
    # Checking to see if I have a combined set of FASTQ files I am using
    try:
        x = configDict['COMBINED_FASTQ']
    except KeyError:
        # Default value is False
        configDict['COMBINED_FASTQ'] = False
        writeAndEcho( '- Using the default value for COMBINED_FASTQ=False', cmd_log )
        keySet.remove('COMBINED_FASTQ')
    #####

    #-----------------------------------    
    # Checking the aligner memory
    try:
        x = configDict['READ_REGEX']
    except KeyError:
        # Set to default regex
        configDict['READ_REGEX'] = def_READ_REGEX
        writeAndEcho( '- Using the default READID REGEX %s'%def_READ_REGEX, cmd_log )
        keySet.remove('READ_REGEX')
    #####

    #-----------------------------------    
    # Checking the MQ cutoff
    try:
        x = configDict['MQ_CUTOFF']
    except KeyError:
        # Set to default map quality cutoff
        configDict['MQ_CUTOFF'] = def_MQ_CUTOFF
        writeAndEcho( '- Using the default MQ CUTOFF %s'%def_MQ_CUTOFF, cmd_log )
        keySet.remove('MQ_CUTOFF')
    #####

    #-----------------------------------    
    # Checking the STATUS
    try:
        x = configDict['STATUS']
    except KeyError:
        # Default status is UNPROCESSED
        configDict['STATUS'] = def_STATUS
        writeAndEcho( '- Using the default STATUS %s'%def_STATUS, cmd_log )
        keySet.remove('STATUS')
    #####

    #-----------------------------------    
    # Checking the COVERAGE
    try:
        x = configDict['COVERAGE']
    except KeyError:
        # Default coverage is NORMAL
        configDict['COVERAGE'] = def_COVERAGE
        writeAndEcho( '- Using the default COVERAGE %s'%def_COVERAGE, cmd_log )
        keySet.remove('COVERAGE')
    #####

    #-----------------------------------    
    # Checking the TYPE
    try:
        x = configDict['TYPE']
    except KeyError:
        # Default TYPE is ILLUMINA
        configDict['TYPE'] = def_TYPE
        writeAndEcho( '- Using the default TYPE %s'%def_TYPE, cmd_log )
        keySet.remove('TYPE')
    #####

    #-----------------------------------    
    # Checking for missing keys
    if ( len(keySet) > 0 ):
        strList = ['parseConfigFile detected that the following keys are missing in your snps.config file\n']
        for key in keySet:
            strList.append( '\t%s\n'%key )
        #####
        strList.append( 'Please add these keys to your config file and resubmit the snp calling job.')
        throwError( ''.join(strList) )
    #####
    
    #-----------------------------------    
    # Checking to see if the reference exists
    if ( not isfile(configDict['REFERENCE']) ):
        throwError( 'REFERENCE genome can not be located %s'%(configDict['REFERENCE']) )
    #####

    #-----------------------------------    
    # Checking to see if the INDEX exists
    if ( not isfile('%s.pac'%configDict['ASSEMINDEX']) ):
        throwError( 'ASSEMINDEX bwa index can not be located %s'%(configDict['ASSEMINDEX']) )
    #####

    #-----------------------------------    
    # Checking to see if the reads exist
    if ( not configDict['COMBINED_FASTQ'] ):
        for item in configDict['READS']:
            if ( not isfile(item) ):
                throwError( 'READS file can not be located %s'%(item) )
            #####
        #####
    #####

    #-----------------------------------    
    # Checking the status of the run
    if ( configDict['STATUS'] == 'PROCESSED' ):
        throwError( 'This run has already been processed with status %s'%(configDict['STATUS']) )
    #####
    
    return ( configDict, cmd_log )

#==============================================================
def isBad_READ_REGEX( tmpREGEX, readFile, isCombinedFASTQ ):
    
    if ( isGzipFile(readFile) ):
        cmd = 'cat %s | gunzip -c | head -1'%( readFile )
    elif ( isBzipFile(readFile) ):
        cmd = 'cat %s | bunzip2 -c | head -1'%( readFile )
    else:
        if ( isCombinedFASTQ ):
            cmd = 'bzcat -k -c %s | head -1'%( readFile )
        else:
            cmd = 'cat %s | head -1'%( readFile )
        #####
    #####

    # Pulling a read
    p = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    for line in p.stdout:
        readID = line[1:-1].split('-')[0]
    #####
    p.poll()
    
    # Checking the readID against the regular expression
    x          = tmpREGEX.replace( '\\','').replace( '\"', '')
    regexCheck = re.compile( x )
    
    if ( len( regexCheck.findall(readID) ) == 0 ):
        return regexCheck.pattern, readID, True
    else:
        return regexCheck.pattern, readID, False
    #####

#==============================================================
def createGroupDictionary( primaryScaffsFile ):
    # Loading the group dictionary
    groupDict = {}
    for line in open( primaryScaffsFile ):
        scaffID, grpNum = line.split( None )
        try:
            groupDict['GRP_%s'%grpNum].append( scaffID )
        except KeyError:
            groupDict['GRP_%s'%grpNum] = [ scaffID ]
        #####
    #####
    # Finding the final group
    DSU = [ (int(item.split('_')[1]),item) for item in groupDict.iterkeys() ]
    DSU.sort()
    # Return to the user
    return groupDict, DSU[-1][1]

#==============================================================
def check_PHRED_encoding( testFile ):
    
    # Checking to see if it is compressed.
    if ( isGzipFile(testFile) ):
        p = subprocess.Popen( 'cat %s | gunzip -c'%testFile, shell=True, stdout=subprocess.PIPE )
    elif ( isBzipFile(testFile) ):
        p = subprocess.Popen( 'cat %s | bunzip2 -c'%testFile, shell=True, stdout=subprocess.PIPE )
    else:
        p = subprocess.Popen( 'cat %s'%testFile, shell=True, stdout=subprocess.PIPE )
    #####

    # Skip the first million reads
    stderr.write( '\t-CHECKING PHRED ENCODING\n' )
    stderr.write( '\t-SKIPPING THE FIRST MILLION LINES\n' )
    oh     = p.stdout
    for n in xrange( 4000000 ): oh.readline()

    # Now looking at the next 1000 reads, assuming that they are PHRED33 encoded
    stderr.write( '\t-CHECKING ENCODING\n' )
    phred_33_encoding = dict( [ (chr(n+33),n) for n in xrange(30,110) ] )
    nReads = 0
    hist   = {}
    while ( True ):
        line1 = oh.readline()
        line2 = oh.readline()
        line3 = oh.readline()
        qual  = oh.readline().strip()
        for score in [phred_33_encoding[char] for char in qual]:
            try:
                hist[score] += 1
            except KeyError:
                hist[score] = 1
            #####
        #####
        nReads += 1
        if ( nReads >= 1000 ): break
    #####
    p.poll()
    
    # Making a decision
    if ( max(hist.keys()) > 42 ): return False
    return True

#==============================================================
def gatkQC( basePath, GATKbam, sortedMergedBamFile, RUN_ID, LIB_ID, genomeFASTA, read_REGEX, cmd_log ):
    
    #============================
    # Checking the phred encoding
#    is_PHRED33 = check_PHRED_encoding( configDict['READS'][0] )

    #============================
    # Adding the read group to the bam.  This is a requirement of GATK.
    # The reason these two commands are not in the createSortedBAM() function is because
    # You can not pipe directly from "samtools sort" to stdout.  If you could, then I would do this
    withRG_bam = join( basePath, '%s.RG.bam'%RUN_ID )
    withRG_bai = join( basePath, '%s.RG.bai'%RUN_ID )
    addRG      = '%s AddOrReplaceReadGroups INPUT=%s OUTPUT=%s SORT_ORDER=coordinate RGID=%s RGLB=%s RGSM=%s RGPL=illumina RGPU=none VALIDATION_STRINGENCY=LENIENT TMP_DIR=%s'% ( prePICARDcmd, sortedMergedBamFile, withRG_bam, LIB_ID, LIB_ID, LIB_ID, basePath)
    bamIndex   = 'picard BuildBamIndex INPUT=%s VALIDATION_STRINGENCY=LENIENT TMP_DIR=%s' % ( withRG_bam, basePath )

    #============================
    # Removing duplicates
    dupMetricsFile = join( basePath, '%s.duplicateMetricsFile.dat'%RUN_ID)
    deDup_bam      = join( basePath, '%s.deDup.bam'%RUN_ID     )
    deDup_bai      = join( basePath, '%s.deDup.bai'%RUN_ID     )
    deDup          = '%s MarkDuplicates I=%s O=%s M=%s VALIDATION_STRINGENCY=LENIENT ASSUME_SORTED=true SORTING_COLLECTION_SIZE_RATIO=0.05 REMOVE_DUPLICATES=true TMP_DIR=%s MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=200 MAX_RECORDS_IN_RAM=2000000 READ_NAME_REGEX=%s' % ( prePICARDcmd, withRG_bam, deDup_bam, dupMetricsFile, basePath, read_REGEX.replace('\\','') )
    reIndex        = 'picard BuildBamIndex INPUT=%s VALIDATION_STRINGENCY=LENIENT TMP_DIR=%s' % ( deDup_bam, basePath )
    rm_RG          = "rm -f %s %s" % (withRG_bam, withRG_bai)
    
    #============================
    # Realiging around indels
    outputIntervalFile = join( basePath, '%s.output.intervals'%RUN_ID )
    reAligned_bam      = join( basePath, '%s.reAligned.bam'%RUN_ID )
    reAligned_bai      = join( basePath, '%s.reAligned.bai'%RUN_ID )
    targetRealign      = '%s -T RealignerTargetCreator -I %s -R %s -o %s --filter_bases_not_stored'% (preGATKcmd, deDup_bam, genomeFASTA, outputIntervalFile)
#    if ( not is_PHRED33 ):  options = '%s --fix_misencoded_quality_scores'%options
    runRealign         = '%s -T IndelRealigner -I %s -R %s -targetIntervals %s -o %s --filter_mismatching_base_and_quals --filter_bases_not_stored'% (preGATKcmd, deDup_bam, genomeFASTA, outputIntervalFile, reAligned_bam)
    rm_deDup           = "rm -f %s %s"%(deDup_bam, deDup_bai)

    #-------------------------------------------------
    # Need an option to fix misencoded quality scores
    # --fix_misencoded_quality_scores
    #-------------------------------------------------
    
    #============================
    # Resorting the bam file
    reSort       = '%s SortSam INPUT=%s OUTPUT=%s SORT_ORDER=coordinate VALIDATION_STRINGENCY=LENIENT TMP_DIR=%s MAX_RECORDS_IN_RAM=2000000 VERBOSITY=DEBUG'% (prePICARDcmd, reAligned_bam, GATKbam, basePath)
    reReIndex    = 'picard BuildBamIndex INPUT=%s VALIDATION_STRINGENCY=LENIENT TMP_DIR=%s'% ( GATKbam, basePath )
    rm_reAligned = "rm -f %s %s"%(reAligned_bam, reAligned_bai)
    
    #============================
    # Putting the commands together
    cmdList=[]
    
    # Adding the remaining commands
    cmdList.extend(  [addRG, \
                      bamIndex, \
                      deDup, \
                      reIndex, \
                      targetRealign, \
                      runRealign, \
                      reSort, \
                      reReIndex ] )

    # Writing the commands to the log file
    writeAndEcho( '\n-----------------------\n- GATK QC:', cmd_log )
    for cmd in cmdList:
        writeAndEcho( cmd, cmd_log )
    #####
    
    return cmdList

#==============================================================
def InsertSize ( cleanedBam, basePath, RUN_ID, cmdLogFile, inserts_to_sample ):
    """
       Computes the insert size distribution and creates a gnuplot histogram
    """
    # Reading in the insert sizes
    insertHistDict = {}
    tmpProcess = subprocess.Popen( 'samtools view %s'% cleanedBam, shell=True, stdout=subprocess.PIPE )			    
    n = 0
    for line in tmpProcess.stdout:
        insertSize = samSplitter(line).insertSize
        if ( insertSize < 0 ): continue
        if ( n > inserts_to_sample ): break
        n += 1
        try:
            insertHistDict[insertSize] += 1
        except KeyError: 
            insertHistDict[insertSize] = 1
        #####
    #####
    tmpProcess.poll()
    
    # Writing the data file
    dataFile = join( basePath, '%s.insertSize.data.dat'%RUN_ID )
    
    # Computing the numerical value
    numInserts = sum( insertHistDict.values() )
    c          = 100.0 / float(numInserts)
    oh         = open( dataFile, 'w' )
    for insertSize in sorted(insertHistDict.keys()):
        per = c * float( insertHistDict[insertSize] )
        oh.write( '%d\t%f\n'%(insertSize,per) )   
    #####
    oh.close()
    
    # Setting up the plotter
    plotFileInsert = join( basePath, '%s.insertSize.gnuplot.dat'%RUN_ID )
    dplot          = open( plotFileInsert, 'w' )
    dplot.write( '\nreset\nset output "%s.insertSize.png"\n'%join(basePath,RUN_ID) )
    dplot.write( 'set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\nset nokey\nset title \"%s Insert Sizes\"\n'%RUN_ID )
    dplot.write( 'set grid x y\nset xlabel \"Insert Size\"\nset ylabel \"Frequency\"\nset autoscale y\nset xrange [10:2000]\nplot \"%s\" using 1:2 axis x1y1 with boxes  lc rgb \'#FF0000\'\n'%dataFile)
    dplot.close()
    echoAndExecute( 'gnuplot %s'%plotFileInsert, cmdLogFile, execute=True )

#==============================================================
class GC_depth_class(object):

    def __init__(self,scaffID,seq,binSize):
        
        # Reading in the information
        self.scaffID = scaffID
        self.seq     = seq
        self.nSize   = len(seq)
        self.binSize = binSize
        
        # Minimum scaffold size has to be 80% of the binSize
        self.minSize = int( 0.80 * float(self.binSize) )
        
        # Setting up the variables
        self.nBins   = max( 1, self.nSize / self.binSize ) # Make sure we have at least one bin
        if ( self.binSize % self.nSize >= self.minSize ):
            self.nBins += 1
        #####
       
        # Setting up the accumulators
        self.GC          = self.nBins * [None]
        self.basesPerBin = self.nBins * [None]
        self.depthList   = self.nBins * [0]
        self.normDepth   = self.nBins * [None]
        self.SNP_list    = self.nBins * [0]
        
        # Perform the analysis to fill the GC and basesPerBin lists
        self.computeGC_bins()
    
    def get_start_end(self,n):
        return ( n * self.binSize, min( self.nSize, (n+1) * self.binSize ) )
    
    def computeGC_bins(self):
        # Computing the initial GC and bases per bin
        nReject = 0
        for n in xrange(self.nBins):
            start, end = self.get_start_end(n)
            binSeq     = self.seq[start:end]
            GC, nBases = self.computeGC(binSeq)
            if ( nBases >= self.minSize ):
                self.GC[n]          = 100.0 * float(GC) / float(nBases)
                self.basesPerBin[n] = nBases
            #####
        #####
    
    def computeGC(self,tmpSeq):
        # I am only counting the upper case bases when counting coverage
        GC     = tmpSeq.count('G') + tmpSeq.count('C')
        AT     = tmpSeq.count('A') + tmpSeq.count('T')
        nBases = GC + AT
        return GC, nBases
    
    def selectBin(self, pos):
        if ( pos == self.nSize ):
            return self.nBins - 1
        else:
            return int( float(pos) / float(self.binSize) )
        #####
    
    def addDepth(self,pos,depth):
        try:
            self.depthList[self.selectBin(pos)] += depth
        except IndexError:
            # This check is to ignore the smaller scaffolds because
            # I did not initialize them
            return
        #####

    def normalizeDepth(self):
        # Normalizing the depth per bin
        tmpList = []
        for n in xrange( self.nBins ):
            if ( (self.basesPerBin[n] == None) or (self.GC[n] == None) ): continue
            self.normDepth[n] = float(self.depthList[n]) / float(self.basesPerBin[n])
            tmpList.append( self.normDepth[n] )
        #####
        # Returning the maximum depth for the scaffold
        try:
            return max(tmpList)
        except ValueError:
            return 0
        #####

    def output_depth_String(self):
        outputList = []
        for n in xrange( self.nBins ):
            start, end = self.get_start_end(n)
            GC    = self.GC[n]
            depth = self.normDepth[n]
            if ( GC == None ): continue
            outputList.append( '%s\t%d\t%.2f\t%.2f\n'%(self.scaffID,end,GC,depth) )
        #####
        return outputList

    def add_SNP(self,pos):
        self.SNP_list[self.selectBin(pos)] += 1
    
    def get_SNP_data(self):
        outputList = []
        for n in xrange( self.nBins ):
            nSNPs = self.SNP_list[n]
            GC    = self.GC[n]
            if ( (nSNPs == 0) or (GC == None) ): continue
            outputList.append( (nSNPs, GC) )
        #####
        return outputList

    def tmp_depth_String(self):
        outputList = []
        for n in xrange( self.nBins ):
            start, end = self.get_start_end(n)
            GC    = self.GC[n]
            outputList.append( (self.scaffID,n,start,end,GC,self.depthList[n],self.normDepth[n]) )
        #####
        return outputList

#==============================================================
def GC_vs_Depth ( cleanedBam, genomeFASTA, basePath, RUN_ID, binSize, cmdLogFile ):

    # Initial GC calculation
    print "initial gc calculation"
    GC_depth_dict = {}
    for r in iterFASTA( open(genomeFASTA) ):
        GC_depth_dict[r.id] = GC_depth_class( r.id, str(r.seq), binSize )
    #####
    
    # Computing the depth on the bam file
    print "Reading in depth information"
    p = subprocess.Popen( 'samtools depth -q 0 -Q 0 %s'%cleanedBam, shell=True, stdout=subprocess.PIPE )
    for line in p.stdout:
        scaffID, pos, depth = line.split(None)
        pos, depth = map( int, [pos,depth] )
        GC_depth_dict[scaffID].addDepth( pos, depth )
    #####
    p.poll()
    
    # Normalizing the depth and finding the maximum depth value at the same time
    maxDepth = max( [ GC_depth_dict[scaffID].normalizeDepth() for scaffID in GC_depth_dict.iterkeys() ] )
    
    # Creating the histogram
    depthHist = histogramClass( 0, int(maxDepth) + 3, int(maxDepth) + 3 )
    for scaffID in GC_depth_dict.iterkeys():
        # Creating the histogram, ignoring the zero depth locations
        map( depthHist.addData, filter( lambda x:( (x!=None) and (x>0) ), GC_depth_dict[scaffID].normDepth ) )
    #####
    
    # Pulling histogram information
    histDict = depthHist.generateOutputDict()
    newHist  = histDict['counts']
    midpoint = histDict['midpoint']
    
#     # Finding the natural bound on the distribution, ignoring the extreme spread in coverage
#     minval, maxval = depthHist.findingBounds( 0.333 )
    
    # Computing average depth
    x            = [ ( float(midpoint[n]*newHist[n]), float(newHist[n]) ) for n in xrange(len(newHist)) ]
    averageDepth = sum( [item[0] for item in x] ) / sum( [item[1] for item in x] )
    
    # Writing out the data file
    dataFile   = join( basePath, '%s.GCdepth.data.dat'%RUN_ID )
    d = open('%s'%dataFile, 'w')
    for scaffID, tmpClass in GC_depth_dict.iteritems():
        for line in tmpClass.output_depth_String():
            d.write( line )
        #####
    #####
    d.close()

    # Writing the complete histogram output file
    denom = sum( [ float(midpoint[n]*newHist[n]) for n in xrange(len(newHist)) ] )
    xs    = []
    ys    = []
    normHistFile = join( basePath, '%s.GCdepth.normHist.dat'%RUN_ID )
    cH           = open( normHistFile, 'w' )
    for n in xrange( len(newHist) ):
        norm = float(newHist[n]) / denom
        xs.append( midpoint[n] )
        ys.append( norm )
        cH.write( '%.8f\t%.8f\n'%( midpoint[n], norm ) )
    #####
    cH.close()

    # Finding the x ranges
    xs.sort()
    xmin = round_dwn( xs[0],  10 )
    xmax = round_up(  xs[-1], 10 )
    
    # Finding the y ranges
    ys.sort()
    ymin = round_dwn( ys[0],  0.00005 )
    ymax = round_up(  ys[-1], 0.00005 )
    
    # Writing the GNU plot file
    GCdepthPNG = join( basePath, '%s.GCdepth.png'%RUN_ID )
    plotFileGC = join( basePath, '%s.GCdepth.gnuplot.dat'%RUN_ID )
    dplot      = open( plotFileGC, 'w' )
    dplot.write('\nreset\n' )
    dplot.write('set output "%s"\n\n' % GCdepthPNG )
    dplot.write('set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\n' )
    dplot.write('set nokey\n' )
    dplot.write('set multiplot title \"Average Depth:\\t%.1f\"\n'% averageDepth )
    dplot.write('set size 0.5,0.75\n' )
    dplot.write('set origin 0.5,0.15\n' )
    dplot.write('set grid x y\n' )
    dplot.write('set title \"Depth Histogram\"\n' )
    dplot.write('set xlabel \"Depth\"\n' )
    dplot.write('set logscale x\n' )
    dplot.write('set ylabel \"Frequency\"\n' )
    dplot.write('set yrange [%.8f:%.8f]\n'%(ymin, ymax) )
    dplot.write('set xrange [1:1000]\n' )
    dplot.write('plot \"%s\" using 1:2 axis x1y1 with boxes\n' % normHistFile )
    dplot.write('\nset size 0.5,0.75\n' )
    dplot.write('set origin 0,0.15\n' )
    dplot.write('set title \"GC vs Depth %dkb Blocks"\n' % int(binSize/1000) )
    dplot.write('set grid x y\n' )
    dplot.write('set xlabel \"GC Content\"\n' )
    dplot.write('set nologscale x\n' )
    dplot.write('set ylabel \"Depth\"\n' )
    dplot.write('set logscale y\n' )
    dplot.write('set yrange [1:1000]\n' )
    dplot.write('set xrange [10:90]\n' )
    dplot.write('plot \"%s\" using 3:4 axis x1y1 with points\n' % dataFile )
    dplot.close()
    
    # Making the plot
    echoAndExecute( 'gnuplot %s' %plotFileGC, cmdLogFile, execute=True ) 

#==============================================================
def pullAverageDepth(basePath, configDict):
    findAvgDepth = re.compile( r'([0-9]+.[0-9]+)' ).findall
    p            = subprocess.Popen( 'cat %s/%s.GCdepth.gnuplot.dat | grep Average'%(basePath,configDict['RUNID']), shell=True, stdout=subprocess.PIPE )
    avgDepth     = float( findAvgDepth( p.stdout.readline() )[0] )
    p.poll()
    return avgDepth

#==============================================================	
def call_SNPs( genomeFASTA, GATKbam, SNP_rawCalls, SNP_preFilter, SNP_postFilter, RUN_ID, \
               basePath, configFile, lowFilter, highFilter, avgDepth, MQ_CUTOFF, cmd_log ):
    
    #---------------------------------------------------------------------------------------------------
    # Downsampling amount
    # Rule of thumb is 10x the average depth, with a minimum of 250
    dCov = max( int(10.0*avgDepth), 250 )
	
    # Forcing annotation of the calls to keep the log file from getting too large
    FA_list = []
    FA_list.append( '-A QualByDepth'               )
    FA_list.append( '-A RMSMappingQuality'         )
    FA_list.append( '-A FisherStrand'              )
    FA_list.append( '-A Coverage'                  )
    FA_list.append( '-A HaplotypeScore'            )
    FA_list.append( '-A MappingQualityRankSumTest' )
    FA_list.append( '-A ReadPosRankSumTest'        )
    FA_list.append( '-A MappingQualityZero'        )
    FA      = ' '.join( FA_list )

    #---------------------------------------------------------------------------------------------------
	# Running the unified genotyper
	# -glm SNP               This is the genotype likelihoods model for snp calling
	# -rf BadCigar           This is the bad cigar filter.
	# --logging_level ERROR  Keeps the log file from getting too large
	# -dcov                  Downselect high coverage areas to this coverage level
    del_vcf_file    = 'rm -f %s' % SNP_rawCalls
    raw_SNP_calling = '%s -T UnifiedGenotyper -glm SNP -R %s -dcov %d -I %s -o %s -rf BadCigar --logging_level ERROR %s'% ( preGATKcmd, genomeFASTA, dCov, GATKbam, SNP_rawCalls, FA )

    #---------------------------------------------------------------------------------------------------
    # Running VariantFiltration
    # 
    #   QD < 2.0               QD = variant confidence (QUAL from column 6) divided by unfiltered allele depth (AD)
    #   MQ < 51.0              MQ = Root Mean Square of all of the MQ values for all reads (from all samples) used at that site used for genotying.
    #   MQ > 61.0
    #   FS > 60.0              FS = Strand bias estimated using Fisher's Exact Test, phred scaled p-value, larger values indicate more bias
    #   HaplotypeScore > 13.0  HS = Consistency of the site with strictly two segregating haplotypes.  High scores indicate regions with bad alignments, typically leading to artifactual SNP and indel calls.
    #   DP < %d                DP = Count of reads that passed the caller's internal quality control metrics on a site
    #   DP > %d
    #   QUAL < 50.0            QUAL = Phred scaled quality score, High QUAL scores indicate high confidence calls.
    # 
    #   MQRankSum < -12.5      MQRankSum      = Compares mapping qualities of reads supporting the reference allele with those supporting the alternate. The ideal result is a value close to zero.  
    #                                           A negative value indicates that the reads supporting the alternate allele have lower mapping quality scores than those supporting the reference allele
    #   ReadPosRankSum < -8.0  ReadPosRankSum = Tests whether there is evidence of bias in the position of alleles within the reads that support them.  
    #                                           The ideal result is a value close to zero, which indicates there is little to no difference in where the alleles are found relative to the ends of reads. 
    #                                           A negative value indicates that the alternate allele is found at the ends of reads more often than the reference allele. 
    #
    #   SB > -10.0             SB = Per-sample component statistics which comprise the Fisher's Exact Test to detect strand bias
    #
    #   GQ < 25                GQ = phred scaled genotype quality
    #
    # --clusterWindowSize 10                    The window size (in bases) in which to evaluate clustered SNPs
    #
    #---------------------------------------------------------------------------------------------------
    FE_list = []
#     FE_list.append( r'--filterExpression "MQ > 61.0" --filterName "MQ_tooHigh"' )
#     FE_list.append( r'--filterExpression "SB > -10.0" --filterName "failed_StrandBias"' )
#     FE_list.append( r'--genotypeFilterExpression "GQ < 25" --genotypeFilterName "GQ_LT_25"' )
    FE_list.append( r'--filterExpression "QD < 2.0" --filterName "LowQD"' )
    FE_list.append( r'--filterExpression "MQ < %d.0" --filterName "low_MQ"' % MQ_CUTOFF )
    FE_list.append( r'--filterExpression "FS > 60.0" --filterName "HighStrandBias"' )
    FE_list.append( r'--filterExpression "DP < %d" --filterName "low_COV"'%( lowFilter ) )
    FE_list.append( r'--filterExpression "DP > %d" --filterName "COV_TooHigh"'%( highFilter ) )
    FE_list.append( r'--filterExpression "HaplotypeScore > 13.0" --filterName "failed_HaplotypeScore"' )
    FE_list.append( r'--filterExpression "QUAL < 50.0" --filterName "Low_Quality"' )
    FE_list.append( r'--filterExpression "MQRankSum < -12.5" --filterName "failed_MQRankSum"' )
    FE_list.append( r'--filterExpression "ReadPosRankSum < -8.0" --filterName "failed_ReadPosRankSum"' )
    FE_list.append( r'--filterExpression "(MQ0 >= 4) && ((MQ0 / (1.0 * DP)) > 0.10)" --filterName "HARD_TO_VALIDATE"' )
    FE = ' '.join( FE_list )
    filter_SNPs = '%s -T VariantFiltration -R %s --variant %s -o %s %s --logging_level ERROR'%( preGATKcmd, genomeFASTA, SNP_rawCalls, SNP_preFilter, FE )

    # Have decided to remove the SB strand bias
    # Also removed the upper bound on mapping quality
    # Also removed the genotype quality filter, as it was too heavy handed.

    # Filtering the vcf file
    grepFilter = r'rm -f %s ; cat %s | grep PASS | grep -v GQ_LT_25 | grep -v "^#" > %s' % ( SNP_postFilter, SNP_preFilter, SNP_postFilter )
    mv_IDX     = 'cp %s.idx %s.idx'%( SNP_preFilter, SNP_postFilter )

    # Putting the commands together
    cmdList    = [ raw_SNP_calling, \
                   filter_SNPs, \
                   grepFilter, \
                   mv_IDX ]

    return cmdList

#==============================================================
def pullSoftMaskWindow( genomeFASTA, softMask_filter_length ):
    # Extracting the soft masked regions
    for r in iterFASTA( open(genomeFASTA) ):
        tmpSeq = str( r.seq  )
        nSize  = len( tmpSeq )
        tmpList = []
        for item in findLowerCase( str(r.seq) ): 
            start, stop = item.span()
            start = max(     0, start - softMask_filter_length )
            stop  = min( nSize, stop  + softMask_filter_length )
            tmpList.append( '%d_%d'%(start,stop) )
        #####
        yield (r.id, tmpList)
    #####

#==============================================================
def pullGapWindow( genomeFASTA, gap_filter_length ):
    # Extracting the soft masked regions
    for r in iterFASTA( open(genomeFASTA) ):
        tmpSeq  = str( r.seq  )
        nSize   = len( tmpSeq )
        tmpList = []
        for item in findGaps( str(r.seq) ): 
            start, stop = item.span()
            start = max(     0, start - gap_filter_length )
            stop  = min( nSize, stop  + gap_filter_length )
            tmpList.append( '%d_%d'%(start,stop) )
        #####
        yield (r.id, tmpList)
    #####
    
#============================================================== 
def Filter( genomeFASTA, softMaskWindow, gapWindow, postFilter, RUN_ID, softMaskFilter, cmd_log ):

    # Pulling the soft mask window
    tmpDict = {}
    for scaffID, tmpList in pullSoftMaskWindow( genomeFASTA, softMaskWindow ):
        # Building the interval tree using soft masking
        if ( len(tmpList) == 0 ): continue
        tmpDict[scaffID] = tmpList
    #####

    filterTree = {}
    for scaffID, tmpList in pullGapWindow( genomeFASTA, gapWindow ):
        # Building the interval tree using soft masking
        if ( len(tmpList) == 0 ): continue
        try:
            tmpSet = set(tmpList).union(tmpDict[scaffID])
        except KeyError:
            tmpSet = set(tmpList)
        #####
        tmpList2 = []
        for item in tmpSet:
            start, end = map( int, item.split('_') )
            tmpList2.append( IntervalClass( start, end, "%s|%d|%d"%(scaffID,start,end) ) )
        #####
        filterTree[scaffID] = IntervalTree( tmpList2 )
    #####
    del tmpDict
    
    # Filtering the snps
    oh = open( softMaskFilter, 'w' )
    for line in open( postFilter ):
        if ( line [0] == '#'):
            oh.write( line )
        else:
            s = line.split(None)
            scaffID  = s[0]
            scaffPos = int( s[1] )
            try:
                check = filterTree[scaffID].find(scaffPos,scaffPos)
            except KeyError:
                check =[]
            #####
            if check == []:
                oh.write( line )
            #####
        #####
    #####
    oh.close()

#==============================================================
def snpHistograms( vcfFile, RUN_ID, basePath, cmd_log ):
    
    # Initializing the histograms
    hetDepth_data = {}
    homDepth_data = {}
    
    # Initializing the 
    ref_to_alt_ratio_data = []
    snpQUAL_data          = []
    
    #----------------------------------------------------        
    # Reading in the information from the vcf file
    for line in open( vcfFile ):
        # Screening the comment lines
        if line[0] == '#': continue
        # Parsing the VCF line
        currentSNP = vcfSplitter(line)
        # Checking for too many variants
        if ( not currentSNP.goodSNP ): continue
        # Checking for zygosity
        if ( currentSNP.isHet ):
            # Screening the depth ratio on the het minor allele must be in the bound of 20 to 80
            if ( isBad_SNP( currentSNP.altAllele_depthFrac ) ): continue
            # Storing the ref_to_alt ratio
            ref_to_alt_ratio_data.append( currentSNP.ref_to_alt_ratio )
            # Storing the snp quality
            snpQUAL_data.append( currentSNP.snpQUAL )
            # Heterozygous snp depth histogram
            try:
                hetDepth_data[currentSNP.totalCallDepth] += 1
            except KeyError:
                hetDepth_data[currentSNP.totalCallDepth] = 1
            #####
        else:
            # Homozygous snp depth histogram
            try:
                homDepth_data[currentSNP.totalCallDepth] += 1
            except KeyError:
                homDepth_data[currentSNP.totalCallDepth] = 1
            #####
        #####
    #####

    #----------------------------------------------------        
    # Computing the het vs depth histograms
    hetHistDataFile = join( basePath, '%s_hetdepth.data.dat'%RUN_ID )
    if ( len(hetDepth_data.keys()) > 0 ):
        min_hetDepth   = min( hetDepth_data.keys() )
        max_hetDepth   = max( hetDepth_data.keys() )
        hetDepth_hist  = dict( [(n,0.0) for n in xrange(min_hetDepth,max_hetDepth+1)] )
        total_het_snps = sum( hetDepth_data.values() )
        c_het          = 1.0 / float( total_het_snps )
        for tmpDepth in hetDepth_data.keys():
            hetDepth_hist[tmpDepth] = c_het * float( hetDepth_data[tmpDepth] )
        #####
        # Pulling the min and max values for the plot
        het_xaxis_min_plot = round_dwn( min_hetDepth, 10 )
        het_xaxis_max_plot = round_up(  max_hetDepth, 10 )
        het_yaxis_max_plot = round_up( max( hetDepth_hist.values() ), 0.025 )
        # write to a file
        het_oh = open( hetHistDataFile, 'w' )
        for tmpDepth in hetDepth_hist.iterkeys(): het_oh.write('%d\t%.5f\n'%( tmpDepth, hetDepth_hist[tmpDepth] ) )
        het_oh.close()
    else:
        het_oh = open( hetHistDataFile, 'w' )
        het_oh.close()
    #####

    #----------------------------------------------------        
    # Computing the hom vs depth histograms
    homHistDataFile = join( basePath, '%s_homdepth.data.dat'%RUN_ID )
    if ( len(homDepth_data.keys()) > 0 ):
        min_homDepth   = min( homDepth_data.keys() )
        max_homDepth   = max( homDepth_data.keys() )
        homDepth_hist  = dict( [(n,0.0) for n in xrange(min_homDepth,max_homDepth+1)] )
        total_hom_snps = sum( homDepth_data.values() )
        c_hom          = 1.0 / float( total_hom_snps )
        for tmpDepth in homDepth_data.keys():
            homDepth_hist[tmpDepth] = c_hom * float( homDepth_data[tmpDepth] )
        #####
        # Pulling the min and max values for the plot
        hom_xaxis_min_plot = round_dwn( min_homDepth, 10 )
        hom_xaxis_max_plot = round_up(  max_homDepth, 10 )
        hom_yaxis_max_plot = round_up( max( homDepth_hist.values() ), 0.025 )
        # write to a file
        hom_oh = open( homHistDataFile, 'w' )
        for tmpDepth in homDepth_hist.iterkeys(): hom_oh.write('%d\t%.5f\n'%( tmpDepth, homDepth_hist[tmpDepth] ) )
        hom_oh.close()
    else:
        hom_oh = open( homHistDataFile, 'w' )
        hom_oh.close()
    #####

    #----------------------------------------------------        
    # Computing the ref_to_alt_ratio histogram
    hmax                  = max( ref_to_alt_ratio_data )
    hmin                  = min( ref_to_alt_ratio_data )
    ref_to_alt_ratio_hist = histogramClass( hmin, hmax, 100 )
    map( ref_to_alt_ratio_hist.addData, ref_to_alt_ratio_data )
    # Pulling histogram information
    ratioDict = ref_to_alt_ratio_hist.generateOutputDict()
    # Normalizing the values
    x_values = ratioDict['midpoint']
    y_values = ratioDict['counts']
    c        = 1.0 / float( sum(y_values) )
    y_values = [ c * float(item) for item in y_values ]
    # Pulling the min and max values for the plot
    ref_to_alt_ratio_yaxis_max_plot = round_up( max(y_values), 0.025 )
    # Writing the file
    refAltRatioDataFile = join( basePath, '%s_refAltRatio.data.dat'%RUN_ID )
    oh_ratio = open( refAltRatioDataFile, 'w' )
    for n in xrange(len(y_values)):
        oh_ratio.write('%.5f\t%.5f\n'%( x_values[n], y_values[n] ) )
    #####
    oh_ratio.close()

    #----------------------------------------------------        
    # Computing the snp quality histogram
    min_snpQUAL  = min( snpQUAL_data )
    max_snpQUAL  = max( snpQUAL_data )
    snpQUAL_hist = histogramClass( min_snpQUAL, max_snpQUAL, 100 )
    map( snpQUAL_hist.addData, snpQUAL_data )
    # Pulling histogram information
    qualDict = snpQUAL_hist.generateOutputDict()
    # Normalizing the values
    x_values = qualDict['midpoint']
    y_values = qualDict['counts']
    c        = 1.0 / float( sum(y_values) )
    y_values = [ c * float(item) for item in y_values ]
    # Pulling the min and max values for the plot
    qual_xaxis_min_plot = round_dwn( min(x_values), 10    )
    qual_xaxis_max_plot = round_up(  max(x_values), 10    )
    qual_yaxis_max_plot = round_up(  max(y_values), 0.025 )
    # Writing the file
    snpQUAL_File = join( basePath, '%s_snpQUAL.data.dat'%RUN_ID )
    SQ_oh = open( snpQUAL_File, 'w' )
    for n in xrange(len(y_values)):
        SQ_oh.write('%.5f\t%.5f\n'%( x_values[n], y_values[n] ) )
    #####
    SQ_oh.close()
    
    # Making the plots
    plotHistograms = join( basePath, '%s.SNP.gnuplot.dat'%RUN_ID )
    histogramPNG   = join( basePath, '%s.SNP.png'%RUN_ID )
    oh          = open( plotHistograms, 'w' )
    oh.write( '\nreset\nset output "%s"\n' % histogramPNG )
    oh.write( 'set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\nset nokey\nset multiplot title \"%s\"\n' % RUN_ID )
    oh.write( 'set grid x y\nset size 0.5,0.5\nset origin 0,0.48\nset title \"Heterozygous Depth: %d SNPs\"\n' % total_het_snps )
    oh.write( 'set xlabel \"Depth\"\nset ylabel \"Frequency\"\nset yrange [0:%.2f]\nset xrange [%d:%d]\n'%(het_yaxis_max_plot, het_xaxis_min_plot, het_xaxis_max_plot+5) )
    oh.write( 'plot \"< sort -n %s\" using 1:2 axis x1y1 with boxes\n'%hetHistDataFile )
    oh.write( 'set size 0.5,0.5\nset origin 0.5,0.48\nset title \"Homozygous Depth: %d SNPs\"\n'%total_hom_snps )
    oh.write( 'set xlabel \"Depth\"\nset ylabel \"Frequency\"\nset yrange [0:%.2f]\nset xrange [%d:%d]\n'%(hom_yaxis_max_plot, hom_xaxis_min_plot, hom_xaxis_max_plot+5) )
    oh.write( 'plot \"< sort -n %s\" using 1:2 axis x1y1 with boxes\n'%homHistDataFile )
    oh.write( 'set size 0.5,0.5\nset origin 0,-0.01\nset title \"Ref to Alt Ratio - Heterozygous\"\nset xlabel \"Ref to Alt Ratio\"\nset ylabel \"Frequency\"\nset yrange [0:%.2f]\nset xrange [0:5]\n'%ref_to_alt_ratio_yaxis_max_plot )
    oh.write( 'plot \"< sort -n %s\" using 1:2 axis x1y1 with boxes\n'%refAltRatioDataFile )
    oh.write( 'set size 0.5,0.5\nset origin 0.5,-0.01\nset title \"SNP Quals - Heterozygous\"\nset xlabel \"SNP Qual\"\nset ylabel \"Frequency\"\nset yrange [0:%.2f]\nset xrange [%d:%d]\nset xtics %d %d\n'%(qual_yaxis_max_plot, qual_xaxis_min_plot, qual_xaxis_max_plot, qual_xaxis_min_plot, (qual_xaxis_max_plot - qual_xaxis_min_plot)/5) )
    oh.write( 'plot \"< sort -n %s\" using 1:2 axis x1y1 with boxes\n'%snpQUAL_File)
    oh.close()
    echoAndExecute( 'gnuplot %s' %plotHistograms, cmd_log, execute=True )

#==============================================================
def SNP_GC_histogram( vcfFile, genomeFASTA, RUN_ID, basePath, binSize, cmd_log ):

    # Initial GC calculation
    GC_snps_dict = {}
    for r in iterFASTA( open(genomeFASTA) ):
        # Building the dictionary
        GC_snps_dict[r.id] = GC_depth_class( r.id, str(r.seq), binSize )
    #####
    
    # Reading in the VCF file
    for line in open( vcfFile ):
        # Screening the comment lines
        if line[0] == '#': continue
        # Parsing the VCF line
        currentSNP = vcfSplitter(line)
        # Checking for too many variants
        if ( not currentSNP.goodSNP ): continue
        # Screening the depth ratio on the het
        if ( currentSNP.isHet ):
            # Screening the depth ratio on the het minor allele must be in the bound of 20 to 80
            if ( isBad_SNP(currentSNP.altAllele_depthFrac) ): continue
            # Adding the snp to the dataset
            GC_snps_dict[currentSNP.scaffID].add_SNP( currentSNP.scaffPos )
        else:
            # Adding the snp to the dataset
            GC_snps_dict[currentSNP.scaffID].add_SNP( currentSNP.scaffPos )
        #####
    #####
    
    # Pulling the data
    GC_depth_data = []
    for scaffID in GC_snps_dict.iterkeys():
        for nSNPs, GC  in GC_snps_dict[scaffID].get_SNP_data():
            for n in xrange(nSNPs): GC_depth_data.append( GC )
        #####
    #####
    
    # Creating the bar chart histogram
    min_GC   = round_dwn( min( GC_depth_data ), 5 )
    max_GC   = round_up(  max( GC_depth_data ), 5 )
    snp_hist = histogramClass( min_GC, max_GC, 100 )
    map( snp_hist.addData, GC_depth_data )
    
    # Pulling histogram information
    histDict = snp_hist.generateOutputDict()
    newHist  = histDict['counts']
    midpoint = histDict['midpoint']
    
    # computing the roundup value
    ymax = round_up( max(newHist), pow( 10.0, floor( log(max(newHist),10) ) ) )

    dataFile   = join( basePath, '%s.GC_vs_snps.data.dat'%RUN_ID )
    oh = open('%s'%dataFile, 'w')
    for n in xrange(len(newHist)):
        oh.write( '%.1f\t%.8f\n'%( midpoint[n], newHist[n] ) )
    #####
    oh.close()
    
    # Making the gnu plot
    plotFileGC = join( basePath, '%s.GC_vs_snps.gnuplot.dat'%RUN_ID )
    SNP_GC_PNG = join( basePath, '%s.GC_vs_snps.png'%RUN_ID )
    dplot    = open('%s'%plotFileGC, 'w')
    dplot.write('\nreset\nset output "%s"\n'%SNP_GC_PNG)
    dplot.write('set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\nset nokey\nset title \"%s SNPs GC\"\n'%RUN_ID)
    dplot.write('set grid x y\nset xlabel \"GC Content\"\nset ylabel \"# of SNPs\"\nset yrange [0:%.2f]\nset xrange [%d:%d]\nplot \"< sort -n %s\" using 1:2 axis x1y1 with boxes\n'%(ymax,(min_GC-1),(max_GC+1),dataFile))
    dplot.close()
    echoAndExecute( 'gnuplot %s' % plotFileGC, cmd_log, execute=True )

#==============================================================	
def call_INDELs( genomeFASTA, GATKbam, INDEL_rawCalls, INDEL_preFilter, INDEL_postFilter, RUN_ID, basePath, cmd_log, lowFilter, highFilter, avgDepth, \
                 MQ_CUTOFF, configFile ):

    #---------------------------------------------------------------------------------------------------
    # Downsampling amount
    # Rule of thumb is 10x the average depth, with a minimum of 250
    dCov = max( int(10.0*avgDepth), 400 )
    
    # Forcing annotation of the calls
    FA_list = []
    FA_list.append( '-A QualByDepth' )
    FA_list.append( '-A RMSMappingQuality' )
    FA_list.append( '-A FisherStrand' )
    FA_list.append( '-A Coverage' )
    FA_list.append( '-A InbreedingCoeff' )
    FA_list.append( '-A MappingQualityRankSumTest' )
    FA_list.append( '-A ReadPosRankSumTest' )
#     FA_list.append( '-A StrandBiasBySample' )
    FA_list.append( '-A MappingQualityZero' )
    FA = ' '.join( FA_list )
    
    #---------------------------------------------------------------------------------------------------
    # Running the unified genotyper
    # -glm SNP      This is the genotype likelihoods model for indel calling
    # -minIndelCnt  This is the minimum number of consensus indels required to trigger genotyping run
    # -minIndelFrac This is the minimum fraction of all reads at a locus that must contain an indel (of any allele) for that sample to contribute to the indel count for alleles
    del_vcf_file      = 'rm -f %s' % INDEL_rawCalls
    raw_INDEL_calling = '%s -T UnifiedGenotyper -glm INDEL -minIndelCnt 2 -minIndelFrac 0.25 -R %s -dcov %d -I %s -o %s %s --logging_level ERROR' % ( preGATKcmd, genomeFASTA, dCov, GATKbam, INDEL_rawCalls, FA )
    
    #---------------------------------------------------------------------------------------------------
    # Running VariantFiltration:
    # 
    #   LowQD:
    #   QD < 2.0                 QD = variant confidence (QUAL from column 6) divided by unfiltered allele depth (AD)
    #
    #   Mapping Quality range:
    #   MQ < 51.0                MQ = Root Mean Square of all of the MQ values for all reads (from all samples) used at that site used for genotying.
    #   MQ > 61.0                     The reason we use this filter is because MQ > 61 typically indicates a read that has high quality ambiguous alignments
    #
    #   HighStrandBias:
    #   FS > 200.0               FS = Strand bias estimated using Fisher's Exact Test, phred scaled p-value, larger values indicate more bias (200 is used for indels)
    #
    #   Coverage range:
    #   DP < %d                  DP = Count of reads that passed the caller's internal quality control metrics on a site
    #   DP > %d
    #
    #   Low_Quality:
    #   QUAL < 50.0              QUAL = Phred scaled quality score, High QUAL scores indicate high confidence calls.
    #   
    #   failed_InbreedingCoeff
    #   InbreedingCoeff < -0.80  Inbreeding coefficient as estimated from the genotype likelihoods per-sample when compared against the Hardy-Weinberg expectation
    #
    #   
    #   failed_MQRankSum:
    #   MQRankSum < -12.5        MQRankSum      = Compares mapping qualities of reads supporting the reference allele with those supporting the alternate. The ideal result is a value close to zero.  
    #                                             A negative value indicates that the reads supporting the alternate allele have lower mapping quality scores than those supporting the reference allele
    #   
    #   failed_ReadPosRankSum:
    #   ReadPosRankSum < -20.0   ReadPosRankSum = Tests whether there is evidence of bias in the position of alleles within the reads that support them.  
    #                                             The ideal result is a value close to zero, which indicates there is little to no difference in where the alleles are found relative to the ends of reads. 
    #                                             A negative value indicates that the alternate allele is found at the ends of reads more often than the reference allele. 
    #   
    #   failed_StrandBias:
    #   SB > -10.0             SB = Per-sample component statistics which comprise the Fisher's Exact Test to detect strand bias
    #
    #   HARD_TO_VALIDATE:
    #   MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)   Basically failing a variant if there are more than 4 reads of mapping quality of zero, or if the MQ0 reads make up
    #                                            more than 10% of the number of reads to call the variant
    #    
    #
    #   GQ_LT_25:
    #   GQ                                       Genotype quality, essentially a phred scaled score for the overall quality of the genotype call
    #
    # --clusterWindowSize 10                    The window size (in bases) in which to evaluate clustered SNPs
    #
    # --logging_level ERROR                     Keeps the log file from exploding in size 
    #
    #---------------------------------------------------------------------------------------------------
    FE_list = []
#     FE_list.append( r'--filterExpression "SB > -10.0" --filterName "failed_StrandBias"' )
#     FE_list.append( r'--filterExpression "MQ > 61.0" --filterName "MQ_tooHigh"' )
#     FE_list.append( r'--genotypeFilterExpression "GQ < 25" --genotypeFilterName "GQ_LT_25"' )
    FE_list.append( r'--filterExpression "QD < 2.0" --filterName "LowQD"' )
    FE_list.append( r'--filterExpression "MQ < %d.0" --filterName "low_MQ"' % MQ_CUTOFF )
    FE_list.append( r'--filterExpression "FS > 200.0" --filterName "HighStrandBias"' )
    FE_list.append( r'--filterExpression "DP < %d" --filterName "low_COV"'%( lowFilter ) )
    FE_list.append( r'--filterExpression "DP > %d" --filterName "COV_TooHigh"'%( highFilter ) )
    FE_list.append( r'--filterExpression "QUAL < 50.0" --filterName "Low_Quality"' )
    FE_list.append( r'--filterExpression "InbreedingCoeff < -0.8" --filterName "failed_InbreedingCoeff"' )
    FE_list.append( r'--filterExpression "MQRankSum < -12.5" --filterName "failed_MQRankSum"' )
    FE_list.append( r'--filterExpression "ReadPosRankSum < -20.0" --filterName "failed_ReadPosRankSum"' )
    FE_list.append( r'--filterExpression "MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)" --filterName "HARD_TO_VALIDATE"' )
    FE = ' '.join( FE_list )

    # Have decided to remove the SB strand bias
    # Also removed the upper bound on mapping quality
    # Also removed the genotype quality filter, as it was too heavy handed.
    
    filterINDELs = '%s -R %s -T VariantFiltration --logging_level ERROR --variant %s -o %s %s'%(preGATKcmd, genomeFASTA, INDEL_rawCalls, INDEL_preFilter, FE )
    
    # Filtering the vcf file
    grepFilter = r'rm -f %s ; cat %s | grep PASS | grep -v GQ_LT_25 | grep -v "^#" > %s' % ( INDEL_postFilter, INDEL_preFilter, INDEL_postFilter )
    mv_IDX     = 'mv %s.idx %s.idx'%( INDEL_preFilter, INDEL_postFilter )
    
    # Putting the commands together
    cmdList    = [ del_vcf_file, \
                   raw_INDEL_calling, \
                   filterINDELs, \
                   grepFilter, \
                   mv_IDX ]
    
    return cmdList
    
#==============================================================
class vcfSplitter:
    
    def __init__(self, line):
        # Pulling the initial information
        self.s        = line.split(None)
        self.scaffID  = self.s[0]
        self.scaffPos = int( self.s[1] )
        self.regSet   = set(["0/1", "1/1"])

        # Building the info dictionary
        self.infoDict = dict( [ item.split('=')  for item in self.s[7].split(';') if (item.count('=') > 0)] )
        
        # Pulling the snp quality Q/D
        try:
            self.snpQUAL = float( self.infoDict['QD'] )
        except KeyError:
            self.snpQUAL = float( self.s[5] )
        #####

        # Building the depth dictionary
        depthKeys      = self.s[8].split(':')
        depthValues    = self.s[9].split(':')
        self.depthDict = dict( [ (depthKeys[n], depthValues[n]) for n in xrange(len(depthKeys)) ] )
	    
        # preComputing call depth
        self.goodSNP = True
        self.is_1_2  = False
        split_AD     = self.depthDict['AD'].split(',')
        if ( self.depthDict['GT'] in self.regSet ):
            # Computing zygosity
            # GT:  0/1 is a heterozygous and 1/1 homozygous
            self.isHet = ( self.depthDict['GT'] == "0/1" )
            # Generating the ref and alt bases
            self.refBase  = self.s[3]
            self.altBase  = self.s[4]
            # Initializing the variables
            self.refDepth, self.altDepth = map( int, split_AD )
            self.totalCallDepth          = self.refDepth + self.altDepth
            # Genotype Quality
            self.genotypeQUAL            = int( self.depthDict['GQ'] )
            #------------------------------------
            # Phase likelihood values (PL)
            # The higher the number, the less likely it is to be the case
            # P_0_0 is homozygous reference
            # P_0_1 is heterozygous
            # P_1_1 is homozygous alternate
            # PL of most likely genotype should be zero
#             self.P_0_0, self.P_0_1, self.P_1_1 = map( int, self.depthDict['PL'].split(',') )
            # Computing the ratio
            try:
                self.altAllele_depthFrac = float(self.altDepth) / float(self.totalCallDepth)  # old hetCheck
                self.refAllele_depthFrac = float(self.refDepth) / float(self.totalCallDepth)
                self.ref_to_alt_ratio    = float(self.refDepth) / float(self.altDepth)        # old hetRatio
            except ZeroDivisionError:
                self.altAllele_depthFrac = None  # old hetCheck
                self.refAllele_depthFrac = None
                self.ref_to_alt_ratio    = None  # old hetRatio
            #####
        elif ( self.depthDict['GT'] == "1/2" ):
            #================================
            # Computing zygosity
            # Strictly speaking this is a het call (GT:1/2), but it is actually just
            # an error in the consensus that needs to be fixed
            self.isHet  = True
            self.is_1_2 = True
            # Setting the type
            self.refBase    = self.s[3]
            splitBase       = self.s[4].split(',')
            self.altBase_1  = splitBase[0]
            self.altBase_2  = splitBase[1]
            #=====================================
            # Keep in mind that the reference base (tmpDepthList[0]) was not
            # present, but both alternate bases were present
            tmpDepthList = map( int, split_AD )
            self.refDepth = tmpDepthList[1]
            self.altDepth = tmpDepthList[2]
            self.totalCallDepth          = self.refDepth + self.altDepth
            # Genotype Quality
            self.genotypeQUAL            = int( self.depthDict['GQ'] )
            #------------------------------------
            # Phase likelihood values (PL)
            # The higher the number, the less likely it is to be the case
            # P_0_0 is homozygous reference
            # P_0_1 is heterozygous
            # P_1_1 is homozygous alternate
            # PL of most likely genotype should be zero
#             self.P_0_0, self.P_0_1, self.P_1_1 = map( int, self.depthDict['PL'].split(',') )
            # Computing the ratio
            try:
                self.altAllele_depthFrac = float(self.altDepth) / float(self.totalCallDepth)  # old hetCheck
                self.refAllele_depthFrac = float(self.refDepth) / float(self.totalCallDepth)
                self.ref_to_alt_ratio    = float(self.refDepth) / float(self.altDepth)        # old hetRatio
            except ZeroDivisionError:
                self.altAllele_depthFrac = None  # old hetCheck
                self.refAllele_depthFrac = None
                self.ref_to_alt_ratio    = None  # old hetRatio
            #####
        else:
            self.goodSNP = False
        #####
	    
#==============================================================
def vcfContextGenerator (vcfFile, genomeFASTA, het_outFile, hom_outFile, cmd_log):
    
    # Initializing the variables
    flankSize = 100 # How much sequence is on either side of the flanking region
    homs      = {}
    hets      = {}
    hets_1_2  = {}
    
    # Reading in the VCF file
    for line in open( vcfFile ):
        # Screening the comment lines
        if line[0] == '#': continue
        # Parsing the VCF line
        try:
            currentSNP = vcfSplitter(line)
        except ValueError:
            print line
            assert False
        #####
        snpPos     = currentSNP.scaffPos - 1 # This is shifted by 1 position because it is zero based
        # This is the case where it is not 0/1, 1/1, or 1/2, but something entirely different
        if ( not currentSNP.goodSNP ): continue
        # Screening the depth ratio on the het
        if ( currentSNP.isHet ):
            if ( currentSNP.is_1_2 ): # The case of a consensus error
                try:
                    hets_1_2[currentSNP.scaffID].append( (snpPos, currentSNP.refBase, currentSNP.altBase_1) )
                except KeyError:
                    hets_1_2[currentSNP.scaffID] = [ (snpPos, currentSNP.refBase, currentSNP.altBase_1) ]
                #####
                try:
                    hets_1_2[currentSNP.scaffID].append( (snpPos, currentSNP.refBase, currentSNP.altBase_2) )
                except KeyError:
                    hets_1_2[currentSNP.scaffID] = [ (snpPos, currentSNP.refBase, currentSNP.altBase_2) ]
                #####
            else: # The case of a true het
                try:
                    hets[currentSNP.scaffID].append( (snpPos, currentSNP.refBase, currentSNP.altBase) )
                except KeyError:
                    hets[currentSNP.scaffID] = [ (snpPos, currentSNP.refBase, currentSNP.altBase) ]
                #####
            #####
        else:
            try:
                homs[currentSNP.scaffID].append( (snpPos, currentSNP.refBase, currentSNP.altBase) )
            except KeyError:
                homs[currentSNP.scaffID] = [ (snpPos, currentSNP.refBase, currentSNP.altBase) ]
            #####
        #####
    #####
    
    # Header Text
    headerText = 'Scaffold\tPosition\tSequence Context\tZygosity\n'
    
    # Writing the output file
    oh_het = open( het_outFile, 'w' )
    oh_hom = open( hom_outFile, 'w' )

    oh_het.write( headerText )
    oh_hom.write( headerText )

    for r in iterFASTA( open(genomeFASTA) ):
        scaffID = r.id
        tmpSeq  = str( r.seq )
        nSize   = len(tmpSeq)
        #==========================================
        # Writing the true hets
        try:
            for snpPos, refBase, altBase in hets[scaffID]:
                # Computing the start position
                refLen   = len(refBase)
                start    = max( 0, (snpPos - flankSize) )
                end      = min( (snpPos + refLen + flankSize), nSize )
                leftSeq  = tmpSeq[           start : snpPos ]
                rightSeq = tmpSeq[ (snpPos+refLen) : end    ]
                oh_het.write( '%s\t%d\t%s(%s/%s)%s\tHeterozygous\n'%( scaffID, (snpPos+1), leftSeq.upper(), refBase, altBase, rightSeq.upper() ) )
            #####
        except KeyError:
            continue
        #####
        #==========================================
        # Writing the true homozygous snps
        try:
            for snpPos, refBase, altBase in homs[scaffID]:
                # Computing the start position
                refLen   = len(refBase)
                start    = max( 0, (snpPos - flankSize) )
                end      = min( (snpPos + refLen + flankSize), nSize )
                leftSeq  = tmpSeq[           start : snpPos ]
                rightSeq = tmpSeq[ (snpPos+refLen) : end    ]
                oh_hom.write( '%s\t%d\t%s(%s/%s)%s\tHomozygous\n'%( scaffID, (snpPos+1), leftSeq.upper(), refBase, altBase, rightSeq.upper() ) )
            #####
        except KeyError:
            continue
        #####
        #==========================================
        # Writing the 1/2 homozygous snps that need to be fixed
        try:
            for snpPos, refBase, altBase in hets_1_2[scaffID]:
                # Computing the start position
                refLen   = len(refBase)
                start    = max( 0, (snpPos - flankSize) )
                end      = min( (snpPos + refLen + flankSize), nSize )
                leftSeq  = tmpSeq[           start : snpPos ]
                rightSeq = tmpSeq[ (snpPos+refLen) : end    ]
                oh_hom.write( '%s\t%d\t%s(%s/%s)%s\tHomozygous_1_2\n'%( scaffID, (snpPos+1), leftSeq.upper(), refBase, altBase, rightSeq.upper() ) )
            #####
        except KeyError:
            continue
        #####
    #####
    oh_het.close()
    oh_hom.close()

#==============================================================
class summaryClass(object):
    def __init__( self, scaffID, tmpSeq, lowFilter, highFilter, softMask_filter_length, gap_filter_length ):
        
        # Initializing the N's to zero
        # gap               = 0
        # good base         = 1
        # lowerCase         = 2
        # lowerCaseAdjacent = 3
        # gapAdjacent       = 4
        self.GAP                 = 0
        self.GOOD_BASE           = 1
        self.LOWER_CASE          = 2
        self.LOWER_CASE_ADJACENT = 3
        self.GAP_ADJACENT        = 4
        self.FAILED_DEPTH        = 5
        
        # Initial information
        self.scaffID    = scaffID
        self.tmpSeq     = tmpSeq
        self.nSize      = len(tmpSeq)
        
        self.softMask_filter_length = softMask_filter_length
        self.gap_filter_length      = gap_filter_length
        
        # Counting basic information
        self.Ns         = tmpSeq.count('N')
        self.totalBases = self.nSize - self.Ns
        self.UC_bases   = tmpSeq.count('A') + tmpSeq.count('C') + tmpSeq.count('T') + tmpSeq.count('G')
        self.LC_bases   = self.totalBases - self.UC_bases
        
        # Depth filtering variables
        self.lowFilter  = lowFilter
        self.highFilter = highFilter
        
        # Generating the depth base list
        self.depthDict = { True:1, False:0 }
        self.UC_depth_pass_bases = 0
        self.UC_depth_fail_bases = 0
        self.goodDepth = lambda depth: ( lowFilter <= depth <= highFilter )
        
        # Generating the lower case filtering criteria
        self.N_LC_set = set( 'abcdefghijklmnopqrstuvwxyzN' )
        self.UC_set   = set( 'ACTG' )
        self.LC_set   = set( 'abcdefghijklmnopqrstuvwxyz' )
        
        # Filtering for gap and lowercase regions
        self.dummySeq = array( self.nSize * [1] )
        for n in xrange( self.nSize ):
            if ( self.tmpSeq[n] == 'N' ):
                self.dummySeq[n] = self.GAP
            elif ( self.tmpSeq[n] in self.LC_set ):
                self.dummySeq[n] = self.LOWER_CASE
            #####
        #####
        
        # Counting mask adjacent bases
        if ( self.softMask_filter_length > 0 ):
            for item in findLowerCase( self.tmpSeq ): 
                start, stop = item.span()
                newStart    = max(          0, start - self.softMask_filter_length )
                newStop     = min( self.nSize, stop  + self.softMask_filter_length )
                for pos in xrange( newStart, newStop ):
                    # If we have a good base, then it is lowercase adjacent
                    if ( self.dummySeq[pos] == self.GOOD_BASE ):
                        self.dummySeq[pos] = self.LOWER_CASE_ADJACENT
                    #####
                #####
            #####
        #####

        # Counting gap adjacent sequence
        if ( self.gap_filter_length > 0 ):
            for item in findGaps( self.tmpSeq ): 
                start, stop = item.span()
                newStart    = max(          0, start - self.gap_filter_length )
                newStop     = min( self.nSize, stop  + self.gap_filter_length )
                for pos in xrange( newStart, newStop ):
                    # If it is good, then it is gap adjacent
                    if ( self.dummySeq[pos] == self.GOOD_BASE ):
                        self.dummySeq[pos] = self.GAP_ADJACENT
                    # If it is lowercase adjacent, then it is gap adjacent
                    elif ( self.dummySeq[pos] == self.LOWER_CASE_ADJACENT ):
                        self.dummySeq[pos] = self.GAP_ADJACENT
                    #####
                #####
            #####
        #####

    def testDepth(self,pos,depth):
        # Checking to see if a base is:
        #  (1) Good Depth
        #  (2) Upper Case
        #  (3) Not mask adjacent
        #  (4) Not gap adjacent
        if ( pos > self.nSize ): return
        try:
            if ( (self.dummySeq[pos-1] == self.GOOD_BASE) ):
                if ( not self.goodDepth(depth) ):
                    self.dummySeq[pos-1] = self.FAILED_DEPTH
                #####
            #####
            self.UC_depth_pass_bases += self.depthDict[ (   self.goodDepth(depth)  and \
                                                          ( self.tmpSeq[pos-1] in self.UC_set) and \
                                                          (self.dummySeq[pos-1] != self.LOWER_CASE_ADJACENT) and \
                                                          (self.dummySeq[pos-1] != self.GAP_ADJACENT) ) ]
            self.UC_depth_fail_bases += self.depthDict[ ( (not self.goodDepth(depth)) and \
                                                          (self.tmpSeq[pos-1] in self.UC_set) and \
                                                          (self.dummySeq[pos-1] != self.LOWER_CASE_ADJACENT) and \
                                                          (self.dummySeq[pos-1] != self.GAP_ADJACENT) ) ]
        except IndexError:
            print self.scaffID
            print self.nSize
            print pos, depth
        #####

    def outputString(self):
        # Summing up the totals
        self.totalGap             = sum( [1 for item in self.dummySeq if (item == self.GAP)] )
        self.UC_depth_pass_bases  = sum( [1 for item in self.dummySeq if (item == self.GOOD_BASE)] )
        self.lowerCaseMaskedBases = sum( [1 for item in self.dummySeq if (item == self.LOWER_CASE)] )
        self.maskAdjacentBases    = sum( [1 for item in self.dummySeq if (item == self.LOWER_CASE_ADJACENT)] )
        self.gapAdjacentBases     = sum( [1 for item in self.dummySeq if (item == self.GAP_ADJACENT)] )
        self.UC_depth_fail_bases  = sum( [1 for item in self.dummySeq if (item == self.FAILED_DEPTH)] )
        print 'scaffID    = %s'%self.scaffID
        print 'totalSize  = %d'%self.nSize
        print 'totalBases = %d'%self.totalBases
        print 'Ns         = %d'%self.totalGap
        print 'UC_bases   = %d'%self.UC_bases
        print 'Depth Pass = %d'%self.UC_depth_pass_bases
        print 'Depth Fail = %d'%self.UC_depth_fail_bases
        print 'Masked     = %d'%self.lowerCaseMaskedBases
        print 'Mask Adj   = %d'%self.maskAdjacentBases
        print 'Gap Adj    = %d'%self.gapAdjacentBases
        print '-----------------------------'
    
#==============================================================
def writeSummaryFiles ( cleanedBam, genomeFASTA, softMask_filter_length, gap_filter_length, lowFilter, highFilter, RUN_ID, basePath ):

    # Setting up the summary class
    print "Initializing the Summary Class"
    scaffDict = {}
    for r in iterFASTA( open( genomeFASTA ) ):
        scaffDict[r.id] = summaryClass( r.id, str(r.seq), lowFilter, highFilter, softMask_filter_length, gap_filter_length )
    #####
    
    # Filtering for depth
    print "Analyzing Depth"
    p = subprocess.Popen( 'samtools depth -q 0 -Q 0 %s'%cleanedBam, shell=True, stdout=subprocess.PIPE )
    for line in p.stdout:
        scaffID, pos, depth = line.split(None)
        pos, depth = map( int, [pos,depth] )
        scaffDict[scaffID].testDepth( pos, depth )
    #####
    p.poll()
    
    # Computing the total callable bases
    UC_depth_pass_bases     = 0
    lowerCaseMaskedBases    = 0
    totalMaskAdjacentBases  = 0
    totalGapAdjacentBases   = 0
    totalBases              = 0
    UC_depth_fail_bases     = 0
    totalGap                = 0
    for scaffID, tmpClass in scaffDict.iteritems():
        tmpClass.outputString()
        totalGap                += tmpClass.totalGap
        UC_depth_pass_bases     += tmpClass.UC_depth_pass_bases
        lowerCaseMaskedBases    += tmpClass.lowerCaseMaskedBases
        totalMaskAdjacentBases  += tmpClass.maskAdjacentBases
        totalGapAdjacentBases   += tmpClass.gapAdjacentBases
        UC_depth_fail_bases     += tmpClass.UC_depth_fail_bases
        totalBases              += tmpClass.totalBases
    #####

    # Computing the total homozygous and heterozygous snps and indels
    snp_indel_counts = {}
    for tmpType in ['SNP','INDEL']:
        snp_indel_counts[tmpType] = {}
        for tmpKind in ['Homozygous','Heterozygous']:
            fileName = join( basePath, '%s.%s.%s.context.dat'%(RUN_ID,tmpType,tmpKind) )
#             snp_indel_counts[tmpType][tmpKind] = len([line.strip() for line in open(fileName) if (line.split(None)[0] != 'Scaffold')])
            for line in open(fileName):
                s = line.split(None)
                if ( s[0] == 'Scaffold' ): continue
                localVarKind = s[3]
                try:
                    snp_indel_counts[tmpType][localVarKind] += 1
                except KeyError:
                    snp_indel_counts[tmpType][localVarKind] = 1
                #####
            #####
        #####
    #####
    
    try:
        allBases_hetSNPrate  = 1000.0 * float(snp_indel_counts['SNP']['Heterozygous'])   / float(totalBases)
    except KeyError:
        allBases_hetSNPrate = 0.0
        snp_indel_counts['SNP']['Heterozygous'] = 0
    #####
    
    try:
        allBases_homSNPrate  = 1000.0 * float(snp_indel_counts['SNP']['Homozygous'])     / float(totalBases)
    except KeyError:
        allBases_homSNPrate = 0
        snp_indel_counts['SNP']['Homozygous'] = 0
    #####
    
    try:
        allBases_1_2_SNPrate = 1000.0 * float(snp_indel_counts['SNP']['Homozygous_1_2']) / float(totalBases)
    except KeyError:
        allBases_1_2_SNPrate = 0.0
        snp_indel_counts['SNP']['Homozygous_1_2'] = 0
    #####
    
    try:
        callable_hetSNPrate  = 1000.0 * float(snp_indel_counts['SNP']['Heterozygous'])   / float(UC_depth_pass_bases)
    except KeyError:
        callable_hetSNPrate = 0.0
        snp_indel_counts['SNP']['Heterozygous'] = 0
    #####

    try:
        callable_homSNPrate  = 1000.0 * float(snp_indel_counts['SNP']['Homozygous'])     / float(UC_depth_pass_bases)
    except KeyError:
        callable_homSNPrate = 0.0
        snp_indel_counts['SNP']['Homozygous'] = 0
    #####

    try:
        callable_1_2_SNPrate = 1000.0 * float(snp_indel_counts['SNP']['Homozygous_1_2']) / float(UC_depth_pass_bases)
    except KeyError:
        callable_1_2_SNPrate = 0.0
        snp_indel_counts['SNP']['Homozygous_1_2'] = 0
    #####
    
    # Testing to see if the indel rate is zero
    try:
        xyz = snp_indel_counts['INDEL']['Homozygous']
    except KeyError:
        snp_indel_counts['INDEL']['Homozygous'] = 0
    #####
    
    try:
        xyz = snp_indel_counts['INDEL']['Heterozygous']
    except KeyError:
        snp_indel_counts['INDEL']['Heterozygous'] = 0
    #####

    try:
        xyz = snp_indel_counts['INDEL']['Homozygous_1_2']
    except KeyError:
        snp_indel_counts['INDEL']['Homozygous_1_2'] = 0
    #####

    allBases_INDELrate  = 1000.0 * float( snp_indel_counts['INDEL']['Heterozygous'] + snp_indel_counts['INDEL']['Homozygous'] + snp_indel_counts['INDEL']['Homozygous_1_2'] ) / float(totalBases)
    callable_INDELrate  = 1000.0 * float( snp_indel_counts['INDEL']['Heterozygous'] + snp_indel_counts['INDEL']['Homozygous'] + snp_indel_counts['INDEL']['Homozygous_1_2']) / float(UC_depth_pass_bases)
    
	# Create summary file
    summaryFile = join( basePath, '%s.Summary.dat'%RUN_ID )
    oh          = open( summaryFile , 'w' )
    oh.write( 'Total Bases:             %s\n'   % commify(totalBases)                                    )
    oh.write( 'Total Gap:               %s\n'   % commify(totalGap)                                      )
    oh.write( 'Gap Adjacent Bases:      %s\n'   % commify(totalGapAdjacentBases)                         )
    oh.write( 'Masked Bases:            %s\n'   % commify(lowerCaseMaskedBases)                          )
    oh.write( 'Masked Adjacent Bases:   %s\n'   % commify(totalMaskAdjacentBases)                        )
    oh.write( 'UC Depth Filtered Bases: %s\n'   % commify(UC_depth_fail_bases)                           )
    oh.write( 'Maximum Callable Bases:  %s\n'   % commify(totalGapAdjacentBases + \
                                                          totalMaskAdjacentBases + 
                                                          UC_depth_pass_bases + \
                                                          UC_depth_fail_bases)                           )
    oh.write( 'UC Depth Pass Bases:     %s\n'   % commify(UC_depth_pass_bases)                           )
    oh.write( '----\n' )
    oh.write( 'Het. SNPs:               %s\n'   % commify(snp_indel_counts['SNP']['Heterozygous'])       )
    oh.write( 'Hom. SNPs:               %s\n'   % commify(snp_indel_counts['SNP']['Homozygous'])         )
    oh.write( 'Hom. SNPs (1/2):         %s\n'   % commify(snp_indel_counts['SNP']['Homozygous_1_2'])     )
    oh.write( '----\n' )
    oh.write( 'Het. INDELs:             %s\n'   % commify(snp_indel_counts['INDEL']['Heterozygous'])     )
    oh.write( 'Hom. INDELs:             %s\n'   % commify(snp_indel_counts['INDEL']['Homozygous'])       )
    oh.write( 'Hom. INDELs (1/2):       %s\n'   % commify(snp_indel_counts['INDEL']['Homozygous_1_2']) )
    oh.write( '----\n' )
    oh.write( 'All rates are per Kb:\n\n' )
    oh.write( 'Het SNP Rate:                %.4f\n'%allBases_hetSNPrate )
    oh.write( 'Hom SNP Rate:                %.4f\n'%allBases_homSNPrate )
    oh.write( 'Hom SNP Rate (1/2):          %.4f\n'%allBases_1_2_SNPrate )
    oh.write( 'INDEL Rate:                  %.4f\n'%allBases_INDELrate )
    oh.write( 'Callable INDEL Rate:         %.4f\n'%callable_INDELrate )
    oh.write( 'Callable Het SNP Rate:       %.4f\n'%callable_hetSNPrate )
    oh.write( 'Callable Hom SNP Rate:       %.4f\n'%callable_homSNPrate )
    oh.write( 'Callable Hom SNP Rate (1/2): %.4f\n'%callable_1_2_SNPrate )
    oh.close()
