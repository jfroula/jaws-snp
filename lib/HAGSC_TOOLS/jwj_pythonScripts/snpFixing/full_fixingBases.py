__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  10/17/13"

from hagsc_lib import  iterFASTA, commify
from hagsc_lib import FASTAFile_dict, iterCounter

from hagsc_lib import SeqRecord, QUALRecord, writeFASTA, writeQUAL

from sys import stderr

import subprocess

import re

seqParser = re.compile( r'([ACTG]+)\(([ACTG]+)/([ACTG]+)\)([ACTG]+)' ).finditer

#==============================================================
def real_main():

    LIB_ID = 'R40'
    
    # Indexing assem and qual
    indexAssemblyFASTA = {}
    scaffOrder         = []
    for r in iterFASTA(open('/global/projectb/scratch/jjenkins/Ceratodon_R40_polish/Assembly/Ceratodon_purpureus_var_R40.mainGenome.fasta')):
        scaffOrder.append( r.id )
        indexAssemblyFASTA[r.id] = r
    #####
    
    # Opening the output files
    oh_f     = open( '/global/projectb/scratch/jjenkins/Ceratodon_R40_polish/Assembly/Ceratodon_purpureus_var_R40.mainGenome.fixed.full.fasta', 'w' )
    
    # snp and indel files
    snpFile   = '%s.SNP.Homozygous.context.dat'%LIB_ID
    p_snp   = subprocess.Popen( 'cat %s'%snpFile, shell=True, stdout=subprocess.PIPE)

    indelFile = '%s.INDEL.Homozygous.context.dat'%LIB_ID
    p_indel = subprocess.Popen( 'cat %s'%indelFile, shell=True, stdout=subprocess.PIPE)

    # Log file
    oh_log = open( 'snp_and_indel_fixing_log.dat', 'w' )

    # Reading in the snps from the snp calling file
    stderr.write( 'Reading in the snps\n' )
    oh_log.write( "-Reading in snps\n" )
    n         = 1
    snp_indel_Dict = {}
    for line in p_snp.stdout:
        snpID = "notSuper_%d"%n
        n+=1
        try:
            scaffID, pos, SNP, dummy = line.split(None)
        except ValueError:
            scaffID, pos, SNP, d1, d2 = line.split(None)
        #####
        try:
            leadSeq, ref, alt, tailSeq = seqParser(SNP).next().groups()
        except StopIteration:
            continue
        #####
        refSeq = ''.join([leadSeq,ref,tailSeq])
        altSeq = ''.join([leadSeq,alt,tailSeq])
        pos = int(pos)
        try:
            snp_indel_Dict[scaffID].append( (pos, snpID, refSeq, altSeq, ref, alt, 'snp') )
        except KeyError:
            snp_indel_Dict[scaffID] =     [ (pos, snpID, refSeq, altSeq, ref, alt, 'snp') ]
        #####
    #####
    p_snp.poll()

    # Reading in the indels from the snp calling file
    stderr.write( 'Reading in the indels\n' )
    oh_log.write( "-Reading in indels\n" )
    n         = 1
    for line in p_indel.stdout:
        indelID = "notSuper_%d"%n
        n += 1
        try:
            scaffID, pos, SNP, dummy   = line.split(None)
        except ValueError:
            scaffID, pos, SNP, d1, d2   = line.split(None)
        #####
        try:
            leadSeq, ref, alt, tailSeq = seqParser(SNP).next().groups()
        except StopIteration:
            continue
        #####
        refSeq = ''.join([leadSeq,ref,tailSeq])
        altSeq = ''.join([leadSeq,alt,tailSeq])
        pos    = int(pos)
        try:
            snp_indel_Dict[scaffID].append( (pos, indelID, refSeq, altSeq, ref, alt, 'indel') )
        except KeyError:
            snp_indel_Dict[scaffID] =     [ (pos, indelID, refSeq, altSeq, ref, alt, 'indel') ]
        #####
    #####
    p_indel.poll()
    
    # Reading in the hetAnalysis
    stderr.write( 'Reading in the full het fixing file\n' )
    nn            = 0
    for line in open( '%s_full_fixing.dat'%LIB_ID ):
        chrID, indelID, leftKmer, rightKmer, leftStart, rightStart, call = line.split(None)
        if ( call == 'INSUFFICIENT_SUPPORT' ): continue
        if ( call == 'AMBIGUOUS_REGION' ):     continue
        try:
            leadSeq, ref, alt, tailSeq = seqParser(call.replace('\\','/')).next().groups()
        except StopIteration:
            continue
        #####
        refSeq = ''.join([leadSeq,ref,tailSeq])
        altSeq = ''.join([leadSeq,alt,tailSeq])
        pos    = int(leftStart) + 1
        try:
            snp_indel_Dict[chrID].append( (pos, indelID, refSeq, altSeq, ref, alt, 'full') )
        except KeyError:
            snp_indel_Dict[chrID] =     [ (pos, indelID, refSeq, altSeq, ref, alt, 'full') ]
        #####
        nn += 1
    #####
    
    # Writing out the log file
    stderr.write( 'NUMBER OF INDELS TO CONSTRAIN %s\n'%commify(nn) )
    oh_log.write( 'NUMBER OF INDELS TO CONSTRAIN %s\n'%commify(nn) )
    
    # Removing SNPs and indels that are too close to one another
    stderr.write( 'Removing snps and indels that are too close to one another\n' )
    for scaffID in scaffOrder:
        
        # Pulling the list of snps
        try:
            tmpList = snp_indel_Dict[scaffID]
        except KeyError:
            writeFASTA( [indexAssemblyFASTA[scaffID]], oh_f )
            continue
        #####
    
        # Testing for overlap of snps and indels
        stderr.write( 'TESTING SNP AND INDEL OVERLAP %s\n'%scaffID )
        tmpList.sort()
        removalSet  = set()
        removeItems = False
        tmpSeq      = str(indexAssemblyFASTA[scaffID].seq).upper()
        for n in xrange( len(tmpList) - 1 ):
            # Reading in a pair
            pos_n, indelID_n, refSeq_n, altSeq_n, ref_n, alt_n, type_n = tmpList[n]
            for m in xrange( (n+1), len(tmpList) ):
                # Reading in a pair
                pos_m, indelID_m, refSeq_m, altSeq_m, ref_m, alt_m, type_m = tmpList[m]
                # If there is an overlap, then act
                if ( ((pos_n+len(alt_n)) >= pos_m) or ((pos_n+len(ref_n)) >= pos_m) ):
                    if ( type_n == 'full' ):
                        removeItems = True
                        removalSet.add(m)
                    elif ( type_m == 'full' ):
                        removeItems = True
                        removalSet.add(n)
                    elif ( (type_n == 'indel') and (type_m == 'snp') ):
                        removeItems = True
                        removalSet.add(m)
                    elif ( (type_m == 'indel') and (type_n == 'snp') ):
                        removeItems = True
                        removalSet.add(n)
                    else:
                        removeItems = True
                        removalSet.add(n)
                    #####
                #####
                # Breaking after we are done
                if ( ( pos_m > (pos_n+len(alt_n)) ) and (pos_m > (pos_n+len(ref_n)) ) ): break
            #####
        #####
        stderr.write( 'done\n')
        
        # Removing items that are too close together
        if ( removeItems ):
            for tmpIndex in sorted(removalSet,reverse=True):
                pos, snpID, refSeq, altSeq, ref, alt, itemType = tmpList[tmpIndex]
                oh_log.write( '--------------------\n' )
                oh_log.write( 'REMOVED\t%s\t%s\n'%(itemType,scaffID) )
                oh_log.write( '%s\t%s\n'%(scaffID,refSeq) )
                oh_log.write( '%s\t%s\n'%(scaffID,altSeq) )
                oh_log.write( '%s\t%s\t%s\n'%(itemType,ref,alt) )
                oh_log.write( 'Position of the %s:  %s\n'%(itemType,commify(pos)) )
                oh_log.write( 'Reference Bases:  %s\n'%tmpSeq[pos-len(ref):pos] )
                tmpList.pop(tmpIndex)
            #####
        #####
        
        # Making the fixes on the remaining items
        tmpList.sort( reverse=True )
        tmpSeqList = []
        zz = iterCounter(10000)
        for pos, snpID, refSeq, altSeq, ref, alt, itemType in tmpList:
            zz()
            if ( itemType == 'snp' ):
                # Writing to the log file
                oh_log.write( '--------------------\n' )
                oh_log.write( 'MODIFIED\t%s\t%s\n'%(itemType,scaffID) )
                oh_log.write( '%s\t%s\n'%(scaffID,refSeq) )
                oh_log.write( '%s\t%s\n'%(scaffID,altSeq) )
                oh_log.write( '%s\t%s\t%s\n'%(itemType,ref,alt) )
                oh_log.write( 'Position of the %s:  %s\n'%(itemType,commify(pos)) )
                oh_log.write( 'Prior to Change Bases:  %s\n'%tmpSeq[(pos-50):(pos+50)] )
                # Making a check on the reference sequence
                if ( tmpSeq[(pos - 1):(pos + len(ref) - 1)] != ref ):
                    print "WE HAVE A PROBLEM"
                    print pos, snpID
                    print refSeq
                    print altSeq
                    print ref, alt, itemType
                    print tmpSeq[(pos - 50):(pos + 50)]
                    assert False
                #####
                # Modifying the sequence
                frontSeq = tmpSeq[:pos-1]
                rearSeq  = tmpSeq[pos:]
#                 tmpSeq   = ''.join( [frontSeq, alt, rearSeq] )

                tmpSeqList.insert(0,rearSeq)
                tmpSeqList.insert(0,alt)
                tmpSeq = frontSeq
                
                oh_log.write( 'Post Change Bases:      %s\n'%tmpSeq[(pos-50):(pos+50)] )
            elif ( itemType == 'indel' ):
                # Writing to the log file
                oh_log.write( '--------------------\n' )
                oh_log.write( 'MODIFIED\t%s\t%s\n'%(itemType,scaffID) )
                oh_log.write( '%s\t%s\n'%(scaffID,refSeq) )
                oh_log.write( '%s\t%s\n'%(scaffID,altSeq) )
                oh_log.write( '%s\t%s\t%s\n'%(itemType,ref,alt) )
                oh_log.write( 'Position of the %s:  %s\n'%(itemType,commify(pos)) )
                oh_log.write( 'Prior to Change Bases:  %s\n'%tmpSeq[(pos-50):(pos+50)] )
                # Making a check on the reference sequence
                if ( tmpSeq[(pos - 1):(pos + len(ref) - 1)] != ref ):
                    print "WE HAVE A PROBLEM"
                    print pos, snpID
                    print refSeq
                    print altSeq
                    print ref, alt, itemType
                    print tmpSeq[(pos - 50):(pos + 50)]
                    print tmpSeq[(pos - 1):(pos + len(ref) - 1)]
                    print ref
                    assert False
                #####
                # Modifying the sequence
                frontSeq = tmpSeq[:pos-1]
                rearSeq  = tmpSeq[pos + (len(ref) - 1):]
#                 tmpSeq   = ''.join( [frontSeq, alt, rearSeq] )
                
                tmpSeqList.insert(0,rearSeq)
                tmpSeqList.insert(0,alt)
                tmpSeq = frontSeq
                
                oh_log.write( 'Post Change Bases:      %s\n'%tmpSeq[(pos-50):(pos+50)] )
            elif ( itemType == 'full' ):
                # Writing to the log file
                oh_log.write( '--------------------\n' )
                oh_log.write( 'MODIFIED\t%s\t%s\n'%(itemType,scaffID) )
                oh_log.write( '%s\t%s\n'%(scaffID,refSeq) )
                oh_log.write( '%s\t%s\n'%(scaffID,altSeq) )
                oh_log.write( '%s\t%s\t%s\n'%(itemType,ref,alt) )
                oh_log.write( 'Position of the %s:  %s\n'%(itemType,commify(pos)) )
                oh_log.write( 'Prior to Change Bases:  %s\n'%tmpSeq[(pos-50):(pos+50)] )
                # Making a check on the reference sequence
                if ( tmpSeq[(pos - 1):(pos + len(ref) - 1)] != ref ):
                    print "WE HAVE A PROBLEM"
                    print pos, snpID
                    print refSeq
                    print altSeq
                    print ref, alt, itemType
                    print tmpSeq[(pos - 50):(pos + 50)]
                    print tmpSeq[pos:(pos + len(ref))]
                    print ref
                    assert False
                #####
                
                # Modifying the sequence
                frontSeq = tmpSeq[:pos-1]
                rearSeq  = tmpSeq[pos + (len(ref) - 1):]
#                tmpSeq   = ''.join( [frontSeq, alt, rearSeq] )

                tmpSeqList.insert(0,rearSeq)
                tmpSeqList.insert(0,alt)
                tmpSeq = frontSeq

                oh_log.write( 'Post Change Bases:      %s\n'%tmpSeq[(pos-50):(pos+50)] )
            #####
        #####
        
        # Adding the front piece back in and joining sequence
        tmpSeqList.insert(0,tmpSeq)
        
        # Writing the fixed SEQ/QUAL to a file
        print "writing %s to a file" % scaffID
        fr = SeqRecord(  id=scaffID, seq=''.join( tmpSeqList ),   description='' )
        writeFASTA( [fr], oh_f )
        
    #####
    oh_f.close()
    oh_log.write( '--------------------\n' )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
