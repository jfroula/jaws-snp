__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  3/23/16"

from hagsc_lib import iterCounter, IntervalTree, IntervalClass
from hagsc_lib import FASTAFile_dict, revComp

from sys import stderr, stdout, argv

import subprocess

import gzip

import re

seqParser = re.compile( r'([ACTG]+)\(([ACTG]+)/([ACTG]+)\)([ACTG]+)' ).finditer

#============================================================================================
def score_match(subject, query, subject_start, query_start, length):
    score = 0
    # for each base in the match
    for i in range(0,length):
        # first figure out the matching base from both sequences
        subject_base = subject[subject_start + i]
        query_base = query[query_start + i]
        # then adjust the score up or down depending on 
        # whether or not they are the same
        if subject_base == query_base:
            score = score + 1
        else:
            score = score - 1
        #####
    #####
    return score
 
#============================================================================================
def pretty_print_match(subject, query, subject_start, query_start, length):
    # first print the start/stop positions for the subject sequence
    print(str(subject_start) + (' ' * length) + str(subject_start+length))
    # then print the bit of the subject that matches
    print(' ' + subject[subject_start:subject_start+length])
    # then print the bit of the query that matches
    print(' ' + query[query_start:query_start+length])
    # finally print the start/stop positions for the query
    print(str(query_start) + (' ' * length) + str(query_start+length))
    print('n--------------------n')

#============================================================================================
def try_all_matches(subject, query, score_limit):
    DSU = []
    for subject_start in range(0,len(subject)):
        for query_start in range(0,len(query)):
            for length in range(0,len(query)):
                if (subject_start + length < len(subject) and query_start + length < len(query)):
                    score = score_match(subject, query, subject_start, query_start, length)
                    # only print a line of output if the score is better than some limie
                    if (score >= score_limit):
                        DSU.append( (score, subject_start, query_start, length) )
#                         print('Score : ' + str(score))
#                         pretty_print_match(subject, query, subject_start, query_start, length)
                    #####
                #####
            #####
        #####
    #####
    DSU.sort(reverse=True)
    return DSU[0]

#==============================================================
class indelClass(object):
    def __init__(self,indelID):
        self.indelID  = indelID
        self.readDict = {}
    
    def addOverlapRead(self,readBase,readString):
        try:
            self.readDict[readBase].append( '%s\t%s'%( readString, 'INDEL' ) )
        except KeyError:
            self.readDict[readBase] = [ '%s\t%s'%( readString, 'INDEL' ) ]
        #####

    def addHetRead(self,readBase,readString):
        try:
            self.readDict[readBase].append( '%s\t%s'%( readString, 'HET' ) )
        except KeyError:
            self.readDict[readBase] = [ '%s\t%s'%( readString, 'HET' ) ]
        #####
    
    def outputString(self,oh):
        for readBase, tmpList in self.readDict.iteritems():
            if ( len(tmpList) <= 1 ): continue
            for item in tmpList:
                stdout.write( '%s\t%s\n'%(self.indelID,item) )
                oh.write( '%s\t%s\n'%(self.indelID,item) )
            #####
        #####

#==============================================================
def pullBestLocation( readSeq, testSeq, testLoc ):
    # Regular expression
    findTestSeq = re.compile( r'(?=%s)'%testSeq ).finditer
    tmpLocList  = [item.start() for item in findTestSeq(readSeq)]
    
    # Pull the best location for the prefix
    nOcc = len(tmpLocList)
    if ( nOcc > 1 ):
        tmpStart = 0
        DSU      = []
        for n in xrange( nOcc ):
            tmpLoc = readSeq.find( testSeq, tmpStart )
            DSU.append( ( abs(tmpLoc-testLoc), tmpLoc ) )
            tmpStart = tmpLoc + 1
        #####
        DSU.sort()
        finalLoc = DSU[0][1]
    elif ( nOcc == 1 ):
        finalLoc = readSeq.find( testSeq )
    elif ( nOcc == 0 ):
        finalLoc = -1
    #####
    return finalLoc

#==============================================================
def real_main():
    
    LIB_ID     = 'PEAH'
    FASTA_file = '/global/projectb/scratch/jjenkins/Pecan_Elliot/Assembly/finalAltHapSet.fixed.full.fasta'
    stage      = int(argv[1])
    
    #=====================================================
    # STEP 1:  PULLING ALL HOMOZYGOUS INDELS THAT OVERLAP
    if ( stage == 1 ):

        # Reading in the overlapping hets
        stderr.write( 'Reading in the homozygous indels\n')
        homIndelDict = {}
        for line in open('%s.SNP.Homozygous.context.dat'%LIB_ID):
            try:
                chrID, pos, calls, d = line.split(None)
            except ValueError:
                continue
            #####
            try:
                leadSeq, ref, alt, tailSeq = seqParser(calls).next().groups()
            except StopIteration:
                continue
            #####
            refSeq = ''.join( [ leadSeq,ref,tailSeq ] )
            altSeq = ''.join( [ leadSeq,alt,tailSeq ] )
            pos    = int( pos )
            try:
                homIndelDict[chrID].append( ( pos, calls, ref, alt ) )
            except KeyError:
                homIndelDict[chrID] = [ ( pos, calls, ref, alt ) ]
            #####
        #####
    
        # Reading in the overlapping hets
        stderr.write( 'Reading in the homozygous indels\n')
        for line in open('%s.INDEL.Homozygous.context.dat'%LIB_ID):
            try:
                chrID, pos, calls, d = line.split(None)
            except ValueError:
                continue
            #####
            try:
                leadSeq, ref, alt, tailSeq = seqParser(calls).next().groups()
            except StopIteration:
                continue
            #####
            refSeq = ''.join([leadSeq,ref,tailSeq])
            altSeq = ''.join([leadSeq,alt,tailSeq])
            pos    = int(pos)
            try:
                homIndelDict[chrID].append( (pos, calls, ref, alt) )
            except KeyError:
                homIndelDict[chrID] = [ (pos, calls, ref, alt) ]
            #####
        #####
        
        # Looking for overlapping indels
        oh = open( '%s_overlapping_homozygous_indel_calls.dat'%LIB_ID, 'w' )
        testDist = 8
        for scaffID, tmpList in homIndelDict.iteritems():
            # Testing for overlap of snps and indels
            tmpList.sort()
            for n in xrange( len(tmpList) - 1 ):
                # Looking for overlapping indel calls
                pos_n, calls_n, ref_n, alt_n = tmpList[n]
                for m in xrange(  (n+1), len(tmpList) ):
                    # Looking for overlapping indel calls
                    pos_m, calls_m, ref_m, alt_m = tmpList[m]
                    # If there is an overlap, then act
                    if ( (pos_n + testDist) >= pos_m ):
                        outputString = [scaffID,'%d'%pos_n,ref_n,alt_n,'%d'%pos_m,ref_m,alt_m]
                        oh.write( '%s\n'%( '\t'.join(outputString) ) )
                        print '\t'.join(outputString)
                    #####
                    # Breaking after we have passed the threshold
                    if ( pos_m > (pos_n + testDist) ): break
                #####
            #####
        #####
      
    #####
    
    #=====================================================
    # STEP 2:  PULLING ALL READ PAIRS THAT OVERLAP AN OVERLAPPING HOMOZYGOUS INDEL
    if ( stage == 2 ):
        
        # Reading in the overlapping indel calls
        indelDict = {}
        for line in open('%s_overlapping_homozygous_indel_calls.dat'%LIB_ID):
            scaffID, pos_n, ref_n, alt_n, pos_m, ref_m, alt_m = line.split(None)
            pos_n, pos_m = sorted( map( int, [pos_n,pos_m] ) )
            try:
                indelDict[scaffID].append( IntervalClass(pos_n,pos_m,'%s_%d_%s_%s_%d_%s_%s'%(scaffID,pos_n,ref_n,alt_n,pos_m,ref_m,alt_m) ) )
            except KeyError:
                indelDict[scaffID] = [ IntervalClass(pos_n,pos_m,'%s_%d_%s_%s_%d_%s_%s'%(scaffID,pos_n,ref_n,alt_n,pos_m,ref_m,alt_m) ) ]
            #####
        #####

        # Screening the reads for indels that overlap an indel pair
        p           = subprocess.Popen( 'samtools view %s.gatk.bam | cut -f1,3,4,10'%LIB_ID, shell=True, stdout=subprocess.PIPE )
        testDict    = {}
        baseSet     = set()
        scaffSet    = set( indelDict.keys() )
        overlapDict = {}
        y           = iterCounter(1000000)
        oh          = gzip.open( '%s_readsOverlapping_HOM_INDEL.gz'%LIB_ID , 'w' )
        for line in p.stdout:
            
            # Reading in a new line
            readBase, scaffID, readStart, readSeq = line.split(None)
            readStart = int(readStart) - 1

            # Screening for scaffolds with no indels on them
            if ( scaffID not in scaffSet ): continue

            # Reads in the new tree when applicable
            try:
                testDict[scaffID]
            except KeyError:
                tmpTree = IntervalTree( indelDict[scaffID]  )
                testDict[scaffID] = None
                # Write the output to a file
                for indelID, tmpClass in overlapDict.iteritems():
                    tmpClass.outputString(oh)
                #####
                # Restarting the overlap dictionary
                del overlapDict
                overlapDict = {}
                stderr.write( 'New scaffold: %s\n'%scaffID )
            #####

            # Does the current read overlap an indel on the current scaffold
            x = tmpTree.find( readStart, readStart+len(readSeq) )
            if ( len(x) > 0 ):
                for item in x:
                    indelID = item.label
                    try:
                        overlapDict[indelID].addOverlapRead(readBase,line.strip())
                    except KeyError:
                        overlapDict[indelID] = indelClass(indelID)
                        overlapDict[indelID].addOverlapRead(readBase,line.strip())
                    #####
                #####
                # Add the readBase to the base set
                baseSet.add( readBase )

            # If there is no overlap, is the read part of a pair that does overlap an 
            # overlapping indel?
            elif ( (len(x)==0) and (readBase in baseSet) ):
            
                try:
                    overlapDict[indelID].addHetRead(readBase,line.strip())
                except KeyError:
                    overlapDict[indelID] = indelClass(indelID)
                    overlapDict[indelID].addHetRead(readBase,line.strip())
                #####
                
            #####

            y()

        #####

        p.poll()
        
        # Write the output to a file
        for indelID, tmpClass in overlapDict.iteritems(): tmpClass.outputString(oh)

        oh.close()
        
    #####

    #========================================================
    # STEP 3:  Screen the read pairs for ones that overlap a 
    # heterozygous snp where the read matches the reference.
    if ( stage == 3 ):
        
        # Indexing the assembly
        stderr.write( 'INDEXING THE ASSEMBLY\n' )
        indexFASTA = FASTAFile_dict( FASTA_file )
        
        # Read in the read sets
        stderr.write( 'READING IN THE READS\n' )
        p         = subprocess.Popen( 'zcat %s_readsOverlapping_HOM_INDEL.gz'%LIB_ID, shell=True, stdout=subprocess.PIPE )
        indelDict = {}
        y         = iterCounter(1000000)
        for line in p.stdout:
            indelID, readBase, chrID, readStart, readSeq, indelType = line.split(None)
            if ( indelType == 'HET' ): continue
            readStart = int(readStart)
            try:
                indelDict[indelID].append( (readStart,readBase,chrID,readSeq,indelType) )
            except KeyError:
                indelDict[indelID] =     [ (readStart,readBase,chrID,readSeq,indelType) ]
            #####
            y()
        #####
        p.poll()
        
        # Remapping to chromosomes
        stderr.write( 'REMAPPING TO CHROMOSOMES\n' )
        chr_to_indelID_Dict = {}
        for indelID, tmpList in indelDict.iteritems():
            for readStart, readBase, chrID, readSeq, indelType in tmpList:
                try:
                    chr_to_indelID_Dict[chrID].add( indelID )
                except KeyError:
                    chr_to_indelID_Dict[chrID] = set([indelID])
                #####
            #####
        #####
        
        oh = open( '%s_full_fixing.dat'%LIB_ID, 'w' )

        stderr.write( 'Fixing close proximity indels\n')
        for chrID, indelSet in chr_to_indelID_Dict.iteritems():

            stderr.write( '%s\n'%chrID )
            
            # Reading in the reference sequence
            tmpSeq   = str(indexFASTA[chrID].seq).upper()
            nChrSize = len(tmpSeq)
            
            # Looping over the indel ID's in the chromosome
            for indelID in indelSet:

                # Splitting the indel
                s1, r1, a1, s2, r2, a2 = indelID.split('_')[-6:]
                s1, s2                    = sorted( map( int, [s1,s2] ) )

                # Finding decent prefix and suffix sequences to bound the indels
                fixDict   = {}
                nPadBases = 8
                prefix    = tmpSeq[ (s1-nPadBases):s1 ]
                suffix    = tmpSeq[ (s2+max(len(r2),len(a2))):(s2+max(len(r2),len(a2))+nPadBases) ]
                
                # Checking the size of the selected prefix and suffix
                # Too small means that they go off the end of the scaffold
                if ( len(prefix) < 8  or len(suffix) < 8  ): continue
                
                # Make sure the prefix and suffix are not the same
                # If they are, then just increment a few bases
                # until they are unique
                while ( prefix == suffix ):
                    nPadBases += 1
                    prefix     = tmpSeq[ (s1-nPadBases):s1 ]
                    suffix     = tmpSeq[ (s2+max(len(r2),len(a2))):(s2+max(len(r2),len(a2))+nPadBases) ]
                    if ( nPadBases >= 10 ): break
                #####
                if ( nPadBases >= 10 ): continue
                
                #-----------------------------------------------------------
                # Making sure that we can find the prefix and suffix strings
                # by varying the prefix and suffix string location if necessary
                notDone = True
                while ( notDone ):
                    
                    # Resetting the loop variable
                    notDone = False
                    
                    # Looping through the reads to see if we can find the prefix/suffix locations
                    prefixSet = set()
                    suffixSet = set()
                    for readStart, readBase, chrID, readSeq, indelType in sorted(indelDict[indelID]):
                        # Computing the location on the read
                        delta1 = s1 - readStart + max( len(a1), len(r1) )
                        delta2 = s2 - readStart + max( len(a1), len(r1) ) + max( len(a2), len(r2) )
                        # Does the read cover both indels
                        if ( (delta1 >= len(readSeq)) or (delta2 >= len(readSeq)) or \
                             (delta1 <=   0) or (delta2 <=   0) ): continue
                        # Pull the best location for the prefix and suffix
                        prefixLoc = pullBestLocation( readSeq, prefix, delta1 )
                        suffixLoc = pullBestLocation( readSeq, suffix, delta2 )
                        prefixSet.add( prefixLoc )
                        suffixSet.add( suffixLoc )
                    #####
                    
                    # This is testing to see the prefix/suffix position have exceeded the chromosome size
                    # If this happens, we will go into an infinite loop
                    # This check prevents this
                    if ( (s1 > nChrSize) or (s2 > nChrSize) ):
                        # Resetting the prefix/suffix to satisfy the output requirements
                        prefix  ="XXXXXXXX"
                        suffix  ="XXXXXXXX"
                        continue
                    #####

                    # This is testing to see if the prefix and suffix are the same
                    if ( prefix == suffix ):
                        s1     = s1 - 1
                        prefix = tmpSeq[ (s1-nPadBases) : s1 ]
                        s2     = s2 + 1
                        suffix = tmpSeq[ (s2+max(len(r2),len(a2))) : (s2+max(len(r2),len(a2))+nPadBases) ]
                        notDone = True
                        continue
                    #####

                    # This is testing to see if we have no positions found for prefix
                    if ( (len(prefixSet) == 1) and (list(prefixSet)[0] == -1) ):
                        s1     = s1 - 1
                        prefix = tmpSeq[ (s1-nPadBases) : s1 ]
                        notDone = True
                    #####

                    # This is testing to see if we have no positions found for suffix
                    if ( (len(suffixSet) == 1) and (list(suffixSet)[0] == -1) ):
                        s2     = s2 + 1
                        suffix = tmpSeq[ (s2+max(len(r2),len(a2))) : (s2+max(len(r2),len(a2))+nPadBases) ]
                        notDone = True
                    #####
                    
                    # This is a test to see if we have a unique prefix and suffix
                    findPrefix = re.compile( r'(?=%s)'%prefix ).finditer
                    if ( len([item for item in findPrefix(readSeq)]) > 1 ):
                        s1     = s1 - 1
                        prefix = tmpSeq[ (s1-nPadBases) : s1 ]
                        notDone = True
                    #####

                    findSuffix = re.compile( r'(?=%s)'%suffix ).finditer
                    if ( len([item for item in findSuffix(readSeq)]) > 1 ):
                        s2     = s2 + 1
                        suffix = tmpSeq[ (s2+max(len(r2),len(a2))) : (s2+max(len(r2),len(a2))+nPadBases) ]
                        notDone = True
                    #####

                #####
                
                #-----------------------------------------------------------
                # All kmer fixing
                for readStart, readBase, chrID, readSeq, indelType in sorted(indelDict[indelID]):
                    
                    # Does the read cover both indels?
                    delta1 = s1 - readStart + max( len(a1), len(r1) )
                    delta2 = s2 - readStart + max( len(a1), len(r1) ) + max( len(a2), len(r2) )
                    if ( (delta1 >= 150) or (delta2 >= 150) or \
                         (delta1 <=   0) or (delta2 <=   0) ): continue
                    
                    # Pull the best location for the prefix and suffix
                    prefixLoc = pullBestLocation( readSeq, prefix, delta1 )
                    suffixLoc = pullBestLocation( readSeq, suffix, delta2 )
                    
                    # No good positions were found for this read
                    if ( (prefixLoc < 0) or (suffixLoc < 0) ):
                        continue
                    # Locations were the same
                    elif ( prefixLoc == suffixLoc ):
                        print "==============================="
                        print 'prefix and suffix locations are same'
                        print indelID
                        print tmpSeq[s1-50:s2+50]
                        print prefixLoc, prefix
                        print suffixLoc, suffix
                        print readSeq
                        assert False
                    #####
                    
                    # Reading in the fixing kmer
                    fixKmer = readSeq[prefixLoc+len(prefix):suffixLoc]
                    
                    if ( fixKmer == '' ): continue
                    
                    try:
                        fixDict[fixKmer] += 1
                    except KeyError:
                        fixDict[fixKmer] = 1
                    #####
                    
                #####
                
                # Providing the solution
                DSU =  [(v,k) for k,v in fixDict.iteritems()]
                DSU.sort(reverse=True)
                
                # Not enough reads to support a fix
                if ( ( len(DSU) == 0 ) and ( len(indelDict[indelID]) <= 3 ) ):
                    oh.write( '%s\t%s\t%s\t%s\t%d\t%d\t%s\n'%(chrID, indelID, prefix, suffix, s1, s2, 'INSUFFICIENT_SUPPORT') )
                    print chrID, indelID, prefix, suffix, s1, s2, 'INSUFFICIENT_SUPPORT'
                elif ( ( len(DSU) == 0 ) and ( len(indelDict[indelID]) > 3 ) ):
                    oh.write( '%s\t%s\t%s\t%s\t%d\t%d\t%s\n'%(chrID, indelID, prefix, suffix, s1, s2, 'AMBIGUOUS_REGION') )
                    print chrID, indelID, prefix, suffix, s1, s2, 'AMBIGUOUS_REGION'
                else:
                    try:
                        # Generating the fixing string
                        left_ref_pos  = s1
                        leftSeq       = tmpSeq[ (left_ref_pos-100):left_ref_pos ]
                        right_ref_pos = s2 + max(len(r2),len(a2)) + nPadBases - len(suffix)
                        rightSeq      = tmpSeq[ right_ref_pos:(right_ref_pos+100)]
                        refSeq        = tmpSeq[left_ref_pos:right_ref_pos]
                        oh.write( '%s\t%s\t%s\t%s\t%d\t%d\t%s\n'%(chrID, indelID, prefix, suffix, left_ref_pos, right_ref_pos, "%s(%s\%s)%s"%(leftSeq,refSeq,DSU[0][1],rightSeq) ) )
                        print chrID, indelID, prefix, suffix, left_ref_pos, right_ref_pos, "%s(%s\%s)%s"%(leftSeq,refSeq,DSU[0][1],rightSeq)
                    except IndexError:
                        print "=========================="
                        print len(indelDict[indelID])
                        print DSU
                        for item in outputString: print item
                        assert False
                    #####
                #####

            ##### IndelID loop

        ##### ChrID loop
        
        oh.close()

    #####
    
    # Testing the idea for an aligner
    if ( False ):
        one_sequence     = 'actgatcgattgatcgatcgatcg'
        another_sequence =        'tttagatcgatctttgatc'
        try_all_matches(one_sequence, another_sequence, 7)
    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
