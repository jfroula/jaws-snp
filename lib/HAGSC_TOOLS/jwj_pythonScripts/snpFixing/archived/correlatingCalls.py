__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  10/23/17"

#==============================================================
def real_main():
    
    for line in open('AGHCH.GRP_3.GATK.INDEL.preFilter.vcf'):
        if ( line[0] in "#" ): continue
        s = line.split(None)
        if ( s[6] != "PASS" ): continue
        tmpDict = dict( item.split('=') for item in s[7].split(';') if (len(item.split('=')) == 2) )
        print tmpDict['DP'], tmpDict['QD'], s[9].split(":")[3]
    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
