__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  10/29/15"

from gp_lib import jobFinished

from sys import argv

#==============================================================
def real_main():

    # Reading in the arguments from the main program    
    jobID_file = argv[1]

    # Just waiting on the jobs to finish
    jobID_set = set([line.strip() for line in open(jobID_file)])
    jobFinished( jobID_set )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
