#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  8/6/15"

from snp_lib import vcfContextGenerator

from sys import argv

#==============================================================
def real_main():
    
    # Reading in the variables
    SNP_softMaskFilter_vcf = argv[1]
    genomeFASTA            = argv[2]
    SNP_het_contextFile    = argv[3]
    SNP_hom_contextFile    = argv[4]
    cmd_log                = argv[5]

    vcfContextGenerator( SNP_softMaskFilter_vcf, genomeFASTA, SNP_het_contextFile, SNP_hom_contextFile, cmd_log )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
