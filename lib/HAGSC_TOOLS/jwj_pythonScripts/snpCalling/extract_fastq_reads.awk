BEGIN {
    while ( (getline < "scaffold_10_hits.dat") > 0 ){
        baseNameDict[$0] = $0
    }
}

{
    # Reading the readID
    if(NR%4==1){readID=$0}
    # Reading the sequence
    if(NR%4==2){seq=$0}
    # Reading the quality scores
    if(NR%4==0){
        # Storing the quality score
        qual  = $0
        # Generating the FASTQ entry
        Entry = readID"\n"seq"\n+\n"qual
        # Printing the entry
        baseName = substr( readID, 2, length(readID)-4 )
        if ( baseName in baseNameDict ){ print Entry }
    }
}
