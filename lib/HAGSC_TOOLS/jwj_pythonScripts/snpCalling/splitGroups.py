#!/usr/bin/env python

import sys
import shlex, subprocess

def main():
	_, groupfile, output_pattern = sys.argv

	groupdict = {}
	with open(groupfile) as h:
		for line in h:
			split = line.split()
			if len(split) == 2:
				scaff = split[0]
				try:
					group = int(split[1])
				except:
					group = 0
				groupdict[scaff] = group
	#groupdict[None] = 0

	headers = []
	#out_handles = {i:open(output_pattern%i, 'w') for i in set(groupdict.values())}
	out_procs = {i:subprocess.Popen(shlex.split("samtools view -Sb -o %s -"%(output_pattern%i)), stdin=subprocess.PIPE) for i in set(groupdict.values()) }
	out_handles = {k:v.stdin for k,v in out_procs.items()}
	has_headers = set()

	for line in sys.stdin:
		if line.startswith('@'):
			headers.append(line)
		else:
			split = line.split()
			if len(split) < 3:
				raise ValueError(split)
			else:
				scaff = split[2]
				group = groupdict[scaff]

			out_handle = out_handles[group]
			if group not in has_headers:
				for x in headers:
					out_handle.write(x)
				has_headers.add(group)
			out_handle.write(line)

	for x in out_procs.values():
		x.communicate()

	return 0


if __name__ == "__main__":
	sys.exit(main())
