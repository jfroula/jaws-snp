#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  8/6/15"

from snp_lib import snpHistograms, SNP_GC_histogram

from sys import argv

#==============================================================
def real_main():
    
    # Reading in the variables
    SNP_softMaskFilter_vcf = argv[1]
    RUN_ID                 = argv[2] 
    basePath               = argv[3]
    cmd_log                = argv[4]
    genomeFASTA            = argv[5]
    GC_Depth_binSize       = int(argv[6])

    # Making it happen
    snpHistograms( SNP_softMaskFilter_vcf, RUN_ID, basePath, cmd_log )
    SNP_GC_histogram( SNP_softMaskFilter_vcf, genomeFASTA, RUN_ID, basePath, GC_Depth_binSize, cmd_log )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
