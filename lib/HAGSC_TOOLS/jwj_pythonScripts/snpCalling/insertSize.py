#!/usr/bin/env python 

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/23/15"

from snp_lib import InsertSize

from sys import argv

#==============================================================
def real_main():
    
    # Read inputs
    gatkBam           = argv[1]
    basePath          = argv[2]
    RUN_ID            = argv[3]
    cmdLog            = argv[4]
    inserts_to_sample = int( argv[5] )
    
    # Run insert size calculator
    InsertSize( gatkBam, basePath, RUN_ID, cmdLog, inserts_to_sample )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
