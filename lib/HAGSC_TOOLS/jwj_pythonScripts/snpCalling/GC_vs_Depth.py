#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/23/15"

from snp_lib import GC_vs_Depth

from sys import argv
import os,sys

#==============================================================
def real_main():
    
    # Read inputs
    cleanedBam  = argv[1]
    genomeFASTA = argv[2]
    basePath    = argv[3]
    RUN_ID      = argv[4]
    binSize     = int(argv[5])
    cmdLogFile  = argv[6]

    if (not os.path.exists(cleanedBam)):
        print("cleanedBam file doesn't exist: {}".format(cleanedBam))
        sys.exit(1)

    if (not os.path.exists(genomeFASTA)):
        print("genomeFASTA file doesn't exist: {}".format(genomeFASTA))
        sys.exit(1)
    
    # Run insert size calculator
    GC_vs_Depth( cleanedBam, genomeFASTA, basePath, RUN_ID, binSize, cmdLogFile )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
