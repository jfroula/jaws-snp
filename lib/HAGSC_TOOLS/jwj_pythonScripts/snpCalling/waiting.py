__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  11/20/15"

from gp_lib import jobFinished_RUN_ID

from sys import argv, stdout, stderr

import subprocess

import time

#==============================================================
def real_main():
    
    RUN_ID = argv[1]
    jobFinished_RUN_ID( RUN_ID )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
