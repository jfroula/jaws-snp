#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/23/15"

from snp_lib import writeSummaryFiles

from sys import argv

#==============================================================
def real_main():
    
    # Read inputs
    GATKbam        = argv[1]
    genomeFASTA    = argv[2]
    maskFilter     = int( argv[3] )
    gapFilter      = int( argv[4] )
    lowFilter      = float( argv[5] )
    highFilter     = float( argv[6] )
    RUN_ID         = argv[7]
    basePath       = argv[8]
    
    # Run insert size calculator
    writeSummaryFiles( GATKbam, genomeFASTA, maskFilter, gapFilter, lowFilter, highFilter, RUN_ID, basePath )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
