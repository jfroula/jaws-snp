__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  8/5/15"

from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify

from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files

# from snp_lib import align_and_split_aln, align_and_split_mem
from snp_lib import gatkQC, parseConfigFile, pullAverageDepth
from snp_lib import writeAndEcho, call_SNPs, call_INDELs, writeSummaryFiles
from snp_lib import check_PHRED_encoding, runPath, createGroupDictionary

from sys import argv, stderr

from os.path import join

from os import chdir
import sys
#==============================================================
def real_main():
    
    # Number of insert sizes to sample
    inserts_to_sample = 100000
        
    # Size of the sequence bin
    GC_Depth_binSize = 5000
    
    # Read the basic information
    basePath    = argv[1]
    configFile  = argv[2]
    snpStep     = argv[3]
    
    # Changing to the base directory
    chdir( basePath )
    
    # Read the config file
    configDict, cmd_log = parseConfigFile( configFile, basePath )
    
    # Important files
    GATKbam             = join( basePath, '%s.gatk.bam'%configDict['RUNID'] )
    
    # Post filter files
    SNP_postFilter   = join( basePath, '%s.GATK.SNP.postFilter.vcf'%configDict['RUNID'] )
    INDEL_postFilter = join( basePath, '%s.GATK.INDEL.postFilter.vcf'%configDict['RUNID'] )
    
    # Setting up the soft masking files
    SNP_softMaskFilter_vcf   = join( basePath, '%s.GATK.SNP.softMaskFilter.vcf'%configDict['RUNID'] )
    INDEL_softMaskFilter_vcf = join( basePath, '%s.GATK.INDEL.softMaskFilter.vcf'%configDict['RUNID'] )
    
    # INDEL Context output files
    INDEL_het_contextFile = join( basePath, '%s.INDEL.Heterozygous.context.dat'%configDict['RUNID'] )
    INDEL_hom_contextFile = join( basePath, '%s.INDEL.Homozygous.context.dat'%configDict['RUNID'] )
    
    # SNP Context output files
    SNP_het_contextFile = join( basePath, '%s.SNP.Heterozygous.context.dat'%configDict['RUNID'] )
    SNP_hom_contextFile = join( basePath, '%s.SNP.Homozygous.context.dat'%configDict['RUNID'] )
    
    # Setting up the primary scaffolds file
    primaryScaffsFile = join( basePath, 'primaryScaffolds.dat' )
    
    # Run the commands
    if ( snpStep == 'align_and_split' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Order the scaffolds by size
        stderr.write( '- Computing the total number of bases in the genome\n' )
        DSU = [(len(r.seq),r.id) for r in iterFASTA(open(configDict['REFERENCE']))]
        DSU.sort(reverse=True)

        # Computing the target file size to balance the load
        targetNumBases = int( float(sum([item[0] for item in DSU])) / float(configDict['NUM_JOBS']) )

        # Group scaffolds starting with the largest
        oh           = open( primaryScaffsFile, 'w' )
        sizeCounter  = 0
        groupCounter = 1
        for scaffSize, scaffID in DSU:
            # Writing the scaffold to the current file
            oh.write( '%s\t%d\n'%(scaffID,groupCounter) )
            # Incrementing the size
            sizeCounter += scaffSize
            if ( sizeCounter > targetNumBases ):
                stderr.write( '\t\tGroup: %d, Size: %s, Target: %s\n'%( \
                              groupCounter, commify(sizeCounter), commify(targetNumBases)) )
                sizeCounter = 0
                groupCounter += 1
            #####
        #####
        stderr.write( '\t\tGroup: %d, Size: %s, Target: %s\n'%( groupCounter, commify(sizeCounter), commify(targetNumBases)) )
        oh.close()
        
        # Building the awk command
        nextStep = 'merge_bam_files'
        varString = ' '.join( ['-v nextStep=\"%s\"'            % nextStep, \
                               '-v configFile=\"%s\"'          % configFile, \
                               '-v ALIGNER=\"%s\"'             % configDict['ALIGNER'], \
                               '-v RUN_ID=\"%s\"'              % configDict['RUNID'], \
                               '-v pairsPerFile=\"%d\"'        % configDict['NUM_PAIRS'], \
                               '-v basePath=\"%s\"'            % basePath, \
                               '-v runPath=\"%s\"'             % runPath, \
                               '-v INDEX=\"%s\"'               % configDict['ASSEMINDEX'], \
                               '-v PRIMARY_SCAFFS_FILE=\"%s\"' % primaryScaffsFile, \
                               '-v ALIGN_MEMORY=\"%s\"'        % configDict['ALIGN_MEMORY'], \
                               '-v TOTAL_BASES=\"%s\"'         % configDict['TOTAL_BASES'], \
                               '-v EMAIL_ADDRESS=\"%s\"'       % configDict['EMAIL']] )
        splitCmd = join( runPath, 'FASTQ_split.awk' )
        awkCmd  = 'awk %s -f %s'%( varString, splitCmd )
        if ( configDict['COMBINED_FASTQ'] ):
            cmdList = [ 'bzcat -k -c %s | %s'%( configDict['READS'][0], awkCmd ) ]
        else:
            if ( isGzipFile(configDict['READS'][0]) ):
                cmdList = [ 'gunzip -c %s | %s'%( configDict['READS'][0], awkCmd ) ]
                print("GZIP CMD: {}".format(cmdList))
            elif ( isBzipFile(configDict['READS'][0]) ):
                cmdList = [ 'bunzip2 -c %s | %s'%( configDict['READS'][0], awkCmd ) ]
                print("BZIP CMD: {}".format(cmdList))
            else:
                cmdList = [ 'cat %s | %s'%( configDict['READS'][0], awkCmd ) ]
            #####
        #####
        
        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Aligning reads to create the split bam files:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )
        
        # Running the job
        maxTime   = '20:00:00'
        maxMemory = '10G'
        sh_File   = join( basePath, '%s_align_and_split.sh'%configDict['RUNID'] )
        
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    elif ( snpStep == 'merge_bam_files' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Setting the timing variables
        maxTime   = '12:00:00'
        maxMemory = '30G'
        nextStep  = 'gatkQC'
        
        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )
        
        # Opening the jobID output file
        jobID_file = join( basePath, '%s.merge_bam.jobIDs.dat'%(configDict['RUNID']) )
        jobID_oh   = open( jobID_file, 'w' )

        # Merging the files together
        import os
        nReps = len( [ line.strip() for line in open( join( basePath, 'full_fastq_file_list.dat') ) ] )
        suppressOutput = False
        for groupID in groupDict.iterkeys():
            bamFileList = []
            shFileList  = []
            for n in xrange( 1, (nReps+1) ):
                bamFileList.append( join( basePath, '%s.%s.%d.sorted.bam' % ( configDict['RUNID'], groupID, n ) ) )
                shFileList.append(  join( basePath, '%s.%d.*.sorting.sh'  % ( configDict['RUNID'], n ) ) )
            #####
            joinedBamList  = ' '.join( bamFileList )
            joined_sh_list = ' '.join( shFileList )

            if len(bamFileList) > 1:
                cmdList        = [ 'samtools merge -h %s %s.%s.sorted.bam %s'%( bamFileList[0], configDict['RUNID'], groupID, joinedBamList ) ]
            else:
                sourcefile = bamFileList[0]
                destfile = "%s.%s.sorted.bam"%(configDict['RUNID'], groupID)
                cmdList = [ "mv %s %s"%(sourcefile, destfile) ]


            sh_File = join( basePath, '%s.%s.merging.sh'%( configDict['RUNID'], groupID ) )

            # Creating the sh files
            create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'] )

            # Submitting the job
            job_ID = submitJob( sh_File )
            jobID_oh.write( '%s\n'%job_ID )

        #####
        jobID_oh.close()

        # Launching the waiting job
        if ( not configDict['MERGE_ONLY'] ):
            cmdList = [ 'python %s/snpCalling/waitingForJobs.py %s'%( runPath, jobID_file ), \
                        'python %s/snpCalling/snpJobLauncher.py %s %s %s'%(runPath, basePath, configFile, nextStep ) ]
        #####
        sh_File = join( basePath, '%s.merge.waiting.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    elif ( snpStep == 'gatkQC' ):

        # Changing to the base directory
        chdir( basePath )
        
        # Setting the timing variables
        maxTime   = '12:00:00'
        maxMemory = '50G'
        
        if ( configDict['CALLER'] == 'VARSCAN' ):
            nextStep = 'insertSizeDist'
        else:
            nextStep  = 'merge_QC_bam_files'
        #####

        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )
        
        # Opening the jobID output file
        jobID_file = join( basePath, '%s.gatkQC.jobIDs.dat'%(configDict['RUNID']) )
        jobID_oh   = open( jobID_file, 'w' )
        
        # Launching the bam sorting jobs
        suppressOutput = False
        for groupID in groupDict.iterkeys():
            # bamID
            bamID              = '%s.%s'%( configDict['RUNID'], groupID )
            # Input bam file
            sortedBamFile      = join( basePath, '%s.sorted.bam'%( bamID ) )
            # Output bam file
            GATKbam_outputFile = join( basePath, '%s.gatk.bam'%( bamID ) )
            # Setting the commands
            cmdList = gatkQC( basePath, GATKbam_outputFile, sortedBamFile, bamID, \
                              configDict['RUNID'], configDict['REFERENCE'], configDict['READ_REGEX'], cmd_log )
            # The last job submitted needs to have the waiter and the launcher
            # for the next round of analysis
            if ( groupID == finalGroup_ID ):
                cmdList.append( 'python %s/snpCalling/waitingForJobs.py %s'%( runPath, jobID_file ) )
                cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%(runPath, basePath, configFile, nextStep) )
            #####
            # Making the .sh submission file
            sh_File         = join( basePath, '%s.gatkQC.sh'%( bamID ) )
            # Creating the sh files
            create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'] )

            # Submitting the job
            job_ID = submitJob( sh_File )

            # Capture the job id for all but the last job
            if ( groupID != finalGroup_ID ):
                jobID_oh.write( '%s\n'%job_ID )
            #####

        #####
        jobID_oh.close()
        
    elif ( snpStep == 'merge_QC_bam_files' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )
        groupList = [item for item in groupDict.keys()]

        # Final bam name
        firstBamFile  = join( basePath, '%s.%s.gatk.bam'%(configDict['RUNID'],groupList[0]) )
        bamFileList = [ 'INPUT=%s'%join(basePath,'%s.%s.gatk.bam'%(configDict['RUNID'],groupID)) for groupID in groupList]
        
        # Setting the commands
        # In this case GATKbam is the main output file that moves on to the next stage
#        cmdList  = ['source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV', \
        cmdList  = ['picard MergeSamFiles %s OUTPUT=%s SORT_ORDER=coordinate ASSUME_SORTED=true USE_THREADING=true'%( ' '.join(bamFileList), GATKbam,  ), \
                    'picard BuildBamIndex INPUT=%s VALIDATION_STRINGENCY=LENIENT TMP_DIR=%s'% ( GATKbam, basePath ) ]
        
        # Adding in the launcher for the next stage
        nextStep = 'insertSizeDist'
        cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep ) )

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Merging the sorted bam files together:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )
        
        # Running the job
        maxTime   = '12:00:00'
        maxMemory = '10G'
        sh_File   = join( basePath, '%s_mergeBamFiles.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )
        
    elif ( snpStep == 'insertSizeDist' ):
        
        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )
        groupList = [item for item in groupDict.keys()]

        # If the caller is VARSCAN, we still need to get some basic statistics on the run
        if ( configDict['CALLER'] == 'VARSCAN' ):
            bamID         = '%s.%s'%( configDict['RUNID'], groupList[0] )
            inputBAM_file = join( basePath, '%s.gatk.bam'%( bamID ) )
        else:
            inputBAM_file = GATKbam
        #####

        # Changing to the base directory
        chdir( basePath )

        # Setting the commands
        nextStep  = 'GC_Depth_plots'
        cmdList   = [ 'python %s/snpCalling/insertSize.py %s %s %s %s %d'%( runPath, inputBAM_file, basePath, configDict['RUNID'], cmd_log, inserts_to_sample ), \
                      'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Computing Insert Sizes:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )

        # Running the job
        maxTime   = "2:00:00"
        maxMemory = '5G'
        sh_File   = join( basePath, '%s_insertSize.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )
    
    elif ( snpStep == 'GC_Depth_plots' ):

        # Changing to the base directory
        chdir( basePath )


        # Setting the commands
        snpStep   = 'call_SNPs'
        indelStep = 'call_INDELS'

        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )
        groupList = [item for item in groupDict.keys()]

        # Setting up the calling
#        cmdList   = ['source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV']
        cmdList   = []
        if ( configDict['CALLER'] == 'VARSCAN' ):
            bamID         = '%s.%s'%( configDict['RUNID'], groupList[0] )
            inputBAM_file = join( basePath, '%s.gatk.bam'%( bamID ) )
            cmdList.append( 'python %s/snpCalling/GC_vs_Depth.py %s %s %s %s %d %s'%( runPath, inputBAM_file, configDict['REFERENCE'], basePath, configDict['RUNID'], GC_Depth_binSize, cmd_log) )
        else:
            cmdList.append( 'python %s/snpCalling/GC_vs_Depth.py %s %s %s %s %d %s'%( runPath, GATKbam, configDict['REFERENCE'], basePath, configDict['RUNID'], GC_Depth_binSize, cmd_log) )
            # If we are only wanting to generate alignments then stop here
            if ( not configDict['ALIGN_ONLY'] ):
                cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, snpStep ) )
                cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, indelStep ) )
            #####
        #####

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Computing GC vs. Depth to Examine GC bias:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )

        # Running the job
        maxTime   = "12:00:00" # hours
        maxMemory = '40G'
        sh_File   = join( basePath, '%s_GC_vs_depth.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )
    
    elif ( snpStep == 'call_SNPs' ):

        # Changing to the base directory
        chdir( basePath )

        # Reading in the average depth and computing the depth filter for snps
        avgDepth   = pullAverageDepth( basePath, configDict )
        lowFilter  = max( 4, int(  avgDepth / 3.0 ) )
        highFilter = max( 40, int( avgDepth * 3.0 ) )

        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )

        # Opening the jobID output file
        jobID_file = join( basePath, '%s.snps.jobIDs.dat'%(configDict['RUNID']) )
        jobID_oh   = open( jobID_file, 'w' )

        # Launching the bam sorting jobs
        nextStep       = 'snp_Filter'
        maxTime        = "12:00:00"
        maxMemory      = '20G'
        suppressOutput = False
        for groupID in groupDict.iterkeys():
            
            # Input/Output Files
            grp_BAM        = join( basePath, '%s.%s.gatk.bam'%(configDict['RUNID'], groupID) )
            grp_rawCalls   = join( basePath, '%s.%s.GATK.SNP.rawcalls.vcf'%(configDict['RUNID'], groupID) )
            grp_preFilter  = join( basePath, '%s.%s.GATK.SNP.preFilter.vcf'%(configDict['RUNID'], groupID) )
            grp_postFilter = join( basePath, '%s.%s.GATK.SNP.postFilter.vcf'%(configDict['RUNID'], groupID) )
            
            # Getting the commands
            cmdList = call_SNPs( configDict['REFERENCE'], grp_BAM, grp_rawCalls, grp_preFilter, grp_postFilter, \
                                 configDict['RUNID'], basePath, configFile, lowFilter, highFilter, avgDepth, \
                                 configDict['MQ_CUTOFF'], cmd_log )


            # The last job submitted needs to have the waiter and the launcher
            # for the next round of analysis
            if ( groupID == finalGroup_ID ):
                cmdList.append( 'python %s/snpCalling/waitingForJobs.py %s'%( runPath, jobID_file ) )
                cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%(runPath, basePath, configFile, nextStep) )
            #####

            # Creating the sh files
            sh_File   = join( basePath, '%s_%s_call_SNPs.sh'%(configDict['RUNID'], groupID) )
            create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=None )

            # Submitting the job
            job_ID = submitJob( sh_File )
            
            # Capture the job id for all but the last job
            if ( groupID != finalGroup_ID ):
                jobID_oh.write( '%s\n'%job_ID )
            #####

        #####
        jobID_oh.close()
    
    elif ( snpStep == 'snp_Filter' ):
    
        # Changing to the base directory
        chdir( basePath )

        # Setting the commands
        nextStep  = 'createSNP_plots'
        cmdList   = ['cat %s/%s.GRP_*.GATK.SNP.postFilter.vcf > %s'%(basePath,configDict['RUNID'],SNP_postFilter), \
                     'python %s/snpCalling/filter_gap_and_masking.py %s %d %d %s %s %s %s'%( runPath, configDict['REFERENCE'], configDict['MASKFILTER'], \
                                                                                             configDict['GAPFILTER'], SNP_postFilter, configDict['RUNID'], \
                                                                                             SNP_softMaskFilter_vcf, cmd_log ), \
                     'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Filtering SNPs on Soft Masking:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )

        # Running the job
        maxTime   = "12:00:00"
        maxMemory = '20G'
        sh_File   = join( basePath, '%s_SNP_sftMskFltr.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    elif ( snpStep == 'createSNP_plots' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Setting the commands
        nextStep  = 'SNP_context_files'
        cmdList   = [ 'python %s/snpCalling/make_SNP_plots.py %s %s %s %s %s %s'%( runPath, SNP_softMaskFilter_vcf, configDict['RUNID'], basePath, \
                                                                                  cmd_log, configDict['REFERENCE'], GC_Depth_binSize ), \
                     'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Creating all SNP plots:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )

        # Running the job
        maxTime   = "12:00:00"
        maxMemory = '5G'
        sh_File   = join( basePath, '%s_SNP_plots.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )
    
    elif ( snpStep == 'SNP_context_files' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Setting the commands
        cmdList   = [ 'python %s/snpCalling/makeContextFile.py %s %s %s %s %s'%( runPath, SNP_softMaskFilter_vcf, configDict['REFERENCE'], \
                                                                                SNP_het_contextFile, SNP_hom_contextFile, cmd_log ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Generating SNP context files:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )

        # Running the job
        maxTime   = "12:00:00"
        maxMemory = '5G'
        sh_File   = join( basePath, '%s_SNP_context.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )
        
    elif ( snpStep == 'call_INDELS' ):
        
        # Changing to the base directory
        chdir( basePath )

        # Reading in the average depth and computing the depth filter for snps
        avgDepth   = pullAverageDepth( basePath, configDict )
        lowFilter  = max( 4, int(  avgDepth / 3.0 ) )
        highFilter = max( 40, int( avgDepth * 3.0 ) )

        # Creating the scaffold list
        groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )

        # Opening the jobID output file
        jobID_file = join( basePath, '%s.indels.jobIDs.dat'%(configDict['RUNID']) )
        jobID_oh   = open( jobID_file, 'w' )

        # Launching the bam sorting jobs
        nextStep       = 'INDEL_Filter'
        maxTime        = "12:00:00"
        maxMemory      = '40G'
        suppressOutput = False
        for groupID in groupDict.iterkeys():
            
            # Input/Output Files
            grp_BAM        = join( basePath, '%s.%s.gatk.bam'%(configDict['RUNID'], groupID) )
            grp_rawCalls   = join( basePath, '%s.%s.GATK.INDEL.rawcalls.vcf'%(configDict['RUNID'], groupID) )
            grp_preFilter  = join( basePath, '%s.%s.GATK.INDEL.preFilter.vcf'%(configDict['RUNID'], groupID) )
            grp_postFilter = join( basePath, '%s.%s.GATK.INDEL.postFilter.vcf'%(configDict['RUNID'], groupID) )
            
            # Getting the commands
            cmdList = call_INDELs( configDict['REFERENCE'], grp_BAM, grp_rawCalls, grp_preFilter, grp_postFilter, \
                                   configDict['RUNID'], basePath, cmd_log, lowFilter, highFilter, avgDepth, \
                                   configDict['MQ_CUTOFF'], configFile )

            # The last job submitted needs to have the waiter and the launcher
            # for the next round of analysis
            if ( groupID == finalGroup_ID ):
                cmdList.append( 'python %s/snpCalling/waitingForJobs.py %s'%( runPath, jobID_file ) )
                cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%(runPath, basePath, configFile, nextStep) )
            #####

            # Creating the sh files
            sh_File   = join( basePath, '%s_%s_call_INDELS.sh'%(configDict['RUNID'], groupID) )
            create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'] )

            # Submitting the job
            job_ID = submitJob( sh_File )

            # Capture the job id for all but the last job
            if ( groupID != finalGroup_ID ):
                jobID_oh.write( '%s\n'%job_ID )
            #####

        #####

        jobID_oh.close()
        
    elif ( snpStep == 'INDEL_Filter' ):

        # Changing to the base directory
        chdir( basePath )

        # Setting the commands
        nextStep  = 'INDEL_context_files'
        cmdList   = ['cat %s/%s.GRP_*.GATK.INDEL.postFilter.vcf > %s'%(basePath,configDict['RUNID'],INDEL_postFilter), \
                     'python %s/snpCalling/filter_gap_and_masking.py %s %d %d %s %s %s %s'%( runPath, configDict['REFERENCE'], configDict['MASKFILTER'], \
                                                                                             configDict['GAPFILTER'], INDEL_postFilter, configDict['RUNID'], \
                                                                                             INDEL_softMaskFilter_vcf, cmd_log ), \
                     'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Filtering INDELs on Soft Masking:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )

        # Running the job
        maxTime   = "12:00:00"
        maxMemory = '10G'
        sh_File   = join( basePath, '%s_INDEL_sftMskFltr.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    elif ( snpStep == 'INDEL_context_files' ):

        # Changing to the base directory
        chdir( basePath )

        # Setting the commands
        nextStep  = 'writeSummaryFiles'
        cmdList   = ['python %s/snpCalling/makeContextFile.py %s %s %s %s %s'%( runPath, INDEL_softMaskFilter_vcf, configDict['REFERENCE'], \
                                                                                INDEL_het_contextFile, INDEL_hom_contextFile, cmd_log ), \
                     'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Generating INDEL context files:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )
        
        # Running the job
        maxTime   = "12:00:00"
        maxMemory = '5G'
        sh_File   = join( basePath, '%s_INDEL_cntxt.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    elif ( snpStep == 'writeSummaryFiles' ):
        
        # Waiting until the context files have been written
        contextFileList = [INDEL_het_contextFile, \
                           INDEL_hom_contextFile, \
                           SNP_het_contextFile, \
                           SNP_hom_contextFile]
        jobFinished_Files( contextFileList )
        
        # Changing to the base directory
        chdir( basePath )

        # Reading in the average depth and computing the depth filter for snps
        avgDepth   = pullAverageDepth( basePath, configDict )
        lowFilter  = max( 4, int(  avgDepth / 3.0 ) )
        highFilter = max( 40, int( avgDepth * 3.0 ) )

        # Setting the commands
        cmdList   = ['python %s/snpCalling/writeSummaryFiles.py %s %s %d %d %.3f %.3f %s %s'%( runPath, \
                                                                                               GATKbam, \
                                                                                               configDict['REFERENCE'], \
                                                                                               configDict['MASKFILTER'], \
                                                                                               configDict['GAPFILTER'], \
                                                                                               lowFilter, \
                                                                                               highFilter, \
                                                                                               configDict['RUNID'], \
                                                                                               basePath ) ]

        # Writing to the log file
        writeAndEcho( '\n-----------------------\n- Writing the summary files:', cmd_log )
        for cmd in cmdList: writeAndEcho( cmd, cmd_log )
        
        # Running the job
        maxTime   = "12:00:00"
        maxMemory = '40G'
        sh_File   = join( basePath, '%s_summaryFiles.sh'%configDict['RUNID'] )
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1 )

    #####
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
