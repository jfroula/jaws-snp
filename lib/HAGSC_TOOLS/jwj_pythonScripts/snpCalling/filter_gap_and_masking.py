#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/23/15"

from snp_lib import Filter

from sys import argv

#==============================================================
def real_main():
    
    # Read inputs
    genomeFASTA     = argv[1]
    softMaskWindow  = int(argv[2])
    gapWindow       = int(argv[3])
    postFilter      = argv[4]
    RUN_ID          = argv[5]
    softMaskFilter  = argv[6]
    cmd_log         = argv[7]
    
    # Run insert size calculator
    Filter( genomeFASTA, softMaskWindow, gapWindow, postFilter, RUN_ID, softMaskFilter, cmd_log )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
