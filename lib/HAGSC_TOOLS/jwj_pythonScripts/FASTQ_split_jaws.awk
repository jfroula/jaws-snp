#=============================================
BEGIN{
    # Initializing the breaking up
    nPairs       = 0
    fileNumber   = 1
    sumSeq       = 0
    fastqFile    = basePath"/"RUN_ID"."fileNumber".fastq.gz"
    system( "rm "fastqFile )
}

#=============================================
function exists( file, line )
{
        if ( (getline line < file) == -1 )
        {
                return 0
        }
        else {
                close(file)
                return 1
        }
}

#=============================================
# MAIN CODE
{
    # Reading in the pair information
    if ( NR%8 == 1 ) { 
        read1 = $0
        gsub( /(-R)/, "/", read1 ) # Converts the separator string in the readID
    }
    if ( NR%8 == 2 ) { seq1  = $0 }
    if ( NR%8 == 4 ) { qual1 = $0 }
    if ( NR%8 == 5 ) {
        read2 = $0 
        gsub( /(-R)/, "/", read2 ) # Converts the separator string in the readID
    }
    if ( NR%8 == 6 ) { seq2  = $0 }
    if ( NR%8 == 0 ) {
        
        # Writing the pair to the file
        qual2  = $0
        print read1"\n"seq1"\n+\n"qual1"\n"read2"\n"seq2"\n+\n"qual2 | " gzip -c > " fastqFile
        nPairs = nPairs + 1

        # Checking to see if the total number of bases has been exceeded
        sumSeq = sumSeq + length(seq1)
        sumSeq = sumSeq + length(seq2)
        
        if ( (nPairs >= pairsPerFile) || (sumSeq >= TOTAL_BASES) ){
            
            # Closing the fastq file
            close ( " gzip -c > " fastqFile )
            
            # Writing the .sh file
            shFile = basePath"/"RUN_ID"."fileNumber".sh"
            system( "rm "shFile )
            print "#!/bin/bash" > shFile

            splitCmd = "awk -v EMAIL_ADDRESS=\""EMAIL_ADDRESS"\" -v nextStep=\""nextStep"\" -v configFile=\""configFile"\" -v runPath=\""runPath"\" -v basePath=\""basePath"\" -v lastJob=\"0\" -v RUN_ID=\""RUN_ID"\" -v fileNumber=\""fileNumber"\" -v PRIMARY_SCAFFS_FILE=\""PRIMARY_SCAFFS_FILE"\" -f "runPath"/bamSplitter_jaws.awk"
            splitCmd_sorting = "awk -v EMAIL_ADDRESS=\""EMAIL_ADDRESS"\" -v nextStep=\""nextStep"\" -v configFile=\""configFile"\" -v runPath=\""runPath"\" -v basePath=\""basePath"\" -v lastJob=\"1\" -v RUN_ID=\""RUN_ID"\" -v fileNumber=\""fileNumber"\" -v PRIMARY_SCAFFS_FILE=\""PRIMARY_SCAFFS_FILE"\" -f "runPath"/bamSplitter_jaws_sorting.awk"
            if ( ALIGNER == "MEM" ){
                print "bwa mem -t 4 -M -p "INDEX" <(cat "fastqFile" | gunzip -c ) | samtools view -Sh -f 2 -F 4 - | "splitCmd > shFile
                print "bwa mem -t 4 -M -p "INDEX" <(cat "fastqFile" | gunzip -c ) | samtools view -Sh -f 2 -F 4 - | "splitCmd_sorting > shFile
            }
            if ( ALIGNER == "ALN" ){
                # Splitting into R1 and R2
                R1_fastq = "<(cat "fastqFile" | gunzip -c | awk -f "runPath"/pull_FASTQ_reads.awk READPOST=1)"
                R2_fastq = "<(cat "fastqFile" | gunzip -c | awk -f "runPath"/pull_FASTQ_reads.awk READPOST=2)"

                # ----------------------
                # Front BWA command
                # -a maxInsertSize
                # -o maxOcc
                # -P load entire FM index into memory
                # -A disable insert size estimation, should speed up the code significantly
                frontBWA = "bwa sampe -a 2000 -o 100 -P -A"
            
                # Performt the R1/R2 alignments
                baseCmd    = "bwa aln -t 3 "INDEX
                R1_sai_cmd = baseCmd" "R1_fastq
                R2_sai_cmd = baseCmd" "R2_fastq
                
                # Producing the bam file
                bwaCmd  = frontBWA" "INDEX" <("R1_sai_cmd") <("R2_sai_cmd") "R1_fastq" "R2_fastq

                # ----------------------
                # Screening for pairing
                # -S input is SAM format
                # -h print SAM header
                # -b output is BAM
                # -f 2 require pairing
                viewCmd = "samtools view -Sh -f 2 -F 4 -"
                
                # Building the command list
                print "module load bwa"               > shFile
                print bwaCmd" | "viewCmd" | "splitCmd > shFile 
            }
            
            # Closing the sh file
            close ( shFile )
            
            # Launching the job
            system( "bash "shFile )

            # Storing the file information
            fastqFileList[fileNumber] = fastqFile

            # Making the new filenames
            nPairs     = 0
            fileNumber = fileNumber + 1
            fastqFile  = basePath"/"RUN_ID"."fileNumber".fastq.gz"
            system( "rm "fastqFile )
            
            # Checking to see if we have exceeded the total number of bases
            if ( sumSeq >= TOTAL_BASES ){ exit }

        }
    }
}

#=============================================
END{
    
    # Closing the fastq file
    if ( sumSeq < TOTAL_BASES ){
        close ( " gzip -c > " fastqFile )
    }
    
    # Checking to see if the last file was actually written
    if ( (exists(fastqFile) == 1) && (sumSeq < TOTAL_BASES) ){

        # Writing the .sh file
        shFile = basePath"/"RUN_ID"."fileNumber".sh"
        system( "rm "shFile )
        print "#!/bin/bash" > shFile

        splitCmd = "awk -v EMAIL_ADDRESS=\""EMAIL_ADDRESS"\" -v nextStep=\""nextStep"\" -v configFile=\""configFile"\" -v runPath=\""runPath"\" -v basePath=\""basePath"\" -v lastJob=\"1\" -v RUN_ID=\""RUN_ID"\" -v fileNumber=\""fileNumber"\" -v PRIMARY_SCAFFS_FILE=\""PRIMARY_SCAFFS_FILE"\" -f "runPath"/bamSplitter_jaws.awk"
        splitCmd_sorting = "awk -v EMAIL_ADDRESS=\""EMAIL_ADDRESS"\" -v nextStep=\""nextStep"\" -v configFile=\""configFile"\" -v runPath=\""runPath"\" -v basePath=\""basePath"\" -v lastJob=\"1\" -v RUN_ID=\""RUN_ID"\" -v fileNumber=\""fileNumber"\" -v PRIMARY_SCAFFS_FILE=\""PRIMARY_SCAFFS_FILE"\" -f "runPath"/bamSplitter_jaws_sorting.awk"
        if ( ALIGNER == "MEM" ){
            print "bwa mem -t 4 -M -p "INDEX" <(cat "fastqFile" | gunzip -c ) | samtools view -Sh -f 2 -F 4 - | "splitCmd > shFile
            print "bwa mem -t 4 -M -p "INDEX" <(cat "fastqFile" | gunzip -c ) | samtools view -Sh -f 2 -F 4 - | "splitCmd_sorting > shFile
        }
        if ( ALIGNER == "ALN" ){
            # Splitting into R1 and R2
            R1_fastq = "<(cat "fastqFile" | gunzip -c | awk -f "runPath"/pull_FASTQ_reads.awk READPOST=1)"
            R2_fastq = "<(cat "fastqFile" | gunzip -c | awk -f "runPath"/pull_FASTQ_reads.awk READPOST=2)"

            # ----------------------
            # Front BWA command
            # -a maxInsertSize
            # -o maxOcc
            # -P load entire FM index into memory
            # -A disable insert size estimation, should speed up the code significantly
            frontBWA = "bwa sampe -a 2000 -o 100 -P -A"
        
            # Performt the R1/R2 alignments
            baseCmd    = "bwa aln -t 3 "INDEX
            R1_sai_cmd = baseCmd" "R1_fastq
            R2_sai_cmd = baseCmd" "R2_fastq
            
            # Producing the bam file
            bwaCmd  = frontBWA" "INDEX" <("R1_sai_cmd") <("R2_sai_cmd") "R1_fastq" "R2_fastq

            # ----------------------
            # Screening for pairing
            # -S input is SAM format
            # -h print SAM header
            # -b output is BAM
            # -f 2 require pairing
            viewCmd = "samtools view -Sh -f 2 -F 4 -"
            
            # Building the command list
            print bwaCmd" | "viewCmd" | "splitCmd > shFile 
        }
        
        # Closing the sh file
        close ( shFile )
        
        # Launching the job
        system( "bash "shFile )
    
        # Storing the file information
        fastqFileList[fileNumber] = fastqFile
        
    }
    else{
        
        #===================================
        # Handling the case where we don't launch the final
        # job, in this case we have to launch the watcher from here
        
        # Writing the .sh file
        shFile = basePath"/"RUN_ID".watching.sh"
        system( "rm "shFile )
        print "#!/bin/bash" > shFile
        print "python "runPath"/snpCalling/waiting.py "RUN_ID > shFile
#        print "python "runPath"/snpCalling/snpJobLauncher.py "basePath" "configFile" "nextStep > shFile

        # Closing the sh file
        close ( shFile )
    
        # Launching the sorting job
        system( "bash "shFile )
    }

    # Writing the names of the fastq files to the output file
    fastq_FOFN = basePath"/full_fastq_file_list.dat"
    system( "rm "fastq_FOFN )
    for ( fileNum in fastqFileList ){
        print fastqFileList[fileNum] > fastq_FOFN
    }
    close( fastq_FOFN )

}
