#=================================
BEGIN{
    # Initializing the global variables
    n        = 1
    header[n]= 0
    # Reading in the primary scaffolds
    split("", primaryScaffs) # Initializes as a blank array
    while ( (getline < PRIMARY_SCAFFS_FILE) > 0 ){
        print $1, $2
        primaryScaffs[$1] = "GRP_"$2
        headerArray["GRP_"$2] = 0
    }
}

#=================================
# Main splitting code
{
    # Pulling the header information
    if ( ($1 == "@SQ") || ($1 == "@PG") ){
        # Storing the header for later
        header[n] = $0
        n++
    }else{
        # Determining the output bam file name
        GRP_ID = primaryScaffs[$3]
        outputBAM_File = basePath"/"RUN_ID"."GRP_ID"."fileNumber".bam"
        # Writing entries to the bam file
        if ( headerArray[GRP_ID] == 0 ){
            # Resetting the header information
            headerArray[GRP_ID] = 1
            system( "rm "outputBAM_File )
            # Writing the header the first time through
            for ( i=1; i<n; i++ ){
                print header[i] | "samtools view -Sbh - > "outputBAM_File
            }
            print $0 | "samtools view -Sbh - > "outputBAM_File
        }else{
            # Writing the alignments
            print $0 | "samtools view -Sbh - > "outputBAM_File
        }
    }
}

#=================================
# LAUNCHING THE SORTING JOBS
END{
    
    # Launching the sorting jobs
    for ( GRP_ID in headerArray ){

        # Writing the sorting scripts
        shFile = basePath"/"RUN_ID "."fileNumber"."GRP_ID".sorting.sh"
        system( "rm "shFile )
        print "#!/bin/bash" > shFile
        print "## Run the command from the current working directory" > shFile
        print "#SBATCH -D ." > shFile
        print "## specify an email address" > shFile
        print "#SBATCH --mail-user "EMAIL_ADDRESS > shFile
        print "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally" > shFile
        print "#SBATCH --mail-type=FAIL" > shFile
        print "#SBATCH -t 12:00:00" > shFile
        print "#SBATCH --mem=10G" > shFile
        print "#SBATCH -N 1" > shFile
        print "## Supresses output" > shFile
        print "#SBATCH -e " shFile".stderr" > shFile
        print "#SBATCH -o " shFile".stdout" > shFile
#        print "## Supresses output" > shFile
#        print "#$ -e /dev/null" > shFile
#        print "#$ -o /dev/null" > shFile
        print "## Job Starts Here" > shFile
#        print "source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV" > shFile
        print "samtools sort -m 1G "basePath"/"RUN_ID"."GRP_ID"."fileNumber".bam "basePath"/"RUN_ID"."GRP_ID"."fileNumber".sorted" > shFile
#        print "rm -f "basePath"/"RUN_ID"."fileNumber".fastq.gz" > shFile
#        print "rm -f "basePath"/"RUN_ID"."fileNumber".sh" > shFile
#        print "rm -f "basePath"/"RUN_ID"."GRP_ID"."fileNumber".bam" > shFile
        
        # Closing the sh file
        close ( shFile )
        
        # Launching the sorting job
        system( "bash "shFile )
    }

    if ( lastJob == 1 ){

        # Writing the .sh file
        shFile = basePath"/"RUN_ID".watching.sh"
        system( "rm "shFile )
        print "#!/bin/bash" > shFile
        print "## Run the command from the current working directory" > shFile
        print "#SBATCH -D ." > shFile
        print "## specify an email address" > shFile
        print "#SBATCH --mail-user "EMAIL_ADDRESS > shFile
        print "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally" > shFile
        print "#SBATCH --mail-type=FAIL" > shFile
        print "#SBATCH -t 12:00:00" > shFile
        print "#SBATCH --mem=10G" > shFile
        print "#SBATCH -N 1" > shFile
        print "## Supresses output" > shFile
        print "#SBATCH -e " shFile".stderr" > shFile
        print "#SBATCH -o " shFile".stdout" > shFile
#        print "## Supresses output" > shFile
#        print "#$ -e /dev/null" > shFile
#        print "#$ -o /dev/null" > shFile
        print "## Job Starts Here" > shFile
        print "python "runPath"/snpCalling/waiting.py "RUN_ID > shFile
        print "python "runPath"/snpCalling/snpJobLauncher.py "basePath" "configFile" "nextStep > shFile

        # Closing the sh file
        close ( shFile )
    
        # Launching the sorting job
        system( "bash "shFile )

    }
    
}
