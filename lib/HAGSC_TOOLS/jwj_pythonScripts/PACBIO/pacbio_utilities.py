__author__="Brent Wilson, bwilson@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from os import system, getpid, uname, mkdir, chdir

from os.path import realpath, isfile, join, abspath, curdir, splitext, split, isdir

from optparse import OptionParser

from sys import stdout, stderr, exit

from shutil import rmtree

from hagsc_lib import iterFASTA, iterFASTQ, isGzipFile, iterCounter, histogramClass, iterBestHit, FASTAFile_dict, writeFASTA, getFiles, SeqRecord

import gzip

import re

import string

import math

import difflib

import subprocess

from scipy.stats.stats import pearsonr

from subprocess import Popen
from subprocess import PIPE

from sys import stderr, stdout, argv

from collections import Counter

#==============================================================
class IntervalTree:
        def __init__(self, intervals):
                self.top_node = self.divide_intervals(intervals)

        def divide_intervals(self, intervals):

                if not intervals:
                        return None

                x_center = self.center(intervals)

                s_center = []
                s_left = []
                s_right = []

                for k in intervals:
                        if k.get_end() < x_center:
                                s_left.append(k)
                        elif k.get_begin() > x_center:
                                s_right.append(k)
                        else:
                                s_center.append(k)

                return Node(x_center, s_center, self.divide_intervals(s_left), self.divide_intervals(s_right))
                

        def center(self, intervals):
                fs = sort_by_begin(intervals)
                length = len(fs)

                return fs[int(length/2)].get_begin()

        def search(self, begin, end=None):
                if end:
                        result = []

                        for j in xrange(begin, end+1):
                                for k in self.search(j):
                                        result.append(k)
                                result = list(set(result))
                        return sort_by_begin(result)
                else:
                        return self._search(self.top_node, begin, [])
        def _search(self, node, point, result):
                
                for k in node.s_center:
                        if k.get_begin() <= point <= k.get_end():
                                result.append(k)
                if point < node.x_center and node.left_node:
                        for k in self._search(node.left_node, point, []):
                                result.append(k)
                if point > node.x_center and node.right_node:
                        for k in self._search(node.right_node, point, []):
                                result.append(k)

                return list(set(result))
                
#==============================================================
class Interval:
        def __init__(self, begin, end):
                self.begin = begin
                self.end = end
        def get_begin(self):
                return self.begin
        def get_end(self):
                return self.end

class Node:
        def __init__(self, x_center, s_center, left_node, right_node):
                self.x_center = x_center
                self.s_center = sort_by_begin(s_center)
                self.left_node = left_node
                self.right_node = right_node

def sort_by_begin(intervals):
        return sorted(intervals, key=lambda x: x.get_begin())

#==============================================================
def iround(x):
    y = round(x) - .5
    return int(y) + (y > 0) 
    
#==============================================================    
def roundup100k(x):     
	return int(math.ceil(x / 100000.0)) * 100000
	
#==============================================================    
def rounddown100k(x):     
	return int(math.floor(x / 100000.0)) * 100000

#==============================================================
def roundup20k(x):     
	return int(math.ceil(x / 20000.0)) * 20000
	
#==============================================================    
def rounddown20k(x):     
	return int(math.floor(x / 20000.0)) * 20000
	
#==============================================================
def roundupY(x,y):     
	floatY = float(y)
	return int(math.ceil(x / floatY)) * y
	
#==============================================================    
def rounddownY(x,y):     
	floatY = float(y)
	return int(math.floor(x / floatY)) * y
	
#==============================================================    
def overLap(x1,x2,y1,y2):     
	if y1 > x2: resultO = 0
	else: resultO = x2 - y1
	return resultO

#==============================================================
def rc(dna):
	complements = string.maketrans('acgtrymkbdhvACGTRYMKBDHV', 'tgcayrkmvhdbTGCAYRKMVHDB')
	rcseq = dna.translate(complements)[::-1]
	return rcseq

#==============================================================
def makeFASTA_string( tmpRec ):
    return ">%s\n%s\n"%( tmpRec['id'], tmpRec['data'] )

#==============================================================
def printAndExec( cmd, oh_cmdLog, execute=True ):
    stderr.write( '%s\n\n'%cmd )
    oh_cmdLog.write(     '%s\n\n'%cmd )
    try:
        if ( execute ): system( cmd )
    except KeyboardInterrupt:
        print( "KeyboardInterrupt detected, stopping the execution" )
        exit()
    #####
    return
	
#==============================================================

def plotDatBlat (baseName, dataOut, oh_cmdLog):
    plotFile = '%s.plot.dat'%baseName
    d = open('%s'%plotFile, 'w')
    d.write('\nreset\nset output "%s.MetaBlatHits.png"\n'%(baseName))
    d.write('set terminal png nocrop size 1280,1020 font \'Helvetica,20\'\nset nokey\nset title \"%s Length Histogram: Gap Filling\"\n'%baseName)
    d.write('set grid x y\nset xlabel \"Length\"\nset ylabel \"Percentage\"\nplot \"%s\" using 1:2 axis x1y1 with boxes, "%s" using 1:3 axis x1y2 with lines'%(dataOut, dataOut))
    d.close()
    printAndExec( 'gnuplot %s' %(plotFile), oh_cmdLog, execute=True )
	
#==============================================================
def frequencyArrayCorrelation(subsetDataFile, dataFile):
	arrayDict = {}
	for line in open('%s'%subsetDataFile):
		lineSplit = line.split(None)
		arrayDict[lineSplit[0]] = [float(lineSplit[1])]
	for line in open('%s'%dataFile):
		lineSplit = line.split(None)
		try: arrayDict[lineSplit[0]].append(float(lineSplit[1]))
		except KeyError: continue
		
	array1 = []
	array2 = []
	for aD in arrayDict:
		array1.append(arrayDict[aD][0])
		array2.append(arrayDict[aD][1])
		
	print len(array1)
	print len(array2)
		
	a1a2 = pearsonr(array1, array2)
	print "Correlation Coefficient:\t%.5f\nP-Value:\t%.5f"%(a1a2[0],a1a2[1])	
			
#==============================================================
def fPfN(dataFile, dataFile2, baseName):
	
	A = set( [line.split()[1] for line in open('%s'%dataFile)] )
	B = set( [line.split()[1] for line in open('%s'%dataFile2)] )
	
	print len(B.intersection(A))
	print len(B.difference(A))
	print len(A.difference(B))
	
#==============================================================
def scaffoldSplitter (fastaFile, baseName):
    outFasta = '%s.fasta'%baseName
    oF= open('%s'%outFasta,'w')
    for y in iterFASTA(open('%s'%fastaFile)):
		record = y
		scaffold = str(y).split()[1]
		pullContigs = re.compile( r'([GATC]+)' ).finditer
		a = 1
		for item in pullContigs(str(record.seq)): 
		  oF.write('>%s_%d\n%s\n'%(scaffold,a,item.group(0)))
		  a += 1
    oF.close()
	
#==============================================================
def merListOverLaps(dataFile, dataFile2, baseName):
	hitDict = {}
	lineDict = {}
	
	matchM = '%s.MatchMers.dat'%baseName
	mM = open('%s'%matchM,'w')
	
	for line in open('%s'%dataFile):
		lineSplit = line.split(None)
		hitDict[lineSplit[0]] = 0
		lineDict[lineSplit[0]] = line
	for line in open('%s'%dataFile2):
		lineSplit = line.split(None)
		try: hitDict[lineSplit[0]] += 1
		except KeyError: pass
	
	for hD in hitDict:
		if hitDict[hD] > 0:
			mM.write('%s'%(lineDict[hD])) 
		
	mM.close()

#==============================================================		
def parseMarkers (bestHitFile, preFix):
	    
    # Parameters
#     bestHitFile = 'MorganReduced100mers.latestGT3Kbclipped.good.blat.bestHit'
#     preFix      = 'test'
    
    # Screening the marker placements
    groupDict    = {}
    outputString = 8*['']; mID = 1
    for record in iterBestHit( open(bestHitFile, 'r') ):
        # Screening for quality
        if ( record.per_ID < 90.0 or record.per_coverage < 85.0 ): continue
        splitName = record.BAC_recordName.split('/')
        markerID  = mID; mID += 1
        mapGroup  = splitName[0]
        mapPos    = float(splitName[1])
        # Generating the output string
        outputString[0] = mapGroup
        outputString[1] = '%.4f'%mapPos
        outputString[2] = record.scaffold
        outputString[3] = '%d'%record.scaffSize
        outputString[4] = '%d'%record.scaffStart
        outputString[5] = '%d'%record.scaffEnd
        outputString[6] = record.placDir
        outputString[7] = '%s\n'%markerID
        finalString = '\t'.join(outputString)
        # Storing markers
        try:
            groupDict[mapGroup].append( (mapPos,record.scaffold,record.scaffSize,record.scaffStart,mapGroup,finalString) )
        except KeyError:
            groupDict[mapGroup] = [(mapPos,record.scaffold,record.scaffSize,record.scaffStart,mapGroup,finalString)]
        #####
    #####
    
    # Writing the results to an output file sorted by map and scaffold
#     outputHandle_Map   = open( 'sortByMap_%s.out'%preFix, 'w' )
    outputHandle_Scaff = open( 'sortByScaff_%s.out'%preFix, 'w' )
    
    # Sorting by map
#     DSU = [item for item in groupDict.keys()]
#     DSU.sort()
#     scaffSizes  = {}
#     scaffPlacs  = {}
#     for mapGroup in DSU:
#         mapList = groupDict[mapGroup]
#         mapList.sort()
#         outputHandle_Map.write( '===========================================\n')
#         for mapPos, scaffID, scaffSize, scaffStart, mapGroup, finalString in mapList:
#             # Storing the scaffold size
#             scaffSizes[scaffID] = scaffSize
#             # Storing the scaffold
#             try:
#                 scaffPlacs[scaffID].append( (scaffStart,mapGroup,finalString) )
#             except KeyError:
#                 scaffPlacs[scaffID] = [(scaffStart,mapGroup,finalString)]
#             #####
#             outputHandle_Map.write(finalString)
#         #####
#     #####
    
    # Sorting by scaffold            
    DSU_scaff = [ (value,key) for key,value in scaffSizes.iteritems() ]
    DSU_scaff.sort(reverse=True)
    for scaffSize, scaffID in DSU_scaff:
        # Pulling the placement list
        placList = scaffPlacs[scaffID]
        placList.sort()
        # Looking for the most abundant map group
        tmpCmp = {}
        for scaffStart,mapGroup,finalString in placList:
            try:
                tmpCmp[mapGroup] += 1
            except KeyError:
                tmpCmp[mapGroup] = 1
            #####
        #####
        DSU_cmp = [(value,key) for key, value in tmpCmp.iteritems()]
        DSU_cmp.sort(reverse=True)
        targetGrp = DSU_cmp[0][1]
        # Write the list
        outputHandle_Scaff.write( '===========================================\n')
        for scaffStart, mapGroup, finalString in placList:
            if ( mapGroup != targetGrp ):
                outputHandle_Scaff.write( ''.join( ['**',finalString] ) )
            else:
                outputHandle_Scaff.write( finalString )
            #####
        #####
    #####
    
    # Closing the output files
#     outputHandle_Map.close()
    outputHandle_Scaff.close()
    
#==============================================================
# run with target starts
def best_extent( qsize, blksizes, blkstarts, querySizeMultiple ):
    # Starting information
    startpt = blkstarts[0]
    endpt   = blkstarts[-1] + blksizes[-1]  # blksizes is already sorted!

    # Use entire match if not more than twice query size
    if ( (endpt - startpt) <= (2 * qsize)  ):
        return startpt, endpt
    #####

    # Otherwise, pick "best" blocks totaling less than a "querySizeMultiple"
    # of query size.  Start with the biggest block!
    nBlks     = len(blksizes)
    DSU       = [ (blksizes[n],n) for n in xrange( nBlks ) ]
    DSU.sort( reverse=True )
    max_b     = DSU[0][1]
    startpt   = blkstarts[max_b]
    endpt     = startpt + blksizes[max_b]

    # Extend to smaller block of left or right;
    # repeat until 2X of query size would be exceeded
    leftb  = max_b - 1
    rightb = max_b + 1
    while ( (leftb>=0) or (rightb<nBlks) ):
        # Left extension
        newstart = ( (leftb<0) and [startpt] or [blkstarts[leftb]] )[0]
        leftAddLength = endpt - newstart
        # Right extension
        newend = ( (rightb==nBlks) and \
                   [endpt] or \
                   [blkstarts[rightb] + blksizes[rightb]] )[0]
        rightAddLength = newend - startpt
        # Decision time
        if ( (rightb<nBlks) and \
             ( (leftb<0) or (rightAddLength < leftAddLength) ) ):
            if ( rightAddLength <= (querySizeMultiple*qsize) ):
                endpt = newend
                rightb += 1
            else:
                rightb = nBlks # Terminal condition
            #####
        elif ( leftAddLength <= (querySizeMultiple*qsize) ):
            startpt = newstart
            leftb -= 1
        else:
            leftb = -1  # Terminal condition
        #####
    ##### end while loop
    return startpt, endpt

#==============================================================
class blat_Entry_class(object):
    def __init__( self, match, misMatch, repMatch, Ns, Q_gap_count, \
                        Q_gap_bases, T_gap_count, T_gap_bases, strand, \
                        Q_name, Q_size, Q_start, Q_end, T_name, T_size, \
                        T_start, T_end):

        self.match    = match
        self.misMatch = misMatch
        self.repMatch = repMatch

        self.Ns = Ns

        self.Q_gap_count = Q_gap_count
        self.Q_gap_bases = Q_gap_bases

        self.T_gap_count = T_gap_count
        self.T_gap_bases = T_gap_bases

        self.strand = strand

        self.Q_name  = Q_name
        self.Q_size  = Q_size
        self.Q_start = Q_start
        self.Q_end   = Q_end

        self.T_name  = T_name
        self.T_size  = T_size
        self.T_start = T_start
        self.T_end   = T_end

    def __str__(self):
        outputString = 22 * ['']
        outputString[0] = '%d\t'%self.match
        outputString[1] = '%d\t'%self.misMatch
        outputString[2] = '%d\t'%self.repMatch
        outputString[3] = '%d\t'%self.Ns
        outputString[4] = '%d\t'%self.Q_gap_count
        outputString[5] = '%d\t'%self.Q_gap_bases
        outputString[6] = '%d\t'%self.T_gap_count
        outputString[7] = '%d\t'%self.T_gap_bases
        outputString[8] = '%s\t'%self.strand

        outputString[9]  = '%s\t'%self.Q_name
        outputString[10] = '%d\t'%self.Q_size
        outputString[11] = '%d\t'%self.Q_start
        outputString[12] = '%d\t'%self.Q_end

        outputString[13] = '%s\t'%self.T_name
        outputString[14] = '%d\t'%self.T_size
        outputString[15] = '%d\t'%self.T_start
        outputString[16] = '%d\t'%self.T_end

        outputString[17] = '\n'

        return ''.join(outputString)

    def __repr__(self):
        return str(self)

#####

#==============================================================
def makingBestHitFile (dataFile, dataFile2, baseName):
    # Reading in the marker names
    reliableSet = set()
    x = iterCounter(1000000)
    for line in open('%s'%dataFile): reliableSet.add(line.split(None)[0]); x()

    # Converting to bestHit format
    oh = open( '%s.%s.bestHit'%(dataFile2, baseName), 'w' )
    x = iterCounter(1000000)
    for blatClass in iterBLAT( open('%s'%dataFile2) ):
        # Size of match divided by shorter of read sizes
        Q_length = float( blatClass.Q_end - blatClass.Q_start )
        T_length = float( blatClass.T_end - blatClass.T_start )
        Q_size   = float( blatClass.Q_size )
        T_size   = float( blatClass.T_size )
        x()
        totalMatchSize = blatClass.match + blatClass.repMatch
        
        # ID = max % of identical bases in the placement length
        # of the query/target
        id = 100.0 * float(totalMatchSize) / min( Q_length, T_length )
        id = min( 100.0, id )

        # Coverage = max % of the total query/target length that places
        cov = 100.0 * max( (Q_length/Q_size), (T_length/T_size) )

        # Initializing the output string
        outputString    = 12 * ['']
        outputString[0] = blatClass.Q_name
        print outputString[0]
        print list(reliableSet)[0]
        outputString[0] = '%s_%s/%s'%(outputString[0].split('_')[0],outputString[0].split('_')[1],outputString[0].split('_')[2])
        print outputString[0]; assert False
        if outputString[0] not in reliableSet: continue

        #####
        outputString[1] = '%d'%blatClass.Q_size
        outputString[2] = '%d'%blatClass.Q_start
        outputString[3] = '%d'%blatClass.Q_end
        outputString[4]  = '%s'%blatClass.strand
        outputString[5]  = '%s'%blatClass.T_name
        outputString[6]  = '%d'%blatClass.T_size
        outputString[7]  = '%d'%blatClass.T_start
        outputString[8]  = '%d'%blatClass.T_end
        outputString[9]  = '%d'%totalMatchSize
        outputString[10] = '%5.2f'%id
        outputString[11] = '%5.2f\n'%cov
        oh.write( '\t'.join(outputString) )
    #####
    oh.close()
    
#==============================================================
def DotPlotChecker (signal, readsFasta, targetFasta, baseName):

#     combos = []
#     for line in open('%s'%mapFile):
#         lineSplit = line.split(None)
#         combos.append((lineSplit[0],lineSplit[1],int(lineSplit[2]),int(lineSplit[3])))

    # Indexing the FASTA file
    print "Indexing fasta file"
    indexedTarget = FASTAFile_dict('%s'%targetFasta)
    
    # Indexing the reads FASTA file
    print "Indexing reads"
    indexReads = FASTAFile_dict('%s'%readsFasta)
    
    # Making dot plots    
    combos = [(signal[0],signal[1],signal[2],signal[3]),(signal[0],signal[4],signal[5],signal[6])]
#     combos = [(signal[0],signal[1],signal[2],signal[3])]
    for comb in combos:
        cloneID = '%s_%s_%d_%d'%(comb[0].replace('/','_'),comb[1],comb[2],comb[3])
        
        # Writing the reads to a file
        oh = open('tmpReadSeq.fasta','w')
        writeFASTA( [indexReads[comb[0]]], oh )
        oh.close()
        
        # Pulling the sequence
        oh = open('tmpScaffold.fasta', 'w')
        writeFASTA( [indexedTarget[comb[1]]], oh )
        oh.close()
        start = max(comb[2] - 10000,0); end = comb[3] + 10000
#         if comb[3] - comb[2] > 60000: 
#             distance = comb[3] - comb[2]
#             start = max(comb[2] + distance/4,0)
#             end = comb[3] - distance/4
#         else: start = max(comb[2] - 10000,0); end = comb[3] + 10000
        
        # Making a dot plot
        cmd = ['gepardCMD']
        cmd.append( '-seq1 tmpReadSeq.fasta' )
        cmd.append( '-seq2 tmpScaffold.fasta' )
        cmd.append( '-from2 %d'%start )
        cmd.append( '-to2 %d'%end )
        cmd.append( '-matrix /mnt/local/EXBIN/gepard-1.30/matrices/edna.mat' )
#         cmd.append( '-zoom 18' )
        cmd.append( '-word 8' )
        cmd.append( '-maxheight 1300')
        cmd.append( '-lower 0' )
        cmd.append( '-upper 20' )
        cmd.append( '-greyscale 0' )
        cmd.append( '-format png' )
        cmd.append( '-outfile %s.png'%(cloneID) )
        cmd_str = "/bin/csh -i -c '%s'"%( ' '.join(cmd) )
        print cmd_str
        system( cmd_str  )
        
    #####
		
#==============================================================		
def iterBLAT( blatFileHandle ):
    """
    iterBLAT( blatFile )

    Generator function to iterate over entries in a blat file, returning
    blat_Entry_class objects.
    """

    # Skipping the first 5 lines if necessary
    firstLine = blatFileHandle.readline()
    splitLine = firstLine.split(None)
    try:
        if ( splitLine[0] == 'psLayout' ):
            for n in xrange(4): blatFileHandle.readline()
        else:
            # Rewind the file handle unless it is stdout
            try:
                blatFileHandle.seek(0)
            except IOError:
                pass
            #####
        #####
    except IndexError:
        pass
    #####

    # Reading in the rest of the file
    for line in blatFileHandle:

        parsedLine = line.split(None)
        
        try:
            # Pulling the information
            match    = int( parsedLine[0] )
            misMatch = int( parsedLine[1] )
            repMatch = int( parsedLine[2] )
            Ns       = int( parsedLine[3] )
    
            Q_gap_count = int( parsedLine[4] )
            Q_gap_bases = int( parsedLine[5] )
            T_gap_count = int( parsedLine[6] )
            T_gap_bases = int( parsedLine[7] )
            strand      = parsedLine[8]
            
            Q_name  = parsedLine[9]
            Q_size  = int( parsedLine[10] )
            Q_start = int( parsedLine[11] )
            Q_end   = int( parsedLine[12] )
            
            T_name  = parsedLine[13]
            T_size  = int( parsedLine[14] )
            T_start = int( parsedLine[15] )
            T_end   = int( parsedLine[16] )
            
            # Adjusting the coordinates if necessary
            if ( strand == '+-' ):
                T_starts = [(T_size - T_starts[n]) for n in xrange(len(T_starts))]
                strand = '-'
            elif ( strand == '-+' ):
                Q_starts = [(Q_size - Q_starts[n]) for n in xrange(len(Q_starts))]
                strand = '-'
            #####
        except ValueError:
            continue
        except IndexError:
            continue
        #####
        
        # Yield up the record and wait for next iteration
        yield blat_Entry_class( match, misMatch, repMatch, Ns, Q_gap_count, \
                             Q_gap_bases, T_gap_count, T_gap_bases, strand, \
                             Q_name, Q_size, Q_start, Q_end, T_name, T_size, \
                             T_start, T_end)
    #####

    # Completed reading the file
    return

#####

#==============================================================
def maxScaffoldsHistPB (blatFile, baseName):
	merDict = {}
	x = iterCounter(1000000)
		
# 	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	tmpProcess1 = subprocess.Popen( 'cat %s '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	   lineSplit = line.split(None)
	   key = lineSplit[9]
	   try: merDict[key].add(lineSplit[13])
	   except KeyError: merDict[key] = set([lineSplit[13]])
	   x()
	tmpProcess1.poll()
		
	results = {}
	for rD in merDict:
		try: results[len(merDict[rD])] += 1
		except KeyError: results[len(merDict[rD])] = 1
		
	tmpList = []
	for res in results: tmpList.append((res,results[res]))
	tmpList.sort()
	
	results = tmpList
	
	dataOut = '%s.NumMerHitsOnPB.dat'%baseName
	dO = open('%s'%dataOut,'w')
	oh_cmdLog = open('%s.cmdLog'%baseName,'w')
	
	for res in results:
		dO.write('%d\t%d\n'%(res[0],res[1]))
	dO.close()
	
	plotDatBlat (baseName, dataOut, oh_cmdLog)
	oh_cmdLog.close()
	
#==============================================================
def maxScaffoldsScatter (blatFile, fastaFile, assemblyFreqs, baseName):
	merDict = {}
	x = iterCounter(1000000)
		
# 	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	   lineSplit = line.split(None)
	   key = lineSplit[9]
	   try: merDict[key].add(lineSplit[13])
	   except KeyError: merDict[key] = set([lineSplit[13]])
	   x()
	tmpProcess1.poll()
	
	merDict2 = {}
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(fastaFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	   if line[0] == '>': key = line.split(None)[0][1:]
	   else: 
		   currSeq = line.split(None)[0]
		   try: merDict2[currSeq] = len(merDict[key])
		   except KeyError: pass
	   x()
	tmpProcess1.poll()
	
	x = iterCounter(1000000)
	resultsDict = {}
	tmpProcess1 = subprocess.Popen( 'cat %s '%(assemblyFreqs), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	   lineSplit = line.split(None)
	   key = lineSplit[0]; currFreq = int(lineSplit[1])
	   x()
	   if currFreq > 50: continue
	   try: resultsDict[merDict2[key]] += 1.0
	   except KeyError: resultsDict[merDict2[key]] = 1.0
	   total += 1.0
	tmpProcess1.poll()
	
	results = []
	for rD in resultsDict: results.append((rD,resultsDict[rD]/total))
	results.sort()
	
	dataOut = '%s.NumMerHitsOnPBaAssemblyHist.dat'%baseName
	dO = open('%s'%dataOut,'w')
	oh_cmdLog = open('%s.cmdLog'%baseName,'w')
	
	for res in results:
		dO.write('%d\t%d\n'%(res[0],res[1]))
	dO.close()
	
	plotDatBlat (baseName, dataOut, oh_cmdLog)
	oh_cmdLog.close()
	
#==============================================================
def eliminateUnreliableMers (blatFile, accepterMarkers, modeNumber, baseName):
	merDict = {}
	x = iterCounter(1000000)
# 	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	tmpProcess1 = subprocess.Popen( 'cat %s '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	   lineSplit = line.split(None)
	   key = lineSplit[9]
	   try: merDict[key].add(lineSplit[13])
	   except KeyError: merDict[key] = set([lineSplit[13]])
	   x()
	tmpProcess1.poll()
		
	reliable = '%s.Reliable.dat'%baseName
	rel = open('%s'%reliable,'w')
	
	unreliable = set()
		
	for merd in merDict:
	   if len(merDict[merd]) <= 3 * modeNumber: continue
	   unreliable.add(merd)
	   
	print len(unreliable); print unreliable
			
	for aM in open('%s'%accepterMarkers):
		mer = aM.split(None)[0]
		if mer in unreliable: continue
		rel.write('%s'%aM)
		
	rel.close()
	
#==============================================================
def explicitTwoXSelection (samFile, baseName):
	initial = 1
	acceptedMarkers = []
	goodMers = '%s.acceptedMers.dat'%baseName
# 	goodMarkers = '%s.acceptedMarkerNames.dat'%baseName
	gM = open('%s'%goodMers,'w')
# 	gMa = open('%s'%goodMarkers,'w')
	for line in open('%s'%samFile):
	   lineSplit = line.split(None)
	   if initial == 1: 
	       acceptedMarkers.append('%s|%s|%s'%(lineSplit[0],lineSplit[2],lineSplit[3]))
	       acceptablePos = int(lineSplit[3]) + 50
	       initial = 0
	       scaffold = lineSplit[2]
	   else:
	       if lineSplit[2] != scaffold: 
        	   acceptablePos = 0
        	   scaffold = lineSplit[2]
	       if int(lineSplit[3]) <= acceptablePos: continue
	       acceptedMarkers.append('%s|%s|%s'%(lineSplit[0],lineSplit[2],lineSplit[3]))
	       acceptablePos = int(lineSplit[3]) + 50
			
	for aM in acceptedMarkers:
	   gM.write('%s\n'%aM.split('|')[0])
# 	   gMa.write('%s\n'%aM)
	gM.close()
	return acceptedMarkers
# 	gMa.close()

#==============================================================
def removeFoundTriangles (trianglesFile, tooLargeFile, baseName):

	triangleSet = set()
	for line in open('%s'%trianglesFile):
		lineSplit = line.split(None)
		triangleSet.add(lineSplit[9])
	
	for line in open('%s'%tooLargeFile):
		lineSplit = line.split(None)
		if lineSplit[9] not in triangleSet: print line[:-1]
		
#==============================================================
def removeBlatHits (blatFile, tooLargeFile, baseName):

	removeSet = set([line.split(None)[0] for line in open('%s'%tooLargeFile)])

	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		if line.split(None)[13] not in removeSet: print line[:-1]
		
#==============================================================		
def maybeGapCrosser (dataFile, scaffoldPairFasta, baseName):
    gapsDict = {}
    for y in iterFASTA(open('%s'%scaffoldPairFasta)):
		record = y; scaffold = str(y).split()[1]
		pullGaps = re.compile( r'([GATC]+)' ).finditer
		for item in pullGaps(str(record.seq)): 
		    start, stop = item.span()
		    try: gapsDict[scaffold].append((start,stop))
		    except KeyError: gapsDict[scaffold] = [(start,stop)]
    
    Trees = {}; contigs = {}
    for pT in gapsDict: Trees[pT] = IntervalTree([Interval(int(window [0]),int(window [1])) for window in gapsDict[pT]])
    for pT in gapsDict: contigs[pT] = [int(window [0]) for window in gapsDict[pT]]
    for cont in contigs: contigs[cont].sort()
    
    x = iterCounter(1000000); pbDict = {}; counter = 1; linesDict = {}
    tmpProcess1 = subprocess.Popen('cat %s | bunzip2 -c | tail -n+2'%(dataFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    x()
	    if line[0] != '=': 
		    try: linesDict[counter].append(line)
		    except KeyError: linesDict[counter] = [line]
	    else: counter+=1
	    
    x = iterCounter(1000000)
    for lD in linesDict: 
	    checkSet = {}; Mmers = {}; mers = set(); tmpList = {}
	    for line in linesDict[lD]:
	        x()
	        lineSplit = line.split(None); pos = float(lineSplit[1]); pb = lineSplit[2]; mer = lineSplit[0]
	        if mer[0] == '*': mer = mer[2:]
	        try: check = Trees[mer].search(pos)
	        except KeyError: check =[]
	        if len(check) < 1: continue
	        try: checkSet[mer][check[0].get_begin()] += 1
	        except KeyError: 
		        try: checkSet[mer][check[0].get_begin()] = 1
		        except KeyError: checkSet[mer] = {check[0].get_begin():1}
	        try: Mmers[mer] += 1
	        except KeyError: Mmers[mer] = 1
	    for mMmers in Mmers:
	        if Mmers[mMmers] > 3: mers.add(mMmers)
	    for mMers in mers:
		    a = 0
		    while a < len(contigs[mMers]):
			    try: 
				    numberMark = checkSet[mMers][contigs[mMers][a]]
				    try: numberMark2 = checkSet[mMers][contigs[mMers][a+1]]
				    except IndexError: a+=1; continue
				    if numberMark > 1 and numberMark2 > 1: 
					    try: pbDict[mMers].append(pb)
					    except KeyError: pbDict[mMers] = [pb]
			    except KeyError: a += 1; continue
			    a += 1
		
    x = iterCounter(10000)
    results = set()
    for pbd in pbDict:
        x()
        if len(pbDict[pbd]) < 2: continue
        for mpbd in pbDict[pbd]: results.add((mpbd,pbd.split('|')[0]))
		
    resultsOut = '%s.maybeGapCrosser.dat'%baseName
    rO = open('%s'%resultsOut,'w')
    x = iterCounter(10000)		
    for combos in list(results): outString = '%s\t%s\n'%(combos[0],combos[1]); rO.write('%s'%outString); x()
    rO.close()
    
#==============================================================
def pullGapCrossers (refFasta, readsFasta, blatFile, baseName):

	#awk '$1 > $11 * 0.24 {print}'
	print blatFile

	preTrees = {}; blatLines = []
	x = iterCounter(100000)
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	    lineSplit = line.split(None); blatLines.append(line); x()
	    try: trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    except IndexError: continue
	    qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
	    if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
	    if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
	    if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
	    if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
	    try: preTrees[lineSplit[13]].append((trueStart,trueEnd))
	    except KeyError: preTrees[lineSplit[13]] = [(trueStart,trueEnd)]
	    
	Trees = {}
	for pT in preTrees: Trees[pT] = IntervalTree([Interval(int(window [0]),int(window [1])) for window in preTrees[pT]])
	    
	outDict = {}; outDict2 = {}; outSet = set(); outData = []; gapsList = []
	for y in iterFASTA(open('%s'%refFasta)):
		record = y
		pullGaps = re.compile( r'([N]+)' ).finditer
		scaffold = str(y).split()[1]; scaff_seq = str(record.seq)
		for item in pullGaps(str(record.seq)): 
		    begin, end = item.span(); outDict[(begin,end)] = {}
		    try: check = Trees[scaffold].search(begin)
		    except KeyError: check =[]
		    try: check2 = Trees[scaffold].search(end)
		    except KeyError: check2 =[]
		    if check != []:
			    for che in check: outDict[(begin,end)][(che.begin,che.end)] = 1
		    if check2 != []:
			    for che in check2:
				    try: outDict[(begin,end)][(che.begin,che.end)] += 1; outDict2[(che.begin,che.end)] = (begin,end); outSet.add((che.begin,che.end))
				    except KeyError: continue
	
	resultsDict = {}; readThreshold = 200
	for line in blatLines: 
	    lineSplit = line.split(None)
	    try: trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    except IndexError: continue
	    goodTuple = (trueStart, trueEnd)
	    if goodTuple in outSet:
	       readStart = int(lineSplit[11]) + (outDict2[(trueStart,trueEnd)][0] - int(lineSplit[15]))
	       readEnd = readStart + (outDict2[(trueStart,trueEnd)][1] - outDict2[(trueStart,trueEnd)][0])
	       if outDict2[(trueStart,trueEnd)][0] - readThreshold < trueStart: continue
	       if outDict2[(trueStart,trueEnd)][1] + readThreshold > trueEnd: continue
	       if 2.0 * (float(lineSplit[12]) - float(lineSplit[11])) < trueEnd - trueStart: continue
	       outData.append('%s\t%d\t%d\t%s\t%d\t%d\t%d\t%d'% 
(lineSplit[9],int(lineSplit[11]),int(lineSplit[12]),lineSplit[13],int(lineSplit[15]),int(lineSplit[16]),outDict2[(trueStart,trueEnd)][0],outDict2[(trueStart,trueEnd)][1]))
	       
	return (outData, blatLines)
	
#==============================================================
def fillGaps( readsFasta, targetFasta, mapFile, blatFile, blatLines, currDir, baseName):	

	gapDict = {}; readDict = {}; readDict2 = {}
# 	for line in open('testOutMulti.dat'):
	for line in mapFile:
	    lineSplit = line.split(None)
	    try: gapDict[(lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))].append(lineSplit[0])
	    except KeyError: gapDict[(lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))] = [lineSplit[0]]
	    try: 
		    try: readDict [lineSplit[3]] [(int(lineSplit[6]),int(lineSplit[7]))].add((lineSplit[0],int(lineSplit[4]),int(lineSplit[5])))
		    except KeyError: readDict [lineSplit[3]] [( int(lineSplit[6]),int(lineSplit[7]))] = set([(lineSplit[0],int(lineSplit[4]),int(lineSplit[5]))]) 
	    except KeyError: readDict [lineSplit[3]] =  {( int(lineSplit[6]),int(lineSplit[7])): set([(lineSplit[0],int(lineSplit[4]),int(lineSplit[5]))]) }
	    readDict2[(lineSplit[0],lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))] =  int(lineSplit[2])-int(lineSplit[1])
	
	print len(readDict), len(readDict2)
	
	blatPos = {}; blatDict = {};  x = iterCounter(100000)
	for line in blatLines: 
	    lineSplit = line.split(None); x()
	    try: trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    except IndexError: continue
	    qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
	    if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
	    if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
	    if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
	    if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
	    try: blatPos[lineSplit[9]].append((trueStart, trueEnd, qtrueStart, qtrueEnd))
	    except KeyError: blatPos[lineSplit[9]] = [(trueStart, trueEnd, qtrueStart, qtrueEnd)] 
	    blatDict[(lineSplit[9],trueStart,trueEnd,qtrueStart,qtrueEnd)] = line
	    
	multiDict = {}  
	for rD in readDict:
		if len(readDict[rD]) < 2: continue 
		tmpGaps = readDict[rD].keys(); tmpGaps.sort(); a = 1; supers = []
		while a < len(tmpGaps):
			if tmpGaps[a-1][1] + 2000 > tmpGaps[a][0]: supers.append(a-1); supers.append(a)
			a += 1
		superList = sorted(supers); b = 1; superGaps = []; tmpSgap = set()
		while b < len(superList):
			if superList[b-1] == superList[b]-1: tmpSgap.add(superList[b-1]); tmpSgap.add(superList[b])
			else: superGaps.append(list(tmpSgap)); tmpSgap = set()
			b += 1
		if len(tmpSgap) > 0: superGaps.append(list(tmpSgap))
		if len(superGaps) < 1: continue
		for sG in superGaps: 
			sG.sort()
			multiDict[(rD,tmpGaps[sG[0]][0],tmpGaps[sG[-1]][1])] = set()
			for mRD in readDict[rD]:
				if mRD[0] <  tmpGaps[sG[0]][0]: continue
				if mRD[1] > tmpGaps[sG[-1]][1]: continue
				for mMRD in readDict[rD][mRD]: multiDict[(rD,tmpGaps[sG[0]][0],tmpGaps[sG[-1]][1])].add((mMRD[0], mMRD[1], mMRD[2]))
				
# 	appTO = open('testOutMulti.dat','a')
	for mD in multiDict:
	    		for mMD in multiDict[mD]: mapFile.append('%s\t0\t0\t%s\t%d\t%d\t%d\t%d'%(mMD[0],mD[0],mMD[1],mMD[2],mD[1],mD[2]))
    		
# 	appTO.close()
	
	targetDict = {}; x = iterCounter(100000)
	for z in iterFASTA(open('%s'%targetFasta)): targetDict[str(z).split()[1]] = str(z.seq); x()
	
	print len(gapDict), len(multiDict)
	
	revGapDict = {}
	for gD in gapDict:
	    tmpSet = set(gapDict[gD])
	    for tS in list(tmpSet):
		    try: revGapDict[tS].append(gD)
		    except KeyError: revGapDict[tS] = [gD]
	
	revMultiDict = {}
	for gD in multiDict:
	    tmpSet = set([zz[0] for zz in multiDict[gD]])
	    for tS in list(tmpSet):
		    try: revMultiDict[tS].append(gD)
		    except KeyError: revMultiDict[tS] = [gD]
	
	gapSet = set([rGD for rGD in revGapDict]); multiSet = set([rMD for rMD in revMultiDict])
	
	xx = iterCounter(100000); gapReadsDict = {}; multiReadsDict = {}
	for z in iterFASTQ(open('%s'%readsFasta)): 
		scaffold = z['seq']['id']; sequence = '%s\n+\n%s'%(z['seq']['seq'], z['qual']['quals']); xx()
		if scaffold in gapSet:
			for mRGD in revGapDict[scaffold]:
				try: gapReadsDict[mRGD][scaffold] = sequence
				except KeyError: gapReadsDict[mRGD] = {scaffold:sequence}
		if scaffold in multiSet:
			for mMGD in revMultiDict[scaffold]:
				try: multiReadsDict[mMGD][scaffold] = sequence
				except KeyError: multiReadsDict[mMGD] = {scaffold:sequence}
	x = 1
	for gD in gapDict:
	    tmpSet = set(gapDict[gD])
	    if len(tmpSet) < 2: continue
	    readNames = list(tmpSet); targetName = gD[0]; gapStart = gD[1]; gapEnd = gD[2]
	    readsDictTmp = gapReadsDict[gD]
# 	    if x > 5: x += 1; continue
# 	    print targetName, gapStart, gapEnd
	    callGapConsensusGP( readsDictTmp, readNames, targetDict, targetName, baseName, gapStart, gapEnd, blatPos, blatDict, currDir)
	    x += 1
	print x
	y = 1
	for mD in multiDict:
	    tmpSet = [mMD[0] for mMD in multiDict[mD]]; tmpSet= set(tmpSet)
	    if len(tmpSet) < 3: continue
	    readNames = list(tmpSet); targetName = mD[0]; gapStart = mD[1]; gapEnd = mD[2]
	    readsDictTmp = multiReadsDict[mD]
# 	    if y > 1: y += 1; continue
# 	    print targetName, gapStart, gapEnd
	    callGapConsensusGP ( readsDictTmp, readNames, targetDict, targetName, baseName, gapStart, gapEnd, blatPos, blatDict, currDir)
	    y += 1
	print x, y
	
	return mapFile
	
#==============================================================
	
def passThroughGaps( readsFasta, targetFasta, mapFile, blatFile, blatLines, baseName):	

	gapDict = {}; readDict = {}; readDict2 = {}
	for line in mapFile:
	    lineSplit = line.split(None)
	    try: gapDict[(lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))].append(lineSplit[0])
	    except KeyError: gapDict[(lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))] = [lineSplit[0]]
	    try: 
		    try: readDict [lineSplit[3]] [(int(lineSplit[6]),int(lineSplit[7]))].add((lineSplit[0],int(lineSplit[4]),int(lineSplit[5])))
		    except KeyError: readDict [lineSplit[3]] [( int(lineSplit[6]),int(lineSplit[7]))] = set([(lineSplit[0],int(lineSplit[4]),int(lineSplit[5]))]) 
	    except KeyError: readDict [lineSplit[3]] =  {( int(lineSplit[6]),int(lineSplit[7])): set([(lineSplit[0],int(lineSplit[4]),int(lineSplit[5]))]) }
	    readDict2[(lineSplit[0],lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))] =  int(lineSplit[2])-int(lineSplit[1])
	
	blatPos = {}; blatDict = {};  x = iterCounter(100000)
	for line in blatLines: 
	    lineSplit = line.split(None); x()
	    trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
	    if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
	    if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
	    if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
	    if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
	    try: blatPos[lineSplit[9]].append((trueStart, trueEnd, qtrueStart, qtrueEnd))
	    except KeyError: blatPos[lineSplit[9]] = [(trueStart, trueEnd, qtrueStart, qtrueEnd)] 
	    blatDict[(lineSplit[9],trueStart,trueEnd,qtrueStart,qtrueEnd)] = line
	    
	multiDict = {}  
	for rD in readDict:
		if len(readDict[rD]) < 2: continue 
		tmpGaps = readDict[rD].keys(); tmpGaps.sort(); a = 1; supers = []
		while a < len(tmpGaps):
			if tmpGaps[a-1][1] + 2000 > tmpGaps[a][0]: supers.append(a-1); supers.append(a)
			a += 1
		superList = sorted(supers); b = 1; superGaps = []; tmpSgap = set()
		while b < len(superList):
			if superList[b-1] == superList[b]-1: tmpSgap.add(superList[b-1]); tmpSgap.add(superList[b])
			else: superGaps.append(list(tmpSgap)); tmpSgap = set()
			b += 1
		if len(tmpSgap) > 0: superGaps.append(list(tmpSgap))
		if len(superGaps) < 1: continue
		for sG in superGaps: 
			sG.sort()
			multiDict[(rD,tmpGaps[sG[0]][0],tmpGaps[sG[-1]][1])] = set()
			for mRD in readDict[rD]:
				if mRD[0] <  tmpGaps[sG[0]][0]: continue
				if mRD[1] > tmpGaps[sG[-1]][1]: continue
				for mMRD in readDict[rD][mRD]: multiDict[(rD,tmpGaps[sG[0]][0],tmpGaps[sG[-1]][1])].add((mMRD[0], mMRD[1], mMRD[2]))
				
	for mD in multiDict:
	    		for mMD in multiDict[mD]: mapFile.append('%s\t0\t0\t%s\t%d\t%d\t%d\t%d'%(mMD[0],mD[0],mMD[1],mMD[2],mD[1],mD[2]))
    		
	
	print 'Num regular:\t%d'%len(gapDict)
	print 'Num special combined:\t%d'%len(multiDict)
	
	readsWithGaps = {}
	
	x = 1
	for gD in gapDict:
	    tmpSet = set(gapDict[gD])
	    if len(tmpSet) < 2: continue
	    readNames = list(tmpSet); targetName = gD[0]; gapStart = gD[1]; gapEnd = gD[2]
# 	    print targetName, gapStart, gapEnd, len(readNames)
# 	    for rN in readNames: print rN
	    readsWithGaps[(targetName,gapStart,gapEnd)] = readNames
	    x += 1
	y = 1
	for mD in multiDict:
	    tmpSet = [mMD[0] for mMD in multiDict[mD]]; tmpSet= set(tmpSet)
	    if len(tmpSet) < 3: continue
	    readNames = list(tmpSet); targetName = mD[0]; gapStart = mD[1]; gapEnd = mD[2]
# 	    print targetName, gapStart, gapEnd, len(readNames)
# 	    for rN in readNames: print rN
	    readsWithGaps[(targetName,gapStart,gapEnd)] = readNames
	    y += 1
    
	print x, y
	return mapFile, readsWithGaps
    
#==============================================================
def makeDummySam ( dummySam, readNames, gapStart, gapEnd, readsDict, blatPos, blatDict, targetName, targetDict, baseName):

	system('/mnt/local/EXBIN/extract_seq_and_qual -o %s.reads.fasta %s.reads.fastq'%(baseName, baseName))
	
	system('/mnt/local/EXBIN/blat -minMatch=3 -tileSize=8 -maxIntron=10000 -repMatch=10000 -noHead -minIdentity=75 -extendThroughN  %s.target.fasta %s.reads.fasta stdout | awk \'{ if (($13 
- $12) > 200) print }\' | awk \'$1 > 500 {print}\' > %s.tmp.psl'%(baseName, baseName, baseName))
	
	pslOut = '%s.tmp2.psl'%baseName; pO = open('%s'%pslOut,'w')
	numHits = {}
	for line in open('%s.tmp.psl'%baseName):
		lineSplit = line.split(None)
		trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
# 		if trueStart > gapStart - 300: continue
# 		if trueEnd <  gapStart + 700: continue
		currHits = trueEnd - trueStart
		currRead = lineSplit[9]
		try: numHits[currRead].append((currHits,line.split('\t'),trueStart,trueEnd))
		except KeyError: numHits[currRead] = [(currHits,line.split('\t'),trueStart,trueEnd)]
		
	for nH in numHits:
		tmpList = [tL for tL in numHits[nH]]; tmpList.sort()
		pO.write('%s\t%d\t%s'%('\t'.join(tmpList[-1][1][:15]),tmpList[-1][2],'\t'.join(tmpList[-1][1][16:])))
	pO.close()
	
	placementDict = {}
	for line in open('%s'%pslOut):
		lineSplit = line.split(None)
		placementDict[lineSplit[9]] = int(lineSplit[15])
		
	return placementDict

# 	system('/opt/samtools/misc/psl2sam.pl %s.tmp2.psl > %s.tmp.sam'%(baseName, baseName))
# 	
# 	blatPos = {}; placement  = {}; netMatches = {}; misses = {}
# 	for line in open('%s.tmp2.psl'%baseName):
# 		lineSplit = line.split(None)
# 		trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
# 		qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 
)
# 		if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
# 		if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
# 		bP = (lineSplit[9])
# 		placement[bP] = (trueStart,trueEnd,qtrueStart,qtrueEnd)
# 		netMatches[bP] = int(lineSplit[0]) - int(lineSplit[1])
# 		misses[bP] = trueEnd - trueStart
# 		
# # 	print placement
# 	
# 	quivSam = '%s.sam'%baseName; qS = open('%s'%quivSam,'w')
# 	
qS.write('@HD\tVN:1.3.1\n@SQ\tSN:dummyName\tLN:%d\tM5:ef2fe07ae23f6310a5632782f1447b56\n@RG\tID:984c5db329\tPU:GlobTest.reads.fastq\tSM:GlobTest.reads.fastq\n'%(len(targetDict[targetName])))
# 	qS.write('@PG\tID:BLASR\tVN:1.3.1.127046\tCL:/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/blasr -sam -out GlobTest.sam GlobTest.reads.fastq GlobTest.target.fasta 
-nproc 4 -minMatch 8\n')
# 	for line in open('%s.tmp.sam'%baseName):
# 		lineSplit = line.split(None)
# # 		print lineSplit[0]
# 		currQstart = placement[lineSplit[0]][2]; currQend = placement[lineSplit[0]][3]; currLength = currQend - currQstart
# 		currSeq = readsDict[lineSplit[0]].split('\n')[0]; currQual = readsDict[lineSplit[0]].split('\n')[2]
# 		newL = ''; newL += '%s/0_%d\t'%(lineSplit[0],len(currSeq)); a =1
# 		while a < 4:
# 			newL += '%s\t'%lineSplit[a]
# 			a += 1
# 		newL += '254\t'; a += 1
# 		while a < 8:
# 			newL+= '%s\t'%lineSplit[a]
# 			a += 1
# # 		currNM = re.findall('[MDI]',lineSplit[5])
# 		countNM = misses[lineSplit[0]]
# 		alignScore = netMatches[lineSplit[0]]*-5
# 		newL += '%d\t%s\t%s\tRG:Z:cb2446dd1a\tAS:i:%d\tXS:i:%d\tXE:i:%d\tXL:i:%d\tXT:i:1\tNM:i:%d\tFI:i:1\tXQ:i:%d\n'%(currLength, currSeq[currQstart:currQend], 
currQual[currQstart:currQend], alignScore, currQstart,currQend, currLength, countNM,len(currQual))
# 		if int(lineSplit[4]) < (gapStart - 300) and int(lineSplit[4]) + currLength > (gapEnd + 300): qS.write('%s'%newL)
# 		qS.write('%s'%newL)
# 	qS.close()

# 	currentNames = set(readNames)
# 	    
# 	selectPSL = '%s.tmp.psl'%baseName; sP =open('%s'%selectPSL,'w'); placement  = {}; netMatches = {}; misses = {}
# 	for bP in blatPos:
# 		if bP not in currentNames: continue
# 		tmpDict = {}; tmpList = []
# 		for mBP in blatPos[bP]: 
# 			tmpDict[mBP[1]-mBP[0]] = mBP
# 			tmpList.append(mBP[1]-mBP[0])
# 		tmpList.sort(); sP.write('%s'%blatDict[(bP,tmpDict[tmpList[-1]][0],tmpDict[tmpList[-1]][1],tmpDict[tmpList[-1]][2],tmpDict[tmpList[-1]][3])])
# 		line = blatDict[(bP,tmpDict[tmpList[-1]][0],tmpDict[tmpList[-1]][1],tmpDict[tmpList[-1]][2],tmpDict[tmpList[-1]][3])]
# 		lineSplit = line.split(None)
# 		placement[bP] = (tmpDict[tmpList[-1]][0],tmpDict[tmpList[-1]][1],tmpDict[tmpList[-1]][2],tmpDict[tmpList[-1]][3])
# 		netMatches[bP] = int(lineSplit[0]) - int(lineSplit[1])
# 		misses[bP] = int(lineSplit[1]) + int(lineSplit[5]) + int(lineSplit[7])
# 		
# 	sP.close()
# 	
# 	system('cat %s.tmp.psl | awk \'$14 = \"dummyName\" {print}\' > %s.tmp2.psl'%(baseName, baseName))
# 
# 	system('/opt/samtools/misc/psl2sam.pl %s.tmp2.psl > %s.tmp.sam'%(baseName, baseName))
# 	
# 	quivSam = '%s.sam'%baseName; qS = open('%s'%quivSam,'w')
# 	
qS.write('@HD\tVN:1.3.1\n@SQ\tSN:%s\tLN:%d\tM5:ef2fe07ae23f6310a5632782f1447b56\n@RG\tID:984c5db329\tPU:GlobTest.reads.fastq\tSM:GlobTest.reads.fastq\n'%(targetName,len(targetDict[targetName])))
# 	qS.write('@PG\tID:BLASR\tVN:1.3.1.127046\tCL:/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/blasr -sam -out GlobTest.sam GlobTest.reads.fastq GlobTest.target.fasta 
-nproc 4 -minMatch 8\n')
# 	for line in open('%s.tmp.sam'%baseName):
# 		lineSplit = line.split(None)
# 		currQstart = placement[lineSplit[0]][2]; currQend = placement[lineSplit[0]][3]; currLength = currQend - currQstart
# 		currSeq = readsDict[lineSplit[0]].split('\n')[0]; currQual = readsDict[lineSplit[0]].split('\n')[2]
# 		newL = ''; newL += '%s/0_%d\t'%(lineSplit[0],len(currSeq)); a =1
# 		while a < 4:
# 			newL += '%s\t'%lineSplit[a]
# 			a += 1
# 		newL += '254\t'; a += 1
# 		while a < 8:
# 			newL+= '%s\t'%lineSplit[a]
# 			a += 1
# # 		currNM = re.findall('[MDI]',lineSplit[5])
# 		countNM = misses[lineSplit[0]]
# 		alignScore = netMatches[lineSplit[0]]*-5
# 		newL += '%d\t%s\t%s\tRG:Z:cb2446dd1a\tAS:i:%d\tXS:i:%d\tXE:i:%d\tXL:i:%d\tXT:i:1\tNM:i:%d\tFI:i:1\tXQ:i:%d\n'%(currLength, currSeq[currQstart:currQend], 
currQual[currQstart:currQend], alignScore, currQstart,currQend, currLength, countNM,len(currQual))
# # 		if placement[lineSplit[0]][0] < (gapStart - 200) and placement[lineSplit[0]][1] > (gapEnd + 200): qS.write('%s'%newL)
# 	qS.close(); assert False
		
    
#==============================================================
def callGapConsensus( readsDict, readNames, targetDict, targetName, baseName, gapStart, gapEnd, blatPos, blatDict):
    readFastq = '%s.reads.fasta'%baseName; rF = open('%s'%readFastq,'w')
    targetFasta = '%s.preTarget.fasta'%baseName; tF = open('%s'%targetFasta,'w')
    
    for rN in readNames: rF.write('@%s\n%s\n'%(rN,readsDict[rN]))
    tF.write('>dummyName\n%s\n'%(targetDict[targetName]))
    rF.close(); tF.close()
    tmpString = ''
    for a in range(gapEnd-gapStart): tmpString += 'A'
    newTarget = '%s.target.fasta'%baseName
    nT = open('%s'%newTarget,'w')
    for z in iterFASTA(open('%s.preTarget.fasta'%baseName)):
        scaffold = str(z).split()[1]; sequence = str(z.seq); 
        before = sequence[:gapStart]; after = sequence[gapEnd:]
        sequence2 = before + tmpString + after
        break
    nT.write('>%s\n%s'%(scaffold, sequence2))
    nT.close()
    
    putGapCrosser = '%s.maybeGapCrosser.dat'%baseName; putGapBlat = '%s.gapSeeker.filtered.blat.bz2'%baseName; dummySam = '%s.sam'%baseName
#     makeDummySam ( dummySam, readNames, gapStart, gapEnd, readsDict, blatPos, blatDict, targetName, targetDict, baseName)
    quiverCommands = '%squiver'%baseName
    qC = open('%s'%quiverCommands,'w')
    qC.write('#!/bin/sh\n\n')
    qC.write('. /opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/etc/setup.sh\n\n')
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/blasr -sam -out %s.sam %s.reads.fastq %s -nproc 6 -minMatch 8 -minPctIdentity 80\n'%(baseName, baseName, 
newTarget))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/samtoh5 %s.sam %s %s.cmp.h5\n'%(baseName, newTarget, baseName))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/cmph5tools.py sort --inPlace %s.cmp.h5\n'%baseName)
    contextStart = gapStart - 500; contextEnd = gapEnd + 500
#     qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --verbose --referenceFilename %s --minCoverage 1 -C 1000000 --noEvidenceConsensusCall nocall 
--minConfidence 1 -o %s.output.fasta -o %s.variants.gff --referenceWindow %s:%d-%d\n'%(baseName, newTarget, baseName, baseName, scaffold, contextStart, contextEnd))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --verbose --referenceFilename %s -x 1 -C 10000000 --noEvidenceConsensusCall nocall 
--minConfidence 20 -o %s.output.fasta -o %s.variants.gff --referenceWindow dummyName:%d-%d\n'%(baseName, newTarget, baseName, baseName, contextStart, contextEnd))

    qC.close()
    system('chmod 777 %s'%quiverCommands)
    system('./%s'%quiverCommands)
#     system('rm %s; rm %s.target.fasta; rm %s.reads.fastq; rm %s.tmp.sam; rm %s.cmp.h5'%(quiverCommands, baseName, baseName, baseName, baseName))
    finalOutput = '%s.gapFilling.dat'%baseName; rawQuiver = '%s.rawQuiver.dat'%baseName
    fO = open('%s'%finalOutput,'a'); rQ = open('%s'%rawQuiver,'a')
    for x in iterFASTA(open('%s.output.fasta'%baseName)): consensusSequence = str(x.seq)
#     polyN = re.compile( r'([GATC])' ).search(consensusSequence)
#     if polyN: pass
#     else: assert False
    fO.write('%s\t%d\t%d\n'%(targetName, gapStart, gapEnd)); rQ.write('>%s_%d_%d\n%s\n'%(targetName, gapStart, gapEnd, consensusSequence))
    fO.write('%s\n'%consensusSequence)
    for rN in readNames: fO.write('%s\n'%rN)
    fO.close(); rQ.close()
    
#==============================================================
def callGapConsensusGP( readsDict, readNames, targetDict, targetName, baseName, gapStart, gapEnd, blatPos, blatDict, currDir):
    readFastq = '%s/%s.%s.%d.%d.reads.fastq'%(currDir,baseName,targetName,gapStart,gapEnd); rF = open('%s'%readFastq,'w')
    targetFasta = '%s/%s.%s.%d.%d.preTarget.fasta'%(currDir,baseName,targetName,gapStart,gapEnd); tF = open('%s'%targetFasta,'w')
    
    for rN in readNames: 
	    try: rF.write('@%s\n%s\n'%(rN,readsDict[rN]))
	    except KeyError: pass
    tF.write('>dummyName\n%s\n'%(targetDict[targetName]))
    rF.close(); tF.close()
    tmpString = ''
    for a in range(gapEnd-gapStart): tmpString += 'A'
    newTarget = '%s/%s.%s.%d.%d.target.fasta'%(currDir,baseName,targetName,gapStart,gapEnd)
    nT = open('%s'%newTarget,'w')
    for z in iterFASTA(open('%s/%s.%s.%d.%d.preTarget.fasta'%(currDir,baseName,targetName,gapStart,gapEnd))):
        scaffold = str(z).split()[1]; sequence = str(z.seq); 
        before = sequence[:gapStart]; after = sequence[gapEnd:]
        sequence2 = before + tmpString + after
        break
    nT.write('>%s\n%s'%(scaffold, sequence2))
    nT.close()
    
    quiverCommands = '%s/%s%s.%d.%d.quiver'%(currDir,baseName,targetName,gapStart,gapEnd)
    qC = open('%s'%quiverCommands,'w')
    qC.write('#!/bin/sh\n#$ -l ram.c=8G\n#$ -l h_rt=1:55:00\n##$ -e /dev/null\n##$ -o /dev/null\nsleep 120\n\n')
    qC.write('module load samtools\n')
    qC.write('module load smrtanalysis/2.2.0\n\n')
    qC.write('dir=%s\n\n'%currDir)
    qC.write('samtools faidx %s\n\n'%newTarget)
#     qC.write('filter_plsh5.py --filter=ReadWhitelist=$dir/%s.%s.%d.%d.testWhitelist.txt --trim=True --outputDir=$dir/%s.%s.%d.%d.filtered_regions 
--outputSummary=$dir/%s.%s.%d.%d.filtered_summary.csv --outputFofn=$dir/filtered_regions.%s.%s.%d.%d.fofn $dir/input.fofn\n\n'%(baseName,targetName,gapStart,gapEnd, 
baseName,targetName,gapStart,gapEnd,baseName,targetName,gapStart,gapEnd,baseName,targetName,gapStart,gapEnd))
#     qC.write('pbalign.py --tmpDir $dir --forQuiver --regionTable $dir/filtered_regions.%s.%s.%d.%d.fofn --nproc 6 --algorithmOptions "-minMatch 8 -minPctIdentity 60 -useQuality" 
$dir/input.fofn %s $dir/%s.%s.%d.%d.cmp.h5\n\n'%(baseName,targetName,gapStart,gapEnd,newTarget,baseName,targetName,gapStart,gapEnd))
    qC.write('blasr -sam -out $dir/%s.%s.%d.%d.sam %s %s -nproc 6 -minMatch 8 -minPctIdentity 70\n'%(baseName, targetName,gapStart,gapEnd, readFastq, newTarget))
    qC.write('samtoh5 $dir/%s.%s.%d.%d.sam %s $dir/%s.%s.%d.%d.cmp.h5\n'%(baseName,targetName,gapStart,gapEnd, newTarget, baseName,targetName,gapStart,gapEnd))
    qC.write('cmph5tools.py sort --inPlace $dir/%s.%s.%d.%d.cmp.h5\n'%(baseName,targetName,gapStart,gapEnd))
    contextStart = gapStart - 500; contextEnd = gapEnd + 500
    qC.write('quiver $dir/%s.%s.%d.%d.cmp.h5 --verbose --referenceFilename %s -x 1 -C 10000000 --noEvidenceConsensusCall nocall --minConfidence 20 -o $dir/%s.%s.%d.%d.output.fasta -o 
$dir/%s.%s.%d.%d.variants.gff --referenceWindow dummyName:%d-%d\n'%(baseName,targetName,gapStart,gapEnd, newTarget, baseName,targetName,gapStart,gapEnd, baseName,targetName,gapStart,gapEnd, 
contextStart, contextEnd))

    qC.close()
    system('chmod 777 %s'%(quiverCommands))
    
#==============================================================
def cleanUpGapsFile (gapsFile, blatsFile, readsFasta, mapFile, assemblyFasta, currDir, baseName):
	results  = {}; preBlats  = set(); mapping = {}; blatKeys = {}; consensus = {}; readSeq = {}; blatSet = set(); usedReads = set()
	indexedFASTA = FASTAFile_dict('%s'%assemblyFasta)
	for line in mapFile:
# 	for line in open('testOutMulti.dat'):
		lineSplit = line.split(None)
		usedReads.add(lineSplit[0])
		try: mapping[(lineSplit[3], int(lineSplit[6]), int(lineSplit[7]))].add(lineSplit[0])
		except KeyError: mapping[(lineSplit[3], int(lineSplit[6]), int(lineSplit[7]))] = set([lineSplit[0]])
		blatKeys[(lineSplit[0], lineSplit[3], int(lineSplit[4]), int(lineSplit[5]))] = (lineSplit[3],int(lineSplit[6]),int(lineSplit[7]))
	
	for line in open('%s'%gapsFile):
		if line[0] == 's': lineSplit = line.split(None); key = (lineSplit[0], int(lineSplit[1]), int(lineSplit[2])); consensus[key] = []
# 		if line[0] == 'C' and line[1] == 'h': lineSplit = line.split(None); key = (lineSplit[0], int(lineSplit[1]), int(lineSplit[2])); consensus[key] = []
		elif line[0] == 'm': continue
		else: consensus[key] = line[:-1]
		
	rCount = iterCounter(1000000)
	for z in iterFASTQ(open('%s'%readsFasta)): 
		rCount()
		if z['seq']['id'] not in usedReads: continue
		readSeq[z['seq']['id']] = z['seq']['seq']
	# for z in iterFASTA(open('%s'%readsFasta)): scaffold = str(z).split()[1]; sequence = str(z.seq); readSeq[scaffold] = sequence; rCount()
	
	bCount = iterCounter(1000000); blatMappings = {}
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatsFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		lineSplit = line.split(None)
		try: blatMappings[blatKeys[(lineSplit[9], lineSplit[13], int(lineSplit[15]), int(lineSplit[16]))]].append(lineSplit[0:17])
		except KeyError: 
			try: blatMappings[blatKeys[(lineSplit[9], lineSplit[13], int(lineSplit[15]), int(lineSplit[16]))]] = [lineSplit[0:17]]
			except KeyError: continue
		bCount()
		
	print len(consensus)
		
	contexts = {}
	for cS in consensus: record = indexedFASTA[cS[0]]; start = cS[1] - 300; end = cS[2] + 300; partSeq = str(record.seq[start:end]); partSeq = re.sub(r'N+', '...NNN...', partSeq); 
contexts[cS] = partSeq
		
	outFile = '%s/%s.gapFillingLong.dat'%(currDir,baseName); oF = open('%s'%outFile,'w')
	for cS in consensus:
		if consensus[cS] == '': continue
		polyF = re.compile( r'([GATC])' ).search(consensus[cS])
		if polyF: pass
		else: continue
# 		polyN = re.compile( r'([N])' ).search(consensus[cS])
		oF.write('#########################################################\n')
		oF.write('GAP\t%s\t%d\t%d\nORIGINAL\t%s\nCONSENSUS\t%s\n'%(cS[0],cS[1],cS[2],contexts[cS],consensus[cS]))
		try:
			for bM in blatMappings[cS]:
				for mbM in bM: oF.write('%s\t'%mbM)
				oF.write('\n')
		except KeyError: print cS
		try:
			for mA in mapping[cS]: oF.write('>%s\n%s\n'%(mA, readSeq[mA]))
		except KeyError: print cS
	oF.write('#########################################################\n')
	oF.close()
	
#==============================================================
def cleanGP (readsWithGaps, GPfile, currDir, baseName):
		
	outFile = '%s/%s.gapFilling.dat'%(currDir, baseName); oF = open('%s'%outFile,'w')
	for y in iterFASTA(open('%s'%GPfile)):
			gap = str(y).split()[1]
			sequence = str(y.seq)	   
# 			tmpGapTup = (gap.split('_')[0],int(gap.split('_')[1]),int(gap.split('_')[2]))
			tmpGapTup = ('%s_%s'%(gap.split('_')[0],gap.split('_')[1]),int(gap.split('_')[2]),int(gap.split('_')[3]))
			#print tmpGapTup
			#for rWG in readsWithGaps: print rWG
			#assert False
			try:
				allReads=('\n').join(readsWithGaps[tmpGapTup])
				oF.write('%s\t%d\t%d\n%s\n%s\n'%(tmpGapTup[0],tmpGapTup[1],tmpGapTup[2],sequence,allReads))
			except KeyError: pass
	oF.close()
	
#==============================================================
def checkEmptyGaps (gapsFile, targetFasta, baseName):
	targetGaps = []
	y = 0
	for line in open('%s'%gapsFile):
		lineSplit = line.split(None)
		if lineSplit[0] != "GAP" and lineSplit[0] != "CONSENSUS": continue
		if lineSplit[0] == "GAP": tmpGap = (lineSplit[1],int(lineSplit[2]),int(lineSplit[3]))
		else: 
			pullEmpty = re.compile( r'([a]+)' ).findall
			try: 
				if len(pullEmpty(str(lineSplit[1]))) > 0: targetGaps.append(tmpGap)
			except IndexError: print tmpGap
			
	tmpPreDat = '%s.tmpPre.dat'%baseName
	tmpTargetDat = '%s.tmpTarget.dat'%baseName
	tmpReadDat = '%s.tmpRead.dat'%baseName
	
	blatOut = '%s.ContigToContig.blat'%baseName
	bO =open('%s'%blatOut,'w')
	alignmentFilter = "awk \'{ if ($1 > (200)) print }\'"
	
	for tG in targetGaps:
		tPD = open('%s'%tmpPreDat,'w')
		tTD = open('%s'%tmpTargetDat,'w')
		tRD = open('%s'%tmpReadDat,'w')
		tPD.write('%s'%tG[0])
		tPD.close()
		system('/mnt/local/EXBIN/extract_seq_and_qual -i %s -o %s.fasta %s'%(tmpPreDat, tmpPreDat, targetFasta))
		for y in iterFASTA(open('%s.fasta'%tmpPreDat)):
			scaffold = str(y).split()[1]
			sequence = str(y.seq)	    
			pullContigs = re.compile( r'([GATC]+)' ).finditer
			for item in pullContigs(str(sequence)): 
				start, stop = item.span()
				if stop == tG[1]: 
					if stop - start <= 10000: tRD.write('%s@%d-%d'%(tG[0], start + 1, stop)); rstart = start; rstop=stop
					else: rstart = stop - 9999; rstop = stop; tRD.write('%s@%d-%d'%(tG[0], rstart, rstop))
				if start ==  tG[2]: tTD.write('%s@%d-%d'%(tG[0], start + 1, stop)); tstart = start; tstop= stop
			tTD.close(); tRD.close()
			targetFasta2 = '%s.%d.%d.fasta'%(tG[0], tstart, tstop); readFasta = '%s.%d.%d.fasta'%(tG[0], rstart, rstop)
			system('/mnt/local/EXBIN/extract_seq_and_qual -i %s -o %s %s.fasta'%(tmpTargetDat, targetFasta2, tmpPreDat))
			system('/mnt/local/EXBIN/extract_seq_and_qual -i %s -o %s %s.fasta'%(tmpReadDat, readFasta, tmpPreDat))
			system('/mnt/local/EXBIN/blat -noHead -minMatch=1 -tileSize=8 -repMatch=20000 %s %s stdout | %s >> %s'%(targetFasta2, readFasta, alignmentFilter, blatOut))
		system('rm %s; rm %s'%(targetFasta2, readFasta))
			
	bO.close()

#==============================================================
def checkEmptyGaps2 (blatFile, baseName):

	results = {}
	for line in open('%s'%blatFile):
		lineSplit = line.split(None)
		trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
		qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 
)
		if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
		if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
		if int(lineSplit[0]) < int(lineSplit[5]) / 2: continue
		try: results[(lineSplit[9],lineSplit[13])].add((rounddownY(qtrueStart,1000),roundupY(qtrueEnd,1000)))
		except KeyError: results[(lineSplit[9],lineSplit[13])] = set([(rounddownY(qtrueStart,1000),roundupY(qtrueEnd,1000))])
		
	for res in results:
		if len(results[res]) > 1: print res, results[res]
			
#==============================================================
def maybeMisjoin (dataFile, readsFasta, scaffoldsFasta, baseName):
		    
    x = iterCounter(1000000); pbDict = {}; counter = 1; linesDict = {}
    tmpProcess1 = subprocess.Popen('cat %s | bunzip2 -c | tail -n+2'%(dataFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    x()
	    if line[0] != '=': 
		    try: linesDict[counter].append(line)
		    except KeyError: linesDict[counter] = [line]
	    else: counter+=1
	    
    x = iterCounter(1000000)
    for lD in linesDict:
	    x(); scaffs = {}; scaffPos = {}
	    for line in linesDict[lD]:
			lineSplit = line.split(None)
			scaff = lineSplit[0]; sP = float(lineSplit[1]); pb = lineSplit[2]; length = float(lineSplit[3]); pos = float(lineSplit[4])
			if scaff[0] == '*': scaff = scaff[2:]
			try: scaffs[scaff].append(pos)
			except KeyError: scaffs[scaff] = [pos]
			try: scaffPos[scaff].append(sP)
			except KeyError: scaffPos[scaff] = [sP]
	    results = []; inclusive = []
	    for sca in scaffs:
    		if len(scaffs[sca]) > 1: inclusive.append((len(scaffs[sca]),sca))
    		if len(scaffs[sca]) > 3: results.append((len(scaffs[sca]),sca))
	    if len(results) != 2 or len(inclusive) > 3:
    		scaffs = {}; scaffPos = {}
    		continue
	    results.sort(reverse=True)
	    aPos = [aP for aP in scaffs[results[0][1]]]; bPos = [aP for aP in scaffs[results[1][1]]]; aPos.sort(); bPos.sort()
	    if aPos[-1] < bPos[0]: 
    		scaffPos[results[0][1]].sort(); scaffPos[results[1][1]].sort()
    		key = (results[0][1], rounddown20k(scaffPos[results[0][1]][0]), roundup20k(scaffPos[results[0][1]][-1]), results[1][1], rounddown20k(scaffPos[results[1][1]][0]), 
roundup20k(scaffPos[results[1][1]][-1]))
    		revKey = (key[3], key[4], key[5], key[0], key[1], key[2])
    		try: pbDict[key].append(pb)
    		except KeyError: pbDict[key] = [pb]
    		try: pbDict[revKey].append(pb)
    		except KeyError: pbDict[revKey] = [pb]
	    if bPos[-1] < aPos[0]: 
    		scaffPos[results[0][1]].sort(); scaffPos[results[1][1]].sort()
    		key = (results[0][1], rounddown20k(scaffPos[results[0][1]][0]), roundup20k(scaffPos[results[0][1]][-1]), results[1][1], rounddown20k(scaffPos[results[1][1]][0]), 
roundup20k(scaffPos[results[1][1]][-1]))
    		revKey = (key[3], key[4], key[5], key[0], key[1], key[2])
    		try: pbDict[key].append(pb)
    		except KeyError: pbDict[key] = [pb]
    		try: pbDict[revKey].append(pb)
    		except KeyError: pbDict[revKey] = [pb]
    
    count = 0; redundSet = set()
    outputFile = '%s.maybeMisjoinList.dat'%baseName; oF = open('%s'%outputFile,'w')
    for pbD in pbDict:
	   if len(pbDict[pbD]) < 2: continue
	   if pbD in redundSet: continue
	   for mpbD in pbDict[pbD]: oF.write('%s\t%s\n%s\t%s\n'%(mpbD, pbD[0], mpbD, pbD[3]))
	   redundSet.add(pbD); redundSet.add((pbD[3],pbD[4],pbD[5],pbD[0],pbD[1],pbD[2]))
# 	   for mpbD in pbDict[pbD]: print mpbD, keys[pbD][0], keys[pbD][1], keys[pbD][2], keys[pbD][3], keys[pbD][4], keys[pbD][5]
# 	   for mpbD in pbDict[pbD]: signal = (mpbD, keys[pbD][0], keys[pbD][1], keys[pbD][2], keys[pbD][3], keys[pbD][4], keys[pbD][5]); DotPlotChecker(signal, readsFasta, scaffoldsFasta, 
baseName)
# 	   count += 1
# 	   if count == 10: break
    oF.close()
			
#==============================================================
def PBMisjoinFinder (blatFile, baseName, currDir):
        
    edgeHits = {}; x = iterCounter(100000); blatDict = {}
    tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(blatFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
        lineSplit = line.split(None)
        if int(lineSplit[10]) < 7000: continue
#         x()
#		awk '$1 > $11 * 0.24 {print}' pacbio onto scaff filter
        trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
        qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
        if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd); qtrueEnd = qtrueStart + (qtrueEnd - tmp)
#         if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
#         if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.24: continue
        if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
        if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.24: continue
        try: edgeHits[lineSplit[9]].append((lineSplit[13],0, trueStart, trueEnd,qtrueStart,qtrueEnd,int(lineSplit[10])))
        except KeyError: edgeHits[lineSplit[9]] = [(lineSplit[13],0,trueStart, trueEnd,qtrueStart,qtrueEnd,int(lineSplit[10]))]
        blatDict[(lineSplit[9],lineSplit[13],trueStart,trueEnd)] = lineSplit

    
    
    results = {}; posDict = {}; blatDict2 = {}
    for eH in edgeHits:
        sections = []; uniques = []
        sortEH = sorted(edgeHits[eH], key=lambda x: x[4])
        accepted = set()
        a = 0
        while a < len(sortEH):
	        station = sortEH[a]
	        for meH in sortEH:
		        if meH == station: continue
		        uniqStart = meH[4] - station[4]
		        if uniqStart <= meH[6]/6: continue
		        uniqReg = min(meH[5] - meH[4],meH[5] - station[5])
		        if uniqReg >=  meH[6]/3: 
			        if station[4] < meH[4]: accepted.add((station[0],0,station[2],station[3])); accepted.add((meH[0],1,meH[2],meH[3]))
			        if station[4] > meH[4]: accepted.add((station[0],1,station[2],station[3])); accepted.add((meH[0],0,meH[2],meH[3]))
	        a += 1
        tmpTups = [(x[0],x[1]) for x in accepted]
        if len(list(set(tmpTups))) != 2: continue
        for meH in accepted:
        	try: results[eH].append((meH[0],meH[1],rounddown20k(meH[2]),roundup20k(meH[3])))
        	except KeyError: results[eH] = [(meH[0],meH[1],rounddown20k(meH[2]),roundup20k(meH[3]))]
        	blatDict2[(eH, meH[0],rounddown20k(meH[2]),roundup20k(meH[3]))] = (eH,meH[0],meH[2],meH[3])
        	
# 	results = {}; posDict = {}; blatDict2 = {}
#     for eH in edgeHits:
#         sections = []; uniques = []
#         sortEH = sorted(edgeHits[eH], key=lambda x: x[4])
#         accepted = set()
#         a = 0
#         while a < len(sortEH):
# 	        station = sortEH[a]
# 	        for meH in sortEH:
# 		        if meH == station: continue
# 		        uniqReg = min(meH[5] - meH[4],meH[5] - station[5])
# 		        if uniqReg >=  meH[6]/3: accepted.add((station[0],0,station[2],station[3])); accepted.add((meH[0],1,meH[2],meH[3]))
# 	        a += 1
#         for meH in accepted:
#         	try: results[eH].append((meH[0],meH[1],rounddown20k(meH[2]),roundup20k(meH[3])))
#         	except KeyError: results[eH] = [(meH[0],meH[1],rounddown20k(meH[2]),roundup20k(meH[3]))]
#         	blatDict2[(eH, meH[0],rounddown20k(meH[2]),roundup20k(meH[3]))] = (eH,meH[0],meH[2],meH[3])
        
    outFile = '%s/%s.misjoinHits.dat'%(currDir, baseName); oF = open('%s'%outFile,'w')
    fullOutputInfo = []; edgeDict = {}; keyDict = {}
    for res in results:
        for mRes in results[res]:
            for mRes2 in results[res]:
                if mRes2[0] == mRes[0]: continue
                if mRes2[1] == mRes[1]: continue
                scaffKey = [(mRes[0],mRes[2],mRes[3]),(mRes2[0],mRes2[2],mRes2[3])]
                scaffKey.sort()
                scaffKey2 = tuple(scaffKey)
                keyDict[scaffKey2] = (mRes[0],mRes[2],mRes[3],mRes2[0],mRes2[2],mRes2[3])
                try: edgeDict[scaffKey2].add(res)
                except KeyError: edgeDict[scaffKey2] = set([res])
                
    x = 0 
    for eD in edgeDict:
	    if len(edgeDict[eD]) < 5: continue
	    startsA = []; endsA = []; startsB = []; endsB = []
	    for meD in list(edgeDict[eD]):
		    startsA.append(blatDict2[(meD,keyDict[eD][0],keyDict[eD][1],keyDict[eD][2])][2]); endsA.append(blatDict2[(meD,keyDict[eD][0],keyDict[eD][1],keyDict[eD][2])][3])
		    startsB.append(blatDict2[(meD,keyDict[eD][3],keyDict[eD][4],keyDict[eD][5])][2]); endsB.append(blatDict2[(meD,keyDict[eD][3],keyDict[eD][4],keyDict[eD][5])][3])
	    aStartA = sum(startsA)/len(startsA); aStartB = sum(startsB)/len(startsB); aEndA = sum(endsA)/len(endsA); aEndB = sum(endsB)/len(endsB)
	    oF.write('#########################################################\n')
	    oF.write('JOIN=\t%s\t%d\t%d\t%s\t%d\t%d\n'%(keyDict[eD][0],aStartA,aEndA,keyDict[eD][3],aStartB,aEndB)); 
fullOutputInfo.append('JOIN=\t%s\t%d\t%d\t%s\t%d\t%d\n'%(keyDict[eD][0],aStartA,aEndA,keyDict[eD][3],aStartB,aEndB))
	    for meD in list(edgeDict[eD]): 
	    	for bl in blatDict[blatDict2[(meD,keyDict[eD][0],keyDict[eD][1],keyDict[eD][2])]][0:17]: oF.write('%s\t'%bl)
	    	fullInfoString = ''
	    	for bl in blatDict[blatDict2[(meD,keyDict[eD][0],keyDict[eD][1],keyDict[eD][2])]]: fullInfoString += '%s\t'%bl
	    	oF.write('\n'); fullOutputInfo.append('%s'%fullInfoString)
	    	fullInfoString = ''
	    	for bl in blatDict[blatDict2[(meD,keyDict[eD][3],keyDict[eD][4],keyDict[eD][5])]][0:17]: oF.write('%s\t'%bl)
	    	for bl in blatDict[blatDict2[(meD,keyDict[eD][3],keyDict[eD][4],keyDict[eD][5])]]: fullInfoString += '%s\t'%bl
	    	oF.write('\n'); fullOutputInfo.append('%s'%fullInfoString)
	    x += 1
    oF.write('#########################################################')
    oF.close()
    return fullOutputInfo
    
#==============================================================
def contradictMaybeBlat( mapFile, maybeFile, baseName):
    maybeDict = {}; checkSet = set()
    tmpProcess1 = subprocess.Popen( 'cat %s'%(maybeFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    if line[0] != 'J': continue
	    lineSplit = line.split(None)
	    try: maybeDict[lineSplit[1]].append((float(lineSplit[2]),float(lineSplit[3])))
	    except KeyError: maybeDict[lineSplit[1]] = [(float(lineSplit[2]),float(lineSplit[3]))]
	    try: maybeDict[lineSplit[4]].append((float(lineSplit[5]),float(lineSplit[6])))
	    except KeyError: maybeDict[lineSplit[4]] = [(float(lineSplit[5]),float(lineSplit[6]))]
	    checkSet.add(lineSplit[1]); checkSet.add(lineSplit[4])
    for mD in maybeDict: maybeDict[mD].sort()
    
    linesDict = {}; counter = 1; x = iterCounter(1000000)
    tmpProcess1 = subprocess.Popen('cat %s | bunzip2 -c | tail -n+2'%(mapFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    x()
	    if line[0] != '=': 
		    try: linesDict[counter].append(line)
		    except KeyError: linesDict[counter] = [line]
	    else: counter+=1
    
    results = set(); x = iterCounter(1000000)
    for lD in linesDict:
	    scaffPos = {}
	    for line in linesDict[lD]:
	        x()
	        lineSplit = line.split(None); scaff = lineSplit[0]; pos = float(lineSplit[1]); pb = lineSplit[2]
	        if scaff[0] == '*': scaff = scaff[2:]
	        try: scaffPos[scaff].append(pos)
	        except KeyError: scaffPos[scaff] = [pos]
	    for sP in scaffPos: 
		    if sP not in checkSet: continue
		    if len(scaffPos[sP]) < 5: continue
		    scaffPos[sP].sort()
		    start = scaffPos[sP][2]; end = scaffPos[sP][-3]
		    for z in maybeDict[sP]: 
			    if start < z[0] and end > z[1]: results.add((pb,sP))
    
    outputFile = '%s.putContra.dat'%baseName; oF = open('%s'%outputFile,'w')    
    for res in results: oF.write("%s\t%s\n"%(res[0], res[1]))
    oF.close()
    
#==============================================================
def contradictMaybeBlatGP( mapFile, maybeFile, baseName):
    maybeDict = {}; checkSet = set()
    tmpProcess1 = subprocess.Popen( 'cat %s'%(maybeFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    if line[0] != 'J': continue
	    lineSplit = line.split(None)
	    try: maybeDict[lineSplit[1]].append((float(lineSplit[2]),float(lineSplit[3])))
	    except KeyError: maybeDict[lineSplit[1]] = [(float(lineSplit[2]),float(lineSplit[3]))]
	    try: maybeDict[lineSplit[4]].append((float(lineSplit[5]),float(lineSplit[6])))
	    except KeyError: maybeDict[lineSplit[4]] = [(float(lineSplit[5]),float(lineSplit[6]))]
	    checkSet.add(lineSplit[1]); checkSet.add(lineSplit[4])
    for mD in maybeDict: maybeDict[mD].sort()
    
    linesDict = {}; counter = 1; x = iterCounter(1000000); pbRead = ''; scaffPos = {}; results = set()
    tmpProcess1 = subprocess.Popen('cat %s'%(mapFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    x()
	    currScaff = line.split(None)[0].split('/')[0]
	    if currScaff not in checkSet: continue
	    currPos = int(line.split(None)[0].split('/')[1])
	    currPB = line.split(None)[1]
	    if pbRead == '': pbRead = currPB
	    elif currPB == pbRead: 
		    try: scaffPos[currScaff].append(currPos)
		    except KeyError: scaffPos[currScaff] = [currPos]
	    else:
		    for sP in scaffPos: 
			    if sP not in checkSet: continue
			    tmpList = scaffPos[sP]
			    tmpList.sort()
			    if len(tmpList) < 5: continue
			    start = tmpList[2]; end = tmpList[-3]
			    for z in maybeDict[sP]: 
				    if start < z[0] and end > z[1]: results.add((pbRead,sP))
		    pbRead = currPB
		    scaffPos = {}
    
    outputFile = '%s.putContra.dat'%baseName; oF = open('%s'%outputFile,'w')    
    for res in results: oF.write("%s\t%s\n"%(res[0], res[1]))
    oF.close()
    
#==============================================================
def misjoinContra (blatFile, mapList, baseName):

	tmpDict = {}; edgeDict = {}; edgeSet = set()
	for line in mapList:
		if line[0] == 'J': 
			lineSplit = line.split(None)
			if len(tmpDict) == 0: pass
			else: 
				signal = [2,2]
				for bD in blatDict:
					if blatDict[bD][0][1] < blatDict[bD][1][1] and blatDict[bD][0][0] == '+' and blatDict[bD][1][0] == '+': signal = [1,0]
					if blatDict[bD][0][1] < blatDict[bD][1][1] and blatDict[bD][0][0] == '+' and blatDict[bD][1][0] == '-': signal = [1,1]
					if blatDict[bD][0][1] > blatDict[bD][1][1] and blatDict[bD][0][0] == '+' and blatDict[bD][1][0] == '+': signal = [0,1]
					if blatDict[bD][0][1] > blatDict[bD][1][1] and blatDict[bD][0][0] == '+' and blatDict[bD][1][0] == '-': signal = [0,0]
					if blatDict[bD][0][1] < blatDict[bD][1][1] and blatDict[bD][0][0] == '-' and blatDict[bD][1][0] == '+': signal = [0,0]
					if blatDict[bD][0][1] < blatDict[bD][1][1] and blatDict[bD][0][0] == '-' and blatDict[bD][1][0] == '-': signal = [0,1]
					if blatDict[bD][0][1] > blatDict[bD][1][1] and blatDict[bD][0][0] == '-' and blatDict[bD][1][0] == '+': signal = [1,1]
					if blatDict[bD][0][1] > blatDict[bD][1][1] and blatDict[bD][0][0] == '-' and blatDict[bD][1][0] == '-': signal = [1,0]
				if signal[0] == 0:
					try: edgeDict[scaffA].append(tmpDict[scaffA][0])
					except KeyError: edgeDict[scaffA] = [tmpDict[scaffA][0]]
				elif signal[0] == 1:
					try: edgeDict[scaffA].append(tmpDict[scaffA][1])
					except KeyError: edgeDict[scaffA] = [tmpDict[scaffA][1]]
				if signal[1] == 0:
					try: edgeDict[scaffB].append(tmpDict[scaffB][0])
					except KeyError: edgeDict[scaffB] = [tmpDict[scaffB][0]]
				elif signal[1] == 1:
					try: edgeDict[scaffB].append(tmpDict[scaffA][1])
					except KeyError: edgeDict[scaffB] = [tmpDict[scaffA][1]]
			scaffA = lineSplit[1]; scaffB = lineSplit[4]; tmpDict = {scaffA:(int(lineSplit[2]), int(lineSplit[3])), scaffB:(int(lineSplit[5]), int(lineSplit[6]))}; blatDict = {}
		else:
			lineSplit = line.split(None)
			trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 
2 )
			qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != 
''], 2 )
			if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd); qtrueEnd = qtrueStart + (qtrueEnd - tmp)
			try: blatDict[lineSplit[9]].append((lineSplit[8], qtrueStart))
			except KeyError: blatDict[lineSplit[9]] = [(lineSplit[8], qtrueStart)]
			
	edgeHits = {}; blatMap = {}; x = iterCounter(100000)
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		lineSplit = line.split(None)
# 		x()
		#		awk '$1 > $11 * 0.24 {print}' pacbio onto scaff filter
		trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
		qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 
)
		if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd); qtrueEnd = qtrueStart + (qtrueEnd - tmp)
		if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
		if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
		try: edgeHits[lineSplit[9]].append((lineSplit[13],trueStart,trueEnd,qtrueStart,qtrueEnd,int(lineSplit[10])))
		except KeyError: edgeHits[lineSplit[9]] = [(lineSplit[13],trueStart,trueEnd,qtrueStart,qtrueEnd,int(lineSplit[10]))]
		blatMap[(lineSplit[9],lineSplit[13],trueStart,trueEnd,qtrueStart,qtrueEnd)] = lineSplit[0:17]
		
	resultsDict = {}
	for eH in edgeHits:
		try:
			for meH in edgeHits[eH]:
				for mE in edgeDict[meH[0]]:
						if meH[1] < mE - 500 and meH[2] > mE + 500: 
							tmpString = ''
							for bM in blatMap[(eH,meH[0],meH[1],meH[2],meH[3],meH[4])]: tmpString += '%s\t'%bM
							try: resultsDict[(meH[0],mE)].add(tmpString)
							except KeyError: resultsDict[(meH[0],mE)] = set([tmpString])
		except KeyError: continue
		
	contraList = []
	for resD in resultsDict:
		contraList.append('%s\t%s\n'%(resD[0],resD[1]))
		tmpString = ''
		for mresD in resultsDict[resD]: contraList.append('%s\n'%mresD) 
		
	return contraList
	
#==============================================================
def separateFalseJoins (joinFile, contraFile, baseName, currDir):
	joinsDict = {}
	initial = 1
	for line in open('%s'%joinFile):
		if line[0] == '#' and initial == 1: continue
		elif line[0] == '#' and initial == 0: joinsDict[joinHalfs] = tmpLines
		elif line[0] == 'J': 
			initial = 0; lineSplit = line.split(None)
			joinHalfs = (lineSplit[1],lineSplit[2],lineSplit[3],lineSplit[4],lineSplit[5],lineSplit[6])
			tmpLines = []
		else: tmpLines.append(line)
		
	contraDict = {}
	for line in contraFile:
		if line[0] == 's': lineSplit = line.split(None); contraJoin = (lineSplit[0],lineSplit[1])
		else: 
			try: contraDict[contraJoin].append(line)
			except KeyError: contraDict[contraJoin] = [line]
			
	contraOutput = '%s/%s.contraHits.dat'%(currDir, baseName)
	cO = open('%s'%contraOutput,'w')
	filteredOutput = '%s/%s.misjoinHits.filtered.dat'%(currDir, baseName)
	fO = open('%s'%filteredOutput,'w')
	for cD in contraDict:
		delete = 0
		for jD in joinsDict:
			if cD == (jD[0],jD[1]): delete = jD
			elif cD == (jD[0],jD[2]): delete = jD			
			elif cD == (jD[3],jD[4]): delete = jD
			elif cD == (jD[3],jD[5]): delete = jD
		if delete == 0: continue
		cO.write('#########################################################\n')
		cO.write('JOIN=\t%s\t%s\t%s\t%s\t%s\t%s\n'%(delete[0], delete[1], delete[2], delete[3], delete[4], delete[5]))
		for mjD in joinsDict[jD]: cO.write(mjD)
		for mcD in contraDict[cD]: cO.write('CONTRA=\t'); cO.write(mcD)
		if delete != 0: del joinsDict[delete]
		
	cO.write('#########################################################\n')
	
	for jD in joinsDict:
		fO.write('#########################################################\n')
		fO.write('JOIN=\t%s\t%s\t%s\t%s\t%s\t%s\n'%(jD[0], jD[1], jD[2], jD[3], jD[4], jD[5]))
		for mjD in joinsDict[jD]: fO.write(mjD)
		
	fO.write('#########################################################\n')
	cO.close(); fO.close()
	
#==============================================================
def reJoin ( assemblyFasta, pbFasta, rejoinFasta, rejoinMap, blatFile, falseJoinMap, baseName, gapSize, oh_cmdLog):

	pbDict = {}; gapDict = {}; pbSet = set()
	for line in open('%s'%rejoinMap):
		lineSplit = line.split(None)
		try: pbDict[lineSplit[0]].append(lineSplit[1])
		except KeyError: pbDict[lineSplit[0]] = [lineSplit[1]]
		pbSet.add(lineSplit[0])
		
	#		awk '$1 > $11 * 0.24 {print}' pacbio onto scaff filter
	x = iterCounter(1000000); blatDict = {}; blatDict2 = {}; blatPos = {}
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		lineSplit = line.split(None)
		x()
		trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
		qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 
)
		if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd); qtrueEnd = qtrueStart + (qtrueEnd - tmp)
		key = tuple(lineSplit[:17])
		blatDict[key] = (float(lineSplit[10]))
		try: blatPos[lineSplit[9]].append((trueStart, trueEnd, qtrueStart, qtrueEnd))
		except KeyError: blatPos[lineSplit[9]] = [(trueStart, trueEnd, qtrueStart, qtrueEnd)] 
		blatDict2[(lineSplit[9],trueStart,trueEnd,qtrueStart,qtrueEnd)] = line
		
	x = iterCounter(1000000); distances = {}
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(falseJoinMap), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		if line[0] == '#': continue
		elif line[0] == 'J':
			lineSplit = line.split(None)
			key = (lineSplit[1], lineSplit[2], lineSplit[3], lineSplit[4], lineSplit[5], lineSplit[6])
		else:
			lineSplit = line.split(None)
			blatKey = tuple(lineSplit[:17])
			try: distances[key].append(blatDict[blatKey])
			except KeyError: distances[key] = [blatDict[blatKey]]
			
# 	for di in distances:
# 		tmpDist = {}
# 		averageLength = sum(distances[di])/float(len(distances[di]))
# 		print di, averageLength
# 	assert False
		
	for y in iterFASTA(open('%s'%rejoinFasta)):
		scaffold = str(y).split()[1]; sequence = str(y.seq)	   
		if scaffold.count('|') < 1: continue
		pullGaps = re.compile( r'([N]{10000})' ).finditer
		for item in pullGaps(str(sequence)): 
			begin, end = item.span()
			try: gapDict[scaffold].append((int(begin),int(end)))
			except KeyError: gapDict[scaffold] = [(int(begin),int(end))]
			
	blatDict = {}
	readsDict = {}; x = iterCounter(100000)
	for z in iterFASTQ(open('%s'%pbFasta)): readsDict[z['seq']['id']] = '%s\n+\n%s'%(z['seq']['seq'], z['qual']['quals']); x()
	targetDict = {}; x = iterCounter(100000)
	for z in iterFASTA(open('%s'%rejoinFasta)): targetDict[str(z).split()[1]] = str(z.seq); x(); baseName2 = '%s.neg'%baseName
	
	for gS in gapSize:		
		for gD in gapDict:
			for mGD in gapDict[gD]:
				if gD not in pbSet: continue
				print gD, mGD
				gapStart = mGD[0]; gapEnd = mGD[1]
# 	 			try: callRejoinGapConsensus( readsDict, pbDict[gD], targetDict, gD, baseName2, gapStart, gapEnd, blatPos, blatDict2, gS)
# 	 			except ValueError: pass
				#callRejoinGapConsensus( readsDict, pbDict[gD], targetDict, gD, baseName2, gapStart, gapEnd, blatPos, blatDict2, gS)
	# 			assert False
			
#==============================================================
def cleanupRJ ( assemblyFasta, pbFasta, rejoinFasta, rejoinMap, baseName, oh_cmdLog):

	misjoinFile = '1and2.misjoinHitsAndMisses.dat.bz2'; outFile = '%s.gapFillingLong.dat'%baseName; oF = open('%s'%outFile,'w')
	
	readsDict = {}; x = iterCounter(100000)
	for z in iterFASTQ(open('%s'%pbFasta)): readsDict[z['seq']['id']] = '%s\n+\n%s'%(z['seq']['seq'], z['qual']['quals']); x()
	
	blatDict = {}

	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(misjoinFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		lineSplit = line.split(None)
		if line[0] == '#': continue
		elif line[0] == 'J': 
			blatKey = lineSplit[1]
			blatKey2 = lineSplit[4]
		else:
			try: blatDict[blatKey].add(line)
			except KeyError: blatDict[blatKey] = set([line])
			try: blatDict[blatKey2].add(line)
			except KeyError: blatDict[blatKey2] = set([line])
			
	pbDict = {}; consensusDict = {}
	for line in open('%s.gapFilling.dat'%(baseName)):
		if line[0] == 's': 
			lineSplit = line.split(None)
			gapKey = (lineSplit[0],int(lineSplit[1]),int(lineSplit[2]))
		elif line[0] == 'm': 
			try: pbDict[gapKey].append(line.split(None)[0])
			except KeyError: pbDict[gapKey] = [line.split(None)[0]]
		else: consensusDict[gapKey] = line
		
	originalDict = {}
	tmpString = '...NNN+...'
	for y in iterFASTA(open('%s'%rejoinFasta)):
		scaffold = str(y).split()[1]; sequence = str(y.seq)	   
		if scaffold.count('-') < 1: continue
		pullGaps = re.compile( r'([N]{10000})' ).finditer
		tmpList = [ item for item in pullGaps(str(sequence))]
		for item in tmpList:
			begin, end = item.span()
			gapStart = begin; gapEnd = end; before = sequence[gapStart-500:gapStart]; after = sequence[gapEnd:gapEnd+500]
			sequence2 = before + tmpString + after
			originalDict[(scaffold,gapStart,gapEnd)] = sequence2
		
	for cD in consensusDict:
		oF.write('####################################################\n')
		oF.write('REJOIN\t%s\t%d\t%d\n'%(cD[0],cD[1],cD[2]))
		oF.write('ORIGINAL\t%s\n'%originalDict[cD])
		oF.write('CONSENSUS\t%s'%consensusDict[cD])
		scaffsInvolved = [sI for sI in cD[0].split('-')]
		blatString = ''
		for sI in scaffsInvolved: 
			indScaffs = sI.split('-')
			for iS in indScaffs:
				for blat in blatDict[iS[:-2]]: blatString += '%s'%blat
		oF.write('%s'%blatString)
		pbString = ''
		for pCD in pbDict[cD]: pbString += '%s\n%s\n'%(pCD, readsDict[pCD].split('\n')[0])
		oF.write('%s'%pbString)
		
	oF.write('####################################################\n'); oF.close()
	
#==============================================================
def callRejoinGapConsensus( readsDict, readNames, targetDict, targetName, baseName, gapStart, gapEnd, blatPos, blatDict, gapSize):
    readFastq = '%s.reads.fastq'%baseName; rF = open('%s'%readFastq,'w')
    targetFasta = '%s.preTarget.fasta'%baseName; tF = open('%s'%targetFasta,'w')
    
    for rN in readNames: rF.write('@%s\n%s\n'%(rN,readsDict[rN]))
#     for rN in readNames: rF.write('@%s\n%s\n'%(rN,readsDict[rN]))
    tF.write('>dummyName\n%s\n'%(targetDict[targetName]))
    rF.close(); tF.close()
    tmpString = ''
#     for a in range(gapEnd-gapStart): tmpString += 'A'
    #for a in range(gapSize): tmpString += 'N'
    newTarget = '%s.target.fasta'%baseName
    nT = open('%s'%newTarget,'w')
    for z in iterFASTA(open('%s.preTarget.fasta'%baseName)):
        scaffold = str(z).split()[1]; sequence = str(z.seq); 
        before = sequence[:gapStart-gapSize]; after = sequence[gapEnd+gapSize:]
        #sequence2 = before + tmpString + after
        sequence2 = before + after
        break
    nT.write('>%s\n%s'%(scaffold, sequence2))
    nT.close()
#     print len(sequence2)
    
    putGapCrosser = '%s.maybeGapCrosser.dat'%baseName; putGapBlat = '%s.gapSeeker.filtered.blat.bz2'%baseName; dummySam = '%s.sam'%baseName
    placementDict = makeDummySam ( dummySam, readNames, gapStart, gapEnd, readsDict, blatPos, blatDict, targetName, targetDict, baseName)
    quiverCommands = '%squiver'%baseName
    qC = open('%s'%quiverCommands,'w')
    qC.write('#!/bin/sh\n\n')
    qC.write('. /opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/etc/setup.sh\n\n')
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/blasr -sam -out %s.tmp.sam %s.reads.fastq %s -nproc 6 -minMatch 8 -minPctIdentity 60\n'%(baseName, baseName, 
newTarget))
    qC.close()
    system('chmod 777 %s'%quiverCommands)
    system('./%s'%quiverCommands)
#     print placementDict
    samOut = open('%s.sam'%baseName,'w')
    for line in open('%s.tmp.sam'%baseName):
	    if line[0] == '@': samOut.write('%s'%line); continue
	    lineSplit = line.split(None)
	    try: 
		    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
		    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
		    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
		    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
	    except KeyError: 
		    samOut.write('%s'%line)
		    samOut.write('%s'%line)
    samOut.close()
    quiverCommands = '%squiver'%baseName
    qC = open('%s'%quiverCommands,'w')
    qC.write('#!/bin/sh\n\n')
    qC.write('. /opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/etc/setup.sh\n\n')
#     qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/blasr -sam -out %s.sam %s.reads.fastq %s -nproc 6 -minMatch 8 -minPctIdentity 60\n'%(baseName, baseName, 
newTarget))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/samtoh5 %s.sam %s %s.cmp.h5\n'%(baseName, newTarget, baseName))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/cmph5tools.py sort --inPlace %s.cmp.h5\n'%baseName)
    contextStart = gapStart - 200; contextEnd = contextStart + 400 + gapSize
#     qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --verbose --referenceFilename %s --minCoverage 1 -C 1000000 --noEvidenceConsensusCall nocall 
--minConfidence 1 -o %s.output.fasta -o %s.variants.gff --referenceWindow %s:%d-%d\n'%(baseName, newTarget, baseName, baseName, scaffold, contextStart, contextEnd))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --verbose --referenceFilename %s -x 1 -C 1000000 --noEvidenceConsensusCall nocall -m 5 
--minConfidence 1 -o %s.output.fasta -o %s.variants.gff --referenceWindow dummyName:%d-%d\n'%(baseName, newTarget, baseName, baseName, contextStart, contextEnd))

    qC.close()
    system('chmod 777 %s'%quiverCommands)
    system('./%s'%quiverCommands)
#     system('rm %s; rm %s.target.fasta; rm %s.reads.fastq; rm %s.tmp.sam; rm %s.cmp.h5'%(quiverCommands, baseName, baseName, baseName, baseName))
    finalOutput = '%s.%d.gapFilling.dat'%(baseName,gapSize); rawQuiver = '%s.%d.rawQuiver.dat'%(baseName, gapSize)
    fO = open('%s'%finalOutput,'a'); rQ = open('%s'%rawQuiver,'a')
    for x in iterFASTA(open('%s.output.fasta'%baseName)): consensusSequence = str(x.seq)
    polyN = re.compile( r'([N])' ).search(consensusSequence)
    if polyN: 
        samOut = open('%s.sam'%baseName,'w')
        for line in open('%s.tmp.sam'%baseName):
		    if line[0] == '@': samOut.write('%s'%line); continue
		    lineSplit = line.split(None)
		    try: 
			    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
			    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
			    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
			    samOut.write('%s\t%d\t%s\n'%('\t'.join(lineSplit[:3]),placementDict['/'.join(lineSplit[0].split('/')[:-1])],'\t'.join(lineSplit[4:])))
		    except KeyError: pass
        samOut.close()
        quiverCommands = '%squiver'%baseName
        qC = open('%s'%quiverCommands,'w')
        qC.write('#!/bin/sh\n\n')
        qC.write('. /opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/etc/setup.sh\n\n')
        qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/samtoh5 %s.sam %s %s.cmp.h5\n'%(baseName, newTarget, baseName))
        qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/cmph5tools.py sort --inPlace %s.cmp.h5\n'%baseName)
        contextStart = gapStart - 200; contextEnd = contextStart + 400 + gapSize
        qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --verbose --referenceFilename %s -x 1 -C 1000000 --noEvidenceConsensusCall nocall -m 5 
--minConfidence 1 -o %s.output.fasta -o %s.variants.gff --referenceWindow dummyName:%d-%d\n'%(baseName, newTarget, baseName, baseName, contextStart, contextEnd))
        for x in iterFASTA(open('%s.output.fasta'%baseName)): consensusSequence = str(x.seq)
        polyN = re.compile( r'([N])' ).search(consensusSequence)
        if polyN: 
            samOut = open('%s.sam'%baseName,'w')
            for line in open('%s.tmp.sam'%baseName): 
		        if line[0] == '@': samOut.write('%s'%line)
		        else:
			        samOut.write('%s'%line)
			        samOut.write('%s'%line)
			        samOut.write('%s'%line)
			        samOut.write('%s'%line)
            samOut.close()
            quiverCommands = '%squiver'%baseName
            qC = open('%s'%quiverCommands,'w')
            qC.write('#!/bin/sh\n\n')
            qC.write('. /opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/etc/setup.sh\n\n')
            qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/samtoh5 %s.sam %s %s.cmp.h5\n'%(baseName, newTarget, baseName))
            qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/cmph5tools.py sort --inPlace %s.cmp.h5\n'%baseName)
            contextStart = gapStart - 200; contextEnd = contextStart + 400 + gapSize
            qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --verbose --referenceFilename %s -x 1 -C 1000000 --noEvidenceConsensusCall nocall -m 5 
--minConfidence 1 -o %s.output.fasta -o %s.variants.gff --referenceWindow dummyName:%d-%d\n'%(baseName, newTarget, baseName, baseName, contextStart, contextEnd))
            for x in iterFASTA(open('%s.output.fasta'%baseName)): consensusSequence = str(x.seq)
	    
    fO.write('%s\t%d\t%d\n'%(targetName, gapStart, gapEnd)); rQ.write('>%s_%d_%d\n%s\n'%(targetName, gapStart, gapEnd, consensusSequence))
    fO.write('%s\n'%consensusSequence)
    for rN in readNames: fO.write('%s\n'%rN)
    fO.close(); rQ.close()
			
#==============================================================
def maybeCollapse (dataFile, baseName):

    x = iterCounter(1000000); counter = 1; linesDict = {}
    tmpProcess1 = subprocess.Popen('cat %s | bunzip2 -c | tail -n+2'%(dataFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    x()
	    if line[0] != '=': 
		    try: linesDict[counter].append(line)
		    except KeyError: linesDict[counter] = [line]
	    else: counter+=1
	    
    results = set(); x = iterCounter(1000000)
    for lD in linesDict:
	    scaffDict = {}
	    for line in linesDict[lD]:
	        x()
	        lineSplit = line.split(None); scaff = lineSplit[0]; scaffStart = float(lineSplit[1]); pb = lineSplit[2]; pbStart = float(lineSplit[4])
	        if scaff[0] == '*': scaff = scaff[2:]
	        try: scaffDict[scaff].append((pbStart,scaffStart))
	        except KeyError: scaffDict[scaff] = [(pbStart,scaffStart)]
	    for sD in scaffDict:
	        tmpList = [z for z in scaffDict[sD]]
	        if len(tmpList) < 4 or len(tmpList) > 50: continue
	        tmpList.sort(key=lambda x: x[1])
	        a = 0
	        while a < len(tmpList) - 4:
		        if abs(tmpList[a][1] - tmpList[a+4][1]) >= 10: a+=1; continue
		        if abs(tmpList[a][0] - tmpList[a+1][0]) <= 200: a+=1; continue
		        if abs(tmpList[a][0] - tmpList[a+2][0]) <= 200: a+=1; continue
		        if abs(tmpList[a][0] - tmpList[a+3][0]) <= 200: a+=1; continue
		        results.add((pb,sD))
		        a += 1
            
    outFile = '%s.maybeFME.dat'%baseName
    oF = open('%s'%outFile,'w')
    for res in results:
        oF.write('%s\t%s\n'%(res[0],res[1]))
    oF.close()
    
#==============================================================
def collapseFinder (blatFile, readsFasta, scaffoldsFasta, baseName):

    edgeHits = {}; x = iterCounter(100000); blatDict = {}
    tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
        lineSplit = line.split(None)
        x()
        if lineSplit[0] < 200: continue
#		awk '$1 > $11 * 0.24 {print}' pacbio onto scaff filter
        trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
        qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
        if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
        if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
#         if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.8: continue
#         if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.24: continue
        try: edgeHits[(lineSplit[9],lineSplit[13])].append((trueStart,qtrueStart,trueEnd,qtrueEnd,lineSplit[8]))
        except KeyError: edgeHits[(lineSplit[9],lineSplit[13])] = [(trueStart,qtrueStart,trueEnd,qtrueEnd,lineSplit[8])]
        blatDict[(lineSplit[9],lineSplit[13],trueStart,trueEnd,qtrueStart,qtrueEnd)] = lineSplit
    
    results = {}; results2 = {}; results3 = {}
    for eH in edgeHits:
	    tmpList = edgeHits[eH]
	    tmpList.sort(key=lambda x: x[1])
	    if len(tmpList) < 3: continue
	    a = 0; score = 0; sucessList = set()
	    while a < len(tmpList) - 1:
	        b = a + 1
        	if tmpList[a][3] > tmpList[b][3]: a+=1; continue
        	if tmpList[a][4] != tmpList[b][4]: a+=1; continue
        	tmpOverlap = overLap(tmpList[a][1],tmpList[a][3],tmpList[b][1],tmpList[b][3])
        	if tmpOverlap * 3 > abs(tmpList[a][0] - tmpList[a][2]): a+=1; continue
        	if abs(tmpList[a][1] - tmpList[b][1]) >= 2 * abs(tmpList[a][0] - tmpList[b][0]): sucessList.add((eH[0],eH[1],tmpList[a][0],tmpList[a][2],tmpList[a][1],tmpList[a][3])); 
sucessList.add((eH[0],eH[1],tmpList[b][0],tmpList[b][2],tmpList[b][1],tmpList[b][3])); score +=1
        	a += 1
	    if score < 3: continue
	    tmpStart = rounddownY(int(tmpList[a][0]),10000)
	    try: results[(eH[1], tmpStart)].add(eH[0])
	    except KeyError: results[(eH[1], tmpStart)] = set([eH[0]])
	    try: results2[(eH[1], tmpStart)].add(tmpList[a][0])
	    except KeyError: results2[(eH[1], tmpStart)] = set([tmpList[a][0]])
	    for sL in sucessList: 
		    try: results3[(eH[1], tmpStart)].append((blatDict[sL]))
		    except KeyError: results3[(eH[1], tmpStart)] = [blatDict[sL]]
		    
    outputFile = '%s.collapsedRepeat.dat'%baseName
    oF = open('%s'%outputFile,'w')   
    for res in results:
	    if len(results[res]) < 2: continue
	    x = 0; averageStart = int(sum(list(results2[res]))/len(results2[res]))
#  	    for mRes in results[res]: 
# 	 	    signal = (mRes,res[0],res[1],res[1]+10000); x += 1
# 	 	    if x <= 3: DotPlotChecker(signal, readsFasta, scaffoldsFasta, baseName)
	    oF.write('#########################################################\n') 
	    oF.write("COLLAPSESTART:\t%s\t%d\n"%(res[0], averageStart))
	    oF.write("NUMBERREADS:\t%d\n"%len(results[res]))
	    tmpString = ''
	    for mRes in results3[res]: 
		    for lS in mRes[0:17]: tmpString += '%s\t'%lS
		    tmpString += '\n'
	    tmpString = tmpString[:-1]; oF.write('%s\n'%tmpString)
			    
    oF.write('#########################################################')
    oF.close()

#==============================================================
def maybeMisjoinWithin (dataFile, readsFasta, scaffoldsFasta, baseName):
		    
    x = iterCounter(1000000)
    results = []
    initial = 1
    for line in open('%s'%dataFile):
        x()
        if line[0] == '=' and initial == 1: 
			initial = 0
			scaffs = {}
			scaffPos = {}
			continue
        if line[0] != '=':
			lineSplit = line.split(None)
			pb = lineSplit[2]
			scaff = lineSplit[0]
			if scaff[0] == '*': scaff = scaff[2:]
			pos = float(lineSplit[4])
			sP = float(lineSplit[1])
			length = float(lineSplit[3])
			try: scaffs[scaff].append(pos)
			except KeyError: scaffs[scaff] = [pos]
			try: scaffPos[scaff].append(sP)
			except KeyError: scaffPos[scaff] = [sP]
        elif initial != 1:
            for sP in scaffPos: scaffPos[sP].sort()
            for sca in scaffs: scaffs[sca].sort()
            for sP in scaffPos:
                a = 0
                if len(scaffPos[sP]) < 8: continue
                addOn = 0
                tmpDictOn = []
                tmpDictOff = []
                while a < len(scaffPos[sP]):
                    diff1 = scaffPos[sP][a] - scaffPos[sP][0]
                    diff2 = scaffs[sP][a] - scaffs[sP][0]
                    if diff2 + length * 0.3 < diff1 and diff2 + 2000 < diff1: addOn = 1
                    if addOn == 1: tmpDictOn.append(a)
                    else: tmpDictOff.append(a)
                    a += 1
                if len(tmpDictOff) > 3 and len(tmpDictOn) > 3: results.append((pb, sP, scaffPos[sP][tmpDictOff[0]], scaffPos[sP][tmpDictOn[-1]]))
            scaffs = {}
            scaffPos = {}
            
    for res in results: print "%s\t%s\t%.1f\t%.1f"%(res[0],res[1], res[2],res [3])
	
#==============================================================
def withinMisjoinFinder (blatFile, readsFasta, targetFasta, baseName):
        
    edgeHits = {}
    blackList = set()
    x = iterCounter(100000)
    for line in open('%s'%blatFile):
        lineSplit = line.split(None)
        x()
        if float(lineSplit[0]) < 2000: continue
        if float(lineSplit[0]) * 2.0 < float(lineSplit[5]): continue
        trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
        qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
        if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
        if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
        if float(qtrueEnd - qtrueStart) * 1.1 > float(lineSplit[10]): blackList.add((lineSplit[9],lineSplit[13]))
        if (lineSplit[9],lineSplit[13]) in blackList: continue
        try: edgeHits[lineSplit[9]].append((lineSplit[13], trueStart, trueEnd,qtrueStart,qtrueEnd,float(lineSplit[10])))
        except KeyError: edgeHits[lineSplit[9]] = [(lineSplit[13],trueStart, trueEnd,qtrueStart,qtrueEnd,float(lineSplit[10]))]
        continue
            
#     print edgeHits["m131207_081800_42157_c100604352550000001823111506171404_s1_p0/29463/0_19336"]; assert False
            
    results = {}
    for eH in edgeHits:
        sortEH = sorted(edgeHits[eH], key=lambda x: x[1])
        a = 0
        if len(sortEH) > 8: continue
        if len(sortEH) < 2: continue
        while a < len(sortEH):
        	b = 0
        	while b < len(sortEH):
        		meH = sortEH[a]
        		meH2 = sortEH[b]
        		if meH[0] != meH2[0]: b +=1; continue
        		qdist = abs(meH2[3] - meH[3])
        		if meH2[3] < meH[3] and meH2[4] + 1000 > meH[4]: b += 1; continue
        		if meH[3] < meH2[3] and meH[4] + 1000 > meH2[4]: b += 1; continue
        		if qdist < 1000: b += 1; continue
        		tdist = abs(meH2[1] - meH[1])
        		if tdist < 2000: b += 1; continue
        		try: results[eH].append((meH[0],rounddown20k(meH[1]),roundup20k(meH[2]),rounddown20k(meH2[1]),roundup20k(meH2[2])))
        		except KeyError: results[eH] = [(meH[0],rounddown20k(meH[1]),roundup20k(meH[2]),rounddown20k(meH2[1]),roundup20k(meH2[2]))]
        		b += 1
    		a += 1
    
    tmpBlack = set()
    for eH in edgeHits:
		sortEH = sorted(edgeHits[eH], key=lambda x: x[1])
		for res in results:
			for mRes in results[res]:
				for meH in sortEH: 
					if meH[0] != mRes[0]: continue
					if rounddown20k(meH[1]) <= mRes[1] and rounddown20k(meH[1]) <= mRes[3] and roundup20k(meH[2]) >= mRes[2] and roundup20k(meH[2]) >= mRes[4]: 
tmpBlack.add(res)
    
    for tB in tmpBlack:
    	del results[tB]
    
    outFile = '%s.withinMisjoinHits.dat'%baseName
    oF = open('%s'%outFile,'w')
    edgeDict = {}
    for res in results:
        for mRes in results[res]:
            scaffKey = (mRes[0],mRes[1],mRes[3])
            try: edgeDict[scaffKey].add(res)
            except KeyError: edgeDict[scaffKey] = set([res])
                
	x = 0 
    for eD in edgeDict:
	    if len(edgeDict[eD]) < 2: continue
	    oF.write('%s\t%d\t%d\n'%(eD[0],eD[1],eD[2]))
	    for meD in list(edgeDict[eD]): 
		    oF.write('%s\n'%meD)
		    tmpList = [eD[1],eD[2]]; tmpList.sort()
		    if x < 20: signal = (meD, eD[0], tmpList[0], tmpList[1]+20000) 
		    if x < 20: DotPlotChecker(signal, readsFasta, targetFasta, baseName)
	    x += 1
    oF.close()

#==============================================================
def moleculoPutative (blatFile, baseName):

	molecDict = {}
	blatDict = {}
	x = iterCounter(100000)
	tmpProcess1 = subprocess.Popen( 'cat %s |  bunzip2 -c | awk \'{ 
ID=(($13-$12)<($17-$16-$4))?100*($1+$3)/($13-$12):100*($1+$3)/($17-$16-$4);COV=100*($13-$12)/$11;x=$6+$8;if((ID>95)&&(COV>95)) print }\' '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout:
		lineSplit = line.split(None)
		x()
		if lineSplit == '-': continue
		try: molecDict[lineSplit[13]].add(lineSplit[9])
		except KeyError: molecDict[lineSplit[13]] = set([lineSplit[9]])
		try: blatDict[(lineSplit[13],lineSplit[9])].append(line)
		except KeyError: blatDict[(lineSplit[13],lineSplit[9])] = [line]
		
	results = {}
	for mD in molecDict:
		aRs = []; eLs = []
		for mmD in molecDict[mD]:
			if mmD[0] == 'R': aRs.append((int(mmD.split('=')[1]),mmD))
			else: eLs.append((int(mmD.split('=')[1]),mmD))
		for mARs in aRs:
			for mELs in eLs:
				if mARs[0] + 1 == mELs[0]: 
					try: results[mD].add((mARs,mELs))
					except KeyError: results[mD] = set([(mARs,mELs)])
					
	outFile = '%s.MoleculoHits.dat'%baseName
	oF = open('%s'%outFile,'w')
	oF.write('#####################################\n')
	for res in results:
		for mRes in results[res]: 
			oF.write('%s\t%s\t%s\n'%(res, mRes[0][0], mRes[1][0]))
			for bD in blatDict[(res,mRes[0][1])]: oF.write('%s'%bD)
			for bD in blatDict[(res,mRes[1][1])]: oF.write('%s'%bD)
			oF.write('#####################################\n')
	oF.close()

#==============================================================

def assemblyTwoXSelection (assemblyFasta, baseName):
	acceptedMarkers = []; acceptedMers = []
	lowerSearch = pullGaps = re.compile( r'([gatc]+)' ).search
	goodMers = '%s.acceptedMers.fasta'%baseName
	goodMarkers = '%s.acceptedMarkers.dat'%baseName
	gM = open('%s'%goodMers,'w'); gM2 = open('%s'%goodMarkers,'w')
	x = 1; z = iterCounter(1000000)
	for y in iterFASTA(open('%s'%assemblyFasta)):
		scaffold = str(y).split()[1]; sequence = str(y.seq)	   
		a = 0
		while a < len(sequence) - 100:
			b = a + 100
			tmpSequence = sequence[a:b]
			if lowerSearch(tmpSequence) or 'N' in tmpSequence or 'X' in tmpSequence: a += 50; x += 1; z()
			else: 
				gM.write('>%s_%d\n%s\n'%(scaffold,a,tmpSequence)); gM2.write('%s/%d\n'%(scaffold,a))
				a += 50; x += 1; z()
			
	gM.close(); gM2.close()
	
#==============================================================
def assemblyTwoXSelectionMem (assemblyFasta, baseName):
	acceptedMarkers = []
	acceptedMers = []
	lowerSearch = pullGaps = re.compile( r'([gatc]+)' ).search
	x = 1
	z = iterCounter(1000000)
	fasta100mers = {}
	for y in iterFASTA(open('%s'%assemblyFasta)):
		scaffold = str(y).split()[1]; sequence = str(y.seq)	   
		a = 0
		while ( a < len(sequence) - 100 ):
			b = a + 100
			tmpSequence = sequence[a:b]
			if lowerSearch(tmpSequence) or 'N' in tmpSequence or 'X' in tmpSequence: a += 50; x += 1; z()
			else: 
				fasta100mers['%s/%d'%(scaffold,a)] = '%s'%(tmpSequence)
				a += 50; x += 1; z()
            #####
        #####
    #####
			
	return fasta100mers
	
#==============================================================
	
def eliminateUnreliableMersMoleculo (blatFile, acceptedMarkers, modeNumber, baseName):
	merDict = {}
	x = iterCounter(1000000)
	for line in open('%s'%blatFile):
	   lineSplit = line.split(None)
	   key = lineSplit[9]
	   try: merDict[key].add(lineSplit[13])
	   except KeyError: merDict[key] = set([lineSplit[13]])
	   x()
		
	reliable = '%s.Reliable.dat'%baseName
	rel = open('%s'%reliable,'w')
	
	unreliable = set()
		
	for merd in merDict:
# 	   print len(set(merDict[merd]))
	   if len(merDict[merd]) <= 3 * modeNumber: continue
	   unreliable.add(merd)
	   
	print len(unreliable)
			
	for aM in open('%s'%acceptedMarkers):
		mer = aM.split('|')[0]
		if mer in unreliable: continue
		rel.write('%s'%aM)
		
	rel.close()

#==============================================================

def pullGapCrossersMoleculo (refFasta, blatFile, molFasta, baseName):

	preTrees = {}; blatLines = []
	x = iterCounter(100000)
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	    lineSplit = line.split(None); blatLines.append(line); x()
	    trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
	    if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
	    if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
	    if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
	    if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
	    try: preTrees[lineSplit[13]].append((trueStart,trueEnd))
	    except KeyError: preTrees[lineSplit[13]] = [(trueStart,trueEnd)]
	    
	Trees = {}
	for pT in preTrees: Trees[pT] = IntervalTree([Interval(int(window [0]),int(window [1])) for window in preTrees[pT]])
	
	rCount = iterCounter(1000000); readSeq = {}
	for z in iterFASTA(open('%s'%molFasta)): scaffold = str(z).split()[1]; sequence = str(z.seq); readSeq[scaffold] = sequence; rCount()
	    
	outDict = {}; outDict2 = {}; outSet = set(); outData = {}; outData2 = {}; outData3 = {}; gapsList = []
	for y in iterFASTA(open('%s'%refFasta)):
		record = y
		pullGaps = re.compile( r'([N]+)' ).finditer
		scaffold = str(y).split()[1]; scaff_seq = str(record.seq)
		for item in pullGaps(str(record.seq)): 
		    begin, end = item.span(); outDict[(begin,end)] = {}
		    try: check = Trees[scaffold].search(begin)
		    except KeyError: check =[]
		    try: check2 = Trees[scaffold].search(end)
		    except KeyError: check2 =[]
		    if check != []:
			    for che in check: outDict[(begin,end)][(che.begin,che.end)] = 1
		    if check2 != []:
			    for che in check2:
				    try: outDict[(begin,end)][(che.begin,che.end)] += 1; outDict2[(che.begin,che.end)] = (begin,end); outSet.add((che.begin,che.end))
				    except KeyError: continue
	
	resultsDict = {}; readThreshold = 200
	for line in blatLines: 
	    lineSplit = line.split(None)
	    trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    goodTuple = (trueStart, trueEnd)
	    if goodTuple in outSet:
	       if outDict2[(trueStart,trueEnd)][0] - readThreshold < trueStart: continue
	       if outDict2[(trueStart,trueEnd)][1] + readThreshold > trueEnd: continue
	       if 2.0 * (float(lineSplit[12]) - float(lineSplit[11])) < trueEnd - trueStart: continue
# 	       print lineSplit[13],outDict2[(trueStart,trueEnd)][0],outDict2[(trueStart,trueEnd)][1], "\n",line, readStart, readEnd, "\n", readSeq[lineSplit[9]][readStart:readEnd], "\n", 
consensusSeq; assert False
	       try: outData['%s\t%d\t%d'% (lineSplit[13],outDict2[(trueStart,trueEnd)][0],outDict2[(trueStart,trueEnd)][1])].append(line)
	       except KeyError: outData['%s\t%d\t%d'% (lineSplit[13],outDict2[(trueStart,trueEnd)][0],outDict2[(trueStart,trueEnd)][1])] = [line]
   	       try: outData2['%s\t%d\t%d'% (lineSplit[13],outDict2[(trueStart,trueEnd)][0],outDict2[(trueStart,trueEnd)][1])].append((lineSplit[9],readSeq[lineSplit[9]]))
	       except KeyError: outData2['%s\t%d\t%d'% (lineSplit[13],outDict2[(trueStart,trueEnd)][0],outDict2[(trueStart,trueEnd)][1])] = [(lineSplit[9],readSeq[lineSplit[9]])]
	       
	outFile = '%s.gapFilling.dat'%baseName; oF = open('%s'%outFile,'w'); oF.write('Scaffold\tGapStart\tGapEnd\tReads\n')
	for oD in outData:
		tmpOutString = ''; tmpOutString += '%s\t'%oD
# 		for mOD in outData[oD]: oF.write('%s'%mOD)
		for mOD2 in outData2[oD]: tmpOutString +='%s '%mOD2[0]
		oF.write('%s\n'%tmpOutString)
	oF.close()
		
#==============================================================
def maybeJoin (dataFile, readsFasta, scaffoldsFasta, baseName):

    lengthsDict = {}
    for y in iterFASTA(open('%s'%scaffoldsFasta)):
	    scaffold = str(y).split()[1]; length = len(str(y.seq))
	    lengthsDict[scaffold] = length
		    
    x = iterCounter(1000000); pbDict = {}; counter = 1; linesDict = {}
    tmpProcess1 = subprocess.Popen('cat %s | bunzip2 -c | tail -n+2'%(dataFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
	    x()
	    if line[0] != '=': 
		    try: linesDict[counter].append(line)
		    except KeyError: linesDict[counter] = [line]
	    else: counter+=1
	    	
    x = iterCounter(100000); putativeJoins = {}
    for lD in linesDict:
	    x(); scaffs = {}; scaffPos = {}
	    for line in linesDict[lD]:
			lineSplit = line.split(None)
			scaff = lineSplit[0]; sP = float(lineSplit[1]); pb = lineSplit[2]; length = float(lineSplit[3]); pos = float(lineSplit[4])
			if scaff[0] == '*': scaff = scaff[2:]
			try: scaffs[scaff].append(pos)
			except KeyError: scaffs[scaff] = [pos]
			try: scaffPos[scaff].append(sP)
			except KeyError: scaffPos[scaff] = [sP]
	    results = set(); inclusive = []
	    for sca in scaffs:
    		if len(scaffs[sca]) > 3: results.add(sca)
	    if len(results) != 2:
    		scaffs = {}; scaffPos = {}
    		continue
	    else: 
		    hits = {1:set(),2:set()}
		    for sca in scaffs:
			    if sca in results:
				    ending = lengthsDict[sca]
				    for mPos in scaffs[sca]:
					    if mPos <= 500: hits[1].add(sca)
					    if mPos >= ending - 500: hits[2].add(sca)
		    if len(hits[1]) < 1: scaffs = {}; scaffPos = {}; continue
		    if len(hits[2]) < 1: scaffs = {}; scaffPos = {}; continue
		    if len(hits[1]) == 1 and len(hits[2]) == 1 and hits[1] != hits[2]: 
			    try: putativeJoins[(list(hits[1])[0],1,list(hits[2])[0],2)].add(pb)
			    except KeyError: putativeJoins[(list(hits[1])[0],1,list(hits[2])[0],2)] = set([pb])
			    
    outFile = '%s.maybeJoiner.dat'%baseName; oF = open('%s'%outFile,'w')
			    
    for pJ in putativeJoins:
    	    for mPJ in putativeJoins[pJ]: oF.write('%s\t%s\n%s\t%s\n'%(mPJ,pJ[2],mPJ,pJ[0]))
			
    oF.close()
    
#==============================================================
    
def findJoins (blatFile, baseName):
        
    edgeHits = {}; x = iterCounter(100000); startDict = {}; endDict = {}; startExtents = {}; endExtents = {}; outFile = '%s.joins.dat'%baseName; oF = open('%s'%outFile,'w')
    print blatFile
    tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c'%(blatFile), shell=True, stdout=subprocess.PIPE )
    for line in tmpProcess1.stdout: 
        lineSplit = line.split(None)
        x()
#		awk '$1 > $11 * 0.24 {print}' pacbio onto scaff filter
        trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
        qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
        if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd); qtrueEnd = qtrueStart + (qtrueEnd - tmp)
#         if qtrueEnd - qtrueStart < int(lineSplit[10])/5: continue
#         if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.24: continue
        if trueStart < 500: 
	        try: startDict[lineSplit[13]].add(lineSplit[9])
	        except KeyError: startDict[lineSplit[13]] = set([lineSplit[9]])
	        startExtents[(lineSplit[9],lineSplit[13],trueStart,trueEnd)] = '\t'.join(lineSplit[:17])
        if trueEnd > int(lineSplit[14]) - 500: 
            try: endDict[lineSplit[13]].add(lineSplit[9])
            except KeyError: endDict[lineSplit[13]] = set([lineSplit[9]])
            endExtents[(lineSplit[9],lineSplit[13],trueStart,trueEnd)] = '\t'.join(lineSplit[:17])
            
    results = {}
    for sD in startDict:
	    for eD in endDict:
		    if eD == sD: continue
		    intersection = list(startDict[sD].intersection(endDict[eD]))
		    if len(intersection) < 2: continue
		    results[(sD,eD)] = intersection
    
    for res in results:
	    oF.write('##############################################\n')
	    oF.write('%s\t%s\n'%(res[1],res[0])); currStartExt = []; currEndExt = []
	    for mRes in results[res]:
		    for sE in startExtents:
			    if sE[0] == mRes and sE[1] == res[0]: currStartExt.append(sE)
		    for eE in endExtents:
			    if eE[0] == mRes and eE[1] == res[1]: currEndExt.append(eE)
	    oF.write('READ\tSCAFFOLD\tTRUE START\tTRUE END\n')
	    for eS in currEndExt: oF.write('%s\n'%('\t'.join(str(x) for x in eS)))
	    for cS in currStartExt: oF.write('%s\n'%('\t'.join(str(x) for x in cS)))
	    oF.write('BLATS\n')
	    for eS in currEndExt: oF.write('%s\n'%(endExtents[eS]))
# 	    for cS in currStartExt: endExtents[eS]
	    for cS in currStartExt: oF.write('%s\n'%(startExtents[cS]))
    oF.write('##############################################\n')
    oF.close()
	    
	
#==============================================================
def lengthHist (readsFasta, baseName, oh_cmdLog):

	preHist = {}; total = 0
	for y in iterFASTA(open('%s'%readsFasta)):
		currLength = len(str(y.seq))
		currLength= iround(float(float(currLength)/100.0)) * 100
		total += currLength
		try: preHist[currLength] += 1
		except KeyError: preHist[currLength] = 1
		
	newPreHist = [(pH, preHist[pH]) for pH in preHist]
	newPreHist.sort()
		
	hist = []; cumu= 0.0 
	for pH in newPreHist: cumu += float(pH[0]*pH[1]/float(total)); hist.append((pH[0],float(pH[0]*pH[1]/float(total)),cumu))
	hist.sort()
	
	outData = '%s.lengthHist.dat'%baseName; oD = open('%s'%outData,'w')
	for h in hist: oD.write('%d\t%.8f\t%.8f\n'%(h[0],h[1],h[2]))
	oD.close()
	plotDatBlat (baseName, outData, oh_cmdLog)
		

#==============================================================
def lowScaffoldsHistPB (blatFile, baseName):
	merDict = {}
	x = iterCounter(1000000)
		
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
# 	tmpProcess1 = subprocess.Popen( 'cat %s '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	   lineSplit = line.split(None)
	   key = lineSplit[9]
	   try: merDict[key].add(lineSplit[13])
	   except KeyError: merDict[key] = set([lineSplit[13]])
	   x()
	tmpProcess1.poll()
		
	results = [rD for rD in merDict if len(merDict[rD]) == 1]
	
	dataOut = '%s.NumMerHitsOnPB.dat'%baseName
	dO = open('%s'%dataOut,'w')
	for res in results: dO.write('%s\n'%res)
	dO.close()

#==============================================================
def uniqueRegionFinder (dataFile, fastaFile, baseName):
    
	gapsDict = {}
	for y in iterFASTA(open('%s'%fastaFile)):
		pullGaps = re.compile( r'([GATC]+)' ).finditer
		scaffold = str(y).split()[1]
		for item in pullGaps(str(y.seq)):
			begin, end = item.span()
			if end - begin > 1000:
				try: gapsDict[scaffold].append((begin,end))
				except KeyError: gapsDict[scaffold] = [(begin,end)]
				
	Trees = {}; contigs = {}
	for pT in gapsDict: Trees[pT] = IntervalTree([Interval(int(window [0]),int(window [1])) for window in gapsDict[pT]])
	for pT in gapsDict: contigs[pT] = [int(window [0]) for window in gapsDict[pT]]
	for cont in contigs: contigs[cont].sort()
	
	x = iterCounter(1000000); pbDict = {}; counter = 1; linesDict = {}
	tmpProcess1 = subprocess.Popen('cat %s | bunzip2 -c | tail -n+2'%(dataFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
		x()
		if line[0] != '=': 
		    try: linesDict[counter].append(line)
		    except KeyError: linesDict[counter] = [line]
		else: counter+=1
	
	x = iterCounter(1000000)
	for lD in linesDict: 
	    checkSet = {}; Mmers = {}; mers = set(); tmpList = {}
	    for line in linesDict[lD]:
	        x()
	        lineSplit = line.split(None); pos = float(lineSplit[1]); pb = lineSplit[2]; mer = lineSplit[0]
	        if mer[0] == '*': mer = mer[2:]
	        try: check = Trees[mer].search(pos)
	        except KeyError: check =[]
	        if len(check) < 1: continue
	        try: checkSet[mer][check[0].get_begin()] += 1
	        except KeyError: 
		        try: checkSet[mer][check[0].get_begin()] = 1
		        except KeyError: checkSet[mer] = {check[0].get_begin():1}
	        try: Mmers[mer] += 1
	        except KeyError: Mmers[mer] = 1
	    for mMmers in Mmers:
	        if Mmers[mMmers] > 3: mers.add(mMmers)
	    for mMers in mers:
		    a = 0
		    while a < len(contigs[mMers]):
			    try: 
				    numberMark = checkSet[mMers][contigs[mMers][a]]
				    try: numberMark2 = checkSet[mMers][contigs[mMers][a+1]]
				    except IndexError: a+=1; continue
				    if numberMark > 1 and numberMark2 > 1: 
					    try: pbDict[mMers].append(pb)
					    except KeyError: pbDict[mMers] = [pb]
			    except KeyError: a += 1; continue
			    a += 1
			    
	x = iterCounter(10000)
	results = set()
	for pbd in pbDict:
		x()
		if len(pbDict[pbd]) < 2: continue
		for mpbd in pbDict[pbd]: results.add((mpbd,pbd.split('|')[0]))
	
	resultsOut = '%s.maybeUniqueCrosser.dat'%baseName
	rO = open('%s'%resultsOut,'w')
	x = iterCounter(10000)		
	for combos in list(results): outString = '%s\t%s\n'%(combos[0],combos[1]); rO.write('%s'%outString); x()
	rO.close()
  	
#==============================================================
def pullUniqueBlat (blatFile, fastaFile, maskFasta, readsFasta, baseName):

	readsDict = {}; x = iterCounter(100000)
	for z in iterFASTQ(open('%s'%readsFasta)): readsDict[z['seq']['id']] = '%s\n+\n%s'%(z['seq']['seq'], z['qual']['quals']); x()
	
	targetDict = {}; x = iterCounter(100000)
	for z in iterFASTA(open('%s'%fastaFile)): targetDict[str(z).split()[1]] = str(z.seq); x()
				
	preTrees = {}; blatLines = []; readDict = {}; blatPos = {}; blatDict = {}
	x = iterCounter(100000)
	tmpProcess1 = subprocess.Popen( 'cat %s | bunzip2 -c '%(blatFile), shell=True, stdout=subprocess.PIPE )
	for line in tmpProcess1.stdout: 
	    lineSplit = line.split(None); blatLines.append(line); x()
	    trueStart, trueEnd = best_extent(int(lineSplit[10]), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[20].split(',') if lS != ''], 2 )
	    qtrueStart, qtrueEnd = best_extent((trueEnd - trueStart), [int(lS) for lS in lineSplit[18].split(',') if lS != ''], [int(lS) for lS in lineSplit[19].split(',') if lS != ''], 2 )
	    if lineSplit[8] == '-': tmp = qtrueStart; qtrueStart = (int(lineSplit[10]) - qtrueEnd)
	    if lineSplit[8] == '-': qtrueEnd = qtrueStart + (qtrueEnd - tmp)
	    if qtrueEnd - qtrueStart < int(lineSplit[10])/3: continue
	    if int(lineSplit[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
	    try: preTrees[lineSplit[13]].append((trueStart,trueEnd))
	    except KeyError: preTrees[lineSplit[13]] = [(trueStart,trueEnd)]
	    readDict[(lineSplit[13], trueStart, trueEnd)] = lineSplit[9]
	    try: blatPos[lineSplit[9]].append((trueStart, trueEnd, qtrueStart, qtrueEnd))
	    except KeyError: blatPos[lineSplit[9]] = [(trueStart, trueEnd, qtrueStart, qtrueEnd)] 
	    blatDict[(lineSplit[9],trueStart,trueEnd,qtrueStart,qtrueEnd)] = line
	    
	Trees = {}
	for pT in preTrees: Trees[pT] = IntervalTree([Interval(int(window [0]),int(window [1])) for window in preTrees[pT]])
	
	outDict = {}; outDict2 = {}; outSet = set(); outData = []; gapsList = []
	for y in iterFASTA(open('%s'%maskFasta)):
		pullGaps = re.compile( r'([GATC]+)' ).finditer
		scaffold = str(y).split()[1]
		for item in pullGaps(str(y.seq)):
			begin, end = item.span()
			if end - begin <= 1000: continue
			try: check = Trees[scaffold].search(begin)
			except KeyError: check =[]
			try: check2 = Trees[scaffold].search(end)
			except KeyError: check2 =[]
			if check != []:
				for che in check: outDict[(begin,end,che.begin,che.end)] = 1
			if check2 != []:
			    for che in check2:
				    try: 
					    outDict[(begin,end,che.begin,che.end)]+= 1
					    try: outDict2[(scaffold, begin, end)].append(readDict[(scaffold,che.begin,che.end)])
					    except KeyError: outDict2[(scaffold, begin, end)] = [readDict[(scaffold,che.begin,che.end)]]
				    except KeyError: continue
					    
	for oD in outDict2: 
	    if len(outDict2[oD]) != 15: continue 
	    try: callAccCheck(oD, outDict2[oD], blatDict, blatPos, fastaFile, readsDict, targetDict, baseName); assert False
	    except ValueError: pass
	    
#==============================================================
def assemblyHybridSelection2 (bigMerFasta, mer200Blat, baseName, currDir):
	acceptedMarkers = []
	goodMers = '%s/%s.acceptedMers.fasta'%(currDir, baseName); gM = open('%s'%goodMers,'w')
	currPos = 0; goodSet = set()
	for line in open('%s'%mer200Blat):
		lineSplit = line.split(None)	   
		merNumber = lineSplit[9]
		merPos = int(lineSplit[15])
		if merPos > currPos: acceptedMarkers.append((merNumber,merPos)); currPos = merPos + 100
		
	for aM in acceptedMarkers: goodSet.add(aM[0])
	
	for z in iterFASTA(open('%s'%bigMerFasta)):
	    read = str(z).split()[1]; sequence = str(z.seq)
	    if read not in goodSet: continue
	    gM.write('>%s\n%s\n'%(read,sequence))    
	
	gM.close()
	
	merDict = {}
	for aM in acceptedMarkers: merDict[int(aM[0])] = aM[1]
	
	goodMers2 = '%s/%s.acceptedMers2.fasta'%(currDir, baseName); gM2 = open('%s'%goodMers2,'w')
	for line in open('%s'%goodMers):
		if line[0] == '>': currNum = line.split(None)[0]; currNum = int(str(currNum[1:])); gM2.write('>%s\n'%(merDict[currNum]))
		else: gM2.write('%s'%line)
	gM2.close()
				
#==============================================================
def callAccCheck (target, readList, blatDict, blatPos, refFasta, readsDict, targetDict, baseName):
	
    readFastq = '%s.reads.fastq'%baseName; rF = open('%s'%readFastq,'w')
    targetFasta = '%s.preTarget.fasta'%baseName; tF = open('%s'%targetFasta,'w')
    print target
    targetName = target[0]; gapStart = target[1]; gapEnd = target[2]
    
    for rN in readList: rF.write('@%s\n%s\n'%(rN,readsDict[rN]))
    tF.write('>%s\n%s\n'%(targetName,targetDict[targetName]))
    rF.close(); tF.close()
    tmpString = ''
    for a in range(gapEnd-gapStart): tmpString += 'A'
    newTarget = '%s.target.fasta'%baseName
    nT = open('%s'%newTarget,'w')
    for z in iterFASTA(open('%s.preTarget.fasta'%baseName)):
        scaffold = str(z).split()[1]; sequence = str(z.seq); before = sequence[:gapStart]; after = sequence[gapEnd:]; sequence2 = before + tmpString + after
#         scaffold = str(z).split()[1]; sequence = str(z.seq); before = sequence[:gapStart]; after = sequence[gapEnd:]; sequence2 = sequence
        break
    nT.write('>%s\n%s'%(scaffold, sequence2))
    nT.close()
    
    dummySam = '%s.sam'%baseName
    makeDummySam ( dummySam, readList, gapStart, gapEnd, readsDict, blatPos, blatDict, targetName, targetDict, baseName)
    quiverCommands = '%squiver'%baseName
    qC = open('%s'%quiverCommands,'w')
    qC.write('#!/bin/sh\n\n')
    qC.write('. /opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/etc/setup.sh\n\n')
#     qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/blasr -sam -out %s.sam %s.reads.fastq %s -nproc 6 -minMatch 8\n'%(baseName, baseName, newTarget))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/samtoh5 %s.sam %s %s.cmp.h5\n'%(baseName, newTarget, baseName))
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/cmph5tools.py sort --inPlace %s.cmp.h5\n'%baseName)
    qC.write('/opt/smrtanalysis/install/smrtanalysis-2.1.1.128549/analysis/bin/quiver %s.cmp.h5 --referenceFilename %s --minCoverage 1 -C 1000000 --noEvidenceConsensusCall nocall 
--minConfidence 1 -o %s.output.fasta -o %s.variants.gff -d all --evidenceDirectory %sevidence -v --referenceWindow %s:%d-%d\n'%(baseName, newTarget, baseName, baseName, baseName, scaffold, 
gapStart, gapEnd))
    qC.close()
    system('chmod 777 %s'%quiverCommands)
    system('./%s'%quiverCommands)
#     system('rm %s; rm %s.target.fasta; rm %s.reads.fastq; rm %s.tmp.sam; rm %s.cmp.h5'%(quiverCommands, baseName, baseName, baseName, baseName))
    finalOutput = '%s.gapFilling.dat'%baseName; rawQuiver = '%s.rawQuiver.dat'%baseName
    fO = open('%s'%finalOutput,'a'); rQ = open('%s'%rawQuiver,'a')
    for x in iterFASTA(open('%s.output.fasta'%baseName)): consensusSequence = str(x.seq)
    fO.write('%s\t%d\t%d\n'%(scaffold, gapStart, gapEnd)); rQ.write('>%s_%d_%d\n%s\n'%(scaffold, gapStart, gapEnd, consensusSequence))
    fO.write('%s\n'%consensusSequence)
    for rN in readList: fO.write('%s\n'%rN)
    fO.close(); rQ.close()

#==============================================================
