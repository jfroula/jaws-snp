__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  3/23/15"

from sys import stdout

import subprocess

from os import curdir, system

from os.path import abspath, split, splitext, join

from math import fmod

import re

import time

#==============================================================
class recordClass(object):
    def __init__(self,id,seq,desc=''):
        self.id   = id
        self.seq  = seq
        self.desc = desc

#==============================================================
def writeRECORD( record, outputHandle, wrap=-1 ):
    if ( wrap > 0 ):
        outputHandle.write( '>%s %s\n'%( record.id, record.desc ) )
        for n in range( 0, len(record.seq), wrap ):
            outputHandle.write( '%s\n'%record.seq[n:n+wrap] )
        #####
    else:
        outputHandle.write( '>%s %s\n%s\n'%(record.id,record.desc,record.seq) )
    #####
    return

#==============================================================
def iterFASTA( tmpHandle ):
    # Skipping any blank lines or comments at the top of the file
    while True:
        line = tmpHandle.readline()
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        id = line[1:].split(None,1)[0]
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or (line[0] == '>') ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield recordClass( id, ''.join(tmpData) )
        
        # Stopping the iteration
        if ( not line ): return
    #####
    assert False, 'Should never reach this line!!'

#==============================================================
def twoXSelection( currDir, assemblyFASTA, baseName, merSize, mersPerFASTA ):
    
    # Breaking the sequence into overlapping 100mers
    findContigs = re.compile( r'[actgN]?([ACTG]+)[actgN]?' ).finditer
    halfSize    = merSize / 2
    merDict     = {}
    merSet      = set()
    for r in iterFASTA( open(assemblyFASTA) ):
        # Finding all of the contigs
        tmpSeq = str(r.seq)
        for nextContig in findContigs(tmpSeq):
            # Pulling the contig sequence
            contigSeq = nextContig.groups()[0]
            nBases    = len(contigSeq)
            start     = 0
            end       = merSize
            while ( end <= nBases ):
                tmpMer = contigSeq[start:end]
                if ( tmpMer in merSet ):
                    # If it is repeated, then throw it out
                    merDict.pop(tmpMer)
                else:
                    # Add the mer to the dictionary
                    merSet.add( tmpMer )
                    merDict[tmpMer] = recordClass('%s/%d'%(r.id,start),tmpMer)
                    start += halfSize
                    end   += halfSize
                #####
            #####
        #####
    #####
    
    # Writing them to a series of files
    merFileList = []
    merCounter  = 0
    fileCounter = 0
    tmpFastaOut = join( currDir, '%s.%d.100mer.fasta'%(baseName, fileCounter) )
    merFileList.append( tmpFastaOut )
    oh = open( tmpFastaOut, 'w' )
    for r in merDict.itervalues():
        if ( merCounter >= mersPerFASTA ):
            oh.close()
            fileCounter += 1
            tmpFastaOut = join( currDir, '%s.%d.100mer.fasta'%(baseName, fileCounter) )
            merFileList.append( tmpFastaOut )
            oh = open( tmpFastaOut, 'w' )
            merCounter = 0
        #####
        writeRECORD( r, oh )
        merCounter += 1
    #####
    oh.close()
    
    return merFileList

#==============================================================
def iterFASTQ( oh ):
    while ( True ):
        readID = oh.readline()[1:]
        seq    = oh.readline()
        dummy  = oh.readline()
        qual   = oh.readline()
        if ( not qual ): break
        yield { 'id':readID, 'seq':seq, 'qual':qual }
    #####
    return

#==============================================================
def writeRecord( r, oh ):
    oh.write( '>%s'%r['id'] )
    oh.write( '%s'%r['seq'] )
    return

#==============================================================
def jobsFinished():
    totalTime = 0
    timeInc   = 60
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        checker = 0
        p = subprocess.Popen( 'qstat -u jjenkins', shell=True, stdout=subprocess.PIPE )
        for line in p.stdout:
            checker += 1
        #####
        if checker == 0: break
        p.poll()
        stdout.write( 'Jobs running........%d seconds\n'%totalTime )
    #####
    stdout.write( 'Jobs Finished in %d seconds!!\n'%totalTime )
    return

#==============================================================
def runJobs( currDir, batchFileList, merFileList, referenceFASTA ):
    
    # Loading libraries
    system( 'module load uge' )

    # Executing the batch of files
    for batchFile in batchFileList:
        batchBase = splitext( split( batchFile )[-1] )[0]
        for merFile in merFileList:
            merBase = splitext( split( merFile )[-1] )[0]
            # Writing the file
            shFile = join( currDir, '%s_%s.sh'%(batchBase,merBase) )
            oh     = open( shFile,'w')
            oh.write( '#!/bin/bash\n' )
            oh.write( '#$ -l ram.c=1G\n' )
            oh.write( '#$ -l h_rt=0:55:00\n' )
            oh.write( '##$ -e /dev/null\n' )
            oh.write( '##$ -o /dev/null\n' )
            oh.write( 'module load blat\n' )
            oh.write( 'sleep 10\n' )
            
            # Base blat command for 100mer and read screening
            blatHeader = 'blat -noHead -extendThroughN -minIdentity=75 -minMatch=3 -tileSize=8 -maxIntron=10000 -repMatch=10000'
            
            # (1)  Align 100mers to reads and determine whether they are putatively contaminated
            contamReadsOutFile = '%s/%s_%s.contamReads'%(currDir,batchBase,merBase)            
            blatCmd            = '%s %s %s stdout'%(blatHeader, batchFile,merFile) 
            awkCmd             = 'awk \'{ if (($1 >= 80) && (($8) <= 20) && ($12 <= 15) && ($13 >= 85)) print }\''
            oh.write( 'rm %s\n'%(contamReadsOutFile) )
            oh.write( '%s | %s | cut -f 14 | sort | uniq -c | cut -b9- > %s\n'%(blatCmd,awkCmd,contamReadsOutFile) )

            # (2) Realign the reads against the reference contaminant to find true contaminants
            possContamReads = '%s/%s_%s.contam.readSet'%(currDir,batchBase,merBase)
            awkCmd          = "awk ' BEGIN{while(getline<\"%s\")readList[$1]=$1};{if(NR%%2==1){readID=substr($0,2)};if(NR%%2==0){if(readID in readList){print \">\"readID\"\\n\"$0}}}'"%contamReadsOutFile
            blatCmd         = '%s %s stdin stdout'%(blatHeader,referenceFASTA)
            screenCmd       = "awk -f /projectb/scratch/jjenkins/phaseolusScreening/bestHit.awk"
            oh.write( "rm %s\n"%possContamReads )
            oh.write( "cat %s | %s | %s | %s > %s\n"%(batchFile, awkCmd, blatCmd, screenCmd, possContamReads) )

            # (3)  Screen the alignments for final 
            oh.write( '\n' )
            
            oh.close()
        #####
    #####
    
    # Submitting the jobs
    system('for x in $(/bin/ls *.sh); do echo $x; qsub $x; done')

    #Check to see if all of the alignments are finished - that is no jobs in the queue
    jobsFinished()	

    # Removing the submit files
    system( 'rm -rf %s/*.sh %s/*.contamReads'%( currDir, currDir ) )
    
#==============================================================
def split_and_align_pacbio_reads( currDir, fastq_list, jobsPerBatch, basesPerBatch, merFileList, referenceFASTA ):
    
    # Splitting the PB reads into parsels and running the blat alignments
    # Looping over the files and splitting the reads
    totalBases    = 0
    fileCounter   = 0
    batchFileList = []
    for fastqFile in fastq_list:
        # Pulling the filename and extension
        fileBase, ext = splitext( split(fastqFile)[-1] )
        # Opening the batch file
        currFile      = join( currDir, '%s_%d.fasta'%(fileBase,fileCounter) )
        batchFileList.append( currFile ) 
        oh_current    = open( currFile, 'w' )
        # Splitting the PB reads into batches of "basesPerBatch"
        for r in iterFASTQ( open(fastqFile) ):
            # Check the total bases in the current file
            if ( totalBases >= basesPerBatch ):
                # Closing the current file
                oh_current.close()
                # Check to see if we need to run the jobs
                if ( (fileCounter > 0) and (fmod( fileCounter,jobsPerBatch) == 0.0) ):
                    # Run the jobs
                    runJobs( currDir, batchFileList, merFileList, referenceFASTA )

                    # Removing the temporary files and resetting the list
                    for batchFile in batchFileList:  system( 'rm -rf %s'%batchFile )
                    batchFileList = []
                    
                #####   
                # Open the next file
                fileCounter += 1
                currFile     = join( currDir, '%s_%d.fasta'%(fileBase,fileCounter) )
                batchFileList.append( currFile )
                oh_current   = open( currFile, 'w' )
                # Resetting the base counter
                totalBases   = 0
            #####
            # Writing the current record
            writeRecord( r, oh_current )
            totalBases += len( r['seq'] )
        #####
        # Closing the current file
        oh_current.close()
        fileCounter += 1
    #####
    
    # Running the jobs
    runJobs( currDir, batchFileList, merFileList, referenceFASTA )
    
    # Removing the temporary files and resetting the list
    for batchFile in batchFileList: system( 'rm -rf %s'%batchFile )
    
#==============================================================
def real_main():
    
    # Recode this to work one chip at a time.
    # Use the trick of cat-ing the reads to form the target
    # Then use an extract_seq_and_qual.
    
    # Driving parameters    
    currDir       = abspath(curdir)
    chloroFASTA   = '/projectb/scratch/jjenkins/phaseolusScreening/chloro.fasta'
    baseName      = 'test'
    merSize       = 100
    mersPerFASTA  = 1e5
    basesPerBatch = 1e9
    fastqFile     = 'pbFiles.dat'
    jobsPerBatch  = 100
    
    #-----------------------------------------------------
    # (1) Pulling the 2x set of 100mers and writing them to a file
    merFileList = twoXSelection( currDir, chloroFASTA, baseName, merSize, mersPerFASTA )
    
    #-----------------------------------------------------
    # (2) Split and align the markers to the pacbio reads
    fastq_list   = [line.strip() for line in open(fastqFile)]
    split_and_align_pacbio_reads( currDir, fastq_list, jobsPerBatch, basesPerBatch, merFileList, chloroFASTA )
    
#     # Performing the initial steps
#     initialPrep ( chloroFASTA, pbFasta, merSize, mersPerFASTA, pbNum, baseName, currDir, repeatLimit, oh_cmdLog )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
