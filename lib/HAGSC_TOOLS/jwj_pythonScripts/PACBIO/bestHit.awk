{

    strand  = $9
    Q_size  = $11
    Q_start = $12
    Q_end   = $13
    
    T_size  = $15
    T_start = $16
    T_end   = $17
    
    # Computing the total match size
    tmpMatch       = $1
    repMatch       = $3
    totalMatchSize = tmpMatch + repMatch
    
    # Size of match divided by shorter of read sizes
    Q_length = Q_end - Q_start
    T_length = T_end - T_start
    
    if ( strand == "+" ){
        if ( (Q_size-Q_end) > (T_size-T_end) ){
            Q_size = Q_end + ( T_size - T_end )
        }
        if ( Q_start > T_start ){
            Q_size = ( Q_size - Q_start ) + T_start
        }
    }
    
    if ( strand == "-" ){
        if ( Q_start > (T_size-T_end) ){
            Q_size = (Q_size - Q_start) + ( T_size - T_end )
        }
        if ( (Q_size-Q_end) > T_start ){
            Q_size = Q_end + T_start
        }
    }
    
    # ID = max % of identical bases in the placement length of the query/target
    denom = Q_length
    if ( T_length < Q_length ){ denom = T_length }
    ID = 100.0 * totalMatchSize / denom
    if ( ID > 100 ){ ID = 100 }
    
    # Coverage = max % of the total query/target length contained in the alignment
    ratio = Q_length / Q_size
    if ( (T_length/T_size) > ratio ){ ratio = T_length/T_size }
    COV = 100.0 * ratio
    
    # Checking alignment quality
    if ( (ID>=80) && (COV>=80) ){ print $10 }
    
}
