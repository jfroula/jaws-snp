__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  6/29/16"

from hagsc_lib import deleteFile, getFiles, deleteDir

from sys import stderr, argv

from os import system

import gzip

#==============================================================
def real_main():

    tmpPath        = argv[1]
    outputFileName = argv[2]

    stderr.write( '\t-Parsing output...\n')
    # Looping over gzip files
    deleteFile(outputFileName)
    oh = open( outputFileName, 'w' )
    for gzipFile in getFiles( 'gz', tmpPath ):
        gzipHandle = gzip.open(gzipFile)
        map( oh.write, gzipHandle )
        gzipHandle.close()
    #####
    oh.close()
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
