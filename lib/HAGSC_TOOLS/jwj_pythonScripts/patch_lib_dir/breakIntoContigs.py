#!/usr/common/usg/languages/python/2.7-anaconda/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import iterFASTA, writeFASTA, SeqRecord, iterCounter

import re

from os.path import realpath, isfile

from sys import stdout

from optparse import OptionParser

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [options]"

    parser = OptionParser(usage)

    defLength = 0
    parser.add_option( "-m", \
                       "--minContigLength", \
                       type    = 'int', \
                       help    = "Minimum contig length.  Default=%d"%defLength, \
                       default = defLength )

    def_Ns = 1
    parser.add_option( "-n", \
                       "--minimum_Ns", \
                       type    = 'int', \
                       help    = "Minimum number of Ns to break on.  Default=%d"%def_Ns, \
                       default = def_Ns )

    def_Ns = 'stdout'
    parser.add_option( "-o", \
                       "--outputFileName", \
                       type    = 'str', \
                       help    = "Output file name.  Default: Write to stdout", \
                       default = def_Ns )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        FASTA = realpath(args[0])
        if ( not isfile(FASTA) ): parser.error( '%s can not be found'%FASTA )
    #####
    
    # Reading in the minimum contig length
    minContigLength = options.minContigLength
    
    # Opening the output file
    if ( options.outputFileName == 'stdout' ):
        oh = stdout
    else:
        oh = open( options.outputFileName, 'w' )
    #####
    
    # Contig breaker
    contigBreakFinder = re.compile( r'N{%d,}'%options.minimum_Ns ).finditer
    x                 = iterCounter(10000)
    for record in iterFASTA( open(FASTA) ):
        # Pulling the sequence
        tmpSeq = str(record.seq)
        nBases = len(tmpSeq)
        # Creating the break indicees
        d = [item.span() for item in contigBreakFinder(tmpSeq)]
        d.insert(0,(0,0))
        d.append((nBases,nBases))
        nContig = 1
        for n in xrange(len(d)-1):
            start = d[n][1]
            stop  = d[n+1][0]
            contigSeq = tmpSeq[ start:stop ]
            if ( len(contigSeq) < minContigLength ): continue
            contigRecord = SeqRecord( id="%s|contig=%d"%(record.id,nContig), seq = contigSeq, description='' )
            writeFASTA( [contigRecord], oh )
            nContig += 1
        #####
        x()
    #####

    if ( options.outputFileName != 'stdout' ): oh.close()

#==============================================================
if ( __name__ == '__main__' ):
    real_main()


  
