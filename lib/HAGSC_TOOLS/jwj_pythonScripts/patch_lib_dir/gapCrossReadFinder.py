from sys         import stdin, argv, stdout
from os.path     import join
from collections import defaultdict
from hagsc_lib   import FASTAFile_dict

#==============================================================

class PB_Read_Class(object):
    def __init__(self, readID):
        self.readID  = readID
        self.gapDict = defaultdict()
        self.gapSet  = set()
        
    def add_gap_adj_kmer(self, tmpList):
        scaffID = tmpList[0]
        start   = tmpList[1]
        gapList = tmpList[2:]
        for gapID in gapList:
            if gapID != 'O':
                try:
                    gapSide, gap = gapID.split('_')
                except ValueError:
                    continue
                #####
                gapID = '%s_%s'%(scaffID, gap)
                if gapID not in self.gapSet: # Create a new gap object
                    self.gapSet.add( gapID )
                    self.gapDict[gapID] = Gap_Class( gapSide )
                else:
                    self.gapDict[gapID].add_kmer( gapSide )
                #####
            #####
        #####
    #####
        
    def return_gaps_crossed(self, minMappings):
        gaps_crossed        = []
        for gapID, gap in self.gapDict.iteritems():
            if ( gap.leftCount >= minMappings ) and ( gap.rightCount >= minMappings ):
                gaps_crossed.append(gapID)
            #####
        #####
        return gaps_crossed
    #####
     
class Gap_Class(object):
    def __init__(self, gapSide):
        self.leftCount  = 0
        self.rightCount = 0
        if gapSide == 'L':
            self.leftCount  += 1
        else:
            self.rightCount += 1
        #####
            
    def add_kmer( self,gapSide ):
        if gapSide == 'L':
            self.leftCount  += 1
        else:
            self.rightCount += 1
        #####
        
#==============================================================
def real_main():
    # Input necessary
    minMappings      = int(argv[1])
    tmpPath          = argv[2]
    pbReadList       = []
    previous_pbRead  = None
    for line in stdin:
        # Send data through while pulling out gap crossing info
        stdout.write(line)
        pbReadID = line.strip().split(None)[1]
        if (previous_pbRead == None):
            pbRead = PB_Read_Class( pbReadID )
        #####
        if (pbReadID != previous_pbRead) and (previous_pbRead != None): # Create new pbRead instance and add kmer if necessary
            pbReadList.append(pbRead)
            pbRead = PB_Read_Class( pbReadID )
        #####
        pbRead.add_gap_adj_kmer( (line.strip().split(None)[0]).split('|') )
        previous_pbRead = pbReadID
    #####
    oh = open(join(tmpPath,'gapCrossers.dat'),'a')
    for pbRead in pbReadList:
        gaps_crossed = pbRead.return_gaps_crossed( minMappings )
        for gapID in gaps_crossed:
            oh.write('%s\t%s\n'%( pbRead.readID, gapID ) )
        #####
    #####
    oh.close()

#==============================================================
if __name__ == "__main__":

    real_main()
