__author__='Robert Herring, rherring@hudsonalpha.org'
__date__ ='Created:  8/1/16'

import multiprocessing as mp
from glob import glob
import subprocess
import re
from os import getpid, system
from collections import defaultdict
from sys import argv
from hagsc_lib import iterFASTA
import pickle

def parse_file(tmpTuple):
    count, fn = tmpTuple
    print count
    p = subprocess.Popen("module load samtools; samtools view %s | awk '{print $1, $3, $17}'"%(fn), shell=True, stdout = subprocess.PIPE)
    d = defaultdict(set)
    for line in p.stdout:
        if len(line.strip().split()) == 3:
            readName, contig_flank, XAline = line.strip().split()
            if len(XAline) > 0:
                XAlist = XAline[5:].split(';')
                XA_pos_list = []
                for entry in XAlist:
                    try:
                        chr, pos, cigar, NM = entry.split(',')
                        XA_pos_list.append( chr )
                    #####
                    except ValueError:
                        continue
                    #####
                #####
                # Running though my secondary alignments
                for contig_flank in XA_pos_list:
                    contig_side = contig_flank[0]
                    scaffID     = (contig_flank[3:]).split('|')[0]
                    contigNum   = int( ((contig_flank[3:]).split('|')[1]).split('=')[1] )
                    d[(scaffID, contigNum, contig_side)].add(readName)
                #####
        elif len(line.strip().split())==2:
            readName, contig_flank = line.strip().split()
            # Example contig flank id > R__scaffold1|contig=2
            contig_side = contig_flank[0]
            scaffID     = (contig_flank[3:]).split('|')[0]
            contigNum   = int( ((contig_flank[3:]).split('|')[1]).split('=')[1] )
            d[(scaffID, contigNum, contig_side)].add(readName)
        #####
    #####
    return d

#==============================================================
def real_main():
    
    tmpPath          = argv[1]
    assemblyFilePath = argv[2]

    files = [x for x in glob('%s/bam/*bam'%(tmpPath))]
    files_count_list = [(i, fn) for i, fn in enumerate(files)]
    
    # Nothing to see here
    pool     = mp.Pool(processes=32)
    results  = pool.imap(parse_file, files_count_list)
    pool.close()
    pool.join()
    aln_dict = defaultdict(set)

    print "Iterating through dictionaries..."
    count = 0
    for tmpD in results:
        print count
        for key, tmpSet in tmpD.iteritems():
            aln_dict[key] = aln_dict[key].union(tmpSet)
        #####
        count += 1
    #####
        
    gapFinder     = re.compile( r'[ATCGatcg][Nn]+[ATCGatcg]' ).finditer
    
    oh = open('%s/reads_to_gaps.dat'%(tmpPath), 'w')

    reads_to_gaps_dict = defaultdict(set)
    
    # Adding in my gap crossers from blat as well
    system('find %s -name "*gap.blat" -exec cat {} >> %s/allGaps.blat \;'%(tmpPath, tmpPath))
    for line in open('%s/allGaps.blat'%(tmpPath)):
        gapID, readName = line.strip().split()
        reads_to_gaps_dict[gapID].add(readName)
    #####
    
    # Now I have a dictionary keyed by tuples of (scaffID, contig_number, contig_side) storing a set of reads that align to it
    for r in iterFASTA(open(assemblyFilePath)):
        for i, gapSpan in enumerate([x.span() for x in gapFinder(str(r.seq))]):
            scaffID     = r.id
            gapNum      = i + 1
            leftContig  = gapNum
            rightContig = gapNum + 1
            leftReads   = aln_dict[(scaffID, leftContig, 'W')].union(aln_dict[(scaffID, leftContig, 'R')])
            rightReads  = aln_dict[(scaffID, rightContig, 'L')].union(aln_dict[(scaffID, rightContig, 'W')])
            readSet     = leftReads.intersection(rightReads)
            reads_to_gaps_dict['%s_%s'%(scaffID, gapNum)].update( readSet )
            reads       = reads_to_gaps_dict['%s_%s'%(scaffID, gapNum)]
            if len(reads) > 0:
                for read in reads:
                    oh.write('%s_%s\t%s\n'%(scaffID, gapNum, read))
                #####
            #####
        #####
    #####
    
    oh.close()
    
    pickle.dump( reads_to_gaps_dict, open( "%s/reads_to_gaps_dict.pickle"%(tmpPath), "wb" ) )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
