__author__='Robert Herring, rherring@hudsonalpha.org'
__date__ ='Created:  4/11/16'


from sys import stdin, argv, stdout

#==============================================================
def best_extent( qsize, blksizes, blkstarts, querySizeMultiple ):
    # Starting information
    startpt = blkstarts[0]
    endpt   = blkstarts[-1] + blksizes[-1]  # blksizes is already sorted!

    # Use entire match if not more than twice query size
    if ( (endpt - startpt) <= (2 * qsize)  ):
        return startpt, endpt
    #####

    # Otherwise, pick "best" blocks totaling less than a "querySizeMultiple"
    # of query size.  Start with the biggest block!
    nBlks     = len(blksizes)
    DSU       = [ (blksizes[n],n) for n in xrange( nBlks ) ]
    DSU.sort( reverse=True )
    max_b     = DSU[0][1]
    startpt   = blkstarts[max_b]
    endpt     = startpt + blksizes[max_b]

    # Extend to smaller block of left or right;
    # repeat until 2X of query size would be exceeded
    leftb  = max_b - 1
    rightb = max_b + 1
    while ( (leftb>=0) or (rightb<nBlks) ):
        # Left extension
        newstart = ( (leftb<0) and [startpt] or [blkstarts[leftb]] )[0]
        leftAddLength = endpt - newstart
        # Right extension
        newend = ( (rightb==nBlks) and \
                   [endpt] or \
                   [blkstarts[rightb] + blksizes[rightb]] )[0]
        rightAddLength = newend - startpt
        # Decision time
        if ( (rightb<nBlks) and ( (leftb<0) or (rightAddLength < leftAddLength) ) ):
            if ( rightAddLength <= (querySizeMultiple*qsize) ):
                endpt = newend
                rightb += 1
            else:
                rightb = nBlks # Terminal condition
            #####
        elif ( leftAddLength <= (querySizeMultiple*qsize) ):
            startpt = newstart
            leftb -= 1
        else:
            leftb = -1  # Terminal condition
        #####
    ##### end while loop
    return startpt, endpt
    
    
#==============================================================
def real_main():
    gapLeft       = int(argv[1])
    gapRight      = int(argv[2])
    for line in stdin:
        splitLine = (line.strip()).split(None)
      
        readLength = int(splitLine[10])
        
        # First time for the target
        try: 
            trueStart, trueEnd = best_extent( int(splitLine[10]), [int(lS) for lS in splitLine[18].split(',') if lS != ''], [int(lS) for lS in splitLine[20].split(',') if lS != ''], 2 )
        except IndexError: continue
        #####
        
        # Second time for the query
        qtrueStart, qtrueEnd = best_extent( (trueEnd - trueStart), [int(lS) for lS in splitLine[18].split(',') if lS != ''], [int(lS) for lS in splitLine[19].split(',') if lS != ''], 2 )
      
        # Correcting for a negative strand
        if splitLine[8] == '-': 
            tmp = qtrueStart
            qtrueStart = (int(splitLine[10]) - qtrueEnd)
            qtrueEnd = qtrueStart + (qtrueEnd - tmp)
        #####

        # Alignment has to be at least 1/3 of the read length
        if qtrueEnd - qtrueStart < int(splitLine[10])/3: continue
        
        # Number of matches must be at least 1/2 of the read alignment length
        if int(splitLine[0]) < (qtrueEnd - qtrueStart) * 0.5: continue
            
        # Must anchor at least max(500bp, 0.20*ReadLength) on both sides of the gap
        readThreshold = max(500, 0.20*readLength)
        if gapLeft - readThreshold < trueStart: continue
        if gapRight + readThreshold > trueEnd: continue
        
        
        # Upon passing checkpoints, write out query name
        stdout.write('%s\n'%( splitLine[9] ))

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
