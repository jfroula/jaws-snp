__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/24/15"

import subprocess

from sys import stdout, stderr

from os import system, mkdir, chdir

from os.path import join

import time

#==============================================================
def jobFinished( job_ID_set ):
    # Adding this for arrays
    for job_ID in job_ID_set:
        if '.' in job_ID:
            new_job_ID = job_ID.split('.')[0]
            job_ID_set.remove(job_ID)
            job_ID_set.add(new_job_ID)
        #####
    #####
    totalTime = 0
    timeInc   = 10
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        checkerSet = set()
        p = subprocess.Popen( 'qstat', shell=True, stdout=subprocess.PIPE )
        checkerSet = set( [line.split(None)[0] for line in p.stdout if (line[0] not in 'j-')] )
        if len(job_ID_set.intersection(checkerSet)) == 0: break
        p.poll()
#         stderr.write( 'Jobs running........%d seconds\n'%totalTime )
    #####
    stderr.write( 'Jobs Finished in %d seconds!!\n'%totalTime )
    return

#==============================================================
def submitJob( shFileName ):
    # Submitting the job
    result = subprocess.check_output( 'qsub %s'%shFileName, stderr=subprocess.STDOUT, shell=True )
    
    # Capturing the output
    job_ID = result.split(None)[2]
    
    # Notifying the user
    stderr.write( 'JOB_ID=%s\n'%job_ID )

    return job_ID

#==============================================================
def genePool_submitter( cmdList, Time, Memory, shFileName, suppressOutput=False, delete_sh_file=True, waitUntilDone=True, emailAddress='rherring@hudsonalpha.org' ):
    
    # Building the submission file
    oh        = open( shFileName, 'w' )
    oh.write( '#!/bin/bash\n')
#     oh.write( '## specify an email address\n')
#     oh.write( '#$ -M %s\n'%emailAddress)
#     oh.write( '## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally\n')
#     oh.write( '#$ -m ae\n')
    oh.write( '#$ -cwd\n')
    oh.write( '#$ -l h_rt=%s\n'%Time)
    oh.write( '#$ -l ram.c=%s\n'%Memory )
    
    # Suppresses output to the user directory
    if ( suppressOutput ):
        oh.write( '#$ -e /dev/null\n' )
        oh.write( '#$ -o /dev/null\n' )
    #####
    oh.write( '## Job Starts Here\n')
    map( oh.write, ['%s\n'%cmd for cmd in cmdList] )
    oh.close()
    
    # Submitting the job
    job_ID = submitJob( shFileName )
    
    # Wait until the job is finished
    if ( waitUntilDone ):
        #Check to see if all of the alignments are finished - that is no jobs in the queue
        jobFinished( set([job_ID]) )
        # Removing the submission file
        if ( delete_sh_file ):  system( 'rm -f %s'%shFileName )
    #####
    
    return job_ID
    
#==============================================================
def genePool_array_submitter( tmpPath, basePath, jobType, cmdsList, Time, Memory, shFileName, suppressOutput=True, delete_sh_file=True, waitUntilDone=False, n_procs=1, conc_jobs=3000, emailAddress='rherring@hudsonalpha.org' ):
    
    # Creating a directory for the individual sh files
    array_files_path = join(tmpPath,'%s_array_files'%jobType)
    mkdir( array_files_path )
    jobCount = 0
    for cmdList in cmdsList:
        oh = open('%s/%s_%s.sh'%(array_files_path, jobType, jobCount), 'w')
        oh.write( '#!/bin/bash\n')
#         oh.write( '## specify an email address\n')
#         oh.write( '#$ -M %s\n'%emailAddress)
#         oh.write( '## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally\n')
#         oh.write( '#$ -m ae\n')
        oh.write( '#$ -l h_rt=%s\n'%Time)
        oh.write( '#$ -l ram.c=%s\n'%Memory )
        # Suppresses output to the user directory
        if ( suppressOutput ):
            oh.write( '#$ -e /dev/null\n' )
            oh.write( '#$ -o /dev/null\n' )
        #####
        oh.write( '## Job Starts Here\n')
        map( oh.write, ['%s\n'%cmd for cmd in cmdList] )
        oh.close()
        jobCount += 1
        
    # Writing script that submits array
    oh = open( shFileName, 'w' )
    oh.write( '#!/bin/bash\n')
#     oh.write( '## specify an email address\n')
#     oh.write( '#$ -M %s\n'%emailAddress)
#     oh.write( '## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally\n')
#     oh.write( '#$ -m ae\n')
    oh.write( '#$ -l h_rt=%s\n'%Time)
    oh.write( '#$ -l ram.c=%s\n'%Memory )
    if int(n_procs) > 1:
        oh.write( '#$ -pe pe_slots %s\n'%n_procs )
    oh.write( '#$ -cwd\n' )
    # Suppresses output to the user directory
    if ( suppressOutput ):
        oh.write( '#$ -e /dev/null\n' )
        oh.write( '#$ -o /dev/null\n' )
    #####
    oh.write('#$ -t 1-%s\n'%jobCount)
    oh.write('#$ -tc %s\n'%conc_jobs)
    oh.write('filename=`ls -1 %s | tail -n +$SGE_TASK_ID | head -1`\n'%(array_files_path))
    oh.write('bash %s/$filename\n'%array_files_path)
    oh.close()

    # Submitting the job
    job_ID = submitJob( shFileName )
    
    # Wait until the job is finished
    if ( waitUntilDone ):
        #Check to see if all of the alignments are finished - that is no jobs in the queue
        jobFinished( set([job_ID]) )
        # Removing the submission file
        if ( delete_sh_file ):  system( 'rm -f %s'%shFileName )
        
    return job_ID
    

#==============================================================
def genePool_keep_busy(N_CONC_JOBS, cmdList, tmpPath, jobType, maxTime, maxMemory, email_Address):
    # Make sure jobType is small enough to be seen by qstat
    jobCount = 0
    for x in range( min( int(N_CONC_JOBS), len(cmdList) ) ):
        tmpCmdList = []
        tmpCmdList.append(cmdList.pop())            
        shFileName = '%s_%s.sh'%(jobType,jobCount)
        genePool_submitter( tmpCmdList, maxTime, maxMemory, shFileName, suppressOutput=False, delete_sh_file=True, waitUntilDone=False, emailAddress=email_Address )
        jobCount += 1
    while (len(cmdList) > 0):
        running = int( subprocess.check_output('qstat | grep %s | wc -l'%jobType, shell=True) )
        if running < int(N_CONC_JOBS):
            for x in range( int(N_CONC_JOBS) - running ):
                tmpCmdList = []
                tmpCmdList.append(cmdList.pop())
                shFileName = '%s_%s.sh'%(jobType,jobCount)
                genePool_submitter( tmpCmdList, maxTime, maxMemory, shFileName, suppressOutput=False, delete_sh_file=True, waitUntilDone=False, emailAddress=email_Address )
                jobCount += 1
            #####
        #####
        time.sleep(30)
        
#==============================================================
class gp_queue():

    def __init__(self, Qname):
        self.name = Qname
        self.waiting = 0
        self.running = 0
        
    def add_job( self, status, array ):
        if status == "qw":
            self.waiting += 1
        #####
        elif status == "r":
            self.running += 1
        #####
        
    def waiting_percent( self ):
        return self.waiting/(float(self.running + self.waiting))
    


    
    
    
    
    
    
    
