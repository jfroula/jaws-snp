#!/usr/common/usg/languages/python/2.7.4/bin/python
__author__='Robert Herring, rherring@hudsonalpha.org'
__date__ ='Created:  11/25/15'

from gp_lib import genePool_submitter, genePool_queue_limiter, jobFinished, create_sh_file

from patch_lib import parseConfigFile, splitPBreads, returnTriangleCmds, kmerizeAssembly, \
                      returnBWAcmds, returnCanuCmds, return_patch_align_cmd, parseSam, \
                      make_patches, returnKmerBlatCmds, returnGapCrossingBlatCmds

from sys import argv

from os.path import join

from os import chdir, system

from datetime import datetime

import subprocess
#==============================================================
def genePool_wait(job_ID, configFile, configDict, progPath, basePath, tmpPath, nextStep, quit):
    
    # Waiting to submit next job
    jobFinished(set([job_ID]))
    cmdList   = []
    maxTime   = "12:00:00"
    maxMemory = '1G'
    sh_File   = join( basePath, 'patchJobLauncher_%s.sh'%configDict['RUN_ID'] )
    cmdList.append( "python %s/patchJobLauncher.py %s %s %s %s %s %s"%(progPath, progPath, basePath, tmpPath, configFile, nextStep, quit) )
    genePool_submitter( cmdList, maxTime, maxMemory, sh_File, suppressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )

#==============================================================
def real_main():

    # Read the basic information
    basePath     = argv[1]
    tmpPath      = argv[2]
    configFile   = argv[3]
    gapStep      = argv[4]
    
    # Changing to the base directory
    chdir( basePath )

    # Read the config file
    configDict, cmd_log = parseConfigFile( configFile, basePath )

    # Run the commands
    if ( gapStep == 'splitPBreads' ):
            
        # Setting the commands
        nextStep = 'removeTriangles'
        
        # Job parameters
        maxTime       = '1:30:00'
        maxMemory     = '1G'
        jobLimit      = 100
        supressOutput = False

        # Writing the sh files
        jobList = []
        cmdList = splitPBreads( tmpPath, configDict['PB_FOFN'], configDict['MAX_PB_BASES'], cmd_log )
        nItems  = len(cmdList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "splitPBreads_%d.sh"%n )
            create_sh_file( [ ''.join(cmdList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
        
        # Launching the next job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )


    if ( gapStep == 'removeTriangles' ):

        # Setting the commands
        nextStep  = 'kmerizeAssembly'

        # Job parameters
        maxTime   = '11:00:00'
        maxMemory = '5G'
        jobLimit      = 100
        supressOutput = False

        # Writing the sh files
        jobList = []
        cmdList = returnTriangleCmds( tmpPath )
        nItems  = len(cmdList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "removeTriangles_%d.sh"%n )
            create_sh_file( [ ''.join(cmdList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
        
        # Launching the next job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )
        
    elif ( gapStep == 'kmerizeAssembly' ):
        
        # Setting the commands
        nextStep = 'blatKmers'
        cmdList  = kmerizeAssembly( configDict['GAP_DISTANCE'], configDict['MASKED_ASSEMBLY'], configDict['MIN_SCAFF_SIZE'], configDict['KMER_SIZE'], configDict['MAX_ISLAND_SIZE'], tmpPath, cmd_log, nextStep, quit )
        
        # Running the job
        maxTime   = "8:00:00"
        maxMemory = '40G'
        sh_File   = join( basePath, 'kmerizeAssembly_%s.sh'%configDict['RUN_ID'] )
        job_ID    = genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=True, emailAddress=configDict['EMAIL'] )
        
        # Launching the next job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )

    elif ( gapStep == 'blatKmers' ):
    
        # Setting the commands
        nextStep      = 'blatGaps'
        blatCmdsList  = returnKmerBlatCmds( tmpPath, cmd_log, basePath, configDict['ASSEMBLY'], configDict['MIN_MAPPINGS'] )  
        
        # Setting up the jobs
        jobLimit      = 100
        supressOutput = False
        maxTime       = '1:00:00'
        maxMemory     = '1G'
        jobList       = []
        
        nItems  = len(blatCmdsList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "kmerBlats_%d.sh"%n )
            create_sh_file( [ ''.join(blatCmdsList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
        
        # Launching the next job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )
        
    elif ( gapStep ==  'blatGaps'):
        
        # Setting the commands
        nextStep = 'alignPB'
        blatCmdsList = returnGapCrossingBlatCmds( basePath, configDict['ASSEMBLY'], tmpPath, configDict['PB_FOFN'], cmd_log, configDict['MAX_READS_TO_GAP'], configDict['MAX_ISLAND_SIZE'], configDict['REQUIRED_CANU_COV'] )
        
        # Setting up the jobs
        jobLimit      = 100
        supressOutput = False
        maxTime       = '12:00:00'
        maxMemory     = '1G'
        jobList       = []
        nItems        = len(blatCmdsList)
        
        for n in xrange(nItems):
            shFileName = join( tmpPath, "gapBlats_%d.sh"%n )
            create_sh_file( [ ''.join(blatCmdsList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
        
        # Launching the job   
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )
    
    if ( gapStep == 'alignPB' ):
        
        # Setting the commands
        nextStep      = 'createMap'
        cmdList       = returnBWAcmds( tmpPath, cmd_log, basePath, configDict['ASSEMBLY'], configDict['N_PROC_FOR_BWA'] )
        
        # Setting up the jobs
        jobList       = []
        jobLimit      = 100
        supressOutput = False
        maxTime       = '01:00:00'
        maxMemory     = '5G'
        
        nItems  = len(cmdList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "bwa_mem_%d.sh"%n )
            create_sh_file( [ ''.join(cmdList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
           
        # Launch the the job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )

    if ( gapStep == 'createMap' ):
        
        # Setting the commands
        nextStep  = 'runCanu'
        cmdList   = ["python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/readBams.py %s %s"%(tmpPath, configDict['ASSEMBLY'])]
        
        # Running the job
        maxTime   = '3:00:00'
        maxMemory = '5G'
        jobType   = 'createMap'
        sh_File   = join( basePath, 'createMap_%s.sh'%configDict['RUN_ID'] )
        
        job_ID    = genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=True, emailAddress=configDict['EMAIL'] )
        
        # Launch the the job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )

    elif ( gapStep == 'runCanu' ):
        
        nextStep = 'alignPatches'
        
        # Setting the commands
        cmdList  = returnCanuCmds( basePath, tmpPath, configDict['ASSEMBLY'], configDict['PB_FOFN'], configDict['MIN_SCAFF_SIZE'], configDict['REQUIRED_CANU_COV'], configDict['PB_FOFN'], configDict['N_PROC_FOR_CANU'], cmd_log )
            
        # Setting up the jobs
        jobList       = []
        jobLimit      = 100
        supressOutput = False
        maxTime       = '11:59:00'
        maxMemory     = '1G'
        
        nItems  = len(cmdList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "canuJob_%d.sh"%n )
            create_sh_file( [ ''.join(cmdList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
           
        # Launch the the job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )
    
    elif ( gapStep == 'alignPatches' ):
        
        nextStep = 'patchAssembly'

        # Setting the commands
        align_cmd = return_patch_align_cmd( basePath, tmpPath, configDict['ASSEMBLY'] )
        
        cmdList  = [[align_cmd]]
            
        # Running the job
        jobList       = []
        jobLimit      = 100
        supressOutput = False
        maxTime       = '11:59:00'
        maxMemory     = '1G'
        
        nItems  = len(cmdList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "patchAlign_%d.sh"%n )
            create_sh_file( [ ''.join(cmdList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
                
        # Launch the the job
        cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, nextStep ) ]
        sh_File   = join( basePath, 'runNextJob_%s.sh' % gapStep )
        maxTime   = "1:00:00"
        maxMemory = '1G'
        genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )


    elif ( gapStep == 'patchAssembly' ):
        
        parseSam(basePath, tmpPath, configDict['ASSEMBLY'])

        system('mkdir %s/new_scaffs'%(tmpPath))
        
        # Setting the commands
        cmdList = []
        cmdList.append( ["python -c\"import patch_lib; patch_lib.make_patches('%s', '%s', '%s')\""%( tmpPath, basePath, configDict['ASSEMBLY'] )] )
            
        # Running the job
        maxTime   = '24:00:00'
        maxMemory = '1G'
        jobType   = 'patching'
        
        supressOutput = False
        jobList       = []
        jobLimit      = 100
        
        nItems  = len(cmdList)
        for n in xrange(nItems):
            # Adding the job to a file
            shFileName = join( tmpPath, "patching_%d.sh"%n )
            create_sh_file( [ ''.join(cmdList[n]) ], maxTime, maxMemory, shFileName, supressOutput, emailAddress=None )
            jobList.append( shFileName )
        #####
        # Running the jobs
        genePool_queue_limiter( jobList, jobLimit )
    #####
        #job_ID    = genePool_array_submitter( tmpPath, basePath, jobType, cmdsList, maxTime, maxMemory, sh_File, suppressOutput=False, delete_sh_file=True, n_procs=32, waitUntilDone=True, emailAddress=configDict['EMAIL'] )
        

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
