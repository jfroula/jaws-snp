#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import parseConfigFile, writeAndEcho, throwError, jobTypeSet, runPath
from snp_lib import isBad_READ_REGEX

from gp_lib import genePool_submitter

from optparse import OptionParser

from os.path import abspath, join

from os import curdir, system

from sys import stdout

import sys

#==============================================================
def real_main():
    
    # Defining the program options
    usage  = "usage: %prog [options]"

    parser = OptionParser(usage)

    restartJob_def = 'align_and_split'
    parser.add_option( "-s", \
                       "--restartJob", \
                       type    = 'str', \
                       help    = "Start at this point in the pipeline.  Default: %s"%restartJob_def, \
                       default = restartJob_def )

    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Finding the base path
    basePath = abspath( curdir )
    
    # Setting the config file name
    configFile = join( basePath, 'snps.config' )
    
    # Read the config file
    configDict, cmd_log = parseConfigFile( configFile, basePath )
    
    # Creating the initial BAM file to start the compuation off
    if ( options.restartJob not in jobTypeSet ):
        throwError( '%s is not a valid step in the SNP/INDEL calling pipeline'%options.restartJob )
    #####
    nextStep = options.restartJob
    
    # Checking for interleaved reads or not
    if ( len(configDict['READS']) == 2 ):
        throwError("ALIGNER CAN ONLY ACCEPT INTERLEAVED READS")
    #####
    
    # Checking the read regular expression
    stdout.write( 'Checking the validity of the read REGEX....\n' )
    tmpRegex, tmpReadID, isBad_REGEX = isBad_READ_REGEX( configDict['READ_REGEX'], configDict['READS'][0], configDict['COMBINED_FASTQ'] )
    if ( isBad_REGEX ):
        throwError( 'The READ_REGEX does not match the readID\n---\nREGEX\t%s\nREADID\t%s' % ( tmpRegex, tmpReadID ) )
    else:
        stdout.write( 'The READ_REGEX matches the readID\n---\nREGEX\t%s\nREADID\t%s' % ( tmpRegex, tmpReadID ) )
    #####
    
    # Indexing the assembly
#    cmdList     = ["source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV"]
#    cmdList     = ["conda activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/SNP_ENV"]
    cmdList = []
    genomeFASTA = configDict['REFERENCE']
    samIndex    = 'samtools faidx %s'%(genomeFASTA)
    picardDict  = 'picard CreateSequenceDictionary R=%s O=%s.dict' % (genomeFASTA, genomeFASTA)
    altName     = '.'.join( genomeFASTA.split('.')[:-1] )
    altDict     = 'picard CreateSequenceDictionary R=%s O=%s.dict' % (genomeFASTA,altName)
    
    # Building the command set
    cmdList.append( samIndex   )
    cmdList.append( picardDict )
    cmdList.append( altDict    )
    cmdList.append( 'python %s/snpCalling/snpJobLauncher.py %s %s %s'%( runPath, basePath, configFile, nextStep) )
    
    # Writing to the log file
    writeAndEcho( '\n-----------------------\n- Starting the computation:', cmd_log )
    for cmd in cmdList: writeAndEcho( cmd, cmd_log )
    
    # Running the job
    maxTime   = "00:30:00"
    maxMemory = '2G'
    sh_File   = join( basePath, '%s_snpStarterJob.sh'%configDict['RUNID'] )
    genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, ncores=1 )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
    
