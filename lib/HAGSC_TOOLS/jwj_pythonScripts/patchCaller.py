#!/usr/common/usg/languages/python/2.7.4/bin/python
__author__='Robert Herring, rherring@hudsonalpha.org'
__date__ ='Created:  11/25/15'

from patch_lib import parseConfigFile, writeAndEcho, throwError, jobTypeSet, CreateOrFindTmpPath

from gp_lib import genePool_submitter

from os.path import abspath, join

from optparse import OptionParser

from os import getcwd

#==============================================================
def real_main():
    
    # Defining the program options
    usage  = "usage: %prog [options]"

    parser = OptionParser(usage)

    restartJob_def = 'splitPBreads'
    parser.add_option( "-s", \
                       "--restartJob", \
                       type    = 'str', \
                       help    = "Start at this point in the pipeline.  Default: %s"%restartJob_def, \
                       default = restartJob_def )
                       
    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    # Finding the base path
    basePath = getcwd()
    
    # Finding or creating the tmp dir
    tmpPath = CreateOrFindTmpPath(basePath)
    
    # Setting the config file name
    configFile = join( basePath, 'patch.config' )
    
    # Read the config file
    configDict, cmd_log = parseConfigFile( configFile, basePath )
    
    # Starting off the pipeline with chosen job
    if ( options.restartJob not in jobTypeSet ):
        throwError( '%s is not a valid step in the PACBIO patching pipeline'%options.restartJob )
    #####
    stepString = options.restartJob
    cmdList    = [ '%s %s %s %s %s'%( 'python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/patch_lib_dir/patchJobLauncher.py', basePath, tmpPath, configFile, stepString ) ]
    
    # Writing to the log file
    writeAndEcho( '\n-----------------------\n- Starting the computation:', cmd_log )
    for cmd in cmdList: writeAndEcho( cmd, cmd_log )

    sh_File   = join( basePath, 'patchStarterJob_%s.sh'%configDict['RUN_ID'] )
    maxTime   = "12:00:00"
    maxMemory = '1G'
    
    genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=configDict['EMAIL'] )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()





















