__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/17/17"

from hagsc_lib import deleteDir, deleteFile, iterCounter, testDirectory

from os.path import isdir, join, split, splitext

from os import mkdir, system

from sys import stdout, stderr

import subprocess

#==============================================================
def computeAverage( x ):
    if ( len(x) == 0 ): return 0.0
    return sum(x) / float( len(x) )

#==============================================================
def create_tmp_dir( tmpDirPath ):
    stderr.write( '\n\t-Creating tmp directory: %s\n'%tmpDirPath)
    deleteDir( tmpDirPath )
    mkdir( tmpDirPath, 0777 )
    testDirectory( tmpDirPath )
    return

#==============================================================
def get_read_sizes( fileName ):
    return [ len( r.seq ) for r in univIter( fileName )]

#==============================================================
class fastqRecord(object):
    def __init__(self,id,seq,qual):
        self.id   = id
        self.seq  = seq
        self.qual = qual

#==============================================================
class fastaRecord(object):
    def __init__(self,id,seq):
        self.id   = id
        self.seq  = seq

#==============================================================
def iterFASTA_local( line1, tmpHandle ):
    # Skipping any blank lines or comments at the top of the file
    line = str( line1 )
    while True:
        if ( not line ): return
        if ( line == "" ):
            raise ValueError( 'Empty line prior to first record encountered.  Exiting.\n')
            exit()
        #####
        if ( line[0] == ">" ): break
        line = tmpHandle.readline()
    #####
    # Pulling the data
    while True:
        if ( line[0] != ">" ):
            raise ValueError( "Records in Fasta files should start with '>' character" )
        #####
        # Pulling the id
        id = line[1:].split(None,1)[0]
        # Reading the sequence
        tmpData = []
        line    = tmpHandle.readline()
        while True:
            # Termination conditions
            if ( (not line) or (line[0] == '>') ): break
            # Adding a line
            tmpData.append( line.strip() )
            line = tmpHandle.readline()
        #####
        yield fastaRecord( id, ''.join(tmpData) )
        
        # Stopping the iteration
        if ( not line ): return
    #####
    assert False, 'Should never reach this line!!'

#==============================================================
def iterFASTQ_local( tmpHandle ):
    # Skipping any blank lines or comments at the top of the file
    while True:
        id   = tmpHandle.readline()[1:-1]
        seq  = tmpHandle.readline()[:-1]
        d    = tmpHandle.readline()[0]
        qual = tmpHandle.readline()[:-1]
        if ( not qual ): break
        if ( d != '+' ):
            stdout.write( 'SOMETHING WENT HORRIBLY WRONG\n')
            stdout.write( '%s\n'%id )
            stdout.write( '%s\n'%seq )
            stdout.write( '%s\n'%d )
            stdout.write( '%s\n'%qual )
            assert False
        #####
        yield fastqRecord( id, seq, qual )
    #####
    assert False, 'Should never reach this line!!'

#==============================================================
def univIter( fileName ):
    
    # Determining the file type
    ext = fileName.split('.')[-1]
    
    # Determining the compression type
    if ( ext == 'bam' ):
        p  = subprocess.Popen( 'samtools view %s | awk \'{print ">"$1"\\n"$10}\'' % fileName, shell=True, stdout=subprocess.PIPE )
        oh = p.stdout
    elif ( ext == 'bz2' ):
        p  = subprocess.Popen( 'cat %s | bunzip2 -c'%fileName, shell=True, stdout=subprocess.PIPE )
        oh = p.stdout
    elif ( ext == 'gz' ):
        p  = subprocess.Popen( 'cat %s | gunzip -c'%fileName, shell=True, stdout=subprocess.PIPE )
        oh = p.stdout
    else:
        oh = open( fileName )
    #####
    
    stdout.write( "\n\t---------------\n" )
    stdout.write( "\t-FILENAME:    %s\n"%fileName )
    stdout.write( "\t-EXTENSION:   %s\n"%ext )
    
    # Determining which iterator to use:
    line1 = oh.readline()
    if ( line1[0] == '@' ):
        
        stdout.write( "\t-FILE_FORMAT: FASTQ\n" )
        id   = line1[1:-1]
        seq  = oh.readline()[:-1]
        d    = oh.readline()[0]
        qual = oh.readline()[:-1]
        yield fastqRecord( id, seq, qual )
        for r in iterFASTQ_local( oh ): yield r
        
    elif ( line1[0] == '>' ):
        
        stdout.write( "\t-FILE_FORMAT: FASTA\n" )
        for r in iterFASTA_local( line1, oh ): yield r
        
    else:
        
        stdout.write( "SOMETHING WENT HORRIBLY WRONG\n" )
        stdout.write( '%s\n'%fileName )
        stdout.write( '%s\n'%ext )
        stdout.write( '%s\n'%line1 )
        assert False
        
    #####
    
    # Closing the file handles
    if ( ext in ['bam', 'bz2', 'gz'] ):
        p.poll()
    else:
        oh.close()
    #####
    
    return 

#==============================================================
def run_cmd(cmd):
    p = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    p.wait()

#==============================================================
def splitPBread_cmdGen( tmpDirPath, inputFiles, MAX_PB_BASES, inputBaseName=None ):
    # Generating a list of command lists for each pb file
    cmdList = []
    envCMD    = "source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/BLAT_ENV/"
    for inputFile in inputFiles:
        if ( inputBaseName == None ):
            baseName = splitext(split(inputFile)[-1])[0]
        else:
            baseName = inputBaseName
        #####
        splitFASTA_FOFN = join( tmpDirPath, 'splitFASTA_%s.dat'%baseName )
        input           = [ tmpDirPath, inputFile, MAX_PB_BASES, splitFASTA_FOFN, inputBaseName ]
        input_string    = ','.join( [r"'%s'"%item for item in input] )
        cmd             = 'python -c "from pacbio_lib import splitPBfile ; splitPBfile({0})"'.format( input_string )
        cmdList.append( "%s;%s" % ( envCMD, cmd ) )
    #####
    return cmdList
    
#==============================================================
def splitPBfile( tmpDirPath, fileName, basePerBatch, splitFASTA_FOFN, inputBaseName=None ):
    
    # Starting my tmp fasta file generation
    tmpFileNum   = 1
    if ( (inputBaseName == None) or (inputBaseName == "None") ):
        baseName     = splitext(split(fileName)[-1])[0]
    else:
        baseName = inputBaseName
    #####
    
    tmpFastaFile = join( tmpDirPath, 'tmpFile_%s_%d.fasta'%(baseName,tmpFileNum) )
    stderr.write( '\t-Writing %s\n'%tmpFastaFile )
    oh_tmp       = open( tmpFastaFile, 'w' )
    
    # Opening the pretriangle files
    oh_preTri = open( splitFASTA_FOFN, 'w' )
    oh_preTri.write( '%s\n'%tmpFastaFile )

    # Info for tracking primary IDs
    primaryReadID = None
    
    # Variable for total bases
    basePerBatch = int(basePerBatch)
    totalBases   = 0

    for r in univIter( fileName ):
        # Pulling the information
        readName   = r.id
        try:
            readID     = readName.split('/')[1]  # This is actually the well location in 16-bit format!!
        except IndexError:
            readID = readName
        #####
        # Do we need to do anything?
        if ( totalBases >= basePerBatch ):
            # Checking to see if we are splitting a read set
            if ( readID != lastReadID ):
                # Close the tmp file
                oh_tmp.close()
                # Increment the file counter by 1
                tmpFileNum += 1
                tmpFastaFile = join( tmpDirPath, 'tmpFile_%s.%d.fasta'%(baseName,tmpFileNum) )
                stderr.write( '\t-Writing %s\n'%tmpFastaFile )
                # Recording the filename
                oh_preTri.write( '%s\n'%tmpFastaFile )
                # Reopen the tmp file
                oh_tmp       = open( tmpFastaFile, 'w' ) 
                # Reset the counter
                totalBases = 0
            #####
        #####
        # Writing the read to the tmp file
        oh_tmp.write( '>%s\n%s\n'%(readName, str(r.seq) ) )
        # Summing the sequence
        totalBases += len(r.seq)
        # Recording the last readID
        lastReadID = readID
    #####
    
    # Closing the final file
    oh_tmp.close()
    
    # Recording the preTriangleFiles
    oh_preTri.close()
    
    return

#==============================================================
def removeTriangleReads_cmdGen( tmpDirPath, inputFiles ):
    # Pulling the tmpFileNames
    envCMD    = "source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/BLAT_ENV/"
    tmpFileNames = []
    for inputFile in inputFiles:
        baseName   = splitext(split(inputFile)[-1])[0]
        preTriFile = join( tmpDirPath, 'splitFASTA_%s.dat'%baseName )
        for line in open(preTriFile):
            tmpFileNames.append( line[:-1] )
        #####
    #####
    # Generating the commands
    cmdList   = []
    for tmpFileName in tmpFileNames:
        tmpArgs = r"'%s', '%s'" % ( tmpFileName, tmpDirPath )
        tmpPre  = splitext( split( tmpFileName )[1] )[0]
        cmdList.append( ( tmpPre, r'%s;python -c "from pacbio_lib import removeTriangleReads ; removeTriangleReads( %s )"' % ( envCMD, tmpArgs ) ) )
    #####
    return cmdList

#==============================================================
def removeTriangleReads( tmpFastaFile, tmpDirPath ):
    
    # Initializing the variables
    baseID         = splitext(split(tmpFastaFile)[-1])[0]
    
    # Setting the screening command
    tmpFASTA  = join( tmpDirPath, 'triRem_%s.fasta'%baseID )
    envCMD    = "source activate /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/ANACONDA_ENVS/BLAT_ENV/"
    blatCMD   = "blat -noHead -minMatch=3 -tileSize=8 -repMatch=10000 -minIdentity=75 -extendThroughN %s %s stdout"%( tmpFASTA, tmpFASTA )
    awkCMD    = "awk 'function abs(v) {return v < 0 ? -v : v} { if ( ($9==\"-\") && ($10==$14) && (abs($12-$16)<=75) && (abs($13-$17)<=75) && ( (($2+$6)/($13-$12))<=0.55 ) && (($13-$12)>=400)) {print $10}}'"
    cmd       = "%s;%s | %s"%( envCMD, blatCMD, awkCMD )
    
    # Opening the triangle read FASTA file
    triangleReadsFile = join( tmpDirPath, 'triangle_reads_%s.fasta'%baseID )
    oh_TR             = open( triangleReadsFile, 'w')
    
    # Opening the LPNTR (Longest primary non-triangle read) FASTA file
    LPNTR_File = join( tmpDirPath, 'LPNTR_%s.fasta'%baseID )
    oh_LPNTR   = open( LPNTR_File, 'w')
    
    # Checking for triangle reads
    x          = iterCounter(1000)
    lag        = 9  # Optimal size for lag!!!  Balances I/O with BLAT compute load
    oh_tmp     = open(tmpFASTA, 'w')
    nCounter   = 0
    lastReadID = None
    recDict    = {}
    triDict    = {}
    for r in univIter( tmpFastaFile ):

        # Pulling base read ID
        try:
            readID = r.id.split('/')[1]  # This is actually the well location in 16-bit format!!
        except IndexError:
            readID = r.id
        #####

        # Writing the read to a temporary file
        if ( nCounter >= lag ):

            # Checking to see if we are splitting a read set
            if ( readID != lastReadID ):

                # Closing the file
                oh_tmp.close()

                # Screening for reads
                triangleReadSet = set( [ item.strip() for item in subprocess.check_output( cmd, shell=True ).splitlines() ] )
                
                # Writing the triangle reads to a file
                for triangleID in triangleReadSet:
                    print "TRI", triangleID
                    triangle_r = recDict[triangleID]
                    oh_TR.write( '>%s\n%s\n' % (triangle_r.id, triangle_r.seq) )
                #####
                
                # Writing the longest non-triangle read to the main output file
                triSet = set( [item.split('/')[1] for item in triangleReadSet] )
                for tmpID, tmpList in triDict.iteritems():
                    if ( tmpID in triSet ): continue
                    tmpList.sort( reverse = True )
                    r_LPNTR = tmpList[0][1]
                    oh_LPNTR.write( '>%s\n%s\n' % ( r_LPNTR.id, r_LPNTR.seq ) )
                #####
                
                # Resetting the counter and file handle
                recDir   = {}
                triDict  = {}
                nCounter = 0
                oh_tmp   = open(tmpFASTA, 'w')

            #####

        #####

        # Writing the read to the temporary file
        x()
        oh_tmp.write( '>%s\n%s\n'%(r.id, r.seq) )
        
        # Incrementing the counter
        nCounter += 1
        lastReadID = readID
        
        # Storing the records
        recDict[r.id] = r
        try:
            triDict[readID].append( (len(r.seq), r) )
        except KeyError:
            triDict[readID] = [ ( len(r.seq), r) ]
        #####
        
    #####
    
    # Closing the temporary file
    oh_tmp.close()
    
    # Picking up the end case
    if ( nCounter > 0 ):
    
        # Screening for reads
        triangleReadSet = set( [ item.strip() for item in subprocess.check_output( cmd, shell=True ).splitlines() ] )
        
        # Writing the triangle reads to a file
        for triangleID in triangleReadSet:
            triangle_r = recDict[triangleID]
            oh_TR.write( '>%s\n%s\n' % (triangle_r.id, triangle_r.seq) )
        #####
        
        # Writing the longest non-triangle read to the main output file
        triSet = set( [item.split('/')[1] for item in triangleReadSet] )
        for tmpID, tmpList in triDict.iteritems():
            if ( tmpID in triSet ): continue
            tmpList.sort( reverse = True )
            r_LPNTR = tmpList[0][1]
            oh_LPNTR.write( '>%s\n%s\n' % ( r_LPNTR.id, r_LPNTR.seq ) )
        #####

    #####

    # Clearing out the temporary FASTA file
    system('rm -f %s'%tmpFASTA )
    
    # Clearing the main FASTA file
    system( 'rm -f %s'%tmpFastaFile )
    
    # Closing the TR file
    oh_TR.close()
    
    # Closing the LPNTR file
    oh_LPNTR.close()

#==============================================================
def BLAT_cmdGen( tmpDirPath, inputFiles, blatCmd, inputBaseName=None ):
    # Pulling the tmpFileNames
    tmpFileNames = []
    for inputFile in inputFiles:
        if ( inputBaseName == None ):
            baseName = splitext(split(inputFile)[-1])[0]
        else:
            baseName = inputBaseName
        #####
        splitFASTAListing = join( tmpDirPath, 'splitFASTA_%s.dat'%baseName )
        for line in open( splitFASTAListing ):
            tmpFASTA = line[:-1]
            blatOutFile = join( tmpDirPath, '%s.compressedBLAT'%(splitext(tmpFASTA)[0]) )
            tmpFileNames.append( (tmpFASTA, blatOutFile) )
        #####
    #####
    # Generating the commands
    cmdList   = []
    for tmpFileName, blatOutFile in tmpFileNames:
        tmpPre  = splitext( split( tmpFileName )[1] )[0]
        cmdList.append( ( tmpPre, blatCmd % (tmpFileName, blatOutFile) ) )
    #####
    return cmdList
    