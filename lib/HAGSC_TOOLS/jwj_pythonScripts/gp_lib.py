__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/24/15"

from subprocess import Popen, PIPE, STDOUT

from sys import stdout, stderr

from os.path import isfile, join, isdir

from os import system, mkdir

import time
import sys

#==============================================================
def create_sh_file( cmdList, Time, Memory, shFileName, supressOutput, emailAddress=None, ncores=1 ):
    
    # Building the submission file
    oh        = open( shFileName, 'w' )
    oh.write( '#!/bin/bash\n')
    map( oh.write, ['%s\n'%cmd for cmd in cmdList] )
    oh.close()

#==============================================================
def genePool_submitter( cmdList, Time, Memory, shFileName, supressOutput=True, delete_sh_file=True, waitUntilDone=True, emailAddress=None, ncores=1 ):
    
    # Making the .sh submission file
    create_sh_file( cmdList, Time, Memory, shFileName, supressOutput, emailAddress, ncores )
    
    submitJob( shFileName )

#==============================================================
def submitJob( shFileName ):

    # Submitting the job
    result = Popen( 'bash %s'%shFileName, shell=True, stdout=PIPE).communicate()[0]
    
    # Notifying the user
    stderr.write( 'submitted  job %s with JOB_ID=%s\n'%(shFileName,"none") )

#==============================================================
def jobFinished( job_ID_set ):
    totalTime = 0
    timeInc   = 10
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        checkerSet = set()
        p = Popen( 'squeue', shell=True, stdout=PIPE )
        checkerSet = set( [line.split(None)[0] for line in p.stdout if (line[0] not in 'J-')] )
        if len(job_ID_set.intersection(checkerSet)) == 0: break
        p.poll()

    stderr.write( 'Jobs Finished in %d seconds!!\n'%totalTime )
    time.sleep( 10 )
    return

#==============================================================
def jobFinished_Files( fileList ):
    timeInc   = 30
    while True:
        time.sleep(timeInc)
        isDone = True
        for fileName in fileList:
            if ( not isfile( fileName ) ): isDone = False

        if ( isDone ): break

    # Giving the file system 30 seconds to control latency
    time.sleep( timeInc )
    return

#==============================================================
def jobFinished_RUN_ID( RUN_ID ):
    totalTime = 0
    timeInc   = 10
    cmd = "squeue | awk \'{print $3}\' | grep %s"%RUN_ID
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        p          = Popen( cmd, shell=True, stdout=PIPE )
        checkerSet = set( [line.split(None)[0] for line in p.stdout if (line[0] not in 'J-')] )
        if ( len(checkerSet) <= 1 ): break
        p.poll()
    #####
    stderr.write( 'Jobs Finished in %d seconds!!\n'%totalTime )
    # Giving the file system 30 seconds to control latency
    time.sleep( 30 )
    return

#==============================================================
def genePool_queue_limiter( jobList, nJobLimit ):
    
    # Starting the queue with a number of jobs
    runningJobSet = set()
    jobNum        = 0
    notDone       = True
    for n in xrange( nJobLimit ):
        try:
            shFileName = jobList[jobNum]
            jobNum += 1
        except IndexError:
            notDone = False
            continue
        #####
        runningJobSet.add( submitJob( shFileName ) )
        stderr.write( "Starting %s, runningJobSet %d\n" % ( shFileName, len(runningJobSet) ) )
    #####
    
    # Keeping the number of jobs in the queue
    timeInc = 15
    while ( notDone or (len(runningJobSet) > 0) ):
        stderr.write( "%d running jobs\n" % len(runningJobSet) ) 
        # Wait a specific amount of time
        time.sleep(timeInc)
        # Extracting all job IDs
        p = Popen( 'squeue', shell=True, stdout=PIPE )
        checkerSet = set( [line.split(None)[0] for line in p.stdout if (line[0] not in 'J-')] )
        p.poll()
        # Computing the shared IDs
        sharedIDs     = runningJobSet.intersection(checkerSet)
        IDs_to_delete = runningJobSet - sharedIDs
        # If the number of shared IDs is too low
        if ( len(IDs_to_delete) > 0 ):
            for item in IDs_to_delete: stderr.write( "\t-JobID %s Completed\n"%item )
            # Pulling the jobIDs from the running IDs
            map( runningJobSet.remove, IDs_to_delete )
            # Launching more jobs
            for n in xrange(len(IDs_to_delete)):
                try:
                    shFileName = jobList[jobNum]
                    jobNum += 1
                except IndexError:
                    notDone = False
                    continue
                #####
                runningJobSet.add( submitJob( shFileName ) )
                stderr.write( "Starting %s, runningJobSet %d\n" % ( shFileName, len(runningJobSet) ) )
            #####
        #####
    #####
    stderr.write( "All jobs have completed\n" )
    time.sleep( 10 )
    return

