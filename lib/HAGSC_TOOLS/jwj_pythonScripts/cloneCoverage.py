__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  11/18/16"

from hagsc_lib import iterCounter, iterFASTA, FASTAFile_dict

from os.path import isfile

from numpy import array

from sys import stdout

from math import sqrt

import subprocess

#==============================================================
class scaffoldClass(object):

    def __init__(self,scaffID,pos,seq,qname):
        self.scaffID  = scaffID
        self.pairDict = {}
        self.addEntry( pos, seq, qname )
    
    def addEntry( self, pos, seq, qname ):
        try:
            self.pairDict[qname].append( (pos, seq) )
        except KeyError:
            self.pairDict[qname] = [ (pos, seq) ]
        #####
    
    def analyzeDepth( self, tmpIndex, oh ):
        
        # Pull sequence and find the number of bases
        tmpSeq = str( tmpIndex[self.scaffID].seq )
        nSize  = len(tmpSeq)
        
        # If it is not big enough, then end
        if ( nSize < 35000 ): return
        
        # Fill the internal array
        internalArray = array( nSize * [0] )
        for qname, tmpList in self.pairDict.iteritems():
            if ( len(tmpList) != 2 ): continue
            pos_1, seq_1 = tmpList[0]
            pos_2, seq_2 = tmpList[1]
            if ( pos_1 < pos_2 ):
                start = pos_1
                end   = pos_2 + len(seq_2)
            else:
                start = pos_2
                end   = pos_1 + len(seq_1)
            #####
            internalArray[start:end] += 1
        #####
        
        for pos in xrange(nSize):
            oh.write( '%s\t%s\t%d\t%d\n'%( self.scaffID, tmpSeq[pos], pos, internalArray[pos] ) )
        #####
        
        # Setting the front, rear skip distance
        skipDist   = 17000
        
        # Computing the statistics on the array to find breaks
        tmp_x   = [                    internalArray[n] for n in xrange(skipDist,(nSize-skipDist)) ]
        tmp_x2  = [ internalArray[n] * internalArray[n] for n in xrange(skipDist,(nSize-skipDist)) ]
        mean_x  = float(sum(tmp_x)) / float(nSize)
        mean_x2 = float(sum(tmp_x2)) / float(nSize)
        stdDev  = sqrt( mean_x2 - mean_x * mean_x )
#         z            = 2.40
#         lowerBound   = mean_x - z * stdDev
        
        # Finding the breaks
        lowerBound   = 20.0
        state        = "OUT"
        outputString = []
        for n in xrange( skipDist, nSize-skipDist ):
            cov = internalArray[n]
            if (state == "OUT"):
                if (cov < lowerBound):
                    # Transitioning into a low coverage state
                    state        = "IN"
                    minVals      = {}
                    minVals[cov] = [n]
                    continue
                else:
                    # State is OUT and coverage is high, keep moving
                    continue
                #####
            elif (state == "IN"):
                if (cov < lowerBound):
                    # Already IN, need to add values
                    try:
                        minVals[cov].append( n )
                    except KeyError:
                        minVals[cov] = [n]
                    #####
                    continue
                else:
                    # Toggle the state
                    state = "OUT"
                    # Transitioning from IN to OUT, do some math
                    minCov = sorted( minVals.keys() )[0]
                    # Writing the range of the minimum coverage
                    outputString.append( '%s\t%d\t%d\t%d\t%.3f\t%.3f\n'%(self.scaffID, minCov, minVals[minCov][0], minVals[minCov][-1], mean_x, stdDev) )
                #####
            #####
        #####
        
        # Write the output to the window
        if ( len(outputString) > 0 ): map( stdout.write,  outputString )

#             if ( cov == 0 ): continue
#             if ( cov < lowerBound ):
#                 print self.scaffID, n, cov, lowerBound
#             #####
        #####
        
#==============================================================
def parseBamFile( bamFileName, tmpIndex, testScaff, outputFileName ):
    # Reading in the infomation from the bam file
    pairClass   = None
    p           = subprocess.Popen( 'samtools view %s'%bamFileName, shell=True, stdout=subprocess.PIPE )
    x           = iterCounter(1000000)
    currentName = None
    oh          = open( outputFileName, 'w' )
    for line in p.stdout:
        
        # Parsing the line
        qname, flag, rname, pos, mapq, cigar, rnext, pnext, tlen, seq = line.split(None)[:10]
        flag, pos, mapq, pnext, tlen = map( int, [flag, pos, mapq, pnext, tlen] )
        x()
        
        # Screening the insert size
        if ( (abs(tlen) < 5000) or (abs(tlen) > 35000) ): continue
        
        # Checking to see if we have switched names
        if ( rname != currentName ):
            
            # Starting condition
            if ( currentName == None ):
                del pairClass
                pairClass   = scaffoldClass( rname, pos, seq, qname )
                currentName = rname
                continue
            #####
            
            # Writing the stuff to a file
            pairClass.analyzeDepth( tmpIndex, oh )
            
            # Resetting the class and doing it again
            del pairClass
            pairClass   = scaffoldClass( rname, pos, seq, qname )
            currentName = rname
            continue
            
        #####
        
        # Adding the pair to the dictionary 
        pairClass.addEntry( pos, seq, qname )

    #####
    p.poll()
    oh.close()

#==============================================================
def real_main():

#     #=======================================================================
#     # Precomputing my bases
#     FALCON_index = FASTAFile_dict('B_K1000_Draft.fa')
#     parseBamFile( 'DAN_2.lucigen.sorted.bam', FALCON_index, '000003F|quiver', 'DAN_2_coverage.dat' )

    #=======================================================================
    # Precomputing Dan's bases
    DAN_index    = FASTAFile_dict('Scaffolds-pass4.fa')
    parseBamFile( 'FOURTH_TRY.lucigen.sorted.bam', DAN_index, 'scaffold_50', 'FOURTH_TRY.lucigen.dat' )
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
