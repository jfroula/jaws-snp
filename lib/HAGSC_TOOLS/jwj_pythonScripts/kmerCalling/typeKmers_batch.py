__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  7/17/14"

from string import maketrans

from os.path import split

from sys import stdout

from os import system

from sys import argv

import subprocess

translationTable = maketrans("ATCG", "TAGC")

#==============================================================
def revComp( seq ):
    return seq.translate(translationTable)[::-1]

#==============================================================
def kmerize( oh, merSize ):
    while (True):
        line = oh.readline()
        if ( not line ): return
        try:
            ID, seq = line.split(None)
        except ValueError:
            ID, d, seq = line.split(None)
        #####
        for n in xrange( len(seq) - merSize + 1 ):
            kmer = seq[ n : (n+merSize) ]
            yield ID, kmer
            yield ID, revComp( kmer )
        #####
    #####
    return

#==============================================================
def real_main():
    
    from time import time
    
    # Getting the inputs
    merSize  = int( argv[1] )
    CR_file  = argv[2]
    kmerFile = argv[3]
    shFile   = argv[4]
    
#     startTime = time()

    # Reading in the kmers file
    kmerDict    = {}
    counterDict = {}
    for line in open( kmerFile ):
        chrID, pos, kmer = line.split(None)
        ID                  = '%s_%s'%(chrID,pos)
        if ( kmer == "NONE" ): continue
        try:
            kmerDict[kmer]    = ID
            counterDict[kmer] = set()
        except KeyError:
            continue
        #####
    #####
    
#    print "Time for loading kmers: %.3f"%( time() - startTime )
#     startTime = time()

    # Reading in kmers
    cmd = 'zcat %s'%CR_file
    p = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    for ID, kmer in kmerize( p.stdout, merSize ):
        try:
            counterDict[kmer].add( ID )
        except KeyError:
            pass
        #####
    #####
    p.poll()
    
    # Providing output
    for kmer, tmpSet in counterDict.iteritems():
        if ( len(tmpSet) == 0 ): continue
        stdout.write('%s\t%s\t%s\n'%( kmerDict[kmer],kmer,';'.join(tmpSet)))
    #####

#     print "Time for processing kmers: %.3f"%( time() - startTime )
    
    # Removing the sh file
    system( "rm -f %s"%(shFile) )

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
