__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  12/16/16"

from sys import argv, stdout

from os import system

import subprocess

#==============================================================
class kmerClass(object):
    def __init__(self,kmer,kmerID,read_IDs):
        self.kmer    = kmer
        self.kmerID  = kmerID
        self.readSet = set()
        self.addReads(read_IDs)
    
    def addReads(self,read_IDs):
        map( self.readSet.add, read_IDs.split(';') )
    
    def outputString(self):
        return "%s\t%s\t%s\n"%( self.kmerID, self.kmer, ';'.join(self.readSet) )

#==============================================================
def real_main():
    
    FOFN    = argv[1]
    tmpDict = {}
    for tmpFile in open( FOFN ):
        p = subprocess.Popen( 'zcat %s'%tmpFile, shell=True, stdout=subprocess.PIPE )
        for line in p.stdout:
            kmerID, kmer, read_IDs = line.split(None)
            try:
                tmpDict[kmer].addReads( read_IDs )
            except KeyError:
                tmpDict[kmer] = kmerClass(kmer,kmerID,read_IDs)
            #####
        #####
        p.poll()
    #####
    
    for kmer, tmpClass in tmpDict.iteritems():
        stdout.write( tmpClass.outputString() )
    #####

    # Cleaning up at the end
    for tmpFile in open( FOFN ):
        system( "rm -f %s"%tmpFile )
    #####
    
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
    