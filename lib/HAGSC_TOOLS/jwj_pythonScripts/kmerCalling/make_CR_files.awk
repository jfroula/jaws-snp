#=============================================
BEGIN{

    nReads     = 0
    fileNumber = 1
    CR_file    = basePath"/"LIB_ID"."fileNumber".cr.gz"
    system( "rm "CR_file )
    system( "rm "CR_FOFN )
    CR_FOFN    = basePath"/full_CR_file_list_"LIB_ID".dat"
}

#=============================================
# MAIN CODE
{
    if ( NR%4 == 1 ) 
    { 
        readID = substr($1,2,length($1))
    }
    
    if ( NR%4 == 2 ) 
    {
        # Writing the count ready line to a file
        seq = $0
        print readID"\t"seq  | " gzip -c > " CR_file
        nReads = nReads + 1
        
        if ( nReads >= readsPerFile ){
            
            # Closing the fastq file
            close ( " gzip -c > " CR_file )

            # Storing the file information
            CR_FileList[fileNumber] = CR_file
            
            # Making the new filenames
            nReads     = 0
            fileNumber = fileNumber + 1
            CR_file    = basePath"/"LIB_ID"."fileNumber".cr.gz"
            # Initializing the CR_file
            system( "rm "CR_file )
            
        }
    }
}

#=============================================
END{
    
    # Closing the fastq file
    close ( " gzip -c > " CR_file )

    # Storing the file information
    CR_FileList[fileNumber] = CR_file

    # Writing the names of the fastq files to the output file
    for ( fileNum in CR_FileList ){
        print CR_FileList[fileNum] > CR_FOFN
    }
    close( CR_FOFN )
}
