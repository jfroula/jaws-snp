#=============================================
BEGIN{

    nReads     = 0
    fileNumber = 1
    CR_file    = basePath"/"RUN_ID"."fileNumber".cr.gz"
    system( "rm "CR_file )
    CR_FOFN    = basePath"/full_CR_file_list_"RUN_ID".dat"
}

#=============================================
function exists( file, line )
{
        if ( (getline line < file) == -1 )
        {
                return 0
        }
        else {
                close(file)
                return 1
        }
}

#=============================================
# MAIN CODE
{
    if ( NR%4 == 1 ) 
    { 
        readID = substr($0,2,length($0))
    }
    
    if ( NR%4 == 2 ) 
    {
        # Writing the count ready line to a file
        seq = $0
        print readID"\t"seq  | " gzip -c > " CR_file
        nReads = nReads + 1
        
        if ( nReads >= readsPerFile ){
            
            # Closing the fastq file
            close ( " gzip -c > " CR_file )
            
            # Writing the .sh file
            shFile = basePath"/"RUN_ID"."fileNumber".sh"
            system( "rm "shFile )
            print "#!/bin/bash" > shFile
            print "## Run the command from the current working directory. Launch qsub from the library directory" > shFile
            print "#$ -cwd" > shFile
            print "## specify an email address" > shFile
            print "#$ -M "EMAIL_ADDRESS > shFile
            print "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally" > shFile
            print "#$ -m a" > shFile
            print "#$ -l h_rt=12:00:00" > shFile
            print "#$ -l ram.c="CALL_MEMORY > shFile
            print "## Supresses output" > shFile
            print "#$ -e /dev/null" > shFile
            print "#$ -o /dev/null" > shFile
            print "## Job Starts Here" > shFile

            # Writing out the commands
            outputFile = basePath"/"RUN_ID"."fileNumber".kmerCalls.dat.gz"
            print "rm "outputFile > shFile
            print "python "RUN_PATH"/typeKmers_batch.py "KMER_SIZE" "CR_file" "kmerFile" "shFile" | gzip -c > "outputFile > shFile
            
            # Closing the sh file
            close ( shFile )
            
            # Launching the job
#             system( "qsub "shFile )

            # Storing the file information
            CR_FileList[fileNumber] = outputFile
            
            # Making the new filenames
            nReads     = 0
            fileNumber = fileNumber + 1
            CR_file    = basePath"/"RUN_ID"."fileNumber".cr.gz"
            system( "rm "CR_file )
            
        }

    }

}

#=============================================
END{
    
    # Closing the fastq file
    close ( " gzip -c > " CR_file )

    #-----------------------------------------------------------------------
    # Writing the final parsing .sh file
    finalParse_shFile = basePath"/"RUN_ID".finalParse.sh"
    system( "rm "finalParse_shFile )
    print "#!/bin/bash" > finalParse_shFile
    print "## Run the command from the current working directory. Launch qsub from the library directory" > finalParse_shFile
    print "#$ -cwd" > finalParse_shFile
    print "## specify an email address" > finalParse_shFile
    print "#$ -M "EMAIL_ADDRESS > finalParse_shFile
    print "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally" > finalParse_shFile
    print "#$ -m a" > finalParse_shFile
    print "#$ -l h_rt=12:00:00" > finalParse_shFile
    print "#$ -l ram.c="CALL_MEMORY > finalParse_shFile
    print "## Supresses output" > finalParse_shFile
#    print "#$ -e /dev/null" > finalParse_shFile
#    print "#$ -o /dev/null" > finalParse_shFile
    print "## Job Starts Here" > finalParse_shFile

    # Writing out the commands
    print "rm "mainOutputFile > finalParse_shFile
    print "python "RUN_PATH"/accumulator.py "CR_FOFN" | bzip2 -c > "mainOutputFile > finalParse_shFile

    # Closing the sh file
    close ( finalParse_shFile )
    
    #-----------------------------------------------------------------------
    # Checking to see if the last file was actually written
    if ( exists(CR_file) == 1 ){

        # Closing the fastq file
        close ( " gzip -c > " CR_file )
        
        # Writing the .sh file
        shFile = basePath"/"RUN_ID"."fileNumber".sh"
        system( "rm "shFile )
        print "#!/bin/bash" > shFile
        print "## Run the command from the current working directory. Launch qsub from the library directory" > shFile
        print "#$ -cwd" > shFile
        print "## specify an email address" > shFile
        print "#$ -M "EMAIL_ADDRESS > shFile
        print "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally" > shFile
        print "#$ -m a" > shFile
        print "#$ -l h_rt=12:00:00" > shFile
        print "#$ -l ram.c="CALL_MEMORY > shFile
        print "## Supresses output" > shFile
        print "#$ -e /dev/null" > shFile
        print "#$ -o /dev/null" > shFile
        print "## Job Starts Here" > shFile

        # Writing out the commands
        outputFile = basePath"/"RUN_ID"."fileNumber".kmerCalls.dat.gz"
        print "rm "outputFile > shFile
        print "python "RUN_PATH"/typeKmers_batch.py "KMER_SIZE" "CR_file" "kmerFile" "shFile" | gzip -c > "outputFile > shFile
        print "python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/snpCalling/waiting.py "RUN_ID > shFile
        print "qsub "finalParse_shFile > shFile
        
        # Closing the sh file
        close ( shFile )
        
        # Launching the job
        system( "qsub "shFile )

        # Storing the file information
        CR_FileList[fileNumber] = outputFile

    }
    else{
        
        #===================================
        # Handling the case where we don't launch the final
        # job, in this case we have to launch the watcher from here
        
        # Writing the .sh file
        shFile = basePath"/"RUN_ID"."fileNumber".sh"
        system( "rm "shFile )
        print "#!/bin/bash" > shFile
        print "## Run the command from the current working directory. Launch qsub from the library directory" > shFile
        print "#$ -cwd" > shFile
        print "## specify an email address" > shFile
        print "#$ -M "EMAIL_ADDRESS > shFile
        print "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally" > shFile
        print "#$ -m a" > shFile
        print "#$ -l h_rt=12:00:00" > shFile
        print "#$ -l ram.c="CALL_MEMORY > shFile
        print "## Supresses output" > shFile
        print "#$ -e /dev/null" > shFile
        print "#$ -o /dev/null" > shFile
        print "## Job Starts Here" > shFile

        # Writing out the commands
        print "python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/snpCalling/waiting.py "RUN_ID > shFile
        print "qsub "finalParse_shFile > shFile

        # Closing the sh file
        close ( shFile )
    
        # Launching the sorting job
        system( "qsub "shFile )
    }

    # Writing the names of the fastq files to the output file
    system( "rm "CR_FOFN )
    for ( fileNum in CR_FileList ){
        print CR_FileList[fileNum] > CR_FOFN
    }
    close( CR_FOFN )
}
