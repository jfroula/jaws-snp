__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  12/19/16"

from os.path import join, isdir, split

from os import system

from sys import argv

#==============================================================
def make_sh_Header( emailAddress, oh, runTime, runMemory ):
    oh.write( "#!/bin/bash\n" )
    oh.write( "## Run the command from the current working directory. Launch qsub from the library directory\n" )
    oh.write( "#$ -cwd\n" )
    oh.write( "## specify an email address\n" )
    oh.write( "#$ -M %s\n"%emailAddress )
    oh.write( "## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally\n" )
    oh.write( "#$ -m a\n" )
    oh.write( "#$ -l h_rt=%s\n"%runTime )
    oh.write( "#$ -l ram.c=%s\n"%runMemory )
    oh.write( "## Supresses output\n" )
    oh.write( "#$ -e /dev/null\n" )
    oh.write( "#$ -o /dev/null\n" )
    oh.write( "## Job Starts Here\n" )
    return

#==============================================================
def real_main():

    #==================================================
    # Dont change
    RUN_PATH     = '/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/kmerCalling'
    readsPerFile = 70000000
    
    #==================================================
    # Setting up the run parameters
    BASE_PATH    = '/projectb/scratch/jjenkins/kmerTypeTest'
    PROGENY_FILE = join( BASE_PATH, 'testProgenyFile.dat' )
    parentalFile = join( BASE_PATH, 'testParentKmerFile.dat' )

    emailAddress = 'jjenkins@hudsonalpha.org'
    KMER_SIZE    = 64
    
    #==================================================
    # Reading in the parents as a dictionary
    parentDict = {}
    for line in open( parentalFile ):
        parent_ID, parentKmerFile = line.split(None)
        parentDict[parent_ID] = parentKmerFile
    #####

    # Reading in the progeny as a dictionary
    progenyDict = {}
    for line in open( PROGENY_FILE ):
        libID, progenyFastq = line.split(None)
        progenyDict[libID] = progenyFastq
    #####
    
    # Setting a default value of the call state
    try:
        callState = argv[1]
    except IndexError:
        callState = 'PARTITION'
    #####
    
    if ( callState == 'PARTITION' ):

        #==================================================
        # Submitting the calling jobs
        for libID, progenyFastq in progenyDict.iteritems():
            
            # Changing into the appropriate directory
            libPath = join( BASE_PATH, libID )
            
            # Making sure that the library directory does exist
            if ( not isdir(libPath) ):
                print "DIRECTORY %s DOES NOT EXIST"%libPath
                print "CREATING DIRECTORY %s"%libPath
                system( "mkdir %s"%libPath)
            #####
            
            # Breaking the library into count ready files
            shFile_lib = join( libPath, '%s.make_CR_files.sh'%libID )
            oh_lib     = open( shFile_lib, 'w' )
            make_sh_Header( emailAddress, oh_lib, "01:00:00", "1G" )
    
            catCmd = 'cat %s | gunzip -c'%progenyFastq
            awkList = [ 'awk -v basePath=\"%s\"'%libPath ]
            awkList.append( '-v LIB_ID=\"%s\"'%libID )
            awkList.append( '-v readsPerFile=%d'%readsPerFile )
            awkList.append( '-f %s/make_CR_files.awk'%RUN_PATH )
            oh_lib.write( '%s | %s\n'%( catCmd, ' '.join(awkList) ) )
            oh_lib.write( 'python %s/kmer_counter_submitter.py COUNTING %s'%(BASE_PATH,libID) )
            oh_lib.close()
            
            # Submitting the jobs
            system( 'cd %s ; qsub %s ; cd ..'%(libPath, shFile_lib) )
            
        #####
    
    elif ( callState == 'COUNTING' ):
        
        # Reading in the library ID
        try:
            libID = argv[2]
        except IndexError:
            print "SOMETHING WENT WRONG"
            print callState
            assert False
        #####

        # Changing into the appropriate directory
        libPath = join( BASE_PATH, libID )
            
        # Making sure that the library directory does exist
        if ( not isdir(libPath) ):
            print "DIRECTORY %s DOES NOT EXIST"%libPath
            assert False
        #####
        
        # Reading the FOFN for the CR files
        full_CR_file = join( libPath, "full_CR_file_list_%s.dat"%libID )
        CR_list      = [line.strip() for line in open( full_CR_file )]
        numFiles     = len(CR_list)
        
        # Handling the parents
        for parent_ID, parentKmerFile in parentDict.iteritems():
    
            # Setting the base run ID
            run_ID = '%s.%s'%( libID, parent_ID )
            
            # Output Files 
            full_output_file = join( libPath, "full_output_file_list_%s.dat"%run_ID )
            oh_output        = open( full_output_file, 'w' )
        
            for CR_file in CR_list:
                
                # Loading the fileNumber
                fileNumber = int( split(CR_file)[1].split('.')[1] )

                # Writing the .sh file
                shFile_parent = join( libPath, "%s.%d.sh"%(run_ID,fileNumber) )
                oh_parent     = open( shFile_parent, 'w' )
                make_sh_Header( emailAddress, oh_parent, "12:00:00", "5G" )

                # Writing the name of the output file to the accumulator file
                outputFile = join( libPath, "%s.%d.kmerCalls.dat.gz"%(run_ID,fileNumber) )
                oh_output.write( "%s\n"%outputFile )
                
                # Writing a command to the sh file
                oh_parent.write( "rm %s\n"%outputFile )
                oh_parent.write( "python %s/typeKmers_batch.py %d %s %s %s | gzip -c > %s\n"%(RUN_PATH,KMER_SIZE,CR_file,parentKmerFile,shFile_parent,outputFile) )
                
                if ( fileNumber == numFiles ):
                    
                    # Write the accumulator sh file
                    shFile_acc = join( libPath, "%s.accumulator.sh"%(run_ID) )
                    oh_acc     = open( shFile_acc, 'w' )
                    make_sh_Header( emailAddress, oh_acc, "12:00:00", "5G" )
                    mainOutputFile = join( libPath, '%s.calls.dat.bz2'%(run_ID) )
                    oh_acc.write( "rm %s\n"%mainOutputFile )
                    oh_acc.write( "python %s/accumulator.py %s | bzip2 -c > %s\n"%(RUN_PATH,full_output_file,mainOutputFile ) )
                    oh_acc.close()
                    
                    # Add the accumulator call to the last CR file submission
                    oh_parent.write( "python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/jwj_pythonScripts/snpCalling/waiting.py %s\n"%run_ID )

                    # Adding the accumulator at the end
                    oh_parent.write( 'qsub %s\n'%shFile_acc )

                #####
                
                # Closing the sh file
                oh_parent.close()
                
                # Submit the job
                system( 'cd %s; qsub %s; cd ..'%(libPath,shFile_parent) )
                
            #####
            
            oh_output.close()
            
        #####
        

    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
