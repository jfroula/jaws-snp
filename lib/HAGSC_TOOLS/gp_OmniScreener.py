#!/usr/bin/python
__author__="plott"
__date__ ="$June 18, 2015$"

from optparse import OptionParser
from os import curdir, getpid, uname, mkdir, chdir, system
from os.path import abspath, realpath, isfile, curdir, join, isdir
import subprocess
import os
from sys import argv, stdin, stdout, stderr
from time import time, sleep
import re
from shutil import rmtree
import time
#=========================================================================
def isBzipFile( fileName ):
    try:
        import bz2
        x = bz2.BZ2File(fileName).readline()
        return True
    except IOError:
        pass
    #####
    return False
#=========================================================================
def isGzipFile(fileName):
    try:
        import gzip
        x = gzip.GzipFile(fileName).readline()
        return True
    except IOError:
        return False
    #####

#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    if ( isGzipFile(fileName) ): return 'cat %s | gunzip -c '%( fileName)
    if ( isBzipFile(fileName) ): return 'cat %s | bunzip2 -c '%( fileName)
    return 'cat %s '%( fileName)
#=========================================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True
#=========================================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): raise IOError( 'Directory not found: %s'%dirName )
    return True
#=========================================================================
def generateTmpDirName( dirNum=None ):
    tmpPID = [dirNum, getpid()][bool(dirNum==None)]
    return r'tmp.%s.%s'%( uname()[1], tmpPID )
#=========================================================================
def createTmpDirectory():
    stderr.write( '\n\t-Creating tmp directory\n')
    basePath = abspath(curdir)
    tmpDir   = generateTmpDirName()
    tmpPath  = join( basePath, tmpDir )
    deleteDir(tmpPath)
    mkdir(tmpDir, 0o777)
    testDirectory(tmpDir)
    return tmpDir, tmpPath, basePath
#=========================================================================
def jobsFinished_cbp(pidSet):
    totalTime = 0
    timeInc   = 60
    while True:
        time.sleep(timeInc)
        totalTime += timeInc
        checker    = 0
        p = set([item.strip() for item in subprocess.Popen( "squeue | awk '{print $1}'", shell=True, stdout=subprocess.PIPE ).stdout])
        pIntersect = pidSet.intersection(p)
        
        if (len(pIntersect) == 0): break       
    #####
    return
#=========================================================================
def real_main():     
    # Defining the program options
    usage = "usage: %prog [FASTA FILE] [OUTPUT BASE NAME] [DB List] [options]"
    
    parser = OptionParser(usage)
    
    parser.add_option( '-l', \
                       "--ListDB", \
                       action  = "store_true", \
                       dest    = 'ListDB', \
                       help    = "Show a list of available DBs" )
    parser.set_defaults( ListDB = False )
    
    # Parsing the arguments
    (options, args) = parser.parse_args()
    
    if (options.ListDB):
        print "DB LIST:"
        for line in open("/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/OMNISCREEN_DB/masterList.txt"):
            print "\t%s"%(line.strip())
        #####
        print "\nNOTE:USE 'ALL' TO RUN ALL DATABASES"
        exit()
    
    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )

    else:
        # Pulling FASTA File
        querryFile = abspath(args[0])
        baseName   = args[1]
        dbList     = args[2].split(",")
        
        allDbList  = [line.strip() for line in open("/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/OMNISCREEN_DB/masterList.txt")]
        
        
        if (dbList[0] == "ALL"):
            dbList = [line.strip() for line in open("/global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/OMNISCREEN_DB/masterList.txt")]
        #####     
    #####
    
    print dbList
    #=================================================================
    # Creating a temporary directory
    tmpDir, tmpPath, basePath = createTmpDirectory()
    chdir( tmpPath )
    

    # Computing the blasted bases
    totalBases = {}
    gcDict     = {}    
    # Commands and Arguments
    # Alignment filtering command
    # Filters for a bitscore of >=300, and then appends the dbtype to the end of the alignment
    # BLASTX Parameters
    # a = 2  enables up to 2 processors to be used for the blast
    # Q = 11 is the bacterial/archeal translation table
    # f = 14 is the neighborhood score to seed an alignment
    # W = 3  is the seed size for seeding alignments
    # -F 'm S' -U:  "m" means masking is on, "S" means we are using SEG to mask low complexity protein sequences, and -U means we are masking lower case letters.
    # e = 1 sets the upper bound on the E-value
    # m = 8 is the single line summary output format
    # b = 10000 truncates the report to this many alignments
    # v = 10000 number of database sequences to show
    # MEGABLAST Parameters
    # a = 2 enables up to 2 processors to be used for blast
    # b = 0 means show all alignments
    # f = T show full ID in the output
    # D = 3 essentially equivalent ot "-m 8" in blast

    #shellHeader  = "#!/bin/bash\n#$ -q \"all.q\"\n#$ -pe smp 16\n#$ -cwd\n#$ -V\n"
    shellHeader  = "#!/bin/bash\n#SBATCH -t 12:00:00\n#SBATCH --mem=40G\n#SBATCH -D .\n"
    #Pull reall DB names:
    realBDList = []
    for db in dbList:
        if (db not in allDbList):
            stderr.write("\tNO DB FOR %s. EXCLUDING FROM SCREEN."%(db))
            continue
        #####
        
        for item in subprocess.Popen("/bin/ls /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/OMNISCREEN_DB/%s*.dmnd"%(db), shell=True, stdout=subprocess.PIPE).stdout:
            realBDList.append(item.strip())
        #####
    #####
    
    if (len(realBDList) == 0):
        stderr.write("\tNO DBS LISTED FOR SCREENING.EXITING")
        exit()
    #####
        
    dmndFront   = "/global/dna/projectdirs/plant/geneAtlas/TEST_DMND/diamond/diamond blastx -q %s -d %s"
    dmndOptions = "-p 8 --query-gencode 11 --max-target-seqs 1000000 --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore --evalue 1 --sensitive"
    filterStep  = "| awk '{if ($12>=300) print}' | sed 's/$/ %s/g' "
    
    
    # megablast -d /home/l3d43/screeningData/rdna.fasta -a 8 -i <> -b 0 -f T -D 3 -o stdout
    
    #alignCmdDict = {'rdna'  : "megablast -d /home/l3d43/screeningData/rdna.fasta -a %d -i %s -b 0 -f T -D 3 -o stdout | awk '{if ($12>=300) print}' | sed 's/$/ rdna/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s",\
    #                'mito'  : "blastall -p blastx -a %d -Q 11 -f 12 -W 3 -F \'m S\' -U -e 1 -m 8 -b 10000 -v 10000 -o stdout -i %s -d /global/homes/c/cplott/screeningDB/mitochondrionDb | awk '{if ($12>=300) print}' | sed 's/$/ mito/g' | python /global/dna/projectdirs/plant/geneAtlas/testing_cbp/scripts/loriPrepScripts/omniScreenerFilter.py %d %s %s >> %s ",\
    
    stderr.write( '\n\t-Running Alignments\n')
    
    outputDict = {}
    pidSet     = set()
    for target in dbList:
        outputDict[target] = []
        count = 1
        for bd in realBDList:
            if (bd.find(target) == -1):continue
            cmd = "%s\n%s %s %s > %s_%d.stdout"%(shellHeader, dmndFront%(querryFile, bd), dmndOptions, filterStep%(target), target, count)
            oh  = open("%s_%d.sh"%(target, count), "w")
            oh.write(cmd)
            oh.close()
            pidSet.add([item.strip() for item in subprocess.Popen("sbatch %s | awk '{print $3}'"%("%s_%d.sh"%(target, count)), shell=True,stdout=subprocess.PIPE).stdout][0])  
            outputDict[target].append("%s_%d.stdout"%(target, count))
            count += 1
        #####
    #####
    jobsFinished_cbp(pidSet)
    
    stderr.write( '\n\t-Generating %s.OMResults.tsv\n'%(baseName))
    
    scaffoldDict = {}
    fileHeader   = "SCAFFOLD\tSIZE"
    for t in outputDict:
        fileHeader = "%s\t%s\t%%COV\tTOPHIT"%(fileHeader, t)
        catCmd = "cat %s | python /global/dna/projectdirs/plant/geneAtlas/HAGSC_TOOLS/gp_OmniScreenerFilter.py %s %s"%(' '.join(outputDict[t]), querryFile, t)
        outLines = [ item.strip() for item in subprocess.Popen(catCmd, shell=True, stdout=subprocess.PIPE).stdout]
        for line in outLines:
            splitLine = line.split(None)
            try:
                scaffoldDict[splitLine[0]].append(splitLine[2])
                scaffoldDict[splitLine[0]].append(splitLine[3])
                scaffoldDict[splitLine[0]].append(splitLine[4])
            except KeyError:
                scaffoldDict[splitLine[0]] = splitLine
            #####
        #####
    #####
    scaffoldKeys = scaffoldDict.keys()
    scaffoldKeys.sort()
    
    oh = open("../%s.OMResults.tsv"%(baseName),"w")
    oh.write("%s\n"%(fileHeader))
    for scaf in scaffoldKeys: oh.write("%s\n"%("\t".join(scaffoldDict[scaf])))
    oh.close()
    #Back into the working directory
    chdir( basePath )
    
    # Taking out the trash
    #deleteDir( tmpPath )    
    
#==============================================================    
if ( __name__ == '__main__' ):
    real_main()