#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import iterCounter, isGzipFile, isBzipFile, testFile
from hagsc_lib import iterFASTA, writeFASTA, iterQUAL, writeQUAL
from time import time
import subprocess

from os.path import splitext, realpath

from optparse import OptionParser

#==============================================================
def makeOutputString( baseName, entries ):
    if ( len(entries) < 2 ):
        print "entries less than 2"
        assert False
    #####
    # Sorting the entries
    entries.sort()
    # Generating the output string
    return ''.join([ '>%s%s%s\n%s\n'%(baseName,item[2], item[0],item[1]) for item in entries])
    
#==============================================================
def pullBaseName( record ):
    if ( record.id.count('.') > 0 ):
        x = record.id.split(None)[0].split('.')
        x.append( '.' )
        return x
    elif ( record.id.count('-R') > 0 ):
        x = record.id.split(None)[0].split('-R')
        x.append( '-R' )
        return x
    else:
        return [record.id.replace(":","_"),record.description[0],"-R"]
    #####

#==============================================================
def Bzip_file(outFile,mode='r',compVal=7):
    if (mode == 'r'): 
        return subprocess.Popen( "cat %s | bunzip2 -c"%(outFile), shell=True, stdout=subprocess.PIPE ).stdout
    elif (mode == 'w'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE, stdout=open(outFile,'w') ).stdin
    elif (mode == 'a'):
        return subprocess.Popen( "bzip2 -z -c -%d"%(compVal), shell=True, stdin=subprocess.PIPE,stdout=open(outFile,'a') ).stdin
    else:
        stderr.write("*****FILE MODE ERROR*****\n\tUnrecognized mode '%s'. Acceptable modes: 'r','w', and 'a'\n"%(mode))
        assert False
        
#=========================================================================
def real_main():

    # Defining the program options
    usage = 'usage: %prog [R1 FASTA] [R1 QUAL] [R2 FASTA] [R2 QUAL] [output FASTA] [options]'

    parser = OptionParser(usage)

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 5 ):
        parser.error( 'Incorrect number of arguments.  ' + \
                      'View usage using --help option.' )
    else:
        # Pulling the FASTA
        R1_inputFASTAFile = realpath( args[0] )
        testFile( R1_inputFASTAFile )

        # Pulling the QUAL
        R1_inputQUALFile  = realpath( args[1] )
        testFile( R1_inputQUALFile )

        # Pulling the FASTA
        R2_inputFASTAFile = realpath( args[2] )
        testFile( R2_inputFASTAFile )

        # Pulling the QUAL
        R2_inputQUALFile  = realpath( args[3] )
        testFile( R2_inputQUALFile )

        # Pulling the QUAL
        outputFASTA  = realpath( args[4] )
    #####

    # Speed
    start = time()

    # Setting up output file names
    fo_FileName     = '%s.fasta.bz2'%outputFASTA
    fasta_outHandle = Bzip_file( fo_FileName, 'w' )

    qo_FileName  = '%s.qual.bz2'%outputFASTA
    qual_outHandle  = Bzip_file( qo_FileName, 'w' )

    # Setting up the subprocesses
    if ( isGzipFile(R1_inputFASTAFile) ):
        R1_fastaSub = 'cat %s | gunzip -c'%( R1_inputFASTAFile )
    elif ( isBzipFile(R1_inputFASTAFile) ):
        R1_fastaSub = 'cat %s | bunzip2 -c'%( R1_inputFASTAFile )
    else:
        R1_fastaSub = 'cat %s'%( R1_inputFASTAFile )
    #####

    if ( isGzipFile(R1_inputQUALFile) ):
        R1_qualSub  = 'cat %s | gunzip -c'%( R1_inputQUALFile )
    elif ( isBzipFile(R1_inputQUALFile) ):
        R1_qualSub  = 'cat %s | bunzip2 -c'%( R1_inputQUALFile )
    else:
        R1_qualSub  = 'cat %s'%( R1_inputQUALFile )
    #####

    if ( isGzipFile(R2_inputFASTAFile) ):
        R2_fastaSub = 'cat %s | gunzip -c'%( R2_inputFASTAFile )
    elif ( isBzipFile(R2_inputFASTAFile) ):
        R2_fastaSub = 'cat %s | bunzip2 -c'%( R2_inputFASTAFile )
    else:
        R2_fastaSub = 'cat %s'%( R2_inputFASTAFile )
    #####

    if ( isGzipFile(R2_inputQUALFile) ):
        R2_qualSub  = 'cat %s | gunzip -c'%( R2_inputQUALFile )
    elif ( isBzipFile(R2_inputQUALFile) ):
        R2_qualSub  = 'cat %s | bunzip2 -c'%( R2_inputQUALFile )
    else:
        R2_qualSub  = 'cat %s'%( R2_inputQUALFile )
    #####
    
    # Starting FASTA process
    R1_fastaProc = iterFASTA( subprocess.Popen( R1_fastaSub, shell=True, stdout=subprocess.PIPE).stdout )
    f_record_R1  = next( R1_fastaProc )
    R2_fastaProc = iterFASTA( subprocess.Popen( R2_fastaSub, shell=True, stdout=subprocess.PIPE).stdout )
    f_record_R2  = next( R2_fastaProc )
    fastaDict    = {}
    fastaSet     = set()
    
    # Starting QUAL process
    R1_qualProc  = iterQUAL( subprocess.Popen( R1_qualSub, shell=True, stdout=subprocess.PIPE).stdout )
    q_record_R1  = next( R1_qualProc )
    R2_qualProc  = iterQUAL( subprocess.Popen( R2_qualSub, shell=True, stdout=subprocess.PIPE).stdout )
    q_record_R2  = next( R2_qualProc )
    qualDict     = {}
    qualSet      = set()

    # Main loop
    x            = iterCounter(1000000)
    stopFASTA_R1 = False
    stopFASTA_R2 = False
    stopQUAL_R1  = False
    stopQUAL_R2  = False
    while True:
    
        # Pull FASTA
        if ( not stopFASTA_R1 ): 
            fbaseName, ext, sep = pullBaseName( f_record_R1 )
            # Filling the data structures
            if ( fbaseName in fastaSet ):
                fastaDict[fbaseName].append( ( ext, str(f_record_R1.seq), sep ) )
            else:
                fastaSet.add( fbaseName )
                fastaDict[fbaseName] = [ ( ext, str(f_record_R1.seq), sep ) ]
            #####
            try:
                # Checking to see if it is time to write the entries
                if ( len(fastaDict[fbaseName]) == 2 and len(qualDict[fbaseName]) == 2 ):
                    # Now we have to do something
                    fasta_outHandle.write( makeOutputString( fbaseName, fastaDict[fbaseName]) )
                    qual_outHandle.write(  makeOutputString( fbaseName, qualDict[fbaseName]) )
                    # Handling the removal
                    fastaSet.remove( fbaseName)
                    fastaDict.pop(   fbaseName )
                    qualSet.remove(  fbaseName)
                    qualDict.pop(    fbaseName )
                    x()
                #####
            except KeyError:
                pass
            #####
            try:
                f_record_R1 = next( R1_fastaProc )
            except StopIteration:
                stopFASTA_R1 = True
            #####
        #####

        # Pull FASTA
        if ( not stopFASTA_R2 ): 
            fbaseName, ext, sep = pullBaseName( f_record_R2 )
            # Filling the data structures
            if ( fbaseName in fastaSet ):
                fastaDict[fbaseName].append( ( ext, str(f_record_R2.seq), sep ) )
            else:
                fastaSet.add( fbaseName )
                fastaDict[fbaseName] = [ ( ext, str(f_record_R2.seq), sep ) ]
            #####
            try:
                # Checking to see if it is time to write the entries
                if ( len(fastaDict[fbaseName]) == 2 and len(qualDict[fbaseName]) == 2 ):
                    # Now we have to do something
                    fasta_outHandle.write( makeOutputString( fbaseName, fastaDict[fbaseName]) )
                    qual_outHandle.write(  makeOutputString( fbaseName, qualDict[fbaseName]) )
                    # Handling the removal
                    fastaSet.remove( fbaseName)
                    fastaDict.pop(   fbaseName )
                    qualSet.remove(  fbaseName)
                    qualDict.pop(    fbaseName )
                    x()
                #####
            except KeyError:
                pass
            #####
            try:
                f_record_R2 = next( R2_fastaProc )
            except StopIteration:
                stopFASTA_R2 = True
            #####
        #####

        # Pull QUAL
        if ( not stopQUAL_R1 ): 
            qbaseName, ext, sep = pullBaseName( q_record_R1 )
            # Filling the data structures
            if ( qbaseName in qualSet ):
                qualDict[qbaseName].append( ( ext, ' '.join(['%d'%item for item in q_record_R1.qual]), sep ) )
            else:
                qualSet.add( qbaseName )
                qualDict[qbaseName] = [ ( ext, ' '.join(['%d'%item for item in q_record_R1.qual]), sep ) ]
            #####
            try:
                # Checking to see if it is time to write the entries
                if ( len(fastaDict[qbaseName]) == 2 and len(qualDict[qbaseName]) == 2 ):
                    # Now we have to do something
                    fasta_outHandle.write( makeOutputString(qbaseName, fastaDict[qbaseName]) )
                    qual_outHandle.write(  makeOutputString( qbaseName, qualDict[qbaseName]) )
                    # Handling the removal
                    fastaSet.remove( qbaseName)
                    fastaDict.pop(   qbaseName )
                    qualSet.remove(  qbaseName)
                    qualDict.pop(    qbaseName )
                    x()
                #####
            except KeyError:
                pass
            #####
            try:
                q_record_R1  = next( R1_qualProc )
            except StopIteration:
                stopQUAL_R1 = True
            #####
        #####

        # Pull QUAL
        if ( not stopQUAL_R2 ): 
            qbaseName, ext, sep = pullBaseName( q_record_R2 )
            # Filling the data structures
            if ( qbaseName in qualSet ):
                qualDict[qbaseName].append( ( ext, ' '.join(['%d'%item for item in q_record_R2.qual]), sep ) )
            else:
                qualSet.add( qbaseName )
                qualDict[qbaseName] = [ ( ext, ' '.join(['%d'%item for item in q_record_R2.qual]), sep ) ]
            #####
            try:
                # Checking to see if it is time to write the entries
                if ( len(fastaDict[qbaseName]) == 2 and len(qualDict[qbaseName]) == 2 ):
                    # Now we have to do something
                    fasta_outHandle.write( makeOutputString(qbaseName, fastaDict[qbaseName]) )
                    qual_outHandle.write(  makeOutputString( qbaseName, qualDict[qbaseName]) )
                    # Handling the removal
                    fastaSet.remove( qbaseName)
                    fastaDict.pop(   qbaseName )
                    qualSet.remove(  qbaseName)
                    qualDict.pop(    qbaseName )
                    x()
                #####
            except KeyError:
                pass
            #####
            try:
                q_record_R2  = next( R2_qualProc )
            except StopIteration:
                stopQUAL_R2 = True
            #####
        #####
        
        # Termination condition
        if ( stopFASTA_R1 and stopFASTA_R2 and stopQUAL_R1 and stopQUAL_R2 ): break
    
    #####
    
    fasta_outHandle.close()
    qual_outHandle.close()

    print "Elapsed Time:"    
    print time() - start

    return
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
    
