#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import SeqRecord, writeFASTA, iterFASTA, baseFileName, iterCounter, revComp

from os.path import realpath, isfile, abspath, join, isdir, split, splitext

from os import getpid, uname, chdir, mkdir, chmod, system, curdir

from shutil import rmtree

from optparse import OptionParser

from sys import stderr, stdout

import subprocess

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA] [options]"

    parser = OptionParser(usage)

    markerLength = 1000
    parser.add_option( '-L', \
                       "--markerLength", \
                       type    = "int", \
                       help    = "Length of output marker.  Default: %dbp"%markerLength, \
                       default = markerLength )

    markerSpacing = 5000
    parser.add_option( '-S', \
                       "--markerSpacing", \
                       type    = "int", \
                       help    = "Space between markers.  Default: %dbp"%markerSpacing, \
                       default = markerSpacing )

    copyNumber = 1
    parser.add_option( '-C', \
                       "--copyNumber", \
                       type    = "int", \
                       help    = "Maximum number of times a marker is allowed to occur.  Default: %d"%copyNumber, \
                       default = copyNumber )

    parser.add_option( "-N", \
                       "--noMasking", \
                       action  = 'store_true', \
                       dest    = 'noMasking', \
                       help    = "Do not mask the genome:  Default=False" )
    parser.set_defaults( noMasking = False )

    kmerSize = 24
    parser.add_option( '-m', \
                       "--kmerSize", \
                       type    = "int", \
                       help    = "Kmer size for masking.  Default: %dbp"%kmerSize, \
                       default = kmerSize )

    t = 20
    parser.add_option( '-t', \
                       "--t", \
                       type    = "int", \
                       help    = "-t value for masking.  Default: %d"%t, \
                       default = t )

    hashSize = '2g'
    parser.add_option( "-z", \
                       "--hashSize", \
                       type    = 'str', \
                       help    = "QUAL file associated with query.  Default: %s"%hashSize, \
                       default = hashSize )

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
        # Pulling the fileNames
        fastaFile = realpath(args[0])
        if ( not isfile(fastaFile) ): parser.error( '%s can not be found'%fastaFile )
    #####
    
    markerLength  = options.markerLength
    markerSpacing = options.markerSpacing
    kmerSize      = options.kmerSize
    hashSize      = options.hashSize
    t             = options.t
    
    #=================================================================
    # Step 1:  Set up the subprocess for cat-ing the file
    if ( options.noMasking ):
        p = subprocess.Popen( 'cat %s'%fastaFile, shell=True, stdout=subprocess.PIPE )
    else:
        cmd = r"mask_repeats_hashn -B 100000 -m %d -t %d -z %s %s"%(kmerSize,t,hashSize,fastaFile)
        p = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    #####
    
    #=================================================================
    # Step 2:  Set up the subprocess
    x = iterCounter(1000)
    markerDict = {}
    for record in iterFASTA( p.stdout ):
        tmpSeq   = str(record.seq).upper()
        scaffLen = len(tmpSeq)
        LG       = record.id
        x()
        start = 0
        stop  = start + markerLength
        m     = 0
        while ( True ):
            potentialMarker = tmpSeq[start:stop]
            numNonBases     = potentialMarker.count('N') + potentialMarker.count('X')
            if ( numNonBases == 0 ):
                seqKey = sorted( [potentialMarker,revComp(potentialMarker)] )[0]
                markerID        = '%s-%d|%s|%d'%(LG,start,LG,m)
                r = SeqRecord( id=markerID, seq=potentialMarker, description='' )
                try:
                    markerDict[seqKey].append( r )
                except KeyError:
                    markerDict[seqKey] = [r]
                #####
            #####
            # Incrementing the position
            start = stop + markerSpacing
            stop  = start + markerLength
            m    += 1
            if ( stop > scaffLen ): break
        #####
    #####
    p.poll()

    #=================================================================
    # Step 3:  Write the unique markers
    for tmpSeq, tmpList in markerDict.iteritems():
        if ( len(tmpList) > options.copyNumber ): continue
        writeFASTA( tmpList, stdout, wrap=1000 )
    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
