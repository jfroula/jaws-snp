#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="plott, cplott@hudsonalpha.org"
__date__ ="$Ju1 7, 2014$"
#=========================================================================
# EDIT LOG
# 
# 
#
#
#=========================================================================
import time

from sys import  stderr

from os import getpid, uname, mkdir, chdir, system

import subprocess

from optparse import OptionParser

from os.path import realpath, isdir, isfile, join, abspath, curdir

from shutil import rmtree

import re

from xml.dom import minidom

from time import sleep

import socket

import getpass

#=========================================================================
# Regular Expressions and Formatting
commify_re      = re.compile( r'(?<=\d)(?=(?:\d\d\d)+(?!\d))' ).sub
laneSF          = "{:75} {:17} {:21} {:8} {:17} {:21} {:21} {:12} {:17} {:8} {:12} {:21} {:21}\n"
laneSF_NUGEN    = "{:75} {:17} {:21} {:8} {:17} {:21} {:21} {:12} {:17} {:8} {:12} {:17} {:8} {:21} {:21}\n"
sampleSF        = "{:22} {:17} {:21} {:8} {:12}\n"
feedBack        = "{:65} --{:30}\n"
HTMLlineFormat  = '<tr class="ro1"><td class="ce1"><a href="Illumina/%s" target="_blank">%s</a></td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td>\n'
laneTotHTML     = '<tr class="ro1"><td class="ce1"><a href="Illumina/%s" target="_blank">%s</a></td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td>\n'
runTotHTML      = '<tr class="ro1"><td class="ce1"><a>Run Totals</a></td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1"></td><td class="ce1"></td></tr></table>\n'

HTML_NUGFormat  = '<tr class="ro1"><td class="ce1"><a href="Illumina/%s" target="_blank">%s</a></td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td>\n'
lane_NUGToHTML  = '<tr class="ro1"><td class="ce1"><a href="Illumina/%s" target="_blank">%s</a></td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td>\n'
runTot_NUGHTML  = '<tr class="ro1"><td class="ce1"><a>Run Totals</a></td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1">%s</td><td class="ce1"></td><td class="ce1"></td><td class="ce1"></td><td class="ce1"></td></tr></table>\n'

blankHTML       = '<tr class="ro1"><td colspan="14" class="Separator"></td></tr>\n'

machineNameDict = {"SN700354R":"HiSeq-2500","D00526":"HiSeq-1TB","M00852":"MiSeq","hiseq":"TESTING","D00109":"LevyLab","7001309F":"LevyLab_2","M00700":"LevyLab_MiSeq_1","D00350":"LevyLab_3","D00351":"LevyLab_4","700936F":"NEW-HiSeq-1TB", "X10":"X10","D00688R":"1TB_Lloyd"}
#=========================================================================
def printAndExec( cmd, execute=True ):
    stderr.write( '%s\n\n'%cmd )
    try:
        if ( execute ): system( cmd )
    except KeyboardInterrupt:
        print( "KeyboardInterrupt detected, stopping the execution" )
        exit()
    #####
    return

#=========================================================================
def commify( value ):
    if ( type(value) == type(1) ):
        return commify_re( ',', '%d'%value )
    elif ( type(value) == type('') ):
        return commify_re( ',', value )

#=========================================================================
def decommify( value ):
    newValue   = int(''.join(value.split(",")))
    return newValue

#=========================================================================
def testDirectory(dirName):
    if ( not isdir( dirName ) ): return False
    return True

#=========================================================================
def testFile(fileName):
    if ( not isfile( fileName ) ): raise IOError( 'File not found: %s'%fileName )
    return True

#=========================================================================
def deleteDir(dirName):
    if ( isdir(dirName) ): rmtree( dirName )
    return True

#=========================================================================
def writeStats( IlluminaID, projectBaseName,sampleDict, laneID, subLaneID, clusterMD, clusterSD, subLaneStats_oh, laneStats_oh, laneHTML_oh, laneTotals, DNA_descriptionDict, currentConfigData, ecoliExcludeSet, oldLaneDict=None):
    
    localSampDict = []
    localLaneLine = ''
    
    phixHQBases   = 0
    phixReads     = 0
    totGBases     = 0
    gHQBases      = 0
    genomeReads   = 0
    totalReads    = 0
    totalBases    = 0
    ecoliReads    = 0
    
    phixHQBases_2 = 0
    phixReads_2   = 0
    totGBases_2   = 0
    gHQBases_2    = 0
    genomeReads_2 = 0
    totalReads_2  = 0
    totalBases_2  = 0
    ecoliReads_2  = 0
    
    localKeyList = sampleDict.keys()
    localKeyList.sort()
    
    for sampleStats in localKeyList:
        localSampDict.append('%s'%(sampleSF.format(sampleStats, commify(int(sampleDict[sampleStats][0])), commify(int(sampleDict[sampleStats][1])),sampleDict[sampleStats][2], sampleDict[sampleStats][3])))
        
        laneTotals["totalReads"]  += int(sampleDict[sampleStats][0])
        laneTotals["totalBases"]  += int(sampleDict[sampleStats][1])
        laneTotals["gHQBases"]    += int(sampleDict[sampleStats][4])
        laneTotals["genomeReads"] += int(sampleDict[sampleStats][5])
        laneTotals["totalGBases"] += int(sampleDict[sampleStats][8])
        laneTotals["ecoliRead"]   += int(sampleDict[sampleStats][9])
        if (sampleStats[-2:] == "R1"):
            totalReads                  += int(sampleDict[sampleStats][0])
            totalBases                  += int(sampleDict[sampleStats][1])
            totGBases                   += int(sampleDict[sampleStats][8])
            gHQBases                    += int(sampleDict[sampleStats][4])
            genomeReads                 += int(sampleDict[sampleStats][5])
            phixHQBases                 += int(sampleDict[sampleStats][6])
            phixReads                   += int(sampleDict[sampleStats][7])
            ecoliReads                  += int(sampleDict[sampleStats][9])
            laneTotals["HQ_phix_R1"]    += int(sampleDict[sampleStats][6])
            laneTotals["reads_phix_R1"] += int(sampleDict[sampleStats][7])
        else:
            totalReads_2                += int(sampleDict[sampleStats][0])
            totalBases_2                += int(sampleDict[sampleStats][1])
            totGBases_2                 += int(sampleDict[sampleStats][8])
            gHQBases_2                  += int(sampleDict[sampleStats][4])
            genomeReads_2               += int(sampleDict[sampleStats][5])
            phixHQBases_2               += int(sampleDict[sampleStats][6])
            phixReads_2                 += int(sampleDict[sampleStats][7])
            ecoliReads_2                += int(sampleDict[sampleStats][9])
            laneTotals["HQ_phix_R2"]    += int(sampleDict[sampleStats][6])
            laneTotals["reads_phix_R2"] += int(sampleDict[sampleStats][7])
        #####
    #####
    
    # Format the data
    if (subLaneID == "Undetermined"):
        laneName  = "Lane%s-%s_R1"%(laneID, subLaneID)
    else:
        laneName = "%s_R1"%(DNA_descriptionDict[laneID][subLaneID])
    #####

    if (totalReads < 1):
        HQAVG          = 0
        per_phix       = 0
        per_ecoli      = 0  
        HQAVG_phix     = 0
        readAVG        = 0
        per_ecoli      = 0
        rawCluster     = " "
        PFCluster      = " "
        clusterDensity = " "
    else:
        if (float(genomeReads) > 0):
            HQAVG = "%.1f"%(float(gHQBases) / float(genomeReads)) 
        else:
            HQAVG = 0
        #####
        
        per_phix      = "%.1f%%"%(float(phixReads) * 100.0 / float(totalReads))
        per_ecoli     = "%.1f%%"%(float(ecoliReads) * 100.0 / float(totalReads))  
        try:
            HQAVG_phix = "%.1f"%(float( phixHQBases ) / float(phixReads))
        except ZeroDivisionError:
            HQAVG_phix = "0.0"
        #####  
        readAVG       = "%.1f"%(float(totalBases)/float(totalReads))
        if (clusterMD[laneID][0] != "N/A"):
            rawCluster = "%s"%(commify(clusterMD[laneID][0]))
            PFCluster  = "%s"%(commify(clusterMD[laneID][1]/clusterMD[laneID][0]))
        else:
            rawCluster = " "
            PFCluster  = " "
        #####
        
        clusterDensity = " "
        if ( ( machineNameDict[projectBaseName.split("_")[1]] != "MiSeq" ) and ( subLaneID == 1 ) and ( type(clusterMD[laneID][0]) in ["int","float"] ) ):
            clusterDensity = "%.1f K\mm2"%(float(clusterMD[laneID][0])/2880.0)
        #####
        
        if (totalBases_2 > 0):
            if (float(genomeReads_2) > 0):
                HQAVG_2 = "%.1f"%(float(gHQBases_2) / float(genomeReads_2)) 
            else: 
                HQAVG_2 = 0
            per_phix_2    = "%.1f%%"%(float(phixReads_2) * 100.0 / float(totalReads_2))
            per_ecoli_2   = "%.1f%%"%(float(ecoliReads_2) * 100.0 / float(totalReads)) 
            try:
                HQAVG_phix_2 = "%.1f"%(float( phixHQBases_2 ) / float(phixReads_2))
            except ZeroDivisionError:
                HQAVG_phix_2 = "0.0"
            #####
            readAVG_2     = "%.1f"%(float(totalBases_2)/float(totalReads_2))
            rawCluster_2  = "%s"%(commify(clusterMD[laneID][0]))
            if (clusterMD[laneID][0] != "N/A"):
                rawCluster_2 = "%s"%(commify(clusterMD[laneID][0]))
                PFCluster_2  = "%s"%(commify(clusterMD[laneID][1]/clusterMD[laneID][0]))
            else:
                rawCluster_2 = " "
                PFCluster_2  = " "
            #####
            if (subLaneID == "Undetermined"):
                laneName_2  = "Lane%s-%s_R2"%(laneID, subLaneID)
            else:
                laneName_2 = "%s_R2"%(DNA_descriptionDict[laneID][subLaneID])
            #####
        #####
    #####
    
    if (oldLaneDict):
        
        try:
            if (currentConfigData.isNUGEN):
                localLaneLine = "%s%s"%(laneSF_NUGEN.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ",commify(ecoliReads), per_ecoli, " ",clusterDensity),\
                                        laneSF_NUGEN.format(laneName_2, commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " ",commify(ecoliReads_2), per_ecoli_2, " ",clusterDensity))
                                    
                laneHTML_oh.write(HTML_NUGFormat%("%s/L00%s_%s.stats.png"%(IlluminaID, laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix," ", commify(ecoliReads), per_ecoli, clusterDensity))
                laneHTML_oh.write(HTML_NUGFormat%("%s/Lane%s-%s_samples.stats"%(IlluminaID,laneID, subLaneID),"SPLIT_DATA", commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2," ",commify(ecoliReads_2), per_ecoli_2, " "))
            
            elif(machineNameDict[projectBaseName.split("_")[1]] != "MiSeq"):
                localLaneLine = "%s%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " "," ",clusterDensity),\
                                        laneSF.format(laneName_2, commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," ",clusterDensity))
                                    
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID, laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ",clusterDensity))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID)," ", commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," "))

            else:
                localLaneLine = "%s%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " "," "," "),\
                                        laneSF.format(laneName_2, commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," "," "))
                                    
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID, laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " "," "))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID)," ", commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," "))
            #####
        except UnboundLocalError:
            if (currentConfigData.isNUGEN):
                localLaneLine = "%s"%(laneSF_NUGEN.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, per_phix, " ",commify(ecoliReads), per_ecoli, " ",clusterDensity))
                laneHTML_oh.write(HTML_NUGFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix," ",commify(ecoliReads), per_ecoli, clusterDensity))
            
            elif(machineNameDict[projectBaseName.split("_")[1]] != "MiSeq"):
                localLaneLine = "%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, per_phix, " "," ",clusterDensity))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ",clusterDensity))

            else:
                localLaneLine = "%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, per_phix, " "," "," "))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ", clusterDensity))
            #####
        #####
        
        laneKeys = oldLaneDict.keys()
        laneKeys.sort()
        tmpLine  = ''
        
        for laneKey in laneKeys:
            if (laneKey == laneName):
                tmpLine = "%s%s"%(tmpLine, localLaneLine)
            
            else:
                tmpLine = "%s%s"%(tmpLine,oldLaneDict[laneKey])
            #####
            
            sampleData = oldLaneDict[lane]
            totalReads += decommify( sampleData[1] )
            totalBases += decommify( sampleData[2] )
        #####
    else:
                
        try:
            if (currentConfigData.isNUGEN):
                localLaneLine = "%s%s"%(laneSF_NUGEN.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ",commify(ecoliReads), per_ecoli, " ",clusterDensity),\
                                        laneSF_NUGEN.format(laneName_2, commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " ",commify(ecoliReads_2), per_ecoli_2, " ",clusterDensity))
                                    
                laneHTML_oh.write(HTML_NUGFormat%("%s/L00%s_%s.stats.png"%(IlluminaID, laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix," ", commify(ecoliReads), per_ecoli, clusterDensity))
                laneHTML_oh.write(HTML_NUGFormat%("%s/Lane%s-%s_samples.stats"%(IlluminaID,laneID, subLaneID),"SPLIT_DATA", commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2," ",commify(ecoliReads_2), per_ecoli_2, " "))
            
            elif(machineNameDict[projectBaseName.split("_")[1]] != "MiSeq"):
                localLaneLine = "%s%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " "," ",clusterDensity),\
                                        laneSF.format(laneName_2, commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," ",clusterDensity))
                                    
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID, laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ",clusterDensity))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID)," ", commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," "))

            else:
                localLaneLine = "%s%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " "," "," "),\
                                        laneSF.format(laneName_2, commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," "," "))
                                    
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID, laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " "," "))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID)," ", commify(totalReads_2), commify(totalBases_2), readAVG_2, commify(genomeReads_2), commify(totGBases_2), commify(gHQBases_2), HQAVG_2, commify(phixReads_2), per_phix_2, " "," "))
            #####
        except UnboundLocalError:
            if (currentConfigData.isNUGEN):
                localLaneLine = "%s"%(laneSF_NUGEN.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, per_phix, " ",commify(ecoliReads), per_ecoli, " ",clusterDensity))
                laneHTML_oh.write(HTML_NUGFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix," ",commify(ecoliReads), per_ecoli, clusterDensity))
            
            elif(machineNameDict[projectBaseName.split("_")[1]] != "MiSeq"):
                localLaneLine = "%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, per_phix, " "," ",clusterDensity))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ",clusterDensity))

            else:
                localLaneLine = "%s"%(laneSF.format(laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, per_phix, " "," "," "))
                laneHTML_oh.write(HTMLlineFormat%("%s/L00%s_%s.stats.png"%(IlluminaID,laneID, subLaneID),laneName, commify(totalReads), commify(totalBases), readAVG, commify(genomeReads), commify(totGBases), commify(gHQBases), HQAVG, commify(phixReads), per_phix, " ", " "))
            #####
        #####   
    ##### 
    # Write out lane line
    laneStats_oh.write(localLaneLine)
    # Write out stats lines
    subLaneStats_oh.write(''.join(localSampDict))        
    
    return        

#=========================================================================
def writeLaneTotals( IlluminaID, laneTotals, laneStats_oh, laneID, clusterMD, clusterSD, runTotals, DNA_descriptionDict, laneHTML_oh, currentConfigData, realPhixPercent):
    # Format the data
    
    HQAVG         = "%.1f"%(float(laneTotals["gHQBases"]) / float(laneTotals["genomeReads"])) 
    if (laneTotals["reads_phix_R2"] < 1):
        phred20_phix_R2 = "N/A"
        per_phix        = "%.1f%%"%(float(laneTotals["reads_phix_R1"]) * 100.0 / float(laneTotals["totalReads"]))
    else: 
        phred20_phix_R2 = "%.1f"%(float( laneTotals["HQ_phix_R2"] ) / float(laneTotals["reads_phix_R2"]))
        per_phix        = "%.1f%%"%((float(laneTotals["reads_phix_R1"])+float(laneTotals["reads_phix_R2"])) * 100.0 / float(laneTotals["totalReads"]))
    #####
    
    if (realPhixPercent != ""): per_phix = realPhixPercent
    try:
        HQAVG_phix    = "R1:%.1f R2:%s"%(float( laneTotals["HQ_phix_R1"] ) / float(laneTotals["reads_phix_R1"]),phred20_phix_R2)
    except ZeroDivisionError:
        HQAVG_phix    = "R1:0.0f R2:%s"%(phred20_phix_R2)
    #####
    readAVG       = "%.1f"%(float(laneTotals["totalBases"])/float(laneTotals["totalReads"]))
    if(clusterMD[laneID][0] == "N/A"):
        rawCluster = " "
        PFCluster  = " "
    else:
        rawCluster    = "%s"%(commify(clusterMD[laneID][0]))
        PFCluster     = "%.1f"%(float(clusterMD[laneID][1])*100.0/float(clusterMD[laneID][0]))
    #####
    
    ecoli_per         = "%.1f%%"%(float(laneTotals["ecoliRead"]*100)/float(laneTotals["totalReads"]))
    
    
    laneName      = "Lane%s-Total"%(laneID)
    
    runTotals["totalReads"]  += laneTotals["totalReads"]
    runTotals["totalBases"]  += laneTotals["totalBases"]
    runTotals["YIELD_READS"] += laneTotals["genomeReads"]
    runTotals["YIELD_BASES"] += laneTotals["totalGBases"]
    runTotals["HQ_BASES"]    += laneTotals["gHQBases"]
    phixTotal                 = laneTotals["reads_phix_R1"]+laneTotals["reads_phix_R2"]
    runTotals["PHIX_READS"]  += phixTotal
    
    
    if (currentConfigData.isNUGEN):
        laneStats_oh.write('%s\n'%(laneSF_NUGEN.format(laneName, commify(laneTotals["totalReads"]), commify(laneTotals["totalBases"]),\
                                   readAVG, commify(laneTotals["genomeReads"]), commify( laneTotals["totalGBases"] ),\
                                   commify(laneTotals["gHQBases"]), HQAVG, commify(phixTotal), per_phix, HQAVG_phix, commify(laneTotals["ecoliRead"]), ecoli_per, rawCluster, PFCluster)))
    
    
        laneHTML_oh.write(lane_NUGToHTML%("%s/L00%s.LaneStats.png"%(IlluminaID, laneID), laneName, commify(laneTotals["totalReads"]), commify(laneTotals["totalBases"]),\
                                 readAVG, commify(laneTotals["genomeReads"]), commify( laneTotals["totalGBases"] ),\
                                 commify(laneTotals["gHQBases"]), HQAVG, commify(phixTotal), per_phix, HQAVG_phix, commify(laneTotals["ecoliRead"]), ecoli_per, "%s (%s%%)"%(rawCluster, PFCluster)))

    else:
        laneStats_oh.write('%s\n'%(laneSF.format(laneName, commify(laneTotals["totalReads"]), commify(laneTotals["totalBases"]),\
                                   readAVG, commify(laneTotals["genomeReads"]), commify( laneTotals["totalGBases"] ),\
                                   commify(laneTotals["gHQBases"]), HQAVG, commify(phixTotal), per_phix, HQAVG_phix, rawCluster, PFCluster)))
    
    
        laneHTML_oh.write(laneTotHTML%("%s/L00%s.LaneStats.png"%(IlluminaID, laneID), laneName, commify(laneTotals["totalReads"]), commify(laneTotals["totalBases"]),\
                                 readAVG, commify(laneTotals["genomeReads"]), commify( laneTotals["totalGBases"] ),\
                                 commify(laneTotals["gHQBases"]), HQAVG, commify(phixTotal), per_phix, HQAVG_phix, "%s (%s%%)"%(rawCluster, PFCluster)))
    #####
    
    laneHTML_oh.write(blankHTML)                         
    return

#=========================================================================
def printLanePNG(statsFileList, readLength, statsDict, laneID):
    # Parce the data
    localStatsDict = {}
    phixStatsList  = {'1':[],'2':[]}
    for i in xrange(readLength): 
        phixStatsList['1'].append({"A":0,"T":0,"C":0,"G":0,"N":0,"BT":0,"Q":0,"QT":0})
        phixStatsList['2'].append({"A":0,"T":0,"C":0,"G":0,"N":0,"BT":0,"Q":0,"QT":0})
    #####
    localBaseName  = "%s/L00%s"%(statsDict, laneID)
    for statsFile in statsFileList:
        libFlag  = statsFile.split("/")[-1].split("_")[1]
        readFlag = statsFile.split("/")[-1].split("_")[-1][1] 
        try:
            localStatsDict[libFlag]["1"]
        except KeyError:
            localStatsDict[libFlag]      = {}
            localStatsDict[libFlag]["1"] = []
            localStatsDict[libFlag]["2"] = []
            for i in xrange(readLength): 
                localStatsDict[libFlag]["1"].append({"A":0,"T":0,"C":0,"G":0,"N":0,"BT":0,"Q":0,"QT":0})
                localStatsDict[libFlag]["2"].append({"A":0,"T":0,"C":0,"G":0,"N":0,"BT":0,"Q":0,"QT":0})
            #####
        #####
        
        for line in open(statsFile):
            splitLine = line.split(None)
            if (splitLine[0] == "#") : continue
            baseLoc = int(splitLine[0])-1
            localStatsDict[libFlag][readFlag][baseLoc]["Q"]  += float(splitLine[1])
            localStatsDict[libFlag][readFlag][baseLoc]["A"]  += float(splitLine[2])
            localStatsDict[libFlag][readFlag][baseLoc]["T"]  += float(splitLine[3])
            localStatsDict[libFlag][readFlag][baseLoc]["C"]  += float(splitLine[4])
            localStatsDict[libFlag][readFlag][baseLoc]["G"]  += float(splitLine[5])
            localStatsDict[libFlag][readFlag][baseLoc]["N"]  += float(splitLine[6])
            localStatsDict[libFlag][readFlag][baseLoc]["BT"] += float(splitLine[7])
            localStatsDict[libFlag][readFlag][baseLoc]["QT"] += float(splitLine[8])
            phixStatsList[readFlag][baseLoc]["Q"]            += float(splitLine[9])
            phixStatsList[readFlag][baseLoc]["A"]            += float(splitLine[10])
            phixStatsList[readFlag][baseLoc]["T"]            += float(splitLine[11])
            phixStatsList[readFlag][baseLoc]["C"]            += float(splitLine[12])
            phixStatsList[readFlag][baseLoc]["G"]            += float(splitLine[13])
            phixStatsList[readFlag][baseLoc]["N"]            += float(splitLine[14])
            phixStatsList[readFlag][baseLoc]["BT"]           += float(splitLine[15])
            phixStatsList[readFlag][baseLoc]["QT"]           += float(splitLine[16])
        #####
    #####        
    # Write the data
    
    plotHeader     = "reset\nset key outside\nset terminal pngcairo enhanced\nset output '%s.LaneStats.png'\n"%(localBaseName)
    plotLayout     = "set terminal png nocrop size 2000,1111 font 'Helvetica,12'\nset multiplot layout 1,1 title '%s'\n"%(localBaseName)
    plotMargins    = "set grid x\nset tmargin 1\nset bmargin 3\nset lmargin 8\nset rmargin 20\n"
    qualLables_R1  = "set border linewidth 2\nset ytic\nset yrange[0:45]\nset xtics 0,%d,%d\nset xrange[0:%d]\nset ylabel 'Average Quality_R1'\nset xlabel 'Base Position' offset 0,.5\n"%(int(readLength/10),readLength,readLength)
    seqLables_R1   = "\n"
    qualLables_R2  = ""
    seqLables_R2   = ""
    qualPlot_R1    = "plot "
    qualPlot_R2    = "plot "
    seqPlot_R1     = " "
    seqPlot_R2     = " "
    
    libKeyList      = localStatsDict.keys()
    libKeyList.sort()
    tmpPID          = [None, getpid()][bool(True)]
    identfier       = r'%s.%s'%( uname()[1], tmpPID )
    plotLineStyles  = ''
    counter         = 0
    for libKey in libKeyList:
        oh = open("lane%s_R1.%s.dat"%(libKey, identfier),'w')
        title = libKey
        if (libKey == "Undetermined"):
            title = "Und"
        #####
        locationCounter = 1
        plotLineStyles += "set style line %d lw 2\nset style line %d lw 2\nset style line %d lw 2\nset style line %d lw 2\nset style line %d lw 2\n"%((counter*4)+1,(counter*4)+2,(counter*4)+3,(counter*4)+4,(counter*4)+5) 
        for bpLoc in localStatsDict[libKey]['1']:
            if (bpLoc["BT"] == 0): continue
            locA = bpLoc["A"]/bpLoc["BT"]
            locT = bpLoc["T"]/bpLoc["BT"]
            locC = bpLoc["C"]/bpLoc["BT"]
            locG = bpLoc["G"]/bpLoc["BT"]
            locN = bpLoc["N"]/bpLoc["BT"]
            locQ = bpLoc["Q"]/bpLoc["QT"]
            oh.write("%d %.2f %.2f %.2f %.2f %.2f %.2f\n"%(locationCounter, locA, locT, locC, locG, locN, locQ))
            locationCounter += 1
        #####
        oh.close()
        
        qualPlot_R1 += "'lane%s_R1.%s.dat' using 1:7 title '%s_%s_Avg' with lines ls %d,"%(libKey, identfier, localBaseName.split("/")[-1], title, (counter+1))
        
        try:
            qValCheck = localStatsDict['1']['2'][0]["Q"]
        except KeyError:
            qValCheck = localStatsDict['2']['2'][0]["Q"]
        #####
        
        if (qValCheck > 0):
            plotLayout     = "set terminal png nocrop size 2000,1111 font 'Helvetica,12'\nset multiplot layout 1,2 title '%s'\n"%(localBaseName)
            oh = open("lane%s_R2.%s.dat"%(libKey, identfier),'w') 
            locationCounter = 1
            title = libKey
            if (libKey == "Undetermined"):
                title = "Und"
            #####
            for bpLoc in localStatsDict[libKey]['2']:
                if (bpLoc["BT"] == 0): continue
                locA = bpLoc["A"]/bpLoc["BT"]
                locT = bpLoc["T"]/bpLoc["BT"]
                locC = bpLoc["C"]/bpLoc["BT"]
                locG = bpLoc["G"]/bpLoc["BT"]
                locN = bpLoc["N"]/bpLoc["BT"]
                locQ = bpLoc["Q"]/bpLoc["QT"]
                oh.write("%d %.2f %.2f %.2f %.2f %.2f %.2f\n"%(locationCounter, locA, locT, locC, locG, locN, locQ))
                locationCounter += 1
            #####
            oh.close()
            
            qualLables_R2 = "\nset ylabel 'Average Quality_R2'\nset xlabel 'Base Position' offset 0,.5\n"
            
            qualPlot_R2  += "'lane%s_R2.%s.dat' using 1:7 title '%s_%s_Avg' with lines ls %d,"%(libKey, identfier, localBaseName.split("/")[-1], title, (counter+1))
        #####
        counter += 1    
    #####
    
    plotLineStyles += "\nset style line %d lw 2\nset style line %d lw 2\nset style line %d lw 2\nset style line %d lw 2\nset style line %d lw 2\n"%((counter*4)+1,(counter*4)+2,(counter*4)+3,(counter*4)+4,(counter*4)+5)
    oh = open("lanePhix_R1.%s.dat"%(identfier),'w')
    locationCounter = 1
    for bpLoc in phixStatsList['1']:
        if (bpLoc["BT"] == 0): continue
        locA = bpLoc["A"]/bpLoc["BT"]
        locT = bpLoc["T"]/bpLoc["BT"]
        locC = bpLoc["C"]/bpLoc["BT"]
        locG = bpLoc["G"]/bpLoc["BT"]
        locN = bpLoc["N"]/bpLoc["BT"]
        locQ = bpLoc["Q"]/bpLoc["QT"]
        oh.write("%d %.2f %.2f %.2f %.2f %.2f %.2f\n"%(locationCounter, locA, locT, locC, locG, locN, locQ))
        locationCounter += 1
    #####    
    
    qualPlot_R1 += "'lanePhix_R1.%s.dat' using 1:7 title '%s_Phix_Avg' with lines ls %d,"%(identfier, localBaseName.split("/")[-1], (counter+1))
    #####
    oh.close()
    
    if (phixStatsList['2'][0]["Q"]>0):
        oh = open("lanePhix_R2.%s.dat"%(identfier),'w')
        locationCounter = 1
        for bpLoc in phixStatsList['2']:
            if (bpLoc["BT"] == 0): continue
            locA = bpLoc["A"]/bpLoc["BT"]
            locT = bpLoc["T"]/bpLoc["BT"]
            locC = bpLoc["C"]/bpLoc["BT"]
            locG = bpLoc["G"]/bpLoc["BT"]
            locN = bpLoc["N"]/bpLoc["BT"]
            locQ = bpLoc["Q"]/bpLoc["QT"]
            oh.write("%d %.2f %.2f %.2f %.2f %.2f %.2f\n"%(locationCounter, locA, locT, locC, locG, locN, locQ))
            locationCounter += 1
        #####
        oh.close()
        
        qualPlot_R2 += "'lanePhix_R2.%s.dat' using 1:7 title '%s_Phix_Avg' with lines ls %d,"%(identfier, localBaseName.split("/")[-1], (counter+1))
    #####
    
    # Build plot file and plot the data
    plot_oh = open("plot.%s.dat"%(identfier), 'w')
    if (seqLables_R2 == ""):
        plot_oh.write("%s\n%s\n%s\n%s\n%s 30 with lines ls 3\n%s\n%s\n"%(plotHeader, plotLayout, plotMargins, qualLables_R1, qualPlot_R1, seqLables_R1, seqPlot_R1))
    else:
        plot_oh.write("%s\n%s\n%s\n%s\n%s 30 with lines ls 3\n%s\n%s 30 with lines ls 3\n%s\n%s\n%s\n%s\n"%(plotHeader, plotLayout, plotMargins, qualLables_R1, qualPlot_R1, qualLables_R2, qualPlot_R2, seqLables_R1, seqPlot_R1, seqLables_R2, seqPlot_R2))
    #####
    plot_oh.close()
    system('gnuplot plot.%s.dat'%(identfier))
    system('rm -f plot.%s.dat lane*.%s.dat'%(identfier, identfier))
    return

#=========================================================================
def createTmpDirectory(sampleName):
    basePath = abspath(curdir)
    tmpDir   = sampleName
    tmpPath  = join( basePath, tmpDir )
    deleteDir(tmpPath)
    mkdir(tmpDir, 0o777)
    testDirectory(tmpDir)
    return tmpPath

#=========================================================================
class configData(object):
    def __init__(self, configFile, projectDataPath):
        self.configFile  = configFile
        self.rawDataPath = projectDataPath
        self.isNUGEN     = False
        self.nugenName   = False
        self.parseMethod = self.BuildFileList
        self.fileList    = self.parseMethod(self.configFile)
    #####            

    def BuildFileList(self, configFile):
        sampleDict = {}
        laneSet    = set()
        for line in open(configFile):
            splitLine = line.split(",")
            if (splitLine[0] == "FCID") : continue
            splitLine.pop(0)
            
            laneID      = int(splitLine[0])
            laneSet.add(laneID)

            subLane     = int(splitLine[1].split("-")[1].split("N")[0])
            if (line.find("NUGEN96")>-1): 
                self.nugenName = True
                self.isNUGEN   = True
            #####            
            
            if (splitLine[4][0] == "+"):
                humanRead   = splitLine[4][1:].split("+")[0]    
            else:
                humanRead   = splitLine[4].split("+")[0]
            #####
                
            if (humanRead == ""):humanRead = splitLine[1]
            
            # Does the lane already exist in the list
            try:
                sampleDict[laneID]
            except KeyError:
                sampleDict[laneID] = {}
            #####
            
            # Does the lane already exist in the list
            try:
                sampleDict[laneID][subLane]
            except KeyError:
                sampleDict[laneID][subLane] = {}
            #####
            
            localProc = subprocess.Popen('ls %sSample_%s/*.fastq.*'%(self.rawDataPath, splitLine[1]), shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            fileErr   = [item for item in localProc.stderr]
            if (len(fileErr)>0):
                stderr.write("\t%s DOES NOT HAVE A DIRECTORY...\n\t\tTHERE WILL BE NO DATA FOR THIS SAMPLE\n\t\tCONTINUING WITH THE REST OF THE CONFIG FILE.\n"%(splitLine[1]))
                continue
            #####            
            
            fileList = []
            R2_files = False
                    
            for data in localProc.stdout:
                extention = data.split(None)[0].split(".")[-1]
                Local_splitLine = data.split("_R2")
                if (len(Local_splitLine) > 1): 
                    R2_files = True
                #####
            #####
            
            fileList.append('%sSample_%s/*_R1*.fastq.%s'%(self.rawDataPath, splitLine[1], extention))
            if (R2_files):
                fileList.append('%sSample_%s/*_R2*.fastq.%s'%(self.rawDataPath, splitLine[1], extention))
            #####
            
            sample = splitLine[2]
            if ( self.isNUGEN == True ):
                sample = splitLine[1].split("-")[-1]
            #####
             
            sampleDict[laneID][subLane][sample] = [fileList, humanRead]
            
        #####
        
        laneSet = list(laneSet)
        laneSet.sort()
        for lane in laneSet:
            localProc_1 = subprocess.Popen('ls %sSample_lane%s-Undetermined/*R1*.fastq.*'%(self.rawDataPath, lane), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            extention   = localProc_1.communicate()[0]
            
            localProc_2 = subprocess.Popen('ls %sSample_lane%s-Undetermined/*R2*.fastq.*'%(self.rawDataPath, lane), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            extention2   = localProc_2.communicate()[0]
            
            sampleDict[lane]["Undetermined"] = {}
            
            if ((localProc_1.returncode == 0) and (localProc_2.returncode == 0)):
                extention   = extention.split(None)[0].split(".")[-1]
                file_1      = '%sSample_lane%s-Undetermined/*_R1*.fastq.%s'%(self.rawDataPath, lane, extention)
                file_2      = '%sSample_lane%s-Undetermined/*_R2*.fastq.%s'%(self.rawDataPath, lane, extention)
                sampleDict[lane]["Undetermined"]["Undetermined"] = [[file_1,file_2],"Lane%s_Undetermined"%(lane)]
            elif ((localProc_1.returncode == 0) and (localProc_2.returncode != 0)):
                extention   = extention.split(None)[0].split(".")[-1]
                file_1      = '%sSample_lane%s-Undetermined/*_R1*.fastq.%s'%(self.rawDataPath, lane, extention)
                sampleDict[lane]["Undetermined"]["Undetermined"] = [[file_1],"Lane%s_Undetermined"%(lane)]
            else:
                continue 
            #####
        #####
        for lane in laneSet:
            try:
                if(sampleDict[lane]["Undetermined"]["Undetermined"][0] == []): sampleDict[lane].pop("Undetermined")
            except KeyError:
                sampleDict[lane].pop("Undetermined")
            #####
        #####
        return sampleDict
    #####
    
#=========================================================================
class configDataV2(object):
    def __init__(self, configFile, projectDataPath,clusterMD):
        self.configFile  = configFile
        self.rawDataPath = projectDataPath
        self.isNUGEN     = False
        self.nugenName   = False
        self.parseMethod = self.BuildFileList
        self.fileList    = self.parseMethod(self.configFile,clusterMD)
    #####            

    def BuildFileList(self, configFile,clusterMD):
        sampleDict = {}
        laneSet    = set()
        skipSet    = set(["[Data]\n","Sample_Project","[Data]"])
        for line in open(configFile):
            splitLine = line.split(",")
            if (splitLine[0] in skipSet) : continue
            try:
                laneID      = int(splitLine[1])
            except IndexError:
                print splitLine
                print "MIKE I MENT FOR THIS TO FAIL"
                assert False
            #####
            laneSet.add(laneID)
             
            subLane     = int(splitLine[2].split("-")[1].split("N")[0])
            
            cmdMD  = "cat %s/V2Results/Reports/html/*/DefaultProject/%s/all/lane.html | grep  '[0-9]' | grep '<td>' | head -3 | tail -1 |cut -f2 -d'>' | cut -f1 -d'<' | sed 's/,//g'"%(self.rawDataPath, splitLine[3])
            cmd1MD = "cat %s/V2Results/Reports/html/*/DefaultProject/%s/all/lane.html | grep  '[0-9]' | grep '<td>' | head -4 | tail -1 |cut -f2 -d'>' | cut -f1 -d'<' | sed 's/,//g'"%(self.rawDataPath, splitLine[3])

            if (line.find("NUGEN96")>-1): 
                self.nugenName = True
                self.isNUGEN   = True
                cmdMD  = "cat %s/V2Results/Reports/html/*/all/all/all/lane.html | grep  '[0-9]' | grep '<td>' | head -3 | tail -1 |cut -f2 -d'>' | cut -f1 -d'<' | sed 's/,//g'"%(self.rawDataPath)
                cmd1MD = "cat %s/V2Results/Reports/html/*/all/all/all/lane.html | grep  '[0-9]' | grep '<td>' | head -4 | tail -1 |cut -f2 -d'>' | cut -f1 -d'<' | sed 's/,//g'"%(self.rawDataPath)
            #####            
            
            
            try:
                humanRead   = splitLine[6].strip()
            except IndexError:
                humanRead   = splitLine[3]
            #####
            
            # Does the lane already exist in the list
            try:
                sampleDict[laneID]
            except KeyError:
                sampleDict[laneID] = {}
            #####
            
            # Does the lane already exist in the list
            try:
                sampleDict[laneID][subLane]
            except KeyError:
                sampleDict[laneID][subLane] = {}
            #####
            try:
                clusterMD[laneID][0] = int([item.strip() for item in subprocess.Popen(cmdMD,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE).stdout][0])
                clusterMD[laneID][1] = int([item.strip() for item in subprocess.Popen(cmd1MD,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE).stdout][0])
            except IndexError:
                clusterMD[laneID] = ["N/A", "N/A"]
            #####
            localProc = subprocess.Popen('ls %s/V2Results/DefaultProject/%s/*.fastq.*'%(self.rawDataPath, splitLine[2]), shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            fileErr   = [item for item in localProc.stderr]
            if (len(fileErr)>0):
                stderr.write("\t%s DOES NOT HAVE A DIRECTORY...\n\t\tTHERE WILL BE NO DATA FOR THIS SAMPLE\n\t\tCONTINUING WITH THE REST OF THE CONFIG FILE.\n"%(splitLine[1]))
                continue
            #####            
            
            fileList = []
            R2_files = False
                    
            for data in localProc.stdout:
                extention = data.split(None)[0].split(".")[-1]
                Local_splitLine = data.split("_R2")
                if (len(Local_splitLine) > 1): 
                    R2_files = True
                #####
            #####
            
            fileList.append('%s/V2Results/DefaultProject/%s/*_R1*.fastq.%s'%(self.rawDataPath, splitLine[2], extention))
            if (R2_files):
                fileList.append('%s/V2Results/DefaultProject/%s/*_R2*.fastq.%s'%(self.rawDataPath, splitLine[2], extention))
            #####
 
            sample = splitLine[3]
            if ( self.isNUGEN == True ):
                sample = splitLine[2].split("-")[-1]
            #####
             
            sampleDict[laneID][subLane][sample] = [fileList, humanRead]
            
        #####
        
        laneSet = list(laneSet)
        laneSet.sort()
        for lane in laneSet:
            localProc_1 = subprocess.Popen('ls %s/V2Results/Undetermined_S0_L00%d_R1_*.fastq.*'%(self.rawDataPath, lane), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            extention   = localProc_1.communicate()[0]
            
            localProc_2 = subprocess.Popen('ls %s/V2Results/Undetermined_S0_L00%d_R2_*.fastq.*'%(self.rawDataPath, lane), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            extention2   = localProc_2.communicate()[0]
            
            sampleDict[lane]["Undetermined"] = {}
            
            if ((localProc_1.returncode == 0) and (localProc_2.returncode == 0)):
                extention   = extention.split(None)[0].split(".")[-1]
                file_1      = '%s/V2Results/Undetermined_S0_L00%d_R1_*.fastq.%s'%(self.rawDataPath, lane, extention)
                file_2      = '%s/V2Results/Undetermined_S0_L00%d_R2_*.fastq.%s'%(self.rawDataPath, lane, extention)
                sampleDict[lane]["Undetermined"]["Undetermined"] = [[file_1,file_2],"Lane%s_Undetermined"%(lane)]
            elif ((localProc_1.returncode == 0) and (localProc_2.returncode != 0)):
                extention   = extention.split(None)[0].split(".")[-1]
                file_1      = '%s/V2Results/Undetermined_S0_L00%d_R1_*.fastq.%s'%(self.rawDataPath, lane, extention)
                sampleDict[lane]["Undetermined"]["Undetermined"] = [[file_1],"Lane%s_Undetermined"%(lane)]
            else:
                continue 
            #####
        #####
        for lane in laneSet:
            try:
                if(sampleDict[lane]["Undetermined"]["Undetermined"][0] == []): sampleDict[lane].pop("Undetermined")
            except KeyError:
                sampleDict[lane].pop("Undetermined")
            #####
        #####
        return sampleDict
    #####
    

#=========================================================================
def checkSpace(targetDir, currentDataDir, projectBaseName, start=True):
    # Is there enough room to move the data to ILLPROCDATA
    checkPROCspace   = subprocess.Popen("df %s"%(targetDir), shell=True, stdout=subprocess.PIPE) 
    PROCspace        = int(checkPROCspace.communicate()[0].split("\n")[-2].split(None)[3])
    
    checkLocalSize   = subprocess.Popen("du %s"%(currentDataDir), shell=True, stdout=subprocess.PIPE)
    LocalSize        = int(checkLocalSize.communicate()[0].split("\n")[-2].split(None)[0])
    
    checkPROCspace_h = subprocess.Popen("df -h %s"%(targetDir), shell=True, stdout=subprocess.PIPE) 
    PROCspace_h      = checkPROCspace_h.communicate()[0].split("\n")[-2].split(None)[3]
    
    checkLocalSize_h = subprocess.Popen("du -h %s"%(currentDataDir), shell=True, stdout=subprocess.PIPE)
    LocalSize_h      = checkLocalSize_h.communicate()[0].split("\n")[-2].split(None)[0]
    
    if ( start ):
        if ( PROCspace > (LocalSize + (LocalSize * .08)) ): # real current version LocalSize * .55; older formula (LocalSize + LocalSize + (LocalSize * .05)
            pass
        else:
            myEmailCommand = subprocess.Popen('mail -s "I NEED SPACE-%s" mfriz@hagsc.org'%(projectBaseName), shell=True, stdin=subprocess.PIPE)
            myOut = myEmailCommand.communicate(input = "Try to move data for %s but there is not enough space.\nAvalable on %s:%s\nNeeded on %s:%s\n\n-fastqQC"%(projectBaseName, targetDir, PROCspace_h, targetDir, LocalSize_h))[0]
            sendEmail = False
            raw_input("\nPRESS ENTER TO BEGIN:\n")
        #####
    else:
        if ( PROCspace > (LocalSize + (LocalSize * .05))):
            pass
        else:
            myEmailCommand = subprocess.Popen('mail -s "I NEED SPACE-%s"  %s@hagsc.org -c mfriz@hagsc.org'%(projectBaseName, getpass.getuser()), shell=True, stdin=subprocess.PIPE)
            myOut = myEmailCommand.communicate(input = "Try to move data for %s but there is not enough space.\nAvalable on %s:%s\nNeeded on %s:%s\n\n-fastqQC"%(projectBaseName, targetDir, PROCspace_h, targetDir, LocalSize_h))[0]
            sendEmail = False
            raw_input("\nPRESS ENTER TO BEGIN DATA TRANSFER:\n")
        #####
    #####
    return

#=========================================================================

def real_main():
    # Defining the program options
    usage = "usage: %prog [Project name] [options]"

    parser = OptionParser(usage)

    parser.add_option( '-s', \
                       "--noSeqQual", \
                       action  = "store_false", \
                       dest    = 'noSeqQual', \
                       help    = "Do not create fasta and qual files.  Default:  Will create fasta and qual." )
    parser.set_defaults( noSeqQual = True )
    
    parser.add_option( '-t', \
                       "--TESTING", \
                       action  = "store_true", \
                       dest    = 'TESTING', \
                       help    = "Run in testing mode,(No Eamil Notifications). Default: FALSE" )
    parser.set_defaults( TESTING = False )
    
    parser.add_option( '-v', \
                       "--VERSION", \
                       action  = "store_true", \
                       dest    = 'VERSION', \
                       help    = "Run was run using version 2 software on the sequencer. Default: FALSE" )
    parser.set_defaults( VERSION = False )
    
    parser.add_option( '-e', \
                       "--noEcoli", \
                       type    = 'str', \
                       help    = 'Coma separated list of lanes to exclude from ecoli screen:  Default exclude no lanes.', \
                       default = None )

    defaultDir = "/home/raid2/ILLRAWDATA/AnalysisInProgress/"
    parser.add_option( '-d', \
                       "--locDir", \
                       type    = 'str', \
                       help    = 'Location of project directory:  Default- %s'%(defaultDir), \
                       default = defaultDir )
                       
    parser.add_option( '-D', \
                       "--UseData", \
                       action  = "store_true", \
                       dest    = 'UseData', \
                       help    = "Force use of /home/f3p2/illuminaProcessingTmp/ . Default: FALSE" )
    parser.set_defaults( UseData = False )
                       
    # Parsing the arguements
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "NO PROJECT NAME WAS GIVEN. " + \
                      "View usage using --help option." )
    else:
        errorLog = open("ErrorLog.%s.dat"%(args[0]), 'w')
        projectBaseName = args[0]
        
        # Find the data
        projectPath     = join( options.locDir, projectBaseName )       
        
        if (not testDirectory( projectPath )): parser.error( "PROJECT DOES NOT EXIST:%s."%projectPath )
        
        # Find the final home
        finalHomeDir  = "/home/raid2/ILLOVERFLOW1"
        websiteScript = "/home/mfriz/scripts/illumina/updateWebsite.sh &"
        if (options.TESTING):
            finalHomeDir  = "/home/raid2/ILLTEST/Processed"
            websiteScript = "/home/mfriz/scripts/illumina/updateTestWebsite.sh &"
        #####
        
        # Do any files get screened for ecoli?
        ecoliExcludeSet = set()
        if ( options.noEcoli ): ecoliExcludeSet = set(options.noEcoli.split(","))
        
        # WHAT VERSION DO I LOOK FOR?
        if ( not options.VERSION ):
            # VERSION 1
            projectDataPath = join (projectPath, "Unaligned/Project_DefaultProject/")
            
            if (not testDirectory( projectDataPath )): parser.error( "DIRECTORY STRUCTURE IS OFF %s DOES NOT EXIST!!!."%projectDataPath )
            
            configFileProc = subprocess.Popen("ls %s/Data/Intensities/BaseCalls/SampleSheet-*.csv"%(projectPath), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            configName     = configFileProc.communicate()[0].strip()
            IlluminaID     = "Illumina-%s"%(configName.split("-")[-1].split(".")[0])
            configFile     = "%s"%(configName)
            testFile(configFile)
            
            currentConfigData = configData(configFile, projectDataPath)
            
            # Does a BUSTARD file exist
            myXMLproc    = subprocess.Popen("ls %s/Unaligned/Basecall_Stats_*/BustardSummary.xml"%(projectPath) , shell = True , stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            myXMLprocOut = myXMLproc.communicate()[0].strip()
            # Yes Parse it for clustering data
            if (myXMLproc.returncode == 0):
                busttardDoc = minidom.parse(myXMLprocOut)
                clusterSD = {}
                clusterMD = {}
                # Raw SD, passed SD
                for key in xrange(len(currentConfigData.fileList.keys())):
                    nodeVal = key*2 + 1
                    clusterSD[key+1] = [int(busttardDoc.childNodes[1].childNodes[9].childNodes[1].childNodes[nodeVal].childNodes[3].childNodes[3].toxml().split(">")[1].split("</")[0]),\
                                             int(busttardDoc.childNodes[1].childNodes[9].childNodes[1].childNodes[nodeVal].childNodes[1].childNodes[3].toxml().split(">")[1].split("</")[0])]
                # Raw mean, passed mean
                    clusterMD[key+1] = [int(busttardDoc.childNodes[1].childNodes[9].childNodes[1].childNodes[nodeVal].childNodes[3].childNodes[1].toxml().split(">")[1].split("</")[0]),\
                                             int(busttardDoc.childNodes[1].childNodes[9].childNodes[1].childNodes[nodeVal].childNodes[1].childNodes[1].toxml().split(">")[1].split("</")[0])]
                
                #####
                realPhixPercent = ""
                
            # No just assign nothing   
            else:
                myXMLproc    = subprocess.Popen("ls %s/Data/Intensities/BaseCalls/Alignment/GenerateFASTQRunStatistics.xml"%(projectPath) , shell = True , stdout = subprocess.PIPE, stderr=subprocess.PIPE)
                myXMLprocOut = myXMLproc.communicate()[0].strip()
                # Yes Parse it for clustering data
                if (myXMLproc.returncode == 0):
                    busttardDoc = minidom.parse(myXMLprocOut)
                    clusterSD = {}
                    clusterMD = {}
                    # Raw SD, passed SD                    
                    clusterSD       = {1:["N/A", "N/A"], 2:["N/A", "N/A"],3:["N/A", "N/A"], 4:["N/A", "N/A"],5:["N/A", "N/A"], 6:["N/A", "N/A"],7:["N/A", "N/A"], 8:["N/A", "N/A"], 9:["N/A", "N/A"]}
                    clusterMD       = {1:[int(busttardDoc.childNodes[0].childNodes[5].childNodes[9].toxml().split(">")[1].split("</")[0]), int(busttardDoc.childNodes[0].childNodes[5].childNodes[7].toxml().split(">")[1].split("</")[0])], 2:["N/A", "N/A"],3:["N/A", "N/A"], 4:["N/A", "N/A"],5:["N/A", "N/A"], 6:["N/A", "N/A"],7:["N/A", "N/A"], 8:["N/A", "N/A"], 9:["N/A", "N/A"]}
                    realPhixPercent = "%.1f%%"%(float(busttardDoc.childNodes[0].childNodes[5].childNodes[19].toxml().split(">")[1].split("</")[0])*100.0/float(busttardDoc.childNodes[0].childNodes[5].childNodes[9].toxml().split(">")[1].split("</")[0]))
                else:
                    clusterSD       = {1:["N/A", "N/A"], 2:["N/A", "N/A"],3:["N/A", "N/A"], 4:["N/A", "N/A"],5:["N/A", "N/A"], 6:["N/A", "N/A"],7:["N/A", "N/A"], 8:["N/A", "N/A"], 9:["N/A", "N/A"]}
                    clusterMD       = {1:["N/A", "N/A"], 2:["N/A", "N/A"],3:["N/A", "N/A"], 4:["N/A", "N/A"],5:["N/A", "N/A"], 6:["N/A", "N/A"],7:["N/A", "N/A"], 8:["N/A", "N/A"], 9:["N/A", "N/A"]}
                    realPhixPercent = ""
                #####
            #####
        else:
            # VERSION 2
            projectDataPath = join (projectPath, "V2Results/DefaultProject")
            
            if (not testDirectory( projectDataPath )): parser.error( "DIRECTORY STRUCTURE IS OFF %s DOES NOT EXIST!!!."%projectDataPath )
            
            # PULL THE FILES THAT NEED PROCESSING
            configFileProc = subprocess.Popen("ls %s/SampleSheet-*.csv"%(projectPath), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            configName     = configFileProc.communicate()[0].strip()
            IlluminaID     = "Illumina-%s"%(configName.split("-")[-1].split(".")[0])
            configFile     = "%s"%(configName)
            testFile(configFile)
            clusterMD         = {1:["N/A", "N/A"], 2:["N/A", "N/A"],3:["N/A", "N/A"], 4:["N/A", "N/A"],5:["N/A", "N/A"], 6:["N/A", "N/A"],7:["N/A", "N/A"], 8:["N/A", "N/A"], 9:["N/A", "N/A"]}
            currentConfigData = configDataV2(configFile, projectPath,clusterMD)
            
            #THIS IS NOT IMPLIMENTED YET BUT IT WILL NEED TO CALCULATE THE CLUSTER DENSITY
            clusterSD       = {1:["N/A", "N/A"], 2:["N/A", "N/A"],3:["N/A", "N/A"], 4:["N/A", "N/A"],5:["N/A", "N/A"], 6:["N/A", "N/A"],7:["N/A", "N/A"], 8:["N/A", "N/A"], 9:["N/A", "N/A"]}
            realPhixPercent = ""
        #####
        
        # PULL THE DATA NEEDED FOR THE HEADER LINE
        HeaderLine   = ""
        
        if (machineNameDict[projectBaseName.split("_")[1]] != "X10"):
            runInfoProc  = subprocess.Popen("ls %s/RunInfo.xml"%(projectPath) , shell = True , stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            runInfoOut   = runInfoProc.communicate()[0].strip()
            runInfoDoc   = minidom.parse(runInfoOut)
            runInfoCount = 1
            while(True):
                try:
                    ifIndex = str(runInfoDoc.childNodes[0].childNodes[1].childNodes[7].childNodes[runInfoCount].toxml()).split("<")[1].split("/>")[0].split("IsIndexedRead=")[1][1]
                    if (ifIndex == "Y"):
                        runInfoName = "Index"
                    else:
                        if (str(runInfoDoc.childNodes[0].childNodes[1].childNodes[7].childNodes[runInfoCount].toxml()).split("<")[1].split("/>")[0].split("Number=")[1][1] == '1'):
                            runInfoName = "R1"
                        else:
                            runInfoName = "R2"
                        #####
                    #####
                    runInfoLength    = int(str(runInfoDoc.childNodes[0].childNodes[1].childNodes[7].childNodes[runInfoCount].toxml()).split("<")[1].split("/>")[0].split("NumCycles=")[1].split("\"")[1])
                    if (runInfoName == "R1"):
                        readLength = runInfoLength
                    #####
                     
                    runInfoCount += 2
                    HeaderLine   += "%s: %d "%(runInfoName,runInfoLength)
                except IndexError:
                    break
                #####
            #####
        #####
        else:
            readLength = 151
        #####
        
        # Where do I run?
        if ( (testDirectory("/mnt/pci-ssd/tmp")) and (not options.UseData) ):
            checkSpace("/mnt/pci-ssd/tmp", projectDataPath, projectBaseName, start=True)
            workingDir = "/mnt/pci-ssd/tmp/%s"%projectBaseName
        
        elif ((testDirectory("/mnt/ssd/tmp")) and (not options.UseData) ):
            checkSpace("/mnt/ssd/tmp", projectDataPath, projectBaseName, start=True)
            workingDir = "/mnt/ssd/tmp/%s"%projectBaseName
        
        elif ((testDirectory("/mnt/data1/tmp/")) and ( not options.UseData)):
            checkSpace("/mnt/data1/tmp/", projectDataPath, projectBaseName, start=True)
            workingDir = "/mnt/data1/tmp/%s"%projectBaseName
        
        elif (options.UseData):
            checkSpace("/home/f3p2/illuminaProcessingTmp/", projectDataPath, projectBaseName, start=True)
            workingDir = "/home/f3p2/illuminaProcessingTmp/%s"%projectBaseName
        else:
            raise IOError( 'There is no place to run the project!!' )
        #####
        seqQual = options.noSeqQual
    #####
    
    # Overall Working Directory
    outputDir   = createTmpDirectory("%s"%(workingDir))
    # This is where the fastqs will reside with original CASAVA assigned names
    dataDir     = createTmpDirectory("%s/Data"%(workingDir))
    # This is where all the stats files will live
    statsDir    = createTmpDirectory("%s/Stats"%(workingDir))
    # This directory will house nothing but relative links back to the Data directory but will have the "English" fastq names. These links will be linked into the HTML
    humReadDir  = createTmpDirectory("%s/Human"%(workingDir))
    # This directory will house results for all future anaylsis requested by the end user
    analyDir    = createTmpDirectory("%s/Analysis"%(workingDir))
    
    chdir( outputDir )
    
    # Open the stats file and the HTML and add the header information
    laneStats_oh   = open('%s/%s.projectStats'%(statsDir, projectBaseName), 'w')
    updateHTML_oh  = open('%s/Tmp.html'%(statsDir), 'w')
    laneHTML_oh    = open('%s/Stats.html'%(statsDir), 'w')
    
    # Create a directroy on ILLPROC
    # Does a stats file already exist in the project
    oldConfig           = False
    runTotals           = {"totalReads":0,"totalBases":0,"YIELD_READS":0,"YIELD_BASES":0,"HQ_BASES":0,"PHIX_READS":0}
    statsBySample       = {}
    fileList            = currentConfigData.fileList
    DNA_descriptionDict = {}
    runingPCName        = socket.gethostname()
    
    if ( testDirectory( "%s/%s"%(finalHomeDir,projectBaseName) ) ):
        # Then we are rerunning a lane in a library
        oldConfig     = True
        oldStatsData  = {}
        humanReadList = set()
        for lane in fileList:
            subLanes = fileList[lane]
            for subLane in subLanes:
                samplesInLane = subLanes[subLane] 
                for samples in samplesInLane: 
                    addName = samplesInLane[samples][1]
                    if (subLane == "Undetermined" ): addName = "Lane%d-Undetermined"%(lane)
                    humanReadList.add(addName)
                #####
            #####
        #####
        oldStatsData = {1:{},2:{},3:{},4:{},5:{},6:{},7:{},8:{},9:{}}
        localLane    = 1
        localSubLane = 1
        for line in open("%s/%s/Stats/%s.projectStats"%(finalHomeDir, projectBaseName, projectBaseName)):
            splitLine = line.split(None)
            if ( len(splitLine) < 2 ): continue
            if ( ( splitLine[0] == IlluminaID ) or ( splitLine[0] == "LANE" ) )  : continue
            if ( ( splitLine[0].find("Total") >= 0 ) ):
                oldStatsData[localLane][splitLine[0]] = [line]
                localLane    += 1
                localSubLane  = 1
                continue
            #####
            
            humanName = splitLine[0].split("_R1")[0].split("_R2")[0]
            if ( humanName not in humanReadList ): 
                runTotals["totalReads"]  += decommify( splitLine[1] )
                runTotals["totalBases"]  += decommify( splitLine[2] )
                runTotals["YIELD_READS"] += decommify( splitLine[4] )
                runTotals["YIELD_BASES"] += decommify( splitLine[5] )
                runTotals["HQ_BASES"]    += decommify( splitLine[6] )
                runTotals["PHIX_READS"]  += decommify( splitLine[8] ) 
                oldStatsData[localLane][splitLine[0]] = [line, localSubLane]
            #####
            localSubLane += 1
        #####
        
        updateHTML_oh.write('<a name="%s"></a><table border="1" cellspacing="0" cellpadding="5" class="ta1"><tr class="ro1"><td colspan="12" class="Title">%s &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s(%s)  &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s-Rerunning MODULE 0 on %s</td></table>'%(IlluminaID,\
                                                                                                                                                                                                                                                                                                                                IlluminaID,\
                                                                                                                                                                                                                                                                                                                                projectBaseName,\
                                                                                                                                                                                                                                                                                                                                machineNameDict[projectBaseName.split("_")[1]],\
                                                                                                                                                                                                                                                                                                                                HeaderLine,\
                                                                                                                                                                                                                                                                                                                                runingPCName))
        updateHTML_oh.close()
        system("mv -f %s/%s/Stas/Stats.html %s/%s/Stas/OLD_Stats.html"%(finalHomeDir, finalHomeDir, projectBaseName, projectBaseName))
        system("mv %s/Tmp.html %s/%s/Stats/"%(statsDir, finalHomeDir, projectBaseName))
    else:
        # Create the directory
        system("mkdir %s/%s"%(finalHomeDir, projectBaseName))
        system("mkdir %s/%s/Stats"%(finalHomeDir, projectBaseName))
        
        try:
            machineNameDict[projectBaseName.split("_")[1]]
        except KeyError:
            stderr.write("\n!!!!!!!!!!\n!!!!!!!!!!\nTHIS MACHINE DOES NOT EXIST IN RECORDS\n\tPLEASE ADD IT AND RESTART\nEXITTING PROGRAM...\n!!!!!!!!!!\n!!!!!!!!!!\n")
            exit()
        #####
        updateHTML_oh.write('<a name="%s"></a><table border="1" cellspacing="0" cellpadding="5" class="ta1"><tr class="ro1"><td colspan="12" class="Title">%s &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s(%s)  &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s-Running MODULE 0 on %s</td></table>'%(IlluminaID,\
                                                                                                                                                                                                                                                                                                                              IlluminaID,\
                                                                                                                                                                                                                                                                                                                              projectBaseName,\
                                                                                                                                                                                                                                                                                                                              machineNameDict[projectBaseName.split("_")[1]],\
                                                                                                                                                                                                                                                                                                                              HeaderLine,\
                                                                                                                                                                                                                                                                                                                              runingPCName))
        updateHTML_oh.close()
        system("mv %s/Tmp.html %s/%s/Stats/"%(statsDir, finalHomeDir, projectBaseName))
        chdir( "%s/"%(finalHomeDir) )
        system("ln -s %s %s"%(projectBaseName, IlluminaID))
        
        chdir( "/home/raid2/ILLPROCDATA/" )
        
        if (options.TESTING):
            chdir( "%s/"%(finalHomeDir) )
        
        system("ln -s %s/%s %s"%(finalHomeDir, projectBaseName, IlluminaID))
        
        chdir( outputDir )
    #####
    subprocess.Popen("%s"%(websiteScript), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
    
    laneHTML_oh.write('<a name="%s"></a><table border="1" cellspacing="0" cellpadding="5" class="ta1"><tr class="ro1"><td colspan="14" class="Title">%s &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s(%s) &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s</td></tr>'%(IlluminaID,\
                                                                                                                                                                                                                                                                                      IlluminaID,\
                                                                                                                                                                                                                                                                                      projectBaseName,\
                                                                                                                                                                                                                                                                                      machineNameDict[projectBaseName.split("_")[1]],\
                                                                                                                                                                                                                                                                                      HeaderLine))
    if (not currentConfigData.isNUGEN):
        laneHTML_oh.write('''<tr class="ro1"><td class="Header co1">LANE</td><td class="Header co2">READS</td><td class="Header co3">BASES</td><td class="Header co4">AVG</td><td class="Header co5">YIELD READS</td>
                             <td class="Header co6">YIELD BASES</td><td class="Header co7">YIELD PHRED20 BASES</td><td class="Header co8">PHRED20 AVG</td><td class="Header co9">PHIX READS</td><td class="Header co10">% PHIX</td>
                             <td class="Header co11">PHRED20 PHIX AVG</td><td class="Header co12">RAW (PF) CLUSTER</td></tr>\n''')
        
        laneStats_oh.write("%s\n%s"%( HeaderLine, laneSF.format("LANE","READS","BASES","AVG_LEN","YIELD_READS","YIELD_BASES","PHRED20_BASES","PHRED20_AVG","PHIX_READS","%_PHIX","PHRED20_PHIX_AVG","RAW_CLUSTER","PF_CLUSTER_PERCENTAGE")))
    
    else:
        laneHTML_oh.write('''<tr class="ro1"><td class="Header co1">LANE</td><td class="Header co2">READS</td><td class="Header co3">BASES</td><td class="Header co4">AVG</td><td class="Header co5">YIELD READS</td>
                             <td class="Header co6">YIELD BASES</td><td class="Header co7">YIELD PHRED20 BASES</td><td class="Header co8">PHRED20 AVG</td><td class="Header co9">PHIX READS</td><td class="Header co10">% PHIX</td>
                             <td class="Header co11">PHRED20 PHIX AVG</td><td class="Header co12">ECOLI READS</td><td class="Header co13">% ECOLI</td><td class="Header co14">RAW (PF) CLUSTER</td></tr>\n''')
        
        laneStats_oh.write("%s\n%s"%( HeaderLine, laneSF_NUGEN.format("LANE","READS","BASES","AVG_LEN","YIELD_READS","YIELD_BASES","PHRED20_BASES","PHRED20_AVG","PHIX_READS","%_PHIX","PHRED20_PHIX_AVG","ECOLI_READS","%_ECOLI","RAW_CLUSTER","PF_CLUSTER_PERCENTAGE")))
    #####
        
    # BEGIN THE PROCESSING
        
    for lane in fileList:
        laneDict            = fileList[lane]
        statsBySample[lane] = {}
        laneTotals          = {"totalReads":0,"totalBases":0,"gHQBases":0,"genomeReads":0,"HQ_phix_R1":0,"reads_phix_R1":0,"HQ_phix_R2":0,"reads_phix_R2":0,"totalGBases":0,"ecoliRead":0}
        
        sepString   = None
        phredOffSet = None
        
        subLaneKeys = laneDict.keys()
        subLaneKeys.sort()
        DNA_descriptionDict[lane] = {}
        
        for subLane in subLaneKeys: 
            
            subLaneDict = laneDict[subLane] 
            sampleKeys  = subLaneDict.keys()
            sampleKeys.sort()
 
            statsBySample[lane][subLane]       = {}
            DNA_descriptionDict[lane][subLane] = subLaneDict[sampleKeys[0]][1]
            updateHTML_oh = open("%s/%s/Stats/Tmp.html"%( finalHomeDir, projectBaseName ), 'w')
            updateHTML_oh.write('<a name="%s"></a><table border="1" cellspacing="0" cellpadding="5" class="ta1" width="100%%"><tr class="ro1"><td colspan="12" class="Title">%s &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s(%s)  &nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;&nbsp;&nbsp; %s-CURRENTLY LANE%s-%s IN MODULE 0 on %s</td></table>'%(IlluminaID,\
                                                                                                                                                                                                                                                                                                                                                        IlluminaID,\
                                                                                                                                                                                                                                                                                                                                                        projectBaseName,\
                                                                                                                                                                                                                                                                                                                                                        machineNameDict[projectBaseName.split("_")[1]],\
                                                                                                                                                                                                                                                                                                                                                        HeaderLine,\
                                                                                                                                                                                                                                                                                                                                                        lane,
                                                                                                                                                                                                                                                                                                                                                        subLane,\
                                                                                                                                                                                                                                                                                                                                                        runingPCName))
            updateHTML_oh.close()
            subprocess.Popen("%s"%(websiteScript), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            sleep(20)
            for sample in sampleKeys:
                #=====================================================================================================================
                # Start of Module 0
                # Set up the output paths and files
                sampleFiles = ','.join(subLaneDict[sample][0])
                if (sampleFiles.find("NUGEN96")>-1):
                    curTmpDir  = createTmpDirectory("%s/Lane%s_%s_%s"%(dataDir, lane, subLane, sample))
                    curStsDir  = createTmpDirectory("%s/Lane%s_%s_%s"%(statsDir, lane, subLane, sample))
                    outputBase = "%s/L00%s_%s_%s"%(curTmpDir, lane, subLane, sample)
                    stderr.write("%s"%feedBack.format("Lane%s_%s_%s"%(lane, subLane, sample), " "))
                    stderr.write("%s"%feedBack.format("\tRUNNING MODULE 0", time.strftime("%c")))
                else:
                    curTmpDir  = createTmpDirectory("%s/Lane%s_%s"%(dataDir, lane, subLane))
                    curStsDir  = createTmpDirectory("%s/Lane%s_%s"%(statsDir, lane, subLane))
                    outputBase = "%s/L00%s_%s"%(curTmpDir, lane, subLane)
                    stderr.write("%s"%feedBack.format("Lane%s_%s"%(lane, subLane), " "))
                    stderr.write("%s"%feedBack.format("\tRUNNING MODULE 0", time.strftime("%c")))
                #####
            
                # Generate the module0 command and execute
                realCMD  = "/home/raid2/SEQ/sharedPythonLibrary/fastqQC_Module_0.py '%s' %s -l %s"%(sampleFiles, outputBase, readLength)
                
                if ((not seqQual) or (str(lane) in ecoliExcludeSet)): realCMD = "%s -s"%(realCMD)
                
                if ( (sepString != None ) and ( phredOffSet != None ) ): realCMD = "%s -o %s -S %s"%(realCMD, phredOffSet, sepString)
                
                realProc = subprocess.Popen(realCMD, shell=True, stdout=subprocess.PIPE , stderr=errorLog)
                RValue   = 1

                for sampleData in realProc.stdout: 
                    sampData    = sampleData.split(None)
                    sepString   = sampData.pop(-1)
                    phredOffSet = sampData.pop(-1)
                    statsBySample[lane][subLane]["%s_R%s"%(sample, RValue)] = sampData
                    statsBySample[lane][subLane]["%s_R%s"%(sample, RValue)].append(0)
                    RValue += 1
                #####
                
                # Generate local Stats and move files.
                if (RValue == 2):
                    system('/home/raid2/SEQ/sharedPythonLibrary/fastqStatPlotter.py %s_R1.tmp.stats %s_R1.phredHist,%s_R1_phix.phredHist %s'%(outputBase, outputBase, outputBase, outputBase))
                else:
                    system('/home/raid2/SEQ/sharedPythonLibrary/fastqStatPlotter.py %s_R1.tmp.stats,%s_R2.tmp.stats %s_R1.phredHist,%s_R1_phix.phredHist,%s_R2.phredHist,%s_R2_phix.phredHist %s'%(outputBase, outputBase,\
                                                                                                                                                                                              outputBase, outputBase,\
                                                                                                                                                                                              outputBase, outputBase,\
                                                                                                                                                                                              outputBase))
                #####
                
                system('mv %s*.stats %s'%(outputBase, curStsDir))
                system('mv %s*.phredHist %s'%(outputBase, curStsDir))
                system('mv %s*.png %s'%(outputBase, curStsDir))
                system('rm -f %s_R1.fastq.bz2 %s_R2.fastq.bz2'%(outputBase, outputBase))
                if (subLane != "Undetermined"):
                    
                    if (sampleFiles.find("NUGEN96") == -1):
                        system('ln -s -r %s/*R1_genome.fastq.bz2 ./Human/%s_I%s_L%s_R1.fastq.bz2'%(curTmpDir, subLaneDict[sample][1], IlluminaID.split("-")[1], lane))
                        if (RValue > 2):
                            system('ln -s -r %s/*R2_genome.fastq.bz2 ./Human/%s_I%s_L%s_R2.fastq.bz2'%(curTmpDir, subLaneDict[sample][1], IlluminaID.split("-")[1], lane))
                        #####
                    else:
                        system('ln -s -r %s/*R1_genome.fastq.bz2 ./Human/%s_I%s_L%s_R1.fastq.bz2'%(curTmpDir, subLaneDict[sample][1], IlluminaID.split("-")[1], "%s-%s-%s"%(lane,subLane,sample)))
                        if (RValue > 2):
                            system('ln -s -r %s/*R2_genome.fastq.bz2 ./Human/%s_I%s_L%s_R2.fastq.bz2'%(curTmpDir, subLaneDict[sample][1], IlluminaID.split("-")[1], "%s-%s-%s"%(lane,subLane,sample)))
                        #####
                    #####
                else:
                    system('ln -s -r %s/*R1_genome.fastq.bz2 ./Human/Lane_%s_Undetermined_I%s_L%s_R1.fastq.bz2'%(curTmpDir, lane,  IlluminaID.split("-")[1], lane))
                    if (RValue > 2):
                        system('ln -s -r %s/*R2_genome.fastq.bz2 ./Human/Lane_%s_Undetermined_I%s_L%s_R2.fastq.bz2'%(curTmpDir, lane, IlluminaID.split("-")[1], lane))
                    #####
                #####

                if ( ( str(lane) not in ecoliExcludeSet ) and ( currentConfigData.isNUGEN ) ):
                    stderr.write("\tI AM SCREENING ECOLI\n")
                    system('mv -f %s_R1_genome.fastq.bz2 %s_R1.unScreened.fastq.bz2'%(outputBase, outputBase))
                    ecoliScreeList = ['%s_R1.unScreened.fastq.bz2'%(outputBase)]
                    if (RValue > 2):
                        system('mv -f %s_R2_genome.fastq.bz2 %s_R2.unScreened.fastq.bz2'%(outputBase, outputBase))
                        ecoliScreeList.append('%s_R2.unScreened.fastq.bz2'%(outputBase))
                    #####
                    ecoliScreeList = ','.join(ecoliScreeList)
                    realCMD  = "/home/raid2/SEQ/sharedPythonLibrary/fastqQC_Module_0.py '%s' %s -U -l %s"%(ecoliScreeList, outputBase, readLength)
                    if (not seqQual): realCMD = "%s -s"%(realCMD)
                    if ( (sepString != None ) and ( phredOffSet != None ) ): realCMD = "%s -o %s -S %s"%(realCMD, phredOffSet, sepString)                    
                    realProc = subprocess.Popen(realCMD, shell=True, stdout=subprocess.PIPE, stderr=errorLog)
                    RValue   = 1
                    for sampleData in realProc.stdout: 
                        sampData    = sampleData.split(None)
                        sepString   = sampData.pop(-1)
                        phredOffSet = sampData.pop(-1)
                        statsBySample[lane][subLane]["%s_R%s"%(sample, RValue)][4] = sampData[4]
                        statsBySample[lane][subLane]["%s_R%s"%(sample, RValue)][5] = sampData[5]
                        statsBySample[lane][subLane]["%s_R%s"%(sample, RValue)][8] = sampData[8]
                        statsBySample[lane][subLane]["%s_R%s"%(sample, RValue)][9] = sampData[7]
                        RValue += 1
                    #####
                    system('rm -f %s*.stats'%(outputBase))
                    system('rm -f %s*.phredHist'%(outputBase))
                    system('rm -f %s*.png'%(outputBase))
                    system('rm -f %s_R1.fastq.bz2 %s_R2.fastq.bz2 %s*unScreened.fastq.bz2'%(outputBase, outputBase, outputBase))
                #####
            #####
            # End of Module 0
            #=====================================================================================================================

            
        
            # Create a unifide stats line
        
            if(oldConfig):
                subLaneStats_oh = open('%s/Lane%s-%s_samples_new.stats'%(statsDir, lane, subLane), 'w')
                subLaneStats_oh.write('%s'%(sampleSF.format("SAMPLE","READS","BASES","AVG_LEN","PHRED20_AVG")))
                writeStats( IlluminaID, projectBaseName, statsBySample[lane][subLane], lane, subLane, clusterMD, clusterSD, subLaneStats_oh, laneStats_oh, laneHTML_oh, laneTotals, DNA_descriptionDict, currentConfigData, ecoliExcludeSet, oldLaneDict=oldLaneStats)
                subLaneStats_oh.close()
            else:
                subLaneStats_oh = open('%s/Lane%s-%s_samples.stats'%(statsDir, lane, subLane), 'w')
                subLaneStats_oh.write('%s'%(sampleSF.format("SAMPLE","READS","BASES","AVG_LEN","PHRED20_AVG")))
                writeStats( IlluminaID, projectBaseName, statsBySample[lane][subLane], lane, subLane, clusterMD, clusterSD, subLaneStats_oh, laneStats_oh, laneHTML_oh, laneTotals, DNA_descriptionDict, currentConfigData, ecoliExcludeSet)
                subLaneStats_oh.close()
            #####
            statsFileListCMD  = 'ls %s/Lane%s_%s*/*.tmp.stats'%(statsDir, lane, subLane)
            statsFileListProc = subprocess.Popen(statsFileListCMD , shell=True, stdout=subprocess.PIPE)
            statsFileList     = open('%s/Lane%s_%s.statsList.dat'%(statsDir, lane, subLane),'w')
            statsFileList.write('\n'.join([item.strip() for item in statsFileListProc.stdout]))
            statsFileList.close()
            
            phredFileListCMD  = 'ls %s/Lane%s_%s*/*.phredHist'%(statsDir, lane, subLane)
            phredFileListProc = subprocess.Popen(phredFileListCMD , shell=True, stdout=subprocess.PIPE)
            phredFileList     = open('%s/Lane%s_%s.phredHistList.dat'%(statsDir, lane, subLane),'w')
            phredFileList.write('\n'.join([item.strip() for item in phredFileListProc.stdout]))
            phredFileList.close()
            
            system('/home/raid2/SEQ/sharedPythonLibrary/fastqStatPlotter.py %s/Lane%s_%s.statsList.dat %s/Lane%s_%s.phredHistList.dat %s/L00%s_%s -f'%(statsDir, lane, subLane, statsDir, lane, subLane , statsDir, lane, subLane)) 
            system('rm -f %s/Lane%s_%s.statsList.dat %s/Lane%s_%s.phredHistList.dat'%(statsDir, lane, subLane, statsDir, lane, subLane))            
        #####
    
        statsFileListCMD  = 'ls %s/Lane%s*/*.tmp.stats'%(statsDir, lane)
        statsFileListProc = subprocess.Popen(statsFileListCMD , shell=True, stdout=subprocess.PIPE)
        statsFileList     = statsFileListProc.communicate()[0].strip().split(None)
        printLanePNG(statsFileList, readLength, statsDir, lane)
        writeLaneTotals( IlluminaID, laneTotals, laneStats_oh, lane, clusterMD, clusterSD, runTotals, DNA_descriptionDict, laneHTML_oh, currentConfigData, realPhixPercent)
    #####
    laneStats_oh.write("\n%s"%(laneSF.format("Total",commify(runTotals["totalReads"]), commify(runTotals["totalBases"]), "%.1f"%(float(runTotals["totalBases"])/float(runTotals["totalReads"])),\
                                             commify(runTotals["YIELD_READS"]), commify(runTotals["YIELD_BASES"]), commify(runTotals["HQ_BASES"]),\
                                             "%.1f"%(float(runTotals["HQ_BASES"])/float(runTotals["YIELD_READS"])), commify(runTotals["PHIX_READS"]),\
                                             "%.1f"%(float(runTotals["PHIX_READS"])*100.0/float(runTotals["totalReads"])),"-","-","-")))
    
    
    if (not currentConfigData.isNUGEN):
        if (realPhixPercent == None):
            laneHTML_oh.write(runTotHTML%(commify(runTotals["totalReads"]), commify(runTotals["totalBases"]), "%.1f"%(float(runTotals["totalBases"])/float(runTotals["totalReads"])),\
                                          commify(runTotals["YIELD_READS"]), commify(runTotals["YIELD_BASES"]), commify(runTotals["HQ_BASES"]),\
                                          "%.1f"%(float(runTotals["HQ_BASES"])/float(runTotals["YIELD_READS"])), commify(runTotals["PHIX_READS"]),\
                                          "%.1f"%(float(runTotals["PHIX_READS"])*100.0/float(runTotals["totalReads"]))))
        else:
            laneHTML_oh.write(runTotHTML%(commify(runTotals["totalReads"]), commify(runTotals["totalBases"]), "%.1f"%(float(runTotals["totalBases"])/float(runTotals["totalReads"])),\
                                          commify(runTotals["YIELD_READS"]), commify(runTotals["YIELD_BASES"]), commify(runTotals["HQ_BASES"]),\
                                          "%.1f"%(float(runTotals["HQ_BASES"])/float(runTotals["YIELD_READS"])), commify(runTotals["PHIX_READS"]),realPhixPercent))
        #####
    else:
        laneHTML_oh.write(runTot_NUGHTML%(commify(runTotals["totalReads"]), commify(runTotals["totalBases"]), "%.1f"%(float(runTotals["totalBases"])/float(runTotals["totalReads"])),\
                                          commify(runTotals["YIELD_READS"]), commify(runTotals["YIELD_BASES"]), commify(runTotals["HQ_BASES"]),\
                                          "%.1f"%(float(runTotals["HQ_BASES"])/float(runTotals["YIELD_READS"])), commify(runTotals["PHIX_READS"]),\
                                          "%.1f"%(float(runTotals["PHIX_READS"])*100.0/float(runTotals["totalReads"]))))
    #####
    
    laneStats_oh.close()
    laneHTML_oh.close()
    errorLog.close()
    
    #system("rm -f ErrorLog.%s.dat"%(args[0]))

    # Is there enough room to move the data to ILLPROCDATA    
    checkSpace("%s/"%(finalHomeDir), ".", projectBaseName, start=False)

    # Move the directory
    system("cp %s %s/%s"%(configFile, finalHomeDir, projectBaseName))
    system("rm -f %s/%s/Stats/Tmp.html"%(finalHomeDir, projectBaseName))
    if(oldConfig):
        finalPath = "%s/%s"%(finalHomeDir, projectBaseName)
        stderr.write("MOVING DATA TO:     %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvData = system('mv -f %s/* %s/Data'%(dataDir, finalPath))
        stderr.write("MOVING STATS TO:    %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvStat = system('mv -f %s/* %s/Stats'%(statsDir, finalPath))
        stderr.write("MOVING HUMAN TO:    %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvHuman = system('mv -f %s/* %s/Human'%(humReadDir, finalPath))
        stderr.write("MOVING ANALYSIS TO: %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvAna = system('mv -f %s/* %s/Analysis'%(analyDir, finalPath))
        
        if (mvData + mvStat + mvHuman + mvAna != 0):
            stderr.write("\n!!!!!!!!!!!!!!!!!!!!\n\nENCOUNTERED PROBLEM TRANSFERING FILE\nYOU WILL NEED TO TO IT MANUALLY\n\n!!!!!!!!!!!!!!!!!!!!\n")
        else:
            stderr.write("Deleting Temporary Project Dir\n")
            chdir("../")
            system('rm -r %s'%(outputDir))
        #####
    else:
        finalPath = "%s/%s"%(finalHomeDir, projectBaseName)
        stderr.write("MOVING DATA TO:     %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvData = system('mv -f %s %s'%(dataDir, finalPath))
        stderr.write("MOVING STATS TO:    %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvStat = system('mv -f %s/* %s/Stats/'%(statsDir, finalPath))
        stderr.write("MOVING HUMAN TO:    %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvHuman = system('mv -f %s %s'%(humReadDir, finalPath))
        stderr.write("MOVING ANALYSIS TO: %s -- %s\n"%(finalPath,time.strftime("%c")))
        mvAna = system('mv -f %s %s'%(analyDir, finalPath))
        
        if (mvData + mvStat + mvHuman + mvAna != 0):
            stderr.write("\n!!!!!!!!!!!!!!!!!!!!\n\nENCOUNTERED PROBLEM TRANSFERING FILE\nYOU WILL NEED TO TO IT MANUALLY\n\n!!!!!!!!!!!!!!!!!!!!\n")
        else:
            stderr.write("Deleting Temporary Project Dir\n")
            chdir("../")
            system('rm -r %s'%(outputDir))
        #####
    #####
    subprocess.Popen("%s"%(websiteScript), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if (options.TESTING ) : exit()
    myEmailCommand = subprocess.Popen('mail -s "%s (%s) POSTED"  productionsequencing@hagsc.org'%(IlluminaID, projectBaseName), shell=True, stdin=subprocess.PIPE)
    myOut = myEmailCommand.communicate(input = "%s has finished and posted\n\nhttp://www.hagsc.org/restricted_access/shgcinternal/ILLstatspage.html#%s"%(IlluminaID,IlluminaID))[0]  
#=========================================================================    
if ( __name__ == '__main__' ):
    real_main()