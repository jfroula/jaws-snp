#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  9/16/13"

from hagsc_lib import iterFASTA, iterCounter, revComp

from os.path import realpath, isfile

from os import system

from sys import stdout, stderr

import subprocess

from optparse import OptionParser

import re

#==============================================================
class clusterClass(object):
    def __init__(self,initialPoint):
        self.points  = [initialPoint]
        self.sum     = initialPoint
        self.nPoints = 1.0
        self.mean    = initialPoint

    def addPoint(self,point):
        self.points.append( point )
        self.sum += point
        self.nPoints += 1.0
        self.mean = self.sum / self.nPoints

    def __repr__(self):
        return self.points.__repr__()

    def __str__(self):
        return self.__repr__()

#==============================================================
def simpleCluster(data):
    # Copying and sorting the data
    tmpData = [item for item in data]
    tmpData.sort()
    nStop = len(data) - 1
    # Selecting the first two points in the cluster
    clusters = 2 * [None]
    clusters[0] = clusterClass( tmpData[-1] )
    clusters[1] = clusterClass( tmpData[0]  )
    # Setting up the index list of remaining points
    nCounter = 1
    while ( True ):
        # Pick next point
        tmpPoint = tmpData[nCounter]
        # Who is it closest too?
        d_0 = abs( tmpPoint - clusters[0].mean )
        d_1 = abs( tmpPoint - clusters[1].mean )
        if ( d_0 <= d_1 ):
            clusters[0].addPoint(tmpPoint)
        else:
            clusters[1].addPoint(tmpPoint)
        #####
        # Continue?
        nCounter += 1
        if ( nCounter == nStop ): break
    #####
    return [clusters[0], clusters[1]]

#==============================================================
class simpleSequenceScore(object):

    def __init__(self):

        # Initializing the triple counter
        bases = ['A','C','G','T']
        self.tripletCounts = {}
        for L_1 in bases:
            for L_2 in bases:
                for L_3 in bases:
                    trpl = ''.join([L_1,L_2,L_3])
                    self.tripletCounts[trpl] = 0
                #####
            #####
        #####

    def computeSimpleSequenceScore(self, seq):
        # Initializing the scores
        for trpl in self.tripletCounts.iterkeys(): self.tripletCounts[trpl] = 0

        # Computing the size
        nSize = len(seq)

        # Accurately tabulating the triples
        seqList = [item for item in seq.split('N') \
                                       if ( (item != '') and (len(item) > 3) ) ]
        for tmpSeq in seqList:
            # Counting the triples
            for n in xrange( nSize - 1 ):
                try:
                    self.tripletCounts[tmpSeq[n:n+3]] += 1
                except KeyError:
                    continue
                #####
            #####
        #####
        
        # Computing the number of bases
        nA     = seq.count('A')
        nC     = seq.count('C')
        nG     = seq.count('G')
        nT     = seq.count('T')
        nBases = float( nA + nC + nG + nT )
        L      = nBases - 2.0

        # Computing the score
        sigma = 0
        k     = 0.0
        for t_count in self.tripletCounts.itervalues():
            sigma += t_count * ( t_count - 1 )
            if ( t_count > 1 ): k += 1.0
        #####

        # Correction for sequencing errors
        if ( k > 2.0 ):
            # Cluster the 3mer counts to find errant 3mers arising due to
            # sequencing error
            clusters = simpleCluster( \
                    [item for item in self.tripletCounts.values() if item > 1] )
            diff = clusters[0].mean - clusters[1].mean
            # If the cluster means are more than 10 counts from one another,
            # then recount
            if ( diff > 10 ):
                k = float( len(clusters[0].points) )
                # Recompute sigma
                sigma = 0
                for t_count in clusters[0].points:
                    sigma += t_count * ( t_count - 1 )
                #####
            else:
                # For regular sequence reset the k-value if it is larger than 4
                if ( k > 4.0 ): k = 4.0
            #####
        #####

        # Compute score
        score = k * float( sigma ) / L / ( L - k )
        
        # Normalized for repetitive simple sequence
        return score

#==============================================================
class read_parse_class( object ):
    def __init__(self):
        self.read_sanger   = re.compile( r'(\D+)(\d+\-.*?)\..(\d+).\-(\D+)' ).findall
        self.read_illumina = re.compile( r'([A-Za-z0-9_:-]+)-R([12]{1})$' ).findall
    def parse(self,readName):
        # Is it a sanger paired read?
        try:
            lib, readID, ext1, ext2 = self.read_sanger(readName)[0]
            if ( lib ):  return {'type':'sanger', 'lib':lib, 'readID':readID, 'ext1':ext1, 'ext2':ext2}
        except IndexError:
            pass
        except ValueError:
            pass
        #####
        # Is it an illumina paired read?
        try:
            readID, ext = self.read_illumina(readName)[0]
            if ( readID ):  return {'type':'illumina', 'baseName':readID, 'ext':ext}
        except IndexError:
            pass
        except ValueError:
            pass
        #####
        return False

#==============================================================
def real_main():

    # Defining the program options
    usage = "usage: %prog [fasta file]"

    parser = OptionParser(usage)
    
    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 1 ):
        parser.error( "Incorrect number of arguments.  View usage using --help option." )
    else:
        # Pulling the fileNames
        fastaFile = realpath(args[0])
        if ( not isfile(fastaFile) ): parser.error( '%s can not be found'%fastaFile )
    #####

    # Defining functions    
    score_simple_sequence = simpleSequenceScore().computeSimpleSequenceScore

    # Defining regular expressions
    parseRead = read_parse_class().parse
    
    # Clipping Call
    clipCall  = '/mnt/local/EXBIN/clip -v -B 10000 -c 50 -f 30 -L 500 %s'%fastaFile
    mer18Hist = '/mnt/local/EXBIN/histogram_hashn -B 10000 -z 100m -m 18 -w 1 -'
    mer36Hist = '/mnt/local/EXBIN/histogram_hashn -B 10000 -z 100m -m 36 -w 1 -'

    # (1)  Pulling in the 18mers
    cmd = '%s | %s'%(clipCall,mer18Hist)
    tmpProcess = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    mer18_dict = {}
    stderr.write( "************************\nReading 18mers\n************************\n" )
    for line in tmpProcess.stdout:
        splitLine = line.split(None)
        mer18_dict[splitLine[0]] = int(splitLine[1])
    #####
    tmpProcess.poll()
    
    # (2)  Pull all 36mers that look like a chimera and screen
    cmd = '%s | %s'%(clipCall,mer36Hist)
    tmpProcess = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    mer36_dict = {}
    stderr.write( "************************\nReading 36mers\n************************\n" )
    mer36Freq_dict = {}
    cutoffScore    = 0.65 * 0.65
    for line in tmpProcess.stdout:
        splitLine = line.split(None)
        mer36   = splitLine[0]
        freq36  = int(splitLine[1])
        mer36Freq_dict[mer36] = freq36
        if ( freq36 <= 2 ):
            # Screening simple sequence
            if ( score_simple_sequence( mer36 ) > cutoffScore ): continue
            # Reading frequencies
            first18      = splitLine[0][:18]
            first18_freq = mer18_dict[first18]
            last18       = splitLine[0][18:]
            last18_freq  = mer18_dict[last18]
            perDiff = float(min(first18_freq,last18_freq))/float(max(first18_freq,last18_freq))
            # Screening based in freqeuncy
            if ( (first18_freq > 1) and (last18_freq > 1) and (perDiff > 0.30) ):
                mer36_dict[mer36] = [mer36,first18,first18_freq,last18,last18_freq]
            #####
        #####
    #####
    
    # (3)  Finding the chimeric 36mers
    stderr.write( '************************\nBe vewy vewy quite...I am hunting chimeras...\n************************\n' )
    tmpProcess   = subprocess.Popen( clipCall, shell=True, stdout=subprocess.PIPE )
    full36merSet = set( mer36_dict.keys() )
    illuminaSet  = set()
    for record in iterFASTA(tmpProcess.stdout):

        # Pulling the sequence
        tmpSeq = str(record.seq).upper()
        tmpLen = len(tmpSeq)
        
        # Computing the overlap set
        kmerList    = [tmpSeq[n:(n+36)] for n in xrange(len(tmpSeq)-36)]
        tmp36merSet = set( kmerList )
        overlapSet  = full36merSet.intersection(tmp36merSet)
        
        # Looking for 36mers with a frequency of 1 off the end of the read
        isTrailing   = False
        freqSet      = set()
        highQuality  = True

        # Looking at the overlap set
        isChimera = False
        if ( len(overlapSet) >= 1 ): isChimera = True
        
        # Making decisions
        if ( isChimera or isTrailing ):
            
            if ( isChimera ):
                # Setting the positions of the 36mers
                overlapList = list(overlapSet)
                posList = [(tmpSeq.find(mer),mer) for mer in overlapSet]
                posList.sort()
            #####

            # Printing out the results
            tmpDict = parseRead(record.id)
            if ( tmpDict['type'] == 'illumina' ):
                illuminaSet.add( tmpDict['baseName'] )
            #####
            
#             print "============================================================="
#             print record.id
#             print tmpSeq
#             if ( isChimera ):
#                 print '%s%s%s'%( posList[0][0]*' ', (posList[-1][0]-posList[0][0]+36)*'-', (tmpLen-(posList[-1][0]-posList[0][0]+36))*' ')
#                 for pos, mer in posList:
#                     first18      = mer[:18]
#                     last18       = mer[18:]
#                     first18_freq = mer18_dict[first18]
#                     last18_freq  = mer18_dict[last18]
#                     print "36mer:          %s\tSTART:%d\t36MERfreq:%d\tFIRST18freq:%d\tLAST18freq:%d"%(mer,pos,mer36Freq_dict[mer],first18_freq,last18_freq)
#                 #####
#             else:
#                 print 'TRAILING CHIMERA'
#                 s = 1
#                 for mer in [tmpSeq[n:(n+36)] for n in xrange(len(tmpSeq)-36)]:
#                     first18      = mer[:18]
#                     last18       = mer[18:]
#                     first18_freq = mer18_dict[first18]
#                     last18_freq  = mer18_dict[last18]
#                     print s, mer, mer36Freq_dict[mer], first18_freq, last18_freq
#                     s += 1
#                     #####
#                 #####
#             #####
            
        #####
    #####
    tmpProcess.poll()
    
    # Writing out the read names
    if ( len(illuminaSet) > 0 ):
        for baseName in illuminaSet:
            stdout.write( '%s-R1\n'%baseName )
            stdout.write( '%s-R2\n'%baseName )
        #####
    #####

#==============================================================
if ( __name__ == '__main__' ):
    real_main()
