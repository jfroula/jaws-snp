#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="$${date} ${time}$"

from hagsc_lib import iterCounter, isGzipFile, isBzipFile, testFile
from hagsc_lib import iterFASTA, writeFASTA, iterQUAL, writeQUAL
from time import time
import subprocess

from os.path import splitext, realpath

from optparse import OptionParser

#==============================================================
def makeOutputString( baseName, entries ):
    if ( len(entries) < 2 ):
        print "entries less than 2"
        assert False
    #####
    # Sorting the entries
    entries.sort()
    # Generating the output string
    return ''.join([ '>%s%s%s\n%s\n'%(baseName,item[2], item[0],item[1]) for item in entries])
    
#==============================================================
def pullBaseName( tmpLine ):
    if ( tmpLine.count('.') > 0 ):
        x = tmpLine.split(None)[0].split('.')
        x.append( '.' )
        return x
    elif ( tmpLine.count('-R') > 0 ):
        x = tmpLine.split(None)[0].split('-R')
        x.append( '-R' )
        return x
    #####

#==============================================================
def real_main():

    # Defining the program options
    usage = 'usage: %prog [FASTA File] [QUAL File] [output FASTA] [options]'

    parser = OptionParser(usage)

    # Parsing the arguments
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( 'Incorrect number of arguments.  ' + \
                      'View usage using --help option.' )
    else:
        # Pulling the FASTA
        inputFASTAFile = realpath( args[0] )
        testFile( inputFASTAFile )

        # Pulling the QUAL
        inputQUALFile  = realpath( args[1] )
        testFile( inputQUALFile )

        # Pulling the QUAL
        outputFASTA  = realpath( args[2] )
    #####

    # Speed
    start = time()

    # Setting up output file names
    fo_FileName     = '%s'%outputFASTA
    fasta_outHandle = open( fo_FileName, 'w' )

    qo_FileName  = '%s.qual'%outputFASTA
    qual_outHandle  = open( qo_FileName, 'w' )

    # Setting up the subprocesses
    if ( isGzipFile(inputFASTAFile) ):
        fastaSub = 'cat %s | gunzip -c'%( inputFASTAFile )
    elif ( isBzipFile(inputFASTAFile) ):
        fastaSub = 'cat %s | bunzip2 -c'%( inputFASTAFile )
    else:
        fastaSub = 'cat %s'%( inputFASTAFile )
    #####

    if ( isGzipFile(inputQUALFile) ):
        qualSub  = 'cat %s | gunzip -c'%( inputQUALFile )
    elif ( isBzipFile(inputQUALFile) ):
        qualSub  = 'cat %s | bunzip2 -c'%( inputQUALFile )
    else:
        qualSub  = 'cat %s'%( inputQUALFile )
    #####
    
    # Starting FASTA process
    fastaProc = iterFASTA( subprocess.Popen( fastaSub, shell=True, stdout=subprocess.PIPE).stdout )
    fastaDict = {}
    fastaSet  = set()
    f_record  = next( fastaProc )
    
    # Starting QUAL process
    qualProc  = iterQUAL( subprocess.Popen( qualSub, shell=True, stdout=subprocess.PIPE).stdout )
    qualDict = {}
    qualSet  = set()
    q_record = next( qualProc )

    # Main loop
    x         = iterCounter(1000000)
    stopFASTA = False
    stopQUAL  = False
    while True:
    
        # Pull FASTA
        if ( not stopFASTA ): 
            fbaseName, ext, sep = pullBaseName( f_record.id )
            # Filling the data structures
            if ( fbaseName in fastaSet ):
                fastaDict[fbaseName].append( ( ext, str(f_record.seq), sep ) )
            else:
                fastaSet.add( fbaseName )
                fastaDict[fbaseName] = [ ( ext, str(f_record.seq), sep ) ]
            #####
            # Checking to see if it is time to write the entries
            if ( len(fastaDict[fbaseName]) == 2 and len(qualDict[fbaseName]) == 2 ):
                # Now we have to do something
                fasta_outHandle.write( makeOutputString( fbaseName, fastaDict[fbaseName]) )
                qual_outHandle.write(  makeOutputString( fbaseName, qualDict[fbaseName]) )
                # Handling the removal
                fastaSet.remove( fbaseName)
                fastaDict.pop(   fbaseName )
                qualSet.remove(  fbaseName)
                qualDict.pop(    fbaseName )
                x()
            #####
            try:
                f_record  = next( fastaProc )
            except StopIteration:
                stopFASTA = True
            #####
        #####

        # Pull QUAL
        if ( not stopQUAL ): 
            qbaseName, ext, sep = pullBaseName( q_record.id )
            # Filling the data structures
            if ( qbaseName in qualSet ):
                qualDict[qbaseName].append( ( ext, ' '.join(['%d'%item for item in q_record.qual]), sep ) )
            else:
                qualSet.add( qbaseName )
                qualDict[qbaseName] = [ ( ext, ' '.join(['%d'%item for item in q_record.qual]), sep ) ]
            #####
            # Checking to see if it is time to write the entries
            if ( len(fastaDict[qbaseName]) == 2 and len(qualDict[qbaseName]) == 2 ):
                # Now we have to do something
                fasta_outHandle.write( makeOutputString(qbaseName, fastaDict[qbaseName]) )
                qual_outHandle.write(  makeOutputString( qbaseName, qualDict[qbaseName]) )
                # Handling the removal
                fastaSet.remove( qbaseName)
                fastaDict.pop(   qbaseName )
                qualSet.remove(  qbaseName)
                qualDict.pop(    qbaseName )
                x()
            #####
            try:
                q_record  = next( qualProc )
            except StopIteration:
                stopQUAL = True
            #####
        #####
        
        # Termination condition
        if ( stopFASTA and stopQUAL ): break
    
    #####
    
    fasta_outHandle.close()
    qual_outHandle.close()

    print "Elapsed Time:"    
    print time() - start

    return
#==============================================================
if ( __name__ == '__main__' ):
    real_main()
    
