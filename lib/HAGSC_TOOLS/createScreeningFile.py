#!/usr/common/software/python/2.7-anaconda-4.4/bin/python
__author__="jjenkins"
__date__ ="12-April-2011"

# Local Library Imports
from hagsc_lib import iterFASTA

# Python Library Imports
from optparse import OptionParser

from os.path import isfile

from sys import stdout
from sys import stderr

def real_main():

    # Defining the program options
    usage = "usage: %prog [FASTA file] [Scaff list file] [label]"
    parser = OptionParser(usage)
    (options, args) = parser.parse_args()

    # Checking the user input
    if ( len(args) < 3 ):
        parser.error( "Incorrect number of arguments.  " + \
                      "View usage using --help option." )
    else:
    
        # Pulling the fileNames
        fastaFile = args[0]
        if ( not isfile(fastaFile) ): parser.error( '%s can not be found'%fastaFile )
        
        scaffListFile = args[1]
        if ( not isfile(scaffListFile) ): parser.error( '%s can not be found'%scaffListFile )
        
        scaffLabel = args[2]
        
    #####
    
    # Reading in the scaffold set
    scaffSet = set([line.strip() for line in open(scaffListFile)])
    for record in iterFASTA( open( fastaFile, 'r' ) ):
        if ( record.id in scaffSet ):
            stdout.write( '%s\t%s\n'%(record.id,scaffLabel))
        else:
            stdout.write( '%s\t\n'%(record.id))
        #####
    #####
    return
    
#####

if ( __name__ == '__main__' ):
    real_main()


