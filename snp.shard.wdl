workflow HA_snp_wf {
    File reads
    File reference
    String assemindex
    String caller
    String runid
    String lib_id
    Int maskfilter
    Int gapfilter
    String email
    String aligner
    Int num_pairs
    Int num_jobs
    Boolean align_only
    String align_memory
    Boolean combined_fastq
    Float total_bases
    String snp_env
    Int chunk_size=100000000

    # call write_snps_config {
    #     input: reads=reads,
    #            reference=reference,
    #            assemindex=assemindex,
    #            caller=caller,
    #            runid=runid,
    #            lib_id=lib_id,
    #            maskfilter=maskfilter,
    #            gapfilter=gapfilter,
    #            email=email,
    #            aligner=aligner,
    #            num_pairs=num_pairs,
    #            num_jobs=num_jobs,
    #            align_only=align_only,
    #            align_memory=align_memory,
    #            combined_fastq=combined_fastq,
    #            total_bases=total_bases
			   
    # }
    call shard {
        input: reads = reads,
	           chunk_size = chunk_size
    }

   call bbmap_indexing {
       input: reference = reference
   }
   Array[String] shard_array = shard.shards
   scatter(coords in shard_array) {
       call alignment {
       input: ref = bbmap_indexing.ref,
              fixed_fastq = shard.fixed_fastq,
              coords = coords
       }
   }
    call merge_bams {
         input: bams = alignment.bam
    }

    call create_reference_dictionaries {
       input: reference = reference,
              num_jobs = num_jobs,
  		      snp_env=snp_env
    }

#     call gatkqc {
#       input: config=write_snps_config.config,
#              primaryScaff=create_reference_dictionaries.primaryScaff,
#              sorted_bam=merge_bams.merged,
#              fastaReference=create_reference_dictionaries.ref,
#              fastaReference_fai=create_reference_dictionaries.ref_fai,
#              fastaReference_dict=create_reference_dictionaries.ref_dict,
#   		     snp_env=snp_env
#     }

#     if (caller != "VARSCAN") {
#        call merge_qc_bam_files {
#         input: config=write_snps_config.config,
#                primaryScaff=create_reference_dictionaries.primaryScaff,
#                gatk_bams=gatkqc.gatk_bams,
#                runid=runid,
# 			   snp_env=snp_env
#        }
#     }

#    if (caller == 'VARSCAN') {
#        Array[File]? bams_varscan=gatkqc.gatk_bams
#    }
#    # If VARSCAN==True, then gatkBamList will be set to gatkqc.gatk_bams
# 	# otherwise it will be set to merge_qc_bam_files.merged_bam
#    Array[File]? gatkBamList =select_first([bams_varscan, merge_qc_bam_files.merged_bam])

#    call insert_size_dist {
#        input: config=write_snps_config.config,
#               primaryScaff=create_reference_dictionaries.primaryScaff,
#               bams=gatkBamList,
# 			   snp_env=snp_env
#    }

#    call gc_depth {
#        input: config=write_snps_config.config,
#               primaryScaff=create_reference_dictionaries.primaryScaff,
#               bams=gatkBamList,
#               fastaReference=create_reference_dictionaries.ref,
#               runid=runid,
# 			   snp_env=snp_env
#    }

#    if (!align_only) {
#        call call_snps {
#            # since I have to use glob to find the "*.dat" file, gcDepthPlotDat has to be
#            # an array. Use gcDepthPlotDat[0] to retrieve the file
#            input: config=write_snps_config.config,
#                   primaryScaff=create_reference_dictionaries.primaryScaff,
#                   gatk_bams=gatkqc.gatk_bams,
#                   gcDepthPlotDat=gc_depth.gcDepthPlotDat,
#                   fastaReference=create_reference_dictionaries.ref,
#                   fastaReference_fai=create_reference_dictionaries.ref_fai,
#                   runid=runid,
# 			 	   snp_env=snp_env
#        }

#         call call_indels {
#             input: config=write_snps_config.config,
#                    primaryScaff=create_reference_dictionaries.primaryScaff,
#                    gatk_bams=gatkqc.gatk_bams,
#                    gatk_bam_bai=gatkqc.gatk_bam_bai,
#                    gcDepthPlotDat=gc_depth.gcDepthPlotDat,
#                    fastaReference=create_reference_dictionaries.ref,
#                    fastaReference_fai=create_reference_dictionaries.ref_fai,
#                    snp_homozygous_dat=call_snps.snp_homozygous_dat,
#                    snp_heterozygous_dat=call_snps.snp_heterozygous_dat,
# 			 	 	snp_env=snp_env
#         }
#    }
}

## ----------------------------- ##
task write_snps_config {
    File reads
    File reference
    String assemindex
    String caller
    String runid
    String lib_id
    Int maskfilter
    Int gapfilter
    String email
    String aligner
    Int num_pairs
    Int num_jobs
    Boolean align_only
    String align_memory
    Boolean combined_fastq
    Float total_bases
    command {
        # hack since using can't use int for total_bases and Float gives number
        # in scientific notation.
        total_bases_real=$(printf '%9.0f\n' ${total_bases})
        capitalized_align_only=
        if [[ ${align_only} == 'true' ]]; then capitalized_align_only='True'; fi
        if [[ ${align_only} == 'false' ]]; then capitalized_align_only='False'; fi
        if [[ ${combined_fastq} == 'true' ]]; then capitalized_combined_fastq='True'; fi
        if [[ ${combined_fastq} == 'false' ]]; then capitalized_combined_fastq='False'; fi
        cat <<- EOF > snps.config
            READS   ${reads}
            REFERENCE ${reference}
            ASSEMINDEX  ${assemindex}
            CALLER  ${caller}
            RUNID  ${runid}
            LIB_ID  ${lib_id}
            MASKFILTER  ${maskfilter}
            GAPFILTER  ${gapfilter}
            EMAIL  ${email}
            ALIGNER  ${aligner}
            NUM_PAIRS  ${num_pairs}
            NUM_JOBS  ${num_jobs}
            ALIGN_ONLY  $capitalized_align_only
            ALIGN_MEMORY  ${align_memory}
            COMBINED_FASTQ  $capitalized_combined_fastq
            TOTAL_BASES  $total_bases_real
        EOF
    }
    output {
        File config="snps.config"
    }

}

task shard {
    File reads
    String bname = basename(reads)
	Int chunk_size

    command {
        set -e -o pipefail

        # fix headers in fastq

        shifter --image=jfroula/jaws-sharding:1.0.10 \
            decompress_and_fix_headers.py -i ${reads} -o ${bname}.fixed

        shifter --image=jfroula/jaws-sharding:1.0.10 \
            fastq_indexer.py --input ${bname}.fixed --output ${bname}.fixed.index

        shifter --image=jfroula/jaws-sharding:1.0.10 \
            create_blocks.py -if ${bname}.fixed.index -ff ${bname}.fixed -of ${bname}.fixed.sharded \
            -bs ${chunk_size} 
    }

    output {
        Array[String] shards = read_lines("${bname}.fixed.sharded")
        File fixed_fastq = "${bname}.fixed"
    }
}

task bbmap_indexing {
    File reference

    command {
        shifter --image=jfroula/aligner-bbmap:1.0.0 bbmap.sh ref=${reference}
    }

    output {
        File ref = "ref"
    }
}


task alignment {
    # fixed_fastq is the uncompressed fastq file with header names
    # converted from -R1 to /1 and -R2 to /2
    File fixed_fastq
    String bname = basename(fixed_fastq)
    File ref
    String coords
    Int threads=4
    # I just want the dirname to ref, not fullpath
    String path_to_ref = sub(ref, "/ref", "")

    command<<<
        start=$(echo ${coords} | awk '{print $1}')
        end=$(echo ${coords} | awk '{print $2}')

        # we are piping a block of the fastq sequence to the aligner
        shifter --image=jfroula/jaws-sharding:1.0.10 \
          shard_reader.py -i ${fixed_fastq} -s $start -e $end | \
        shifter --image=jfroula/aligner-bbmap:1.0.0 \
          bbmap.sh int in=stdin.fq path=${path_to_ref} out=${bname}.sam overwrite keepnames mappedonly threads=${threads}

        # create a sorted bam file from the sam file
        shifter --image=jfroula/aligner-bbmap:1.0.0 \
          samtools view -uS ${bname}.sam | \
        shifter --image=jfroula/aligner-bbmap:1.0.0 \
          samtools sort - -o ${bname}.sorted.bam
    >>>

    output {
        File bam = "${bname}.sorted.bam"
    }
}

 task merge_bams {
     Array[File] bams

     command {
       shifter --image=jfroula/aligner-bbmap:1.0.0 \
         picard MergeSamFiles I=${sep=' I=' bams} OUTPUT=merged.sorted.bam SORT_ORDER=coordinate ASSUME_SORTED=true USE_THREADING=true
     }

     output {
        File merged = "merged.sorted.bam"
     }
 }

task create_reference_dictionaries {
    File reference
    Float num_jobs
	String snp_env
    String reference_bname = basename(reference)
    String ref_prefix = sub(reference_bname,"\\.\\w+$","")

    command {
        ${snp_env}
        ln -s ${reference} .
        create_indexes.py -r ${reference_bname} -n ${num_jobs}
    }

    output {
        File ref = "${reference_bname}"
        File ref_fai = "${reference_bname}.fai"
        File ref_dict = "${ref_prefix}.dict"
        File primaryScaff =  "primaryScaffolds.dat"
    }
}

task gatkqc {
	String snp_env
    File config
    File primaryScaff
    File sorted_bam
    File fastaReference
    File fastaReference_fai
    File fastaReference_dict
    
    command {
        conda activate snp_env
        ln -s ${sorted_bam} . || echo symlink of bam files failed
		${snp_env}
        /global/homes/j/jfroula/Tools/memtime gatkqc.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
        Array[File] gatk_bams = glob("*.gatk.bam")
        Array[File] gatk_bam_bai = glob("*.gatk.bam.bai")
    }
}

task merge_qc_bam_files {
	String snp_env
    File config
    File primaryScaff
    Array[File] gatk_bams
    String runid
    command {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
		${snp_env}
        /global/homes/j/jfroula/Tools/memtime merge_qc_bam_files.py -c ${config} -p ${primaryScaff}
    }
    output {
        Array[File] merged_bam = ["${runid}.gatk.bam"]
    }
}

task insert_size_dist {
	String snp_env
    File config
    File primaryScaff
    Array[File] bams

    command
    {
        ln -s ${sep=' ' bams} . || echo symlink of bam files failed
		${snp_env}
        /global/homes/j/jfroula/Tools/memtime insert_size_dist.py -c ${config} -p ${primaryScaff}
    }
    output {
    }
}

task gc_depth {
	String snp_env
    File config
    File primaryScaff
    Array[File] bams
    File fastaReference
    String runid

    command {
        ln -s ${sep=' ' bams} . || echo symlink of bam files failed
		${snp_env}
        /global/homes/j/jfroula/Tools/memtime gc_depth_plots.py -c ${config} -p ${primaryScaff} -r ${fastaReference}
    }
    output {
        File gcDepthPlotDat = "${runid}.GCdepth.gnuplot.dat"
    }
}

task call_snps {
	String snp_env
    File config
    File primaryScaff
    Array[File] gatk_bams
    File gcDepthPlotDat
    File fastaReference
    File fastaReference_fai
	String runid

    command {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        ln -s ${gcDepthPlotDat} . || echo symlink of gcDepthPlotDat file failed
		${snp_env}
        /global/homes/j/jfroula/Tools/memtime call_snps.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
		File snp_homozygous_dat = "${runid}.SNP.Homozygous.context.dat"
		File snp_heterozygous_dat = "${runid}.SNP.Heterozygous.context.dat"
    }
}

task call_indels {
	String snp_env
    File config
    File primaryScaff
    Array[File] gatk_bams
    Array[File] gatk_bam_bai
    File gcDepthPlotDat
    File fastaReference
    File fastaReference_fai
    File snp_homozygous_dat
    File snp_heterozygous_dat

    command
    {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        ln -s ${sep=' ' gatk_bam_bai} . || echo symlink of bam.bai files failed
        ln -s ${gcDepthPlotDat} . || echo symlink of gcDepthPlotDat file failed
        ln -s ${snp_homozygous_dat} . || echo symlink of snp_homozygous_dat failed
        ln -s ${snp_heterozygous_dat} . || echo symlink of snp_heterozygous_dat failed
		${snp_env}
        /global/homes/j/jfroula/Tools/memtime call_indels.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
}


