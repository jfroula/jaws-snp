# 1) clone the repository
```
git clone git@gitlab.com:jfroula/jaws-snp.git
cd jaws-snp
```
# 2) set up environment vars
In the following set of comands, replace `$path_to_your_repo` with the path you used to save the repository.
For example if you saved to `/Users/bigmac/jaws-snp` then run the following.  Everything should be full paths.

```
path_to_your_repo=/Users/bigmac/jaws-snp

# scripts called by wdl and which represent the tasks
export PATH=$path_to_your_repo/scripts:$PATH

# other dependency sub-scripts
export PATH=$path_to_your_repo/lib/HAGSC_TOOLS/jwj_pythonScripts:$PATH
export PATH=$path_to_your_repo/lib/HAGSC_TOOLS/jwj_pythonScripts/snpCalling:$PATH

export PYTHONPATH=$path_to_your_repo/lib/loriPrepScripts:$PYTHONPATH
export PYTHONPATH=$path_to_your_repo/lib/HAGSC_TOOLS/jwj_pythonScripts:$PYTHONPATH
export PYTHONPATH=$path_to_your_repo/lib/HAGSC_TOOLS:$PYTHONPATH
# export PYTHONPATH=$path_to_your_repo/lib/HAGSC_TOOLS:$path_to_your_repo/lib/HAGSC_TOOLS/jwj_pythonScripts:$path_to_your_repo/lib/loriPrepScripts:$PYTHONPATH

# this variable is used in HAGSC_TOOLS/jwj_pythonScripts/snp_lib.py
# this uses my environment for now but will probably change paths.
export prePICARDcmd="java -jar /global/homes/j/jfroula/.conda/envs/snp_env/share/picard-2.17.2-0/picard.jar"
```

# 3) change file paths in inputs.json
make file paths point to your repository in the `inputs.json` file.  Note that the `snp_env` variable points to my conda environment for now.

# 4) run the pipeline with supplied test data
First you need to edit the input.json file.  All the paths to files should point to where ever you saved the `snp_env` repository.
After this is done, run the following.
```
java -jar /global/dna/projectdirs/DSI/workflows/cromwell/java/cromwell.jar run snp.wdl -i inputs.json
```
note that the inputs.json takes the place of `snps.config`.

# 5) running in JAWS

## Set up environment to run in JAWS (gives you access to JAWS commands "jaws" and "wf")
```
# you can symlink the jaws environment to your home (not required). This example adds the jaws environment to your .conda directory which allows you to just say source activate jaws.
ln -s /global/project/projectdirs/jaws/prod/cli ~/.conda/envs/jaws

# you will either use "conda activate" or "source activate" depending on how you set up conda.
source activate jaws
```

## submit wdl 
```
jaws submit snp.wdl inputs.json
# you should see something like this
Successfully queued job e3efe82e-ce4a-4b7d-8c33-8a63dc30cd80

# now you can copy the job ID to monitor jobs and see logs
# To see status
jaws status e3efe82e-ce4a-4b7d-8c33-8a63dc30cd80

# To see cromwell log
jaws log e3efe82e-ce4a-4b7d-8c33-8a63dc30cd80

# To see running directory
jaws meta e3efe82e-ce4a-4b7d-8c33-8a63dc30cd80
#the last line should be the running dir
    "workflowRoot": "<somedir>/cromwell-executions/HA_snp_wf/e3efe82e-ce4a-4b7d-8c33-8a63dc30cd80"
    
# run jaws and wf without any arguments to see all options
```