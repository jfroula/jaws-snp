workflow HA_snp_wf {
    File reads
    File reference
    File assemindex
    File config
    String caller
    Boolean align_only
    String runid

    # call split_and_align {
    #   input: reads=reads,
    #          fastaReference=reference,
    #          assemindex=assemindex,
    #          config=config
    # }
    call split_and_align_test {
        input: fastaReference=reference
    }
    call gatkqc_test {}



    # call gatkqc {
    #   input: config=config,
    #          primaryScaff=split_and_align.primaryScaff,
    #          sorted_bams=split_and_align.sorted_bams,
    #          fastaReference=split_and_align.ref,
    #          fastaReference_fai=split_and_align.ref_fai,
    #          fastaReference_dict=split_and_align.ref_dict
    #
    # }

    call merge_qc_bam_files_test
    {
        input: runid=runid
    }
    # if (caller != "VARSCAN") {
    #    call merge_qc_bam_files {
    #     input: config=config,
    #            primaryScaff=split_and_align.primaryScaff,
    #            gatk_bams=gatkqc.gatk_bams,
    #            runid=runid
    #    }
    # }
    #
    if (caller == 'VARSCAN') {
        Array[File]? bams_varscan=gatkqc_test.gatk_bams
    }
    if (caller != 'VARSCAN') {
        Array[File]? bams_other=merge_qc_bam_files_test.merged_bam
    }
    Array[Array[File]?] tmpBamList=[bams_varscan, bams_other]
    # this will return whichever array has values in it.
    # if VARSCAN, then the bams in bams_varscan will be set to
    # gatkBamList.
    Array[File] gatkBamList = select_first(tmpBamList)
    #
    # call insert_size_dist {
    #     input: config=config,
    #            primaryScaff=split_and_align.primaryScaff,
    #            bams=gatkBamList
    # }

    call gc_depth_test {
        input: runid=runid
    }
    # call gc_depth {
    #     input: config=config,
    #            primaryScaff=split_and_align_test.primaryScaff,
    #            bams=gatkBamList,
    #            fastaReference=split_and_align_test.ref
    # }

    if (!align_only) {
        call call_snps {
            # since I have to use glob to find the "*.dat" file, gcDepthPlotDat has to be
            # an array. Use gcDepthPlotDat[0] to retrieve the file
            input: config=config,
                   primaryScaff=split_and_align_test.primaryScaff,
                   gatk_bams=gatkqc_test.gatk_bams,
                   gcDepthPlotDat=gc_depth_test.gcDepthPlotDat,
                   fastaReference=split_and_align_test.ref,
                   fastaReference_fai=split_and_align_test.ref_fai,
                   runid=runid
        }
    
         call call_indels {
             input: config=config,
                    primaryScaff=split_and_align_test.primaryScaff,
                    gatk_bams=gatkqc_test.gatk_bams,
                    gatk_bam_bai=gatkqc_test.gatk_bam_bai,
                    gcDepthPlotDat=gc_depth_test.gcDepthPlotDat,
                    fastaReference=split_and_align_test.ref,
                    fastaReference_fai=split_and_align_test.ref_fai,
                    snp_homozygous_dat=call_snps.snp_homozygous_dat,
                    snp_heterozygous_dat=call_snps.snp_heterozygous_dat
         }
    }
}

task split_and_align_test {
    File splitdir
    File fastaReference
    String bname = basename(fastaReference)
    String prefix = basename(fastaReference, ".fasta")
    command {
        cp -r ${splitdir}/* .
    }
    output {
        File primaryScaff = "primaryScaffolds.dat"
        Array[File] sorted_bams = glob("*.sorted.bam")
        File ref = "${bname}"
        File ref_fai = "${bname}.fai"
        File ref_dict = "${prefix}.dict"
    }
}

task gatkqc_test {
    File gatdir
    command {
        cp -r ${gatdir}/* .
    }
    output {
        Array[File] gatk_bams = glob("*.gatk.bam")
        Array[File] gatk_bam_bai = glob("*.gatk.bam.bai")
    }
}
task merge_qc_bam_files_test {
    File mergedir
    String runid
    command {
        cp -r ${mergedir}/* .
    }
    output {
        Array[File] merged_bam = ["${runid}.gatk.bam"]
    }
}
task gc_depth_test {
    File depthdir
    String runid
    command {
        cp -r ${depthdir}/* .
    }
    output {
        File gcDepthPlotDat = "${runid}.GCdepth.gnuplot.dat"
    }
}
## ----------------------------- ##
task split_and_align {
    File reads
    File fastaReference
    String assemindex
    File config
    String bname = basename(fastaReference)
    String prefix = basename(fastaReference, ".fasta")

    command {
#        split_and_align_wrapper.sh ${fastaReference} ${config} ${assemindex} ${reads}


        ln -s ${fastaReference} .  # copy to local dir so .fai will get created here
        split_and_align.py -c ${config} -i ${assemindex} -r ${bname} -f ${reads}
    }

    output {
        File primaryScaff = "primaryScaffolds.dat"
        Array[File] sorted_bams = glob("*.sorted.bam")
        File ref = "${bname}"
        File ref_fai = "${bname}.fai"
        File ref_dict = "${prefix}.dict"
    }
}

task gatkqc {
    File config
    File primaryScaff
    Array[File] sorted_bams
    File fastaReference
    File fastaReference_fai
    File fastaReference_dict
    
    command
    {
        ln -s ${sep=' ' sorted_bams} . || echo symlink of bam files failed
        gatkqc.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
        Array[File] gatk_bams = glob("*.gatk.bam")
        Array[File] gatk_bam_bai = glob("*.gatk.bam.bai")
    }
}

# split paired end reads for spades (and generate some read stats)
task merge_qc_bam_files {
    File config
    File primaryScaff
    Array[File] gatk_bams
    String runid
    command
    {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        merge_qc_bam_files.py -c ${config} -p ${primaryScaff}
    }
    output {
        Array[File] merged_bam = ["${runid}.gatk.bam"]
    }
}

# run MetaSpades
task insert_size_dist {
    File config
    File primaryScaff
    Array[File] bams

    command
    {
        ln -s ${sep=' ' bams} . || echo symlink of bam files failed
        insert_size_dist.py -c ${config} -p ${primaryScaff}
    }
    output {
    }
}

# fungalrelease step will improve conteguity of assembly
task gc_depth {
    File config
    File primaryScaff
    Array[File] bams
    File fastaReference
    String runid

    command
    {
        ln -s ${sep=' ' bams} . || echo symlink of bam files failed
        gc_depth_plots.py -c ${config} -p ${primaryScaff} -r ${fastaReference}
    }
    # since I have to use glob to find the dat file, gcDepthPlotDat has to be
    # an array. Use gcDepthPlotDat[0] to retrieve the file
    output {
        File gcDepthPlotDat = "runid.GCdepth.gnuplot.dat"
    }
}

# generate some assembly stats for the report
task call_snps {
    File config
    File primaryScaff
    Array[File] gatk_bams
    File gcDepthPlotDat
    File fastaReference
    File fastaReference_fai
	String runid

    command
    {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        ln -s ${gcDepthPlotDat} . || echo symlink of gcDepthPlotDat file failed
        call_snps.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
		File snp_homozygous_dat = "${runid}.SNP.Homozygous.context.dat"
		File snp_heterozygous_dat = "${runid}.SNP.Heterozygous.context.dat"
    }
}

# Map the filtered (but not bfc corrected) reads back to scaffold
task call_indels {
    File config
    File primaryScaff
    Array[File] gatk_bams
    Array[File] gatk_bam_bai
    File gcDepthPlotDat
    File fastaReference
    File fastaReference_fai
    File snp_homozygous_dat
    File snp_heterozygous_dat

    command
    {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        ln -s ${sep=' ' gatk_bam_bai} . || echo symlink of bam.bai files failed
        ln -s ${gcDepthPlotDat} . || echo symlink of gcDepthPlotDat file failed
        ln -s ${snp_homozygous_dat} . || echo symlink of snp_homozygous_dat failed
        ln -s ${snp_heterozygous_dat} . || echo symlink of snp_heterozygous_dat failed
        call_indels.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
    }
}


