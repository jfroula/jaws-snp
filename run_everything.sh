#!/bin/bash -l
# set -e
export PATH=/global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/scripts:$PATH
#export PATH=/global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/lib/HAGSC_TOOLS/jwj_pythonScripts:$PATH
#export PYTHONPATH=/global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/lib/HAGSC_TOOLS:/global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/lib/HAGSC_TOOLS/jwj_pythonScripts:/global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/lib/loriPrepScripts
#conda activate snp_env
HOME=$(realpath .)
BAMID='CSPBG'
ALIGN_ONLY=

echo split_and_align.py
mkdir -p split_and_align && cd split_and_align
ln -s $HOME/snps.config .
cp /global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/DOE_UTEX.polished.t635masked.fasta .
ln -s /global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/short.1m.fastq.bz2 .
ln -s /global/cscratch1/sd/jfroula/JAWS/JerryJenkins/WDL/Index .
split_and_align.py --index_prefix Index/INDEX_MAIN --reference DOE_UTEX.polished.t635masked.fasta --fastq short.1m.fastq.bz2
cd $HOME

echo gatkqc.py
mkdir -p gatkqc && cd gatkqc
ln -s $HOME/split_and_align/primaryScaffolds.dat .
ln -s $HOME/snps.config .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta.fai .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.dict .
ln -s $HOME/split_and_align/*.sorted.bam .
gatkqc.py --reference DOE_UTEX.polished.t635masked.fasta
cd $HOME

if [[ $CALLER != 'VARSCAN' ]]; then
    echo merge_qc_bam_files.py
    mkdir -p merge_qc_bam_files && cd merge_qc_bam_files
    ln -s $HOME/snps.config .
    ln -s $HOME/split_and_align/primaryScaffolds.dat .
    ln -s $HOME/gatkqc/*.gatk.bam .
    merge_qc_bam_files.py
    cd $HOME
fi

echo insert_size_dist.py
mkdir -p insert_size_dist && cd insert_size_dist
if [[ $CALLER != 'VARSCAN' ]]; then
    ln -s $HOME/merge_qc_bam_files/$BAMID.gatk.bam .
else
    ln -s $HOME/gatkqc/*.gatk.bam .
fi

ln -s $HOME/snps.config .
ln -s $HOME/split_and_align/primaryScaffolds.dat .
insert_size_dist.py
cd $HOME

echo gc_depth_plots.py
mkdir -p gc_depth && cd gc_depth
if [[ $CALLER != 'VARSCAN' ]]; then
    ln -s $HOME/merge_qc_bam_files/$BAMID.gatk.bam .
else
    ln -s $HOME/gatkqc/*.gatk.bam .
fi
ln -s $HOME/snps.config .
ln -s $HOME/split_and_align/primaryScaffolds.dat .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta .
gc_depth_plots.py --reference DOE_UTEX.polished.t635masked.fasta
cd $HOME

if [[ $ALIGN_ONLY ]]; then
    echo pipeline done
    exit
fi

echo call_snps.py
mkdir -p call_snps && cd call_snps
ln -s $HOME/gc_depth/$BAMID.GCdepth.gnuplot.dat .
ln -s $HOME/gatkqc/*.gatk.bam .
ln -s $HOME/snps.config .
ln -s $HOME/split_and_align/primaryScaffolds.dat .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.dict .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta.fai .
call_snps.py --reference DOE_UTEX.polished.t635masked.fasta
cd $HOME

echo call_indels.py
mkdir -p call_indels && cd call_indels
ln -s $HOME/gatkqc/*.gatk.bam .
ln -s $HOME/gatkqc/*.gatk.bam.bai .
ln -s $HOME/gc_depth/$BAMID.GCdepth.gnuplot.dat .
ln -s $HOME/snps.config .
ln -s $HOME/split_and_align/primaryScaffolds.dat .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.fasta.fai .
ln -s $HOME/split_and_align/DOE_UTEX.polished.t635masked.dict .
ln -s $HOME/call_snps/$BAMID.SNP.Homozygous.context.dat .
ln -s $HOME/call_snps/$BAMID.SNP.Heterozygous.context.dat .
#ln -s $HOME/gc_depth/$BAMID.GCdepth.normHist.dat .
#ln -s $HOME/gc_depth/$BAMID.GCdepth.data.dat .
call_indels.py --reference DOE_UTEX.polished.t635masked.fasta
cd $HOME



