FROM continuumio/miniconda2

RUN conda install -c bioconda gnuplot==5.0.4
RUN conda install -c conda-forge libgd==2.2.5
RUN conda install -c bioconda blat=35
RUN conda install -c bioconda bwa==0.7.15
RUN conda install -c bioconda picard==2.17.2
RUN conda install -c bioconda samtools==0.1.19
RUN conda install -c conda-forge numpy==1.14.0 

# installing gatk
apt-get update && apt-get install -y wget 
COPY GenomeAnalysisTK-3.6-0-g89b7209.tar.bz2 /root/GenomeAnalysisTK-3.6-0-g89b7209.tar.bz2
RUN conda install -c bioconda gatk==3.6
RUN bash ~/.conda/envs/snp_env/opt/gatk-3.6/gatk-register.sh /root/GenomeAnalysisTK-3.6-0-g89b7209.tar.bz2

COPY loriPrepScript /root
COPY HAGSC_TOOLS /root
ENV PATH /root/HAGSC_TOOLS/jwj_pythonScripts:$PATH
ENV PYTHONPATH /root/loriPrepScripts
ENV PYTHONPATH /root/HAGSC_TOOLS:$PYTHONPATH
ENV PYTHONPATH /root/HAGSC_TOOLS/jwj_pythonScripts:$PYTHONPATH
