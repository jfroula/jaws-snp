#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import (parseConfigFile, writeAndEcho, throwError, jobTypeSet, 
                    gatkQC, parseConfigFile, pullAverageDepth, isBad_READ_REGEX, writeAndEcho, call_SNPs,
                    call_INDELs, writeSummaryFiles, check_PHRED_encoding, createGroupDictionary)

import argparse
from os.path import abspath, join
from os import curdir, system
from sys import argv, stderr, stdout
import sys, os
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify
from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files
from subprocess import Popen, PIPE, STDOUT
#==============================================================

parser = argparse.ArgumentParser(description='run split_and_align step')

parser.add_argument( "-c", \
                   "--config", \
                   type    = str, \
                   help    = "Full path to config  Default: currdir", \
                   default = "./snps.config")


parser.add_argument( "-r", \
                   "--reference", \
                   type    = str, \
                   required = True, \
                   help    = "path to reference fasta ")

parser.add_argument( "-i", \
                   "--index_prefix", \
                   type    = str, \
                   required = True, \
                   help    = "path to assembly index prefix (prefix of each index file)")

parser.add_argument( "-f", \
                   "--fastq", \
                   type    = str, \
                   required = True, \
                   help    = "path to fastq input ")


### Parsing the arguments and set variables ###

args = parser.parse_args()
    
# Finding the base path
basePath = abspath( curdir )

# derive runPath from FASTQ_split_jaws.awk path
result = Popen('which FASTQ_split_jaws.awk', shell=True, stdout=PIPE).communicate()[0]
runPath = os.path.dirname(result)
if not os.path.exists(runPath):
	print("runPath could not be determined from \"which FASTQ_split_jaws.awk\"")
	print(runPath)
	sys.exit(1)

# fasta reference
genomeFASTA = args.reference
if not os.path.exists(genomeFASTA):
    print("Fasta reference not found: {}".format(genomeFASTA))
    sys.exit(1)

# fasta reference
fastqFile  = args.fastq
if not os.path.exists(fastqFile):
    print("Fastq input file not found: {}".format(fastqFile))
    sys.exit(1)

# fasta reference
assemIndex = args.index_prefix
if not os.path.isdir(os.path.dirname(assemIndex)):
    print("Index dir not found: {}".format(assemIndex))
    sys.exit(1)

# Setting the config file name
configFile = args.config
if not os.path.exists(configFile):
    print("config file not found: {}".format(configFile))
    sys.exit(1)

# Read the config file
configDict, cmd_log = parseConfigFile( configFile, basePath )

# Number of insert sizes to sample
inserts_to_sample = 100000

# Size of the sequence bin
GC_Depth_binSize = 5000

primaryScaffsFile = 'primaryScaffolds.dat'

# Read the config file
configDict, cmd_log = parseConfigFile(configFile, basePath)


# Checking for interleaved reads or not
if ( len(configDict['READS']) == 2 ):
    throwError("ALIGNER CAN ONLY ACCEPT INTERLEAVED READS")


# Checking the read regular expression
stdout.write( 'Checking the validity of the read REGEX....\n' )
tmpRegex, tmpReadID, isBad_REGEX = isBad_READ_REGEX( configDict['READ_REGEX'], configDict['READS'][0], configDict['COMBINED_FASTQ'] )
if ( isBad_REGEX ):
    throwError( 'The READ_REGEX does not match the readID\n---\nREGEX\t%s\nREADID\t%s' % ( tmpRegex, tmpReadID ) )
else:
    stdout.write( 'The READ_REGEX matches the readID\n---\nREGEX\t%s\nREADID\t%s' % ( tmpRegex, tmpReadID ) )
#####

cmdList = []
fullname = os.path.basename(genomeFASTA)
prefix,suffix = os.path.splitext(fullname)
samIndex    = 'samtools faidx %s'%(genomeFASTA)
#picardDict  = 'picard CreateSequenceDictionary R=%s O=%s.dict' % (genomeFASTA, fullname)
altDict     = 'picard CreateSequenceDictionary R=%s O=%s.dict' % (genomeFASTA,prefix)
#altDict     = 'ln -s %s %s.dict' % (fullname,prefix)
# Building the command set
cmdList.append( samIndex   )
#cmdList.append( picardDict )
cmdList.append( altDict    )

# Writing to the log file
writeAndEcho( '\n-----------------------\n- Starting the computation:', cmd_log )
for cmd in cmdList: writeAndEcho( cmd, cmd_log )

# Running the job
maxTime   = "00:30:00"
maxMemory = '2G'

###################################################################
### This section was pulled from snpJobLauncher.split_and_align ###
###################################################################


# Order the scaffolds by size
stderr.write('- Computing the total number of bases in the genome\n')
DSU = [(len(r.seq), r.id) for r in iterFASTA(open(configDict['REFERENCE']))]
DSU.sort(reverse=True)

# Computing the target file size to balance the load
targetNumBases = int(float(sum([item[0] for item in DSU])) / float(configDict['NUM_JOBS']))

# Group scaffolds starting with the largest
oh = open(primaryScaffsFile, 'w')
sizeCounter = 0
groupCounter = 1
for scaffSize, scaffID in DSU:
    # Writing the scaffold to the current file
    oh.write('%s\t%d\n' % (scaffID, groupCounter))
    # Incrementing the size
    sizeCounter += scaffSize
    if (sizeCounter > targetNumBases):
        stderr.write('\t\tGroup: %d, Size: %s, Target: %s\n' % ( \
            groupCounter, commify(sizeCounter), commify(targetNumBases)))
        sizeCounter = 0
        groupCounter += 1
    #####
#####
stderr.write(
    '\t\tGroup: %d, Size: %s, Target: %s\n' % (groupCounter, commify(sizeCounter), commify(targetNumBases)))
oh.close()

#
# Building the awk command and running the alignments
#
nextStep = 'merge_bam_files'
varString = ' '.join(['-v nextStep=\"%s\"' % nextStep, \
                      '-v configFile=\"%s\"' % configFile, \
                      '-v ALIGNER=\"%s\"' % configDict['ALIGNER'], \
                      '-v RUN_ID=\"%s\"' % configDict['RUNID'], \
                      '-v pairsPerFile=\"%d\"' % configDict['NUM_PAIRS'], \
                      '-v basePath=\"%s\"' % ".", \
                      '-v runPath=\"%s\"' % runPath, \
                      '-v INDEX=\"%s\"' % assemIndex, \
                      '-v fastqFile=\"%s\"' % fastqFile, \
                      '-v PRIMARY_SCAFFS_FILE=\"%s\"' % primaryScaffsFile, \
                      '-v ALIGN_MEMORY=\"%s\"' % configDict['ALIGN_MEMORY'], \
                      '-v TOTAL_BASES=\"%s\"' % configDict['TOTAL_BASES'], \
                      '-v EMAIL_ADDRESS=\"%s\"' % configDict['EMAIL']])
splitCmd = join(runPath,'FASTQ_split_jaws.awk')
awkCmd = 'awk %s -f %s' % (varString, splitCmd)
if (configDict['COMBINED_FASTQ']):
    tmpCmdList = ['bzcat -k -c %s | %s' % (configDict['READS'][0], awkCmd)]
else:
    if (isGzipFile(configDict['READS'][0])):
        tmpCmdList = ['gunzip -c %s | %s' % (configDict['READS'][0], awkCmd)]
    elif (isBzipFile(configDict['READS'][0])):
        tmpCmdList = ['bunzip2 -c %s | %s' % (configDict['READS'][0], awkCmd)]
    else:
        tmpCmdList = ['cat %s | %s' % (configDict['READS'][0], awkCmd)]
    #####
#####

cmdList.append(' '.join(tmpCmdList))

# Writing to the log file
writeAndEcho('\n-----------------------\n- Aligning reads to create the split bam files:', cmd_log)
for cmd in cmdList: writeAndEcho(cmd, cmd_log)

# Running the job
maxTime = '20:00:00'
maxMemory = '10G'
sh_File = join(basePath, '%s_align_and_split.sh' % configDict['RUNID'])

genePool_submitter(cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False,
                   waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1)



#
# Merging bam files
#
cmdList = []
groupDict, finalGroup_ID = createGroupDictionary(primaryScaffsFile)
jobID_file = join(basePath, '%s.merge_bam.jobIDs.dat' % (configDict['RUNID']))
jobID_oh = open(jobID_file, 'w')

# Merging the files together
nReps = len([line.strip() for line in open(join(basePath, 'full_fastq_file_list.dat'))])
suppressOutput = False
for groupID in groupDict.iterkeys():
    bamFileList = []
    shFileList = []
    for n in xrange(1, (nReps + 1)):
        bamFileList.append(join(basePath, '%s.%s.%d.sorted.bam' % (configDict['RUNID'], groupID, n)))
        shFileList.append(join(basePath, '%s.%d.*.sorting.sh' % (configDict['RUNID'], n)))

    joinedBamList = ' '.join(bamFileList)
    joined_sh_list = ' '.join(shFileList)

    if len(bamFileList) > 1:
        cmdList = [
            'samtools merge -h %s %s.%s.sorted.bam %s' % (bamFileList[0], configDict['RUNID'], groupID, joinedBamList)]
    else:
        sourcefile = bamFileList[0]
        destfile = "%s.%s.sorted.bam" % (configDict['RUNID'], groupID)
        cmdList = ["mv %s %s" % (sourcefile, destfile)]


    sh_File = join(basePath, '%s.%s.merging.sh' % (configDict['RUNID'], groupID))

    # Creating the sh files
    create_sh_file(cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'])

    # Submitting the job
    submitJob(sh_File)

    # Writing to the log file
    writeAndEcho('\n-----------------------\n- merging split bam files:', cmd_log)
    for cmd in cmdList: writeAndEcho(cmd, cmd_log)

#####
jobID_oh.close()



