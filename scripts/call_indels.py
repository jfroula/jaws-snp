#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import (parseConfigFile, writeAndEcho, throwError, jobTypeSet,
                    gatkQC, parseConfigFile, pullAverageDepth, isBad_READ_REGEX, writeAndEcho,
                    call_INDELs, writeSummaryFiles, check_PHRED_encoding, createGroupDictionary)

import argparse
from os.path import abspath, join
from os import curdir, system
from sys import argv, stderr, stdout
import sys, os
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify
from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files
from subprocess import Popen, PIPE, STDOUT
from glob import glob
#==============================================================

parser = argparse.ArgumentParser(description='run call_indels step')

parser.add_argument( "-c", \
                   "--config", \
                   type    = str, \
                   help    = "Full path to config  Default: currdir", \
                   default = "./snps.config")

parser.add_argument( "-p", \
                   "--scaff", \
                   type    = str, \
                   help    = "path to primaryScaffsFile.dat file Default: ./primaryScaffsFile.dat", \
                   default = "./primaryScaffolds.dat")

parser.add_argument( "-r", \
                     "--reference", \
                     type = str, \
                     required = True, \
                     help = "path to reference fasta")

args = parser.parse_args()


# Test that there are some gatk.bam files in local working directory.
# The source of these will depend on whether the CALLER was VARSCAN or GATK
# TODO: These should be input as arguments
bams=glob("*.gatk.bam")
if not len(bams):
    print("No gatk.bam files were found in this working directory as required.")
    sys.exit(1)

# Test that there is a GCdepth.gnuplot.dat file in local working directory.
# The source of this should be from the gc_depth step
# TODO: This should be input as argument
datfile=glob("*.GCdepth.gnuplot.dat")
if not len(datfile):
    print("No *.GCdepth.gnuplot.dat file found in this working directory as required.")
    sys.exit(1)

# Setting up the primary scaffolds file
primaryScaffsFile = args.scaff
if not os.path.exists(primaryScaffsFile):
    print("<primary scaffolds file>.dat not found: {}".format(primaryScaffsFile))
    sys.exit(1)

# Setting the config file name
configFile = args.config
if not os.path.exists(configFile):
    print("config file not found: {}".format(configFile))
    sys.exit(1)

# fasta reference
fastaReference = args.reference
if not os.path.exists(fastaReference):
    print("Fasta reference not found: {}".format(fastaReference))
    sys.exit(1)

if not os.path.exists("{}.fai".format(fastaReference)):
    print("Index file (*.fai) for {} isn't in same path.".format(fastaReference))
    print("Looking for: {}.fai".format(fastaReference))
    sys.exit(1)

# Finding the base path
basePath = abspath( curdir )


# Read the config file
configDict, cmd_log = parseConfigFile( configFile, basePath )


# Important files
GATKbam = join(basePath, '%s.gatk.bam' % configDict['RUNID'])

# Post filter files
INDEL_postFilter = join(basePath, '%s.GATK.INDEL.postFilter.vcf' % configDict['RUNID'])

# Setting up the soft masking files
INDEL_softMaskFilter_vcf = join(basePath, '%s.GATK.INDEL.softMaskFilter.vcf' % configDict['RUNID'])

# INDEL Context output files
INDEL_het_contextFile = join(basePath, '%s.INDEL.Heterozygous.context.dat' % configDict['RUNID'])
INDEL_hom_contextFile = join(basePath, '%s.INDEL.Homozygous.context.dat' % configDict['RUNID'])

# Reading in the average depth and computing the depth filter for snps
avgDepth = pullAverageDepth(basePath, configDict)
lowFilter = max(4, int(avgDepth / 3.0))
highFilter = max(40, int(avgDepth * 3.0))

# Creating the scaffold list
groupDict, finalGroup_ID = createGroupDictionary(primaryScaffsFile)

# Opening the jobID output file
jobID_file = join(basePath, '%s.indels.jobIDs.dat' % (configDict['RUNID']))
jobID_oh = open(jobID_file, 'w')

maxTime = "12:00:00"
maxMemory = '40G'
suppressOutput = False
for groupID in groupDict.iterkeys():

    # Input/Output Files
    grp_BAM = join(basePath, '%s.%s.gatk.bam' % (configDict['RUNID'], groupID))
    grp_rawCalls = join(basePath, '%s.%s.GATK.INDEL.rawcalls.vcf' % (configDict['RUNID'], groupID))
    grp_preFilter = join(basePath, '%s.%s.GATK.INDEL.preFilter.vcf' % (configDict['RUNID'], groupID))
    grp_postFilter = join(basePath, '%s.%s.GATK.INDEL.postFilter.vcf' % (configDict['RUNID'], groupID))

    # Getting the commands
    cmdList = call_INDELs(fastaReference, grp_BAM, grp_rawCalls, grp_preFilter, grp_postFilter, \
                          configDict['RUNID'], basePath, cmd_log, lowFilter, highFilter, avgDepth, \
                          configDict['MQ_CUTOFF'], configFile)


    # Creating the sh files
    sh_File = join(basePath, '%s_%s_call_INDELS.sh' % (configDict['RUNID'], groupID))
    create_sh_file(cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'])

    # Submitting the job
    submitJob(sh_File)

jobID_oh.close()

# INDEL_Filter
cmdList = ['cat %s/%s.GRP_*.GATK.INDEL.postFilter.vcf > %s'%(basePath,configDict['RUNID'],INDEL_postFilter), \
           'filter_gap_and_masking.py %s %d %d %s %s %s %s'%(fastaReference, configDict['MASKFILTER'], \
                                                                                  configDict['GAPFILTER'], INDEL_postFilter, configDict['RUNID'], \
                                                                                  INDEL_softMaskFilter_vcf, cmd_log)]
# INDEL_context_files
cmdList.append('makeContextFile.py %s %s %s %s %s'%(INDEL_softMaskFilter_vcf, fastaReference, \
                                                    INDEL_het_contextFile, INDEL_hom_contextFile, cmd_log ))

# Reading in the average depth and computing the depth filter for snps
avgDepth = pullAverageDepth(basePath, configDict)
lowFilter = max(4, int(avgDepth / 3.0))
highFilter = max(40, int(avgDepth * 3.0))

# Setting the commands
cmdList.append('writeSummaryFiles.py %s %s %d %d %.3f %.3f %s %s' % (GATKbam, \
                                                                     fastaReference, \
                                                                     configDict['MASKFILTER'], \
                                                                     configDict['GAPFILTER'], \
                                                                     lowFilter, \
                                                                     highFilter, \
                                                                     configDict['RUNID'], \
                                                                     basePath))

# Writing to the log file
writeAndEcho('\n-----------------------\n- Writing the summary files:', cmd_log)
for cmd in cmdList: writeAndEcho(cmd, cmd_log)


maxTime   = "12:00:00"
maxMemory = '40G'
sh_File   = join( basePath, '%s_summaryFiles.sh'%configDict['RUNID'] )
genePool_submitter( cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False, waitUntilDone=False, emailAddress=None, ncores=1 )

