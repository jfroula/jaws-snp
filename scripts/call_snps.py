#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import (parseConfigFile, writeAndEcho, throwError, jobTypeSet,
                    gatkQC, parseConfigFile, pullAverageDepth, isBad_READ_REGEX, writeAndEcho, call_SNPs,
                    call_INDELs, writeSummaryFiles, check_PHRED_encoding, createGroupDictionary)

import argparse
from os.path import abspath, join
from os import curdir, system
from sys import argv, stderr, stdout
import sys, os
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify
from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files
from glob import glob
#==============================================================

parser = argparse.ArgumentParser(description='run call_snp step')

parser.add_argument( "-c", \
                   "--config", \
                   type    = str, \
                   help    = "Full path to config  Default: currdir", \
                   default = "./snps.config")

parser.add_argument( "-p", \
                   "--scaff", \
                   type    = str, \
                   help    = "path to primaryScaffsFile.dat file Default: ./primaryScaffsFile.dat", \
                   default = "./primaryScaffolds.dat")

parser.add_argument( "-r", \
                     "--reference", \
                     type    = str, \
                     required= True, \
                     help    = "path to reference fasta")


args = parser.parse_args()

# Test that there are some gatk.bam files in local working directory.
# The source of these will depend on whether the CALLER was VARSCAN or GATK
# TODO: These should be input as arguments
bams=glob("*.gatk.bam")
if not len(bams):
    print("No gatk.bam files were found in this working directory as required.")
    sys.exit(1)

# Test that there is a GCdepth.gnuplot.dat file in local working directory.
# The source of this should be from the gc_depth step
# TODO: This should be input as argument
datfile=glob("*.GCdepth.gnuplot.dat")
if not len(datfile):
    print("No *.GCdepth.gnuplot.dat file found in this working directory as required.")
    sys.exit(1)

# Setting up the primary scaffolds file
primaryScaffsFile = args.scaff
if not os.path.exists(primaryScaffsFile):
    print("<primary scaffolds file>.dat not found: {}".format(primaryScaffsFile))
    sys.exit(1)

# Setting the config file name
configFile = args.config
if not os.path.exists(configFile):
    print("config file not found: {}".format(configFile))
    sys.exit(1)

# fasta reference
fastaReference = args.reference
if not os.path.exists(fastaReference):
    print("Fasta reference not found: {}".format(fastaReference))
    sys.exit(1)

if not os.path.exists("{}.fai".format(fastaReference)):
    print("Index file (*.fai) for {} isn't in same path.".format(fastaReference))
    print("Looking for: {}.fai".format(fastaReference))
    sys.exit(1)

# Finding the base path
basePath = abspath( curdir )

# Read the config file
configDict, cmd_log = parseConfigFile( configFile, basePath )

# Size of the sequence bin
GC_Depth_binSize = 5000


# Post filter files
SNP_postFilter = join(basePath, '%s.GATK.SNP.postFilter.vcf' % configDict['RUNID'])

# Setting up the soft masking files
SNP_softMaskFilter_vcf = join(basePath, '%s.GATK.SNP.softMaskFilter.vcf' % configDict['RUNID'])

# SNP Context output files
SNP_het_contextFile = join(basePath, '%s.SNP.Heterozygous.context.dat' % configDict['RUNID'])
SNP_hom_contextFile = join(basePath, '%s.SNP.Homozygous.context.dat' % configDict['RUNID'])


# Reading in the average depth and computing the depth filter for snps
avgDepth = pullAverageDepth(basePath, configDict)
lowFilter = max(4, int(avgDepth / 3.0))
highFilter = max(40, int(avgDepth * 3.0))

# Creating the scaffold list
groupDict, finalGroup_ID = createGroupDictionary(primaryScaffsFile)

# Opening the jobID output file
jobID_file = join(basePath, '%s.snps.jobIDs.dat' % (configDict['RUNID']))
jobID_oh = open(jobID_file, 'w')

# Launching the bam sorting jobs
nextStep = 'snp_Filter'
maxTime = "12:00:00"
maxMemory = '20G'
suppressOutput = False
for groupID in groupDict.iterkeys():

   # Input/Output Files
   grp_BAM = join(basePath, '%s.%s.gatk.bam' % (configDict['RUNID'], groupID))
   grp_rawCalls = join(basePath, '%s.%s.GATK.SNP.rawcalls.vcf' % (configDict['RUNID'], groupID))
   grp_preFilter = join(basePath, '%s.%s.GATK.SNP.preFilter.vcf' % (configDict['RUNID'], groupID))
   grp_postFilter = join(basePath, '%s.%s.GATK.SNP.postFilter.vcf' % (configDict['RUNID'], groupID))

   # Getting the commands
   cmdList = call_SNPs(fastaReference, grp_BAM, grp_rawCalls, grp_preFilter, grp_postFilter, \
                       configDict['RUNID'], basePath, configFile, lowFilter, highFilter, avgDepth, \
                       configDict['MQ_CUTOFF'], cmd_log)

   # Creating the sh files
   sh_File = join(basePath, '%s_%s_call_SNPs.sh' % (configDict['RUNID'], groupID))
   create_sh_file(cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=None)

   # Submitting the job
   submitJob(sh_File)

jobID_oh.close()

# snp_Filter
cmdList = ['cat %s/%s.GRP_*.GATK.SNP.postFilter.vcf > %s'%(basePath,configDict['RUNID'],SNP_postFilter), \
           'filter_gap_and_masking.py %s %d %d %s %s %s %s'%(fastaReference, configDict['MASKFILTER'], \
                                                                                  configDict['GAPFILTER'], SNP_postFilter, configDict['RUNID'], \
                                                                                  SNP_softMaskFilter_vcf, cmd_log )]
# create_SNP_Plots
# cmdList.append('make_SNP_plots.py %s %s %s %s %s %s'%(SNP_softMaskFilter_vcf, configDict['RUNID'], basePath, \
#                                                                           cmd_log, fastaReference, GC_Depth_binSize ))

# SNP_context_filter
cmdList.append('makeContextFile.py %s %s %s %s %s'%(SNP_softMaskFilter_vcf, fastaReference, \
                                                                         SNP_het_contextFile, SNP_hom_contextFile, cmd_log ) )

# Creating the sh files
sh_File = join(basePath, '%s_call_SNPs_summary.sh' % (configDict['RUNID']))
create_sh_file(cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=None)

# Submitting the job
submitJob(sh_File)

