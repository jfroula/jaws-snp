#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import (parseConfigFile, writeAndEcho, throwError, jobTypeSet,
                    gatkQC, parseConfigFile, pullAverageDepth, isBad_READ_REGEX, writeAndEcho, call_SNPs,
                    call_INDELs, writeSummaryFiles, check_PHRED_encoding, createGroupDictionary)

import argparse
from os.path import abspath, join
from os import curdir, system
from sys import argv, stderr, stdout
import sys, os
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify
from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files
from glob import glob
#==============================================================

parser = argparse.ArgumentParser(description='run insert_size_dist step')

parser.add_argument( "-c", \
                   "--config", \
                   type    = str, \
                   help    = "Full path to config  Default: currdir", \
                   default = "./snps.config")

parser.add_argument( "-p", \
                   "--scaff", \
                   type    = str, \
                   help    = "path to primaryScaffsFile.dat file Default: ./primaryScaffsFile.dat", \
                   default = "./primaryScaffolds.dat")



# Test that there are some gatk.bam files in local working directory.
# The source of these will depend on whether the CALLER was VARSCAN or GATK
# TODO: These should be input as arguments
bams=glob("*.gatk.bam")
if not len(bams):
    print("No gatk.bam files were found in this working directory as required.")
    sys.exit(1)

args = parser.parse_args()

# Setting up the primary scaffolds file
primaryScaffsFile = args.scaff
if not os.path.exists(primaryScaffsFile):
    print("<primary scaffolds file>.dat not found: {}".format(primaryScaffsFile))
    sys.exit(1)

# Setting the config file name
configFile = args.config
if not os.path.exists(configFile):
    print("config file not found: {}".format(configFile))
    sys.exit(1)

# Finding the base path
basePath = abspath( curdir )

# Read the config file
configDict, cmd_log = parseConfigFile( configFile, basePath )

# Number of insert sizes to sample
inserts_to_sample = 100000

# Important files
GATKbam = '%s.gatk.bam' % configDict['RUNID']


# Creating the scaffold list
groupDict, finalGroup_ID = createGroupDictionary(primaryScaffsFile)
groupList = [item for item in groupDict.keys()]

# If the caller is VARSCAN, we still need to get some basic statistics on the run
#configDict['CALLER'] = 'VARSCAN'
#bamID=0
if (configDict['CALLER'] == 'VARSCAN'):
    bamID = '%s.%s' % (configDict['RUNID'], groupList[0])
    inputBAM_file = '%s.gatk.bam' % (bamID)
else:
    inputBAM_file = GATKbam


# Setting the commands
#    nextStep = 'GC_Depth_plots'
cmdList = ['insertSize.py %s %s %s %s %d' % (
 inputBAM_file, basePath, configDict['RUNID'], cmd_log, inserts_to_sample)]

# Writing to the log file
writeAndEcho('\n-----------------------\n- Computing Insert Sizes:', cmd_log)
for cmd in cmdList: writeAndEcho(cmd, cmd_log)

# Running the job
maxTime = "2:00:00"
maxMemory = '5G'
sh_File = join(basePath, '%s_insertSize.sh' % configDict['RUNID'])
genePool_submitter(cmdList, maxTime, maxMemory, sh_File, supressOutput=False, delete_sh_file=False,
                   waitUntilDone=False, emailAddress=configDict['EMAIL'], ncores=1)


