#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import (parseConfigFile, writeAndEcho, throwError, jobTypeSet,
                    gatkQC, parseConfigFile, pullAverageDepth, isBad_READ_REGEX, writeAndEcho, call_SNPs,
                    call_INDELs, writeSummaryFiles, check_PHRED_encoding, createGroupDictionary)

import argparse
from os.path import abspath, join
from os import curdir, system
from sys import argv, stderr, stdout
import sys, os
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify
from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files
from glob import glob
#==============================================================

# # Defining the program args

parser = argparse.ArgumentParser(description='run gatkQC step')

parser.add_argument( "-c", \
                   "--config", \
                   type    = str, \
                   help    = "Full path to config  Default: currdir", \
                   default = "./snps.config")

parser.add_argument( "-p", \
                   "--scaff", \
                   type    = str, \
                   help    = "path to primaryScaffsFile.dat file Default: ./primaryScaffsFile.dat", \
                   default = "./primaryScaffolds.dat")

parser.add_argument( "-r", \
                     "--reference", \
                     type = str, \
                     required = True, \
                     help = "path to reference fasta")

args = parser.parse_args()

# Test that there are some sorted.bam files in local working directory.
# TODO: These should be input as arguments
bams=glob("*.sorted.bam")
if not len(bams):
    print("No sorted.bam files were found in this working directory as required.")
    sys.exit(1)

# Setting up the primary scaffolds file
primaryScaffsFile = args.scaff
if not os.path.exists(primaryScaffsFile):
    print("<primary scaffolds file>.dat not found: {}".format(primaryScaffsFile))
    sys.exit(1)

# Setting the config file name
configFile = args.config
if not os.path.exists(configFile):
    print("config file not found: {}".format(configFile))
    sys.exit(1)

# fasta reference
fastaReference = args.reference
if not os.path.exists(fastaReference):
    print("Fasta reference not found: {}".format(fastaReference))
    sys.exit(1)

if not os.path.exists("{}.fai".format(fastaReference)):
    print("Index file (*.fai) for {} isn't in same path.".format(fastaReference))
    print("Looking for: {}.fai".format(fastaReference))
    sys.exit(1)

# Finding the base path
basePath = abspath( curdir )

# Read the config file
configDict, cmd_log = parseConfigFile( configFile, basePath )

 # Setting the timing variables
maxTime   = '12:00:00'
maxMemory = '50G'

# Creating the scaffold list
groupDict, finalGroup_ID = createGroupDictionary( primaryScaffsFile )

# Opening the jobID output file
jobID_file = join( basePath, '%s.gatkQC.jobIDs.dat'%(configDict['RUNID']) )
jobID_oh   = open( jobID_file, 'w' )

# Launching the bam sorting jobs
suppressOutput = False
for groupID in groupDict.iterkeys():
    # bamID
    bamID              = '%s.%s'%( configDict['RUNID'], groupID )
    # Input bam file
    sortedBamFile      = join( basePath, '%s.sorted.bam'%( bamID ) )
    # Output bam file
    GATKbam_outputFile = join( basePath, '%s.gatk.bam'%( bamID ) )
    # Setting the commands
    cmdList = gatkQC( basePath, GATKbam_outputFile, sortedBamFile, bamID, \
                      configDict['RUNID'], fastaReference, configDict['READ_REGEX'], cmd_log )

    # Making the .sh submission file
    sh_File         = join( basePath, '%s.gatkQC.sh'%( bamID ) )
    # Creating the sh files
    create_sh_file( cmdList, maxTime, maxMemory, sh_File, suppressOutput, emailAddress=configDict['EMAIL'] )

    # Submitting the job
    print("*** Submitting job for  gatk {}".format(bamID))
    submitJob( sh_File )

    # Writing to the log file
    writeAndEcho('\n-----------------------\n- running gatkQC:', cmd_log)
    for cmd in cmdList: writeAndEcho(cmd, cmd_log)

jobID_oh.close()


