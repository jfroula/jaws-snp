#!/usr/bin/env python
import json
import sys

if len(sys.argv) < 2:
	print("Usage: {} <inputs.json>".format(sys.argv[0]))
	sys.exit(1)

config_keys = [ "RUNID","LIB_ID","REFERENCE","ASSEMINDEX","READS","MASKFILTER","GAPFILTER","EMAIL","ALIGNER","CALLER","NUM_PAIRS","NUM_JOBS","ALIGN_ONLY","ALIGN_MEMORY","COMBINED_FASTQ","TOTAL_BASES"]

with open(sys.argv[1],"r") as config:
	data = json.load(config)
	for line in data.keys():
		a = line.split(".")[-1].upper()

		# only print out snps.config keys and ignore other WDL specific entries
		if (a not in config_keys):
			continue

		print("{}\t{}".format(a,data[line]))

