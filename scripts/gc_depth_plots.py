#!/usr/bin/env python

__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"
__date__ ="Created:  4/27/15"

from snp_lib import (parseConfigFile, writeAndEcho, throwError, jobTypeSet,
                    gatkQC, parseConfigFile, pullAverageDepth, isBad_READ_REGEX, writeAndEcho, call_SNPs,
                    call_INDELs, writeSummaryFiles, check_PHRED_encoding, createGroupDictionary)

import argparse
from os.path import abspath, join
from os import curdir, system
from sys import argv, stderr, stdout
import sys, os
from hagsc_lib import isGzipFile, isBzipFile, iterFASTA, commify
from gp_lib import genePool_submitter, create_sh_file, submitJob, jobFinished_Files
from subprocess import Popen, PIPE, STDOUT
from glob import glob
#==============================================================

parser = argparse.ArgumentParser(description='run gc_depth_plots step')

parser.add_argument( "-c", \
                   "--config", \
                   type    = str, \
                   help    = "Full path to config  Default: currdir", \
                   default = "./snps.config")

parser.add_argument( "-p", \
                   "--scaff", \
                   type    = str, \
                   help    = "path to primaryScaffsFile.dat file Default: ./primaryScaffsFile.dat", \
                   default = "./primaryScaffolds.dat")

parser.add_argument( "-r", \
                     "--reference", \
                     type    = str, \
                     required= True, \
                     help    = "path to reference fasta")

args = parser.parse_args()

# Test that there are some gatk.bam files in local working directory.
# The source of these will depend on whether the CALLER was VARSCAN or GATK
# TODO: These should be input as arguments
bams=glob("*.gatk.bam")
if not len(bams):
    print("No gatk.bam files were found in this working directory as required.")
    sys.exit(1)

# Setting up the primary scaffolds file
primaryScaffsFile = args.scaff
if not os.path.exists(primaryScaffsFile):
    print("<primary scaffolds file>.dat not found: {}".format(primaryScaffsFile))
    sys.exit(1)

# Setting the config file name
configFile = args.config
if not os.path.exists(configFile):
    print("config file not found: {}".format(configFile))
    sys.exit(1)

# fasta reference
fastaReference = args.reference
if not os.path.exists(fastaReference):
    print("Fasta reference not found: {}".format(fastaReference))
    sys.exit(1)

# Finding the base path
basePath = abspath( curdir )

# Read the config file
configDict, cmd_log = parseConfigFile( configFile, basePath )

# Size of the sequence bin
GC_Depth_binSize = 5000

# Important files
GATKbam = join(basePath, '%s.gatk.bam' % configDict['RUNID'])


# Creating the scaffold list
groupDict, finalGroup_ID = createGroupDictionary(primaryScaffsFile)
groupList = [item for item in groupDict.keys()]

# Setting up the calling
maxTime = "12:00:00"  # hours
maxMemory = '40G'

cmd=''
if (configDict['CALLER'] == 'VARSCAN'):
    # one bam file is selected at random
    bamID = '%s.%s' % (configDict['RUNID'], groupList[0])
    inputBAM_file = join(basePath, '%s.gatk.bam' % (bamID))
    cmd = 'GC_vs_Depth.py %s %s %s %s %d %s' % (
      inputBAM_file, fastaReference, basePath, configDict['RUNID'], GC_Depth_binSize, cmd_log)
    result = Popen(cmd, shell=True, stdout=PIPE).communicate()[0]
    print("GC_vs_Depth.py using VARSCAN and bam: %s"%inputBAM_file)

else:
    cmd = 'GC_vs_Depth.py %s %s %s %s %d %s' % (
      GATKbam, fastaReference, basePath, configDict['RUNID'], GC_Depth_binSize, cmd_log)
    result = Popen(cmd, shell=True, stdout=PIPE).communicate()[0]
    print("GC_vs_Depth.py using bam: %s"%GATKbam)


# Writing to the log file
writeAndEcho('\n-----------------------\n- Calculating GC depth plots:', cmd_log)
writeAndEcho(cmd, cmd_log)
