#!/usr/bin/env python

import argparse
import sys, os
from sys import argv, stderr, stdout
from subprocess import Popen, PIPE, STDOUT
from hagsc_lib import iterFASTA,commify
#==============================================================

parser = argparse.ArgumentParser(description='creates some index files like .fai, .dict, and primaryScaffolds.dat')

parser.add_argument( "-r", \
                   "--reference", \
                   type    = str, \
                   required = True, \
                   help    = "path to reference fasta ")

parser.add_argument( "-n", \
                   "--num_jobs", \
                   type    = float, \
                   help    = "Number of jobs", \
                   required = True) 

args = parser.parse_args()

#configDict, cmd_log = parseConfigFile( args.config, "." )
    
genomeFASTA = args.reference
if not os.path.exists(genomeFASTA):
    print("Fasta reference not found: {}".format(genomeFASTA))
    sys.exit(1)

# create reference.fai file
fullname = os.path.basename(genomeFASTA)
samIndex    = 'samtools faidx %s -o %s'%(genomeFASTA,fullname)
result = Popen(samIndex, shell=True, stdout=PIPE).communicate()[0]

# create reference dict file
prefix,suffix = os.path.splitext(fullname)
altDict     = 'picard CreateSequenceDictionary R=%s O=%s.dict' % (genomeFASTA,prefix)
result = Popen(altDict, shell=True, stdout=PIPE).communicate()[0]

# Writing to the log file
#writeAndEcho( '\n-----------------------\n- Starting the computation:', cmd_log )
#for cmd in cmdList: writeAndEcho( cmd, cmd_log )


###################################################################
### Create the primaryScaffsFile.                               ###
### This section was pulled from snpJobLauncher.split_and_align ###
###################################################################


# Order the scaffolds by size
stderr.write('- Computing the total number of bases in the genome\n')
DSU = [(len(r.seq), r.id) for r in iterFASTA(open(genomeFASTA))]
DSU.sort(reverse=True)

# Computing the target file size to balance the load
targetNumBases = int(float(sum([item[0] for item in DSU])) / float(args.num_jobs))

# Group scaffolds starting with the largest
primaryScaffsFile = 'primaryScaffolds.dat'
oh = open(primaryScaffsFile, 'w')
sizeCounter = 0
groupCounter = 1
for scaffSize, scaffID in DSU:
    # Writing the scaffold to the current file
    oh.write('%s\t%d\n' % (scaffID, groupCounter))
    # Incrementing the size
    sizeCounter += scaffSize
    if (sizeCounter > targetNumBases):
        stderr.write('\t\tGroup: %d, Size: %s, Target: %s\n' % ( \
            groupCounter, commify(sizeCounter), commify(targetNumBases)))
        sizeCounter = 0
        groupCounter += 1
    #####
#####
stderr.write(
    '\t\tGroup: %d, Size: %s, Target: %s\n' % (groupCounter, commify(sizeCounter), commify(targetNumBases)))
oh.close()

# Writing to the log file
#writeAndEcho('\n-----------------------\n- Aligning reads to create the split bam files:', cmd_log)
#for cmd in cmdList: writeAndEcho(cmd, cmd_log)


