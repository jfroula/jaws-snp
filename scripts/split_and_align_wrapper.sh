#!/bin/bash
# This is a wrapper for the python tasks that were pulled from snpJobLauncher.py.
# The python script that this bash code is wrapping, expects some files to be in local working directory implicitly.
# This is a problem for Cromwell, so this wrapper makes all inputs explicitly called (as positional arguments) and
# symlinks them to working dir.  Cromwell likes each input to be called explicitly by the task.

set -e
REFERENCE=$1
CONFIG=$2
ASSEMINDEX=$3
READS=$4

ARGS=$@
IFS=', ' read -r -a myargs <<< "$ARGS"

function check_file_exists {
    FILE=$1
    if [[ ! -s $FILE ]]; then
        echo "Error: File is missing or empty: $FILE"
        exit
    fi
}

echo count ${#myargs[@]}
if [[ ${#myargs[@]} != 4 ]]; then
	echo "Usage $0 <reference fasta> <config file> <assembly index> <reads fastq>"
	exit
fi

ln -s $REFERENCE . 2> /dev/null || echo $REFERENCE already present
check_file_exists $REFERENCE
LOCAL_REF=${REFERENCE##*/}

ln -s $CONFIG . 2> /dev/null || echo $CONFIG already present
check_file_exists $CONFIG
LOCAL_CONFIG=${CONFIG##*/}


# Verify that we have good fasta index files
set -x
INDEX_PREFIX=${ASSEMINDEX##*/}  # INDEX_MAIN

PREFIX_DIR=${ASSEMINDEX%/*}	    # <directory>/Index
if [[ ! -d $PREFIX_DIR ]]; then
    echo $PREFIX_DIR doesn\'t seem to exist
    exit
fi
files=($(ls $PREFIX_DIR/$INDEX_PREFIX.*))
if [[ ${#files[@]} != 5 ]]; then
    echo "Error: fasta index files not found in $PREFIX_DIR with prefix $INDEX_PREFIX"
    exit
fi

ln -s $PREFIX_DIR . 2> /dev/null || echo $PREFIX_DIR already present

ln -s $READS . 2> /dev/null || echo $READS already present
check_file_exists $READS
LOCAL_READS=${READS##*/}

split_and_align.py -c $LOCAL_CONFIG -i $PREFIX_DIR/$INDEX_PREFIX  -r $LOCAL_REF -f $LOCAL_READS
