workflow HA_snp_wf {
	meta {
	  version: '0.1.0'
	  author: 'Jeff Froula <jlfroula@lbl.gov>'
	}

    File reads
    File reference
    String assemindex
    String caller
    String runid
    String lib_id
    Int maskfilter
    Int gapfilter
    String email
    String aligner
    Int num_pairs
    Int num_jobs
    Boolean align_only
    String align_memory
    Boolean combined_fastq
    Float total_bases
    String snp_env

    call write_snps_config {
        input: reads=reads,
               reference=reference,
               assemindex=assemindex,
               caller=caller,
               runid=runid,
               lib_id=lib_id,
               maskfilter=maskfilter,
               gapfilter=gapfilter,
               email=email,
               aligner=aligner,
               num_pairs=num_pairs,
               num_jobs=num_jobs,
               align_only=align_only,
               align_memory=align_memory,
               combined_fastq=combined_fastq,
               total_bases=total_bases
    }

   call split_and_align {
     input: reads=reads,
            fastaReference=reference,
            assemindex=assemindex,
            config=write_snps_config.config,
       snp_env=snp_env
   }
     call gatkqc {
       input: config=write_snps_config.config,
              primaryScaff=split_and_align.primaryScaff,
              sorted_bams=split_and_align.sorted_bams,
              fastaReference=split_and_align.ref,
              fastaReference_fai=split_and_align.ref_fai,
              fastaReference_dict=split_and_align.ref_dict,
            snp_env=snp_env
     }

    if (caller != "VARSCAN") {
       call merge_qc_bam_files {
        input: config=write_snps_config.config,
               primaryScaff=split_and_align.primaryScaff,
               gatk_bams=gatkqc.gatk_bams,
               runid=runid,
         snp_env=snp_env
       }
    }

    if (caller == 'VARSCAN') {
        Array[File]? bams_varscan=gatkqc.gatk_bams
    }
    # If VARSCAN==True, then gatkBamList will be set to gatkqc.gatk_bams
  # otherwise it will be set to merge_qc_bam_files.merged_bam
    Array[File]? gatkBamList =select_first([bams_varscan, merge_qc_bam_files.merged_bam])

    call insert_size_dist {
        input: config=write_snps_config.config,
               primaryScaff=split_and_align.primaryScaff,
               bams=gatkBamList,
         snp_env=snp_env
    }

    call gc_depth {
        input: config=write_snps_config.config,
               primaryScaff=split_and_align.primaryScaff,
               bams=gatkBamList,
               fastaReference=split_and_align.ref,
               runid=runid,
         snp_env=snp_env
    }

    if (!align_only) {
        call call_snps {
            input: config=write_snps_config.config,
                   primaryScaff=split_and_align.primaryScaff,
                   gatk_bams=gatkqc.gatk_bams,
                   gcDepthPlotDat=gc_depth.gcDepthPlotDat,
                   fastaReference=split_and_align.ref,
                   fastaReference_fai=split_and_align.ref_fai,
                   runid=runid,
           snp_env=snp_env
        }

         call call_indels {
            
             input: config=write_snps_config.config,
                    primaryScaff=split_and_align.primaryScaff,
                    gatk_bams=gatkqc.gatk_bams,
                    gatk_bam_bai=gatkqc.gatk_bam_bai,
                    gcDepthPlotDat=gc_depth.gcDepthPlotDat,
                    fastaReference=split_and_align.ref,
                    fastaReference_fai=split_and_align.ref_fai,
					fastaReference_dict=split_and_align.ref_dict,
                    snp_homozygous_dat=call_snps.snp_homozygous_dat,
                    snp_heterozygous_dat=call_snps.snp_heterozygous_dat,
          snp_env=snp_env
         }
    }
}

## ----------------------------- ##
task write_snps_config {
    File reads
    File reference
    String assemindex
    String caller
    String runid
    String lib_id
    Int maskfilter
    Int gapfilter
    String email
    String aligner
    Int num_pairs
    Int num_jobs
    Boolean align_only
    String align_memory
    Boolean combined_fastq
    Float total_bases
    command {
        # hack since using can't use int for total_bases and Float gives number
        # in scientific notation.
        total_bases_real=$(printf '%9.0f\n' ${total_bases})
        capitalized_align_only=
        if [[ ${align_only} == 'true' ]]; then capitalized_align_only='True'; fi
        if [[ ${align_only} == 'false' ]]; then capitalized_align_only='False'; fi
        if [[ ${combined_fastq} == 'true' ]]; then capitalized_combined_fastq='True'; fi
        if [[ ${combined_fastq} == 'false' ]]; then capitalized_combined_fastq='False'; fi
        cat <<- EOF > snps.config
            READS   ${reads}
            REFERENCE ${reference}
            ASSEMINDEX  ${assemindex}
            CALLER  ${caller}
            RUNID  ${runid}
            LIB_ID  ${lib_id}
            MASKFILTER  ${maskfilter}
            GAPFILTER  ${gapfilter}
            EMAIL  ${email}
            ALIGNER  ${aligner}
            NUM_PAIRS  ${num_pairs}
            NUM_JOBS  ${num_jobs}
            ALIGN_ONLY  $capitalized_align_only
            ALIGN_MEMORY  ${align_memory}
            COMBINED_FASTQ  $capitalized_combined_fastq
            TOTAL_BASES  $total_bases_real
        EOF
    }
    output {
        File config="snps.config"
    }

}
task split_and_align {
  String snp_env
    File reads
    File fastaReference
    String assemindex
    File config
    String bname = basename(fastaReference)
    String prefix = basename(fastaReference, ".fasta")

    command {
        ln -s ${fastaReference} .  # copy to local dir so .fai will get created here
    ${snp_env}
        split_and_align.py -c ${config} -i ${assemindex} -r ${bname} -f ${reads}
    }

    output {
        File primaryScaff = "primaryScaffolds.dat"
        Array[File] sorted_bams = glob("*.sorted.bam")
        File ref = "${bname}"
        File ref_fai = "${bname}.fai"
        File ref_dict = "${prefix}.dict"
    }
}

task gatkqc {
  String snp_env
    File config
    File primaryScaff
    Array[File] sorted_bams
    File fastaReference
    File fastaReference_fai
    File fastaReference_dict
    
    command {
        conda activate snp_env
        ln -s ${sep=' ' sorted_bams} . || echo symlink of bam files failed
    ${snp_env}
        gatkqc.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
        Array[File] gatk_bams = glob("*.gatk.bam")
        Array[File] gatk_bam_bai = glob("*.gatk.bai")
    }
}

# split paired end reads for spades (and generate some read stats)
task merge_qc_bam_files {
  String snp_env
    File config
    File primaryScaff
    Array[File] gatk_bams
    String runid
    command {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
    ${snp_env}
        merge_qc_bam_files.py -c ${config} -p ${primaryScaff}
    }
    output {
        Array[File] merged_bam = ["${runid}.gatk.bam"]
    }
}

# run MetaSpades
task insert_size_dist {
  String snp_env
    File config
    File primaryScaff
    Array[File] bams

    command
    {
        ln -s ${sep=' ' bams} . || echo symlink of bam files failed
    ${snp_env}
        insert_size_dist.py -c ${config} -p ${primaryScaff}
    }
    output {
    }
}

# fungalrelease step will improve conteguity of assembly
task gc_depth {
  String snp_env
    File config
    File primaryScaff
    Array[File] bams
    File fastaReference
    String runid

    command {
        ln -s ${sep=' ' bams} . || echo symlink of bam files failed
    ${snp_env}
        gc_depth_plots.py -c ${config} -p ${primaryScaff} -r ${fastaReference}
    }
    # since I have to use glob to find the dat file, gcDepthPlotDat has to be
    # an array. Use gcDepthPlotDat[0] to retrieve the file
    output {
        File gcDepthPlotDat = "${runid}.GCdepth.gnuplot.dat"
    }
}

# generate some assembly stats for the report
task call_snps {
  String snp_env
    File config
    File primaryScaff
    Array[File] gatk_bams
    File gcDepthPlotDat
    File fastaReference
    File fastaReference_fai
  String runid

    command {
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        ln -s ${gcDepthPlotDat} . || echo symlink of gcDepthPlotDat file failed
		${snp_env}
        call_snps.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
    output {
	  File snp_homozygous_dat = "${runid}.SNP.Homozygous.context.dat"
	  File snp_heterozygous_dat = "${runid}.SNP.Heterozygous.context.dat"
    }
}

# Map the filtered (but not bfc corrected) reads back to scaffold
task call_indels {
  String snp_env
    File config
    File primaryScaff
    Array[File] gatk_bams
    Array[File] gatk_bam_bai
    File gcDepthPlotDat
    File fastaReference
    File fastaReference_fai
    File fastaReference_dict
    File snp_homozygous_dat
    File snp_heterozygous_dat

    command
    {
		# CSPBG.GRP_5.GATK.INDEL.rawcalls.vcf
        ln -s ${sep=' ' gatk_bams} . || echo symlink of bam files failed
        ln -s ${sep=' ' gatk_bam_bai} . || echo symlink of bam.bai files failed
        ln -s ${gcDepthPlotDat} . || echo symlink of gcDepthPlotDat file failed
        ln -s ${snp_homozygous_dat} . || echo symlink of snp_homozygous_dat failed
        ln -s ${snp_heterozygous_dat} . || echo symlink of snp_heterozygous_dat failed
        ln -s ${fastaReference_dict} . || echo symlink of fastaReference_dict failed
    ${snp_env}
        call_indels.py -c ${config} -p ${primaryScaff} --reference ${fastaReference}
    }
}


